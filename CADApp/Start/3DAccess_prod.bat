REM Startdatei für OTC Creo Elements/Direct 3D Access

 
REM Serverpfad
 SET SERVERPFAD=\\CAD-SERVER\CADApp

 SET PTCVERSION=20.3.2.0

REM Lizenzserver
 SET LIZENZSERVER=CAD-SERVER
 
 IF "%COMPUTERNAME%"=="MHOG-PC" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 )
 
 IF "%COMPUTERNAME%"=="TB-DWORSCH2020" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 )

 IF "%COMPUTERNAME%"=="GODER2015" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 ) 
 
REM Einsatzzweck der Startdatei
 SET EINSATZZWECK=%~n0 %PTCVERSION%
 
REM Lokales Installationsziel
 SET INSTZIEL=C:\CADApp

REM Lokale Daten vom Server aktualisieren?
 SET AKTUALISIEREN=Ja
 
REM PTC Programmstart-Parameter
  REM Protokollausgabe FILE=%TEMP%\debug_modeling.txt oder DOS 
  SET SDDEBUGOUTPUT=FILE=%TEMP%\debug_3DAccess.txt
  SET SDVOPTION=-v
   
  REM Sprache 
   SET SDLANG=german
 
  REM Ordner der Modeling-Installation
   SET PTC3DACCESSDIR=%INSTZIEL%\PTC\Creo Elements\Direct 3D Access %PTCVERSION%
 
REM PTC Anpassungen

  REM Pfad für Anpassungen, die bei der lokalen Installation ausgetauscht werden  
  REM SET SDAUSTAUSCHDIR=%SERVERPFAD%\Custom\3D Access\Austausch\20.3_01

  REM Pfad für unternehmensweite Anpassungen 
  REM SET SDCORPCUSTOMIZEDIR=%INSTZIEL%\Custom\3D Access\Corp\20.3_01

  REM Pfad für Standort/Gruppenanpassungen 
  REM SET SDSITECUSTOMIZEDIR=

  REM Pfad für lokale Benutzeranpassungen in %APPDATA%
   SET SDUSERCUSTOMIZEDIR=%APPDATA%\PTC\%EINSATZZWECK%
   
  REM Sprachabhängige Anpassungspfade 
  REM erfordert Pfad mit Ordner für Sprache, z.B. ..\20.3_01\german
   SET SDLANGDEPCUSTOMIZEDIRS=0
  
  REM Anpassungspfade Corp, Site, User mit Version, z.B. 20.2 
  REM Wird nicht genutzt, weil 20.2_01 verwendet wird, um auch innerhalb einer Version verschiedene Anpassungen zu ermöglichen
   SET SDVERSIONDEPCUSTOMIZEDIRS=
  
  REM Corp-Anpassung wird zu Standard-Anpassung hinzugefügt
   SET SDCORPISADDITIVE=1
 
  REM Wenn 1, Anpassungen werden mit load geladen, sonst mit safe-load
  REM mit load stoppt der Ladeprozess beim ersten Fehler
   SET SDDONTUSESAFELOAD=
  
  REM Wenn 1, Schreiben von Toolbars, Ribbinbar usw. wird unterbunden. Keine interaktive Benutzeroberflächenanpassung möglich
   SET SDDISALLOWINTERACTIVECUSTOMIZATION=
  
  REM Wenn 1, Schreiben in *_fluentui_layout.def wird unterbunden. Keine interaktive Benutzeroberflächenanpassung möglich
   SET SDDONTSTOREUILAYOUTFILES=
  
  REM HOME-Laufwerk im Lade-Menü 
   SET SDPROJECTDIRHOME=%HOMEDRIVE%%HOMEPATH%
   
  REM Pfad zum Editor für Textdateien, z.B. Notepad++
   SET SDEDITOR=
   
  REM Pfad zum Icon-Editor, wenn leer, dann Paint
   SET SDTOOLBARICONEDITOR=

REM Anpassungen von LOHMANN
 SET RAHMEN_DIR=%INSTZIEL%\Custom\Rahmen
 SET SD_ANPASSUNG_DIR=%INSTZIEL%\Custom\3D Access\Anpassungen\SD\01
 SET AM_ANPASSUNG_DIR=%INSTZIEL%\Custom\3D Access\Anpassungen\AM\01

REM Lokale Daten aktualisieren, wenn der Server-Pfad erreichbar ist

IF EXIST %SERVERPFAD% (

  REM Protokollierung des Setups durch Robocopy nul oder %TEMP%\Setup_%EINSATZZWECK%.txt
    SET SETUP-PROTOKOLL="%TEMP%\Setup_%EINSATZZWECK%.txt"

  REM Kopieren der benötigten Applikationen auf INSTZIEL
  REM die ~9 sind die Zeichen, die vom Pfad der Installationsquelle von vorne abgezogen werden, sind von %INSTZIEL%

  REM In der Installation von PTC Modeling werden diese Dateien ausgetauscht:
  REM ROBOCOPY "%SDAUSTAUSCHDIR%" "%INSTZIEL%\PTC\Creo Elements\Direct Modeling %PTCVERSION%" /S /E /COPY:DAT /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%
 
  REM Lokale Anpassungesdateien entsprechend dem Stand auf dem Server aktualisieren
    REM ROBOCOPY "%SERVERPFAD%%SDCORPCUSTOMIZEDIR:~9%" "%SDCORPCUSTOMIZEDIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >%SETUP-PROTOKOLL%
    REM ROBOCOPY "%SERVERPFAD%%RAHMEN_DIR:~9%" "%RAHMEN_DIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%
    REM ROBOCOPY "%SERVERPFAD%%SD_ANPASSUNG_DIR:~9%" "%SD_ANPASSUNG_DIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%
    REM ROBOCOPY "%SERVERPFAD%%AM_ANPASSUNG_DIR:~9%" "%AM_ANPASSUNG_DIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%

) 



REM Programmstart
  "%PTC3DACCESSDIR%\SD.exe" -e SDLANG=%SDLANG% "%PTC3DACCESSDIR%\binx64\SolidDesigner.exe" -melshost %LIZENZSERVER% -title "%EINSATZZWECK%" %SDVOPTION%
