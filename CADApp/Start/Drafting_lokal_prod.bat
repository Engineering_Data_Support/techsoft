REM Lokale Startdatei f�r PTC Creo Elements/Direct Drafting
REM Die Verkn�pfung zum Start von Drafting mu� auf diese Datei verweisen
 
REM Serverpfad
 SET SERVERPFAD=\\CAD-SERVER\CADApp
 SET INSTQUELLE=%SERVERPFAD%\Start
REM Lokales Installationsziel
 SET INSTZIEL=C:\CADApp\Start
  
REM Aktualisierung der Startdatei, falls Server erreichbar ist 
IF EXIST "%SERVERPFAD%\Start\Drafting_prod.bat" (
 "%SYSTEMROOT%\system32\robocopy.exe" "%INSTQUELLE%" "%INSTZIEL%" Drafting_prod.bat /R:0 /W:0 /NP
) 

REM Drafting wird von der Startdatei gestartet
 CALL "%INSTZIEL%\Drafting_prod.bat"
