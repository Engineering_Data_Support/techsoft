REM Lokale Startdatei f�r PTC Creo Elements/Direct Modeling
REM Die Verkn�pfung zum Start von Modeling mu� auf diese Datei verweisen
 
REM Serverpfad
 SET SERVERPFAD=\\CAD-SERVER\CADApp
 SET INSTQUELLE=%SERVERPFAD%\Start
 
REM Lokales Installationsziel
 SET INSTZIEL=C:\CADApp\Start
 
REM Aktualisierung der lokalen Startdatei, falls Server erreichbar ist 
IF EXIST "%SERVERPFAD%\Start\Modeling_prod.bat" (
 "%SYSTEMROOT%\system32\robocopy.exe" "%INSTQUELLE%" "%INSTZIEL%" Modeling_prod.bat /R:0 /W:0 /NP
) 

REM Modeling wird von der lokalen Startdatei gestartet
 CALL "%INSTZIEL%\Modeling_prod.bat"
