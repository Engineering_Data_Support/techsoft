REM Startdatei für OTC Creo Elements/Direct Modeling

 
REM Serverpfad
 SET SERVERPFAD=\\CAD-SERVER\CADApp

 SET PTCVERSION=20.3.2.0

REM Lizenzserver
 SET LIZENZSERVER=CAD-SERVER
 
 IF "%COMPUTERNAME%"=="MHOG-PC" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 )
 
 IF "%COMPUTERNAME%"=="TB-DWORSCH2020" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 )

 IF "%COMPUTERNAME%"=="GODER2015" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 ) 
 
REM Einsatzzweck der Startdatei
 SET EINSATZZWECK=%~n0 %PTCVERSION%
 
REM Lokales Installationsziel
 SET INSTZIEL=C:\CADApp

REM PTC Programmstart-Parameter
  REM Protokollausgabe FILE=%TEMP%\debug_modeling.txt oder DOS 
  REM SET SDDEBUGOUTPUT=FILE=%TEMP%\debug_modeling.txt
  REM Protokoll anzeigen -v sonst leer lassen  
   SET SDVOPTION=
   
  REM Sprache 
   SET SDLANG=german
 
  REM Ordner der Modeling-Installation
   SET PTCMODELINGDIR=%INSTZIEL%\PTC\Creo Elements\Direct Modeling %PTCVERSION%
 
REM PTC Anpassungen

  REM Pfad für Anpassungen, die bei der lokalen Installation ausgetauscht werden  
   SET SDAUSTAUSCHDIR=%SERVERPFAD%\Custom\Modeling\Austausch\20.3_01

  REM Pfad für unternehmensweite Anpassungen 
   SET SDCORPCUSTOMIZEDIR=%INSTZIEL%\Custom\Modeling\Corp\20.3_09

  REM Pfad für Standort/Gruppenanpassungen 
   SET SDSITECUSTOMIZEDIR=%INSTZIEL%\Custom\Modeling\Site\20.3_06

  REM Pfad für lokale Benutzeranpassungen in %APPDATA%
   SET SDUSERCUSTOMIZEDIR=%APPDATA%\PTC\%EINSATZZWECK%
   
  REM Sprachabhängige Anpassungspfade 
  REM erfordert Pfad mit Ordner für Sprache, z.B. ..\20.3_01\german
   SET SDLANGDEPCUSTOMIZEDIRS=1
  
  REM Anpassungspfade Corp, Site, User mit Version, z.B. 20.2 
  REM Wird nicht genutzt, weil 20.2_01 verwendet wird, um auch innerhalb einer Version verschiedene Anpassungen zu ermöglichen
   SET SDVERSIONDEPCUSTOMIZEDIRS=
  
  REM Corp-Anpassung wird zu Standard-Anpassung hinzugefügt
   SET SDCORPISADDITIVE=1
 
  REM Wenn 1, Anpassungen werden mit load geladen, sonst mit safe-load
  REM mit load stoppt der Ladeprozess beim ersten Fehler
   SET SDDONTUSESAFELOAD=
  
  REM Wenn 1, Schreiben von Toolbars, Ribbinbar usw. wird unterbunden. Keine interaktive Benutzeroberflächenanpassung möglich
   SET SDDISALLOWINTERACTIVECUSTOMIZATION=
  
  REM Wenn 1, Schreiben in *_fluentui_layout.def wird unterbunden. Keine interaktive Benutzeroberflächenanpassung möglich
   SET SDDONTSTOREUILAYOUTFILES=
  
  REM HOME-Laufwerk im Lade-Menü 
   SET SDPROJECTDIRHOME=%HOMEDRIVE%%HOMEPATH%
   
  REM Pfad zum Editor für Textdateien, z.B. Notepad++
   SET SDEDITOR=
   
  REM Pfad zum Icon-Editor, wenn leer, dann Paint
   SET SDTOOLBARICONEDITOR=

REM Anpassungen für Techsoft

  REM Solid Power
		 SET TSPRODIR=%INSTZIEL%\Techsoft\SolidPower\tspro20_4
		 SET PART_LIBRARY_INSTALLATION_DIR=%TSPRODIR%
   SET OS_SolidPower=1
   SET SOLIDPOWER_PARTS=%INSTZIEL%\Custom\NORMTEILE\01
   
  REM AddON
   SET TSADDONDIR=%INSTZIEL%\Techsoft\AddOn\AM\20.3_01
   
   
REM Anpassungen von NEUE HERBOLD
 SET RAHMEN_DIR=%INSTZIEL%\Custom\Rahmen
 SET SD_ANPASSUNG_DIR=%INSTZIEL%\Custom\Modeling\Anpassungen\SD\01
 SET AM_ANPASSUNG_DIR=%INSTZIEL%\Custom\Modeling\Anpassungen\AM\01

REM Lokale Daten aktualisieren

   REM Kopieren der benötigten Applikationen auf INSTZIEL
   REM die ~9 sind die Zeichen, die vom Pfad der Installationsquelle von vorne abgezogen werden, sind von %INSTZIEL%
 
   IF EXIST "%SERVERPFAD%\Start\Modeling_prod.bat" (

     REM In der Installation von PTC Modeling werden diese Dateien ausgetauscht:
     REM "%SYSTEMROOT%\system32\robocopy.exe" "%SDAUSTAUSCHDIR%" "%INSTZIEL%\PTC\Creo Elements\Direct Modeling %PTCVERSION%" *.* /E /COPY:DAT /R:0 /W:0 /NP /NS /V /LOG:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     
     REM neuere lokale Anpassungen in Annotation zum Server kopieren
     "%SYSTEMROOT%\system32\robocopy.exe" "%SDUSERCUSTOMIZEDIR%\german\Annotation\Sketches" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%\german\Annotation\Sketches" *.*  /E /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SDUSERCUSTOMIZEDIR%\german\Annotation\Symbols" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%\german\Annotation\Symbols" *.*  /E /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SDUSERCUSTOMIZEDIR%\german\Annotation\Texts" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%\german\Annotation\Texts"  *.*  /E /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
    
     REM neuere lokale Anpassungen in SolidPower Material zum Server kopieren
     "%SYSTEMROOT%\system32\robocopy.exe" "%SDUSERCUSTOMIZEDIR%\german\SolidPower\sdp_material\tables\german" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%\german\SolidPower\sdp_material\tables\german" material.csl /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     
     REM neuere lokale Anpassungen in SolidPower Toleranztabelle zum Server kopieren
     "%SYSTEMROOT%\system32\robocopy.exe" "%SDUSERCUSTOMIZEDIR%\german\SolidPower" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%\german\SolidPower" sdp_fits.lsp /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SDUSERCUSTOMIZEDIR%\german\SolidPower" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%\german\SolidPower" sdp_fits.tab /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"

     REM neuere lokale Anpassungen in Custom-Normteilen zum Server kopieren
     "%SYSTEMROOT%\system32\robocopy.exe" "%SOLIDPOWER_PARTS%" "%SERVERPFAD%%SOLIDPOWER_PARTS:~9%" *.* /E /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"

     REM neuere lokale Anpassungen in SolidPower-Normteilen zum Server kopieren
     "%SYSTEMROOT%\system32\robocopy.exe" "%TSPRODIR%" "%SERVERPFAD%%TSPRODIR:~9%" *.* /E /COPY:DAT /XO /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
 
     
     REM Lokale Anpassungen entsprechend dem Stand auf dem Server aktualisieren
     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%SDCORPCUSTOMIZEDIR:~9%" "%SDCORPCUSTOMIZEDIR%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%SDSITECUSTOMIZEDIR:~9%" "%SDSITECUSTOMIZEDIR%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"

     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%RAHMEN_DIR:~9%" "%RAHMEN_DIR%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%SD_ANPASSUNG_DIR:~9%" "%SD_ANPASSUNG_DIR%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%TSADDONDIR:~9%" "%TSADDONDIR%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%AM_ANPASSUNG_DIR:~9%" "%AM_ANPASSUNG_DIR%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
     "%SYSTEMROOT%\system32\robocopy.exe" "%SERVERPFAD%%SOLIDPOWER_PARTS:~9%" "%SOLIDPOWER_PARTS%" *.* /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS /V /LOG+:"%TEMP%\Setup_%EINSATZZWECK%.txt"
    )

REM Programmstart
  "%PTCMODELINGDIR%\SD.exe" -e SDLANG=%SDLANG% "%PTCMODELINGDIR%\binx64\SolidDesigner.exe" -melshost %LIZENZSERVER% -title "%EINSATZZWECK%" %SDVOPTION%
