REM Startdatei für PTC Creo Elements/Direct Drafting

REM Serverpfad
 SET SERVERPFAD=\\CAD-SERVER\CADApp

 SET PTCVERSION=20.3.2.0

REM Lizenzserver
 SET LIZENZSERVER=CAD-SERVER
 
 IF "%COMPUTERNAME%"=="MHOG-PC" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 )
 
 IF "%COMPUTERNAME%"=="DWORSCHAK2015" (
  SET LIZENZSERVER=LOCALHOST;CAD-SERVER
 )
 
REM Einsatzzweck der Startdatei
 SET EINSATZZWECK=%~n0 %PTCVERSION%
 
REM Lokales Installationsziel
 SET INSTZIEL=C:\CADApp

REM Lokale Daten vom Server aktualisieren?
 SET AKTUALISIEREN=Ja
 
REM PTC Programmstart-Parameter
 
  REM Sprache de, en (in der Softare später "C"), es, it, 
   SET LANGUAGE=de
 
  REM Ordner der Drafting-Installation
   SET PTCDRAFTINGDIR=%INSTZIEL%\PTC\Creo Elements\Direct Drafting %PTCVERSION%
 
REM PTC Anpassungen
  
  REM Benutzeroberfläche Pelook 0,1,2,3,4
   SET MEPELOOK=4
   
  REM Pfad für Anpassungen, die bei der lokalen Installation ausgetauscht werden  
   SET MEAUSTAUSCHDIR=%SERVERPFAD%\Custom\Drafting\Austausch\20.3_01
   
  REM Pfad für unternehmensweite Anpassungen 
   SET MECORPCUSTOMIZEDIR=%INSTZIEL%\Custom\Drafting\Corp\20.3_02
  
  REM Pfad für Standort/Gruppenanpassungen 
   SET MESITECUSTOMIZEDIR=%INSTZIEL%\Custom\Drafting\Site\pelook%MEPELOOK%\20.3_01
   
  REM Pfad für lokale Benutzeranpassungen in %APPDATA%
   SET ME_HOME_DIRECTORY=%APPDATA%\PTC\%EINSATZZWECK%
   
  
  REM HOME-Laufwerk im Lade-Menü 
   SET MEPROJECTDIRHOME=%HOMEDRIVE%%HOMEPATH%
   
  REM Pfad zum Editor für Textdateien, z.B. Notepad++
   SET MEEDITOR=
   
REM PDSVISION Tools
 SET PDS2DTOOLDIR=%INSTZIEL%\PDSVision\PDS 2D Tools\20.3_01


REM Anpassungen von NEUE HERBOLD
 SET RAHMEN_DIR=%INSTZIEL%\Custom\Rahmen\01
 
REM Lokale Daten aktualisieren

   REM Kopieren von SERVERPFAD zu INSTZIEL: AKTUALISIEREN="Ja" oder AKTUALISIEREN="Nein"
    SET AKTUALISIEREN="Ja"

   REM Protokollierung des Setups durch Robocopy nul oder %TEMP%\Setup_%EINSATZZWECK%.txt
    SET SETUP-PROTOKOLL="%TEMP%\Setup_%EINSATZZWECK%.txt"

   REM Kopieren der benötigten Applikationen auf INSTZIEL
   REM die ~9 sind die Zeichen, die vom Pfad der Installationsquelle von vorne abgezogen werden, sind von %INSTZIEL%
 
    IF %AKTUALISIEREN%=="Ja" (
    
     REM In der Installation von PTC Drafting werden diese Dateien ausgetauscht:
      ROBOCOPY "%MEAUSTAUSCHDIR%" "%INSTZIEL%\PTC\Creo Elements\Direct Drafting %PTCVERSION%" /S /E /COPY:DAT /R:0 /W:0 /NP /NS >%SETUP-PROTOKOLL%


     REM Lokale Anpassungesdateien entsprechend dem Stand auf dem Server aktualisieren
      ROBOCOPY "%SERVERPFAD%%MECORPCUSTOMIZEDIR:~9%" "%MECORPCUSTOMIZEDIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%
      ROBOCOPY "%SERVERPFAD%%MESITECUSTOMIZEDIR:~9%" "%MESITECUSTOMIZEDIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%
      ROBOCOPY "%SERVERPFAD%%RAHMEN_DIR:~9%" "%RAHMEN_DIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%
      ROBOCOPY "%SERVERPFAD%%PDS2DTOOLDIR:~9%" "%PDS2DTOOLDIR%" /S /E /COPY:DAT /MIR /R:0 /W:0 /NP /NS >>%SETUP-PROTOKOLL%

    )

REM Programmstart
  "%PTCDRAFTINGDIR%\me10.exe" -dir "%PTCDRAFTINGDIR%"  -title "%EINSATZZWECK%" -pelook %MEPELOOK% -nosplash
