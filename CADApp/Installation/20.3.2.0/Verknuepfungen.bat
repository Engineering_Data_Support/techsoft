
REM Installation Verknuepfungen
SET PTCVERSION=20.3.2.0

REM Datenquelle für die Installation
SET SERVERPFAD=\\CAD-SERVER\CADApp
SET INSTQUELLE=%SERVERPFAD%\Start

REM Zielpfad für die Installation
SET INSTZIEL=C:\Users\Public\Desktop

robocopy "%INSTQUELLE%" "%INSTZIEL%" Modeling_prod Drafting_prod /R:0 /W:0 /NP
