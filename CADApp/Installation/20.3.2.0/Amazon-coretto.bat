

REM Datenquelle für die Installation
SET SERVERPFAD=\\CAD-SERVER\CADApp

SET INSTQUELLE=%SERVERPFAD%\AWS\11.0.10.9.1\amazon-corretto-11.0.10.9.1-windows-x64.msi

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\AWS\11.0.10.9.1

REM Installation
MSIEXEC /i "%INSTQUELLE%" INSTALLDIR="%INSTZIEL%" /quiet /qr /norestart /log "%TEMP%\setup_amazon-coretto.log"
