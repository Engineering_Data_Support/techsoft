
REM Installation 2D-Access
SET PTCVERSION=20.3.2.0

REM Lizenzserver, mehrere mit ; getrennt, wird in passwords.m eingetragen
SET LIZENZSERVER=cad-server

REM Datenquelle für die Installation
SET SERVERPFAD=\\CAD-SERVER\CADApp

SET INSTQUELLE=%SERVERPFAD%\PTC\Creo Elements\%PTCVERSION%\MED-60890-CD-203_20-3-2-0_2OF2\Modeling and Drafting\2D CAD\2D Access (x64)

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\PTC\Creo Elements\Direct 2D Access %PTCVERSION%

REM Installation
"%INSTQUELLE%\2DAccess.exe" /s /v"/qb INSTALLDIR=\"%INSTZIEL%\" MELS=\"%LIZENZSERVER%\" ALLUSERS=1 ADDLOCAL=ALL  /log \"%TEMP%\setup_2d-access.log\" "
