REM Installation Modeling
SET PTCVERSION=20.3.2.0

REM Lizenzserver, mehrere mit ; getrennt, wird in Registry eingetragen
SET LIZENZSERVER=cad-server

REM Datenquelle für die Installation
SET SERVERPFAD=\\CAD-SERVER\CADApp

SET INSTQUELLE=%SERVERPFAD%\PTC\Creo Elements\%PTCVERSION%\MED-60890-CD-203_20-3-2-0_1OF2\Modeling and Drafting\3D CAD\Modeling (x64)

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\PTC\Creo Elements\Direct Modeling %PTCVERSION%

REM Installation
"%INSTQUELLE%\setup.exe" /s /v" /qb INSTALLDIR=\"%INSTZIEL%\" MELS=\"%LIZENZSERVER%\" ADDDEFAULT=\"BASE,BINNT,PERS,RENDER,IKIT,HELP_DE,L10N_DE,THUMVIEW\" /log \"%TEMP%\setup_modeling.log\" "
