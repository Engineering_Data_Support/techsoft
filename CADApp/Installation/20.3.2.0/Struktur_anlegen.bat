REM Datenstruktur auf Server erstellen/erweitern
REM Hinweis: Befehlserweiterung muß aktiviert sein (Standard), falls nicht: cmd /e:on


SET SERVERPFAD=K:\NDT\APPLIKATION\CADApp
SET PTCVERSION=20.3.2.0
SET KUNDE=NDT


REM Für PTC, hier hin wird die PTC-Software heruntergeladen
MKDIR "%SERVERPFAD%\PTC\Creo Elements\%PTCVERSION%"

REM Für den Model Manager Server und Clients, hier hin wird Amazon Coretto heruntergeladen
REM MKDIR "%SERVERPFAD%\aws\11.0.10.9.1"

REM Für Zusatzsoftware
MKDIR "%SERVERPFAD%\PDSVision\Tools"

REM Für kundenspezifische Anpassungen
MKDIR "%SERVERPFAD%\%KUNDE%\Modeling\Corp\20.3_01"
MKDIR "%SERVERPFAD%\%KUNDE%\Modeling\Site\20.3_01"
MKDIR "%SERVERPFAD%\%KUNDE%\Modeling\User\20.3_01"
MKDIR "%SERVERPFAD%\%KUNDE%\Drafting\Corp\20.3_01"
MKDIR "%SERVERPFAD%\%KUNDE%\Drafting\User\20.3_01"

REM Für die Startdateien
MKDIR "%SERVERPFAD%\Start"

REM Für die Installationsscripte
MKDIR "%SERVERPFAD%\Installation\%PTCVERSION%"



PAUSE







