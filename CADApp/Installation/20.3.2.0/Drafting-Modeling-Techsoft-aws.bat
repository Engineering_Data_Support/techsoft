

REM Lizenzserver, mehrere mit ; getrennt, wird in passwords.m eingetragen
SET LIZENZSERVER=cad-server

REM Datenquelle für die Installation
SET SERVERPFAD=\\CAD-SERVER\CADApp


REM Installation Drafting
SET PTCVERSION=20.3.2.0

SET INSTQUELLE=%SERVERPFAD%\PTC\Creo Elements\%PTCVERSION%\MED-60890-CD-203_20-3-2-0_1OF2\Modeling and Drafting\2D CAD\Drafting (x64)

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\PTC\Creo Elements\Direct Drafting %PTCVERSION%

"%INSTQUELLE%\OSDD.exe" /s /v"/qb INSTALLDIR=\"%INSTZIEL%\" MELS=\"%LIZENZSERVER%\" ALLUSERS=1 ADDLOCAL=ALL  /log \"%TEMP%\setup_drafting.log\" "


REM Installation Modeling

SET INSTQUELLE=%SERVERPFAD%\PTC\Creo Elements\%PTCVERSION%\MED-60890-CD-203_20-3-2-0_1OF2\Modeling and Drafting\3D CAD\Modeling (x64)

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\PTC\Creo Elements\Direct Modeling %PTCVERSION%

"%INSTQUELLE%\setup.exe" /s /v" /qb INSTALLDIR=\"%INSTZIEL%\" MELS=\"%LIZENZSERVER%\" ADDDEFAULT=\"BASE,BINNT,PERS,RENDER,IKIT,HELP_DE,L10N_DE,THUMVIEW\" /log \"%TEMP%\setup_modeling.log\" "

REM INstallation Amazo-Coretto für Java
SET INSTQUELLE=%SERVERPFAD%\AWS\11.0.10.9.1\amazon-corretto-11.0.10.9.1-windows-x64.msi

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\AWS\11.0.10.9.1

REM Installation
MSIEXEC /i "%INSTQUELLE%" INSTALLDIR="%INSTZIEL%" /quiet /qr /norestart /log "%TEMP%\setup_amazon-coretto.log"


REM Installation Techsoft
SET INSTQUELLE=%SERVERPFAD%\Techsoft\SolidPower\tspro20_4

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\Techsoft\SolidPower\tspro20_4

robocopy "%INSTQUELLE%" "%INSTZIEL%" /E /R:0 /W:0 /NP


REM Installation Verknuepfungen auf zentrale (öffentlichen) Desktop

REM Datenquelle für die Installation
SET INSTQUELLE=%SERVERPFAD%\Start

REM Zielpfad für die Installation
SET INSTZIEL=C:\Users\Public\Desktop

robocopy "%INSTQUELLE%" "%INSTZIEL%" Modeling_prod Drafting_prod /R:0 /W:0 /NP