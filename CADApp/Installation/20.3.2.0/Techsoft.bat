
REM Installation Techsoft
SET PTCVERSION=20.3.2.0

REM Datenquelle für die Installation
SET SERVERPFAD=\\CAD-SERVER\CADApp
SET INSTQUELLE=%SERVERPFAD%\Techsoft\SolidPower\tspro20_4

REM Zielpfad für die Installation
SET INSTZIEL=C:\CADApp\Techsoft\SolidPower\tspro20_4

robocopy "%INSTQUELLE%" "%INSTZIEL%" /E /R:0 /W:0 /NP

