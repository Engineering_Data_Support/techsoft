{ TRACE DEL_OLD ((GETENV "TEMP") + "\trace_2daccess.mac") }

BINDTEXTDOMAIN "me10mac"  (MEDIR + "/locale")
BINDTEXTDOMAIN "me10lt"  (MEDIR + "/locale")
LOAD_MACRO 'layout.mc'

DEFINE Ribbon_layout_localized
INQ_ENV 10
IF ((INQ 6) = 4)
FUI_RIBBON_LAYOUT_LOCALIZED
END_IF
END_DEFINE
Ribbon_layout_localized 

Wui_start_layout
{ PGM: Do not translate: README}
DISPLAY_NO_WAIT (DGETTEXT "me10mac" "*** Attention !!!   Extra information is now available in the 'README' file")
{ REMOVE THIS AND ALL LINES BEFORE THIS ONE IF NO LONGER NEEDED }
LICENSE_SERVER (GETENV "LIZENZSERVER")
{INPUT 'passwords.m'}
LET Load_construction_icons 1
LOAD_MODULE 'VIEW'
LOAD_MODULE 'RC'
DDE_ENABLE ON
DEFINE_UDC_MAPPING 'CP932' 'EBA0' 'E200' 
DEFINE_UDC_MAPPING 'CP932' '8888' 'E201' 

LOAD_FONT 'osd_default.fnt'
LOAD_FONT 'osd_default2.fnt'
LOAD_FONT 'osd_default3.fnt'
LOAD_FONT 'hp_block_c.fnt'
LOAD_FONT 'hp_block_v.fnt'
LOAD_FONT 'hp_d17_c.fnt'
LOAD_FONT 'hp_d17_v.fnt'
LOAD_FONT 'hp_jasc_c.fnt'
LOAD_FONT 'hp_jasc_v.fnt'
LOAD_FONT 'hp_symbols.fnt'
LOAD_FONT 'hp_symbols2.fnt'
LOAD_FONT 'hp_Y14.5.fnt'
LOAD_FONT 'hp_i3098_c.fnt'
LOAD_FONT 'hp_i3098_v.fnt'
LOAD_FONT 'hp_jisj_c.fnt'
LOAD_FONT 'hp_jisj_v.fnt'
LOAD_FONT 'hp_kanji_c.fnt'
LOAD_FONT 'hp_kanj1_c.fnt'
LOAD_FONT 'hp_kanj2_c.fnt'
LOAD_FONT 'hp_kjis_c.fnt'
LOAD_FONT 'hp_kjis_v.fnt'

CURRENT_FONT 'osd_default'
CURRENT_DIM_TEXTS FONT 'osd_default'
LET Menu_slot_border_distance_y 0
LET Fill_up_menu 0
LET Menu_position LOWER
LOAD_MACRO 'hp_macro_v.mc'
Configure_system
INPUT 'hp_iconm.m'
LOAD_MACRO Sys_macro_filename      {screen only user interface}
LOAD_MACRO Sys_tmacro_filename     {tablet-screen user interface}
LOAD_MACRO Sys_menu_filename       {screen only user interface}
INPUT      Sys_tmenu_filename      {tablet-screen user interface}
INPUT 'defaults.m'
INPUT 'me10anno.m'
INPUT 'common_undelete.m'
Startup_menus
LOAD_MACRO Wui_v_filename   {View Window user interface}

Wui_v_set

{Use Creo colors if applicable}
DEFINE Fui_colors
  IF ((GET_CREO_ELEM_COLOR_MODE = 1) AND (GET_CREO_VP_COLOR_MODE = 1))
    INPUT 'fui.m'
  END_IF
END_DEFINE
Fui_colors

{ Creo colors for elements}
{These commands are common in Drafting and 2D Access}
DEFINE Fui_elem_colors_common
  IF (GET_CREO_ELEM_COLOR_MODE = 1)
    TEXT BLACK END
    CURRENT_HIGHLIGHT_ATTR RGB_COLOR 1.000 0.647 0.000 END
  END_IF
END_DEFINE

Fui_elem_colors_common

INPUT ((GETENV "MECORPCUSTOMIZEDIR") + "\customize.m")
INPUT ((GETENV "MESITECUSTOMIZEDIR") + "\customize.m")
 
FBROWSER SET_DIR (GETENV "MEPROJECTDIRHOME")

{INPUT 'customize'}
AUTO_NEW_SCREEN ON
