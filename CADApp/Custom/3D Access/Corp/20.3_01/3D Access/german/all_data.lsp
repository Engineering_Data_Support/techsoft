;; Automatically written on 19-Mar-2021 20:48:51
;; Module ALL
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "ALL")
(persistent-data
 :key "BUTTON-FACE-COLOR"
 :value 15790320)
(persistent-data
 :key "AM-BROWSER"
 :value '( :VISIBLE NIL :TABLE-SEC-ICONS NIL ) )
(persistent-data
 :key "IGNORE-CMD-ICONS"
 :value NIL)
(persistent-data
 :key "CLASH-ANALYSIS-CONFIGURATIONS-BROWSER"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "FILE-DIALOG"
 :value '( :VIEW-MODE 6 ) )
(persistent-data
 :key "STARTUPDIR"
 :value "C:\\Users\\CAD\\AppData\\Roaming\\PTC")
(persistent-data
 :key "GLOBAL-SKIN"
 :value :CREO)
(persistent-data
 :key "FILING-REVISION"
 :value :DEFAULT)
(persistent-data
 :key "DB-ATTRIBUTE-BROWSER"
 :value '( :TABLE-SEC-ICONS NIL :TREE-SEC-ICONS NIL :DISP-ROOT-NODE NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-SHADOW-MIRROR"
 :value '( :ENVIRONMENT-MAP 
           "C:/CADApp/PTC/Creo Elements/Direct 3D Access 20.3.2.0/personality/rendering/environments/studio1.jpg" ) )
(persistent-data
 :key "UIL-HISTORY"
 :value '( :ENTRIES NIL :MAXNUM 50 :STORE T ) )
(persistent-data
 :key "PARCEL-GBROWSER"
 :value '( :RESTORE-EXPAND-STATE NIL :FILTER-SELECT-IN-TREE NIL 
           :FILTER-VP-HIGHLIGHT NIL :SEARCH-VP-HIGHLIGHT NIL ) )
(persistent-data
 :key "UNITS"
 :value '( :MASS ( :G 1 ) :ANGLE ( :DEG 1 ) :LENGTH ( :MM 1 ) ) )
(persistent-data
 :key "PRESELECTION"
 :value T)
(persistent-data
 :key "DEFAULT-SETTINGS-STYLE-REFERENCE_COLUMN-TREE-DIALOG"
 :value '( :HEIGHT 214 :WIDTH 960 :COLUMN-WIDTHS ( 117 71 182 163 81 53 184 303 90 
           59 ) ) )
(persistent-data
 :key "CLASH-ANALYSIS-BROWSER"
 :value '( :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "RELATION-BROWSER"
 :value '( :VISIBLE NIL :TREE-SEC-ICONS NIL :TABLE-MODE T :GRAPH-MODE NIL ) )

;; ----------- end of file -----------
