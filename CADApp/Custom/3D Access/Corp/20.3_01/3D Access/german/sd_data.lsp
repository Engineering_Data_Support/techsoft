;; Automatically written on 19-Mar-2021 20:48:51
;; Module SOLIDDESIGNER
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "SOLIDDESIGNER")
(persistent-data
 :key "BROWSER-VIEW"
 :value '( :CURRENT-BROWSER-VIEW "DEFAULT" ) )
(persistent-data
 :key "SYSTEM-SETTING-WARNING-MODIFY"
 :value NIL)
(persistent-data
 :key "CALCULATOR"
 :value '( :RADIUS-MODE T :RPN-MODE NIL ) )
(persistent-data
 :key "BROWSER-QUERY"
 :value '( :CURRENT-BROWSER-SEARCH NIL :CURRENT-BROWSER-FILTER NIL ) )
(persistent-data
 :key "SYSTEM-SETTING-WARNING-DELETE"
 :value NIL)
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "SolidDesigner/ColorSchemes" :CREO ) ) )
(persistent-data
 :key "DEFAULT-VP-DUMP"
 :value '( :OUTPUT-FORMAT NIL ) )

;; ----------- end of file -----------
