﻿; ------------------------------------------------------------------------------
; SVN: $Id: sd_avail_cmds.cmd 38125 2014-07-03 10:03:33Z pjahn $
; commands for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(:Application "All")

;;----- All: Techosft ----------------------------------------------------------

(:Group "TECHSOFT" :title "TECHSOFT")

("Sheet Metal"
 :title       "Sheet Metal"
 :action      (LISP::PROGN
                (LISP::IF (OLI::SD-MODULE-ACTIVE-P "SHEETADVISOR")
                    (LISP::PROGN
                      (OLI::SD-SWITCH-APPLICATION "SheetAdvisor"))
                    (LISP::PROGN (STARTUP::ACTIVATE-SHEETADVISOR))))
 :description "Startet das bzw. wechselt zur Anwendung Sheet Metal"
 :image       "All/TECHSOFT/SheetMetal"
 :ui-behavior :DEFAULT)
 
("Model Manager"
 :title       "Model Manager"
 :action      (LISP::PROGN
				(LISP:IF (startup::license-free-module-active-p "ModelManager")
					(LISP::PROGN
						(MODELMANAGER::MM-SEND-CMD "SHOW-MANAGER"))
					  (LISP::PROGN (act_deact_module :act "ModelManager" "MODULE-CONTROLLER-Modules-ModelManager-TB" '(STARTUP::ACTIVATE-MODELMANAGER)))))
 :description "Öffnet das Arbeitsbereichsfenster von Model Manager"
 :image       "All/TECHSOFT/ModelManager"
 :sd-access   :yes
 :modeling-pe :no
 ) 
 
("TECHSOFT Filecenter"
 :title       "TECHSOFT Filecenter"
 :action      (oli::sd-display-url "https://support.techsoft.at")
 :description "TECHSOFT Filecenter - zum webbasierenden Datenaustauch mit TECHSOFT"
 :image       "All/TECHSOFT/filecenter"
 :sd-access   :yes)
 
("TECHSOFT Support-Formular"
 :title       "TECHSOFT Support-Formular"
 :action      (oli::sd-display-url "http://www.techsoft.at/support/service/support-formular/")
 :description "TECHSOFT Support-Formular - Eröffnen Sie eine Supportanfrage bei TECHSOFT via Web Browser"
 :image       "All/TECHSOFT/supportform"
 :sd-access   :yes)

("Netviewer starten"
 :title       "Netviewer starten"
 :action      (oli::sd-sys-background-job (format nil "\"~a\\\addons\\netviewer\\netviewer.exe\"" (oli::sd-sys-getenv "BASEDIR")))
 :description "Starten des Netviewer-Clients für die Verbindung zu einem/einer TECHSOFT-MitarbeiterIn."
 :image       "All/TECHSOFT/netviewer"
 :sd-access   :yes)

("Teamviewer starten"
 :title       "Teamviewer starten"
 :action      (oli::sd-sys-background-job (format nil "\"~a\\\addons\\teamviewer\\teamviewer.exe\"" (oli::sd-sys-getenv "BASEDIR")))
 :description "Starten des Teamviewer-Clients für die Verbindung zu einem/einer TECHSOFT-MitarbeiterIn."
 :image       "All/TECHSOFT/teamviewer"
 :sd-access   :yes)

("TECHSOFT Support per E-Mail"
 :title       "TECHSOFT Support per E-Mail"
 :action      (oli::sd-sys-background-job (format nil "explorer.exe \"mailto:support@techsoft.at?subject=Supportanfrage zu ~a\"" (oli::sd-sys-getenv "LINKNAME") (oli::sd-sys-getenv "TEMP")))
 :description "Erstellen Sie eine E-Mail an das TECHSOFT Support-Team"
 :image       "All/TECHSOFT/email"
 :sd-access   :yes)

("SDCORPCUSTOMIZEDIR anzeigen"
 :title       "SDCORPCUSTOMIZEDIR anzeigen"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")))
 :description "Anzeigen der Anpassungen für den Konzern"
 :image       "All/TECHSOFT/corp"
 :sd-access   :yes) 

("SDSITECUSTOMIZEDIR anzeigen"
 :title       "SDSITECUSTOMIZEDIR anzeigen"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDSITECUSTOMIZEDIR")))
 :description "Anzeigen der Anpassungen für diesen Standort"
 :image       "All/TECHSOFT/site"
 :sd-access   :yes)
 
("SDUSERCUSTOMIZEDIR anzeigen"
 :title       "SDUSERCUSTOMIZEDIR anzeigen"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDUSERCUSTOMIZEDIR")))
 :description "Anzeigen der Anpassungen für diesen Benutzer"
 :image       "All/TECHSOFT/user"
 :sd-access   :yes)
 
("Software Distribution Server"
 :title       "Software Distribution Server"
 :action      (oli::sd-display-url (format nil "http://~a:~a" (oli::sd-sys-getenv "SWDSERVERHOSTNAME") (oli::sd-sys-getenv "SWDSERVERPORT")))
 :description "Die Website des Software Distribution Server für Creo Elements/Direct Manager Server anzeigen"
 :image       "All/TECHSOFT/swdserver"
 :sd-access   :yes)

("License Server auflisten"
 :title       "License Server auflisten"
 :action      (frame2-ui::display (oli::sd-sys-getenv "LICENSESERVER"))
 :description "Gibt eine Liste der konfigurierten Lizenzserver aus"
 :image       "All/TECHSOFT/licenseserver"
 :sd-access   :yes)
 
("PTC Online Dokumentation"
 :title       "Online Dokumentation"
 :action      (oli::sd-display-url "http://www.ptc.com/cs/help/creo_elements_direct_hc/modeling_190_hc/")
 :description "PTC Online Dokumentation - Öffnet die Online Dokumentation zu Creo Elements/Direct Modeling von der PTC Website (Internet-Verbindung erforderlich)"
 :image       "All/TECHSOFT/ptc_online_documentation"
 :sd-access   :yes)
 
("PTC Online Tutorials"
 :title       "Online Tutorials"
 :action      (oli::sd-display-url "http://learningexchange.ptc.com/tutorials/listing/product_version_id:81")
 :description "PTC Online Tutorials - Öffnet eine Liste von Tutorials für Creo Elements/Direct Modeling von der PTC Website (Internet-Verbindung erforderlich)"
 :image       "All/TECHSOFT/ptcu"
 :sd-access   :yes)

("Black Design"
 :title       "Schwarz"
 :action      (uib::win-set-skin-style :black)
 :description "Benutzeroberfläche auf schwarzes Design umstellen"
 :image       "All/TECHSOFT/black_design"
 :sd-access   :yes)
 
("Silver Design"
 :title       "Silber"
 :action      (uib::win-set-skin-style :silver)
 :description "Benutzeroberfläche auf silbernes Design umstellen"
 :image       "All/TECHSOFT/silver_design"
 :sd-access   :yes)

("Blue Design"
 :title       "Blau"
 :action      (uib::win-set-skin-style :blue)
 :description "Benutzeroberfläche auf blaues Design umstellen"
 :image       "All/TECHSOFT/blue_design"
 :sd-access   :yes)

("Creo Design"
 :title       "Creo"
 :action      (uib::win-set-skin-style :creo)
 :description "Benutzeroberfläche auf Creo Design umstellen"
 :image       "All/TECHSOFT/creo_design"
 :sd-access   :yes)
 
;;----- SolidDesigner: Construction --------------------------------------------
 
(:Application "SolidDesigner")

(:Group "Construction" :title "Hilfsgeometrie")

("Querschnitt"
 :title       "Querschnitt als Hilfgeometrie projizieren"
 :action      "geometry_mode :construction cross_section :cross_section_wp"
 :description "Teile-Querschnitt als Hilfsgeometrie auf eine Arbeitsebene projizieren."
 :image       "SolidDesigner/Construction/Querschnitt"
 :ui-behavior :DEFAULT)
 
;;----- Annotation: TECHSOFT --------------------------------------------

(:Application "Annotation")

(:Group "TECHSOFT" :title "TECHSOFT")

("Annotation Farbschema wechseln"
 :title       "Annotation Farbschema wechseln"
 :action      "(ts-switch-background-color)"
 :description "Wechselt zwischen normalem Farbschema und S/W-Farbschema in Annotation."
 :image       "Annotation/TECHSOFT/Switch_Annotation_Color_Scheme"
 :ui-behavior :DEFAULT)
