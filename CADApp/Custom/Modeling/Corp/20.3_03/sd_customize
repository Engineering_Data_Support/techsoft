; ------------------------------------------------------------------------------
; SVN: $Id: sd_customize 38148 2014-07-03 14:56:27Z pjahn $
; customization file for creo elements/direct modeling 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

;;Enlarge the default LISP system to 90 MB=================
    ;;Advantage: Garbage Collector will get in action less often
    ;;and hence the LISP system performance will increase

    (progn (make-string 90000000) t) (gbc)
 
;;3d object settings=======================================

  ;;part appearance
    ;;part base color
    (set_default_part_color 10066329)
    ;;part edge color
    (set_default_part_edge_color 0)
    ;;part face color
    (set_default_face_part_color 4243417)
    ;;part face color
    (set_gfx_only_part_color 3712698)
    ;;lightweight edge color
	(set_partial_edge_color 65535)
	;;current part color
    (set_current_part_color 65280)
    ;;part reflectance color
    (set_default_part_reflectance_color 16777215)
    ;;part refelectance power
    (set_default_part_reflectance_power 0.15625)
    ;;part reflectance specularity
    (set_default_part_reflectance_specularity -1.0)
    ;;part default transparency
    (set_default_part_transparency :OPAQUE) ;semi, full
    
  ;;part misc
    ;;density
    (oli::sd-defdialog 'ts-set-default-density
      :dialog-title "SetDefaultPartDens"
      :toolbox-button nil
      :dialog-control :sequential
      :dialog-type :terminate
      :ok-action '(oli::sd-call-cmds (set_default_part_density 0.00786))
    )
    (ts-set-default-density)
    
    ;;graphical resolution
    (set_facet_resolution 65)
    ;;default mixed mode
    (set_default_mixed_display :shaded :on)
    (set_default_mixed_display :edged :on)
    (set_default_mixed_display :wire :off)
    (set_default_mixed_display :hidden :on)
    (set_default_mixed_display :hidden_dimmed :off)

  ;;load part
    ;;lightweight
     ;;no defined settings here. => persistent data
    
  ;;resolution
    ;;part
    (set_default_part_resolution 1e-6)
    ;;workplane
    (set_wp_default_resolution 1e-6)
    
  ;;workplane appearance
    ;;border
    (set_wp_default_border_type :outline) ;full
    ;;transparency
    (set_wp_fill_transparent :ON)
    ;;border font
    (set_wp_default_border_font "osd_default")
    ;;border color
    (set_wp_default_front_border_color 13421823)
    (set_wp_default_back_border_color 10066367)
    ;;active border color
    (set_wp_default_front_current_color 327560)
    (set_wp_default_back_current_color 48896)
    ;;geometry color
    (set_wp_default_geom_color 16777215)
    ;;construction lines color
    (set_wp_default_constr_color 16711935)
    ;;geometry line type
    (set_wp_default_geom_line_type :SOLID)
    (set_wp_default_constr_line_type :SOLID)
    ;;SPR 2159856 - Poor performance when loading geometry into a workplane or when removing the workplane in Creo Elements/direct modeling 19 M020
    ;;SPR 2159856 has been fixed in version 18.10 and version 19.00 F000; see https://www.ptc.com/appserver/cs/view/spr.jsp?n=2159856
    ;;do (not) show linetype and color in workplanes
    ;;(IMPORT-2D-GEO-ATTRIBUTES nil) ;t
    
  ;;workplane grid
    ;;mode
    (set_wp_default_grid_mode :OFF) ;DOTS, LINE
    ;;color
    (set_wp_default_grid_color 255)
    ;;spacing 
    (set_wp_default_grid_width 5)
    (set_wp_default_grid_height 5)
    
  ;;workplane misc
    ;;minimum size
    (set_wp_default_minimal_size 100)
    ;;new on face
    (elan-ui::new-wp-on-face-alignment-method :FIT) ;:DEFAULT

  ;;coordinate system
    ;;color
    (set_default_color_coordinate_system 16711680)
    ;;size
    (set_default_size_coordinate_system 20)
    ;;store visibility
    (set_store_visibility_flag_coordinate_system :off)
    ;;axis labels
    ;(SET_DEFAULT_U_AXIS_LABEL_COORDINATE_SYSTEM "u")
    ;(SET_DEFAULT_V_AXIS_LABEL_COORDINATE_SYSTEM "v")
    ;(SET_DEFAULT_W_AXIS_LABEL_COORDINATE_SYSTEM "w")

;;configuration settings===================================

  ;;smooth camera update
  (cfn_settings :SMOOTH_UPDATE :on)
  ;;smooth camera update time
  (cfn_settings :UPDATE_TIME 2.0)
  ;;positioning outside configuration owner
  (cfn_settings :POSITION_OUTSIDE_OWNER :off)
  ;;inform on old formations when loading
  (cfn_settings :WARN_ON_FORMATIONS :on)
  ;;colors for highlighting
  (cfn_settings :assy_color 65535)
  (cfn_settings :part_color 255)

;;settings for clash analysis==============================
 
  (clash_analysis_settings :clashing_color 16711680)
  (clash_analysis_settings :touching_color 16776960)
  (clash_analysis_settings :clearance_color 65535)
  (clash_analysis_settings :select_list_color 16776960)
  (clash_analysis_settings :exclude_list_color 16711935)
  
;;modeling settings========================================

  ;;general
    (modify_3d_default :auto_bodycheck) ;:no_auto_bodycheck
    ;(modify_3d_default :feat_select :FR-BOSS-POCKET) ;:FR-ANY-FEAT, :FR-RIB, :FR-SLOT, :FR-BOSS, :FR-POCKET, :FR-POCKET
    ;(modify_3d_default :no_keep_tangent) ; keep_tangent                                   
    ;(modify_3d_default :blend_auto) ;no_blend_auto
    ;(modify_3d_default :no_update_rels) ;update_rels
  
  ;;feedback
    ;(modify_3d_default :transparency) ;:no_transparency
    ;(modify_3d_default :show_source_geometry) ;:dont_show_source_geometry
    ;(modify_3d_default :realistic_feedback) ;:no_realistic_feedback
    ;(modify_3d_default :show_realistic_feedback_colors) ;:dont_show_realistic_feedback_colors

  ;;machine
    ;(machining_settings :no_keep_wp) ;:keep_wp
    ;(machining_settings :no_keep_profile) ;:keep_profile
    ;(machining_settings :no_keep_tool ;:keep_tool
    ;(machining_settings :no_keep_core) ;:keep_core

  ;;blend/chamfer
    ;(modify_3d_default :auto_chain) ;:no_auto_chain
    ;(modify_3d_default :auto_affected) ;:no_auto_affected
    ;(modify_3d_default :auto_labelfdbk) ;:no_auto_labelfdbk
    ;(modify_3d_default :auto_feedback) ;:no_auto_feedback 
    ;(modify_3d_default :auto_dragging) ;:no_auto_dragging

  ;;taper
    ;(modify_3d_default :absolute_taper_angle) ;:relative_taper_angle
    (modify_3d_default :keep_as_taper_feature) ;:no_keep_as_taper_feature
  
  ;;offset
    ;(modify_3d_default :offset_selected_blends) ;:no_offset_selected_blends
  
  ;;curve/surfacing
    ;(curve_settings :vpx :on)
    ;(curve_settings :vpy :on)
    ;(curve_settings :vpz :on)
    ;(curve_settings :standard_vp :on)
    ;(curve_settings :coord_system :on)
    ;(curve_settings :coord_system_follow :on)
    ;(curve_settings :curvature_feedback :off)

;;anno 3d settings=========================================

 ;;no defined settings here. use recorder.
  
;;system settings==========================================

 ;;units
   (units 1 :MM)
   (units 1 :DEG)
   (units 1 :G)

 ;;save
   ;;on exit (save persistent data)
     ;(save_persistent_settings_on_exit :on)
 
   ;;auto save
     (setf *ts-autosave-path* (format nil "~a/modeling_autosave_~a" (oli::sd-sys-getenv "TEMP") (oli::sd-sys-getenv "USERNAME")))
     (when (and (oli::sd-inq-file-status *ts-autosave-path* :existence)
                (oli::sd-inq-file-status *ts-autosave-path* :write-access)
                (not (oli::sd-directory-p *ts-autosave-path*))
            )
        (delete-file *ts-autosave-path*)
     )
                  
     (auto_save_settings
      :save_on :on
      :TIME 30
      :steps :STEPS 20
      :time :TIME 30
      :OUT_OF_MEM :OFF
      :FILE *ts-autosave-path*
      :BACKUP_COUNT 10
      :CONFIRM :OFF
      :OK)   

   ;;default file version
     ;(backward_file_version :MR-18-RELEASE)
     ;(backward_file_version :MR-18-10-RELEASE)

   ;;measure
     ;;no defined settings here. => persistent data
     
   ;;name suffix
     ;;no defined settings here. => persistent data
     
   ;;notifications
     ;;on model load
     (set-load-of-container-notification-level :ONCE) ;:ALL, :NONE
     ;(set-load-of-relation-set-notification-level :ALL) ;:NONE
     ;;relation warnings
     (warn_on_modify :on)
     (warn_on_delete :on)
     ;;face set feature warnings
     ;(face_set_feature_notifications :warn_on_invalidation :off)
     ;(face_set_feature_notifications :warn_on_revalidation :off)
     ;(face_set_feature_notifications :warn_on_deletion :off)

   ;;model advisor warnings
     ;;message generation
     (set_model_advisor :on)
     ;;check
     (set_model_adv_edge_length :value 0.01)
     (set_model_adv_max_number_control_points :value 20000)
     (set_model_adv_void_shells :on)
     (set_model_adv_nonmanifolds :on)
     (set_model_adv_knife_edges :on)
     ;;add check for axis deviations (undocumented)
     (k2::CHECK-AXIS-DEVIATION-ENABLE)
     ;;disable/enable check for axis deviations (undocumented)
     (set_model_adv_inexact_geo :off) ;;:on

   ;;startup
     ;;no defined settings here. => persistent data
     
   ;;calculator
     ;;no defined settings here. => persistent data     

;;initial system settings==================================

 ;;...are defined after first startup by the individual user

;;user interface settings==================================

 ;;task bar
  ;(ui_settings :MENU_HOVER :off)
  ;(ui_settings :ANIM_WINFRACTION 45)
  ;(ui_settings :SHOWICONS :ON)

 ;;commands
  ;(ui_settings :USE-VP-BUTTONS :ON)
  ;(ui_settings :OPTION-DIALOG-POS :LEFT) ;:RIGHT, :RELATIVE
  ;(ui_settings :showlastusedcmdtoolbar :on)
  ;(ui_settings :UIS_LAST_USED_CMD_NO 6)
  ;(ui_settings :resetwithnewsession :off)

 ;;controls
  ;(ui_settings :TRIGGER_RELEASE) ;:TRIGGER_PUSH
  ;(ui_settings :AUTO_ACCEPT :ON)
  ;(ui_settings :LAST_VALUES :ON)
  ;(ui_settings :COMPLETE_FOCUS :ON)

 ;;user input line
  ;(ui_settings :AUTO_COMPLETE :ON)
  ;(ui_settings :KEYBOARD_FORWARD :ON)
  ;(ui_settings :UIL_HIST_NO 50)
  ;(ui_settings :UIL_HIST_TO_REGISTRY :ON)

 ;;fast access
  ;;preselect
  ;(ui_settings :preselection_handles :on)
  ;(ui_settings :2d_preselection_handles :on
  ;(ui_settings :enable_cmd_mtbar :on)
  ;;command options
  ;(ui_settings :enable_option_mtbar :on)
  ;(ui_settings :enable_option_mtbar_extended_indicator :on)
  
 ;;misc
  ;;recently used files
  (ui_settings :NUM_OF_MRU 16)
  ;;alert history
  ;;(ui_settings :alert_history :on)
  (ui_settings :NUM_OF_ALERT 50)
  
 ;;show/hide command dialogs
  ;(setq ui::*ui-on-request-enabled* nil) ;t
  
 ;;set skin style
  ;(uib::win-set-skin-style :creo) ; :black, :blue, :silver
  
;;2d-copilot settings======================================

  ;;not all settings here => persistent data

 ;;2d-snapping
  (progn (2d::2d-copilot :set-switch :snap-to-same-xy t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-projectors t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-extensions t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-distant t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-perp-distant t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-midpoints t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-intersections t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-extremes t) (values))
  (progn (2d::2d-copilot :set-switch :snap-to-corner-lines t) (values))
  
 ;;3d-snapping
 (progn (2d::2d-copilot :set-switch :snap-3d-active-part-only nil) (values))
 (progn (2d::2d-copilot :set-switch :snap-3d-active-wp-only nil) (values))

;;3d-copilot settings======================================

 ;;switch back to catch behaviour without pressing shift button
 (progn (nx::dragger-switch-modify :fly_by_snapping t) (values)) 

 ;;show minus for negative direction
 (dragger-switch-modify :drag_show_signed_values nil) ;;t

 ;;not all setting defined here. => persistent data
 
;;viewport settings========================================

 ;;dynamic viewing
   ;;3dobject
   (set_dynamic_redraw_mode :default) ;:shaded, :edged, :wire
   ;;enhanced realism
   (set_dynamic_redraw_mode :ALL) ;:MIRROR, :SHADOW, :NOTHING
   ;;clipping
   (set_dynamic_redraw_mode :clip-no-lines) ;:clip-default, :clip-no-caps, :clip-no-lines-and-caps
   ;;workplane
   (set_dynamic_redraw_mode :wp-default) ;:wp-edge-color, :wp-edges, :wp-border, :wp-nothing
   ;;label
   (set_dynamic_redraw_mode :l3d-default) ;:l3d-no-text, :l3d-nothing
   ;;cae
   (set_dynamic_redraw_mode :cae-default) ;:cae-no-mesh, :cae-nothing
   ;;simplify model
   (set_dynamic_level_of_detail_mode :boxed) ;:full
   (set_dynamic_level_of_detail_factor 70)
   ;;timeout for redraw (ms)
   (set_redraw_timeout 0) ;;no timeout
   ;;ctrl-less treshold (pixel)
   (set-ctrl-less-viewing-threshold 20)
  
  ;;redraw
   ;;simplify model
   (set_level_of_detail_mode :boxed)
   (set_level_of_detail_factor 40) ;:full
   ;;timeout for redraw
   (set_static_redraw_timeout 2000) ;0
   ;;smooth camera update
   (set_smooth_camera_update_mode :on)
   (set_smooth_camera_update_interpolation_time 500)
   ;;phong shading
   (set_phong_shading_mode :off)
   ;;occlusion culling
   (set_occlusion_culling_mode :off)
   ;;display lists
   (set_displaylist_mode :off)
   
  ;;enhanced realism
   ;(set-shadow-enabled t) ;nil
   ;(set-mirror-enabled t) ;nil
   ;(set-shadow-on-floor-enabled t) ;nil
   ;(set-shadow-mirror-floor-type :GRID) ;:CHECKBOARD, :DROP-SHADOW
   ;(set-part-face-mirror-enabled t) ;nil
   ;(set-global-default-mirror-power 0.3)
   
  ;;floor position
   ;;no defined settings here

 ;;2d
  ;(set-2d-geometry-line-width 2)
  (set-2d-construction-line-width 1)
  ;(set_geo2d_accuracy_factor 30)
  ;(set_inactive_wp_dim_factor 0.7)
  
 ;;fly-by
  ;;highlight
   ;(enable_fly_by :general :on)
   (set_fly_by_delay :general 20) ;(ms)
   ;;3d fly-by-color
   ;(set_fly_by_color :color-3d 16762986)
   ;;2d fly-by-color
   ;(set_fly_by_color :color-2d 16762986)
   ;;lightweight fly-by-color
   ;(set_fly_by_color :color-gfx 16547841)

  ;;tooltip
   ;;shown, after [ms]
   (set_fly_by_delay :show-path 500) ;(ms)
   ;;relative font size for fly-by labels
   ;(set-fly-by-tooltip-relative-font-size 1.0)

 ;;selection
  ;;enable preselection mode
  (ui_settings :preselection :on)
  ;;thicken edges
  ;(set-thicken-highlighted-edges t)
  ;;width for highlighted edges
  ;(set-highlighted-edges-width 2)
  ;;edge width for highlighted parts
  ;(set-highlighted-part-edges-width 1.5)
  ;;selection color
  (set_select_color :modify_rgb_color 15373056 :done)

 ;;view by
  ;;face - up direction
  (set-view-by-face-up-dir-mode :BEST) ;:MIN-DEFAULT , :DEFAULT, :NO
  ;;face - center face pick point
  (set-view-by-face-center-face t)

  ;;workplane - pan/zoom
  (set-view-by-wp-pan-zoom-mode :CENTER-CENTER) ;:NONE, :CENTER-ORIGIN, :FIT-BORDER

 ;;show
  ;;3d geometry
  (set-default-show-setting :3dgeo t)
  (set-default-show-setting :3dgeo_shaded t)
  (set-default-show-setting :3dgeo_edged t)
  (set-default-show-setting :3dgeo_hidden t)
  (set-default-show-setting :3dgeo_wire nil)
  (set-default-show-setting :3dgeo_mixed nil)
  (set-default-show-setting :3dgeo_hidden_dimmed nil)
  (set-default-show-setting :3dgeo_vertices nil)
  ;;(set-default-show-setting :3dgeo_part_transparency t)
  ;;(set-default-show-setting :3dgeo_face_transparency t)
  ;;(set-default-show-setting :3dgeo_face_color t)
  ;;(set-default-show-setting :3dgeo_part_reflectance t)
  ;;(set-default-show-setting :3dgeo_face_reflectance t)
  ;;(set-default-show-setting :3dgeo_edge_color t)
  (set-default-show-setting :3dgeo_label t)
  (set-default-show-setting :docu_planes t)
  (set-default-show-setting :coord_system t)
  (set-default-show-setting :feature_pts nil)
  (set-default-show-setting :clip_planes t)
  (set-default-show-setting :clip_lines t)
  (set-default-show-setting :clip_hatches nil)

  ;;workplanes
  (set-default-show-setting :workplanes t)
  (set-default-show-setting :workplanes_border t)
  (set-default-show-setting :workplanes_dim_inactive t)
  (set-default-show-setting :workplane_sets_match_lines t)
  (set-default-show-setting :workplanes_local_axis nil)
  (set-default-show-setting :workplanes_label t)
  (set-default-show-setting :workplanes_2dgeo t)
  (set-default-show-setting :workplanes_2dconstruction t)
  (set-default-show-setting :workplanes_2dgeo_vertices nil)
  (set-default-show-setting :workplanes_hidden t)
  (set-default-show-setting :workplanes_2dgeo_label t)

 ;;background
  ;;(default_vp_settings :color1 3758447)
  ;;(default_vp_settings :color2 6522268)
  ;;(default_vp_settings :color3 13294049)

 ;;clipping
  ;;(set_clip_line_appear :capcolor 65280)
  (set_clip_line_appear :clashcolor 16711680)
  (set_clip_line_appear :linecolor 0)
  ;;(set_clip_line_appear :linewidth 1.5)
  (set_clip_line_appear :hatchcolor 0)

 ;;contour map
  ;(settings_contour_map :position :LEFT)
  ;(settings_contour_map :transp :TRANSPARENT)
  ;(settings_contour_map :transp :NOT_TRANSP)
  ;(settings_contour_map :frame :OFF)
  ;(settings_contour_map :colortext 65535)
  
;;configure enhanced realism===============================

 ;;disable enhanced realism
 (elan-ui::set-vp-scenery-enabled nil t)
 ;;only disable shadows
 ;(set-shadow-enabled nil) ;t

;;license hold time========================================

 ;;activate
 ;(oli::sd-set-idle-behavior :on)
 ;;timeout
 ;(oli::sd-set-hold-time 60) ;(minutes)

;;exception handling=======================================

 ;;(frame2::enable-exception-report :internal) 
 ;;(frame2::keep-all-exception-reports t) 

;;prevent users to define their own startup modules========

 ;;(oli::sd-disable-button "MODULE-CONTROLLER-STARTUP-TB") ;; disables the Startup-Button
 ;;(oli::sd-hide-control "MODULE-CONTROLLER-STARTUP-TB") ;; hides the Startup-Button

;;alternate memory handling when minimized================

 (f2::iconify-without-trimming t)  ;; nil
 ;;please read documentation on this goodie!

;;set DXFDIR and DWGDIR====================================

 (frame2::putenv "DXFDIR" (format nil "~a\\addons\\dxfdwg" (oli::sd-sys-getenv "BASEDIR")))
 (frame2::putenv "DWGDIR" (format nil "~a\\addons\\dxfdwg" (oli::sd-sys-getenv "BASEDIR"))) 

;;set startup directory====================================

 ;;(ignore-previous-startup-directory)
 ;cd "c:/users"

 ;;some more examples
 ;cd "//server/share/projectdir" ;; UNC
 ;cd (format nil "x:/users/~a" (oli::sd-sys-getenv "USERNAME"))
 ;cd (format nil "x:/projects/~a" (oli::sd-sys-getenv "DATE"))

;;settings for file dialogs (load/save)====================

 ;;... in Modeling
 ;;(sd-set-initial-file-type
      ;; :application application         ;;default="SolidDesigner"
    ;; :load-file-type file-type-key
    ;; :save-file-type file-type-key)

 ;;... in Annotation
 (sd-set-initial-file-type :application "Annotation"
    :load-file-type :drawing
    :save-file-type :drawing)

    ;;use most recently used file type ...
    (sd-allow-last-used-file-type t)

    ;;... or ignore it
    ;; (sd-allow-last-used-file-type nil)

 ;;standard view in file dialog
 (sd-set-file-browser-default-view-mode :details) ;;detail view
 ;;(sd-set-file-browser-default-view-mode :list)  ;;list view

;;layout to drafting=======================================

 ;;enable this for the old layout option
 ;(show-layout)

;;structure browser appearence=============================

 ;;highlighting of active objects
 (set_structure_browser_appearance :filled)
 ;;(set_structure_browser_appearance :underlined)

 ;;color of active objects
 (gbrowser_active_object_color :modify_rgb_color 65280 :done)
 
 ;;disable tooltips
 ;;(oli::sd-browser-exec-cmd "parcel-gbrowser" :ENABLE-WIDGET-TIPS nil) 

;;undo settings============================================

 ;;how many steps to keep
 (undo :keep_steps 30)

;;copy option one level====================================

 (copy-default :onelevel)
 ;;(copy-default :deep)

;;unshare option one level==================================

 (unshare-default :onelevel)
 ;;(unshare-default :deep)

;;show diameter or radius in fly by information============

 (elan::prim-show-diameter-in-tooltip t) ;; diameter
 ;;(elan::prim-show-diameter-in-tooltip nil) ;; radius
 
;;switch between new (> v. 16.00) or old zoom behavior=====

 (frame2::SET-ZOOM-AROUND-VIEW-CENTER nil) ;; new default behavior since version 16.00
 ;;(frame2::SET-ZOOM-AROUND-VIEW-CENTER t) ;; old behavior
 
;;switch back to cocreate mouse interaction mode===========
 (set-mouse-interaction-enabled :on)

;;suppress notification for file size reduction============

 ;;(ELAN::SUPPRESS-CLEANUP-THREAD-MESSAGE)

;;goodies==================================================

 ;;recorder
 (load "recorder")
 ;;unrecognize blends
 (load "bc_unrec")
 ;;unlock objects
 (load "dbdialog")
 ;;check untouchables
 (load "chk_untouchable")
 ;;enhanced surface modification: multiple node
 (load "sf_mod_ext")
 ;;check open references
 (load "chk_open_ref")
 ;;removing read-only and untouchable locks and the
 ;;partial load status of parts/assemblies
 (load "unlock_obj")
 ;;displaying defined attributes of a part/assembly
 (load "inq_all_atts")
 ;;this tool allows you to create face rounds between selected edges and faces
 (load "edgeFaceRoundDia")
 ;;this tool allows to unhide questions hidden by the user
 (load "sd_question_show_again") 
 
;;workaround for SPR 1979431 - Color setting for auxiliary line changes also background color of distance value readout 
 
 (oli::sd-set-persistent-data "SolidDesigner" "2D-CoPilot-Settings" 0 :subkey :cursor-text-foreground-color) 

;;workaround for SPR 1662576 - SDSA - add punch/stamp tools don't use CORP/SITE/USER to load non parametric punch/stamp tools

 (SEARCH_DIRECTORIES :append (format nil "~a/german/SheetAdvisor/stamp_tools" (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")))
 (SEARCH_DIRECTORIES :append (format nil "~a/german/SheetAdvisor/punch_tools" (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR"))) 
 (SEARCH_DIRECTORIES :append (format nil "~a/german/SheetAdvisor/stamp_tools" (oli::sd-sys-getenv "SDSITECUSTOMIZEDIR")))
 (SEARCH_DIRECTORIES :append (format nil "~a/german/SheetAdvisor/punch_tools" (oli::sd-sys-getenv "SDSITECUSTOMIZEDIR")))
 (SEARCH_DIRECTORIES :append (format nil "~a/german/SheetAdvisor/stamp_tools" (oli::sd-sys-getenv "SDUSERCUSTOMIZEDIR")))
 (SEARCH_DIRECTORIES :append (format nil "~a/german/SheetAdvisor/punch_tools" (oli::sd-sys-getenv "SDUSERCUSTOMIZEDIR")))  

;;enable the last used command bar
 
 (frame2::ui_settings :showlastusedcmdtoolbar :on)

;;integrate Klietsch SolidTools============================

 ;;(load "C:/program files/klietsch/osd18_nt/klmodule.txt")
 ;;http://www.klietsch.com
 
;===========================================================================
; Beginning SolidPower
;===========================================================================
(if (probe-file (format NIL "~a/sd_4/tspro_loads.lsp" (oli::sd-sys-getenv "TSPRODIR") ))
 (progn
	  (load (format nil "~a/sd_4/tspro_loads.lsp" (oli::sd-sys-getenv "TSPRODIR")))
	;;(load (format nil "~a/sd_4/tspro_am_loads.lsp" (oli::sd-sys-getenv "TSPRODIR")))
  )
)  	

 (use-package :custom)
 