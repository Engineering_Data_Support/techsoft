;% Automatically written on 09/29/2008 at 08:50:55
;% CoCreate Modeling Revision: 2008 (16.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation"
  :style :TECHSOFT
  :title "Techsoft"
  :values '("Dimension/Text/Common/Gap" 2.0
            "View/Detail/Appearance/ViewLabel/Case" :UPPER
            "View/Detail/Appearance/ViewLabel/Numbering" :NUMBERSONLY
            "View/Detail/Appearance/ViewLabel/CaptionStyle" :DETAIL_CONV1600_TECHSOFT
            "View/Standard/Appearance/ViewLabel/Case" :ASIS
            "View/Section/Appearance/Line/Leader/Mode" :LEADER_TO
            "View/Section/Other/ModeDependantViews" :YES
            "View/Section/TempAuxLines/Color" 16711680
            "View/Section/Appearance/Line/Leader/ArrowStyle" :|PFEIL_GROß|
            "Dimension/Specific/Coordinate/Standard" :DIN
            "Dimension/Specific/Diameter/Standard" :DIN
            "General/ArrowStyle/Type" :ARROW_TYPE
            "Symbol/Appearance" :STANDARD
            "View/Broken/Appearance/Pattern/Width" 5.0
            "View/Broken/Appearance/Pattern/Height" 5.0
            "View/Detail/Appearance/ViewLabel/Offset" 5.0
            "Symbol/Specific/Welding/Sizes/AllaroundCircle" 1.25
            "View/Section/Appearance/SectionLabel/Offset" 7.0
            "General/TextStyle/Size" 3.5
            "View/Detail/Appearance/DetailLabel/Case" :ASIS
            "Dimension/Text/SecondTolerance/TextStyle" :SMALL
            "View/Cutaway/Appearance/ViewLabel/Case" :ASIS
            "Dimension/AutoPlace/DatumStep" 8.0
            "Dimension/Text/Common/Location" :ABOVE
            "Dimension/Text/Common/UnderlineEdited" LISP::T
            "Dimension/Text/MainValue/LengthUnitFormat" :MM
            "Hatch/Other/DistanceFactor" 1.0001
            "View/FormatStyle/InternalFormatBrowser" "default Format browser"
            "Dimension/Specific/Chamfer/Prefix" ""
            "View/Section/Appearance/ViewLabel/Offset" 5.0
            "View/Standard/Appearance/ViewLabel/Offset" 5.0
            "View/Partial/Appearance/ViewLabel/CaptionStyle" :PARTIAL_CONV1600_TECHSOFT
            "Dimension/Specific/Thread/Postfix" ""
			"Dimension/Specific/Thread/LabelLeftHand" "links"						
            "Dimension/Specific/Thread/LabelRightHand" ""			
            "Dimension/Specific/Diameter/BothDiaArrows" LISP::NIL
            "View/Section/TempAuxLines/Mode" :OFF
            "View/Section/Other/AddSymCenLines" :ON
            "Dimension/Lines/OffsetPoint" 0.0
            "View/Section/Appearance/Line/Leader/Length" 15.0
            "Dimension/Specific/Diameter/Prefix" " <Diameter>"
            "Dimension/Text/MainTolerance/TextStyle" :SMALL
            "Dimension/Specific/Radius/CenterLineVisibility" LISP::NIL
            "View/Broken/Appearance/Pattern/Gap" 15.0
            "View/Partial/Appearance/ViewLabel/Numbering" :NUMBERSONLY
            "Symbol/Appearance/AutoAngle" LISP::T
            "View/Section/Appearance/Line/Segment/LineStyle" :SECTION_SEGM
            "View/Appearance/ThreadLines/ArcBegin" 3.2288591161895095
            "Dimension/Text/Common/Orientation" :PARALLEL
            "View/Standard/Appearance/ViewLabel/Numbering" :NUMBERSONLY
            "View/Cutaway/Other/ModeDependantViews" :NO
            "View/Section/Appearance/Line/Segment/Mode" :SEGM_OVERLAP
            "Bom/ScanContainer" :OFF
            "Dimension/Text/Common/Space" 2.0
            "View/Detail/Appearance/DetailLabel/Offset" 5.0
            "Dimension/Lines/OffsetLine" 2.0
            "View/Detail/Other/AddSymCenLines" :ON
            "Drawing/ProjectionMode" :FIRST_ANGLE
            "View/Appearance/ThreadLines/ArcEnd" 1.6580627893946129
            "Dimension/Specific/Chamfer/Postfix" "x45<Degree>"
            "View/Cutaway/Appearance/ViewLabel/CaptionStyle" :CUTAWAY_CONV1600_TECHSOFT
            "General/TextStyle/Slant" 0
            "View/Section/Appearance/Line/Leader/LineStyle" :SECTION_SEGM
            "View/Section/Appearance/ViewLabel/Case" :ASIS
            "General/LineStyle/Pensize" 0.34999999999999998
            "View/Section/Appearance/SectionLabel/Position" :TOP_SIDE
            "View/Cutaway/Appearance/ViewLabel/Numbering" :NUMBERSONLY
            "Dimension/AutoPlace/MinimalSpace" 0.0
            "View/Detail/Appearance/ViewLabel/LabelStyle" :MEDIUM
            "View/Section/Appearance/SectionLabel/LabelStyle" :LARGE
            "View/Section/Appearance/Line/Geo" :SECTION_GEO
            "Symbol/Appearance/TextReadable" LISP::T
            "View/Section/Appearance/Line/Segment/Length" 5.0
            "View/FormatStyle/InternalFormatLabel" "default Format label"
            "View/FormatStyle/FormatBrowser" "default Format browser"
            "View/FormatStyle/FormatLabel" "default Format label"
			"Viewport/Appearance/BackgroundColor" 1644895
			"View/Other/CopiedPartsComponentBehaviour" :ALWAYS
            "General/ArrowStyle/Size" 3.5
            "View/Section/Appearance/ViewLabel/CaptionStyle" :SECTION_CONV1600_TECHSOFT
            "View/Standard/Appearance/ViewScale/Case" :ASIS
            "View/Partial/Appearance/ViewLabel/Case" :ASIS
            "View/Update/UpdateMode/FacePartsInView" :CALCULATE
            "Hatch/Appearance" :TECHSOFT
            )
)
