;% Automatically written on 02/22/2021 at 16:27:45
;% Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Dimension"
  :style :NH
  :title "nh"
  :values
   '("AutoPlace/DatumStep" 7.0
     "AutoPlace/MinimalSpace" 10.0
     "CatchCheck/CatchRange" 4.0
     "Lines/OffsetPoint" 2.0
     "Specific/Diameter/BothDiaArrows" LISP::T
     "Specific/Diameter/Prefix" "<Diameter>"
     "Specific/Radius/Prefix" "<BASIC>R"
     "Specific/Thread/LabelLeftHand" "links"
     "Specific/Thread/LabelRightHand" ""
     "Text/Common/Location" :ABOVE
     "Text/Common/Space" 1.0
     "Text/MainTolerance/SizeMode" :REL06
     "Text/MainTolerance/TextStyle" :TOLERANZ
     "Text/Postfix/TextStyle" :TOLERANZ
     "Text/Prefix/TextStyle" :TOLERANZ
     "Text/SecondTolerance/TextStyle" :TOLERANZ
     "Text/Superfix/TextStyle" :STANDARD
     )
)
