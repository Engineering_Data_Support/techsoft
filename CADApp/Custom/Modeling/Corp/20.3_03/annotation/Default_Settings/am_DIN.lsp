;% Automatically written on 09/29/2008 at 08:50:55
;% CoCreate Modeling Revision: 2008 (16.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation"
  :style :DIN
  :title "DIN"
  :values '("View/FormatStyle/FormatBrowser" "Internal Format browser"
            "View/FormatStyle/FormatLabel" "Internal Format label"
            "Symbol/Appearance" :STANDARD
            "Hatch/Appearance" :STANDARD
            )
)
