;% Automatically written on 03/21/2007 at 15:27:08
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :GUSSEISEN
  :title "Gusseisen"
  :values '("Description" "L:4, A: 45.0, D:   6.0"
            "PatternDistance" 6.0
            "SubPattern" (:LABEL "Gusseisen" :SUBPATTERN
                                 ((:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET 0 :ANGLE
                                          0.0 :WIDTH 0)
                                  (:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET
                                          0.17000000000000001 :ANGLE
                                          0.0 :WIDTH 0)
                                  (:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET
                                          0.34000000000000002 :ANGLE
                                          0.0 :WIDTH 0)
                                  (:COLOR 1.0,1.0,0.0 :LINETYPE
                                          :LONG_DASHED :DISTANCE 1
                                          :OFFSET 0.67000000000000004
                                          :ANGLE 0.0 :WIDTH 0)))
            "PatternAngle" 0.78539816339744828
            )
)
