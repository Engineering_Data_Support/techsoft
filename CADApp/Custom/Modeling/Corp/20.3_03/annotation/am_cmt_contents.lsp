;; CoCreate Modeling Revision 17.00
;;
;; NOTE: This file defines the contents of the Command Mini Toolbar which
;; appears after a selection during PRESELECT in Annotation.
;; The commands referenced by sd-define-application-cmt-default-contents are
;; available commands identified by "Application" - "Group" - "Command" 
;;
;; In the Annotation application the following scenarios (identified by
;; specific keywords) are supported:
;;
;;    :am_view      	One or more modifiable views
;;    :am_dimension 	One or more modifiable dimensions are selected
;;    :am_text      	One or more modifiable texts
;;    :am_symbol    	One or more modifiable symbols
;;    :am_refline   	One or more modifiable reference / leader lines
;;    :am_sketch    	One or more modifiable sketches
;;    :am_picture   	One or more modifiable pictures
;;    :am_ole       	One or more modifiable OLE
;;    :am_geometry  	One or more modifiable geometry
;;    :am_hatch     	One or more modifiable hatches
;;    :am_auxgeo    	One or more modfiable auxiliary geometry
;;	  :am_section_line 	One or more modfiable section line
;;    :am_detail_border	One or more modfiable detail border
;;    :am_cutaway_borderOne or more modfiable cutaway border

(oli:sd-define-application-cmt-default-contents
 "Annotation"
 :contents 
 '(:am_view
   (
    ("Annotation" "View 2D" "Update View")
    ("Annotation" "View 2D" "Create Detail View")
    ("Annotation" "View 2D" "Create Section View")
    ("Annotation" "View 2D" "Cutaway")
    ("Annotation" "Viewport" "Aux 3D Selected View Dir")
    ("Annotation" "View 2D" "View Properties")
    )
  
   :am_dimension
   (
    ("Annotation" "Modify Dimension" "Stagger")
    ("Annotation" "Modify Dimension" "Break")
    ("Annotation" "Modify Dimension" "Slant")
    ("Annotation" "Miscellaneous" "Highlight Owner")
    ("Annotation" "Modify Dimension" "Dim Properties")
    )
   :am_text
   (
    ("Annotation" "Text" "Text Edit")
    ("Annotation" "Text" "Copy Text")
    ("Annotation" "Text" "Create Ref Line")
    ("Annotation" "Miscellaneous" "Highlight Owner")
    ("Annotation" "Text" "Modify Text Settings")
    )
   :am_symbol
   (
    ("Annotation" "Symbol" "Edit Symbol")
    ("Annotation" "Symbol" "Copy Symbol")
    ("Annotation" "Text" "Create Ref Line")
    ("Annotation" "Miscellaneous" "Highlight Owner")
    ("Annotation" "Symbol" "Symbol Modify")
    )
   :am_refline
   (
    ("Annotation" "Text" "Reassign Ref Line")
    ("Annotation" "Text" "Modify Ref Line")
    )
   :am_sketch
   (
    ("Annotation" "Sketch" "Sketch Properties")
    )
   :am_picture
   (
    ("Annotation" "Picture" "Picture Edit")
    ("Annotation" "Picture" "Picture Properties")
    )
   :am_ole
   (
    ("Annotation" "OLE" "OLE Edit")
    ("Annotation" "OLE" "OLE Properties")
    )
   :am_geometry
   (
    ("Annotation" "Geometry" "Geo Properties")
    ("Annotation" "Miscellaneous" "Highlight Owner")
    )
   :am_hatch
   (
    ("Annotation" "Hatch" "Modify Hatch Settings")
    )
   :am_auxgeo
   (
    ("Annotation" "Aux Geometry" "Modify Aux Geometry Settings")
    )
   :am_section_line
   (
     ("Annotation" "View 2D" "Section Line Modify")
     ("Annotation" "View 2D" "Section Arrows")
    )
   :am_detail_border
   (
    ("Annotation" "View 2D" "Border Modify")
    )
   :am_cutaway_border
   (
    ("Annotation" "View 2D" "Border Modify")
    )
   )
 )
