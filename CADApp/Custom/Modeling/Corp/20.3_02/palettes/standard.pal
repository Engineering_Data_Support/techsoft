﻿; ------------------------------------------------------------------------------
; SVN: $Id: standard.pal 38125 2014-07-03 10:03:33Z pjahn $
; default color palette for creo elements/direct modeling
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

; -*-Lisp-*- (sort of)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Description:  Standard Colors Palette
;
; (C) Copyright 2011 Parametric Technology GmbH, all rights reserved.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; This file contains the definition of the standard colors palette.
; The format of this definition is as follows:
;
;    (
;      :Palette-Name  "Name of Palette"
;      :Format        Keyword: one of :rgb, :hsl or :motif
;      :Colors        List of color specifications
;    )
;
;    where 'List of color specification' is:
;
;      in case of ':Format :rgb':
;       (  (1,0,0 "Red")  (0,1,0 "Green") ... more colors  )
;
;      in case of ':Format :hsl':
;       (  (1,1,1 "Red")  (0.3333333,1,1 "Green")   ... more colors  )
;
;      in case of ':Format :motif':
;       (  ("#ff0000" "Red")  ("#00ff00" "Green")  ... more colors  )
;
;
; To create your own color palette file, copy this file to the 'palettes'
; directory of your personal customization directory and rename it:
;   e.g.  cp standard.pal $HOME/sd_customize/palettes/my_colors.pal
; Edit the file and make your adjustments.
;
; Note: You have to specify the color values in the format you've 
;       specified, i.e. either as :rgb, :hsl or as :motif values.
;       You can't mix these formats within one palette specification. 
;
;       One palette file can contain only one palette specification.
;       Create a new XXX.pal file for every palette.
;
;
; The better way to create (and modify) your own palette(s) is to use
; the user interface provided with the Color Selector. Here you can
; create new palettes and fill them with user defined colors or modify
; existing palettes and individual colors interactively.
; The created/modified palettes will be saved automatically for you.
;
;
;------------------------------------------------------------------------------
;
; Specification of the Standard Colors Palette:

(
  :Palette-Name     "Standard"
  :Format           :rgb
  :Colors
  (
   (0,0,0 "Schwarz")
   (1,1,1 "Weiß")
   (1,0,0 "Rot")
   (0,1,0 "Grün")
   (0,0,1 "Blau")
   (1,1,0 "Gelb")
   (0,1,1 "Cyan")
   (1,0,1 "Magenta")
  )
)

;------------------------------------------------------------------------------
;
; Other example:
;
;(
;  :Palette-Name     "Motif Blue Tones"
;  :Format           :motif
;  :Colors
;  (
;    ("#0000ff" "Blue")
;    ("#b0e2ff" "BlueLight")
;    ("#3232cc" "BlueMedium")
;    ("#f0f8ff" "BlueAlice")
;    ("#5f929e" "BlueCadet")
;    ("#222298" "BlueCornflower")
;    ("#00bfff" "BlueDeepSky")
;    ("#1e90ff" "BlueDodger")
;    ("#2f2f64" "BlueMidnight")
;    ("#232375" "BlueNavy")
;    ("#b0e0e6" "BluePowder")
;    ("#4169e1" "BlueRoyal")
;    ("#729fff" "BlueSky")
;    ("#87cefa" "BlueSkyLight")
;    ("#7e88ab" "BlueSlate")
;    ("#8470ff" "BlueSlateLight")
;    ("#6a6a8d" "BlueSlateMedium")
;    ("#384b66" "BlueSlateDark")
;    ("#5470aa" "BlueSteel")
;    ("#7c98d3" "BlueSteelLight")
;    ("#8a2be2" "BlueViolet")
;  )
;)
;
;------------------------------------------------------------------------------
