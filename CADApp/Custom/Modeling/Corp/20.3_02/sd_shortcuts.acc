﻿; ------------------------------------------------------------------------------
; SVN: $Id: sd_shortcuts.acc 38125 2014-07-03 10:03:33Z pjahn $
; keyboard shortcut file for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

;; This file contains definitions of keyboard accelerators and command
;; abbreviations which are valid in "SolidDesigner".


(keyboard-filing-revision "18.00")

;; ----- Definition of Keyboard Accelerators ----------------------------------
;; A keyboard accelerator for an available command can be defined like this:
;;     A combination of:
;;        - Shift, Ctrl, Alt
;;        - a character or number
;;        - F1 ... F12
;;        - NumPad0 ... NumPad9
;;     separated by a space " "
;;     Examples: "Shift Ctrl A"  or  "Ctrl NumPad5"

(start-keyboard-accelerators)

("F4" :command "3D Hidden On/Off"   :group "Show" :application "SolidDesigner")
("F5" :command "3D Geometry On/Off")
("F6" :command "Shaded/Wire")
("F7" :command "WP Borders On/Off")
("F8" :command "WP Axes On/Off")


("CTRL ALT A" :command "Fit" :group "View" :application "All")
("CTRL ALT T" :command "View Dir Z")
("CTRL ALT L" :command "View Dir X")
("CTRL ALT R" :command "View Dir -X")
("CTRL ALT F" :command "View Dir Y")

("CTRL F" :command "Search" :group "Miscellaneous" :application "SolidDesigner")
("CTRL N" :command "New Session" :group "Filing")
("CTRL O" :command "Load ...")
("CTRL S" :command "Save ...")
("CTRL P" :command "Print" :group "Print")
("CTRL X" :command "Cut" :group "Edit")

;;("CTRL TAB"  :command "Flyby Probe" :group "Select")
("TAB"       :command "Tab Key Handler" :group "Miscellaneous")
("SHIFT TAB" :command "Shift Tab Key Handler")

("DELETE" :command "Universal Delete" :group "Delete")

("CTRL ALT 9" :command "Delete 2D")
("CTRL ALT 5" :command "Set Active WP" :group "Workplane")
("CTRL ALT 0" :command "Set Active Part" :group "Part and Assy")
("CTRL ALT 7" :command "New WP on Axis" :group "Workplane")
("CTRL ALT 6" :command "New WP on Face")
("CTRL ALT 1" :command "New WP on Face and Project Constr")
("CTRL ALT 2" :command "New WP by Point and Dir")
("CTRL ALT 8" :command "Draw Only ..." :group "Drawlist")
("CTRL ALT 3" :command "Remove from Drawlist ...")

;;----- Definition of Command Abbreviations -----------------------------------
;; A command abbreviation is a sequence of one to N characters to be typed 
;; in to invoke an available command. 
;; NOTE: The [ENTER] key is necessary after input of the character
;;       sequence to invoke the command.

(start-command-abbreviations)

("dac" :command "Delete All 3D" :group "Delete" :application "SolidDesigner")

("netviewer" :command "Netviewer starten" :group "TECHSOFT" :application "All")
("teamviewer" :command "Teamviewer starten" :group "TECHSOFT" :application "All")

