; ------------------------------------------------------------------------------
; SVN: $Id: da_config 38125 2014-07-03 10:03:33Z pjahn $
; configuration file for ptc creo elements direct fea 18
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Customization File for Design Advisor
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; The following steps are performed in this file:
; - customize Design Advisor in order to match the actual non-standard 
;   installation.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Some general notes on customizing Design Advisor
;
;   To avoid changes during later software updates of Design Advisor,
;   this customization file should be copied to a file named "da_customize"
;   or "da_customize.lsp" which will not be overwritten by the update process.
;   Only if "da_customize" is not found, "da_customize.orig" will be loaded as
;   default.
;   If you are running Design Advisor over a network you can also copy
;   the ``da_customize'' file to the individual user directories from
;   where Design Advisor is started.  This way, every user can work with
;   his own preferred customization.
;
;   In addition to "da_customize.orig" the material database files 
;   should not be used for individual changes. They should be copied
;   to a safe place before editing. 
;
;   For further information : please refer to the online documentation
;   provided in:
;   "/opt/CoCreate/SolidDesigner/documentation/DesignAdvisor/DA_customize.html"
;   you can use the HELP-Browser to access this file.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;
;---- the values in the calls below are the values used by default within
;---- Design Advisor, therefore they are commented out
;
;(if (si:eval-feature :NT)
;  (progn
;    ;; NT customization
;    (ferrum::ferrum-set-scratchdir "C:/TEMP")
;    (ferrum::ferrum-set-nastranexe "/Progra~1/CoCreate/Msc/NASTRAN_V69.1/msc69/i386/nastran.exe")
;    (ferrum::ferrum-set-nastranexe "/Progra~1/CoCreate/Msc/NASTRAN_V70.5/msc705/i386/nastran.exe")
;    (ferrum::ferrum-docu-docu-dir  "C:/TEMP")
;  )
;  (progn ;; else
;    ;; Unix customization
;    (ferrum::ferrum-set-scratchdir "/var/tmp")
;    (ferrum::ferrum-set-nastranexe "/opt/CoCreate/MSC/NASTRAN_V69.1/bin/nast691")
;    (ferrum::ferrum-set-nastranexe "/opt/CoCreate/MSC/NASTRAN_V70.5/bin/nast705")
;    (ferrum::ferrum-docu-docu-dir  "/var/tmp")
;  )
; )
;
;---- Further customization regarding the resources the solver is allowed to
;     use, the default is to use 16MB, but be careful, when increasing this
;     value, it has to stay below the SHMMAX parameter (maximum shared memory size)
;---- of the HP-UX kernel.
;
 (ferrum::ferrum-set-nastran-real-mem 1024)
;
;---- general customization
;
; (ferrum::ferrum-set-rootdir	 "personality")
; (ferrum::ferrum-docu-logo      "personality/DesignAdvisor/CoCreate.gif")
; (ferrum::ferrum-docu-project   "") ;; e.g. use "<some directory>/project_description.html"
; (ferrum::ferrum-docu-author    "<your name fetched from the system>")

;;define default material database
 (FERRUM::FERRUM-MAT-DEFAULT-SOURCE :DB_TABLE)
 (FERRUM::SE-DB-ATTACHE (format nil "~a/~a/DesignAdvisor/test_materials.dat" (oli::sd-convert-filename-from-platform (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")) (oli::sd-sys-getenv "SDLANG")))
 (FERRUM::SE-DB-CONNECT (format nil "~a/~a/DesignAdvisor/test_materials.dat" (oli::sd-convert-filename-from-platform (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")) (oli::sd-sys-getenv "SDLANG")))
