﻿; ------------------------------------------------------------------------------
; SVN: $Id: am_customize 38127 2014-07-03 10:54:23Z pjahn $
; customization file for creo elements/direct annotation 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

;; *************************************************************
;; WorkManager - Annotation Link customization
;; please add customizations for the WorkManager - Annotation Link
;; to the file "am_wm_customize"
;; *************************************************************

;;Example: Frame registration
;(docu-register-frame 
;	:ui-name "Company-A0"
;	:wm-name "" 
;	:file "/tmp/Company-A0"
;	:default :everytime)     ;; T/NIL/:everytime

;;Further frame related functions:
;(docu-unregister-frame "<ui-name>")
;(docu-unregister-frame-all)

;;-----------------------------------------------------------------------------
;; register standard frame sizes for recognition during migration (A..E,A0..A4)
;;-----------------------------------------------------------------------------
(docu-register-migr-frame-standard-sizes)

;; to register NON standard frame sizes for recognition during migration
;;                                "<ui-name>" size-horizontal size-vertical
;; (docu-register-migr-frame-size "Q44" 400 400)

;;-----------------------------------------------------------------------------
;; predefined text reference groups and types for frame customization
;;-----------------------------------------------------------------------------
(docu-unregister-tr-group "Sheet_local")
(docu-register-tr-group   "Sheet_local"  "SHEET")
(docu-register-tr-type    "Sheet_local"  "SHEET_NO" "???" 
                          'docu::docu-get-current-sheet-name) 
(docu-register-tr-type    "Sheet_local"  "SHEET_SCALE" "???"
                          'docu::docu_inq_curr_sheet_scale_text)

(docu-unregister-tr-group "Sheet_global")
(docu-register-tr-group   "Sheet_global" "GLOBAL")
(docu-register-tr-type    "Sheet_global" "SHEETS"   "???" 
                          'docu::docu-inq-highest-sheet-name) 
(docu-register-tr-type    "Sheet_global" "DRAWING_NO"   "???"
                          'docu::docu_inq_drawing_number_text)

;;-----------------------------------------------------------------------------
;; automatically update text references for "current sheet" and "highest sheet"
;;-----------------------------------------------------------------------------
(oli::sd-unsubscribe-event "*DOCU-CHANGE-SHEET-DATA-EVENT*" 
                           'docu-update-sheet-info)

(defun docu-update-sheet-info ()
  (docu-update-tr-group "Sheet_local" )
  (docu-update-tr-group "Sheet_global")
)

(oli::sd-subscribe-event   "*DOCU-CHANGE-SHEET-DATA-EVENT*" 
                           'docu-update-sheet-info)


;; Example: Special Character

;(docu-register-special-char :char-code    126
;                            :font-code    :hp_symbols2
;                            :meta-name    "Micro"
;                            :pixmap-file  "bitmaps/bmp/Annotation/spc2_126.bmp")

;(docu-register-special-char :char-code    63
;                            :font-code    :hp_symbols
;                            :meta-name    "Depth"
;                            :pixmap-file  "bitmaps/bmp/Annotation/spc_063.bmp")

;;-----------------------------------------------------------------------------
;; Example: Mapping for user defined characters via DEFINE_UDC_MAPPING
;(oli::sd-execute-annotator-command
;                            :cmd "DEFINE_UDC_MAPPING 'CP932' #240#64 #226#0")

;;-----------------------------------------------------------------------------
;; Example: Merging of an old, loaded 1 byte and an old, loaded 2 byte font 
;; into a Unicode font. 
;(docu::docu-merge-fonts :font1b "hp_i3098_c" :font2b "hp_kanji_c")

;;-----------------------------------------------------------------------------
;; Common Owner
;;-----------------------------------------------------------------------------
;; Set all owner groups :geo :c-geo :text :symbol :sketch to one owner group:
(docu-set-common-owner :all)
;; Set two common owner groups for geo/c-geo and text/symbol
;; (docu-set-common-owner '((:geo :c-geo) (:text :symbol)))
;; Reset common owner specification  (behaviour of version <7.0)
;; (docu-set-common-owner nil)

;; Example: control the capability of the Grab command
;(docu-selected-settings-single-selection-auto-grab :on)  ;;(:on/:off)

;; Example: disable/enable setting of pen- or linesize 
;(docu-set-pen-and-line-size-mode :both) 
;;(:both/:pensize_only/:linesize_only/:pensize_prefered/:linesize_prefered/:none) 

;; Example: Enable/Disable test for converting dim tolerance types
;(docu-enable-test-for-dim-conversion :ON)  ;; (:ON/:OFF)

;; the following function defines the behaviour of font translation
;;   during file 2D data export
;;  input 
;;    mode        :to_geo | :to_text    alternative keywords
;;    font-list   (list of strings)     font name list
;;                or  :none  :all       empty list / all fonts
;;   when mode==:to_geo then all texts using listed fonts will
;;                            be converted to geometry
;;   when mode==:to_text then all texts NOT using listed fonts
;;                            will be converted to geometry
;;                            (the listed fonts will be kept)
;;   text_to_geometry_button
;;       :on / :off   define the default for related button
;;                      of export dialogs / option

(docu-define-export-fonts :to_geo :all)

;; The following functions define behavior during Import 2D, which
;; is also used for database loading:
;;(DOCU-CLEAR-TOP-PART-FOR-IMPORTED-SD-FILES   :ON)  ;;:OFF/:ON
;;(DOCU-CLEANUP-ON-ERROR-FOR-IMPORTED-SD-FILES :OFF) ;;:ON/:OFF

;; *** Annotation Plot Settings : see am_plot_customize

;;Use nearest point on source for reflines
;;(docu-set-rtl-nearest-mode :ON)

;;Customization function to define VIEW PROFILES used within the
;;view creation commands and update
;; (docu-clear-view-profiles)
;; resets all defined view profiles
;;
;; (docu-register-view-profile 
;; defines standard profiles for all types of views
;; :name                           "Unique profile name" 
;; :label                          "This name is displayed in the UI"
;; :minimum-number-of-parts        100  ;; if these two keywords are not specified then the profile will
;; :maximum-number-of-parts        1000 ;; never be selected by default, but can be selected 
                                        ;; within the user interface.
                                        ;; if there are more than 1 profile with the same number-of-parts
                                        ;; settings the LAST one is selected automatically (see profile
                                        ;; 'Complex Simple' and 'Simple Single' below).
;; :update-mode                    :graphics ;; or :classic  or :shaded_only
;; :shaded-mode                    :off ;; or :on
;; :render-mode                    :off ;; or :on
;; :shaded-resolution           300 ;; or any other positive number or :undefined --> automatic
;; :facet-accuracy                 :medium   ;; or :low, :high, :current
;; :associativity-2d               :full ;; or :limited
;; :pressfit-handling              :on   ;; or :off or :default
;; :clash-recognition                 :off  ;; or :on  or :default
;; :remove-small-parts             0 ;; or any percentage number, ie 1.0 
;; :remove-library-parts           :on ;; or off 
;; :remove-parts-mode              :immediately  ;; or :temporary
;; :remove-full-circles            0 ;; or any threshold value, ie. 0.5 
;; :remove-duplicate-hidden-lines  :on ;; or :off 
;; :thread-creation                  :on ;; or :off, :default or :parent (for detail & section)
;; :centerline-creation            :on ;; or :off, :default or :parent (for detail & section)
;; :symmetryline-creation      :on ;; or :off, :default or :parent (for detail & section)
;;  :use-part-color                   :off ;; or  :on
;; :hidden-line-visible            :on ;; or :off, :default or :parent (for detail & section)
;; :tangent-line-visible           :on ;; or :off, :default or :parent (for detail & section)
;; )
;;
;; (docu-register-exception-profile
;; defines global settings for specific view types
;; :name                           "Unique profile name"
;; :label                          "This name is displayed in the UI"
;; :minimum-number-of-parts        1
;; :maximum-number-of-parts        999999
;; :view-type                      :standard ;; or :all, :section, :detail, :general
;;                                              or a list of view-types like (list :standard :general)
;; :update-mode                    :graphics ;; or :classic
;; :facet-accuracy                 :medium   ;; or :low, :high, :current
;; :associativity-2d               :full ;; or :limited
;; :pressfit-handling              :on   ;; or :off or :default
;; :clash-recognition              :off  ;; or :on  or :default
;; :remove-small-parts             0 ;; or any percentage number, ie 1.0
;; :remove-library-parts           :on ;; or off
;; :remove-parts-mode              :immediately  ;; or :temporary
;; :remove-full-circles            0 ;; or any threshold value, ie. 0.5
;; :remove-duplicate-hidden-lines  :on ;; or :off
;; :thread-creation                :on ;; or :off, :default or :parent (for detail & section)
;; :centerline-creation            :on ;; or :off, :default or :parent (for detail & section)
;; :symmetryline-creation          :on ;; or :off, :default or :parent (for detail & section)
;; :hidden-line-visible            :on ;; or :off, :default or :parent (for detail & section)
;; :tangent-line-visible           :on ;; or :off, :default or :parent (for detail & section)
;; )

(docu-clear-view-profiles)

(docu-register-view-profile
  :name                           "Simple Single" ;; same as 'Complex Single' but
                                                  ;; :update-view-immediately :on
  :label                          "Einzelnes Teil"
  :minimum-number-of-parts        1
  :maximum-number-of-parts        1
  :update-mode                    :classic
  :shaded-mode                    :off    
  :associativity-2d               :full
  :pressfit-handling              :off
  :clash-recognition              :off
  :update-view-immediately        :on
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off
  :use-part-color                 :off   
  :thread-creation                :on
  :centerline-creation            :on
  :symmetryline-creation          :on
  :hidden-line-visible            :on
  :tangent-line-visible           :on
)


(docu-register-view-profile
  :name                           "Small"
  :label                          "Kleine Baugruppe"
  :minimum-number-of-parts        2
  :maximum-number-of-parts        99
  :update-mode                    :classic
  :shaded-mode                    :off    
  :associativity-2d               :full
  :pressfit-handling              :default
  :clash-recognition              :default
  :update-view-immediately        :on
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off
  :use-part-color                 :off  
  :thread-creation                :on
  :centerline-creation            :on
  :symmetryline-creation          :on
  :hidden-line-visible            :off
  :tangent-line-visible           :on
)

(docu-register-view-profile
  :name                           "Medium"
  :label                          "Mittelgroße Baugruppe"
  :minimum-number-of-parts        100
  :maximum-number-of-parts        500
  :update-mode                    :graphics
  :shaded-mode                    :off
  :facet-accuracy                 :medium
  :associativity-2d               :full
  :pressfit-handling              :default
  :clash-recognition              :off
  :update-view-immediately        :on
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off
  :use-part-color                 :off    
  :thread-creation                :off
  :centerline-creation            :on
  :symmetryline-creation          :on
  :hidden-line-visible            :off
  :tangent-line-visible           :on
)

(docu-register-view-profile
  :name                           "Large"
  :label                          "Große Baugruppe"
  :minimum-number-of-parts        501
  :maximum-number-of-parts        999999
  :update-mode                    :graphics
  :shaded-mode                    :off
  :facet-accuracy                 :low
  :associativity-2d               :limited
  :pressfit-handling              :off
  :clash-recognition              :off
  :update-view-immediately        :off
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :on 
  :use-part-color                 :off    
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off
  :hidden-line-visible            :off
  :tangent-line-visible           :off
)

(docu-register-view-profile 
  :name                           "PhotoRealistic" 
  :label                          "Fotorealistisch"
  :update-mode                    :shaded_only
  :shaded-mode                    :on
  :render-mode                    :on
  :use-part-color                 :on  
  :hidden-line-visible            :off
  :tangent-line-visible           :off
  :remove-duplicate-hidden-lines  :on   
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off  
)

(docu-register-view-profile 
  :name                           "ShadedOnly" 
  :label                          "Nur schattiert"
  :update-mode                    :shaded_only
  :shaded-mode                    :on
  :render-mode                    :off  
  :use-part-color                 :on
  :hidden-line-visible            :off
  :tangent-line-visible           :off
  :remove-duplicate-hidden-lines  :on   
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off  
)

(docu-register-view-profile 
  :name                           "ShadedGeometry" 
  :label                          "Schattiert + Geometrie"
  :update-mode                    :graphics
  :shaded-mode                    :on
  :render-mode                    :off  
  :use-part-color                 :on
  :hidden-line-visible            :off
  :tangent-line-visible           :off
  :remove-duplicate-hidden-lines  :on   
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off  
)

(docu-register-view-profile 
  :name                           "NC" 
  :label                          "NC"
  :update-mode                    :classic 
  :shaded-mode                    :off  
  :associativity-2d               :full
  :pressfit-handling              :on
  :clash-recognition              :default
  :update-view-immediately        :on
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately  
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off 
  :use-part-color                 :off  
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off
  :hidden-line-visible            :off
  :tangent-line-visible           :off
)

(docu-register-view-exception-profile 
  :name                           "Section" 
  :label                          "Schnitt"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :section
  :centerline-creation            :parent
  :symmetryline-creation          :parent
  :hidden-line-visible            :off
  :tangent-line-visible           :parent
  :visible-cutaway-mode           :no
)

(docu-register-view-exception-profile 
  :name                           "Detail" 
  :label                          "Detail"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :detail
  :centerline-creation            :parent
  :symmetryline-creation          :parent
  :thread-creation                :parent
  :hidden-line-visible            :parent
  :tangent-line-visible           :parent
  :visible-cutaway-mode           :yes
)

(docu-register-view-exception-profile 
  :name                           "Broken"
  :label                          "Unterbrochen"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :broken
  :centerline-creation            :parent
  :symmetryline-creation          :parent
  :thread-creation                :parent
  :hidden-line-visible            :parent
  :tangent-line-visible           :parent
)

(docu-register-view-exception-profile
  :name                           "Isometric Small" 
  :label                          "Isometrisch klein"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        99
  :view-type                      :general
  :centerline-creation            :off
  :symmetryline-creation          :off
  :thread-creation                :off
  :visible-cutaway-mode           :yes
  :visible-section-mode           :yes
)

(docu-register-view-exception-profile 
  :name                           "Isometric Large" 
  :label                          "Isometrisch groß"
  :minimum-number-of-parts        100
  :maximum-number-of-parts        999999
  :view-type                      :general
  :update-mode                    :graphics
  :facet-accuracy                 :low
)

;;disable econo fast mode for views without view profile
(setq docu::*docu-use-hardware* :normal) ;;econofast


;;UNITS=====================================================
;; reset to SD default units
(restore-user-units)
;; you may can set the UNITS to your preferred values now
;;(units 1 :mm) (units 1 :deg)


;;goodies==================================================

 ;;text on arc
 (load "am_text_arc")

 ;;set reference 3d-viewport
 (load "am_ref3dvp")

 ;;color parts and assemblies in drawing
 ;;(load "am_color_part")

;;Delete default styles for node 'Annotation/Plot'=========

  (oli:sd-delete-settings-style "Annotation/Plot" :LASER_CL :forced t)
  (oli:sd-delete-settings-style "Annotation/Plot" :LASER_BW :forced t)
  (oli:sd-delete-settings-style "Annotation/Plot" :PLOTTER_PEN :forced t)
  (oli:sd-delete-settings-style "Annotation/Plot" :PLOTTER_INK :forced t)
  (oli:sd-delete-settings-style "Annotation/Plot" :CLIPBOARD :forced t)  

;;projected reference points===============================

 (AM_MBP_SETTINGS
  :circle_size 1
  :circle_color :color YELLOW :done
  :circle_linetype :PHANTOM
  :lines_size 1.5
  :lines_color :color YELLOW :done
  :lines_linetype :SOLID
  :extension_color :color YELLOW :done
  :extension_linetype :PHANTOM
  :lost_color :color RED :done
  :automatic_update
 )
 
;;add support for legacy dimensioning commands=============

 (load "old_am_dimension")

;;radius dimension without arc extension
  (oli::sd-execute-annotator-command :cmd "DIM_RAD_NEVER_WITH_ARC_EXTENSION ON")
  
;;replace special character "square" with a smaller one====

 (docu-register-special-char :char-code 171 
   :font-code :hp_symbols2 
   :meta-name "Square" 
   :pixmap-file (format nil "~a/german/bitmaps/bmp/Annotation/spc2_191.bmp" (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")))
   
;;prevent user modifications to annotation default settings

  ;; Disallow editing of ALL settings:
  ;(oli:sd-set-setting-modifiable-flag :subpath "Annotation" :modifiable nil)
		
  ;; Allow editing of SPECIFIC settings (see some examples below) (adapt to your needs)
  ;(oli:sd-set-setting-modifiable-flag :subpath "Annotation/Plot" :modifiable t)
  ;(oli:sd-set-setting-modifiable-flag :subpath "Annotation/Plot" :modifiable t)
  ;(oli:sd-set-setting-modifiable-flag :subpath "Annotation/Viewport/Appearance" :modifiable t)
  ;(oli:sd-set-setting-modifiable-flag :subpath "Annotation/Copilot" :modifiable t)
  
;;enable or disable wrong owner warning====================

	;(setq docu::*docu-hide-wrong-owner-warning* t) ;; turn warning off
	;(setq docu::*docu-hide-wrong-owner-warning* nil) ;; turn warning back on

;;integrate Techsoft SolidPower============================

 (if (oli::sd-string-p (oli::sd-sys-getenv "TSPRODIR"))
    (load (format nil "~a/sd_4/tspro_am_loads.lsp" (oli::sd-sys-getenv "TSPRODIR")))
    ;; else
    (load "C:/tspro/sd_4/tspro_am_loads.lsp")
 )
 
;;add ability to switch between default an b/w color scheme
 (load (format nil "~a/addons/lisp/am_colors.lsp" (oli::sd-sys-getenv "BASEDIR")))