;% Automatically written on 03/07/2007 at 13:08:58
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/General/ArrowStyle"
  :style :|PFEIL_GROß|
  :title "Pfeil_Groß"
  :visible-if :TECHSOFT
  :values '("Filled" LISP::T
            "Type" :ARROW_TYPE
            "Size" 7.0
            )
)
