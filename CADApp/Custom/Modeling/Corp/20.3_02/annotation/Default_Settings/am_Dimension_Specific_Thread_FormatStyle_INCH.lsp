;% Automatically written on 10/13/2008 at 14:32:14
;% CoCreate Modeling Revision: 2008 (16.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Dimension/Specific/Thread/FormatStyle"
  :style :INCH
  :title "Inch"
  :values '("Format" "<Nominal-Durchmesser-Zoll> - <TPI>x<Länge> <Gewinderichtung>"
            )
)
