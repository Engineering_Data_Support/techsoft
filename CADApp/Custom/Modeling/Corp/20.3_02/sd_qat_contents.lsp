; -------------------------------------------------------------------------------
; SVN: $Id: sd_qat_contents.lsp 38125 2014-07-03 10:03:33Z pjahn $
; customization file the quick access toolbar in creo elements/direct modeling 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; -------------------------------------------------------------------------------

(oli::sd-fluentui-set-quick-access-toolbar-contents "SolidDesigner"
  '(
   (:AVAILCMD ("SolidDesigner" "Filing" "New Session"))
   (:AVAILCMD ("SolidDesigner" "Filing" "Load ..."))
   (:AVAILCMD ("SolidDesigner" "Filing" "Save ..."))
   (:AVAILCMD ("SolidDesigner" "Print" "Print Preview"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "Miscellaneous" "Annotation"))
   (:AVAILCMD ("All" "TECHSOFT" "Sheet Metal"))
   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))
   ))