﻿; ------------------------------------------------------------------------------
; SVN: $Id: sd_avail_cmds.cmd 38125 2014-07-03 10:03:33Z pjahn $
; commands for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(:Application "All")

;;----- All: Techosft ----------------------------------------------------------

(:Group "TECHSOFT" :title "TECHSOFT")

("Sheet Metal"
 :title       "Sheet Metal"
 :action      (LISP::PROGN
                (LISP::IF (OLI::SD-MODULE-ACTIVE-P "SHEETADVISOR")
                    (LISP::PROGN
                      (OLI::SD-SWITCH-APPLICATION "SheetAdvisor"))
                    (LISP::PROGN (STARTUP::ACTIVATE-SHEETADVISOR))))
 :description "Startet das bzw. wechselt zur Anwendung Sheet Metal"
 :image       "All/TECHSOFT/SheetMetal"
 :ui-behavior :DEFAULT)
 
("Model Manager"
 :title       "Model Manager"
 :action      (LISP::PROGN
				(LISP:IF (startup::license-free-module-active-p "ModelManager")
					(LISP::PROGN
						(MODELMANAGER::MM-SEND-CMD "SHOW-MANAGER"))
					  (LISP::PROGN (act_deact_module :act "ModelManager" "MODULE-CONTROLLER-Modules-ModelManager-TB" '(STARTUP::ACTIVATE-MODELMANAGER)))))
 :description "Öffnet das Arbeitsbereichsfenster von Model Manager"
 :image       "All/TECHSOFT/ModelManager"
 :sd-access   :yes
 :modeling-pe :no
 ) 
 
("SDCORPCUSTOMIZEDIR anzeigen"
 :title       "SDCORPCUSTOMIZEDIR anzeigen"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")))
 :description "Anzeigen der Anpassungen für den Konzern"
 :image       "All/TECHSOFT/corp"
 :sd-access   :yes) 

 
;;----- SolidDesigner: Construction --------------------------------------------
 
(:Application "SolidDesigner")

(:Group "Construction" :title "Hilfsgeometrie")

("Querschnitt"
 :title       "Querschnitt als Hilfgeometrie projizieren"
 :action      "geometry_mode :construction cross_section :cross_section_wp"
 :description "Teile-Querschnitt als Hilfsgeometrie auf eine Arbeitsebene projizieren."
 :image       "SolidDesigner/Construction/Querschnitt"
 :ui-behavior :DEFAULT)
 
;;----- Annotation: TECHSOFT --------------------------------------------

(:Application "Annotation")

(:Group "TECHSOFT" :title "TECHSOFT")

("Annotation Farbschema wechseln"
 :title       "Annotation Farbschema wechseln"
 :action      "(ts-switch-background-color)"
 :description "Wechselt zwischen normalem Farbschema und S/W-Farbschema in Annotation."
 :image       "Annotation/TECHSOFT/Switch_Annotation_Color_Scheme"
 :ui-behavior :DEFAULT)
