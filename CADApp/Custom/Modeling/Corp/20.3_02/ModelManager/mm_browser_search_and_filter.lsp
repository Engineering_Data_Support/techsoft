﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Description:  Model Manager's default "Browser Search and Filter" file
;
; (C) Copyright 2011 Parametric Technology GmbH, all rights reserved.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This file contains definitions of browser views that can be activated
;; via the structure browser "Views" menu button when Model Manager is
;; activated.

;; Define MM Status integer
(oli::sd-create-uda-string-column-definition 
 :mm-status-int-column
 :title "MM-Status - Ganzzahl"
 :attribute "DB-GREF"
 :value-list-key :mm_status  ;; contains the MM status code
 :attachment :contents
 :ui-accessible nil
 )

(oli:sd-create-browser-filter                                                       
 "parcel-gbrowser"
 :name "MM STATUS INT"
 :title "MM-Probleme"
 :match :any
 :criteria '(
             (:column :mm-status-int-column :value "-3")
             (:column :mm-status-int-column :value "-1")
             (:column :mm-status-int-column :value "-2")
             (:column :mm-status-int-column :value "3")
             )
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )
(oli:sd-create-browser-search
 "parcel-gbrowser"
 :name "MM STATUS INT"
 :title "MM-Probleme"
 :match :any
 :criteria '(
             (:column :mm-status-int-column :value "-3")
             (:column :mm-status-int-column :value "-1")
             (:column :mm-status-int-column :value "-2")
             (:column :mm-status-int-column :value "3")
             )
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )

;; Define load-type attribute
(oli::sd-create-uda-enum-column-definition 
 :load-type-column
 :title "DB-Ladetyp"
 :attribute "DB-GREF"
 :value-list-key :load_type
 :enumerators '("0" "1" "2")
 :mapping '(
            ("0" "Vollst.")
            ("1" "Teilweise")
            ("2" "Lightweight")
           )
 :attachment :contents
 )

;; Fully loaded parts filter and search
(oli:sd-create-browser-filter                                                       
 "parcel-gbrowser"
 :name "LOAD_TYPE_0"
 :title "Vollständig geladene Modelle"
 :match :any
 :criteria '(
             (:column :load-type-column :value "0")
             (:column :load-type-column :operation :is-not-defined)
             )
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )
(oli:sd-create-browser-search
 "parcel-gbrowser"
 :name "LOAD_TYPE_0"
 :title "Vollständig geladene Modelle"
 :match :any
 :criteria '(
             (:column :load-type-column :value "0")
             (:column :load-type-column :operation :is-not-defined)
             )
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )

;; Partially loaded parts filter and search
(oli:sd-create-browser-filter
 "parcel-gbrowser"
 :name "LOAD_TYPE_1"
 :title "Teilweise geladene Modelle"
 :criteria '((:column :load-type-column :value "1"))
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )
(oli:sd-create-browser-search
 "parcel-gbrowser"
 :name "LOAD_TYPE_1"
 :title "Teilweise geladene Modelle"
 :criteria '((:column :load-type-column :value "1"))
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )

;; Lightweight loaded parts filter and search
(oli:sd-create-browser-filter
 "parcel-gbrowser"
 :name "LOAD_TYPE_2"
 :title "Als Lightweight-Version geladene Modelle"
 :criteria '((:column :load-type-column :value "2"))
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )
(oli:sd-create-browser-search
 "parcel-gbrowser"
 :name "LOAD_TYPE_2"
 :title "Als Lightweight-Version geladene Modelle"
 :criteria '((:column :load-type-column :value "2"))
 :enable '(oli:sd-license-free-module-active-p "ModelManager")
 )

