﻿; ------------------------------------------------------------------------------
; SVN: $Id: sha_demoshop_func.lsp 38153 2014-07-04 12:22:54Z pjahn $
; customization file for creo elements/direct sheet metal 18
; Wolfgang Hofer; Techsoft Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

;LOLAMAX=33
(in-package :sheet-advisor)

;============================= CONTENTS =======================================
;  1. Global variables
;  2. Bend allowance formulas
;  3. Offset allowance formula
;  4. Advisor check functions
;  4a. Check functions to determine wether given tool is already used in actual part
;  5. Radius formula for air-bending tools
;  6. Distance check formula
;  7. Default relief size formula
;  8. Default bend-radius formula 
;  9. Automatic part attribute setting
; 10. Example function for a customer defined drawing option in MULTI_UNFOLD =
; 11. Example dialog for a customer defined MULTI_UNFOLD
;     interactive postprocess dialog 
; 12. Example function for a customer defined MULTI_UNFOLD
;     "SHOP defined" (= NON interactive) postprocess function
; 13. Example for an automatic process selection function in MULTI_UNFOLD 
;==============================================================================


;=========================== 1. Global variables ==============================

;----------------------------------------------------------------------------
; Severity of warnings and errors detected by the Advisor.
; To allow different customer specific classifications, the severity is derived
; from a mapping associated to each error type. The list of actual used
; error types and their severity mapping is hold by the *sha-severity*
; global property list.
; Change this list if you want to change some of the severities.
; New error types, to be used in warning or error message in the other
; functions in this file, may also be introduced.

(setf *sha-severity*
  '(:tdb-shop-missing :high
    :material-not-found :medium
    :using-fallback-strategy :low
    :tdb-table-missing :medium
    :tdb-tool-missing  :medium
    :tdb-entry-missing :medium
    :DFM_rule_violation :low
    :tool-overlap :low
    :unacceptable-deformation :medium
    :not-manufacturable :high
    )
)

;----------------------------------------------------------------------------
; range in which angles should be detected as equal
(defvar *sha-angle-data-eps* 0.00001)

;----------------------------------------------------------------------------
; range in which lengths and radius should be detected as equal
(defvar *sha-length-data-eps* 0.001)

; following variables are used to switch on/off the warning messages in the
; the Sh_bend_allowance_din formula.

(defvar *sh_global_dfm_checks* t)
(defvar *sh_local_dfm_checks* t)



;==================== 2. Bend allowance formulas ==============================;
;                                                                              ;
; to be used as entry in the :ALLOWANCE_FORMULA column of a bend process table ;
;==============================================================================;

(defun air_bend_allow (&key sheet_thickness bend_angle bend_radius 
                            sheet_material tool_id 
                       &allow-other-keys)
   ;___________________________________________________________________________
   ;
   ; Description:
   ; ------------
   ; As the name suggests this function is intended to be used as allowance 
   ; formula for the air-bending process. But similar or even the same formula
   ; may be used for other bend processes too.
   ; Different algorithms for the calculation of the resulting bend allowance
   ; may be used (see examples at the end of this function definition). Please
   ; comment out or change the function calls to adapt the calculation to your
   ; needs.
   ; The allowance itself is defined as described in the chapter 
   ; "Allowance tables and formulas" within online help.
   ;
   ; Parameters:
   ; -----------
   ; The following five parameters are added by the GENERATE_FLAT, ADD_SHEET 
   ; ADD_PROFILE , NEW_PROFILE ,SHA_INQUIRE_BEND and the SHA_UNFOLD  commands,
   ; when they ask for a specific bend  allowance calling this function.
   ;
   ; These parameters can not be modified, {units} are fixed to mm and deg     
   ;
   ; INPUT
   ; =====
   ; sheet_thickness   [mm]  Material thickness                            
   ; bend_angle        [deg] bend angle of bend  (0 = no bend )          
   ; bend_radius       [mm]  bend radius of bend  (0 = no bend )           
   ; sheet_material    Material property list :                           
   ;                     syntax:
   ;                      (:shopname <shopname> :tabname <sheet mat. table>
   ;                       :rowkey <material key property list>)
   ;                     syntax e.g.
   ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
   ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
   ; tool_id           Tool property list:                                
   ;                     syntax:
   ;                      (:shopname <shopname> :tabname <tool table name> 
   ;                       :rowkey <tool key property list>)
   ;                     syntax e.g.
   ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
   ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
   ;                                :PISTON_ANG 0.78539816339744828))
   ;
   ;
   ; RETURN
   ; ======
   ; property list ( :allowance allowance-value         ; if not given the
   ;                                                    ; system uses a 
   ;                                                    ; fallback strategy
   ;                 :k-factor k-factor                 ; optional: k-factor as an alternative to allowance  
   ;                                                    ; must for conical bending   
   ;                 :warning-headline message-string   ; optional
   ;                 :error-type violation-type         ; optional
   ;                 :error-message message_string )    ; optional
   ;___________________________________________________________________________


  ;; ALTERNATIVE 1: (default)
  ;; ==============
  ;; Call standard DIN formula which reads the three parameter thickness    
  ;; radius and bend angle (tool_id and sheet_material are not used) 
  ;; The DIN Formula calculates a variable K-factor (describing the position
  ;; of the neutral phase in the bend zone) based on the relation 
  ;; bend_radius / sheet_thickness.

  (sh_bend_allowance_din :sheet_thickness sheet_thickness 
                         :bend_radius     bend_radius  
                         :bend_angle      bend_angle
                         :sheet_material  sheet_material
                         :tool_id         tool_id)

  ;; ALTERNATIVE 2:
  ;; ==============
  ;; Call a formula with material dependent or constant K-factor        
  ;; Depending on the way the K-factor is defined 
  ;;             (Middle phase of sheet : ANSI: K = 0.5
  ;;                                       DIN: K = 1)
  ;; Use the appropriate formula inside the Sh_bend_allowance_K_fact fnc.
  ;; see there for further explanation.

  ;  (Sh_bend_allowance_K_fact :sheet_thickness sheet_thickness 
  ;                            :bend_radius     bend_radius  
  ;                            :bend_angle      bend_angle
  ;                            :sheet_material  sheet_material
  ;                            :tool_id         tool_id)

  ;; ALTERNATIVE 3:
  ;; ==============
  ;; Call an interpolation formula, which uses an given allowance table but
  ;; does interpolation for those values which are not directly found in that
  ;; table. This allows to adapt to all those situations where above formulas
  ;; might be too inaccurate or due to special circumstances do not reflect the
  ;; the situation at all.

  ; (linear_interp :sheet_thickness sheet_thickness 
  ;                :bend_radius     bend_radius  
  ;                :bend_angle      bend_angle
  ;                :sheet_material  sheet_material
  ;                :tool_id         tool_id)

  ;; Other ALTERNATIVES may be introduced here:
  ;; ==========================================
)

;=================== allowance formulas alternatives ==========================
;===================     and fallback formulas       ========================== 
;
; Formulas used as fallback formula (:ALLOWANCE_FORMULA entry in the shop table)
; will be fed with the the three parameters:
;            sheet_thickness   bend_radius   bend_angle
; If a regular allowance formula could be satisfied by this three parameters
; only, the formula can also be used as fallback formula.            
;==============================================================================

;------ ALTERNATIVE 1: Calculate allowance according to DIN standard ---------

(defun Sh_bend_allowance_din (&key sheet_thickness bend_angle bend_radius
                                   sheet_material tool_id
                              &allow-other-keys )
  ;____________________________________________________________________________
  ;
  ; Description:
  ; 
  ; Calculate the allowance value according to DIN where the position of the
  ; neutral phase is calculated proportional to the logarithm of the
  ; relation : bend_radius / Sheet_thickness
  ;
  ; NOTE:  This function can also directly be used as fallback formula 
  ;        (then feeded by the first three parameters only)
  ;
  ; The parameters can not be modified, {units} are fixed to mm and deg     
  ;
  ; INPUT
  ; =====
  ; sheet_thickness   Material thickness [mm]                           
  ; bend_angle        [deg] bend angle of bend  (0 = no bend )          
  ; bend_radius       [mm] bend radius of bend  (0 = no bend )           
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ;
  ;
  ; RETURN
  ; ======
  ; property list ( :allowance allowance-value         ; if not given the
  ;                                                    ; system uses a 
  ;                                                    ; fallback strategy
  ;                 :k-factor k-factor                 ; optional: k-factor as an alternative to allowance  
  ;                                                    ; must for conical bending  
  ;                 :warning-headline message-string   ; optional
  ;                 :error-type violation-type         ; optional
  ;                 :error-message message_string )    ; optional
  ;___________________________________________________________________________

  (declare (ignore sheet_material)) ; to keep compiler quite when compiling

  (let ((given-allowance (getf (getf tool_id :ROWKEY) :ALLOWANCE))) 

    (if given-allowance
      ; return given allowance value
      (list :allowance given-allowance)

      ;; else - check whether tool_id contains a column k_factor.
      ;;        if so, call allowance formula of process in tool_id
      (let* ((shopname (or (getf tool_id :SHOPNAME) (getf sheet_material :SHOPNAME)))
             (tabname  (shoptabname shopname (getf tool_id :tabname)))
             (column-identifiers (when (sd-logical-table-p tabname)
                                   (sd-get-logical-table-column-identifiers tabname)))
            )

        (if (member :K_FACTOR column-identifiers)
          (let ((allowance-formula (sha-get-db-entry tool_id :ALLOWANCE_FORMULA))
               )
            (if (functionp allowance-formula)
              (funcall allowance-formula :sheet_thickness sheet_thickness
                                         :bend_angle      bend_angle 
                                         :bend_radius     bend_radius
                                         :sheet_material  sheet_material 
                                         :tool_id         tool_id)
              ;; else - fallback
              (roll_bending_allow_din :sheet_thickness sheet_thickness 
                                      :bend_angle      bend_angle 
                                      :bend_radius     bend_radius 
                                      :sheet_material  sheet_material 
                                      :tool_id         tool_id) 
            )
          )
          (let (R_t
                K_factor
                return_values)

            (setf R_t (/ bend_radius sheet_thickness))
            ; Calculate k-factor according DIN 
 
            (cond ((< R_t  0.65 )
                   (if (and *Sh_global_dfm_checks* *Sh_local_dfm_checks*)
                      (progn
                        (setf return_values 
                             (list :warning-headline "WARNUNG: Verstoß gegen Fertigungsregel (DFM)."
                                   :error-type :DFM_rule_violation
                                   :error-message 
                                    "Verhältnis Radius/Dicke sollte nicht kleiner als 0.65 sein."))
    
                        ; K_factor could get negative with very small R/t relation.
                        ; Therefore R/t will be limited to 0.1 
                        ; In reality this might reflect a fissure in the material
    
                        (setf R_t (max R_t 0.1))
                        (setf K_factor (+ 0.65 (/ (log R_t 10) 2)))
                      )
                   )
                  )
                  ((<= R_t 5)
                    (setf K_factor (+ 0.65 (/ (log R_t 10) 2)))
                  )
                  (t ;  R_t > 5
                    (setf K_factor 1)
                  )
            )
   
            ; Calculate bend allowance and set global return value  
            ; additoinally return k-factor
            ; Pass open angle between bent lips instead bend angle 
  
            (append return_values (list :allowance 
                                        (Sh_calculate_bend_allowance_DIN sheet_thickness bend_radius 
                                                                         (- 180 bend_angle) K_factor)
                                        :k-factor K_factor))
  
          ); end let
        ); end if
      ); end let
    ); end if
  );end let
)

;---------- ALTERNATIVE 2: use material dependent or constant K-factor --------

(defun Sh_bend_allowance_K_fact (&key sheet_thickness bend_angle bend_radius
                                      sheet_material 
                                 &allow-other-keys)

  ;____________________________________________________________________________
  ;
  ; Description:
  ; 
  ; Calculate the allowance value using the position of the neutral phase
  ; calculated with a given constant K-factor. This K-factor is searched as
  ; attribute (column :K_FACTOR) of the material (if given) and if not found
  ; there by looking for an :K_FACTOR column at the shop description. If that
  ; also is missing it uses a hard coded K_factor value (here set to be 40% of
  ; sheet thickness counted from inner radius, which is 0.4 in ANSI notation
  ; and 0.8 in DIN notation)
  ;
  ; NOTE:  This function can also directly be used as fallback formula 
  ;        (then feeded by the first three parameters only)
  ;
  ; INPUT:
  ; ======
  ; Sheet_thickness [mm]  Thickness of sheet material
  ; bend_angle     [deg]  Bend angle 0..180. 0 = no bend
  ; bend_radius     [mm]  Inner radius of bending zone
  ; sheet_material        material property list
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ;
  ; RETURN
  ; ======
  ; property list ( :allowance allowance-value         ; if not given the
  ;                                                    ; system uses a 
  ;                                                    ; fallback strategy
  ;                 :k-factor k-factor                 ; optional: k-factor as an alternative to allowance  
  ;                                                    ; must for conical bending  
  ;                 :warning-headline message-string   ; optional
  ;                 :error-type violation-type         ; optional
  ;                 :error-message message_string )    ; optional
  ;____________________________________________________________________________


  (let  ( K_factor
          return_values)

    ; seek K_factor at material, if not found there seek K_factor as shop entry
    ; if not found there take default K_factor 0.4
    (unless (setf K_factor (sha-get-db-entry sheet_material :K_FACTOR))
       (unless (setf K_factor (sha-get-shop-entry 
                                     :shopname (sha-get-current-shop)
                                     :column   :K_FACTOR))
          (setf K_factor 0.4)
       )
    )
 
  ; Calculate bend allowance and set global return value  
  ; Pass open angle between bent lips instead bend angle 

  (append return_values (list :allowance 
                                (Sh_calculate_bend_allowance_ANSI sheet_thickness 
                                                                  bend_radius 
                                                                  (- 180 bend_angle) 
                                                                  K_factor)
                                :k-factor K_factor))

  );end let
)

;-------------- ALTERNATIVE 3: interpolation of allowance table ----------------

(defun linear_interp (&key sheet_thickness bend_angle bend_radius 
                           sheet_material tool_id enquire-key
                      &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; Description:
  ; 
  ; Interpolates an allowance table (which must have been named in the         
  ; :ALLOWANCE_TABLE column of the given tool) to derive the appropriate
  ; enquire value (by default the allowance value).
  ; The interpolation is linear between the closest lower
  ; and upper bend angle values.
  ;        
  ; Can also be used to interpolate a tool independent allowance table e.g.
  ; as needed for a fall-back allowance formula. In this case the parameter
  ; tool_id is not given (= set to nil from LISP). The tool key will then be
  ; set to :RADIUS only. The needed allowance table will be searched under
  ; the :ALLOWANCE_TABLE entry of the shop data.
  ;
  ; ATTENTION: 
  ;
  ; To avoid unexpected results of extrapolations, this algorithm 
  ; assumes the highest bend angle in the allowance table
  ; being the highest  p o s s i b l e bend angles.
  ; The lowest angle is assumed to be 0 with allowance 0 (no bending).
  ; For other enquiries than allowance, the smalles return value is always the
  ; the smalles value found in table
  ;
  ; INPUT:
  ; ======
  ; sheet_thickness [mm]   Thickness of sheet material
  ; bend_angle      [deg]  Bend angle 0..180. 0 = no bend
  ; bend_radius     [mm]   Inner radius of bending zone
  ; sheet_material     Material property list:
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals"
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ;                   if not given (= nil) the :ROWKEY will be set to
  ;                      (:ROWKEY bend_radius)
  ; enquire-key    [:allowance] enquire key, default: seek for :allowance
  ;
  ; RETURN
  ; ======
  ; property list ( :allowance allowance-value         ; if not given the
  ;                                                    ; system uses a 
  ;                                                    ; fallback strategy
  ;                 :enquire-value                     ; enquired value if
  ;                                                    ; this is NOT allowance
  ;                 :warning-headline message-string   ; optional
  ;                 :error-type violation-type         ; optional
  ;                 :error-message message_string )    ; optional
  ;___________________________________________________________________________

   (declare (ignore sheet_thickness))
   (let* (
          (shopname           (or (getf tool_id :SHOPNAME) (getf sheet_material :SHOPNAME)))
          (tool-rowkey        (or (copy-list (getf tool_id :rowkey))
                                 ; if used as fallback formula (no tool_id)
                                   (list :RADIUS bend_radius)))
          (dummy-remf         (remf tool-rowkey :ANGLE)) ; destructive on tool-rowkey
          (allowance-table    (if tool_id
                                (let ((allow-table (sha-get-db-entry tool_id :ALLOWANCE_TABLE)))
                                  (if (sd-logical-table-p (shoptabname shopname allow-table)) allow-table))
                                 ; if used as fallback formula (no tool_id)
                                (let ((allow-table (sha-get-shop-entry :shopname shopname :column :ALLOWANCE_TABLE)))
                                  (if (sd-logical-table-p (shoptabname shopname allow-table)) allow-table))))
          (material-rowkey    (getf sheet_material :rowkey))
          (key-plist          (append material-rowkey tool-rowkey))
          (bend-angle         (sd-deg-to-rad bend_angle)) ; convert to internal units
          (next-smaller-angle 0)
          (next-smaller-allowance 0) 
          next-greater-angle
          next-greater-allowance
          actual-allowance
          actual-angle
          tool-found 
         )

     (declare (ignore dummy-remf)) ; only to avoid compiler warnings
     (if allowance-table
       (let* ((allow-rows (sd-read-logical-table-row 
                                 (shoptabname shopname allowance-table)
                                    :pList key-pList
                                    :units :internal
                                    :allowmultirows t))
                )
         (if allow-rows
           (dolist (allow-row allow-rows)
             (setf tool-found t)
             (setf actual-allowance (getf allow-row (or enquire-key
                                                        :ALLOWANCE)))
             (setf actual-angle     (getf allow-row :ANGLE))
             (when (or (and (not next-smaller-angle)
                                (< actual-angle (+  bend-angle 
                                                    *sha-angle-data-eps*)))
                       (and next-smaller-angle
                            (> actual-angle next-smaller-angle)
                            (< actual-angle (+  bend-angle 
                                                *sha-angle-data-eps*))))
                 (setf next-smaller-angle actual-angle)
                 (setf next-smaller-allowance actual-allowance)
             )
             (when (or (and (not next-greater-angle)
                            (> actual-angle (- bend-angle 
                                               *sha-angle-data-eps*)))
                       (and next-greater-angle
                            (< actual-angle next-greater-angle)
                            (> actual-angle (- bend-angle
                                               *sha-angle-data-eps*))))
               (setf next-greater-angle actual-angle)
               (setf next-greater-allowance actual-allowance)
             )
           ); end dolist

           ; no fitting entries found
           (return-from linear_interp
             (list :warning-headline 
                   "WARNUNG:   Material nicht in Einzugswerttabelle gefunden;   deshalb wird Standardstrategie verwendet."
                   :error-type :tdb-entry-missing
                   :error-message
                   (fix_mesg "WARNUNG: Material {1} nicht in Einzugswerttabelle \"{2}\" gefunden.

Deshalb wird Standardstrategie verwendet, um Einzugswert zu erhalten."
                             (sha-rowkey-plist-to-generalstring shopname allowance-table material-rowkey) allowance-table)
             )
           )
         )

         (if (and next-greater-allowance
                  next-smaller-allowance)
         ; relevant entries found
           (progn
             (if (< (abs  (- next-greater-angle next-smaller-angle))
                    *sha-angle-data-eps*)
               (list (or enquire-key :ALLOWANCE) next-smaller-allowance)
               (list (or enquire-key :ALLOWANCE) (+ next-smaller-allowance 
                                   (* (- next-greater-allowance
                                         next-smaller-allowance)
                                      (/ (- bend-angle
                                            next-smaller-angle)
                                         (- next-greater-angle
                                            next-smaller-angle)))))
             )
           )
         ; no entries found or bend-angle outside range
           (list :warning-headline 
                   "WARNUNG: Winkel ungültig; deshalb wird Standardstrategie verwendet."
                   :error-type :tdb-tool-missing
                   :error-message
                   "Winkel liegt außerhalb des gültigen Bereichs der Einzugswerttabelle."
               
           )
         )
       )
       ;no allowance table found
       (list :warning-headline 
                "WARNUNG: Einzugswerttabelle fehlt."
               :error-type :tdb-table-missing
               :error-message
                 (fix_mesg "WARNUNG: Einzugswerttabelle \"{1}\" für Interpolationsformel fehlt." allowance-table)
       )
     )
   )
)

; special allowance (fallback) calculation when special bends (bends including allowance values) are used

(defun allowance_incl_fallback (&key sheet_thickness bend_angle bend_radius 
                                     sheet_material tool_id 
                                &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; Description:
  ; 
  ; Use direct acces to allowance table. If entry not found call fallback
  ; formula directly. 
  ; The bend process table must list an :ALLOWANCE_TABLE column
  ; to the regular allowance table.
  ;        
  ; INPUT:
  ; ======
  ; sheet_thickness  [mm]  Thickness of sheet material
  ; bend_angle      [deg]  Bend angle 0..180. 0 = no bend
  ; bend_radius      [mm]  Inner radius of bending zone
  ; sheet_material     Material property list:
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals"
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ;                   if not given (= nil) the :ROWKEY will be set to
  ;                      (:ROWKEY bend_radius)
  ;
  ; RETURN
  ; ======
  ; property list ( :allowance allowance-value         ; if not given the
  ;                                                    ; system uses a 
  ;                                                    ; fallback strategy
  ;                 :k-factor k-factor                 ; optional: k-factor as an alternative to allowance  
  ;                                                    ; must for conical bending  
  ;                 :warning-headline message-string   ; optional
  ;                 :error-type violation-type         ; optional
  ;                 :error-message message_string )    ; optional
  ;___________________________________________________________________________

   (let* ((shopname           (or (getf tool_id :SHOPNAME) (getf sheet_material :SHOPNAME)))
          (allowance-table    (sha-get-db-entry tool_id :ALLOWANCE_TABLE))
          (material-rowkey    (getf sheet_material :rowkey))
          (bend-angle-rad     (sd-deg-to-rad bend_angle)) ; convert to internal units
          (given-allowance     (getf (getf tool_id :ROWKEY) :ALLOWANCE)) 
         ;(tool-angle          (getf (getf tool_id :ROWKEY) :ANGLE)) 
          (tool-rowkey        (copy-list (getf tool_id :rowkey)))
                              ; attention: copy-list needed to avoid overwrite
                              ; of original value with following (setf (getf)..)
          (allowance-rowkey   (append  material-rowkey tool-rowkey))
           allowance-id
           allowance
          (material-shop      (getf sheet_material :shopname))
          special-allowance
         )
     ; check for special bend with given allowance
     (if given-allowance
       (if (and (EQUAL material-shop (sha-get-current-shop))
                (sd-num-equal-p sheet_thickness (getf (getf tool_id :ROWKEY) :THICK))
                (EQUAL (getf (getf sheet_material :ROWKEY) :MATERIAL) (getf (getf tool_id :ROWKEY) :MATERIAL))
                (sd-num-equal-p bend-angle-rad  (getf (getf tool_id :ROWKEY) :ANGLE))
                (sd-num-equal-p bend_radius (getf (getf tool_id :ROWKEY) :RADIUS))
                (and given-allowance (sd-num-equal-p given-allowance (getf (getf tool_id :ROWKEY) :ALLOWANCE)))
           )
          (return-from allowance_incl_fallback (list :ALLOWANCE given-allowance :ALLOWANCE-TYPE 0
                                                     :TOOL-KEY-PLIST tool_id ))
          (progn
            (setf (getf special-allowance :error-message)
            "WARNUNG: Spezialbiegung: Ein Parameter (RADIUS, WINKEL, MATERIAL, DICKE, EINZUGSWERT) wurde geändert. Formel für den Standardeinzugswert wird verwendet."
)
            (setf (getf special-allowance :error-type) :tdb-tool-missing)
          )
        )
      )
         
     ; add implicit :ANGLE column
     (setf (getf allowance-rowkey :ANGLE) bend-angle-rad)
     (setf allowance-id   (list :SHOPNAME shopname :TABNAME allowance-table
                               :ROWKEY allowance-rowkey))

     ; 2nd attempt: use interpolation formula
     (setf allowance (linear_interp :sheet_thickness sheet_thickness 
                                    :bend_radius     bend_radius  
                                    :bend_angle      bend_angle
                                    :sheet_material  sheet_material
                                    :tool_id         tool_id)
	)
	
     (if (numberp (getf allowance :allowance))
       (return-from allowance_incl_fallback allowance)
       ; final fallback
       (setf allowance (air_bend_allow  :sheet_thickness sheet_thickness
                                        :bend_radius     bend_radius
                                        :bend_angle      bend_angle
                                        :sheet_material  sheet_material
                                        :tool_id         tool_id)
       )
     )

     (setf (getf allowance :warning-headline) 
           "WARNING:   Tool not found in allowance table.   Using fallback strategy")
     (setf (getf allowance :error-type ) :tdb-entry-missing)
     (when special-allowance 
        (setf (getf allowance :error-message) (getf special-allowance :error-message))
        (setf (getf allowance :error-type)    (getf special-allowance :error-type))
     )
     allowance
   )
)

;=============== sub functions for allowance calculation ======================

(defun Sh_calculate_bend_allowance_DIN(sheet_thickness bend_radius Open_angle
                                       K_factor)
  ;_________________________________________________________________________
  ;
  ; Description:
  ;
  ; Calculate the bend allowance by using the given K_factor as position of
  ; the neutral phase of the bend. For geometrical explanation see Chapter
  ; "Allowance tables and formulas" within online help.
  ; For bend angles > 180 degree, i.e. open angles < 0 (roll bending) the DIN 
  ; formula is extended to return a bend allowance which includes the 
  ; elongation for the part of the bend greater than 180 degrees.
  ;
  ; INPUT
  ; =====
  ;
  ; sheet_thickness [mm]
  ; bend_radius     [mm]
  ; Open_angle      [deg]  { Angle between bent lips 0..180. }
  ;                        { 180 = no bend }
  ;
  ; K_factor  The K_factor is a unit free value which describes the position
  ;           of the neutral phase of the material in the bending zone.
  ;           The K_factor is defined in DIN to be the distance from the
  ;           inner sheet surface (in the bending zone) to the neutral phase
  ;           divided by half of the sheet thickness. That means it starts with
  ;           0 at the inner sheet surface end normally ends with 1 at the
  ;           ideal neutral phase in the middle of the material.
  ;  
  ;           ATTENTION: ANSI defines the K_factor relative to the full sheet
  ;                      thickness. That means:
  ;                             K_factor_DIN    = 2 * K_factor_ANSI          
  ;                 see: function Sh_calculate_bend_allowance_ANSI
  ; RETURN
  ; ======
  ; allowance      [mm]  allowance value
  ;__________________________________________________________________________


  (let (Sh_actual_bend_allowance)

    (cond  ; DIN sets the allowance to 0 for open-angle >165 deg.
           ; We leave it here with the regular formula to avoid the 
           ; small step in the formula.
           ; If you want to have the original DIN settings, out comment the
           ; following two lines.
         ;((> Open_angle 165) 
         ;)
          ((> Open_angle 90)
           (setf  Sh_actual_bend_allowance 
                      (- (* (/ (* PI (- 180 Open_angle)) 180)
                            (+ bend_radius (/ (* sheet_thickness K_factor) 2)))
                         (* 2 (+ bend_radius sheet_thickness) 
                            (tan (/ (* PI (/ (- 180 Open_angle) 2)) 180)))))
          )
          ((or (> Open_angle 20)
               (> bend_radius sheet_thickness) ; no hem
               (< Open_angle 0))               ; bend angles > 180 
            (setf  Sh_actual_bend_allowance 
                      (- (* (/ (* PI (- 180 Open_angle)) 180)
                            (+ bend_radius (/ (* sheet_thickness K_factor) 2)))
                         (* 2 (+ bend_radius sheet_thickness))))
          )
          (t ; (Open_angle near 0 deg and radius smaller than thickness = hem)
             ; uses experimental values from mild steel 
            (setf  Sh_actual_bend_allowance (* (- sheet_thickness 1) -0.46))
          )
    )
  )
)

(defun Sh_calculate_bend_allowance_ANSI(sheet_thickness bend_radius Open_angle
                                        K_factor)
  
  ;_________________________________________________________________________
  ;
  ; Description:
  ;
  ; Calculate the bend allowance by using the given K_factor as position of
  ; the neutral phase of the bend. For geometrical explanation see Chapter
  ; "Allowance tables and formulas" within online help.
  ;
  ; INPUT
  ; =====
  ;
  ; sheet_thickness [mm]
  ; bend_radius     [mm]
  ; Open_angle      [deg]  { Angle between bent lips 0..180. }
  ;                        { 180 = no bend }
  ;
  ; K_factor  The K_factor is a unit free value which describes the position
  ;           of the neutral phase of the material in the bending zone.
  ;           The K_factor is defined in ANSI to be the distance from the
  ;           inner sheet surface (in the bending zone) to the neutral phase
  ;           divided by the sheet thickness. That means it starts with
  ;           0 at the inner sheet surface end normally ends with 0.5 at the
  ;           ideal neutral phase in the middle of the material.
  ;  
  ;           ATTENTION: DIN defines the K_factor relative to the half sheet
  ;                      thickness. That means:
  ;                             K_factor_ANSI  = 0.5 * K_factor_DIN          
  ;                 see: function Sh_calculate_bend_allowance_DIN
  ; RETURN
  ; ======
  ; allowance      [mm]  allowance value
  ;__________________________________________________________________________


  (let (Sh_actual_bend_allowance)

    (cond  ; DIN sets the allowance to 0 for open-angle >165 deg.
           ; We leave it here with the regular formula to avoid the 
           ; small step in the formula.
           ; If you want to have the original DIN settings, out comment the
           ; following two lines.
         ;((> Open_angle 165)
         ; (setf  Sh_actual_bend_allowance 0)
         ;)
          ((> Open_angle 90)
           (setf  Sh_actual_bend_allowance 
                      (- (* (/ (* PI (- 180 Open_angle)) 180)
                            (+ bend_radius (* sheet_thickness K_factor)))
                         (* 2 (+ bend_radius sheet_thickness) 
                            (tan (/ (* PI (/ (- 180 Open_angle) 2)) 180)))))
          )
          (t ; Open_angle <= 90, including case of bend angle > 180 degree. 
            (setf  Sh_actual_bend_allowance 
                      (- (* (/ (* PI (- 180 Open_angle)) 180)
                            (+ bend_radius (* sheet_thickness K_factor)))
                         (* 2 (+ bend_radius sheet_thickness))))
          )
    )
  )
)

;==================== 3. Offset allowance formulas ============================;
;                                                                              ;
; To be used as fallback entry in the ::OFFSET_FORMULA column of the shop      ;
; definition.                                                                  ;
;==============================================================================;

(defun sh_offset_allowance(&key sheet_thickness offset_height 
                                sheet_material tool_id
                           &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; The following three parameters are added by the GENERATE_FLAT command  
  ; when it asks for an offset allowance.                                 
  ; These parameters can not be modified, {units} are fixed to mm         
  ;                                                                     
  ; Sheet_thickness  [mm] Material thickness                          
  ; offset_height    [mm] height of offset (0 = no offset )          
  ; sheet_material        material property list
  ;                        syntax:
  ;                         (:shopname <shopname> :tabname <sheet mat. table>
  ;                          :rowkey <material key property list>)
  ;                        syntax e.g.
  ;                         (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                          :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ;
  ; RETURN
  ; ======
  ; property list ( :allowance allowance-value         ; must
  ;                 :warning-headline message-string   ; optional
  ;                 :error-type violation-type         ; optional
  ;                 :error-message message_string)     ; optional
  ;___________________________________________________________________________

   (declare (ignore sheet_material)) ; to keep compiler quite until it's used

  ;; The formula used here is based on the demoshop data for offset
  ;; allowances. It does not distinguish between material types (e.g. Steel vs.
  ;; Aluminum). The formula represents a cubic curve for Allowance values
  ;; vs. Offset height until the Offset height is equal 2 times the thickness
  ;; of the sheet material. For larger offset_height values the Allowance is
  ;; equal to the offset_height plus a certain constant.
  ;; The error of the formula compared to the demoshop data is for nearly all
  ;; cases below 0.1 mm.

  (let* ((given-allowance (getf (getf tool_id :ROWKEY) :ALLOWANCE)) ; known allowance
         (a (/ 1 92 sheet_thickness sheet_thickness))
         (b (/ 5 23 sheet_thickness))
         (c (/ -24 23))
        )
    (if given-allowance
      ; return given allownace value
      (list :allowance given-allowance)
      (if (< offset_height (* 2 sheet_thickness))
        ; cubic curve 
        (list :allowance 
              (+ ( * a (expt offset_height 3)) (* b (expt offset_height 2))))
        ; linear (angle 45 deg)
        (list :allowance
              (+ offset_height (* sheet_thickness c)))
      )
    )
  )
)

;==================== 3a. Hem allowance formulas ============================;
;                                                                              ;
; To be used as fallback entry in the ::HEM_FORMULA column of the shop      ;
; definition.                                                                  ;
;==============================================================================;

(defun sh_hem_allowance(&key sheet_thickness hem_dist 
                             sheet_material tool_id
                           &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; The following three parameters are added by the GENERATE_FLAT command  
  ; when it asks for an hem allowance.                                 
  ; These parameters can not be modified, {units} are fixed to mm         
  ;                                                                     
  ; Sheet_thickness  [mm] Material thickness                          
  ; hem_dist         [mm] inner height of hem (must be greater 0)          
  ; sheet_material        material property list
  ;                        syntax:
  ;                         (:shopname <shopname> :tabname <sheet mat. table>
  ;                          :rowkey <material key property list>)
  ;                        syntax e.g.
  ;                         (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                          :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "hems" 
  ;                       :ROWKEY (:HEM_DIST 1))
  ;
  ; RETURN
  ; ======
  ; property list ( :allowance allowance-value         ; must
  ;                 :warning-headline message-string   ; optional
  ;                 :error-type violation-type         ; optional
  ;                 :error-message message_string)     ; optional
  ;___________________________________________________________________________

  ;; The formula uses either the given allowance value in case of a special hem or 
  ;; it relays on the regular din formula, whihc has been extended to deal with
  ;; hems

  (let* ((given-allowance (getf (getf tool_id :ROWKEY) :ALLOWANCE))) ; known allowance
    (if given-allowance
      ; return given allownace value
      (list :allowance given-allowance)
      (sh_bend_allowance_din :SHEET_THICKNESS sheet_thickness
                             :BEND_ANGLE 180
                             :BEND_RADIUS (/ hem_dist 2.0)
                             :SHEET_MATERIAL sheet_material 
                             :TOOL_ID tool_id
      )
    )
  )
)

;==================== K-Factor based bend allowance ===========================;
;                                                                              ;
; To be used for roll bending or in case a radius dependent k-factor should    ;
; be used instead of a bend allowance.                                         ;
; The function is mainly intended to handle bends > 180 degrees as for         ;
; those bends the standard bend allowance calculation is undefined.            ;
;                                                                              ;
;==============================================================================;
(defun roll_bending_allow_din (&key sheet_thickness bend_angle bend_radius 
                                    sheet_material tool_id 
                                    &allow-other-keys)
   ;___________________________________________________________________________
   ;
   ; Description:
   ; ------------
   ; As the name suggests this function is intended to be used as allowance 
   ; formula for the roll-bending process.
   ; It supports the DIN standard of defining the k-factor, between 0 and 2
   ;
   ; Parameters:
   ; -----------
   ; The following five parameters are added by the GENERATE_FLAT, ADD_SHEET 
   ; ADD_PROFILE , NEW_PROFILE ,SHA_INQUIRE_BEND and the SHA_UNFOLD  commands,
   ; when they ask for a specific bend allowance calling this function.
   ;
   ; These parameters can not be modified, {units} are fixed to mm and deg     
   ;
   ; INPUT
   ; =====
   ; sheet_thickness   [mm]  Material thickness                            
   ; bend_angle        [deg] bend angle of bend  (0 = no bend )          
   ; bend_radius       [mm]  bend radius of bend  (0 = no bend )           
   ; sheet_material    Material property list :                           
   ;                     syntax:
   ;                      (:shopname <shopname> :tabname <sheet mat. table>
   ;                       :rowkey <material key property list>)
   ;                     syntax e.g.
   ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
   ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
   ; tool_id           Tool property list:                                
   ;                     syntax:
   ;                      (:shopname <shopname> :tabname <tool table name> 
   ;                       :rowkey <tool key property list>)
   ;                     syntax e.g.
   ;                      (:SHOPNAME "demoshop" :TABNAME "roll_bending" 
   ;                       :ROWKEY ( :RADIUS 10 :MATERIAL "UST 1203" 
   ;                                 :THICK 1.25 :K_FACTOR 0.5))
   ;
   ;
   ; RETURN
   ; ======
   ; property list ( :allowance allowance-value         ; if not given the
   ;                                                    ; system uses a 
   ;                                                    ; fallback strategy
   ;                 :k-factor k-factor                 ; optional: k-factor as an alternative to allowance  
   ;                                                    ; must for conical bending
   ;                 :warning-headline message-string   ; optional
   ;                 :error-type violation-type         ; optional
   ;                 :error-message message_string )    ; optional
   ;___________________________________________________________________________

  (let ((K_factor (getf (getf tool_id :ROWKEY) :K_FACTOR))
        (return_values nil)
       )

    (unless K_factor
      (unless (getf tool_id :ROWKEY)
        (let* ((shopname (or (getf tool_id :SHOPNAME) (getf sheet_material :SHOPNAME)))
               (tabname  (shoptabname shopname (getf tool_id :tabname)))
               (rows (sd-read-logical-table-row tabname 
                                                :pList (getf sheet_material :rowkey)
                                                :units :internal
                                                :allowmultirows t))
              )
        (if rows
          (if (= (length rows) 1)
            (setq K_factor (getf (first rows) :K_FACTOR))
            (let ((next_smaller_radius   nil)
                  (next_greater_radius   nil)
                  (next_smaller_k_factor nil)
                  (next_greater_k_factor nil))
              (dolist (row rows)
                (let ((rad (getf row :RADIUS)))
                  (when (or (< rad bend_radius) (tolequal rad bend_radius))
                    (when (or (not next_smaller_radius) (> rad next_smaller_radius))
                      (setq next_smaller_radius rad)
                      (setq next_smaller_k_factor (getf row :K_FACTOR))
                    )
                  )
                  (when (or (> rad bend_radius) (tolequal rad bend_radius))
                    (when (or (not next_greater_radius) (< rad next_greater_radius))
                      (setq next_greater_radius rad)
                      (setq next_greater_k_factor (getf row :K_FACTOR))
                    )
                  )
                )
              )

                ;; Only interpolation, no extrapolation for radius beyond 
                ;; minimum and maximum entry in bend process table 
                (if (and next_smaller_k_factor next_greater_k_factor)
                  (if (< (abs  (- next_greater_radius next_smaller_radius))
                         *sha-length-data-eps*)
                    (setq K_factor next_smaller_k_factor)
                    (setq K_factor (+ next_smaller_k_factor
                                      (* (- next_greater_k_factor
                                            next_smaller_k_factor)
                                         (/ (- bend_radius         next_smaller_radius)
                                            (- next_greater_radius next_smaller_radius)))))
                  )
                  (setf return_values (append return_values
                                              (list :warning-headline 
                                                    "WARNUNG: Radius ungültig; deshalb wird Standardstrategie verwendet."
                                                    :error-type :tdb-tool-missing
                                                    :error-message
                                                    (fix_mesg "Radius außerh. des gült. Bereichs in Biegeproz.-tab. \"{1}\".
Std-Strategie für K-Faktor  verw."
                                                              tabname))))
                )
              )
            )
            ;; no rows found
          (setf return_values (append return_values
                                      (list :warning-headline 
                                            "WARNUNG: Material nicht in Biegeprozesstabelle; Standardstrategie verwenden"
                                            :error-type :tdb-entry-missing
                                            :error-message
                                            (fix_mesg "WARNUNG: Material {1} nicht in Prozesstabelle \"{2}\".
Standardstrategie für K-Faktor verwenden."
                                                      (sha-rowkey-plist-to-generalstring shopname tabname (getf sheet_material :rowkey)) tabname))))
          )
        )
      )
	)

    ;; fallback strategy to get k-factor.
    (unless K_factor
      (unless (setf K_factor (sha-get-db-entry sheet_material :K_FACTOR))
        (unless (setf K_factor (sha-get-shop-entry 
                                :shopname (sha-get-current-shop)
                                :column   :K_FACTOR))
          (setf K_factor 0.8)
        )
      )
    )

    (append return_values (list :allowance 
                                (Sh_calculate_bend_allowance_DIN sheet_thickness
                                                                 bend_radius 
                                                                 (- 180 bend_angle)
                                                                                                                                  K_factor)
                                :k-factor K_factor))
  )
)

;==================== Cone bending K-Factor ===================================;
;                                                                              ;
; Function for interpolating k-factor for conical bends                        ;
; For cones the specified function must return a k-factor                      ;
;                                                                              ;
;==============================================================================;
(defun cone_bending_allow_din (&key sheet_thickness bend_angle cone_angle  
                                    sheet_material tool_id 
                                    &allow-other-keys)
   ;___________________________________________________________________________
   ;
   ; Description:
   ; ------------
   ; As the name suggests this function is intended to be used as allowance 
   ; formula for the roll-bending process.
   ; It supports the DIN standard of defining the k-factor, between 0 and 2
   ;
   ; Parameters:
   ; -----------
   ; The following five parameters are added by the GENERATE_FLAT, ADD_SHEET 
   ; ADD_PROFILE , NEW_PROFILE ,SHA_INQUIRE_BEND and the SHA_UNFOLD  commands,
   ; when they ask for a specific bend allowance calling this function.
   ;
   ; These parameters can not be modified, {units} are fixed to mm and deg     
   ;
   ; INPUT
   ; =====
   ; sheet_thickness   [mm]  Material thickness                            
   ; bend_angle        [deg] bend angle of bend  (0 = no bend )          
   ; cone_angle        [deg] cone angle of bend  (0 = no bend )           
   ; sheet_material    Material property list :                           
   ;                     syntax:
   ;                      (:shopname <shopname> :tabname <sheet mat. table>
   ;                       :rowkey <material key property list>)
   ;                     syntax e.g.
   ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
   ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
   ; tool_id           Tool property list:                                
   ;                     syntax:
   ;                      (:shopname <shopname> :tabname <tool table name> 
   ;                       :rowkey <tool key property list>)
   ;                     syntax e.g.
   ;                      (:SHOPNAME "demoshop" :TABNAME "roll_bending" 
   ;                       :ROWKEY ( :CONE_ANGLE 24 :MATERIAL "UST 1203" 
   ;                                 :THICK 1.25 :K_FACTOR 0.5))
   ;
   ;
   ; RETURN
   ; ======
   ; property list ( :allowance allowance-value         ; if not given the
   ;                                                    ; system uses a 
   ;                                                    ; fallback strategy
   ;                 :k-factor k-factor                 ; optional: k-factor as an alternative to allowance  
   ;                                                    ; must for conical bending
   ;                 :warning-headline message-string   ; optional
   ;                 :error-type violation-type         ; optional
   ;                 :error-message message_string )    ; optional
   ;___________________________________________________________________________

  (let ((K_factor (getf (getf tool_id :ROWKEY) :K_FACTOR))
        (return_values nil)
       )

    (unless K_factor
      (let* ((shopname (or (getf tool_id :SHOPNAME) (getf sheet_material :SHOPNAME)))
             (tabname  (shoptabname shopname (getf tool_id :tabname)))
             (rows (when (oli:sd-logical-table-p tabname) ;; check Sheet Metal tool
                     (sd-read-logical-table-row tabname 
                                                :pList (getf sheet_material :rowkey)
                                                :units :internal
                                                :allowmultirows t)))
            )
        (if rows
          (if (= (length rows) 1)
            (setq K_factor (getf (first rows) :K_FACTOR))
            (let ((next_smaller_angle   nil)
                  (next_greater_angle   nil)
                  (next_smaller_k_factor nil)
                  (next_greater_k_factor nil))
              (dolist (row rows)
                (let ((ang (getf row :CONE_ANGLE)))
                  (when (or (< ang cone_angle) (tolequal ang cone_angle))
                    (when (or (not next_smaller_angle) (> ang next_smaller_angle))
                      (setq next_smaller_angle ang)
                      (setq next_smaller_k_factor (getf row :K_FACTOR))
                    )
                  )
                  (when (or (> ang cone_angle) (tolequal ang cone_angle))
                    (when (or (not next_greater_angle) (< ang next_greater_angle))
                      (setq next_greater_angle ang)
                      (setq next_greater_k_factor (getf row :K_FACTOR))
                    )
                  )
                )
              )
              ;; Only interpolation, no extrapolation for angle beyond 
              ;; minimum and maximum entry in bend process table 
              (if (and next_smaller_k_factor next_greater_k_factor)
                (if (< (abs  (- next_greater_angle next_smaller_angle))
                       *sha-length-data-eps*)
                  (setq K_factor next_smaller_k_factor)
                  (setq K_factor (+ next_smaller_k_factor
                                    (* (- next_greater_k_factor
                                          next_smaller_k_factor)
                                       (/ (- cone_angle         next_smaller_angle)
                                          (- next_greater_angle next_smaller_angle)))))
                )
                (setf return_values 
                      (append return_values
                              (list :warning-headline 
                                    "WARNUNG: Kegelwinkel ungültig; Std-Strategie verw."
                                    :error-type :tdb-tool-missing
                                    :error-message
                                    (fix_mesg "Kegelradius lt. Biegeprozesstabelle \"{1}\" nicht gültig.
Std-Strat. für k-Faktor verw."
                                              tabname))))
              )
            )
          )
          ;; no rows found
          (setf return_values 
                (append return_values
                        (list :warning-headline 

						"WARNING: Material not found in bend process table. Using fallback strategy"
                              :error-type :tdb-entry-missing
                              :error-message
                              (fix_mesg "WARNUNG: Material {1} nicht in Prozesstabelle \"{2}\".
Std-Strat. für k-Faktor verw."
                                        (sha-rowkey-plist-to-generalstring shopname tabname (getf sheet_material :rowkey)) tabname))))
        )
      )
    )

    ;; fallback strategy to get k-factor.
    (unless K_factor
      (unless (setf K_factor (sha-get-db-entry sheet_material :K_FACTOR))
        (unless (setf K_factor (sha-get-shop-entry 
                                :shopname (sha-get-current-shop)
                                :column   :K_FACTOR))
          (setf K_factor 0.8)
        )
      )
    )

    (append return_values (list :allowance nil
                                :k-factor K_factor))
  )
)


;=============== 4. Advisor check functions ===================================
;                                                                              ;
; Following functions are used when the tool tables of the mentioned tools are ;
; build up and the :ADVICE column is filled.                                   ;
;==============================================================================;

;---- BEND TOOL checking

(defun bend-tool-check (&key tool_id sheet_material sheet_thickness bend_angle
                             lip_length bend_length (specific_check :all)
                        &allow-other-keys)

  ;___________________________________________________________________________
  ;
  ; Description:
  ; ------------
  ; Check the given bending tool for any inconsistencies with given or 
  ; inquired context. Here check: 
  ;     - bend_angle exists (if given)        (tool-column  :ANGLE)
  ;        or 
  ;     - bend_angle is in allowed range      (tool-column  :PISTON_ANG)
  ;     - minimum and maximum material thickness violation
  ;                                           (tool-column  :MIN_THICK)
  ;                                           (tool-column  :MAX_THICK)
  ;     - tool radius > minimum bend radius of material 
  ;                                           (material-column :MIN_BEND_RAD)
  ;     - bend_length <= maxmal bend length
  ;     - lip_length >= minimum lip length
  ;     - bend force <= maximum bend force
  ;
  ; Parameters:
  ; -----------
  ;
  ; INPUT
  ; =====
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness   Material thickness [mm]                           
  ; bend_angle        [deg] bend angle of bend  (0 = no bend )          
  ; lip_length        actual lip length
  ; bend_length       length of bend
  ; specific_check    :all  = check all
  ;                   :lip_length = check only lip length
  ;                   :bend-force = check only bend force
  ;
  ;  RETURN
  ;  ======        or
  ;  error-message | t  error message if a rule violation was found
  ;                     t = boolean true, if tool passed the check
  ;___________________________________________________________________________

  (when bend_angle
    (let ((table-angle (sha-get-db-entry tool_id :angle))
          (piston-angle  (rad-to-deg (sha-get-db-entry tool_id :PISTON_ANG)))
         )

    ;; Check fixed bend angle
      (if (and table-angle bend_angle (equal :all specific_check))
        (unless (tolequal bend_angle table-angle)
          (return-from bend-tool-check 
            "Winkel ist ungeeignet."
          )
        )
      )

    ;; Piston angle, limits maximal bending angle   
      (if (and piston-angle (equal :all specific_check))
       ; For mild Steel and  Sh_actual_bend_radius / Sheet_thickness < 4 
       ; the factor K indicating the relation between the final bend angle
       ; and the bend angle plus spring back angle is roughly: 0.98
       ; This value is normally dependent on the material quality and the ratio:
       ;                inner bend radius / sheet thickness
       ;  Any more exact formulas for this value should be introduced here.

       (let* ((K 0.98)      ; relation between the final bend angle and the
                            ; bend angle plus spring back angle
              (spring-back  (* (rad-to-deg bend_angle) (- (/ 1  K) 1)))
             )

       ; Check against maximum bend angle
         (when ( < (- 180 (rad-to-deg bend_angle)) (+ piston-angle spring-back))
           (return-from bend-tool-check
             (fix_mesg "Biegewinkel = {1}{2} ist größer als der maximale Biegewinkel = {3}{4} dieses Werkzeugs."
               (num-to-str (from_rad bend_angle) 1)
               (current-angle-unit-string)
               (num-to-str (from_rad 
                              (deg-to-rad (- 180 piston-angle spring-back)))
                            1)
               (current-angle-unit-string)))
         )
       )
      ) 
    )
  )
  
  ;; check for minimal bend radius
  (if (equal :all specific_check)
    (unless (between (sha-get-db-entry sheet_material :MIN_BEND_RAD)
                     (sha-get-db-entry tool_id :RADIUS)
                     nil)
      (return-from bend-tool-check 
       "Radius ist kleiner als der minimale Material-Biegeradius (min_bend_radius).")))

  ;; check for maximal bend length
  (if (and bend_length (or (equal :all specific_check)
                           (equal :bend-length specific_check)))
    (let ((check-result (check-tool-max-bend-length tool_id bend_length)))
      (unless (equal check-result t)
        (return-from bend-tool-check check-result)
      )
    )
  )

  ;; check for minimal lip length
  (if (and lip_length bend_angle (or (equal :all specific_check) 
                                     (equal :lip_length specific_check)))
    (let ((check-result (check-tool-min-lip-length tool_id bend_angle
                                                   lip_length)))
      (unless (equal check-result t)
        (return-from bend-tool-check check-result)
      )
    )
  )

  ;; check for maximal absolute bend force (machine) and
  ;; check for tool dependent maximal bend force per length
  (when (and bend_length (or (equal :all specific_check) 
                             (equal :bend-force specific_check)))
    (let ((check-result (check-tool-max-bend-force tool_id sheet_material
                           sheet_thickness bend_length bend_angle lip_length)))
      (unless (equal check-result t)
        (return-from bend-tool-check check-result)
      )
    )
  )

  (let ((tool_thickness (sha-get-db-entry tool_id :thick))
        (tool_mat_id (sha-get-db-entry tool_id :material))
        (mat_rowkey (getf sheet_material :rowkey))
        (mat_id nil)
       )
    (when mat_rowkey
      (setq mat_id (getf mat_rowkey :material))
    )
    (when (and tool_thickness tool_mat_id)
      (unless (and (tolequal tool_thickness sheet_thickness)
                   (equal tool_mat_id mat_id))
        (return-from bend-tool-check 
          "Material ist ungeeignet.")
      )
    )
  )

  ; check defined tool limits
  (check-tool-min-max-thick tool_id sheet_thickness)
)


;---- OFFSET TOOL checking

(defun offset-tool-check (&key tool_id sheet_material sheet_thickness
                               inner-radius outer-radius 
                          &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; Description:
  ; ------------
  ; Check the given offset tool for any inconsistencies with given or 
  ; inquired context, here check: 
  ;                      minimum and maximum material thickness violation
  ;
  ; Parameters:
  ; -----------
  ;
  ; INPUT
  ; =====
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "offsets" 
  ;                       :ROWKEY (:OFFSET_HEIGHT 2.0))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness  [mm]  Material thickness                            
  ; inner_radius     [mm] inner (smaller) radius of offset          
  ; outer_radius     [mm] outer (greater) radius of offset          
  ;
  ;  RETURN
  ;  ======        or
  ;  error-message | t  error message if a rule violation was found
  ;                     t = boolean true, if tool passed the check
  ;___________________________________________________________________________

   (declare (ignore sheet_material)
            (ignore inner-radius)
            (ignore outer-radius)) ; to keep compiler quite until it's used
   (check-tool-min-max-thick tool_id sheet_thickness))
    
;---- HEM TOOL checking

(defun hem-tool-check (&key tool_id sheet_material sheet_thickness
                            inner-radius
                            lip_length bend_length (specific_check :all)
                       &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; Description:
  ; ------------
  ; Check the given offset tool for any inconsistencies with given or 
  ; inquired context, here check: 
  ;                      minimum and maximum material thickness violation
  ;
  ; Parameters:
  ; -----------
  ;
  ; INPUT
  ; =====
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "hems" 
  ;                       :ROWKEY (:HEM_DIST 3.0))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness  [mm]  Material thickness                            
  ; inner_radius     [mm] inner (smaller) radius of hem
  ;
  ;  RETURN
  ;  ======
  ;  error-message - error message if a rule violation was found
  ;                  t = boolean true, if tool passed the check
  ;___________________________________________________________________________

   (declare (ignore inner-radius)) ; to keep compiler quite until it's used

  ;; check for maximal bend length
  (if (and bend_length (or (equal :all specific_check)
                           (equal :bend-length specific_check)))
    (let ((check-result (check-tool-max-bend-length tool_id bend_length)))
      (unless (equal check-result t)
        (return-from hem-tool-check check-result)
      )
    )
  )

  ;; check for minimal lip length
  (if (and lip_length (or (equal :all specific_check) 
                          (equal :lip_length specific_check)))
    (let ((check-result (check-tool-min-lip-length tool_id PI lip_length)))
      (unless (equal check-result t)
        (return-from hem-tool-check check-result)
      )
    )
  )

  ;; check for absolute maximal bend force (machine) and
  ;; check for tool dependent maximal bend force per length
  (when (and bend_length (or (equal :all specific_check) 
                             (equal :bend-force specific_check)))
    (let ((check-result (check-tool-max-bend-force tool_id sheet_material
                           sheet_thickness bend_length PI lip_length)))
      (unless (equal check-result t)
        (return-from hem-tool-check check-result)
      )
    )
  )

  ;; check min/max material thickness range
  (check-tool-min-max-thick tool_id sheet_thickness)
)

;---- Cone Bend TOOL checking

(defun cone-bend-tool-check (&key tool_id sheet_material sheet_thickness
                                  &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; Description:
  ; ------------
  ; Check the given cone bend tool for any inconsistencies with given or 
  ; inquired context, here check:
  ;                    * material fits to tool 
  ;                    * minimum and maximum material thickness violation
  ;                      To activate: add according columns to cone_bending table
  ;
  ; Parameters:
  ; -----------
  ;
  ; INPUT
  ; =====
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "hems" 
  ;                       :ROWKEY (:HEM_DIST 3.0))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness  [mm] Material thickness                            
  ;
  ;  RETURN
  ;  ======         
  ;  error-message - error message if a rule violation was found
  ;                  t = boolean true, if tool passed the check
  ;___________________________________________________________________________

   (let ((tool_thickness (sha-get-db-entry tool_id :thick))
         (tool_mat_id (sha-get-db-entry tool_id :material))
         (mat_rowkey (getf sheet_material :rowkey))
         (mat_id nil)
         )
     (when mat_rowkey
       (setq mat_id (getf mat_rowkey :material))
     )
     (when (and tool_thickness tool_mat_id)
       (unless (and (tolequal tool_thickness sheet_thickness)
                    (equal tool_mat_id mat_id))
         (return-from cone-bend-tool-check 
           "Material ist ungeeignet.")
       )
     )
   )

   ;; check min/max material thickness range
   (check-tool-min-max-thick tool_id sheet_thickness)
)

;---- CUT (PUNCH) TOOL checking

(defun cut-tool-check (&key tool_id sheet_material sheet_thickness
                       &allow-other-keys)
  ;___________________________________________________________________________
  ;
  ; Description:
  ; ------------
  ; Check the given cut (punch) tool for any inconsistencies with given or
  ; inquired context, here check:
  ;                      minimum and maximum material thickness violation
  ;
  ; Parameters:
  ; -----------
  ;
  ; INPUT
  ; =====
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "rnd" 
  ;                       :ROWKEY (:DIA 12.7))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness  [mm]  Material thickness                            
  ;
  ;  RETURN
  ;  ======        or
  ;  error-message | t  error message is a rule violation was found
  ;                     t = boolean true, if tool passed the check
  ;___________________________________________________________________________

  (declare (ignore sheet_material)) ; to keep compiler quite until it's used
  (check-tool-min-max-thick tool_id sheet_thickness))

;---- STAMP TOOL checking

(defun stamp-tool-check (&key tool_id sheet_material sheet_thickness
                         &allow-other-keys) 
  ;___________________________________________________________________________
  ;
  ; Description:
  ; ------------
  ; Check the given stamp tool for any inconsistencies with given or
  ; inquired context, here check:
  ;                      minimum and maximum material thickness violation
  ;
  ; Parameters:
  ; -----------
  ;
  ; INPUT
  ; =====
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "dmp" 
  ;                       :ROWKEY (:DIMPLE_DIA 11.1 :DEPTH 1.2 
  ;                                :ANG 1.5707963267948966 :STAMP_DIR "UP"))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness  [mm]  Material thickness                            
  ;
  ;  RETURN
  ;  ======        or
  ;  error-message | t  error message is a rule violation was found
  ;                     t = boolean true, if tool passed the check
  ;___________________________________________________________________________

  (declare (ignore sheet_material)) ; to keep compiler quite until it's used
  (check-tool-min-max-thick tool_id sheet_thickness))


;--------------- sub functions for tool checking ---------------------------

(defun check-tool-min-max-thick (tool-id sheet-thickness)
   (let ((min-thick (sha-get-db-entry tool-id :min_thick))
         (max-thick (sha-get-db-entry tool-id :max_thick)))
     (if (between min-thick sheet-thickness nil)
       (if (between nil sheet-thickness max-thick)
         t
         "Material ist dicker als maximal bearbeitbar."
       )
       "Material ist dünner als minimal bearbeitbar."
     )
  )
)

(defun check-tool-max-bend-length (tool-id bend-length)
  ;; check for maximal bend length
    (let ((max-bend-length (sha-get-db-entry tool-id :MAX_BEND_LENGTH)))
      (if (and max-bend-length (> bend-length max-bend-length))
          (fix_mesg "Biegelänge = {1}{2} ist größer als die maximale Biegelänge = {3}{4} dieses Biegewerkzeugs."
           (num-to-str (from_mm bend-length) 1)
           (current-length-unit-string)
           (num-to-str (from_mm max-bend-length) 1)
           (current-length-unit-string)
          )
          t
      )
    )
)

(defun check-tool-min-lip-length (tool-id bend-angle lip-length)
  ;; check for minimal lip length
  (let* 
    ((die-width (sha-get-db-entry tool-id :DIE_WIDTH))
     (min-lip-length (or (sha-get-db-entry tool-id :MIN_LIP_LENGTH)
                        (if (and die-width (NOT (sd-num-equal-p PI bend-angle)))
                          (/ (* 0.5 die-width) (cos (* 0.5 bend-angle))))))
    )
    (if (and min-lip-length (< lip-length min-lip-length))
      (fix_mesg "Laschenlänge = {1}{2} ist kleiner als die minimale Laschenlänge = {3}{4} dieses Biegewerkzeugs."
         (num-to-str (from_mm lip-length) 1)
         (current-length-unit-string)
         (num-to-str (from_mm min-lip-length) 1)
         (current-length-unit-string)
      )
      T
    )
  )
)

(defun check-tool-max-bend-force (tool-id sheet-material sheet-thickness 
                                  bend-length bend-angle lip-length)
  ;; check for absolute maximal bend force of machine
  ;; check for tool dependent maximal bend force per length
  ;; RETRUN T = no problem found
  ;;        string = error message string 

  (declare (ignore bend-angle)
           (ignore lip-length))
  (let
   ((max-bend-force (sha-get-db-entry tool-id :MAX_BEND_FORCE)) ; [N]
    (max-bend-force-per-length 
      (sha-get-db-entry tool-id :MAX_BEND_FORCE_PER_M)) ; [KN/m]
    (Ts (sha-get-db-entry sheet-material :TENSILE_STRENGTH)) ; [N/mm^2]
    (Dw (sha-get-db-entry tool-id :DIE_WIDTH))
    (Pr (sha-get-db-entry tool-id :PISTON_RAD))
    bend-force
    bend-force-per-length
   )
    (when (and max-bend-force Ts Dw Pr)
      (setf bend-force (/ (* 1.33 bend-length Ts sheet-thickness 
                             sheet-thickness)
                          (- Dw Pr)))
      (when (< max-bend-force bend-force)
        (return-from check-tool-max-bend-force
        (fix_mesg "Erforderliche Biegekraft = {1}{2} ist größer als die maximale Biegekraft = {3}{4} dieser Maschine."
           (num-to-str (frame2::from_N bend-force) 1)
           (current-force-unit-string)
           (num-to-str (frame2::from_N max-bend-force) 1)
           (current-force-unit-string)
        ))
      )
      (setf bend-force-per-length (/ bend-force bend-length))
      (when (< max-bend-force-per-length bend-force-per-length)
        (return-from check-tool-max-bend-force
          (fix_mesg "Biegekraft = {1}{2} ist größer als die maximale Biegekraft = {3}{4} dieses Werkzeugs."
           (num-to-str (frame2::from_N_per_mm bend-force-per-length) 1)
           (current-force-per-length-unit-string)
           (num-to-str (frame2::from_N_per_mm max-bend-force-per-length) 1)
           (current-force-per-length-unit-string)
          )
        )
      )
    )
    T
  )
)

;=============== 4a. Check functions to determine wether given tool is already used in actual part ======================
; 
(defun sha-punch-tool-already-used (&key tool_id 
                                    &allow-other-keys)
  ;_____________________________________________________________________________________________
  ;
  ; Description:
  ;
  ; This function is called whenever a punch tool is used, which holds a dynamic 
  ; column :USED with a call to this function. 
  ; The function then should determine whether the given tool is alrady used in the 
  ; actual part.
  ;
  ; The parameter of this routine is filled by the calling command SHA_PUNCH
  ; The parameter list is a "keyed" list to allow easy future
  ; extensions. This means: 
  ;   The names of the parameters MUST NOT be changed
  ;   The sequence does not matter.
  ;   The units of these parameters are fixed to mm and deg  
  ;
  ; INPUT
  ; =====
  ;
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ;
  ; further input parameters will be inquired by special functions:
  ;  sha-punch-get-selected-part = returns the actuallly selected part in the PUNCH command 
  ;  sha-get-used-punch-tools = returns a list of actually used punch-tools in the selected part 
  ;
  ; RETURN
  ; ======
  ; property-list: 
  ;       ( :USED " " | " <usage-count> "     ; must   
  ;         :error-type :dfm_warning          ; optional
  ;         :error-message <message string> ) ; optional
  ;_____________________________________________________________________________________________

   (let* ((part (sha-punch-get-selected-part)) ; get actaul selected part from PUNCH command
          (part-punches (sha-get-used-punch-tools part)) 
          (tool-count 0)
         )
     (dolist (one-tool part-punches)
       (if (marabou-ui::sha-tool-equal one-tool tool_id) (incf tool-count)))
     (if (equal tool-count 0)
       (list :USED " ")
       (list :USED (format nil "   ~A" tool-count))
     )
   )
)

(defun sha-stamp-tool-already-used (&key tool_id 
                                    &allow-other-keys)
  ;_____________________________________________________________________________________________
  ;
  ; Description:
  ;
  ; This function is called whenever a stamp tool is used, which holds a dynamic 
  ; column :USED with a call to this function. 
  ; The function then should determine whether the given tool is alrady used in the 
  ; actual part.
  ;
  ; The parameter of this routine is filled by the calling command SHA_STAMP.
  ; The parameter list is a "keyed" list to allow easy future
  ; extensions. This means: 
  ;   The names of the parameters MUST NOT be changed
  ;   The sequence does not matter.
  ;   The units of these parameters are fixed to mm and deg  
  ;
  ; INPUT
  ; =====
  ;
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ;
  ; further input parameters will be inquired by special functions:
  ;  sha-stamp-get-selected-part = returns the actuallly selected part in the STAMP command 
  ;  sha-get-used-stamp-tools = returns a list of actually used stamp-tools in the selected part 
  ;
  ; RETURN
  ; ======
  ; property-list: 
  ;       ( :USED " " | " <usage-count> "     ; must   
  ;         :error-type :dfm_warning          ; optional
  ;         :error-message <message string> ) ; optional
  ;_____________________________________________________________________________________________

   (let* ((part (sha-stamp-get-selected-part)) ; get actually selected part from STAMP command
          (part-stamps (sha-get-used-stamp-tools part)) 
          (tool-count 0)
         )
     (dolist (one-tool part-stamps)
       (if (marabou-ui::sha-tool-equal one-tool tool_id) (incf tool-count)))
     (if (equal tool-count 0)
       (list :USED " ")
       (list :USED (format nil "   ~A" tool-count))
     )
   )
)

;=============== 5. Radius formula for air-bending tools ======================
; 

(defun air_bend_rad (&key tool_id sheet_material sheet_thickness bend_angle
                     &allow-other-keys)
  ;______________________________________________________________________
  ;
  ; Description:
  ;
  ; This function is called by ADD_SHEET, ADD_PROFILE, NEW_PROFILE and 
  ; ADD_LIP commands, when they ask for a table of bend tools for the 
  ; air_bend_process. 
  ;
  ; The 5 parameters of this routine are filled by the calling commands
  ; listed above. The parameter list is a "keyed" list to allow easy future
  ; extensions. This means: 
  ;   The names of the parameters MUST NOT be changed
  ;   The sequence does not matter.
  ;   The units of these parameters are fixed to mm and deg  
  ;
  ; INPUT
  ; =====
  ;
  ; tool_id           Tool property list:                                
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <tool table name> 
  ;                       :rowkey <tool key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "air_bending" 
  ;                       :ROWKEY (:DIE_WIDTH 20.0 :PISTON_RAD 1.6
  ;                                :PISTON_ANG 0.78539816339744828))
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; sheet_thickness  [mm]  Material thickness
  ; Bend_angle       [rad] bend angle of bend  (0 = no bend )   
  ;
  ; RETURN
  ; ======
  ; property-list: 
  ;       ( :radius <radius value>            ; must  = nil if tool not valid
  ;         :error-type :dfm_warning          ; optional
  ;         :error-message <message string> ) ; optional
  ;
  ;----------------------------------------------------------------------
  ;
  ; Other additional parameters may be:
  ;         more die geometries (like die angle, border radii etc)
  ;         bending force limits
  ; They all should be checked for consistency and manufacturability within
  ; this macro
  ;
  ;  The other additional tool parameters are grabbed from the       
  ;  tool data base by this formula using the tool_id:
  ;  The units of theses parameters are in SD internal units: mm, rad and kg
  ;_________________________________________________________________________

   (declare (ignore sheet_material)
            (ignore sheet_thickness) 
            (ignore bend_angle)  ; to keep compiler quite until it's used
   ) 

   ; get further tool parameter
   ; The units of theses parameters are in SD internal units: mm, rad and kg

   (LET* (
          (Piston_radius      (or (sha-get-db-entry tool_id  :PISTON_RAD)   ; radius  of piston stamp
                                (sha-get-db-entry tool_id  :RADIUS)))
          (Die_width    (sha-get-db-entry tool_id :DIE_WIDTH))   ; relevant width of bending die
          ;  More explicit and material dependent formulas should be introduced here

          (Sh_actual_bend_radius (* 0.15 Die_width))
         )

   (if (and Piston_radius (< Sh_actual_bend_radius Piston_radius))
     (setf Sh_actual_bend_radius Piston_radius))

   (list :radius Sh_actual_bend_radius)
  )
)

;============== 6. Distance check formula ====================================
; Used for distance checks when cutting (punching) or stamping tools are
; placed on the sheet metal
;=============================================================================

(defun check-dist ( &key sheet_material sheet_thickness min_value factor
                    &allow-other-keys)
  
  ;__________________________________________________________________________
  ;
  ; Description:
  ;
  ; This function will be used to calculate the appropriate distance value
  ; which should be checked when a cut, stamp or bend feature is generated or
  ; moved. 
  ;
  ; The basic values of this distances are process dependent, and
  ; hold in the tool data base. The :CHECK-DISTANCES field in the shop
  ; definition should hold the name of this table. The table must hold a
  ; factor and a minimal distance value for each combination of two of
  ; the existing processes. These two values finally get passed to this 
  ; function, which allows to include a customer defined dependency on the
  ; actual material for the final check distance calculation.
  ;
  ; INPUT
  ; =====
  ; sheet_material    Material property list :                           
  ;                     syntax:
  ;                      (:shopname <shopname> :tabname <sheet mat. table>
  ;                       :rowkey <material key property list>)
  ;                     syntax e.g.
  ;                      (:SHOPNAME "demoshop" :TABNAME "sheet_metals" 
  ;                       :ROWKEY (:MATERIAL "UST 1203" :THICK 1.25))
  ; thickness  [mm] Material thickness   
  ; min_value  [mm] minimum distance value
  ; factor          distance factor 
  ;
  ; RETURN 
  ; ======
  ; check-distance [mm]
  ;___________________________________________________________________________

  (declare (ignore sheet_material)) ; to keep compiler quite until it's used
  (let ((dist (* factor sheet_thickness)))
    (if (< dist min_value)
      min_value
      dist
    )
  )
)

;============== Default settings formula =================================

;============== 7. Default relief tool formula =================================
; Used for automatic adaption of default relief tool to given material thickness
; The function will be called if it's name is listed beside the 
;  :DEFAULT-FUNCTION
; keyword in a relief tool table
;=============================================================================

(defun sha-default-relief-tool (tool-id relief-size-column 
                                         material-id)
  ;____________________________________________________________________________
  ;
  ; With input of a given default relief tool (derived from default settings)
  ; and the actual material, this function tries to find a valid relief tool 
  ; (of same type) with a size equal to given material dependent default-relief   ; size or (if that was not found) sheet-thickness or close to it.
  ; 
  ; INPUT: tool-id = Identifier of default relief tool (defined in 
  ;                  Sheet Metal settings)
  ;        relief-size-column = column keyword which defines the size of
  ;                             the relief tool (e.g. :VERT)
  ;        material-id = Identifier of material for which a tool should
  ;                      be found
  ;                   
  ; RETURN: new-tool = ID of new (MATERIAL dependent) default tool
  ;
  ;____________________________________________________________________________

  (let* (
         (new-default-tool-size (or (sha-get-db-entry MATERIAL-ID 
                                                      :DEFAULT-RELIEF-SIZE)
                                    (sha-get-db-entry MATERIAL-ID :THICK)
                                    (getf (getf material-id :ROWKEY) :THICK)))
         (logical-table (shoptabname (getf tool-id :SHOPNAME)
                                     (getf tool-id :TABNAME)))
         (next-smaller 0)
         (next-smaller-row 0)
         (next-greater 999999)
         (next-greater-row (- (sd-get-logical-table-number-of-rows
                               logical-table  ) 1))
         (tool-keys (sd-get-logical-table-key-column-identifiers 
                                                      logical-table))
         new-tool-row-number
         new-tool-row
         new-tool
       )

    (if new-default-tool-size  
      (progn
    (dotimes (rownumber (sd-get-logical-table-number-of-rows
                             logical-table  ))
       (let* ((tool-row (sd-read-logical-table-row logical-table
                                                  :row rownumber
                                                  :units :internal))
             (tool-size (getf tool-row relief-size-column))
            )

         ; RULE: check for equivalence with material thickness
         ;       and take the tool with equal or nearest to equal size

         (if (sd-num-equal-p new-default-tool-size tool-size)
            (progn 
              (setf new-tool-row-number rownumber)
              (return)
            )
            (progn
               (if (and (< tool-size new-default-tool-size)
                        (> tool-size next-smaller))
                  (progn
                    (setf next-smaller tool-size)
                    (setf next-smaller-row rownumber)
                  )
                  (when (and (> tool-size new-default-tool-size)
                             (< tool-size next-greater))
                    (setf next-greater tool-size)
                    (setf next-greater-row rownumber)
                  )
                )
            )
         )
       )
    )
 
    ; if identical tool NOT found take next best
    (unless new-tool-row-number
       (if (< (- new-default-tool-size next-smaller)
              (- next-greater new-default-tool-size))
          (setf new-tool-row-number next-smaller-row)
          (setf new-tool-row-number next-greater-row)))
        
    ; get row plist of new tool
    (setf new-tool-row (sd-read-logical-table-row logical-table
                                                  :row new-tool-row-number
                                                  :units :internal))
    ; make new tool key list
    (dolist (a-key tool-keys)
       (push a-key new-tool)
       (push (getf new-tool-row a-key) new-tool))
    
    (nreverse new-tool)
   )
   nil ; tool and material not found
 )
))

;============== 8. Default bend-radius tool formula ============================
; Used for automatic adaption of default bend radius to given material / 
; thickness / bend-process / lip-length / bend-length
; The function will be called if it's name is listed beside the 
;  :DEFAULT-FUNCTION
; keyword in a bend process tool table.
; Alternatively this function may be used as shopwide (not process spcific)
; default setting with:
; (sha-set-default-bend-radius 'sha-default-bend-radius)
; usually in the (default-settings "demoshop"  section of the shop definition
;=============================================================================

(defun sha-default-bend-radius (&key material bend-process bend-angle
                                     thickness lip-length bend-length
                                &allow-other-keys)
  ;____________________________________________________________________________
  ;
  ; With input of a given material or thickness and otionally further more 
  ; specific environment variables like bend-process bend-angle lip-length 
  ; and bend-length, this function calculates an appropriate bend radius and 
  ; tryes to find the best fitting radius in the tool table.
  ;
  ; INPUT: material     = material property list       <plist>
  ;        thickness    = material thickness           <number> [mm]
  ;        bend-process = name of bend process         <string> 
  ;        bend-angle   = bend-angle in internal units <number>[rad] (optional)
  ;        lip-length   = length of new lip            <number>[mm] (optional)
  ;        bend-length  = length of new bend           <number>[mm] (optional)
  ;                   
  ; RETURN: new-radius  = new bend tool default radius
  ;
  ;____________________________________________________________________________

  (declare (ignore material)
           (ignore bend-angle)
           (ignore lip-length)
           (ignore bend-length)  ; to keep compiler quite until it's used
  )
  (let* (
         (new-default-radius thickness) ; take sheet thickness as default size

         (logical-table (shoptabname (sha-get-current-shop) bend-process))
         (next-greater nil)
        )

    (if new-default-radius  
      (progn ; check for next greater available tool
      (dotimes (rownumber (sd-get-logical-table-number-of-rows
                               logical-table  ))
         (let* ((tool-row (sd-read-logical-table-row logical-table
                                                    :row rownumber
                                                    :units :internal))
                (this-radius (getf tool-row :RADIUS))
              )
  
         ; RULE: check for equivalence with tool radius and take exact radius
         ; if found in table otherwise take next bigger radius

         (if (sd-num-equal-p new-default-radius this-radius)
            (progn 
              (return-from sha-default-bend-radius new-default-radius)
            )
            (progn
               (if (and (> this-radius new-default-radius)
                        (or (NOT next-greater)
                            (< this-radius next-greater)))
                 (setf next-greater this-radius)
               )
            )
         )
       )
    )

    ; return next-greater-radius  if one was found
    (if next-greater
      next-greater
    )
   )
   nil ; wrong default calculation
 )
))

;============== 9. Automatic part attribute setting ===========================
;
; The following (or similar) function will be called with each:
;    sheet metal creation or material setting or material modification
; IF the function is listed beside keyword :HANDLE in the material table.
; It allows automatic user defined settings of attributes to sheet metal parts.
;
;==============================================================================

;;===    Addition TECHSOFT      WoHo 03.10.2011
;;===    Add density and color to the base instead of the instance of a sheet part.

(defun sha-part-creation-handler(&key part material-id)
  (let ((density (sha-get-db-entry material-id :DENSITY))
        (part-color  (sha-get-db-entry material-id :COLOR))
        (part-refl-color  (sha-get-db-entry material-id :REFL-COLOR))
        (part-refl-power  (sha-get-db-entry material-id :REFL-POWER))
       )
    ; set part density ( units will always be internal units )
    (when (and part density)
      ;(sd-call-cmds (SET_PART_INST_DENSITY :PARTS part :DENS density))
      (sd-call-cmds (SET_PART_BASE_DENSITY :PARTS part :DENS density))
    )

    ; set part color
    (when (and part part-color)
      ;(sd-call-cmds (SET_PART_INST_COLOR :PARTS part :COLOR part-color))
	  (sd-call-cmds (SET_PART_BASE_COLOR :PARTS part :COLOR part-color))
    )

    ; set part reflectance
    (when (and part part-refl-color)
      (sd-call-cmds (SET_PART_INST_REFLECTANCE :PARTS part 
                                               :REFL_COLOR part-refl-color))
    )
    (when (and part part-refl-color part-refl-power)
      (sd-call-cmds (SET_PART_INST_REFLECTANCE :PARTS part 
                                               :REFL_POWER part-refl-power))
    )
  )
)

;;===    Addition  TECHSOFT      WoHo 03.10.2011
;;===    Add density and color to the base instead of the instance of a sheet part.
;;===    Use SolidPower material
;;===    If no SolidPower material is available display a message.

(unless (find-package "TS-SD-STL") (make-package "TS-SD-STL"))
(defun sha-part-creation-handler-solidpower(&key part material-id)
  (let ((density (sha-get-db-entry material-id :DENSITY))
        (part-color  (sha-get-db-entry material-id :COLOR))
        (part-refl-color  (sha-get-db-entry material-id :REFL-COLOR))
        (part-refl-power  (sha-get-db-entry material-id :REFL-POWER))
		werkst material dicke
       )
    ; set part density ( units will always be internal units )
    (when (and part density)
      ;(sd-call-cmds (SET_PART_INST_DENSITY :PARTS part :DENS density))
      (sd-call-cmds (SET_PART_BASE_DENSITY :PARTS part :DENS density))
    )

    ; set part color
    (when (and part part-color)
      ;(sd-call-cmds (SET_PART_INST_COLOR :PARTS part :COLOR part-color))
	  (sd-call-cmds (SET_PART_BASE_COLOR :PARTS part :COLOR part-color))
    )

    ; set part reflectance
    (when (and part part-refl-color)
      (sd-call-cmds (SET_PART_INST_REFLECTANCE :PARTS part 
                                               :REFL_COLOR part-refl-color))
    )
    (when (and part part-refl-color part-refl-power)
      (sd-call-cmds (SET_PART_INST_REFLECTANCE :PARTS part 
                                               :REFL_POWER part-refl-power))
    )
	
	 ;;---    Addition TECHSOFT      Scha 29.04.2009
    (setf material (sha-get-db-entry material-id :MATERIAL)) ;;Scha 11.02.2014
    (when (sd-logical-table-p "ts-werkstofftabelle")
     (progn
      (setf werkst (sd-read-logical-table-cell "ts-werkstofftabelle" 
         :column :Bezeichnung
         :pList `(:Identifikator ,material)))
      (if (sd-string-p werkst)
        (progn
         (custom::TS-WERKSTOFF-ZUORDNEN :Identifikator material :teile part)
        )
        (progn
         (sd-display-error "Gewaehlter Werkstoff ist nicht in der SolidPower Werkstofftabelle")
        )
      )

     )
    )
	(setf dicke (sha-get-db-entry material-id :THICK))
	(when (and dicke
	           (sd-logical-table-p "ts-sd-stl-attr")
	           (sd-string-p (sd-read-logical-table-cell "ts-sd-stl-attr" :column :ATTRIBUTNAME :pList `(:ATTRIBUTNAME "BLECHDICKE")))
		  )
	  (TS-SD-STL::ts-am-stl-set-info-sel-item part "BLECHDICKE" (sd-num-to-string dicke))
	)

  )
)

;;===    Addition TECHSOFT      WoHo 03.10.2011
;;===    Add density and color to the base instead of the instance of a sheet part.
;;===    Use SolidPower material
;;===    If no SolidPower material is available use color and density from SheetMetal table.


(defun sha-part-creation-handler-solidpower-ohne-meldung(&key part material-id)
  (let ((density (sha-get-db-entry material-id :DENSITY))
        (part-color  (sha-get-db-entry material-id :COLOR))
        (part-refl-color  (sha-get-db-entry material-id :REFL-COLOR))
        (part-refl-power  (sha-get-db-entry material-id :REFL-POWER))
       )
    ; set part density ( units will always be internal units )
    (when (and part density)
      ;(sd-call-cmds (SET_PART_INST_DENSITY :PARTS part :DENS density))
      (sd-call-cmds (SET_PART_BASE_DENSITY :PARTS part :DENS density))
    )

    ; set part color
    (when (and part part-color)
      ;(sd-call-cmds (SET_PART_INST_COLOR :PARTS part :COLOR part-color))
	  (sd-call-cmds (SET_PART_BASE_COLOR :PARTS part :COLOR part-color))
    )

    ; set part reflectance
    (when (and part part-refl-color)
      (sd-call-cmds (SET_PART_INST_REFLECTANCE :PARTS part 
                                               :REFL_COLOR part-refl-color))
    )
    (when (and part part-refl-color part-refl-power)
      (sd-call-cmds (SET_PART_INST_REFLECTANCE :PARTS part 
                                               :REFL_POWER part-refl-power))
    )
	 ;;---    Addition TECHSOFT      WoHo 03.10.2011
    (setf material (sha-get-db-entry material-id :MATERIAL))
    (when (sd-logical-table-p "ts-werkstofftabelle")
     (progn
      (setf werkst (sd-read-logical-table-cell "ts-werkstofftabelle" 
         :column :Bezeichnung
         :pList `(:Identifikator ,material)))
      (if (sd-string-p werkst)
        (progn
         (custom::TS-WERKSTOFF-ZUORDNEN :Identifikator material :teile part)
        )
       )

     )
    )	

	(setf dicke (sha-get-db-entry material-id :THICK))
	(when (and dicke
	           (sd-logical-table-p "ts-sd-stl-attr")
	           (sd-string-p (sd-read-logical-table-cell "ts-sd-stl-attr" :column :ATTRIBUTNAME :pList `(:ATTRIBUTNAME "BLECHDICKE")))
		  )
	  (TS-SD-STL::ts-am-stl-set-info-sel-item part "BLECHDICKE" (sd-num-to-string dicke))
	)
  )
)





(defun sha-change-feature-color(&key feature tool-id)
  (let ((feat-color (or  (sha-get-db-entry tool-id :COLOR) 16711680)) ;:RED
       )

    ; set feature face color
    (when feature
      (sd-call-cmds (SET_FACE_COLOR :BY_FEATURE feature :color feat-color))
    )
  )
)


;== 10. Example function/macro for a customer defined drawing option in MULTI_UNFOLD =

(defun sheet-advisor::sha-flat-remove-chamfer (&key part_path &allow-other-keys)
  ;____________________________________________________________________________
  ;
  ; With input of a given part-path the function calls the DELETE_CHAMFER
  ; command to delete all existing chamfers on the sheet metal part.
  ; The function will be used as "option doer" for the user defined 
  ; UNFOLD / Drawings option called "Chamfer". The definition of this user 
  ; defined button can be found in the sha_demoshop.lsp file. 
  ; This function (sha-flat-remove-chamfer) is listed beside the :FUNCTION key
  ; of the chamfer option ; defintiion.
  ;
  ; INPUT: part_path    = string with unique part name <string>
  ;                   
  ; RETURN: t when successful
  ;

  (let ((face-list (sd-call-cmds
                     (get_selection :focus_type *sd-chamfer-3d-seltype*
                                                    :select :in_part (sd-pathname-to-obj part_path)))
                           )
        (part (sd-pathname-to-obj part_path))
        (counter 0)
        (do-single-steps nil)
       )
    (when face-list
      (setf counter (+ (* (length face-list) 2) 2))
      (sd-call-cmds
       (delete_chamfer :check :local_mod :on face-list)
       :success t
       :failure (progn (setf do-single-steps t)
                               )
       )
      (when do-single-steps
        (loop
          (setf face-list (sd-call-cmds (get_selection :focus_type *sd-chamfer-3d-seltype*
                                                       :select :in_part (sd-pathname-to-obj part_path))))
          (when (= (length face-list) 0) (return t))
            (sd-call-cmds (delete_chamfer :check :local_mod :on (first face-list))
                            :success t
                            :failure (progn
                                       (sd-call-cmds
                                         (delete_chamfer :check :local_mod :on (second face-list))
                                           :success t
                                           :failure nil
                                         )
                                     )
            )
            (when (= (setf counter (- counter 1)) 0) (return nil)))
      )
    )
    (when part
      (sd-call-cmds
        (DELETE_INCOMPLETE_CHAMFERS :CHMF_PART part
                                  ;;:THICKNESS-SI thickness
                                    :BODYCHECK-SI nil)
        :failure (sd-multi-lang-string "Deleting incomplete chamfers failed" :german "Löschen von scharfkantigen Fasen fehlgeschlagen")
      )
    )
    (sd-set-model-checkpoint)
  )
  t
)

(defun sha-change-color-red()
  ; Example macro sequence for 2D Drafting postprocessing of 2D flat data
  ; RETRUN: A string or a list of strings which can be used as command sequence in Drafting/Annotation
  ;         to change the actual 2D flat data
   "CHANGE_COLOR RED SELECT GLOBAL ALL CONFIRM END"
)

;== 11. Example dialog for a customer defined multi UNFOLD ===================
;       interactive postprocess dialog 

(sd-defdialog 'sha_unfold_post_process_dialog
  :dialog-title "Custom Process"
  :dialog-type :subaction
  :embedded-ui nil
  :subaction-close-behavior :cancel
  :subaction-bottom-line :ok-cancel-help
  :variables '(
    ;; The calling unfold dialog delivers a property-list (hold in internal 
    ;; variable DEFAULT) with a set of keyword/value pairs.
    ;; This list contains a combination of following hardcoded properties plus
    ;; the postprocess properties defined in the shop database (defined in a 
    ;; property list wich is refrenced via the keyword :POSTPROCESS-SETTINGS).
    ;;
    ;; hardcoded properties
    ;;   (:TMPDIR    <string>  = string holding the temporary directory
    ;;                           where the produced files will be put
    ;;    :PART_PATH <string>  = string holding the absolute path of the
    ;;                           actual unfolded part
    ;;    :THICKNESS <number>  = thickness of actual unfolded part in
    ;;                           internal units [mm]
    ;;    :MATERIAL  <string>  = material name
    ;;    :STATION   <string>  = name of workstantion the user is actually
    ;;                           working
    ;;    :USER      <string>  = user name 
    ;;    :DR_NR     <string>  = drawing number (derived from part-name )
    ;;    :L_TAB     <string>  = name of file containing MM/DDM database 
    ;;                           settings
    ;;    :MI-LIST   <list>    = list of MI-filenames produced by unfold
    ;;    :DXF-LIST  <list>    = list of DXF-filenames produced by unfold
    ;;    :DWG-LIST  <list>    = list of DWG-filenames produced by unfold
    ;;    :IGES-LIST <list>    = list of IGES-filenames produced by unfold
    ;;    :VRML      <string>  = name of VRML filename produced by unfold 
    ;;    :HTML-LIST <list>    = list of HTML-filenames produced by unfold
    ;;    :JPEG-2D-LIST <list> = list of JPEG-filenames produced by unfold
    ;;                           (2D flat drawing data)
    ;;    :TIFF-2D-LIST <list> = list of IGES-filenames produced by unfold
    ;;                           (2D flat drawing data data)
    ;;    :JPEG      <string.  = name of JPEG filename containing 3D 
    ;;                           screen shot 
    ;;   )

    (PROPS   :initial-value DEFAULT)
    (DP_PR   :title (sd-multi-lang-string "Display the Prop-Lst" :german "Prop List")
             :toggle-type :wide-toggle
             :push-action 
               (let (propsstring (count 1))
                 (dolist (p1 (reverse PROPS)) 
                   (if (evenp count)
                     (setf propsstring (format nil "~%~S ~A" p1 propsstring))
                     (setf propsstring (format nil " ~S ~A" p1 propsstring))
                   )
                   (setf count (+ 1 count))
                 )
                 (sd-display-message propsstring)
               )
    )
    (GET_MI :title (sd-multi-lang-string "MI File" :german "MI Datei")
            :toggle-type :wide-toggle
            :push-action 
              (if (getf PROPS :PART_PATH)
                (when (probe-file (getf PROPS :MIFILENAME))
                  (sd-display-message
                    (format nil "File ~A found" (getf PROPS :MIFILENAME)))
                )
              )
    )
  )
  :ok-action '(progn
   ;(dbg-print "OK Action Subdialog done")
  )
)

;== 12. Example function for a customer defined multi UNFOLD ==================
;       SHOP defined (NON interactive) postprocess function

(defun sheet-advisor::sha-shop-postprocess-function
    (&rest keyword-pairs 
     &key TMPDIR PART_PATH THICKNESS MATERIAL STATION USER DR_NR
          L_TAB MI-LIST DXF-LIST DWG-LIST IGES-LIST VRML HTML-LIST
          JPEG-2D-LIST TIFF-2D-LIST JPEG 
     &allow-other-keys )

    ;; Final customer definable postprocessing on produced MULTI_UNFOLD data. 
    ;; This function can for example start other programs which work on the
    ;; produced data (postprocessing), or it can inform user to act on delivery 
    ;; of this data (workflow). 
    ;; Beside the here listed standard parameters, all options of the 
    ;; :POSTPROCESS-SETTINGS property-list (which is defined in the shop) are
    ;; passed as paramters to this function.
    ;; Access to parameters might either be done via:  
    ;;   (getf keyword-pairs :MYKEY) 
    ;; or via the listed known
    ;; standard parameters. Alternatively these parqmeter list can also be
    ;; extended by the user.
    ;;
    ;; (:TMPDIR    <string>  = string holding the temporary directory where the
    ;;                         produced files will be put
    ;;  :PART_PATH <string>  = string holding the absolute path of the actual
    ;;                         unfolded part
    ;;  :THICKNESS <number>  = thickness of actual unfolded part in internal 
    ;;                         units [mm]
    ;;  :MATERIAL  <string>  = material name
    ;;  :STATION   <string>  = name of workstantion the user is actually working
    ;;  :USER      <string>  = user name 
    ;;  :DR_NR     <string>  = drawing number (derived from part-name )
    ;;  :L_TAB     <string>  = name of file containing MM/DDM database settings
    ;;  :MI-LIST   <list>    = list of MI-filenames produced by unfold
    ;;  :DXF-LIST  <list>    = list of DXF-filenames produced by unfold
    ;;  :DWG-LIST  <list>    = list of DWG-filenames produced by unfold
    ;;  :IGES-LIST <list>    = list of IGES-filenames produced by unfold
    ;;  :VRML      <string>  = name of VRML filename produced by unfold 
    ;;  :HTML-LIST <list>    = list of HTML-filenames produced by unfold
    ;;  :JPEG-2D-LIST <list> = list of JPEG-filenames produced by unfold
    ;;                         (2D flat drawing data data)
    ;;  :TIFF-2D-LIST <list> = list of IGES-filenames produced by unfold
    ;;                         (2D flat drawing data)
    ;;  :JPEG      <string.  = name of JPEG filename containing 3D screen shot 
    ;; )

   (let (propstr)
    (setf propstr (format nil "~%:TMPDIR       ~S ~A" TMPDIR propstr))
    (setf propstr (format nil "~%:PART_PATH    ~S ~A" PART_PATH propstr))
    (setf propstr (format nil "~%:THICKNESS    ~S ~A" THICKNESS propstr))
    (setf propstr (format nil "~%:MATERIAL     ~S ~A" MATERIAL propstr))
    (setf propstr (format nil "~%:STATION      ~S ~A" STATION propstr))
    (setf propstr (format nil "~%:USER         ~S ~A" USER propstr))
    (setf propstr (format nil "~%:DR_NR        ~S ~A" DR_NR propstr))
    (setf propstr (format nil "~%:L_TAB        ~S ~A" L_TAB propstr))
    (setf propstr (format nil "~%:MI-LIST      ~S ~A" MI-LIST propstr))
    (setf propstr (format nil "~%:DXF-LIST     ~S ~A" DXF-LIST propstr))
    (setf propstr (format nil "~%:DWG-LIST     ~S ~A" DWG-LIST propstr))
    (setf propstr (format nil "~%:IGES-LIST    ~S ~A" IGES-LIST propstr))
    (setf propstr (format nil "~%:VRML         ~S ~A" VRML propstr))
    (setf propstr (format nil "~%:HTML-LIST    ~S ~A" HTML-LIST propstr))
    (setf propstr (format nil "~%:JPEG-2D-LIST ~S ~A" JPEG-2D-LIST propstr))
    (setf propstr (format nil "~%:TIFF-2D-LIST ~S ~A" TIFF-2D-LIST propstr))
    (setf propstr (format nil "~%:JPEG         ~S ~A" JPEG propstr))
    (setf propstr (format nil "~%:keyword-pairs  ~S ~A" keyword-pairs propstr))
    ;(sd-display-message propstr)
    (sd-display-warning "WARNUNG: Postprozess-Funktion noch nicht definiert
Diese Funktion entweder definieren (siehe Beispiel: sha-shop-postprocess-function in "personality/sd_customize/SheetAdvisor/sha_demoshop_func.lsp") oder die Nutzung der Funktion deaktivieren, indem Sie :POSTPROCESS in Ihrer Fertigungsstättendatei auf null setzen.")
   )
)

; 13. Example for an automatic proceess selection function in MULTI_UNFOLD 

(defun sh-needed-shop-processes(&key part material thickness &allow-other-keys)
  ;; Get a list of all production processes in current shop which are relevant for the actual part
  ;; This might be dependent on the used material and manufacturing processes of the selected part
  ;; INPUT: :PART <sel-item>
  ;;        :MATERIAL material       e.g. "UST 1203"
  ;;        :THICKNESS thickness     e.g. 2.00
  ;; OUTPUT: list of (probably) needed production processes for given part as property list
  ;;         e.g. ((:process "Punching" :visible t :valid t) (:process "Stamping :visible t :valid nil) ...)

  (let* ((shopname     (mbu-get-current-shop))
        ;(used-punches (sha-inquire-punches :PART part))
         (used-stamps  (sha-inquire-stamps  :PART part))
         (used-bends   (sha-inquire-bends   :PART part))
        ;(used-reliefs (sha-inquire-reliefs :PART part))
         (used-welds   nil)
         (production-processes (sha-get-shop-entry :shopname shopname :column :PRODUCTION-PROCESSES))
         (process-relation-table (shoptabname shopname (sha-get-shop-entry :shopname shopname :column :PROCESS-RELATIONS)))
         results
        )

    ;; seek relation table and check for valid processes for given material
    (when (and process-relation-table (sd-logical-table-p process-relation-table))
      (dotimes (rownumber (sd-get-logical-table-number-of-rows process-relation-table))
        (let ((one-row (sd-read-logical-table-row process-relation-table :row rownumber :units :internal))
             )
          ; check material keys
          (when (and (sha-table-entry-eq (getf one-row :material) material)
                     (between (getf one-row :MIN_THICK) THICKNESS (getf one-row :MAX_THICK)))
             (setf production-processes (getf one-row :processes))
             (return)
          )
        )
      )
    )

    ;; compare actual list of production processes against known needed processes (those used in part)
    (dolist (prod-process production-processes)
      (let ((process-type (sha-get-db-entry (list :SHOPNAME shopname :TABNAME prod-process) :PROCESS-TYPE)))
        (case process-type
          ((:BEND-PROCESSES :BEND-PROCESS-180)
           (if used-bends
             (push (list :process prod-process :visible t :valid t) results)
             (push (list :process prod-process :visible t :valid nil) results)
           ))
          (:CUT-PROCESSES   (push (list :process prod-process :visible t :valid t) results)) ;; needed in all cases
          (:STAMP-PROCESSES (if used-stamps
                              (push (list :process prod-process :visible t :valid t) results)
                              (push (list :process prod-process :visible t :valid nil) results)
                            ))
          (:WELD-PROCESSES  (if used-welds
                              (push (list :process prod-process :visible t :valid t) results)
                              (push (list :process prod-process :visible t :valid nil) results)
                            ))
          (t                (push (list :process prod-process :visible t :valid nil) results))
        )
      )
    )

    ;; No specials, all valid
    (unless results
      (dolist (prod-process production-processes)
        (push (list :process prod-process :visible t :valid nil) results)
      )
    )
    (nreverse results)
  )
)
