;=================================================;
;                                                 ;
;    This file contains functions to model some   ;
;    parametric stamp tools.                      ;
;                                                 ;
;=================================================;

(in-package :sheet-advisor)
(use-package :oli)

;=================================================
;
;   EXTRUDED FLANGE
;
;=================================================


(defun sha-ext_flange-profile
  (&key
   hole_dia
   thickness
   (radius (* 0.6 thickness))
   (tap_thick (* 0.6 thickness))
   height
   resolution
   &allow-other-keys
  )
  (when (< height (+ radius resolution))  ; reduce radius if it would be to big
    (setf radius (* .9 height))
    (setf tap_thick radius)
  )
  (let (
        (tool-wp   (sha-tool-wp))
       )
       (create_workplane :new
                         :name tool-wp
                         :world_origin
       )
       ;********* create Profile **********
       (CIRCLE :CEN_RAD
               0,0
               (+ (/ hole_dia 2) radius tap_thick)
       )
       ;******** create Adjustpoints ******
       (C_POINT
          0,0
       )
       (setq result (sha-profile-of-wp tool-wp))
       (delete_3d (sha-absolute-name tool-wp))
       result
  )
)

(defun sha-ext_flange-tool
  (&key
  hole_dia
  height
  stamp_dir 
  thickness
  resolution
  (radius (* 0.6 thickness))
  (tap_thick (* 0.6 thickness))
  &allow-other-keys
  )

  (let ((result nil)
        (tool-part (sha-tool-part))
        (tool-wp   (sha-tool-wp))
       )
  (when (< height (+ radius resolution)) ; reduce radius if it would be too big
    (setf radius (* .9 height))
    (setf tap_thick radius)
  )
  (unless (or
	  (< hole_dia resolution)
	  (< height (+ radius resolution))
	  (< radius resolution)
	  (< tap_thick resolution)
      )
         
        (if (equal stamp_dir :DOWN)           ;create a new wp for direction DOWN
          (create_workplane :new
                            :name tool-wp            
			    :pt_dir :origin (gpnt3d 0 0 0)
			    :normal :x
			    :u_dir :neg_z
          )
          (create_workplane :new            ;create a new wp for direction UP
			    :name tool-wp             
			    :pt_dir :origin (gpnt3d 0 0 (- thickness))
			    :normal :x
			    :u_dir :z
          )
        )
        (POLYGON
	   (gpnt2d 0 (+ (/ hole_dia 2) radius))
	   (gpnt2d 0 (+ (/ hole_dia 2) radius tap_thick))
	   (gpnt2d thickness (+ (/ hole_dia 2) radius tap_thick))
        )
	(ARC :CEN_RAD_ANG
           (gpnt2d (+ thickness radius) (+ (/ hole_dia 2) radius tap_thick))
	   radius
	   (gpnt2d thickness (+ (/ hole_dia 2) radius tap_thick))
	   (gpnt2d (+ thickness radius) (+ (/ hole_dia 2) tap_thick))
        )
        (POLYGON
	   (gpnt2d (+ thickness radius) (+ (/ hole_dia 2) tap_thick))
	   (gpnt2d (+ thickness height) (+ (/ hole_dia 2) tap_thick))
	   (gpnt2d (+ thickness height) (/ hole_dia 2))
           (gpnt2d radius (/ hole_dia 2))
        )
        (ARC :CEN_RAD_ANG
           (gpnt2d radius (+ (/ hole_dia 2) radius))
	   radius
	   (gpnt2d 0 (+ (/ hole_dia 2) radius))
	   (gpnt2d radius (/ hole_dia 2) )
        )
        (turn
          :part tool-part       
          :axis :z
        )
        (setq result (sha-body-of-tool-part tool-part))
        (delete_3d (sha-absolute-name tool-wp))
        (delete_3d tool-part)
      )
    result
    )
)

;=================================================
;
;   CARD GUIDE     
;
;=================================================


(defun sha-cdg-profile
  (&key
   slotlength
   slotrad
   liprad
   liplength
   x_clust
   y_clust
   bottomwidth
   topwidth
   lipheight
   thickness
   resolution
   &allow-other-keys
  )
  (let* (
         (tool-wp   (sha-tool-wp))
	 (alpha (atan (/ (- topwidth bottomwidth) lipheight )))
	 (b (+ (/ topwidth 2) (* (cos alpha) thickness) liprad))
	 (l (+ (/ liplength 2) b (/ bottomwidth -2)))
         (adj_y (+ (* b 2) (* slotrad 4) y_clust))
        )
        (create_workplane :new
                          :name tool-wp
                          :world_origin
        )
        ;********* create Profile **********
        (LINE :TWO_POINTS
           (gpnt2d 0 (+ b (* slotrad 2)))
	   (gpnt2d (- (/ slotlength 2) slotrad) (+ b (* slotrad 2)))
        )
	(when (> (- slotlength (* slotrad 2)) (+ liplength resolution))
           (LINE :TWO_POINTS
		 (gpnt2d (- (/ slotlength 2) slotrad) b)
		 (gpnt2d l b)
           )
        )
	(LINE :TWO_POINTS
 	   (gpnt2d l b)
	   (gpnt2d l 0)
	)
	(ARC :CEN_RAD_ANG
           (gpnt2d (- (/ slotlength 2) slotrad) (+ b slotrad))
	   slotrad
	   (gpnt2d (- (/ slotlength 2) slotrad) b)
	   (gpnt2d (- (/ slotlength 2) slotrad) (+ b (* slotrad 2)))
        )
	(MIRROR_2D 
	   :SELECT :all_2d
	   :HORIZONTAL
	   0
	)
	(MIRROR_2D
	   :SELECT :all_2d
	   :VERTICAL
	   0
	)
        ;******** create Adjustpoints ******
        (C_POINT
           0,0
	   (gpnt2d (+ slotlength x_clust) 0)
	   (gpnt2d (- (+ slotlength x_clust)) 0)
	   (gpnt2d 0 adj_y)
	   (gpnt2d (+ slotlength x_clust) adj_y)
	   (gpnt2d (- (+ slotlength x_clust)) adj_y)
	   (gpnt2d 0 (- adj_y))
	   (gpnt2d (+ slotlength x_clust) (- adj_y))
	   (gpnt2d (- (+ slotlength x_clust)) (- adj_y))
        )
        (setq result (sha-profile-of-wp tool-wp))
        (delete_3d (sha-absolute-name tool-wp))
        result
  )
)

(defun sha-cdg-tool
  (&key
   slotrad
   slotlength
   liplength
   liprad
   topwidth
   bottomwidth
   lipheight
   stamp_dir
   x_clust
   y_clust
   thickness
   resolution
   &allow-other-keys
  )

  (let ((result nil)
        (tool-part (sha-tool-part))
        (tool-wp   (sha-tool-wp))
	(alpha (atan (/ (- topwidth bottomwidth) lipheight)))
       )
       (unless (or
               (or (not bottomwidth) (< bottomwidth resolution))
	       (or (not topwidth)    (< topwidth (+ bottomwidth resolution)))
	       (or (not liplength)   (< liplength resolution))
	       (or (not lipheight)   (< lipheight resolution))
	       (or (not slotrad)     (< slotrad resolution))
	       (or (not slotlength)  (< slotlength (+ liplength (/ topwidth 2) (* (cos alpha) thickness) liprad (/ bottomwidth -2))))
	       (or (not x_clust)     (< x_clust resolution))
	       (or (not y_clust)     (< y_clust resolution))
               )
               (let* 
		 (
		  (p5x (+ (/ topwidth 2) (* (cos alpha) thickness)))
	          (p5y (- (+ lipheight thickness) (* (sin alpha) thickness)))
          	  (p6x (- p5x (- topwidth bottomwidth)))
	          (p6y (* (- 1 (sin alpha)) thickness))
                 )
             
                 (if (equal stamp_dir :DOWN)           ;create a new wp for direction DOWN
                       (create_workplane :new
                                         :name tool-wp
                                         :pt_dir :origin (gpnt3d 0 0 0)
                                         :normal :x
                                         :u_dir :neg_y
                       )
                       (create_workplane :new            ;create a new wp for direction UP
                                         :name tool-wp
                                         :pt_dir :origin (gpnt3d 0 0 (- thickness))
                                         :normal :x
                                         :u_dir :y
                       )
                 )
                 (POLYGON
                   (gpnt2d 0 0)
                   (gpnt2d 0 thickness)
                   (gpnt2d (/ bottomwidth 2) thickness)
          	   (gpnt2d (/ topwidth 2) (+ lipheight thickness))
	           (gpnt2d p5x p5y)
	           (gpnt2d p6x p6y)
                   (gpnt2d (/ bottomwidth 2) 0)
                   (gpnt2d 0 0)
                 )
                 (extrude
                   :part tool-part
                   :distance (/ liplength 2)
                 )
                 (position_wp
                   (sha-absolute-name tool-wp)
                   :translate
                   :dir_len
                   :ref_wp (sha-absolute-name tool-wp)
                   :w 
                   (/ liplength 2)
                 )
		 (turn	
		   :part tool-part
		   :axis :vert (+ p5x liprad)
		   :rotation_angle (deg-to-rad 90)
                 )
		 ( delete_2d :all_2d)
		 (reflect_3d :parts tool-part 
			     :reflect_plane :neg_u 
			     :both_sides :off
		 )
                 (position_wp
                   (sha-absolute-name tool-wp)
                   :translate
                   :dir_len
                   :ref_wp (sha-absolute-name tool-wp)
                   :w 
                   (+ p5x liprad (/ bottomwidth -2))
                 )
		 (rectangle 
		   (gpnt2d (- (+ p5x liprad)) 0) 
		   (gpnt2d (+ p5x liprad) thickness)
		 )
		 (extrude :to_part 
			  :current 
			  :reverse
		 )
		 (mill
		   :distance bottomwidth
		   :reverse
                 )
		 (position_wp
                   (sha-absolute-name tool-wp)
                   :translate
                   :dir_len
                   :ref_wp (sha-absolute-name tool-wp)
                   :w 
                   (+ (/ bottomwidth 2) (/ liplength -2) (- (+ p5x liprad)))
                 )
		 (reflect_3d 
		   :parts tool-part 
		   :reflect_plane :neg_w 
		   :both_sides :off
		 )
                 (setq result (sha-body-of-tool-part tool-part))
                 (delete_3d (sha-absolute-name tool-wp))
                 (delete_3d tool-part)
               )
       ) 
       result
    )
)

;=================================================
;
;   LOUVER SIMPLE
;
;=================================================


(defun sha-lvr_smpl-profile
  (&key
   length
   height
   (tool_dist_x height)
   (tool_dist_y height)
   thickness
   resolution
   &allow-other-keys
  )
  (let* (
         (tool-wp   (sha-tool-wp))
        )
        (create_workplane :new
                          :name tool-wp
                          :world_origin
        )
        ;********* create Profile **********
        (LINE :TWO_POINTS
           (gpnt2d 0 0)   (gpnt2d (+ (/ length 2) height) 0)
           (gpnt2d 0 height) (gpnt2d (/ length 2) height)
        )
        (ARC :CEN_RAD_ANG
           (gpnt2d (/ length 2) 0)
           height
           (gpnt2d (+ (/ length 2) height) 0)
           (gpnt2d (/ length 2) height)
        )
        (MIRROR_2D
           :SELECT :all_2d
           :VERTICAL
           0
        )
        ;******** create Adjustpoints ******
        (C_POINT
           0,0
	   (gpnt2d (+ length (* height 2) tool_dist_x) 0)
	   (gpnt2d (- (+ length (* height 2) tool_dist_x)) 0)
	   (gpnt2d 0 (+ height tool_dist_y))
	   (gpnt2d (+ length (* height 2) tool_dist_x) (+ height tool_dist_y))
	   (gpnt2d (- (+ length (* height 2) tool_dist_x)) (+ height tool_dist_y))
           (gpnt2d 0 (- (+ height tool_dist_y)))
	   (gpnt2d (+ length (* height 2) tool_dist_x) (- (+ height tool_dist_y)))
	   (gpnt2d (- (+ length (* height 2) tool_dist_x))  (+ height tool_dist_y))
	   (gpnt2d 0 (/ height 2))
        )
        (setq result (sha-profile-of-wp tool-wp))
        (delete_3d (sha-absolute-name tool-wp))
        result
  )
)

(defun sha-lvr_smpl-tool
  (&key
   length
   height
   (tool_dist_x height)
   (tool_dist_y height)
   stamp_dir
   thickness
   resolution
   &allow-other-keys
  )

  (let ((result nil)
        (tool-part (sha-tool-part))
        (tool-wp   (sha-tool-wp))
       )
       (unless (or
               (< length (* (+ height resolution) 2))
               (< height (+ thickness resolution))
               (< tool_dist_x resolution)
               (< tool_dist_y resolution)
               )

               (if (equal stamp_dir :DOWN)           ;create a new wp for direction DOWN
                     (create_workplane :new
                                       :name tool-wp
                                       :pt_dir :origin (gpnt3d 0 0 0)
                                       :normal :neg_x
                                       :u_dir :y
                     )
                     (create_workplane :new            ;create a new wp for direction UP
                                       :name tool-wp
                                       :pt_dir :origin (gpnt3d 0 0 (- thickness))
                                       :normal :x
                                       :u_dir :y
                     )
               )
               (LINE :TWO_POINTS
                 (gpnt2d height 0)                    (gpnt2d height thickness)
                 (gpnt2d 0 (+ height thickness))      (gpnt2d 0 height)
               )
               (ARC :CEN_RAD_ANG
                 (gpnt2d height thickness)
                 thickness
                 (gpnt2d (- height thickness) thickness)
                 (gpnt2d height 0)
               )
               (ARC :CEN_RAD_ANG
                 (gpnt2d 0 thickness)
                 (- height thickness)
                 (gpnt2d (- height thickness) thickness)
                 (gpnt2d 0 (- height thickness))
               )
               (ARC :CEN_RAD_ANG
                 (gpnt2d 0 thickness)
                 height
                 (gpnt2d height thickness)
                 (gpnt2d 0 (+ thickness height))
               )
               (EXTRUDE
                 :part tool-part
                 :distance (/ length 2)
               )
               (POSITION_WP
                 (sha-absolute-name tool-wp)
                 :translate
                 :dir_len
                 :ref_wp (sha-absolute-name tool-wp)
                 :w
                 (/ length 2)
               )
               (TURN
                 :part tool-part
                 :axis :neg_v 
                 :rotation_angle (deg-to-rad 90)
               )
               (POSITION_WP
                 (sha-absolute-name tool-wp)
                 :translate
                 :dir_len
                 :ref_wp (sha-absolute-name tool-wp)
                 :w
                 (/ length -2)
               )
               (REFLECT_3D 
                 :parts tool-part
                 :reflect_plane :neg_w
                 :both_sides :off
               )
               (setq result (sha-body-of-tool-part tool-part))
               (delete_3d (sha-absolute-name tool-wp))
               (delete_3d tool-part)
       )
       result
    )
)

;=================================================
;
;   LOUVER
;
;=================================================


(defun sha-lvr-profile
  (&key
   length
   depth
   width
   angle
   radius
   (tool_dist_x width)
   (tool_dist_y width)
   thickness
   resolution
   &allow-other-keys
  )
  (let* (
         (tool-wp   (sha-tool-wp))
         (hl (- (/ length 2) radius))
        )
        (create_workplane :new
                          :name tool-wp
                          :world_origin
        )
        ;********* create Profile **********
        (LINE :TWO_POINTS
           (gpnt2d 0 0)   (gpnt2d (+ (/ length 2) (* (tan angle) depth)) 0)
           (gpnt2d 0 (+ width (* (tan angle) depth))) (gpnt2d hl (+ width (* (tan angle) depth)))
           (gpnt2d (+ (/ length 2) (* (tan angle) depth)) 0) (gpnt2d (+ (/ length 2) (* (tan angle) depth)) (- width radius))
        )
        (ARC :CEN_RAD_ANG
           (gpnt2d hl (- width radius))
           (+ radius (* (tan angle) depth))
           (gpnt2d (+ (/ length 2) (* (tan angle) depth)) (- width radius))
           (gpnt2d hl (+ width (* (tan angle) depth)))
        )
        (MIRROR_2D
           :SELECT :all_2d
           :VERTICAL
           0
        )
        ;******** create Adjustpoints ******
        (let ( 
               (dist_x (+ length 2 (* (* (tan angle) depth) 2) tool_dist_x))
               (dist_y (+ width (* (tan angle) depth) tool_dist_y))
             )
             (C_POINT
                0,0
                (gpnt2d dist_x 0)
                (gpnt2d (- dist_x) 0)
                (gpnt2d 0 dist_y)
                (gpnt2d dist_x dist_y)
                (gpnt2d (- dist_x) dist_y)
                (gpnt2d 0 (- dist_y))
                (gpnt2d dist_x (- dist_y))
                (gpnt2d (- dist_x) (- dist_y))
                (gpnt2d 0 (/ (+ width (* (tan angle) depth)) 2))
             )
        )
        (setq result (sha-profile-of-wp tool-wp))
        (delete_3d (sha-absolute-name tool-wp))
        result
  )
)

(defun sha-lvr-tool
  (&key
   length
   depth
   width
   angle
   radius
   (tool_dist_x width)
   (tool_dist_y width)
   stamp_dir
   thickness
   resolution
   &allow-other-keys
  )

  (let ((result nil)
        (tool-part (sha-tool-part))
        (tool-wp   (sha-tool-wp))
        (hl (- (/ length 2) radius))
       )
       (unless (or
                (or (not hl)          (< hl resolution))
                (or (not depth)       (< depth (+ thickness resolution)))
                (or (not tool_dist_x) (< tool_dist_x resolution))
                (or (not tool_dist_y) (< tool_dist_y resolution))
                (or (not angle)       (< angle (deg-to-rad 0)))
                (> angle (deg-to-rad 90))
                (or (not radius)      (< radius resolution))
                (or (not width)       (< width (+ radius resolution)))
               )

               (if (equal stamp_dir :DOWN)           ;create a new wp for direction DOWN
                     (create_workplane :new
                                       :name tool-wp
                                       :pt_dir :origin (gpnt3d 0 (- width radius) 0)
                                       :normal :y
                                       :u_dir :x
                     )
                     (create_workplane :new            ;create a new wp for direction UP
                                       :name tool-wp
                                       :pt_dir :origin (gpnt3d 0 (- width radius) (- thickness))
                                       :normal :y
                                       :u_dir :neg_x
                     )
               )
               (POLYGON
		 (gpnt2d 0 depth)
		 (gpnt2d 0 (+ depth thickness))
		 (gpnt2d radius (+ depth thickness))
		 (gpnt2d (+ radius (* (tan angle) depth)) thickness)
		 (gpnt2d (+ radius (* (tan angle) depth)) 0)
		 (gpnt2d radius depth)
		 (gpnt2d 0 depth)
               )
	       (MOVE_2D
		 :SELECT :all_2d
		 :HORIZONTAL hl
               )
               (EXTRUDE
                 :part tool-part
                 :distance (- radius width)
               )
               (TURN
                 :part tool-part
                 :axis :neg_vert hl
                 :rotation_angle (deg-to-rad 90)
               )
               (POSITION_WP
                 (sha-absolute-name tool-wp)
                 :rotate
		 :axis :neg_vert hl
		 :rotation_angle (deg-to-rad 90)
               )
	       (delete_2d (gpnt2d hl (+ depth (/ thickness 2))))
               (POLYGON
                 (gpnt2d hl (+ depth thickness))
		 (gpnt2d (- hl (- width radius)) (+ depth thickness))
		 (gpnt2d (- hl (- width radius)) depth)
		 (gpnt2d hl depth)
               )
	       (EXTRUDE
		 :part tool-part
                 :distance hl
               )
               (POSITION_WP
                 (sha-absolute-name tool-wp)
                 :translate
                 :dir_len
                 :ref_wp (sha-absolute-name tool-wp)
                 :w
                 hl
               )
               (REFLECT_3D
                 :parts tool-part
                 :reflect_plane :w
                 :both_sides :off
               )
               (setq result (sha-body-of-tool-part tool-part))
               (delete_3d (sha-absolute-name tool-wp))
               (delete_3d tool-part)
       )
       result
    )
)

;=================================================
;
;   SEMI-PIERCED RECTANGLE
;
;=================================================


(defun sha-spr-profile
  (&key
   horiz_punch 
   vert_punch
   (tool_dist_x (* horiz_punch 2))
   (tool_dist_y (* vert_punch 2))
   &allow-other-keys
  )
  (let* (
         (tool-wp   (sha-tool-wp))
        )
        (create_workplane :new
                          :name tool-wp
                          :world_origin
        )
        ;********* create Profile **********
        (RECTANGLE
           (gpnt2d (/ horiz_punch 2) (/ vert_punch 2))   (gpnt2d (/ horiz_punch -2) (/ vert_punch -2))
        )
        ;******** create Adjustpoints ******
        (C_POINT
           0,0
           (gpnt2d tool_dist_x 0)
           (gpnt2d (- tool_dist_x) 0)
           (gpnt2d 0 tool_dist_y)
           (gpnt2d tool_dist_x tool_dist_y)
           (gpnt2d (- tool_dist_x) tool_dist_y)
           (gpnt2d 0 (- tool_dist_y))
           (gpnt2d tool_dist_x (- tool_dist_y))
           (gpnt2d (- tool_dist_x) (- tool_dist_y))
        )
        (setq result (sha-profile-of-wp tool-wp))
        (delete_3d (sha-absolute-name tool-wp))
        result
  )
)

(defun sha-spr-tool
  (&key
   horiz
   vert
   horiz_punch
   vert_punch
   (tool_dist_x horiz_punch)
   (tool_dist_y vert_punch)
   height
   depth
   stamp_dir
   thickness
   resolution
   &allow-other-keys
  )

  (let ((result nil)
        (tool-part (sha-tool-part))
        (tool-wp   (sha-tool-wp))
       )       (unless (or
               (< vert resolution)
               (< horiz resolution)
               (< height resolution)
               (< vert_punch vert)
               (< horiz_punch horiz)
               (< thickness depth)
               )

               (if (equal stamp_dir :DOWN)           ;create a new wp for direction DOWN
                     (create_workplane :new
                                       :name tool-wp
                                       :pt_dir :origin (gpnt3d 0 0 (- thickness))
                                       :normal :neg_z
                                       :u_dir :x
                     )
                     (create_workplane :new            ;create a new wp for direction UP
                                       :name tool-wp
                                       :pt_dir :origin (gpnt3d 0 0 0)
                                       :normal :z
                                       :u_dir :x
                     )
               )
               (RECTANGLE
                  (gpnt2d (/ horiz_punch 2) (/ vert_punch 2))   (gpnt2d (/ horiz_punch -2) (/ vert_punch -2))
               )
               (EXTRUDE
                 :part tool-part
                 :distance (- depth thickness)
               )
               ( delete_2d :all_2d)               
               (RECTANGLE
                  (gpnt2d (/ horiz 2) (/ vert 2))   (gpnt2d (/ horiz -2) (/ vert -2))
               )
               (EXTRUDE
                 :part tool-part
                 :distance height
               )
               (setq result (sha-body-of-tool-part tool-part))
               (delete_3d (sha-absolute-name tool-wp))
               (delete_3d tool-part)
       )
       result
    )
)

;=================================================
;
;   TRUMPF MULTIBEND TOOL
;
;=================================================


(defun sha-multibend-profile

  (&key
   thick
   B 
   H
   F
   U
   Z
   delta_x
   stamp_dir
   thickness
   resolution
   &allow-other-keys
  )
  (let* (
         (BF      (+ B (* 2 F)))
         (BF2     (/ BF 2))
         (HF      (- H delta_x))
         (HFF     (+ HF F))
         (Ri      thickness)
         (tool-wp   (sha-tool-wp))
        )
        (create_workplane :new
                          :name tool-wp
                          :world_origin
        )
        ;********* create Profile **********
        (RECTANGLE
           (gpnt2d BF2 HFF)   (gpnt2d (- BF2) (* -1 U ))
        )
        ;******** create Adjustpoints ******
        (C_POINT
           (gpnt2d 0 Z) 
           (gpnt2d BF2 Z)
           (gpnt2d (- BF2) Z)
           (gpnt2d 0 (- U))
           (gpnt2d BF2 (- U))
           (gpnt2d (- BF2) (- U))
           (gpnt2d 0 HFF)
           (gpnt2d BF2 HFF)
           (gpnt2d (- BF2) HFF)
        )
        (setq result (sha-profile-of-wp tool-wp))
        (delete_3d (sha-absolute-name tool-wp))
        (unless (sd-num-equal-p 0 Z)
          (when (equal 'SHA_STAMP (INQ-ACTIVE-ACTION-SYMBOL 0))
            (PUT-BUFFER (format nil ":adjust_point 0,0 ~A" (gpnt2d 0 Z)))
          )
        )
        result
  )
)

(defun sha-multibend-tool
  (&key
   B 
   H
   F
   U
   Z
   delta_x
   stamp_dir
   thickness
   material
   resolution
   &allow-other-keys
  )

  (let* ((result nil)
         (tool-part (sha-tool-part))
         (tool-wp   (sha-tool-wp))
         (B2        (/ B 2))
         (allowance (* -1 delta_x))
         (material-rowkey (getf MATERIAL :ROWKEY))
        )  
    (progn 
      (if (equal stamp_dir :DOWN)           ;create a new wp for direction DOWN
            (create_workplane :new
                              :name tool-wp
                              :pt_dir :origin (gpnt3d 0 0 (- thickness))
                              :normal :neg_z
                              :u_dir :x
            )
            (create_workplane :new            ;create a new wp for direction UP
                              :name tool-wp
                              :pt_dir :origin (gpnt3d 0 0 0)
                              :normal :z
                              :u_dir :x
            )
      )
      (RECTANGLE (gpnt2d B2 Z) (gpnt2d (- B2) (* thickness -10)))
      (SHA_NEW_BY_OUTLINE
        :sheet_part tool-part
        :material
        :MATERIAL_PLIST material-rowkey
        :KEEP_WP :ON)
      (sd-call-cmds
        (SHA_ADD_LIP 
          :lip_edge (sd-call-cmds (GET_SELECTION :NO_HIGHLIGHT :focus_type *sd-edge-3d-seltype*
                                    :curr_part_only
                                    :select (gpnt3d 0 Z 0)))
          :allow-implicit-bends :on
          :bend_process "multibend_bend"
          :radius :BEND_TOOL_KEY (list :RADIUS thickness :ANGLE (sd-deg-to-rad 90)  
                                       :THICK thickness :ALLOWANCE allowance)
          :lip_length  H
          :check_sheet :off
        )
        :failure nil
        :success (MOVE :faces (gpnt3d 0 (* thickness -10) (* thickness -0.5))
                       :dynamic_transformation 
                       :two-points-method :on (gpnt3d 0 (* thickness -10) 0) (gpnt3d 0 (- U) 0))
      )

      (setq result (sha-body-of-tool-part tool-part))
      (delete_3d (sha-absolute-name tool-wp))
      (delete_3d tool-part)
       result
    )
  )
)

