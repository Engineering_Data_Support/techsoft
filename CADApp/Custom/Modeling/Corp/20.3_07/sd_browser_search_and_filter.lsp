;% Automatically written on 03/05/2021 at 13:04:03
;% Creo Elements/Direct Modeling Revision: 19.0 (19.0)


(oli:sd-create-browser-search
  "parcel-gbrowser" :NAME "UNNAMED-SEARCH"
  :TITLE "Unbenannte Suche"
  :CASE-SENSITIVE T
  :MATCH :ALL
  :CRITERIA '((:COLUMN :CONTENTS-NAME :OPERATION :EQUALS :VALUE
                       "10204215")))
(oli:sd-create-browser-filter
  "parcel-gbrowser" :NAME "UNNAMED-FILTER"
  :TITLE "Unbenannter Filter"
  :CASE-SENSITIVE T
  :MATCH :ALL
  :CRITERIA '((:COLUMN :CONTENTS-NAME :OPERATION :EQUALS :VALUE
                       "20280090")))
(oli:sd-create-browser-filter
  "parcel-gbrowser" :NAME "SolidPower-Material"
  :TITLE "Keinen SolidPower Werkstoff"
  :CASE-SENSITIVE NIL
  :MATCH :ALL
  :CRITERIA '((:COLUMN :IS-PART :OPERATION :EQUALS :VALUE "Yes")
              (:COLUMN :SOLIDPOWER-MATERIAL :OPERATION :EQUALS :VALUE
                       "")))
(oli:sd-create-browser-filter
  "parcel-gbrowser" :NAME "SolidPower-Normteil"
  :TITLE "Kein SolidPower Normteil"
  :CASE-SENSITIVE NIL
  :MATCH :ALL
  :CRITERIA '((:COLUMN :SOLIDPOWER-PART :OPERATION :EQUALS :VALUE
                       "n/a")))