{  @(#)  awm_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of COBRA
}

                                { >>>if false }

{ LOLAMAX = 246 }

{ General messages }

LET Awm_msg_1  "Pointez SUBPART ou CONFIRM pour charger :"
LET Awm_msg_2  "Pointez CONFIRM pour renommer le dessin en :"
LET Awm_msg_3  "Identifiez la pi�ce pour info ou ACTIVE"
LET Awm_msg_4  "Le dessin n'a pas �t� charg� � partir de WorkManager"
LET Awm_msg_5  "Saisissez le 'Nom' de la classe pi�ce pour le cartouche actif :"
LET Awm_msg_6  "Saisissez le 'Nom' de la classe document pour le cartouche actif :"
LET Awm_msg_7  "Identifiez la pi�ce � stocker ou ACTIVE"
LET Awm_msg_8  "Pointez CONFIRM pour effacer l'ID dessin dans ME10"
LET Awm_msg_9  "Lancement de la connexion WorkManager ..."
LET Awm_msg_10 "Argument incorrect pour la fonction arithm�tique de ME10INTF"
LET Awm_msg_11 "D�marrez WorkManager puis r�-essayez l'action"
LET Awm_msg_12 "ERREUR dans WorkManager"
LET Awm_msg_13 "Aucune r�f�rence trouv�e pour la mise � jour"
LET Awm_msg_14 "Tablette non connect�e"
LET Awm_msg_15 "D�marrez WorkManager ou utilisez 'DDE_ENABLE ON'  avec WorkManager puis r�-essayez l'action"
LET Awm_msg_16 "ATTENTION ! : la pi�ce de Niveau 0 du dessin 1 se trouve encore au niveau 1"
LET Awm_msg_17 "ATTENTION !: La pi�ce de Niveau 0 du dessin 2 se trouve encore au niveau 1"
LET Awm_msg_18 "ATTENTION !: La pi�ce de Niveau 0 du dessin mis � jour se trouve encore au niveau 1"
DEFINE Awm_msg_19 ("Renommez la pi�ce " + Adu_part_y + " en " + Adu_part_x)
END_DEFINE
LET Awm_msg_20 "S�lectionnez l'�l�ment dont vous d�sirez obtenir de l'aide"
LET Awm_msg_35 "Drawing BOM table. Wait..."
LET Awm_msg_36 "Add PDF limited to unmodified drawings"

{ Menu texts }

DEFINE Awm_mtext_1
		 CENTER "WM"
END_DEFINE
DEFINE Awm_mtext_2
		 CENTER "Desktop"
END_DEFINE
LET Awm_mtext_3  "CHARGER"
LET Awm_mtext_4  "SOUS-ENS"
LET Awm_mtext_5  "ABSOLU"
LET Awm_mtext_6  "STOCKER"
LET Awm_mtext_7  "Tout"
LET Awm_mtext_8  "Pi�ce"
LET Awm_mtext_9  "ACTIVE"
LET Awm_mtext_10 "ECRASER"
LET Awm_mtext_11 "RESERVER"
LET Awm_mtext_12 "ID"
LET Awm_mtext_13 "Dessin"
LET Awm_mtext_14 "Article-WM"
LET Awm_mtext_15 "INFO"
LET Awm_mtext_16 "Niveau 0"
LET Awm_mtext_17 "EFFACER ID"

LET Awm_mtext_18 "M A JOUR"
LET Awm_mtext_19 "INSCRIRE"
LET Awm_mtext_20 "Noms TR"
LET Awm_mtext_21 "Valeurs TR"
LET Awm_mtext_22 "CHARGER CT"
LET Awm_mtext_23 ""
LET Awm_mtext_24 "AFFECTER"
LET Awm_mtext_25 "Article-WM"
LET Awm_mtext_26 "ACTIVE"
LET Awm_mtext_27 "NIVEAU 0"
LET Awm_mtext_28 "NIVEAUX"
LET Awm_mtext_29 "ANN_RESERV"
LET Awm_mtext_30 ""
LET Awm_mtext_31 ""
LET Awm_mtext_32 ""
LET Awm_mtext_33 "AFFIC DONN"
LET Awm_mtext_34 ""

LET Awm_mtext_35 "PARAMETRES"
LET Awm_mtext_36 "Classes"
LET Awm_mtext_37 "AFFECTER"
LET Awm_mtext_38 "LIBERER"
LET Awm_mtext_39 "Largeur"
LET Awm_mtext_40 "Maj/Min"
LET Awm_mtext_41 "Lignes"
LET Awm_mtext_42 "Bouclage"
LET Awm_mtext_43 "Pr�cision"
LET Awm_mtext_44 "OPTIONS"
LET Awm_mtext_45 "Majuscules"
LET Awm_mtext_46 "Oui"
LET Awm_mtext_47 "Minuscules"
LET Awm_mtext_48 "Non"
LET Awm_mtext_49 "MODIFIER"
LET Awm_mtext_50 "AFFICH REF"
LET Awm_mtext_51 "Tout"
LET Awm_mtext_52 "S�lection"
LET Awm_mtext_53 "AFFICHER"
LET Awm_mtext_54 "SELECT."
LET Awm_mtext_55 ""
LET Awm_mtext_56 ""
LET Awm_mtext_57 ""
LET Awm_mtext_58 ""
LET Awm_mtext_59 ""

DEFINE Awm_mtext_60
		 CENTER "M.Principal AIP"
END_DEFINE
DEFINE Awm_mtext_61
		 CENTER "Menu Principal AIP"
END_DEFINE
LET Awm_mtext_62 "FICH."
LET Awm_mtext_63 "CRTCHE"
LET Awm_mtext_64 "CONF-CT"
LET Awm_mtext_65 ""
LET Awm_mtext_66 ""
LET Awm_mtext_67 ""
LET Awm_mtext_68 ""
LET Awm_mtext_69 ""

LET Awm_mtext_70 "STOCKER DESSIN"
LET Awm_mtext_71 "STOCKER PIECE"
LET Awm_mtext_72 "Ecraser le document actif"
LET Awm_mtext_73 "Cr�er une nouvelle version"
LET Awm_mtext_74 "Cr�er un nouveau document"
LET Awm_mtext_75 "Cr�er un nouvel article et un document"
LET Awm_mtext_76 "ANNULER"

DEFINE Awm_mtext_77
		 CENTER "Data Management"
END_DEFINE
DEFINE Awm_mtext_78
		 CENTER "Data Management"
END_DEFINE
LET Awm_mtext_79 "Sous-ens"
LET Awm_mtext_80 "Piece"
DEFINE Awm_mtext_81
		 CENTER "Ouvrir"
END_DEFINE
DEFINE Awm_mtext_82
		 CENTER "Editeur"
END_DEFINE

DEFINE Awm_mtext_83
		 CENTER "Ajouter PDF"
END_DEFINE

{ Table Titles }

LET Awm_ttitle_1 "NON"
LET Awm_ttitle_2 "NOMS TEXTE REF"
LET Awm_ttitle_3 "NOMS & VALEURS TEXTE de REF"
LET Awm_ttitle_4 "NOMS"
LET Awm_ttitle_5 "VALEURS"
LET Awm_ttitle_6 "INFOS TEXTE de REFERENCE [ @s0 �l�ments trouv�s ]"
LET Awm_ttitle_7 "TABLE"
LET Awm_ttitle_8 "NOM"
LET Awm_ttitle_9 "COL."
LET Awm_ttitle_10 "LARG."
LET Awm_ttitle_11 "LIGNES"
LET Awm_ttitle_12 "PRECISION"
LET Awm_ttitle_13 "BOUCLAGE"
LET Awm_ttitle_14 "MAJ/MIN"

{ New General messages }
LET Awm_msg_21
    "Identifiez une pi�ce pour l'affectation des donn�es article-WM ou ACTIVE ou NIVEAU 0"
LET Awm_msg_22
    "Nom de pi�ce incorrect :"
LET Awm_msg_23
    "Saisissez le nombre de niveaux de la structure :"

{ Message if BOM not enabled }
LET Awm_msg_24
	"Erreur de configuration : le module NOMENC n'est pas activ�"

LET Awm_msg_25
	"Une op�ration de M�J/CMP est encore en cours"
LET Awm_msg_26
	"D�finition des informations sur les pi�ces :"
LET Awm_msg_27
	"Lecture de l'arbre des pi�ces ..."
LET Awm_msg_28
	"Interrogation des informations sur les pi�ces :"
LET Awm_msg_29 "Saisissez le nom du dessin :"
LET Awm_msg_30 "Il n'existe pas de fichier de dessin"
LET Awm_msg_31 "Aucune classe de dessin enregistr�e"
LET Awm_msg_32 "Saisissez la version du dessin :"
LET Awm_msg_33 "Saisissez la description :"
LET Awm_msg_34 "Saisissez une note de modification :"

{* Toolbar Item Text Messages for NewUI (PELOOK = 2,3) *}

LET Awm_ttext_1  "A"
LET Awm_ttext_2  "A0"
LET Awm_ttext_3  "A1"
LET Awm_ttext_4  "A2"
LET Awm_ttext_5  "A3"
LET Awm_ttext_6  "A4"
LET Awm_ttext_7  "Absolu"
LET Awm_ttext_8  "Tout"
DEFINE Awm_ttext_9
 ("Attrib niveaux : "+ (STR(Awmc_val_assign_level)))
END_DEFINE
LET Awm_ttext_10  "Affecter Pi�ces"
LET Awm_ttext_11  "Attrib r�f�rences"
LET Awm_ttext_12  "B"
LET Awm_ttext_13  "Fmt NOMENC"
LET Awm_ttext_14  "Param�tres NOMENC"
LET Awm_ttext_15  "NOMENC"
LET Awm_ttext_16  "Config NOMENC"
LET Awm_ttext_17  "C "
DEFINE Awm_ttext_18
  ("Maj-Min : " + Tr_case_convention_value)
END_DEFINE
LET Awm_ttext_19  "Maj-Min"
LET Awm_ttext_20  "Changer"
LET Awm_ttext_21  "Eff ID"
LET Awm_ttext_22  "Continuer"
LET Awm_ttext_23  "Cr�er par identif"
LET Awm_ttext_24  "Cr�er depuis liste"
LET Awm_ttext_25  "Cr�er depuis table"
LET Awm_ttext_26  "Actif"
LET Awm_ttext_27  "D "
LET Awm_ttext_28  "Suppr Rep�res"
LET Awm_ttext_29  "ADVANCED DesignManager"
LET Awm_ttext_30  "DESIGNER DesignManager"
LET Awm_ttext_31  "DESKTOP DesignManager"
LET Awm_ttext_32  "EXPERT DesignManager"
LET Awm_ttext_33  "EXPERT CLASSIC DesignManager"
LET Awm_ttext_34  "Noms trac�"
LET Awm_ttext_35  "Valeurs trac�"
LET Awm_ttext_36  "ID Dessin"
LET Awm_ttext_37  "Dessin"
LET Awm_ttext_38  "E"
LET Awm_ttext_39  "Fin"
LET Awm_ttext_40  "Dispos rep�re"
LET Awm_ttext_41  "Horizontal"
DEFINE Awm_ttext_42
  ("Incr�menter : " + (STR(Awmc_bom_val_increment)))
END_DEFINE  
LET Awm_ttext_43  "Info Dessin"
LET Awm_ttext_44  "Info"
LET Awm_ttext_45  "Gauche"
DEFINE Awm_ttext_46
  ("Lignes : " + Tr_set_lines_value)
END_DEFINE  
LET Awm_ttext_47  "Lignes"
LET Awm_ttext_48  "Chrg cartouche"
LET Awm_ttext_49  "Chrg"
LET Awm_ttext_50  "Inf�rieur"
LET Awm_ttext_51  "Nv Dessin"
LET Awm_ttext_52  "Nv pce&dessin"
LET Awm_ttext_53  "Nv version"
LET Awm_ttext_54  "Non"
LET Awm_ttext_55  "Oui"
LET Awm_ttext_56  "Ouvr NOMENC"
LET Awm_ttext_57  "Options"
LET Awm_ttext_58  "Ecraser"
LET Awm_ttext_59  "Cr�er Rep�res"
LET Awm_ttext_60  "Pi�ce"
LET Awm_ttext_61  "Rep�re"
DEFINE Awm_ttext_62
  ("Pr�cision : " + Tr_set_precision_value)
END_DEFINE  
LET Awm_ttext_63  "Pr�cision"
LET Awm_ttext_64  "Retracer Rep�res"
LET Awm_ttext_65  "Noms r�f"
LET Awm_ttext_66  "Valeurs r�f"
LET Awm_ttext_67  "Suppr Rep�re Tout"
LET Awm_ttext_68  "Suppr Rep�res"
LET Awm_ttext_69  "R�serv Dessin"
LET Awm_ttext_70  "CompDess"
LET Awm_ttext_71  "Droite"
LET Awm_ttext_72  "Analys (ajout)"
LET Awm_ttext_73  "Analys (eff)"
DEFINE Awm_ttext_74
  ("Analys niveaux : " + (STR(Awmc_val_scan_level)))
END_DEFINE
LET Awm_ttext_75  "S�lect classes"
LET Awm_ttext_76  "S�lect"
LET Awm_ttext_77  "D�f extr�mit� ligne"
LET Awm_ttext_78  "Param�tres"
LET Awm_ttext_79  "Aff NOMENC"
LET Awm_ttext_80  "Aff Donn�es"
LET Awm_ttext_81  "Aff Etat Pce"
LET Awm_ttext_82  "Aff r�f�rences"
DEFINE Awm_ttext_83
  ("Lancer Rep�res : " + (STR(Awmc_bom_val_start_posnr)))
END_DEFINE
LET Awm_ttext_84  "Stock tt"
LET Awm_ttext_85  "Stock Pce"
LET Awm_ttext_86  "Stock sous-ens"
LET Awm_ttext_87  "Stock"
LET Awm_ttext_88  "Sous-ens"
LET Awm_ttext_89  "Conf CT"
LET Awm_ttext_90  "Conf CT-NOMENC"
LET Awm_ttext_91  "Config CT"
LET Awm_ttext_92  "Table vers Dessin"
LET Awm_ttext_93  "Cartouche"
LET Awm_ttext_94  "Pce niv0"
LET Awm_ttext_95  "Niveau 0"
LET Awm_ttext_96  "UTIL."
LET Awm_ttext_97  "Non attrib"
LET Awm_ttext_98  "Annul. R�serv"
LET Awm_ttext_99  "M�J Dessin"
LET Awm_ttext_100  "M�J CT"
LET Awm_ttext_101  "M�J depuis ME10"
LET Awm_ttext_102  "M�J"
LET Awm_ttext_103  "Sup�rieur"
LET Awm_ttext_104  "Vertical"
LET Awm_ttext_105  "ID pce WM"
LET Awm_ttext_106  "WMDT"
DEFINE Awm_ttext_107 
  ("Largeur : " + Tr_set_width_value)
END_DEFINE   
LET Awm_ttext_108  "Largeur"
LET Awm_ttext_109  "Bouclage"
LET Awm_ttext_110  "DIN"
LET Awm_ttext_111  "ISO"

LET Awm_ttext_112  "Workspace"
LET Awm_ttext_113  "Rechercher"
LET Awm_ttext_114  "Ajouter PDF"
LET Awm_mttext_1   "Drawing Manager"

{* Toolbar Messages New User Interface (PELOOK = 2,3) *}

LET Awm_tmess_1 "ADU non accept� par l'interface utilisateur Windows"

{ >>>endif }
