{  @(#)  bom_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of BOM-Module
}

{ >>>if false }

{ LOLAMAX = 162 }

{ General messages }

LET Awm_bom_msg_1
	"カレント BOM の位置番号をすべて削除するには CONFIRM を入力して下さい"
LET Awm_bom_msg_2
    "位置番号レイアウトがロードされていません \nレイアウトを選択してからアクションを再実行して下さい"
LET Awm_bom_msg_3
    "削除する位置番号を指定して下さい"
LET Awm_bom_msg_4
    "位置番号が指定されていません"
LET Awm_bom_msg_5
    "要素は位置情報を含んでいません"
LET Awm_bom_msg_6
    "位置番号を定義するパーツを指定して下さい"
LET Awm_bom_msg_7
    "パーツはカレント BOM ツリー内にはありません"
LET Awm_bom_msg_8
    "すでに位置番号が定義されています"
LET Awm_bom_msg_9
    "このパーツにはパーツ要素は定義されていません"
LET Awm_bom_msg_10
    "位置引出線の始点を入力するか または CONTINUE を押して次のパーツへ進んで下さい"
LET Awm_bom_msg_11
    "水平引出線の始点を入力して下さい \nEND を押して終了するか または CONTINUE を押して次のパーツへ進んで下さい"
LET Awm_bom_msg_12
    "鉛直引出線の始点を入力して下さい \nEND を押して終了するか または CONTINUE を押して次のパーツへ進んで下さい"
LET Awm_bom_msg_13
    "位置引出線上の点を指定して下さい \nEND を押して終了するか または CONTINUE を押して次のパーツへ進んで下さい"
LET Awm_bom_msg_14
    "BOM 要素を選択して下さい"
LET Awm_bom_msg_15
	"不適当な入力です"
LET Awm_bom_msg_16
	"BOM はこのパーツを含んでいません"
LET Awm_bom_msg_17
    "位置番号がないとパーツが見つかりません"
LET Awm_bom_msg_18
    "位置引出線を再描画するために位置番号を指定して下さい"
LET Awm_bom_msg_19
	"位置フラグを描画するために BOM 要素を選択して下さい"
LET Awm_bom_msg_20
    "不適当なマクロファイルです"
LET Awm_bom_msg_21
    "書き換えファイルがありません"
LET Awm_bom_msg_22
	"不適当なパーツ構造: 位置フラグレイアウトは子パーツを含んではいけません"
LET Awm_bom_msg_23
    "終点 'UPPER LEFT' が定義されていません"
LET Awm_bom_msg_24
    "終点 'UPPER RIGHT' が定義されていません"
LET Awm_bom_msg_25
    "終点 'LOWER LEFT' が定義されていません"
LET Awm_bom_msg_26
    "終点 'LOWER RIGHT' が定義されていません"
LET Awm_bom_msg_27
	"BOM ヘッダにするパーツを指定して下さい"
LET Awm_bom_msg_28
	"BOM 要素にするパーツを指定して下さい"
LET Awm_bom_msg_29
	"不適当なパーツ構造: BOM レイアウトは BOM ヘッダと BOM 要素で \n構成されなければなりません"
LET Awm_bom_msg_30
	"オプションを選択するか または 位置引出線の存在する終点を指定して下さい"
LET Awm_bom_msg_31
	"LEFT または RIGHT または位置引出線の新しい終点を入力して下さい"
LET Awm_bom_msg_32
	"位置引出線の新しい終点を入力して下さい"
LET Awm_bom_msg_33
	"BOM が WorkManager から定義されていません !"
LET Awm_bom_msg_34
    "BOM レイアウトがロードされていません"
LET Awm_bom_msg_35
    "BOM 内の点を指定して下さい"
LET Awm_bom_msg_36
	"BOM を作成するためのアセンブリを指定して下さい"
LET Awm_bom_msg_37
    "WM-BOM は有効ではありません!"
LET Awm_bom_msg_38
	"アセンブリが指定されていません"
LET Awm_bom_msg_39
	"新しい位置番号を入力して下さい"
LET Awm_bom_msg_40
	"要素が WM-BOM と対応していません"
LET Awm_bom_msg_41
	"カレント BOM にするアセンブリを指定して下さい"
LET Awm_bom_msg_42
    "BOM ヘッダに対応する ME10 パーツはありません"
LET Awm_bom_msg_43
"WorkManager の BOM を更新するには CONFIRM を入力して下さい"
LET Awm_bom_msg_44
"現在の ME10 パーツ構造で ME10 の BOM を更新するには CONFIRM を\n入力して下さい"
LET Awm_bom_msg_45 ""
LET Awm_bom_msg_46
"ME10 BOM の位置番号をすべて削除するには CONFIRM を入力して下さい"
LET Awm_bom_msg_47
	"WorkManager の BOM をオープンするためにはアセンブリまたは CURRENT を指定して下さい"
LET Awm_bom_msg_48
	"パーツにはパーツ要素データが結びついていません"
LET Awm_bom_msg_49
	"開始位置番号を入力して下さい:"
LET Awm_bom_msg_50
	"位置番号増分値を入力して下さい:"
LET Awm_bom_msg_51
	"BOM 要素にするパーツを指定して下さい"
LET Awm_bom_msg_52
    "終点 'UPPER' が定義されていません"
LET Awm_bom_msg_53
    "終点 'LOWER' が定義されていません"
LET Awm_bom_msg_54
    "終点 'RIGHT' が定義されていません"
LET Awm_bom_msg_55
    "終点 'LEFT' が定義されていません"

LET Awm_bom_msg_56
    "部品は複数の BOM の中にあります"
LET Awm_bom_msg_57
    "レベル n の BOM とレベル 1 の BOM は結合できません"
LET Awm_bom_msg_58
    "レベル n の BOM の中では更新できません"

LET Awm_bom_msg_59
    "構造レベルの数を入力して下さい:"
LET Awm_bom_msg_21
    "BOM データをスキャンするパーツ または CURRENT または TOP を指定して下さい"
LET Awm_bom_msg_22
    "不適当なパーツ名:"

{ Menu texts }

LET Awm_bom_mtext_1 "位置番"
LET Awm_bom_mtext_2 "BOM"
LET Awm_bom_mtext_3 "BOM設定"
LET Awm_bom_mtext_4 "位置作成"
LET Awm_bom_mtext_5 "リスト"
LET Awm_bom_mtext_6 "指　定"
LET Awm_bom_mtext_7 "水　平"
LET Awm_bom_mtext_8 "鉛　直"
LET Awm_bom_mtext_9 "再描画"
LET Awm_bom_mtext_10 "位置番号"
LET Awm_bom_mtext_11 "削　除"
LET Awm_bom_mtext_12 "全　て"
LET Awm_bom_mtext_13 "開始位置"
LET Awm_bom_mtext_14 "増　分"
LET Awm_bom_mtext_15 "位置ﾚｲｱｳﾄ"
LET Awm_bom_mtext_16 "出　力"
LET Awm_bom_mtext_17 "図　面"
LET Awm_bom_mtext_18 "BOM ﾚｲｱｳﾄ"
LET Awm_bom_mtext_19 "BOM ｵｰﾌﾟﾝ"
LET Awm_bom_mtext_20 "トップ"
LET Awm_bom_mtext_21 "パーツ"
LET Awm_bom_mtext_22 "表　示"
LET Awm_bom_mtext_23 "BOM"
LET Awm_bom_mtext_24 ""
LET Awm_bom_mtext_25 "更　新"
LET Awm_bom_mtext_26 ""
LET Awm_bom_mtext_27 ""
LET Awm_bom_mtext_28 ""
LET Awm_bom_mtext_29 ""
LET Awm_bom_mtext_30 "ME10 -> WM"
LET Awm_bom_mtext_31 "位置番号"
LET Awm_bom_mtext_32 "カレント"
LET Awm_bom_mtext_33 "パーツ状態"
LET Awm_bom_mtext_34 "継　続"
LET Awm_bom_mtext_35 "終　了"
LET Awm_bom_mtext_36 "線終点"
LET Awm_bom_mtext_37 "設　定"
LET Awm_bom_mtext_38 "　上"
LET Awm_bom_mtext_39 "　下"
LET Awm_bom_mtext_40 "　左"
LET Awm_bom_mtext_41 "　右"
LET Awm_bom_mtext_42 "ストア"
LET Awm_bom_mtext_43 "BOM ﾚｲｱｳﾄ"
LET Awm_bom_mtext_44 "削　除"
LET Awm_bom_mtext_45 "BOM ﾃｰﾌﾞﾙ"
LET Awm_bom_mtext_46 "DIN"
LET Awm_bom_mtext_47 "ISO"
LET Awm_bom_mtext_48 "ユーザ"
LET Awm_bom_mtext_49 "A"
LET Awm_bom_mtext_50 "B"
LET Awm_bom_mtext_51 "C"

LET Awm_bom_mtext_52 "BOMｽｷｬﾝ"
LET Awm_bom_mtext_53                                ""
LET Awm_bom_mtext_54 "追　加"
LET Awm_bom_mtext_55 "クリア"
LET Awm_bom_mtext_56 "カレント"
LET Awm_bom_mtext_57 "トップ"
LET Awm_bom_mtext_58 "レベル"

{ Table Titles }

LET Awm_bom_ttitle_1  "パーツ名"
LET Awm_bom_ttitle_2  "パーツ-ID"
LET Awm_bom_ttitle_3  "部品"
LET Awm_bom_ttitle_4  "BOM"
LET Awm_bom_ttitle_5  "位置番号"
LET Awm_bom_ttitle_6  "X"
LET Awm_bom_ttitle_7  "エラー"
LET Awm_bom_ttitle_8  "元"
LET Awm_bom_ttitle_9  "カレント ME10-BOM"
LET Awm_bom_ttitle_10 "ME10-パーツ状態情報"
LET Awm_bom_ttitle_11 "カレント BOM 要素"
LET Awm_bom_ttitle_12 "メッセージ"

{ >>>endif }
