{  ~f~ Awmc_call_help_viewer - Calls up the help viewer
***********************************************************************}
{   DESCRIPTION: This macro calls up the predifined help viewer and     }
{   ------------ tells it which file it has to open and to which label  }
{   it has to go                                                        }
{                                                                       }
{   PARAMETER-DEFINITION: Help_file  --> bears help file that was found }
{   --------------------- Help_label --> bears Help_label that was  "   }
{                                                                       }
{   INPUT: Help_file Help_label                                         }
{   ------                                                              }
{                                                                       }
{   RETURN-VALUE: -                                                     }
{   -------------                                                       }
{                                                                       }
{                                                                       }
{ **********************************************************************}
DEFINE Awmc_call_help_viewer            {call up the corresponding help-viewer}
PARAMETER Help_file
PARAMETER Help_label

LOCAL Awm_os
	INQ_ENV 10
	LET Awm_os (INQ 4)
	IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
		{ DOS, Windows, Windows NT, Windows 95 }
		Awmc_call_help_viewer_pc Help_file Help_label
	ELSE
		{ HP-UX }
		Awmc_call_help_viewer_ux Help_file Help_label
	END_IF
END_DEFINE

DEFINE Awmc_call_help_viewer_ux         {call up the corresponding help-viewer}
  PARAMETER Help_file
  PARAMETER Help_label

  LOCAL Rem_cmd

  LET Rem_cmd           (Awm_call_viewer_script+' '+Awm_val_help_path+'/')

  IF (Help_file<>'')
    LET Rem_cmd         (Rem_cmd+Help_file+'.html')
    IF (Help_label<>'')
      LET Rem_cmd       (Rem_cmd+'#'+Help_label)
    END_IF
  ELSE
    LET Rem_cmd         (Rem_cmd+'error.html')
  END_IF
  RUN GRAPHIC Rem_cmd
END_DEFINE

DEFINE Awmc_call_help_viewer_pc         {call up the corresponding help-viewer}
  PARAMETER Help_file
  PARAMETER Help_label
  LOCAL Handle
  LOCAL Status
  LOCAL Cmd_str
  LOCAL Destination
  LOCAL Loop_count

  IF (Help_file<>'')
    LET Destination (Awm_val_help_path+'/'+Help_file+'.htm')
    IF (Help_label<>'')
      LET Destination (Destination+'#'+Help_label)
    END_IF
  ELSE
	LET Destination (Awm_val_help_path+'/'+'error.htm')
  END_IF
  LET Destination (Destination+',,0xFFFFFFFF,0x0,,,')

  LET Loop_count 0
  LET Handle (DDE_INITIATE 'NETSCAPE' 'WWW_OpenUrl')
  IF(Handle = ERROR)
	LET Status (WINEXEC Awmc_val_netscape_exe)
	LOOP
		WAIT 2
		LET Loop_count (Loop_count + 1)
		LET Handle (DDE_INITIATE 'NETSCAPE' 'WWW_OpenUrl')
		EXIT_IF ((Handle <> ERROR) OR (Loop_count > 15))
	END_LOOP
  END_IF

  IF(Handle <> ERROR)
    LET Status (DDE_REQUEST Handle Destination)
    IF (Status = ERROR)
      DISPLAY "Error calling Help Viewer"
	  CANCEL
    END_IF
    LET Status (DDE_CLOSE Handle)

    LET Handle (DDE_INITIATE 'NETSCAPE' 'WWW_Activate')
    IF(Handle <> ERROR)
      LET Status (DDE_REQUEST Handle '0xFFFFFFFF,0x0')
      LET Status (DDE_CLOSE Handle)
    END_IF
  ELSE
    DISPLAY "Error calling Help Viewer"
	CANCEL
  END_IF

END_DEFINE
