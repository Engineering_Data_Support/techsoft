{  @(#)  $Header: /data/cvsrepositories/cvswm/WorkManager/aip_src/me10/startup/startup.m,v 1.20 2004/09/27 08:13:14 nwightma Exp $ Revision 1.04 $ 04/23/01

  Copyright (c) 1996-2003 CoCreate Software GmbH & Co. KG.  All Rights Reserved.

 ***********************************************************************}
{*                                                                     *}
{*  WorkManager Desktop                                                *}
{*  Version 11.50: Load ME10 macros                                      *}
{*                                                                     *}
{***********************************************************************}

DEFINE Try_input
{+---------------------------------------------------------------------------+
 | FUNCTION:   Inputs a file if the file exists (ME10 version of macro)      |
 |                                                                           |
 | PARAMETERS: file_name       string      Name of file to input.            |
 |                                                                           |
 | RETURNS:    1 if an error was trapped.                                    |
 |             0 if no error was trapped.                                    |
 +---------------------------------------------------------------------------+}
  PARAMETER Filename

  TRAP_ERROR
  INPUT Filename
  LET Dms_return_val (CHECK_ERROR) END
END_DEFINE

DEFINE Awm_get_desktop_directory
   LET Dms_return_val (GETENV 'WMDT_DIR_CUSTOM')
    IF (Dms_return_val = '')
        {* does this work for annotation + drafting? *}
        LET Dms_return_val (MEDIR + '/desktop/')
    END_IF
END_DEFINE

{********************}
{* read environment *}
{********************}
DEFINE Awm_read_env

    Awm_get_desktop_directory
    {* still set the Wmdt_dir macro variable *}
    LET Wmdt_dir Dms_return_val

END_DEFINE

Awm_read_env
DELETE_MACRO Awm_read_env

{***************************************************************************}
{* load macros and operating system dependent tools                        *}
{***************************************************************************}
DEFINE Wmdt_get_local_lang_dir
LOCAL Lang
LOCAL Lang_short
LOCAL Old_dir

    LET Lang (GETENV "LANG")
    IF (((UPC Lang) = 'POSIX') OR ((UPC Lang) = 'C')
        OR ((UPC Lang) = 'N-COMPUTER'))
      LET Wmdt_local_lang_dir ""
    ELSE_IF (Lang = '')
      LET Wmdt_local_lang_dir ""
    ELSE
      LET Lang_short (SUBSTR Lang 1 2)
      IF (Lang_short = "en")
        LET Wmdt_local_lang_dir ""
      ELSE_IF (Lang_short = "fr")
        LET Wmdt_local_lang_dir "local/french"
      ELSE_IF (Lang_short = "de")
        LET Wmdt_local_lang_dir "local/german"
      ELSE_IF (Lang_short = "it")
        LET Wmdt_local_lang_dir "local/italian"
      ELSE_IF (Lang_short = "ja")
        LET Wmdt_local_lang_dir "local/japanese"
      ELSE_IF (Lang_short = "es")
        LET Wmdt_local_lang_dir "local/spanish"
      ELSE
        LET Wmdt_local_lang_dir ("local/" + Lang)
        INQ_ENV 0
        LET Old_dir (INQ 302)
        TRAP_ERROR
        CURRENT_DIRECTORY (Wmdt_dir + '/aips/me10/' + Wmdt_local_lang_dir)
        IF (CHECK_ERROR)
          LET Wmdt_local_lang_dir ""
        END_IF
        CURRENT_DIRECTORY Old_dir
      END_IF
    END_IF
    LET Dms_return_val Wmdt_local_lang_dir
END_DEFINE

{****************************************************************}
{ Load a file from the WMDT_DIR/aips/me10/local/LANG/startup dir }
{ Cannot use serach path as its remembered for ever              }
{ this results in all localisation dirs being in the search path }
{ PARAMETER is the filename to load                              }
{****************************************************************}
DEFINE Awm_load_lang_file 
   PARAMETER filename
   LOCAL Old_pwd
   
   Wmdt_get_local_lang_dir
   
   INQ_ENV 0
   LET Old_pwd (INQ 302)
   
    TRAP_ERROR
    CURRENT_DIRECTORY (Wmdt_dir + '/aips/me10/' + Wmdt_local_lang_dir + '/startup')
    IF (CHECK_ERROR)
        CURRENT_DIRECTORY (Wmdt_dir + '/aips/me10/startup')
    END_IF
    
    INPUT filename
    
    CURRENT_DIRECTORY Old_pwd
END_DEFINE

{*******************}
{* add search path *}
{*******************}
Wmdt_get_local_lang_dir
SEARCH ADD 
    (Wmdt_dir + '/aips/me10/custom')
    (Wmdt_dir + '/aips/me10/startup')
END


{************************}
{* check presence of DM *}
{************************}
DEFINE Awm_is_drawing_manager_active
    LET Dms_return_val 0
END_DEFINE

DEFINE Awm_load_1300_dwg_active
  LOCAL Vers_no
  
  INQ_ENV 0
  LET Vers_no (INQ 2)
  IF(Vers_no >= 13.0)
    INPUT (Wmdt_dir + "/aips/me10/startup/dwgm_active.m")
  END_IF
END_DEFINE

Awm_load_1300_dwg_active
DELETE_MACRO Awm_load_1300_dwg_active

{***************************************************************************}
{* Check whether Annotation-Module/SD is active or not                       *}
{***************************************************************************}
DEFINE Awm_check_annotation_module
LOCAL Inq_string

    INQ_ENV 0
    LET Inq_String (INQ 301)
    IF ((POS Inq_String "HP DOCU") OR
      (POS Inq_String "CoCreate Annotation"))
    {* Global Variable for Annotation-Module   *}
    {*******************************************}
      LET Annotation_active 1

    {* Store a TB in a SD Drawing permit *}
    {*********************************** *}
      LET Awmc_val_store_with_tb (TRUE)
    ELSE
      LET Annotation_active 0
    END_IF
END_DEFINE
Awm_check_annotation_module

DEFINE Awm_load_lm
LOCAL Awm_os
LOCAL Old_pwd
LOCAL Lang_dir

    Wmdt_get_local_lang_dir
    LET Lang_dir Dms_return_val

    IF ( Lang_dir <> "" )
      LET Lang_dir ("/" + Lang_dir)
    END_IF

    { Get current directory }
    INQ_ENV 0
    LET Old_pwd (INQ 302)

    { Set new current directory, depending on local setting }
    TRAP_ERROR
    CURRENT_DIRECTORY (Wmdt_dir + '/aips/me10' + Lang_dir + '/startup')
    IF (CHECK_ERROR)
      CURRENT_DIRECTORY (Wmdt_dir + '/aips/me10/startup')
    END_IF

    { Switch back to previous current directory }
    CURRENT_DIRECTORY Old_pwd

END_DEFINE
Awm_load_lm
DELETE_MACRO Awm_load_lm

{**************************************************}
{* directory for ME10 <-> WorkManager filetransfer *}
{**************************************************}
DEFINE Awm_set_tmpdir
   LOCAL Awm_os
   {-------------------------------------------------------------------}
   { Directory of temporary files                                      }
   {-------------------------------------------------------------------}
   LET Awm_val_tmpdir (GETENV "METMPDIR")
   IF (Awm_val_tmpdir = "")
     LET Awm_val_tmpdir (GETENV "TMPDIR")
     IF (Awm_val_tmpdir = "")
       LET Awm_val_tmpdir (GETENV "TEMP")
       IF (Awm_val_tmpdir = "")
         LET Awm_val_tmpdir (GETENV "TMP")
         IF (Awm_val_tmpdir = "")
           INQ_ENV 10
           LET Awm_os (INQ 4)
           IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
             LET Awm_val_tmpdir 'C:\'      (* DOS,3.x,(NT/2000/XP),win98 *)
           ELSE
             LET Awm_val_tmpdir '/usr/tmp' (* UNIX *)
           END_IF
         END_IF
       END_IF
     END_IF
   END_IF
END_DEFINE
Awm_set_tmpdir
DELETE_MACRO Awm_set_tmpdir

DEFINE Awm_load_tools
LOCAL Awm_os
LOCAL Inq_string
LOCAL Lang

    LET Lang (GETENV "LANG")
    LET Lang (LWC(SUBSTR Lang 1 2))

    INQ_ENV 10
    LET Awm_os (INQ 4)
    IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
    { DOS, Windows 3.x, Windows NT/2000/XP, Windows 95 }
      INQ_ENV 0
      LET Inq_string (INQ 301)
      IF (POS Inq_string "ME10v")
        INPUT (Wmdt_dir + "/aips/me10/startup/pewmme10v.m")
      END_IF
      LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdt.mac")
      LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/aip_pc.mac")
      LET Enable_data_button 1
      DDE_ENABLE ON

      Awm_check_annotation_module
      IF (Annotation_active)
        LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdtamsd.mac")
      END_IF
    ELSE
    { HP-UX }
      INQ_ENV 0
      LET Inq_string (INQ 301)
      IF (POS Inq_string "ME10v")
        INPUT (Wmdt_dir + "/aips/me10/startup/pewmme10v.m")
      ELSE_IF (POS Inq_string "ME30")
        INPUT 'pewmme30.m'
      END_IF
      LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdt.mac")
      LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/aip_ux.mac")

      Awm_check_annotation_module

      IF (Annotation_active)
        LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdtamsd.mac")
      ELSE
        ACCEPT_CONNECTIONS
      END_IF

    END_IF
END_DEFINE
Awm_load_tools
DELETE_MACRO Awm_load_tools

{**********************************}
{* load ME10_7.xx specific macros *}
{**********************************}
DEFINE Awm_load_tools
LOCAL Vers_no
    INQ_ENV 0
    LET Vers_no (INQ 2)
    IF(Vers_no >= 7.0)
    LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdt_7.mac")
    END_IF
END_DEFINE
Awm_load_tools
DELETE_MACRO Awm_load_tools

{*************************************}
{* load message files from local dir *}
{*************************************}
Awm_load_lang_file 'awm_msg.m'
Awm_load_lang_file 'bom_msg.m'

{**************************************}
{* load DB-definitions from local dir *}
{**************************************}
Awm_load_lang_file 'db_defs.m'

{*************************}
{* load enable functions *}
{*************************}

INPUT (Wmdt_dir + "/aips/me10/startup/enable.m")

{*************}
{* Init menu *}
{*************}
DEFINE Awm_amsd_init_menu
  INQ_ENV 10
{ 2 = MFC look ; 3 = MIXED look ; 4 = FLUENT look }
    IF ((NOT Annotation_active) AND (NOT (((INQ 6)= 2) OR ((INQ 6) = 3) OR ((INQ 6) = 4))))
      Awmc_m_init_menu
    ELSE
      Awm_i_get_program_name
      Sm_dms_menu_type
    END_IF
END_DEFINE
Awm_amsd_init_menu
DELETE_MACRO Awm_amsd_init_menu

{***************************************************************************}
{* load profile or customize file                                          *}
{***************************************************************************}
DEFINE Awm_load_cust

  LET Wmdt_profile (GETENV "WMDT_PROFILE")
  IF ((Wmdt_profile = '') OR (Wmdt_profile = 'DESKTOP'))
{* WMDT-Standard-Customizing *}
  {*****************************}
    Try_input 'cust.m'
  ELSE
{* WMDT-Profile-Customizing *}
  {****************************}
    Try_input (Wmdt_dir + '/profile/' + (LWC Wmdt_profile) +
            '/aips/me10/startup/profile.m')
  END_IF
END_DEFINE
Awm_load_cust
DELETE_MACRO Awm_load_cust

{************************}
{* Init menu and tables *}
{************************}
Awmc_m_init_tb_tables

{****************************}
{* Init RCC/ADU definitions *}
{****************************}
DEFINE Awm_amsd_init_rca
  IF (NOT Annotation_active)
          Awm_m_init_rca
  END_IF
END_DEFINE
Awm_amsd_init_rca
DELETE_MACRO Awm_amsd_init_rca

{****************}
{* Init toolbar *}
{****************}
DEFINE Awm_amsd_init_toolbar
  Awm_i_get_program_name Sm_dms_menu_type
  INQ_ENV 10
{ 2 = MFC look ; 3 = MIXED look ; 4 = FLUENT look}
  IF (((INQ 6)= 2) OR ((INQ 6) = 3) OR ((INQ 6) = 4))
    INPUT (Wmdt_dir + "/aips/me10/startup/uitoolbar.m")
  END_IF
END_DEFINE
Awm_amsd_init_toolbar
DELETE_MACRO Awm_amsd_init_toolbar

{********************}
{* pop up AIP menu  *}
{********************}
DEFINE Awm_amsd_pop_up_aip_menu
  IF (NOT Annotation_active)
    Awmc_file_menu
    Awmc_file_dm_menu
  END_IF
END_DEFINE
Awm_amsd_pop_up_aip_menu
DELETE_MACRO Awm_amsd_pop_up_aip_menu

{***************************************************************************}
{* Show the DATA button on the ME10/Win Screen Menu                        *}
{***************************************************************************}
DEFINE Awm_dsp_menu
LOCAL Awm_os
  INQ_ENV 10
  LET Awm_os (INQ 4)
  IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
  { DOS, Windows 3.x, Windows NT, Windows 95 }
    IF (NOT ((POS (INQ 301) "ME10v") OR (INQ 6 = 3))) { INQ 6 = MIX PE Look }
      Dsp_menu
    END_IF
  END_IF
END_DEFINE

DEFINE Awm_amsd_show_data_button
  IF (NOT Annotation_active)
    INQ_ENV 10
    IF (INQ 3 = 0) { msmouse is activ }
      Awm_dsp_menu
    END_IF
  END_IF
END_DEFINE
Awm_amsd_show_data_button
DELETE_MACRO Awm_dsp_menu
DELETE_MACRO Awm_amsd_show_data_button

{***********************************}
{* try to load thumbnail support *}
{***********************************}
Try_input (Wmdt_dir + '/startup/thmbnl.inp')

{****************************************}
{*        load DDE_Topic                *}
{****************************************}

DEFINE Awm_load_dde_topic
LOCAL Awm_os

  INQ_ENV 10
  LET Awm_os (INQ 4)
  IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
  { DOS, Windows 3.x, Windows NT, Windows 95 }
    IF (NOT Annotation_active)
    {* if not Annotation, it should be ME10 *}
      DDE_ADD_TOPIC "I_AM_ME10"
    ELSE
    {* its Annotation *}
      DDE_ADD_TOPIC "I_AM_SDANNO"
    END_IF
  END_IF

END_DEFINE
Awm_load_dde_topic
DELETE_MACRO Awm_load_dde_topic


{****************************************************************************}
{* Enable "WorkManager Desktop" - default schema                            *}
{****************************************************************************}
DEFINE Awm_switch_default_schema
  LOCAL Default_schema
  LOCAL Wmdt_profile

  LET Wmdt_profile (GETENV "WMDT_PROFILE")
  LET Default_schema (GETENV 'WMDT_ENABLE_DEFAULT_SCHEMA')

  IF ((Default_schema = '1') AND ((Wmdt_profile = '') OR (Wmdt_profile = 'DESKTOP')))
    Enable_default_schema
  END_IF
END_DEFINE
Awm_switch_default_schema
DELETE_MACRO Awm_switch_default_schema

DEFINE Awm_set_csl_format
  { Set correct csl format }
  Awm_is_drawing_manager_active
  IF (Dms_return_val = 1)
     SET_CSL_FORMAT UTF8
  ELSE
     SET_CSL_FORMAT LEGACY
  END_IF
END_DEFINE

Awm_set_csl_format
DELETE_MACRO Awm_set_csl_format

DEFINE Awm_init_startup_menu
  { Set correct current menu }
  Awm_is_drawing_manager_active
  IF (Dms_return_val = 1)
    ACCEPT_CONNECTIONS
    DWG_MGMT_SEND_COMMAND "INIT"
    CURRENT_MENU 'DM_menu'
  ELSE
    CURRENT_MENU 'PEWM_menu'
  END_IF
END_DEFINE

DEFINE Awm_get_default_encoding
  Awm_is_drawing_manager_active
  IF (Dms_return_val = 1)
    LET Dms_return_val 'UTF-8'
  ELSE
    IF ((SUBSTR (GETENV "LANG") 1 2) = "ja")
       LET Dms_return_val 'SJIS'    
    ELSE
      LET Dms_return_val 'ROMAN8'
    END_IF
  END_IF
END_DEFINE

Awm_init_startup_menu
DELETE_MACRO Awm_init_startup_menu

{ Delete macro used at the top }
DELETE_MACRO Awm_load_lang_file

{***********************************}
{* try to load user customize file *}
{***********************************}
Try_input 'ui_cust.m'

{ *** end of file ** }
