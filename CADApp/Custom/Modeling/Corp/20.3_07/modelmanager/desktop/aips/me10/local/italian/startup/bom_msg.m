{  @(#)  bom_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of BOM-Module
}

{ >>>if false }

{ LOLAMAX = 162 }

{ General messages }

LET Awm_bom_msg_1
	"Inserire CONFIRM per rimuovere tutti i numeri di posizione della Distinta base"
LET Awm_bom_msg_2
    "Nessun layout numeri di posizione caricato; selez. layout, poi riprovare azione"
LET Awm_bom_msg_3
    "Identificare il numero di posizione da rimuovere"
LET Awm_bom_msg_4
    "Non � stato identificato alcun numero di posizione"
LET Awm_bom_msg_5
    "L'elemento non contiene alcuna informazione sulla posizione"
LET Awm_bom_msg_6
    "Identificare la parte a cui assegnare il numero di posizione"
LET Awm_bom_msg_7
    "La parte non si trova nell'albero della Distinta Base corrente"
LET Awm_bom_msg_8
    "Il numero di posizione � gi� assegnato"
LET Awm_bom_msg_9
    "A questa parte non sono stati assegnati dati di parte-WM"
LET Awm_bom_msg_10
    "Inserire punto iniz. linea guida o premere CONTINUA per cont. con parte seguente"
LET Awm_bom_msg_11
    "Ins. punto iniz. pos. orizz. linea guida; FINE completa o CONTINUA parte seg."
LET Awm_bom_msg_12
    "Inser. punto iniz. pos. vert. linea guida; FINE completa, CONTINUA cont. p.seg"
LET Awm_bom_msg_13
    "Indicare punto su linea guida posiz.; FINE completa, CONTINUA cont. parte seg."
LET Awm_bom_msg_14
    "Selezionare il componente della Distinta Base"
LET Awm_bom_msg_15
	"Input non valido"
LET Awm_bom_msg_16
	"La Distinta Base non contiene questa parte"
LET Awm_bom_msg_17
    "Non si sono trovate parti senza numero di posizione"
LET Awm_bom_msg_18
    "Identificare numero di posiz. per ritracciare la linea di guida della posizione"
LET Awm_bom_msg_19
	"Selezionare il componente della Distinta Base per tracciare il flag della posiz."
LET Awm_bom_msg_20
    "File di macro non valido"
LET Awm_bom_msg_21
    "File di scambio non trovato"
LET Awm_bom_msg_22
	"Struttura parti non valida: il layout flag posizioni non deve avere sottoparti"
LET Awm_bom_msg_23
    "Il punto finale 'SUPERIORE SINISTRO' non � definito"
LET Awm_bom_msg_24
    "Il punto finale 'SUPERIORE DESTRO' non � definito"
LET Awm_bom_msg_25
    "Il punto finale 'INFERIORE SINISTRO' non � definito"
LET Awm_bom_msg_26
    "Il punto finale 'INFERIORE DESTRO' non � definito"
LET Awm_bom_msg_27
	"Identificare una parte per l'intestazione Distinta Base"
LET Awm_bom_msg_28
	"Identificare una parte per il componente della Distinta Base"
LET Awm_bom_msg_29
	"Struttura parti non valida. Il layout Dist.B. deve essere formato da intestazione Dist.B. e parte componente Dist.B."
LET Awm_bom_msg_30
	"Selezionare opzione o identificare punto finale esist. di linea guida posizione"
LET Awm_bom_msg_31
	"Selezionare SINISTRO o DESTRO o nuovo punto finale per linea guida posizione"
LET Awm_bom_msg_32
	"Inserire il nuovo punto finale per la linea guida della posizione"
LET Awm_bom_msg_33
	"Non � stata assegnata alcuna Distinta Base da parte di WorkManager."
LET Awm_bom_msg_34
    "Non � stato caricato alcun layout di Distinta Base"
LET Awm_bom_msg_35
    "Identificare la posizione nella Distinta Base"
LET Awm_bom_msg_36
	"Identificare il gruppo per generare la Distinta Base"
LET Awm_bom_msg_37
    "Distinta Base-WM non disponibile."
LET Awm_bom_msg_38
	"Nessun gruppo identificato"
LET Awm_bom_msg_39
	"Inserire il nuovo numero di posizione"
LET Awm_bom_msg_40
	"Il componente non corrisponde alla Distinta Base-WM"
LET Awm_bom_msg_41
	"Identificare il gruppo per la Distinta Base corrente"
LET Awm_bom_msg_42
    "Non � stata trovata alcuna parte ME10 corrispondente alla intestazione Dist.Base"
LET Awm_bom_msg_43
"Inserire CONFIRM per aggiornare la Distinta Base in WorkManager"
LET Awm_bom_msg_44
"Inserire CONFIRM per aggiornare Dist.Base in ME10 con struttura parti ME10 corr."
LET Awm_bom_msg_45 ""
LET Awm_bom_msg_46
"Inserire CONFIRM per cancellare tutti i numeri di posiz. nella Dist.Base di ME10"
LET Awm_bom_msg_47
	"Identificare il gruppo per aprire la Dist. Base in WorkManager o CORRENTE"
LET Awm_bom_msg_48
	"La parte non ha dati di parte-WM associati"
LET Awm_bom_msg_49
	"Inserire il numero della posizione iniziale:"
LET Awm_bom_msg_50
	"Inserire il valore di incremento del numero di posizione:"
LET Awm_bom_msg_51
	"Identificare una parte per il componente della Distinta Base"
LET Awm_bom_msg_52
    "Il punto finale 'SUPERIORE' non � definito"
LET Awm_bom_msg_53
    "Il punto finale 'INFERIORE' non � definito"
LET Awm_bom_msg_54
    "Il punto finale 'DESTRO' non � definito"
LET Awm_bom_msg_55
    "Il punto finale 'SINISTRO' non � definito"

LET Awm_bom_msg_56
    "Il componente appare in piu' di una Distinta Base"
LET Awm_bom_msg_57
    "non e' possibile combinare una Distinta Base n-livelli con una monolivello"
LET Awm_bom_msg_58
    "aggiornamento non possibile in Distinta Base a n-livelli"

LET Awm_bom_msg_59
    "Inserire il numero di livelli della struttura:"
LET Awm_bom_msg_21
    "Identificare la parte di cui aprire la Distinta Base o CORRENTE o RADICE-TOP"
LET Awm_bom_msg_22
    "Nome di parte non valido:"

{ Menu texts }

LET Awm_bom_mtext_1 "NUM-POSIZ"
LET Awm_bom_mtext_2 "DIST. BASE"
LET Awm_bom_mtext_3 "CNF-D.BASE"
LET Awm_bom_mtext_4 "CREA POSIZ"
LET Awm_bom_mtext_5 "Lista"
LET Awm_bom_mtext_6 "Identif"
LET Awm_bom_mtext_7 "ORIZZONT."
LET Awm_bom_mtext_8 "VERTICALE"
LET Awm_bom_mtext_9 "RITRACCIA"
LET Awm_bom_mtext_10 "Num-Posiz"
LET Awm_bom_mtext_11 "CANCELLA"
LET Awm_bom_mtext_12 "Tutto"
LET Awm_bom_mtext_13 "POSIZ-INIZ"
LET Awm_bom_mtext_14 "INCREMENTO"
LET Awm_bom_mtext_15 "LAYOUT-POS"
LET Awm_bom_mtext_16 "OUTPUT"
LET Awm_bom_mtext_17 "Disegno"
LET Awm_bom_mtext_18 "LAYOUT DB"
LET Awm_bom_mtext_19 "APRI D.BAS"
LET Awm_bom_mtext_20 "RADICE-TOP"
LET Awm_bom_mtext_21 "Parte"
LET Awm_bom_mtext_22 "MOSTRA"
LET Awm_bom_mtext_23 "DIST. BASE"
LET Awm_bom_mtext_24 ""
LET Awm_bom_mtext_25 "AGGIORNA"
LET Awm_bom_mtext_26 ""
LET Awm_bom_mtext_27 ""
LET Awm_bom_mtext_28 ""
LET Awm_bom_mtext_29 ""
LET Awm_bom_mtext_30 "ME10 -> WM"
LET Awm_bom_mtext_31 "Num-Posiz"
LET Awm_bom_mtext_32 "CORRENTE"
LET Awm_bom_mtext_33 "StatoParti"
LET Awm_bom_mtext_34 "CONTINUA"
LET Awm_bom_mtext_35 "FINE"
LET Awm_bom_mtext_36 "FINE LINEA"
LET Awm_bom_mtext_37 "Definisci"
LET Awm_bom_mtext_38 "SUPERIORE"
LET Awm_bom_mtext_39 "INFERIORE"
LET Awm_bom_mtext_40 "SINISTRO"
LET Awm_bom_mtext_41 "DESTRO"
LET Awm_bom_mtext_42 "SALVA"
LET Awm_bom_mtext_43 "Layout DB"
LET Awm_bom_mtext_44 "RIMUOVI"
LET Awm_bom_mtext_45 "Tabella DB"
LET Awm_bom_mtext_46 "DIN"
LET Awm_bom_mtext_47 "ISO"
LET Awm_bom_mtext_48 "UTENTE"
LET Awm_bom_mtext_49 "A"
LET Awm_bom_mtext_50 "B"
LET Awm_bom_mtext_51 "C"

LET Awm_bom_mtext_52 "SCAN D.BAS"
LET Awm_bom_mtext_53                                ""
LET Awm_bom_mtext_54 "AGGIUNGI"
LET Awm_bom_mtext_55 "SOSTITUIRE"
LET Awm_bom_mtext_56 "CORRENTE"
LET Awm_bom_mtext_57 "RADICE-TOP"
LET Awm_bom_mtext_58 "LIVELLI"

{ Table Titles }

LET Awm_bom_ttitle_1  "Numero di parte"
LET Awm_bom_ttitle_2  "ID della parte"
LET Awm_bom_ttitle_3  "Parte di WM"
LET Awm_bom_ttitle_4  "DIST. BASE"
LET Awm_bom_ttitle_5  "Num-Posiz"
LET Awm_bom_ttitle_6  "X"
LET Awm_bom_ttitle_7  "ERRORE"
LET Awm_bom_ttitle_8  "DA"
LET Awm_bom_ttitle_9  "D.BASE ME10 CORRENTE"
LET Awm_bom_ttitle_10 "INF.STATO PARTE ME10"
LET Awm_bom_ttitle_11 "COMPON. CORRENTI DB"
LET Awm_bom_ttitle_12 "Messaggio"

{ >>>endif }
