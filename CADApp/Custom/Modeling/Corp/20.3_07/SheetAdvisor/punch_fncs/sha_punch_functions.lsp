;=================================================;
;                                                 ;
;    This file contains functions to model the    ;
;    profiles for some parametric punch tools.    ;
;                                                 ;
;=================================================;


(in-package :sheet-advisor)
(use-package :oli)

(defvar *sheet-advisor-show-tool* nil)

(defmacro sha-delete-or-show-wp()
  `(unless *sheet-advisor-show-tool*
     (delete_3d (sha-absolute-name tool-wp)))
)

(defmacro sha-delete-or-show-corner-wp()
  `(if *sheet-advisor-show-tool*
    (progn
      (GEOMETRY_MODE :CONSTRUCTION)
      (POLYGON (sd-vec-add PROFILE_PNT_AT_BEND_ZONE
                           PROFILE_DIR_AT_BEND_ZONE)
               PROFILE_PNT_AT_BEND_ZONE
               OUTER_CORNER_POINT
               OTHER_PROFILE_PNT_AT_BEND_ZONE
               (sd-vec-add OTHER_PROFILE_PNT_AT_BEND_ZONE
                           OTHER_PROFILE_DIR_AT_BEND_ZONE)
      )
      (POLYGON PROFILE_PNT_AT_BEND_ZONE
               INNER_CORNER_PROJECTION_POINT   
               INNER_CORNER_POINT
               OTHER_INNER_CORNER_PROJECTION_POINT
               OTHER_PROFILE_PNT_AT_BEND_ZONE
      )
      (GEOMETRY_MODE :REAL)
      (uic_c_line_cross 0,0)
     ;(dbg-show-vtx PROFILE_PNT_AT_BEND_ZONE)
     ;(dbg-show-vtx BEND_START_PNT)
      
     ;(dbg-save :ALL_AT_TOP "_after_relief_profile_creation")
    )
    (delete_3d (sha-absolute-name tool-wp))
  )
)

;=================================================
;
;   CLOVER LEAF 1 
;
;=================================================

(defun sha-clv_leaf_1  
    (&key
     radius
     angle
     tool_width
     slot_width
     &allow-other-keys
        )  
    (let ((result nil)
	  (tool-wp   (sha-tool-wp))
	 )
	 (create_workplane :new
			   :name tool-wp
			   :world_origin
	 )
	 ;********* create Profile *******
	 (POLYGON
	  (gpnt2d slot_width (+ slot_width radius))
	  (gpnt2d (- slot_width (* (tan angle) (- tool_width (+ radius slot_width)))) tool_width)
	  (gpnt2d 0 tool_width)
	  0,0
	  (gpnt2d tool_width 0)
	  (gpnt2d tool_width (- slot_width (* (tan angle) (- tool_width (+ radius slot_width)))))
	  (gpnt2d (+ slot_width radius) slot_width)
	 )
	 (ARC :CEN_RAD_ANG
	  (gpnt2d (+ slot_width radius) (+ slot_width radius))
	  radius
	  (gpnt2d slot_width (+ slot_width radius))
	  (gpnt2d (+ slot_width radius) slot_width)
         )
	 ;******** create Adjustpoints ******
	 (C_POINT
	  0,0
	  (gpnt2d tool_width 0)
	  (gpnt2d tool_width tool_width)
	  (gpnt2d 0 tool_width)
	  (gpnt2d slot_width slot_width)
	  (gpnt2d (+ slot_width radius) (+ slot_width radius))
	 )
	 ;******* move Profile to the 1st. Adjustpoint ******
	 (MOVE_2D
	  :SELECT :all_2d 
	  :DIRECTION
	  0,0 
	  (gpnt2d (- slot_width) (- slot_width))
	 )

	 (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
	 result
    )
)
  
;=================================================
;
;   CLOVER LEAF 4
;
;=================================================

(defun sha-clv_leaf_4  
    (&key
     radius
     angle
     tool_width
     slot_width
     &allow-other-keys
        )  
    (let ((result nil)
	  (tool-wp   (sha-tool-wp))
	 )
	 (create_workplane :new
			   :name tool-wp
			   :world_origin
	 )
	 ;********* create Profile *******
	 (POLYGON
	  (gpnt2d (/ slot_width 2) (+ (/ slot_width 2) radius))
	  (gpnt2d (- (/ slot_width 2) (* (tan angle) (- (/ tool_width 2) (+ radius (/ slot_width 2))))) (/ tool_width 2))
	  (gpnt2d 0 (/ tool_width 2))
         )
         (POLYGON
	  (gpnt2d (/ tool_width 2) 0)
	  (gpnt2d (/ tool_width 2) (- (/ slot_width 2) (* (tan angle) (- (/ tool_width 2) (+ radius (/ slot_width 2))))))
	  (gpnt2d (+ (/ slot_width 2) radius) (/ slot_width 2))
	 )
	 (ARC :CEN_RAD_ANG
	  (gpnt2d (+ (/ slot_width 2) radius) (+ (/ slot_width 2) radius))
	  radius
	  (gpnt2d (/ slot_width 2) (+ (/ slot_width 2) radius))
	  (gpnt2d (+ (/ slot_width 2) radius) (/ slot_width 2))
         )
	 ;******** create Adjustpoints ******
	 (C_POINT
	  0,0
	  (gpnt2d (/ tool_width 2) 0)
	  (gpnt2d (/ tool_width 2) (/ tool_width 2))
	  (gpnt2d 0 (/ tool_width 2))
	  (gpnt2d (/ slot_width 2) (/ slot_width 2))
	  (gpnt2d (+ (/ slot_width 2) radius) (+ (/ slot_width 2) radius))
	 )
	 ;******* mirror Profile and Adjustpoints ********
	 (MIRROR_2D
	  :SELECT :all_2d
	  :HORIZONTAL 0
	 )
	 (MIRROR_2D
          :SELECT :all_2d
	  :VERTICAL 0
	 )

	 (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
	 result
    )
)

;=================================================
;
;   CONNECTOR
;
;=================================================

(defun sha-con
    (&key
     out_length
     out_width
     in_length
     in_width
     radius
     n_rad
     (resolution 0.000001)
     &allow-other-keys
        )
    (let ((result nil)
          (tool-wp   (sha-tool-wp))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (POLYGON
          (gpnt2d 0 (/ out_width 2))
          (gpnt2d (/ in_length 2) (/ out_width 2))
          (gpnt2d (/ in_length 2) (/ in_width 2))
          (gpnt2d (/ out_length 2) (/ in_width 2))
          (gpnt2d (/ out_length 2) 0)
         )
         (when (> n_rad resolution)
           (FILLET :CREATE
            :FILLET_RADIUS n_rad
            (gpnt2d (/ in_length 2) (/ out_width 2))
           )
         )
         (when (> radius resolution)
           (FILLET :CREATE
            :FILLET_RADIUS radius
            (gpnt2d (/ out_length 2) (/ in_width 2))
           )
         )
         (MIRROR_2D
          :SELECT :all_2d
          :HORIZONTAL
          0,0
         )
         (MIRROR_2D
          :SELECT :all_2d
          :VERTICAL
          0,0
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
;          (gpnt2d dist 0)
;          (gpnt2d (- dist) 0)
;          (gpnt2d 0 (* (cos (deg-to-rad 30)) (- dist)))
;          (gpnt2d dist (* (cos (deg-to-rad 30)) (- dist)))
;          (gpnt2d (- dist) (* (cos (deg-to-rad 30)) (- dist)))
;          (gpnt2d 0 (* (cos (deg-to-rad 30)) dist))
;          (gpnt2d dist (* (cos (deg-to-rad 30)) dist))
;          (gpnt2d (- dist) (* (cos (deg-to-rad 30)) dist))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

  
;=================================================
;
;   RADIUS   
;
;=================================================

(defun sha-radius      
    (&key
     radius
     slot_width
     &allow-other-keys
        )  
    (let ((result nil)
	  (tool-wp   (sha-tool-wp))
	 )
	 (create_workplane :new
			   :name tool-wp
			   :world_origin
	 )
	 ;********* create Profile *******
	 (POLYGON
	  (gpnt2d slot_width 0)
	  0,0
          (gpnt2d 0 slot_width)
 	 )
	 (POLYGON
          (gpnt2d radius (+ radius slot_width))
          (gpnt2d (+ radius slot_width) (+ radius slot_width))
          (gpnt2d (+ radius slot_width) radius)
	 )
         (ARC :CEN_RAD_ANG
	  (gpnt2d (+ radius slot_width) 0)
	  radius
	  (gpnt2d (+ radius slot_width) radius)
          (gpnt2d slot_width 0)
         )
         (ARC :CEN_RAD_ANG
          (gpnt2d radius slot_width)
          radius
	  (gpnt2d radius (+ radius slot_width))
          (gpnt2d 0 slot_width)
         )
	 ;******** create Adjustpoints ******
	 (C_POINT
	  0,0
	  (gpnt2d (+ radius slot_width) 0)
	  (gpnt2d (+ radius slot_width) (+ radius slot_width))
	  (gpnt2d 0 (+ radius slot_width))
	  (gpnt2d radius slot_width)
	  (gpnt2d slot_width radius)
	 )
	 ;******* move Profile to the 1st. Adjustpoint ******
	 (MOVE_2D
	  :SELECT :all_2d 
	  :DIRECTION
	  0,0 
	  (gpnt2d  (- slot_width) (- radius))
	 )

	 (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
	 result
    )
)
  
;=================================================
;
;   CLUSTER OBLONG
;
;=================================================

(defun sha-clstr_obl
    (&key
     horiz
     vert
     dist_x
     dist_y
     rows
     holes_row
     (indent_x (/ dist_x 2))
     (indent_y (/ dist_y 2))
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
	   (h (/ horiz 2))
	   (v (- (/ vert 2) h))
	   (ox (if (> rows 1)
		(/ (+ (* (- holes_row 1) dist_x) indent_x) -2)
	        (/ (* (- holes_row 1) dist_x) -2)
	       )
	   )
           (oy (if (= (/ rows 2) (ceiling (/ rows 2)))
                (/ (- (* (/ rows 2) dist_y) indent_y) -2)
	        (/ (* (truncate (/ rows 2)) dist_y) -2)
               )
           )
	   (x 0)
	   (y 0)
           (adj_x (* holes_row dist_x) 
                  
           )
           (adj_y (if (> rows 1)
		      (if (= (/ rows 2) (ceiling (/ rows 2)))
			  (* (truncate (/ rows 2)) dist_y)
			  (+ (* (truncate (/ rows 2)) dist_y) indent_y)
                      )
		      dist_y
                  )
           )
          )
          (create_workplane :new
                            :name tool-wp
                            :world_origin
          )
          ;********* create Profile *******
	  (flet (
	 	 (create_oblong (x y)
                      (LINE :TWO_POINTS
                         (gpnt2d (- x h) (- y v))
			 (gpnt2d (- x h) (+ y v))
			 (gpnt2d (+ x h) (- y v))
			 (gpnt2d (+ x h) (+ y v))
                      )
                      (ARC :CEN_RAD_ANG
                         (gpnt2d x (- y v))
			 h
			 (gpnt2d (- x h) (- y v))
			 (gpnt2d (+ x h) (- y v))
                      )
                      (ARC :CEN_RAD_ANG
                         (gpnt2d x (+ y v))
			 h
			 (gpnt2d (+ x h) (+ y v))
			 (gpnt2d (- x h) (+ y v))
                      )
                 )
                )
		(dotimes (i (ceiling rows))
                      (dotimes (j (ceiling holes_row))
                          (create_oblong (+ x ox) (+ y oy))
			  (setf x (+ x dist_x))
                      )
	              (if (= (/ i 2) (ceiling (/ i 2))) 
                              (progn 
                                   (setf x (- dist_x indent_x))
                                   (setf y (+ y indent_y))
                              )
                              (progn
                                   (setf x 0)
                                   (setf y (+ y (- dist_y indent_y)))
                              )
                      )
                )
          )
          ;******** create Adjustpoints ******
          (if (= (/ rows 2) (ceiling (/ rows 2)))          
               (C_POINT
                   0,0
                   (gpnt2d adj_x 0)
                   (gpnt2d (- adj_x) 0)
                   (gpnt2d adj_x adj_y)
                   (gpnt2d (- adj_x) adj_y)
                   (gpnt2d 0 adj_y)
                   (gpnt2d adj_x (- adj_y))
                   (gpnt2d (- adj_x) (- adj_y))
                   (gpnt2d 0 (- adj_y))
               )
               (C_POINT
                   0,0
		   (gpnt2d adj_x 0)
		   (gpnt2d (- adj_x) 0)
		   (gpnt2d (- (+ adj_x dist_x) indent_x) adj_y)
		   (gpnt2d (- dist_x  (+ adj_x indent_x)) adj_y)
		   (gpnt2d (- dist_x indent_x) adj_y)
		   (gpnt2d (- adj_x indent_x) (- adj_y))
		   (gpnt2d (- (- adj_x) indent_x) (- adj_y))
		   (gpnt2d (- indent_x) (- adj_y))
               )
	  )
          (setq result (sha-profile-of-wp tool-wp))
          (sha-delete-or-show-wp)
          result
   )
)

;=================================================
;
;   CLUSTER RECTANGULAR
;
;=================================================

(defun sha-clstr-rect-profile
    (&key
     horiz
     vert
     dist_x
     dist_y
     rows
     holes_row
     indent
     (indent_x (or indent (/ dist_x 2)))
     (indent_y (/ dist_y 2))
     tool_dist_x
     tool_dist_y
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
	   (h (/ horiz 2))
	   (v (/ vert 2))
	   (ox (if (> rows 1)
		(/ (+ (* (- holes_row 1) dist_x) indent_x) -2)
	        (/ (* (- holes_row 1) dist_x) -2)
	       )
	   )
           (oy (if (evenp rows)
                (/ (- (* (/ rows 2) dist_y) indent_y) -2)
	        (/ (* (truncate (/ rows 2)) dist_y) -2)
               )
           )
	   (x 0)
	   (y 0)
           (adj_x tool_dist_x )
           (adj_y tool_dist_y )
          )
          (create_workplane :new
                            :name tool-wp
                            :world_origin
          )
          ;********* create Profile *******
	  (flet (
	 	 (create_rectangle (x y)
                      (POLYGON
                         (gpnt2d (- x h) (- y v))
			 (gpnt2d (- x h) (+ y v))
			 (gpnt2d (+ x h) (+ y v))
			 (gpnt2d (+ x h) (- y v))
                         (gpnt2d (- x h) (- y v))
                      )
                 )
                )
		(dotimes (i (ceiling rows))
                      (dotimes (j (ceiling holes_row))
                        (create_rectangle (+ x ox) (+ y oy))
			(setf x (+ x dist_x))
                      )
	              (if (evenp i) 
                        (progn 
                          (setf x (- dist_x indent_x))
                          (setf y (+ y indent_y))
                        )
                        (progn
                          (setf x 0)
                          (setf y (+ y (- dist_y indent_y)))
                        )
                      )
                )
          )
          ;******** create Adjustpoints ******
          (C_POINT
            0,0
            (gpnt2d adj_x      0)
            (gpnt2d (- adj_x)  0) 
            (gpnt2d adj_x      adj_y)
            (gpnt2d (- adj_x)  adj_y)
            (gpnt2d 0          adj_y)
            (gpnt2d adj_x      (- adj_y))
            (gpnt2d (- adj_x)  (- adj_y))
            (gpnt2d 0          (- adj_y))
          )

          (setq result (sha-profile-of-wp tool-wp))
          (sha-delete-or-show-wp)
          result
   )
)

;=================================================
;
;   CLUSTER RECTANGULAR
;
;=================================================

(defun sha-regular-clstr-rect-profile
    (&key
     horiz
     vert
     dist_x
     dist_y
     rows
     holes_row
     tool_dist_x
     tool_dist_y
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
	   (h (/ horiz 2))
	   (v (/ vert 2))
	   (ox (/ (* (- holes_row 1) dist_x) -2)) 
           (oy (/ (* (- rows 1) dist_y) -2))
	   (x 0)
	   (y 0)
           (adj_x tool_dist_x )
           (adj_y tool_dist_y )
          )
          (create_workplane :new
                            :name tool-wp
                            :world_origin
          )
          ;********* create Profile *******
	  (flet (
	 	 (create_rectangle (x y)
                      (POLYGON
                         (gpnt2d (- x h) (- y v))
			 (gpnt2d (- x h) (+ y v))
			 (gpnt2d (+ x h) (+ y v))
			 (gpnt2d (+ x h) (- y v))
                         (gpnt2d (- x h) (- y v))
                      )
                 )
                )
		(dotimes (i (ceiling rows))
                      (dotimes (j (ceiling holes_row))
                        (create_rectangle (+ x ox) (+ y oy))
			(setf x (+ x dist_x))
                      )
                      (setf x 0)
                      (setf y (+ y dist_y))
                )
          )
          ;******** create Adjustpoints ******
          (C_POINT
            0,0
            (gpnt2d adj_x      0)
            (gpnt2d (- adj_x)  0) 
            (gpnt2d adj_x      adj_y)
            (gpnt2d (- adj_x)  adj_y)
            (gpnt2d 0          adj_y)
            (gpnt2d adj_x      (- adj_y))
            (gpnt2d (- adj_x)  (- adj_y))
            (gpnt2d 0          (- adj_y))
          )

          (setq result (sha-profile-of-wp tool-wp))
          (sha-delete-or-show-wp)
          result
   )
)

;=================================================================
;
;   HEXAGON  (old version: sha-hex kept for compatibility reasons)
;            (new version (compatible to help file description)
;              called: sha-hexagon                    )
;
;=================================================================

(defun sha-hexagon
    (&key
     dist
     &allow-other-keys
        )
    (let ((result nil)
          (tool-wp   (sha-tool-wp))
          (edge-length (/ dist 2 (cos (deg-to-rad 30))))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (POLYGON
          (gpnt2d (* edge-length -1) 0         ) 
          (gpnt2d (/ edge-length -2) (/ dist  2))
          (gpnt2d (/ edge-length  2) (/ dist  2))
          (gpnt2d    edge-length     0         ) 
          (gpnt2d (/ edge-length  2) (/ dist -2))
          (gpnt2d (/ edge-length -2) (/ dist -2))
          (gpnt2d (* edge-length -1) 0         ) 
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          (gpnt2d    edge-length        0       )
          (gpnt2d (- edge-length)       0       )
          (gpnt2d    edge-length     (/ dist 2) )
          (gpnt2d    0               (/ dist 2) )
          (gpnt2d (- edge-length)    (/ dist 2) )
          (gpnt2d    edge-length     (/ dist -2))
          (gpnt2d    0               (/ dist -2))
          (gpnt2d (- edge-length)    (/ dist -2))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

;=================================================
(defun sha-hex
    (&key
     dist
     &allow-other-keys
        )
    (let ((result nil)
          (tool-wp   (sha-tool-wp))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (LINE :TWO_POINTS
          (gpnt2d (- (/ dist 2)) (* (cos (deg-to-rad 30)) (- dist))) 
          (gpnt2d (/ dist 2) (* (cos (deg-to-rad 30)) (- dist))) 
         )
         (LINE :ANGLE
          (gpnt2d (- (/ dist 2)) (* (cos (deg-to-rad 30)) (- dist)))
          (deg-to-rad 120)
          dist
         )
         (LINE :ANGLE
          (gpnt2d  (/ dist 2) (* (cos (deg-to-rad 30)) (- dist)))
          (deg-to-rad 60)
          dist
         )
         (MIRROR_2D
          :SELECT :all_2d
          :HORIZONTAL
          0,0
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          (gpnt2d dist 0)
          (gpnt2d (- dist) 0)
          (gpnt2d 0 (* (cos (deg-to-rad 30)) (- dist)))
          (gpnt2d dist (* (cos (deg-to-rad 30)) (- dist)))
          (gpnt2d (- dist) (* (cos (deg-to-rad 30)) (- dist)))
          (gpnt2d 0 (* (cos (deg-to-rad 30)) dist))
          (gpnt2d dist (* (cos (deg-to-rad 30)) dist))
          (gpnt2d (- dist) (* (cos (deg-to-rad 30)) dist))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

;=================================================
;
;   KEYWAY IN
;
;=================================================

(defun sha-kwy_in
    (&key
     dia
     width
     depth
     ang
     (resolution 0.000001)
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           (r (/ dia 2))
           (alpha_1 (atan (/ (/ width 2) (- r depth))))
           (laenge_b (/ (/ width 2) (sin alpha_1)))
           (gamma (+ alpha_1 (deg-to-rad 180) (- ang)))
           (beta (if (> (abs (- (deg-to-rad 180) gamma)) (/ resolution r))
                   (asin (/ (* laenge_b (sin gamma)) r))
                   0
                 )
           )
           (alpha (- (deg-to-rad 180) (+ gamma beta)))
           (winkel (+ alpha_1 alpha))
           (px (* (cos winkel) r))
           (py (* (sin winkel) r))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (POLYGON
          (gpnt2d (- r depth) 0)
          (gpnt2d (- r depth) (/ width 2))
          (gpnt2d px py)
         )
         (ARC :CEN_RAD_ANG
          0,0
          r
          (gpnt2d px py)
          (gpnt2d (- r) 0)                    
         )
         (MIRROR_2D
          :SELECT :all_2d
          :HORIZONTAL
          0,0
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          (gpnt2d (- r) 0)
          (gpnt2d r 0)
          (gpnt2d (- r) r)
          (gpnt2d 0 r)
          (gpnt2d r r)
          (gpnt2d (- r) (- r))
          (gpnt2d 0 (- r))
          (gpnt2d r (- r))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

;=================================================
;
;   TWIST SAFETY
;
;=================================================

(defun sha-twstsfty
    (&key
     out_dia
     in_dia
     slot_width
     nr_slots
     angle
     (resolution 0.000001)
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           (outr (/ out_dia 2))
           (inr (/ in_dia 2))
           (angle_step (deg-to-rad (/ 360 nr_slots)))  
           (alpha_1 (asin (/ (/ slot_width 2) inr)))
           (gamma_2 (+ (deg-to-rad 180) alpha_1 (- (/ angle 2))))
           (beta_2 (if (> (abs (- (deg-to-rad 180) gamma_2)) (/ resolution outr))
                     (asin (* (sin gamma_2) (/ inr outr)))
                     0
                   )
           )
           (alpha_2 (- (deg-to-rad 180) (+ gamma_2 beta_2)))
           (winkel (+ alpha_1 alpha_2))
           (p1x (* (cos winkel) outr))
           (p1y (* (sin winkel) outr))
           (p2x (* (cos (/ angle_step 2)) outr))
           (p2y (* (sin (/ angle_step 2)) outr))
           (p3x (* (cos alpha_1) inr))
           (p3y (* (sin alpha_1) inr))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (LINE :TWO_POINTS
          (gpnt2d p3x p3y)
          (gpnt2d p1x p1y)
         )
         (ARC :CEN_RAD_ANG
          0,0
          inr
          (gpnt2d inr 0)
          (gpnt2d p3x p3y)
         )
         (ARC :CEN_RAD_ANG
          0,0
          outr
          (gpnt2d p1x p1y)
          (gpnt2d p2x p2y)
         )
         (MIRROR_2D 
          :SELECT :all_2d
          :HORIZONTAL
          0,0
         )
         (ROTATE_2D
          :SELECT :all_2d
          :ANGLE angle_step
          :CENTER_POINT 0,0
          :REPEAT_FACTOR nr_slots
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          (gpnt2d (- outr) 0)
          (gpnt2d outr 0)
          (gpnt2d (- outr) outr)
          (gpnt2d 0 outr)
          (gpnt2d outr outr)
          (gpnt2d (- outr) (- outr))
          (gpnt2d 0 (- outr))
          (gpnt2d outr (- outr))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

;=================================================
;
;   KEYWAY OUT  
;
;=================================================

(defun sha-kwy_out
    (&key
     dia
     length
     width
     (fillet 0)
     ang
     (resolution 0.000001)
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           (p1x (/ (sqrt (- (expt dia 2) (expt width 2))) 2))
           (p1y (/ width 2))
           (check (* (mod ang (* 2 pi)) (- length dia)))
           (c (if (> check resolution)
                 (/ (+ width (* (tan ang) (sqrt (- (expt dia 2) (expt width 2))))) 2)))
           (p2x (- length (/ dia 2)))
           (p2y (if (> check resolution)
                   (+ (* (- (tan ang)) p2x) c)
                   (/ width 2)))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (POLYGON
          (gpnt2d p1x p1y)
          (gpnt2d p2x p2y)
          (gpnt2d p2x 0)
         )
         (ARC :CEN_RAD_ANG
          0,0
          (/ dia 2)
          (gpnt2d p1x p1y)
          (gpnt2d (- (/ dia 2)) 0)
         )
         (unless (< fillet resolution)
           (FILLET :CREATE
            :FILLET_RADIUS fillet
            (gpnt2d p2x p2y)
           )
         )
         (MIRROR_2D :SELECT
          :SELECT :all_2d
          :HORIZONTAL
          0,0
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          (gpnt2d (- (/ dia 2)) 0)
          (gpnt2d (/ dia 2) 0)
          (gpnt2d (- (/ dia 2)) (/ dia 2))
          (gpnt2d 0 (/ dia 2))
          (gpnt2d (/ dia 2) (/ dia 2))
          (gpnt2d (- (/ dia 2)) (- (/ dia 2)))
          (gpnt2d 0 (- (/ dia 2)))
          (gpnt2d (/ dia 2) (- (/ dia 2)))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

;=================================================
;
;   KEYWAY OUT RND
;
;=================================================

(defun sha-kwy_out_rnd
    (&key
     dia
     length
     width
     (fillet (/ width 2))
     (ang 0)
     (resolution 0.000001)
     &allow-other-keys
    )
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           (p1x (/ (sqrt (- (expt dia 2) (expt width 2))) 2))
           (p1y (/ width 2))
           (check (* (mod ang (* 2 pi)) (- length dia)))
           (c (if (> check resolution)
                 (/ (+ width (* (tan ang) (sqrt (- (expt dia 2) (expt width 2))))) 2)))
           (p2x (- length (/ dia 2)))
           (p2y (if (> check resolution)
                   (+ (* (- (tan ang)) p2x) c)
                   (/ width 2)))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )
         ;********* create Profile *******
         (POLYGON
          (gpnt2d p1x p1y)
          (gpnt2d p2x p2y)
          (gpnt2d p2x 0)
         )
         (ARC :CEN_RAD_ANG
          0,0
          (/ dia 2)
          (gpnt2d p1x p1y)
          (gpnt2d (- (/ dia 2)) 0)
         )
         (unless (< fillet resolution)
           (FILLET :CREATE
            :FILLET_RADIUS fillet
            (gpnt2d p2x p2y)
           )
         )
         (MIRROR_2D :SELECT
          :SELECT :all_2d
          :HORIZONTAL
          0,0
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          (gpnt2d (- (/ dia 2)) 0)
          (gpnt2d (/ dia 2) 0)
          (gpnt2d (- (/ dia 2)) (/ dia 2))
          (gpnt2d 0 (/ dia 2))
          (gpnt2d (/ dia 2) (/ dia 2))
          (gpnt2d (- (/ dia 2)) (- (/ dia 2)))
          (gpnt2d 0 (- (/ dia 2)))
          (gpnt2d (/ dia 2) (- (/ dia 2)))
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-wp)
         result
   )
)

(defun sha-miter-chamfer-profile
    (&key
     PERCT_DEPTH  
    ;THICKNESS  
    ;RESOLUTION  
     SIDE  
     CONCAVE
     BEND_RADIUS  
     BEND_ANGLE  
     BEND_FLAT_LENGTH  
     BEND_LINE_DIST 
     EDGE_VTX    
     BEND_START_PNT  
     BEND_START_DIR
     PROFILE_PNT_AT_BEND_ZONE 
     PROFILE_PNT_AT_LIP_END  
     PROFILE_DIR
     &allow-other-keys
    )

    ;  with: :LENGTH_X_ANGLE (:DEG 0) ; rot angle when bend is in X_POS; value in shop tool table
    ;
    ;  usual ~90 deg bend angle case                                                                            
    ;                                               _______________________________________________                     
    ;                                              /                                               \                 
    ;                                             /               New Lip                           \                  
    ;                                 |--d1--|   /                                                   \                
    ;                                 |      |  /  \                                                  \            
    ;                                 |  |-d2| / new-lip-side-angle                                    \               
    ;                    EDGE_VTX     |  |   |/      \                                                  \                  
    ;                             \   |  |   /--------------- bend end   ----------+---------------------\          EDGE_VTX
    ;                              \  |  |  / \                                    |                      \        /
    ;                               \ |  | /   PROFILE_PNT_AT_BEND_ZONE            |                       \      /
    ;                           __     __ /__________________ orig base lip end ___|________________________\ __
    ;           BEND_LINE_DIST {      |  / \                                       |                         \  |
    ;                           --    | / --\---------------- bend line ----------- } ------------------------\ |
    ;                           |      /     \                                     | BEND_FLAT_LENGTH          \
    ;       BEND_FLAT_LENGTH/2 /      /       PROFILE_PNT_AT_LIP_END               |                            \
    ;                          \      |                                            |                            |
    ;                           |     |                                            |                            |  
    ;                           |---  |----------------------- bend start ---------+----------------------------|            
    ;                           |     | \ BEND_START_PNT                                                        |   
    ;               BEND_DEPTH {    Y ^                                                                         ^ Y   
    ;                           |     |                                                                         |  
    ;                           |     |   X                                                                     |  X
    ;                            ---  +---->                                                                 0,0+--->        
    ;                                 |                                                                         |   
    ;                                 v    (direction of BEND_DEPTH)      Base Face                             |            
    ;                                                                                                   
    ;  with: :LENGTH_X_ANGLE (:DEG 0) ; rot angle when bend is in X_POS; value in shop tool table
    ;
    ;  smaller bend angle situation (e.g. 60 deg, R=1.6):                                                                            
    ;                                                 ___________________________________________                     
    ;                                                /                                           \                 
    ;                                               /               New Lip                       \                  
    ;                                              /                                               \                
    ;                                             /  \                                              \            
    ;                                 |d1|d2|    / new-lip-side-angle                                \               
    ;                     EDGE_VTX    |  |  |   /      \                                              \                  
    ;                              \  |  |  |  /                                                       \         EDGE_VTX
    ;                               \ |  |  | /                                                         \         /
    ;                                \|  |   /                                                           \       /
    ;                           ___   \____ /__________________ orig base lip end ________________________\ ____/
    ;                           |     |    / \ PROFILE_PNT_AT_LIP_END                                      \    | 
    ;                           |     |   /                                                                 \   | 
    ;           BEND_LINE_DIST {      |  /-------------------- bend end -----------+-------------------------\  |
    ;                           |     | / \ PROFILE_PNT_AT_BEND_ZONE               |                          \ |
    ;                           +--   |/--------------------- bend line ----------- } BEND_FLAT_LENGTH --------\
    ;       BEND_FLAT_LENGTH/2 {      /                                            |                            \
    ;                           +--   |----------------------- bend start ---------+----------------------------|
    ;                           |     | \ BEND_START_PNT                                                        |  
    ;                           |     |                                                                         |            
    ;                           |     |                                                                         |   
    ;               BEND_DEPTH {    Y ^                                                                         ^ Y   
    ;                           |     |                                                                         |  
    ;                           |     |   X                                                                     |  X
    ;                            ---  +---->                                                                 0,0+--->        
    ;                                 |                                                                         |   
    ;                                 v    (direction of BEND_DEPTH)      Base Face                             |            
    ;                                                                                                   
                                                                                                        
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
         )
         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******
         (if CONCAVE
         ; distinguish cases where bend zone overlaps original lip length and where it does not
           (POLYGON EDGE_VTX PROFILE_PNT_AT_BEND_ZONE PROFILE_PNT_AT_LIP_END EDGE_VTX)  
           (if (< (ABS (GPNT2D_Y PROFILE_PNT_AT_BEND_ZONE)) (ABS (GPNT2D_Y PROFILE_PNT_AT_LIP_END)))
             (POLYGON EDGE_VTX BEND_START_PNT PROFILE_PNT_AT_BEND_ZONE PROFILE_PNT_AT_LIP_END EDGE_VTX)  
             (POLYGON EDGE_VTX BEND_START_PNT PROFILE_PNT_AT_BEND_ZONE EDGE_VTX)  
           )
         )

      tool-wp  ; return wp 
      ;(setq result (sha-profile-of-wp tool-wp))
      ;(sha-delete-or-show-wp)
      ;result
   )
)

(defun sha-miter-spline-profile
    (&key
     PERCT_DEPTH  
    ;THICKNESS  
    ;RESOLUTION  
     SIDE  
     BEND_RADIUS  
     BEND_ANGLE  
     BEND_FLAT_LENGTH  
     BEND_LINE_DIST 
     NEW-LIP-START-DIST
     EDGE_VTX    
     BEND_START_PNT  
     BEND_START_DIR  
     PROFILE_PNT_AT_BEND_ZONE 
     PROFILE_PNT_AT_LIP_END  
     PROFILE_DIR
     MID-POINTS ; number of middle points in spline
     &allow-other-keys
    )

    ;  with: :LENGTH_X_ANGLE (:DEG 0) ; rot angle when bend is in X_POS; value in shop tool table
    ;
    ;  usual ~90 deg bend angle case                                                                            
    ;                                               _______________________________________________                     
    ;                                              /                                               \                 
    ;                                             /               New Lip                           \                  
    ;                                 |--d1--|   /                                                   \                
    ;                                 |      |  /  \                                                  \            
    ;                                 |  |-d2| / new-lip-side-angle                                    \               
    ;                    EDGE_VTX     |  |   |/      \                                                  \                  
    ;                             \   |  |   /--------------- bend end   ----------+---------------------\          EDGE_VTX
    ;                              \  |  |  / \                                    |                      \        /
    ;                               \ |  | /   PROFILE_PNT_AT_BEND_ZONE            |                       \      /
    ;                           __     __ /__________________ orig base lip end ___|________________________\ __
    ;           BEND_LINE_DIST {      |  / \                                       |                         \  |
    ;                           --    | / --\---------------- bend line ----------- } ------------------------\ |
    ;                           |      /     \                                     | BEND_FLAT_LENGTH          \
    ;       BEND_FLAT_LENGTH/2 /      /       PROFILE_PNT_AT_LIP_END               |                            \
    ;                          \      |                                            |                            |
    ;                           |     |                                            |                            |  
    ;                           |---  |----------------------- bend start ---------+----------------------------|            
    ;                           |     | \ BEND_START_PNT                                                        |   
    ;               BEND_DEPTH {    Y ^                                                                         ^ Y   
    ;                           |     |                                                                         |  
    ;                           |     |   X                                                                     |  X
    ;                            ---  +---->                                                                 0,0+--->        
    ;                                 |                                                                         |   
    ;                                 v    (direction of BEND_DEPTH)      Base Face                             |            
    ;                                                                                                   
    ;  with: :LENGTH_X_ANGLE (:DEG 0) ; rot angle when bend is in X_POS; value in shop tool table
    ;
    ;  smaller bend angle situation (e.g. 60 deg, R=1.6):                                                                            
    ;                                                 ___________________________________________                     
    ;                                                /                                           \                 
    ;                                               /               New Lip                       \                  
    ;                                              /                                               \                
    ;                                             /  \                                              \            
    ;                                 |d1|d2|    / new-lip-side-angle                                \               
    ;                     EDGE_VTX    |  |  |   /      \                                              \                  
    ;                              \  |  |  |  /                                                       \         EDGE_VTX
    ;                               \ |  |  | /                                                         \         /
    ;                                \|  |   /                                                           \       /
    ;                           ___   \____ /__________________ orig base lip end ________________________\ ____/
    ;                           |     |    / \ PROFILE_PNT_AT_LIP_END                                      \    | 
    ;                           |     |   /                                                                 \   | 
    ;           BEND_LINE_DIST {      |  /-------------------- bend end -----------+-------------------------\  |
    ;                           |     | / \ PROFILE_PNT_AT_BEND_ZONE               |                          \ |
    ;                           +--   |/--------------------- bend line ----------- } BEND_FLAT_LENGTH --------\
    ;       BEND_FLAT_LENGTH/2 {      /                                            |                            \
    ;                           +--   |----------------------- bend start ---------+----------------------------|
    ;                           |     | \ BEND_START_PNT                                                        |  
    ;                           |     |                                                                         |            
    ;                           |     |                                                                         |   
    ;               BEND_DEPTH {    Y ^                                                                         ^ Y   
    ;                           |     |                                                                         |  
    ;                           |     |   X                                                                     |  X
    ;                            ---  +---->                                                                 0,0+--->        
    ;                                 |                                                                         |   
    ;                                 v    (direction of BEND_DEPTH)      Base Face                             |            
    ;                                                                                                   
                                                                                                        
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           relief-pnts
           (pnt-close-to-edge-vtx (sd-vec-add EDGE_VTX (sd-vec-scale (sd-vec-subtract BEND_START_PNT EDGE_VTX) 0.1)))
           (pnt-close-to-profile (sha-miter-spline-mid-point :STEP-FACT (- 1 (/ 1 (* MID-POINTS 10)))
                                                             :PROFILE-PNT-AT-BEND-ZONE PROFILE_PNT_AT_BEND_ZONE
                                                             :BEND-START-PNT BEND_START_PNT 
                                                             :BEND-START-DIR BEND_START_DIR
                                                             :BEND-RADIUS BEND_RADIUS 
                                                             :BEND-ANGLE  BEND_ANGLE 
                                                             :PROFILE-DIR PROFILE_DIR 
                                                             :BEND-FLAT-LENGTH BEND_FLAT_LENGTH
                                                             :NEW-LIP-START-DIST NEW-LIP-START-DIST))
         )
     ;(dbg PROFILE_PNT_AT_BEND_ZONE)
      (create_workplane :new
                        :name tool-wp
                        :world_origin
      )
      
      (when *sheet-advisor-show-tool*
        (GEOMETRY_MODE :CONSTRUCTION)
        (POLYGON (sd-vec-add BEND_START_PNT BEND_START_DIR) BEND_START_PNT EDGE_VTX PROFILE_PNT_AT_LIP_END
                 (sd-vec-add PROFILE_PNT_AT_LIP_END PROFILE_DIR))
        (POLYGON pnt-close-to-profile pnt-close-to-edge-vtx )
        (GEOMETRY_MODE :REAL)
      )
      (marabou::dbg-save :ALL_AT_TOP "_after_check_geo_in_profile")
      (push PROFILE_PNT_AT_BEND_ZONE relief-pnts)
      (push pnt-close-to-profile relief-pnts) ; near tangent condition
      
      (dotimes (step MID-POINTS)
        (let ((one-step (/ 1.0 (+ MID-POINTS 1))))
          (push (sha-miter-spline-mid-point :STEP-FACT (- 1.0 (* one-step (+ step 1)))
                                            :PROFILE-PNT-AT-BEND-ZONE PROFILE_PNT_AT_BEND_ZONE
                                            :BEND-START-PNT BEND_START_PNT 
                                            :BEND-START-DIR BEND_START_DIR
                                            :BEND-RADIUS BEND_RADIUS 
                                            :BEND-ANGLE  BEND_ANGLE 
                                            :PROFILE-DIR PROFILE_DIR 
                                            :BEND-FLAT-LENGTH BEND_FLAT_LENGTH
                                            :NEW-LIP-START-DIST NEW-LIP-START-DIST)

                relief-pnts) 
        )
      )
      (push (sd-vec-subtract BEND_START_PNT BEND_START_DIR) relief-pnts)
      (push :TANGENT relief-pnts)
      (push BEND_START_PNT relief-pnts)

     ;(dbg relief-pnts)
      ;********* create Profile *******
      ; distinguish cases where bend zone overlaps original lip length and where it does not
      (if (< (ABS (GPNT2D_Y PROFILE_PNT_AT_BEND_ZONE)) (ABS (GPNT2D_Y PROFILE_PNT_AT_LIP_END)))
        (POLYGON PROFILE_PNT_AT_BEND_ZONE PROFILE_PNT_AT_LIP_END EDGE_VTX BEND_START_PNT)  
        (POLYGON PROFILE_PNT_AT_BEND_ZONE EDGE_VTX BEND_START_PNT)  
      )

      (APPLY 'BSPLINE_INT relief-pnts)

      ; check for self overlaps and trim
      ;(sd-call-cmds 
        (TRIM :two_elements pnt-close-to-profile pnt-close-to-edge-vtx )
      ;  :failure nil
      ;)
       
      tool-wp
      ;(setq result (sha-profile-of-wp tool-wp))
      ;(sha-delete-or-show-wp)
      ;result
   )
)

;=================================================================
;
;   MIN-PERP-V-RELIEF (minimal, (to bend zone) perpendicular corner relief 
;   with variable angle)
;            ( actual angle passed via parameter :CRN_ANGLE)
;
;                                                                              
;           \    Sheet metal basic face                                        
;            \                                                                 
;   other     \    inner-corner-point                                        
;   bend       \  /                                                            
;               \_____________________                 _                       
;             _//|                     \                |                       
;           _/ / |                      } bend_radius   |                       
;  \  P1  _/ g/  |____________________ /                |                  
;   \ | _/  a/   |                                      } bend_flat_length      
;lip2\_/   i/    |           new bend                   |       
;     \   d/     | P3= inner-corner-projection-point    |                      
;      \  /a/2   | /                                    |                      
;       \/_______|______________________         _______|                      
;       /     |\  s                                                             
;      /  \   | \____profile_pnt_at_bend_zone                                   
;     /    \  |                                                                
;   |_      outer corner-point                                          
;             |                                                       
;             |__________________________                             
;     
; WP +Y direction                                                          
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;============================================================================n

(defun sha-min-perp-v-shape-profile
  (&key ;MIN_WIDTH MIN_LENGTH MIN_ANGLE ; not yet used
        ; following parameter automatically derived from context
        CORNER_ANGLE BEND_RADIUS BEND_FLAT_LENGTH BEND_LINE_DIST
        OTHER_BEND_RADIUS OTHER_BEND_FLAT_LENGTH
        THICKNESS PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
                  PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT
   &allow-other-keys)
   (progn 
      ; set defaults for unspecified usage
      (unless corner_angle (setf corner_angle (sd-deg-to-rad 90)))
      (unless bend_radius (setf bend_radius (MBU-GET-DEFAULT-BEND-RADIUS)))
      (unless other_bend_radius (setf other_bend_radius bend_radius))
      (unless bend_flat_length
        (setf bend_flat_length (* (+ bend_radius (* thickness 0.4) ) PI 0.5)))
      (unless other_bend_flat_length 
        (setf other_bend_flat_length (* (+ other_bend_radius (* thickness 0.4))
                                        PI 0.5)))
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
          )

         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******

         ; check for self intersecting cut profile
         (if (> 0 (* (gpnt2d_x INNER_CORNER_PROJECTION_POINT)
                     (gpnt2d_x OTHER_INNER_CORNER_PROJECTION_POINT)))
           ; regular case -> take given points
           (POLYGON INNER_CORNER_POINT INNER_CORNER_PROJECTION_POINT
                    OUTER_CORNER_POINT OTHER_INNER_CORNER_PROJECTION_POINT
                    INNER_CORNER_POINT)
           ; self intersecting (very asymmetric case) -> create a triangle only
           (let ((RELEVANT-PROJECTION-POINT 
                   (if (< (gpnt2d_y INNER_CORNER_PROJECTION_POINT )
                          (gpnt2d_y OTHER_INNER_CORNER_PROJECTION_POINT))
                     INNER_CORNER_PROJECTION_POINT
                     OTHER_INNER_CORNER_PROJECTION_POINT))
                 (IRELEVANT-PROJECTION-POINT 
                   (if (< (gpnt2d_y INNER_CORNER_PROJECTION_POINT )
                          (gpnt2d_y OTHER_INNER_CORNER_PROJECTION_POINT))
                     OTHER_INNER_CORNER_PROJECTION_POINT
                     INNER_CORNER_PROJECTION_POINT))
                )
             (POLYGON INNER_CORNER_POINT 
                    (sha-line-line-intersection
                      INNER_CORNER_POINT
                      (sd-vec-normalize
                        (sd-vec-subtract IRELEVANT-PROJECTION-POINT
                                         INNER_CORNER_POINT))
                      RELEVANT-PROJECTION-POINT       
                      (sd-vec-normalize
                        (sd-vec-subtract OUTER_CORNER_POINT   
                                         RELEVANT-PROJECTION-POINT)))
                    RELEVANT-PROJECTION-POINT       
                    INNER_CORNER_POINT)
           )
         )

         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          INNER_CORNER_POINT
          OUTER_CORNER_POINT
          INNER_CORNER_PROJECTION_POINT
          OTHER_INNER_CORNER_PROJECTION_POINT
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-corner-wp)
         result
    )
  )
)

;============================================================================
;
;   CONNECTED-V-RELIEF relief connected to intersection of lip edge with bend
;   zone 
;   optionally blended with relative blend position
;      relative position (% of diagonal with 0=inner-corner-point (= sharp)  
;                                            100%=at profile_pnt-at_bend_zone)
;   (with variable angle)
;   ( actual angle passed via parameter :CRN_ANGLE)
;
;                                                                              
;           \    Sheet metal basic face                                        
;            \                                                                 
;   other     \   P2=inner-corner-point                                        
;   bend       \  /                                                            
;               \_____________________                 _                       
;             _//|                     \                |                       
;           _/ / |                      } bend_radius   |                       
;  \  P1  _/_g/  /____________________ /                |                  
;   \ | _/  a/\ /                                       } bend_flat_length      
;lip2\_/   i/  |\            new bend                   |       
;     \   d/   | optional blend                         |                      
;      \  /a/2 /                                        |                      
;       \/____|_________________________         _______|                      
;       /     |\                                                                
;      /  \   | \____profile_pnt_at_bend_zone                                   
;     /    \  |                                                                
;   |_      outer corner-point                                          
;             |                                                       
;             |__________________________                             
;     
; WP +Y direction                                                          
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;=============================================================================

(defun sha-connected-v-shape-profile
  (&key ;MIN_WIDTH MIN_LENGTH MIN_ANGLE ; not yet used
        (REL_BLEND_POS 0) (BLEND_RADIUS 0)
        ; following parameter automatically derived from context
        RESOLUTION SIDE
        CORNER_ANGLE BEND_RADIUS BEND_FLAT_LENGTH BEND_LINE_DIST
        OTHER_BEND_RADIUS OTHER_BEND_FLAT_LENGTH
        THICKNESS PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
                  PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT  
   &allow-other-keys)
   (progn 
      ; set defaults for unspecified usage
      (unless corner_angle (setf corner_angle (sd-deg-to-rad 90)))
      (unless bend_radius (setf bend_radius (MBU-GET-DEFAULT-BEND-RADIUS)))
      (unless other_bend_radius (setf other_bend_radius bend_radius))
      (unless bend_flat_length
        (setf bend_flat_length (* (+ bend_radius (* thickness 0.4) ) PI 0.5)))
      (unless other_bend_flat_length 
        (setf other_bend_flat_length (* (+ other_bend_radius (* thickness 0.4))
                                        PI 0.5)))
      ; if other profile point at bend zone not found, use symmetric point
      (unless OTHER_PROFILE_PNT_AT_BEND_ZONE
        (setf OTHER_PROFILE_PNT_AT_BEND_ZONE 
                (make-gpnt2d :x (* -1 (gpnt2d_x profile_pnt_at_bend_zone))
                             :y (gpnt2d_y profile_pnt_at_bend_zone))))
        
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           (xpp (gpnt2d_x PROFILE_PNT_AT_BEND_ZONE))
           (xop (gpnt2d_x OTHER_PROFILE_PNT_AT_BEND_ZONE)) 
           (X-case (or (NOT (< (* xpp xop) RESOLUTION)) 
                       (sd-vec-equal-p PROFILE_PNT_AT_BEND_ZONE OUTER_CORNER_POINT :resolution RESOLUTION)
                       (sd-vec-equal-p OTHER_PROFILE_PNT_AT_BEND_ZONE OUTER_CORNER_POINT :resolution RESOLUTION)))
           (Vvec1 (sd-vec-subtract PROFILE_PNT_AT_BEND_ZONE
                                   INNER_CORNER_POINT))
           (Vdir1 (sd-vec-normalize Vvec1))
           (Vvec2 (sd-vec-subtract OTHER_PROFILE_PNT_AT_BEND_ZONE
                                   INNER_CORNER_POINT))
           (Vdir2 (sd-vec-normalize Vvec2))
           (Vmin-length (min (sd-vec-length Vvec1) (sd-vec-length Vvec2)))
           (V-angle (if (equal SIDE :LEFT)
                      (sd-vec-angle-between vdir1 vdir2)
                      (sd-vec-angle-between vdir2 vdir1)
                    ))
           (do-blend (if (or (NOT (sd-num-equal-p 0 REL_BLEND_POS))
                             (NOT (sd-num-equal-p 0 BLEND_RADIUS))) t nil))
           
           (half-tan (if do-blend (tan (* 0.5 V-angle))))
           (half-sin (if do-blend (sin (* 0.5 V-angle))))
           (half-cos (if do-blend (cos (* 0.5 V-angle))))
           (max-dist (if do-blend (/ Vmin-length half-cos)))
           (center-dist (if do-blend
                          (if (sd-num-equal-p 0 BLEND_RADIUS) 
                            (* max-dist REL_BLEND_POS)
                            ; radius is dominant
                            (/ BLEND_RADIUS half-sin)
                        )))
           (symm-dir (if do-blend (sd-vec-normalize (sd-vec-add vdir1 vdir2))))
           (tang-radius (if do-blend (* half-sin center-dist)))
           (blend-radius (if do-blend 
                           (if (sd-num-equal-p 0 BLEND_RADIUS)
                             tang-radius
                             BLEND_RADIUS)))
           (blend-center (if do-blend
                           (sd-vec-add INNER_CORNER_POINT
                                       (sd-vec-scale symm-dir center-dist))))
           (actual-tan-dist (if do-blend (/ blend-radius half-tan)))
           (tang-pnt1    (if do-blend
                             (sd-vec-add INNER_CORNER_POINT
                                (sd-vec-scale vdir1 actual-tan-dist))))
           (tang-pnt2    (if do-blend
                           (sd-vec-add INNER_CORNER_POINT
                              (sd-vec-scale vdir2 actual-tan-dist))))
         )

         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******
         ; in case of X profile use triangle  
         ; ATTENTION ! this will usually produce a self intersecting corner 
         ; region

         (if do-blend
           (if (> actual-tan-dist (+ Vmin-length resolution))
              ; make circle only
             (CIRCLE :CENTER blend-center BLEND_RADIUS)
             (progn
               (APPLY 'POLYGON
                    (delete-duplicates (list tang-pnt1
                                             PROFILE_PNT_AT_BEND_ZONE
                                             OUTER_CORNER_POINT
                                             OTHER_PROFILE_PNT_AT_BEND_ZONE
                                             tang-pnt2)
                                       :test 'oli::sd-vec-equal-p))
               (if (equal side :LEFT)
                 (ARC :CENTER blend-center tang-pnt2 tang-pnt1)
                 (ARC :CENTER blend-center tang-pnt1 tang-pnt2)
               )
             )
           )
           (if X-case
             ; X profile -> make triangle only
             (POLYGON INNER_CORNER_POINT 
                      PROFILE_PNT_AT_BEND_ZONE
                      OTHER_PROFILE_PNT_AT_BEND_ZONE
                      INNER_CORNER_POINT)
             ; regular case -> cut profile is a quadrangle
             (POLYGON INNER_CORNER_POINT 
                      PROFILE_PNT_AT_BEND_ZONE
                      OUTER_CORNER_POINT
                      OTHER_PROFILE_PNT_AT_BEND_ZONE
                      INNER_CORNER_POINT)
           )
         )
         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          INNER_CORNER_POINT
          OUTER_CORNER_POINT
          PROFILE_PNT_AT_BEND_ZONE
          OTHER_PROFILE_PNT_AT_BEND_ZONE
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-corner-wp)
         result
    )
  )
)

;============================================================================
;
;   SPLINE-V-RELIEF spline relief smooth connected to intersection
;   of lip edge  
;   relative depth position (% of diagonal with 0=inner-corner-point (= sharp)  
;                                            100%=at profile_pnt-at_bend_zone)
;   (with variable angle)
;   ( actual angle passed via parameter :CRN_ANGLE)
;
;                                                                              
;           \    Sheet metal basic face                                        
;            \                                                                 
;   other     \   P2=inner-corner-point                                        
;   bend       \  /                                                            
;               \_____________________                 _                       
;               /                                       |                       
;              /                                        |                       
;  \  P1   __g/                                         |                  
;   \ |  _/ a/\                                         } bend_flat_length      
;____\__/  i/ |          new bend                       |       
;     \   d/  |                                         |                      
;      \  /a/2|                                        |                      
;       \/____|_________________________      _  _______|                      
;       /     |\                               |                                
;      /  \   | \____profile_pnt_at_bend_zone  |                                
;     /    \  |                                |                               
;   |_      outer corner-point                  } optional START_OFFSET  
;             |                                |                      
;             |                                |                      
;             |\_____________________________ _|                               
;             |                                                       
;             |                                                       
;             |                                                       
;             |                                                       
;             |__________________________                             
;     
; WP +Y direction                                                          
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;=============================================================================

(defun sha-connected-spline-V-profile
  (&key (REL_EXTREM_POS 0) (FORM_FACTOR 1) (START_OFFSET 0) 
        (OTHER_START_OFFSET 0)  (AUTOMATIC_ADAPTION t)
        MIN_DIST  ; parameter used as minimum dist between control points of
                  ; spline to avoid self overlaps
        ; following parameter automatically derived from context
        RESOLUTION SIDE
        CORNER_ANGLE BEND_RADIUS BEND_ANGLE BEND_FLAT_LENGTH BEND_LINE_DIST
        OTHER_BEND_RADIUS OTHER_BEND_ANGLE OTHER_BEND_FLAT_LENGTH
        THICKNESS PROFILE_PNT_AT_BEND_ZONE PROFILE_DIR_AT_BEND_ZONE
        OTHER_PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT  
   &allow-other-keys)
   (let* ((orig-profile-pnt-at-bend-zone PROFILE_PNT_AT_BEND_ZONE)
         (orig-other-profile-pnt-at-bend-zone OTHER_PROFILE_PNT_AT_BEND_ZONE)
         (this-dist (sd-vec-length 
                       (sd-vec-subtract PROFILE_PNT_AT_BEND_ZONE
                                        INNER_CORNER_POINT)))
         (other-dist (sd-vec-length 
                       (sd-vec-subtract OTHER_PROFILE_PNT_AT_BEND_ZONE
                                        INNER_CORNER_POINT)))
  
         (xpp (gpnt2d_x PROFILE_PNT_AT_BEND_ZONE))
         (xop (gpnt2d_x OTHER_PROFILE_PNT_AT_BEND_ZONE)) 
         (X-case (NOT (< (* xpp xop) RESOLUTION)))
         (tool-wp   (sha-tool-wp))
         result
        )


    ; set defaults for unspecified usage
    (unless MIN_DIST (setf min_dist (* 0.1 thickness)))
    (unless corner_angle (setf corner_angle (sd-deg-to-rad 90)))
    (unless bend_radius (setf bend_radius (MBU-GET-DEFAULT-BEND-RADIUS)))
    (unless other_bend_radius (setf other_bend_radius bend_radius))
    (unless bend_flat_length
      (setf bend_flat_length (* (+ bend_radius (* thickness 0.4) ) PI 0.5)))
    (unless other_bend_flat_length 
      (setf other_bend_flat_length (* (+ other_bend_radius (* thickness 0.4))
                                      PI 0.5)))
    ; if other profile point at bend zone not found, use symmetric point
    (unless OTHER_PROFILE_PNT_AT_BEND_ZONE
      (setf OTHER_PROFILE_PNT_AT_BEND_ZONE 
              (make-gpnt2d :x (* -1 (gpnt2d_x profile_pnt_at_bend_zone))
                           :y (gpnt2d_y profile_pnt_at_bend_zone))))
        
      
    (create_workplane :new
                      :name tool-wp
                      :world_origin
    )

      ; if relief should start at different position change start points
      ; point
    (if AUTOMATIC_ADAPTION
      (let ((profile-data (sha-corner-data
              :CORNER_ANGLE CORNER_ANGLE :BEND_RADIUS BEND_RADIUS 
              :BEND_FLAT_LENGTH BEND_FLAT_LENGTH
              :OTHER_BEND_RADIUS OTHER_BEND_RADIUS 
              :OTHER_BEND_FLAT_LENGTH OTHER_BEND_FLAT_LENGTH
              :THICKNESS THICKNESS 
              :PROFILE_PNT_AT_BEND_ZONE PROFILE_PNT_AT_BEND_ZONE 
              :PROFILE_DIR_AT_BEND_ZONE PROFILE_DIR_AT_BEND_ZONE
              :OTHER_PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE 
              :OTHER_PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
              :INNER_CORNER_POINT INNER_CORNER_POINT 
              :OUTER_CORNER_POINT OUTER_CORNER_POINT 
              :INNER_CORNER_PROJECTION_POINT INNER_CORNER_PROJECTION_POINT
              :OTHER_INNER_CORNER_PROJECTION_POINT 
                OTHER_INNER_CORNER_PROJECTION_POINT 
              :BEND_ANGLE BEND_ANGLE :OTHER_BEND_ANGLE OTHER_BEND_ANGLE
              :REL_EXTREM_POS REL_EXTREM_POS
             ))
           )
       ;********* create Profile *******
       (apply 'BSPLINE_INT 
          `(,(getf profile-data :short-relief-start-pnt)
            :TANGENT 
            ,(sd-vec-add (getf profile-data :short-relief-start-pnt)
                         (sd-vec-scale 
                           (getf profile-data :short-relief-start-dir) -1))
            ,@(when (< (* thickness 0.5)
                           (sd-vec-length 
                             (sd-vec-subtract 
                               (getf profile-data :short-relief-start-pnt)
                               (getf profile-data :short-relief-support-pnt))))
                `(,(getf profile-data :short-relief-support-pnt))
             )
            ,(getf profile-data :extrem-position)
            ,@(when (< (* thickness 0.5)
                       (sd-vec-length 
                         (sd-vec-subtract 
                            (getf profile-data :long-relief-start-pnt)
                            (getf profile-data :long-relief-support-pnt))))
                 `(,(getf profile-data :long-relief-support-pnt)))
            ,(getf profile-data :long-relief-start-pnt)
            :TANGENT 
            ,(sd-vec-add (getf profile-data :long-relief-start-pnt)
                         (getf profile-data :long-relief-start-dir))
           )
       )
       ; check for spline overlap with closing polygon
       (let* ((short-start (getf profile-data :short-relief-start-pnt))
             (long-start  (getf profile-data :long-relief-start-pnt))
             (long-dir    (getf profile-data :long-relief-start-dir))
             (short-dir   (getf profile-data :short-relief-start-dir))
             (short-long-vec (sd-vec-subtract long-start short-start))
             (short-long-dir (sd-vec-normalize short-long-vec))
             (short-long-perp (make-gpnt2d :x (* -1 (gpnt2d_x short-long-dir))
                                           :y (gpnt2d_y short-long-dir)))
             (mid-point  (sd-vec-scale (sd-vec-add long-start short-start) 0.5))
             (imid-vec (sd-vec-subtract mid-point INNER_CORNER_POINT))
             (imid-dir (sd-vec-normalize imid-vec))
             (imid-dist  (sd-vec-length imid-vec))
             (long-imid-inters (sha-line-line-intersection 
                                  mid-point imid-dir
                                  long-start long-dir))
             (short-imid-inters (sha-line-line-intersection 
                                  mid-point imid-dir
                                  short-start short-dir))
             (io-vec (sd-vec-subtract OUTER_CORNER_POINT INNER_CORNER_POINT))
             (io-dist (sd-vec-length io-vec))
             (ispi-vec (sd-vec-subtract short-imid-inters INNER_CORNER_POINT))
             (ispi-dir (sd-vec-normalize ispi-vec))
             (ispi-dist  (sd-vec-length ispi-vec))
             (ilpi-vec (sd-vec-subtract long-imid-inters INNER_CORNER_POINT))
             (ilpi-dir (sd-vec-normalize ilpi-vec))
             (ilpi-dist  (sd-vec-length ilpi-vec))
             (poly-midpoint (unless X-case OUTER_CORNER_POINT))
            )
         (when (and (sd-vec-null-p (sd-vec-subtract ispi-dir imid-dir))
                   (> ispi-dist imid-dist)
                   (> ispi-dist io-dist))
           (setf poly-midpoint short-imid-inters))
         (when (and (sd-vec-null-p (sd-vec-subtract ilpi-dir imid-dir))
                   (> ilpi-dist imid-dist)
                   (> ilpi-dist io-dist)
                   (NOT (and (sd-vec-null-p (sd-vec-subtract ispi-vec imid-vec))
                             (> ispi-dist ilpi-dist))))
           (setf poly-midpoint long-imid-inters))
         
         (if poly-midpoint
           (POLYGON short-start poly-midpoint long-start)
           (POLYGON short-start long-start)
         )
       )

       (when *sheet-advisor-show-tool*
         (GEOMETRY_MODE :CONSTRUCTION)
         (POLYGON (sd-vec-add PROFILE_PNT_AT_BEND_ZONE 
                              PROFILE_DIR_AT_BEND_ZONE)
                  PROFILE_PNT_AT_BEND_ZONE
                  OUTER_CORNER_POINT
                  OTHER_PROFILE_PNT_AT_BEND_ZONE
                  (sd-vec-add OTHER_PROFILE_PNT_AT_BEND_ZONE
                              OTHER_PROFILE_DIR_AT_BEND_ZONE))
         (POLYGON PROFILE_PNT_AT_BEND_ZONE INNER_CORNER_POINT
                  OTHER_PROFILE_PNT_AT_BEND_ZONE)
          
         (apply 'POLYGON `(,(getf profile-data :short-relief-start-pnt) 
                           ,@(when (< (* thickness 0.05)
                              (sd-vec-length
                                (sd-vec-subtract
                                (getf profile-data :short-relief-start-pnt)
                                (getf profile-data :short-relief-support-pnt))))
                             `(,(getf profile-data :short-relief-support-pnt) ))
                            ,(getf profile-data :extrem-position)
                          ,@(when (< (* thickness 0.05)
                           (sd-vec-length
                             (sd-vec-subtract
                               (getf profile-data :long-relief-start-pnt)
                               (getf profile-data :long-relief-support-pnt))))
                            `(,(getf profile-data :long-relief-support-pnt)))
                            ,(getf profile-data :long-relief-start-pnt)))
         (GEOMETRY_MODE :REAL)
       ); when debugging on
      )
      (progn
        (when START_OFFSET 
          (setf PROFILE_PNT_AT_BEND_ZONE 
               (sd-vec-add PROFILE_PNT_AT_BEND_ZONE
                 (sd-vec-scale PROFILE_DIR_AT_BEND_ZONE START_OFFSET))))
        (when OTHER_START_OFFSET 
          (setf OTHER_PROFILE_PNT_AT_BEND_ZONE 
            (sd-vec-add OTHER_PROFILE_PNT_AT_BEND_ZONE
             (sd-vec-scale OTHER_PROFILE_DIR_AT_BEND_ZONE OTHER_START_OFFSET))))
  
        (let* ((pp-dir   (sd-vec-normalize 
                         (sd-vec-subtract PROFILE_PNT_AT_BEND_ZONE
                                          INNER_CORNER_POINT)))
             (opp-dir   (sd-vec-normalize 
                         (sd-vec-subtract OTHER_PROFILE_PNT_AT_BEND_ZONE
                                          INNER_CORNER_POINT)))
             (symm-dir (sd-vec-normalize 
                         (sd-vec-add pp-dir opp-dir)))
  
             (max-dist (sd-vec-length 
                         (sd-vec-subtract
                           (sd-vec-scale (sd-vec-add PROFILE_PNT_AT_BEND_ZONE 
                                                  OTHER_PROFILE_PNT_AT_BEND_ZONE)
                                          0.5)
                           INNER_CORNER_POINT)))
  
             (extrem-dist (* max-dist REL_EXTREM_POS))
             (extrem-position (sd-vec-add INNER_CORNER_POINT  
                                 (sd-vec-scale symm-dir extrem-dist)))
           )
  
           ;********* create Profile *******
  
           (BSPLINE_INT PROFILE_PNT_AT_BEND_ZONE 
             :TANGENT (sd-vec-add PROFILE_PNT_AT_BEND_ZONE 
                                  (sd-vec-scale PROFILE_DIR_AT_BEND_ZONE -1))
             extrem-position  OTHER_PROFILE_PNT_AT_BEND_ZONE 
             :TANGENT (sd-vec-add OTHER_PROFILE_PNT_AT_BEND_ZONE
                                  OTHER_PROFILE_DIR_AT_BEND_ZONE))
  
           ; check for self overlap or form-factor
           (let* ((actual-spline (getres (get_selection :NO_HIGHLIGHT
                                     :focus_type *sd-spline-2d-seltype*
                                     :SINGLE_SELECTION :CURR_WP_ONLY
                                     :select :all_2d)))
                  (knot-vectors (sd-inq-bspline-edge-knot-vector actual-spline))
                  (bspline-geo (sd-inq-edge-geo actual-spline 
                                            :dest-space tool-wp))
                  (ctrl-pnt2 (sd-gpnt3d-to-2d 
                                (sd-inq-bspline-edge-ctrl-pnt actual-spline 
                                  :index 2 :dest-space :tool-wp)))
                  (ctrl-pnt3 (sd-gpnt3d-to-2d 
                                 (sd-inq-bspline-edge-ctrl-pnt actual-spline 
                                  :index 3 :dest-space :tool-wp)))
                  (ctrl-pnt4 (sd-gpnt3d-to-2d 
                                 (sd-inq-bspline-edge-ctrl-pnt actual-spline 
                                  :index 4 :dest-space :tool-wp)))
                  (ctrl-vec2 (sd-vec-subtract ctrl-pnt2 
                                                PROFILE_PNT_AT_BEND_ZONE))
                  (ctrl-dir2 (sd-vec-normalize ctrl-vec2))
                  (ctrl-vec4 (sd-vec-subtract ctrl-pnt4
                                                OTHER_PROFILE_PNT_AT_BEND_ZONE))
                  (ctrl-dir4 (sd-vec-normalize ctrl-vec4))
                  (move-vec2 (sd-vec-scale ctrl-vec2 (- FORM_FACTOR 1)))
                  (move-vec4 (sd-vec-scale ctrl-vec4 (- FORM_FACTOR 1)))
                  (new-ctrl-pnt2 (sd-vec-add ctrl-pnt2 move-vec2))
                  (new-ctrl-pnt4 (sd-vec-add ctrl-pnt4 move-vec4))
                  (mid-ctrl-pnt24 (sd-vec-scale 
                                   (sd-vec-add new-ctrl-pnt2 new-ctrl-pnt4) 0.5))
                  (new-ctrl-pnt3 (sd-vec-add mid-ctrl-pnt24
                                    (sd-vec-scale
                                     (sd-vec-subtract extrem-position 
                                                    mid-ctrl-pnt24) 2.0)))
                  (control-points-changed (sd-num-equal-p 1.0 FORM_FACTOR))
                  (lengthv2 (sd-vec-length (sd-vec-subtract new-ctrl-pnt2
                                                PROFILE_PNT_AT_BEND_ZONE)))
                  (lengthv4 (sd-vec-length (sd-vec-subtract new-ctrl-pnt4
                                                OTHER_PROFILE_PNT_AT_BEND_ZONE)))
                ; check for self overlap
  
                  (ctrl-inters24 (sha-4pnt-intersection
                                  PROFILE_PNT_AT_BEND_ZONE new-ctrl-pnt2
                                  OTHER_PROFILE_PNT_AT_BEND_ZONE new-ctrl-pnt4))
                  (ctrl-inters2 (sha-4pnt-intersection
                                  PROFILE_PNT_AT_BEND_ZONE new-ctrl-pnt2
                                  INNER_CORNER_POINT new-ctrl-pnt4))
                  (ctrl-inters4 (sha-4pnt-intersection
                                  INNER_CORNER_POINT new-ctrl-pnt2
                                  OTHER_PROFILE_PNT_AT_BEND_ZONE new-ctrl-pnt4))
                  new-ctrl-pnt3a 
                  new-ctrl-pnt3b 
                  new-ctrl-pnt3c 
                  (new-extrem (sd-vec-scale 
                                 (sd-vec-add
                                  (sd-vec-scale
                                   (sd-vec-add new-ctrl-pnt2 new-ctrl-pnt3) 0.5)
                                  (sd-vec-scale
                                   (sd-vec-add new-ctrl-pnt4 new-ctrl-pnt3) 0.5)
                                 )
                                 0.5))
                 )
  
                (when (and ctrl-inters24
                           (NOT ctrl-inters2)
                           (NOT ctrl-inters4))
                  (let* ((vec13 (sd-vec-subtract new-ctrl-pnt3 
                                                PROFILE_PNT_AT_BEND_ZONE))
                         (vec53 (sd-vec-subtract new-ctrl-pnt3 
                                                OTHER_PROFILE_PNT_AT_BEND_ZONE))
                        (ang213 (mod (sd-vec-angle-between ctrl-vec2 vec13) PI))
                        (ang453 (mod (sd-vec-angle-between ctrl-vec4 vec53) PI))
                        (veci2 (sd-vec-subtract new-ctrl-pnt2 ctrl-inters24))
                        (veci4 (sd-vec-subtract new-ctrl-pnt4 ctrl-inters24))
                        (lv2 (sd-vec-length veci2))
                        (lv4 (sd-vec-length veci4))
                        (corrdist2 (min min_dist (* 0.8 (- lengthv2 lv2))))
                        (corrdist4 (min min_dist (* 0.8 (- lengthv4 lv4))))
                        
                       )
                    (if (> ang213 ang453)
                      (progn 
                        (setf new-ctrl-pnt2 
                               (sd-vec-subtract ctrl-inters24 
                                 (sd-vec-scale ctrl-dir2 corrdist2)))
                        (setf new-ctrl-pnt4 (sd-vec-add new-ctrl-pnt4
                                              (sd-vec-scale ctrl-dir4
                                                            corrdist4)))
                      )
                      (progn
                        (setf new-ctrl-pnt4 
                               (sd-vec-subtract ctrl-inters24 
                                 (sd-vec-scale ctrl-dir4 corrdist4)))
                        (setf new-ctrl-pnt2 (sd-vec-add new-ctrl-pnt2
                                              (sd-vec-scale ctrl-dir2 corrdist2)))
                      )
                    )
                    (setf control-points-changed t)
                  )
                )
                (when ctrl-inters2
                  (let* ((veci2 (sd-vec-subtract new-ctrl-pnt2 ctrl-inters2))
                         (lv2 (sd-vec-length veci2))
                         (corrdist2 (min MIN_DIST 
                                        (* 0.8 (- lengthv2 lv2))))
                         (corrdist4 (/ corrdist2 lengthv2))
                       )
                    (setf new-ctrl-pnt2 
                               (sd-vec-subtract ctrl-inters2 
                                 (sd-vec-scale ctrl-dir2 corrdist2)))
                    (setf new-ctrl-pnt4 (sd-vec-add new-ctrl-pnt4
                                          (sd-vec-scale ctrl-dir4 corrdist4)))
                    (setf control-points-changed t)
                  )
                )
                (when ctrl-inters4
                  (let* ((veci4 (sd-vec-subtract new-ctrl-pnt4 ctrl-inters4))
                         (lv4 (sd-vec-length veci4))
                         (corrdist4 (min MIN_DIST 
                                        (* 0.8 (- lengthv4 lv4))))
                         (corrdist2 (/ corrdist4 lengthv4))
                        )
                    (setf new-ctrl-pnt4 
                               (sd-vec-subtract ctrl-inters4 
                                 (sd-vec-scale ctrl-dir4 corrdist4)))
                    (setf new-ctrl-pnt2 (sd-vec-add new-ctrl-pnt2
                                          (sd-vec-scale ctrl-dir2 corrdist2)))
                    (setf control-points-changed t)
                  )
                )
                  
                (when control-points-changed
                  (setf mid-ctrl-pnt24 (sd-vec-scale 
                                  (sd-vec-add new-ctrl-pnt2 new-ctrl-pnt4) 0.5))
                  (setf new-ctrl-pnt3 (sd-vec-add mid-ctrl-pnt24
                                 (sd-vec-scale
                                   (sd-vec-subtract extrem-position 
                                                  mid-ctrl-pnt24) 2.0)))
                  ; In case of complete cut ensure passing of spline through
                  ; given point to avoid tiny edges.
                  ; Do this by splitting spline into 2 parts joining in given
                  ; point
  
                  (when (sd-num-equal-p 0 REL_EXTREM_POS)
                    (setf new-ctrl-pnt3a (sd-vec-scale
                                  (sd-vec-add new-ctrl-pnt2 new-ctrl-pnt3) 0.5))
                    (setf new-ctrl-pnt3c (sd-vec-scale
                                  (sd-vec-add new-ctrl-pnt4 new-ctrl-pnt3) 0.5))
                    (setf new-ctrl-pnt3b (sd-vec-scale
                                  (sd-vec-add new-ctrl-pnt3a new-ctrl-pnt3c) 0.5))
                  )
                )
                                       
             (when control-points-changed
                (DELETE_2D actual-spline)
                (if (sd-num-equal-p 0 REL_EXTREM_POS)
                  (progn
                    (BSPLINE_CON PROFILE_PNT_AT_BEND_ZONE
                                 new-ctrl-pnt2
                                 new-ctrl-pnt3a
                                 new-ctrl-pnt3b)
                    (BSPLINE_CON new-ctrl-pnt3b
                                 new-ctrl-pnt3c
                                 new-ctrl-pnt4
                                 OTHER_PROFILE_PNT_AT_BEND_ZONE)
                  )
                  (BSPLINE_CON PROFILE_PNT_AT_BEND_ZONE
                               new-ctrl-pnt2
                               new-ctrl-pnt3
                               new-ctrl-pnt4
                               OTHER_PROFILE_PNT_AT_BEND_ZONE)
                )
             )
  
             (apply 'POLYGON 
                      `(,PROFILE_PNT_AT_BEND_ZONE 
                        ,@(when (< (+ START_OFFSET RESOLUTION) 0)
                            `(,orig-profile-pnt-at-bend-zone))
                        ,@(unless X-case `(,OUTER_CORNER_POINT))
                        ,@(when (< (+ OTHER_START_OFFSET RESOLUTION) 0)
                             `(,orig-other-profile-pnt-at-bend-zone))
                        ,OTHER_PROFILE_PNT_AT_BEND_ZONE
                       )
             )
  
           ;for test purpose with direction vectors
             (GEOMETRY_MODE :CONSTRUCTION)
             (POLYGON (sd-vec-add PROFILE_PNT_AT_BEND_ZONE 
                                  PROFILE_DIR_AT_BEND_ZONE)
              PROFILE_PNT_AT_BEND_ZONE
              OUTER_CORNER_POINT
              OTHER_PROFILE_PNT_AT_BEND_ZONE
              (sd-vec-add OTHER_PROFILE_PNT_AT_BEND_ZONE
                          OTHER_PROFILE_DIR_AT_BEND_ZONE))
          
             (POLYGON PROFILE_PNT_AT_BEND_ZONE
                    new-ctrl-pnt2
                    new-ctrl-pnt3
                    new-ctrl-pnt4
                    OTHER_PROFILE_PNT_AT_BEND_ZONE)
             (GEOMETRY_MODE :REAL)
  
           )
        )
      )
    ); if AUTOMATIC_ADAPTION

    ;******** create Adjustpoints ******
    (C_POINT
      0,0
      INNER_CORNER_POINT
      OUTER_CORNER_POINT
      PROFILE_PNT_AT_BEND_ZONE
      OTHER_PROFILE_PNT_AT_BEND_ZONE
    )
    (setq result (sha-profile-of-wp tool-wp))
    (sha-delete-or-show-corner-wp)
    result
  )
)

;============================================================================
;
;    sha-join-lips-profile used as preparation for relief-less bending
;    by cutting superflous part of bend-zone overlap 
;    (with variable angle)
;            ( actual angle passed via parameter :CRN_ANGLE)
;
;                                                                              
;           \    Sheet metal basic face                                        
;            \                                                                 
;   other     \   P2=inner-corner-point                                        
;   bend       \  /                                                            
;               \_____________________                      _              
;               /                      \                     |                  
;              /                        } bend_radius        |                  
;  \  P1    __/   ____________________ /                     |                  
;   \ |  __/g/|\                                             } bend_flat_length 
;lip2\__/  a/ | join pnt (opt. blended with BLEND_RADIUS)    |
;  __/\   i/  |                                              |                  
; /    \ d/a/2|               new bend                       |                  
;       \/____|_________________________              _______|                 
;       /     |\                                                                
;      /  \   | \____profile_pnt_at_bend_zone                                   
;     /    \  |                                                                
;   |_      outer corner-point                                          
;             |                                                       
;             |__________________________ lip 1                       
;     
;             |                                                       
;             | - profile dir at bend zone        
;             v                                                       
;
; WP +Y direction                                                          
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;=============================================================================

(defun sha-join-lips-profile
  (&key ; following parameters are automatically derived from context
        RESOLUTION SIDE
        CORNER_ANGLE BEND_RADIUS BEND_FLAT_LENGTH BEND_LINE_DIST
        OTHER_BEND_RADIUS OTHER_BEND_FLAT_LENGTH
        THICKNESS PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
        PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT (BLEND_RADIUS 0)
   &allow-other-keys)
  (progn 
     ; if other profile point at bend zone not found, use symmetric point
     (unless other_profile_pnt_at_bend_zone
       (setf other_profile_pnt_at_bend_zone 
             (make-gpnt2d :x (* -1 (gpnt2d_x profile_pnt_at_bend_zone))
                          :y (gpnt2d_y profile_pnt_at_bend_zone))))
        
     (let*
       ((result nil)
        (tool-wp   (sha-tool-wp))
        (join-pnt (sha-line-line-intersection 
                 PROFILE_PNT_AT_BEND_ZONE PROFILE_DIR_AT_BEND_ZONE 
                 OTHER_PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE))
        (vec-join-pp (sd-vec-subtract PROFILE_PNT_AT_BEND_ZONE join-pnt))
        (vec-length-join-pp (sd-vec-length vec-join-pp))
        (vec-join-opp (sd-vec-subtract OTHER_PROFILE_PNT_AT_BEND_ZONE join-pnt))
        (vec-length-join-opp (sd-vec-length vec-join-opp))
        (join-dir (sd-vec-normalize (sd-vec-subtract PROFILE_PNT_AT_BEND_ZONE
                                                     join-pnt)))
        (other-join-dir (sd-vec-normalize 
                          (sd-vec-subtract OTHER_PROFILE_PNT_AT_BEND_ZONE
                                           join-pnt)))
        (axdir (sd-vec-normalize (sd-vec-subtract PROFILE_PNT_AT_BEND_ZONE
                                                     OUTER_CORNER_POINT)))
        (oaxdir (sd-vec-normalize 
                     (sd-vec-subtract OTHER_PROFILE_PNT_AT_BEND_ZONE
                                      OUTER_CORNER_POINT)))
        (join-pnt-opb (sha-line-line-intersection 
                 PROFILE_PNT_AT_BEND_ZONE axdir 
                 OTHER_PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE))
        (join-pnt-ppob (sha-line-line-intersection 
                 OTHER_PROFILE_PNT_AT_BEND_ZONE oaxdir 
                 PROFILE_PNT_AT_BEND_ZONE PROFILE_DIR_AT_BEND_ZONE))
        (vec-join-join-opb (if join-pnt-opb
                             (sd-vec-subtract join-pnt-opb join-pnt)))
        (vec-length-join-join-opb  (if join-pnt-opb
                                     (sd-vec-length vec-join-join-opb)))
        (vec-dir-join-join-opb (if join-pnt-opb
                                 (sd-vec-normalize vec-join-join-opb)))
        (vec-join-join-ppob (if join-pnt-ppob
                              (sd-vec-subtract join-pnt-ppob join-pnt)))
        (vec-length-join-join-ppob (if join-pnt-ppob
                                     (sd-vec-length vec-join-join-ppob)))
        (vec-dir-join-join-ppob (if join-pnt-ppob
                                  (sd-vec-normalize vec-join-join-ppob)))
        (profile-angle (if (equal side :LEFT)
                             (sd-vec-angle-between 
                                profile_dir_at_bend_zone
                                other_profile_dir_at_bend_zone)
                             (sd-vec-angle-between 
                                other_profile_dir_at_bend_zone
                                profile_dir_at_bend_zone)))
        (do-blend (and (NOT (sd-num-equal-p 0 BLEND_RADIUS))
                       (NOT (sd-num-equal-p 0 (abs (mod (+ resolution 
                                                         profile-angle) PI))))))
        (half-tan (if do-blend (tan (* 0.5 profile-angle))))
        (half-sin (if do-blend (sin (* 0.5 profile-angle))))
        (symm-dir (sd-vec-normalize (sd-vec-add profile_dir_at_bend_zone
                                               other_profile_dir_at_bend_zone)))
        (blend-center (if do-blend 
                           (sd-vec-add join-pnt 
                            (sd-vec-scale symm-dir (/ BLEND_RADIUS half-sin)))))
        (tang-pnt1    (if do-blend
                          (sd-vec-add join-pnt 
                             (sd-vec-scale profile_dir_at_bend_zone
                                            (/ BLEND_RADIUS half-tan)))))
        (tang-pnt2    (if do-blend
                           (sd-vec-add join-pnt 
                              (sd-vec-scale other_profile_dir_at_bend_zone
                                            (/ BLEND_RADIUS half-tan)))))
       )

        ; check for oversized blend and limit to bend zone area
        (if (and do-blend (> (/ BLEND_RADIUS half-tan) (sd-vec-length
                     (sd-vec-subtract join-pnt PROFILE_PNT_AT_BEND_ZONE))))
            (setf tang-pnt1 PROFILE_PNT_AT_BEND_ZONE))
        (if (and do-blend (> (/ BLEND_RADIUS half-tan) (sd-vec-length
                   (sd-vec-subtract join-pnt OTHER_PROFILE_PNT_AT_BEND_ZONE))))
            (setf tang-pnt2 OTHER_PROFILE_PNT_AT_BEND_ZONE))
        (create_workplane :new
                          :name tool-wp
                          :world_origin
        )

        ;********* create Profile *******

        (if do-blend 
          (progn
            (APPLY 'POLYGON 
                   (delete-duplicates (list tang-pnt1
                                            PROFILE_PNT_AT_BEND_ZONE 
                                            OUTER_CORNER_POINT
                                            OTHER_PROFILE_PNT_AT_BEND_ZONE
                                            tang-pnt2)
                                      :test 'oli::sd-vec-equal-p))
            (if (equal side :LEFT)
              (ARC :CENTER blend-center tang-pnt2 tang-pnt1)
              (ARC :CENTER blend-center tang-pnt1 tang-pnt2)
            )
          ) 
          (if (and (sd-vec-null-p 
                     (sd-vec-subtract join-dir PROFILE_DIR_AT_BEND_ZONE))
                   (sd-vec-null-p 
                     (sd-vec-subtract other-join-dir 
                       OTHER_PROFILE_DIR_AT_BEND_ZONE)))
            ; regular case join point inside sheet
            ; but check for join-opb between join and profile_pnt
            (if (and join-pnt-ppob
                     (sd-vec-null-p
                       (sd-vec-subtract vec-dir-join-join-ppob 
                                        PROFILE_DIR_AT_BEND_ZONE))
                     (< vec-length-join-join-ppob vec-length-join-pp))
              (POLYGON join-pnt join-pnt-ppob
                       OTHER_PROFILE_PNT_AT_BEND_ZONE join-pnt)
              ;check for join-ppob between join and other_profile_pnt
              (if (and join-pnt-opb
                       (sd-vec-null-p
                         (sd-vec-subtract vec-dir-join-join-opb 
                                        OTHER_PROFILE_DIR_AT_BEND_ZONE))
                     (< vec-length-join-join-opb vec-length-join-opp))
                (POLYGON join-pnt join-pnt-opb PROFILE_PNT_AT_BEND_ZONE 
                         join-pnt)
                ; really regular case
                (POLYGON PROFILE_PNT_AT_BEND_ZONE join-pnt
                         OTHER_PROFILE_PNT_AT_BEND_ZONE
                         OUTER_CORNER_POINT
                         PROFILE_PNT_AT_BEND_ZONE)
              )
            )
            ; irregular triangle cases
            (if (and join-pnt-opb 
                 (NOT (sd-vec-null-p
                    (sd-vec-subtract join-dir PROFILE_DIR_AT_BEND_ZONE))))
              (POLYGON join-pnt-opb OTHER_PROFILE_PNT_AT_BEND_ZONE
                      OUTER_CORNER_POINT join-pnt-opb)
              (if (and join-pnt-ppob 
                   (NOT (sd-vec-null-p
                    (sd-vec-subtract join-dir OTHER_PROFILE_DIR_AT_BEND_ZONE))))
               (POLYGON join-pnt-ppob PROFILE_PNT_AT_BEND_ZONE
                        OUTER_CORNER_POINT join-pnt-ppob)
              )
            )
          )
        )
        ;******** create Adjustpoints ******
        (C_POINT
         0,0
        )
        (setq result (sha-profile-of-wp tool-wp))
        (sha-delete-or-show-corner-wp)
        result
    )
  )
)

;============================================================================
;
;    blended V profile used as preparation for relief-less bending
;    by cutting superflous part of bend-zone overlap 
;    angle and radius are given as fix tool parameter (punch tool)
;
;                                                                              
;           \    Sheet metal basic face                                        
;            \                                                                 
;   other     \   P2=inner-corner-point                                        
;   bend       \  /                                                            
;               \_____________________                 _                       
;               /                      \                |                       
;              /                        } bend_radius   |                       
;  \  P1    __/   ____________________ /                |                  
;   \ |   _/g/\                                         } bend_flat_length      
;lip2\   / a/ |              new bend                   |       
;  __/\_/ i/  |                                         |                      
; /      d/a/2|                                         |                      
;       \/_   |_________________________         _______|                      
;       /       |\                                                                
;      /  \     | \____profile_pnt_at_bend_zone                                   
;     /    \    |                                                                
;   |_      outer corner-point                                          
;               |                                                       
;               |__________________________ lip 1                       
;     
; WP +Y direction                                                          
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;=============================================================================

(defun sha-blended-V-profile
  (&key V_ANGLE V_LENGTH BLEND_RAD REL_BLEND_POS MIN_ANGLE ; not yet used
        ; following parameters are automatically derived from context
        RESOLUTION SIDE CORNER_ANGLE BEND_RADIUS BEND_FLAT_LENGTH BEND_LINE_DIST
        OTHER_BEND_RADIUS OTHER_BEND_FLAT_LENGTH
        THICKNESS PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
        PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT
   &allow-other-keys)
   (let* ((join-profile (sha-join-lips-profile
          :RESOLUTION RESOLUTION
          :SIDE SIDE
          :CORNER_ANGLE CORNER_ANGLE
          :BEND_RADIUS BEND_RADIUS
          :BEND_FLAT_LENGTH BEND_FLAT_LENGTH
          :BEND_LINE_DIST BEND_LINE_DIST
          :OTHER_BEND_RADIUS OTHER_BEND_RADIUS
          :OTHER_BEND_FLAT_LENGTH OTHER_BEND_FLAT_LENGTH
          :THICKNESS  THICKNESS
          :PROFILE_PNT_AT_BEND_ZONE   PROFILE_PNT_AT_BEND_ZONE
          :OTHER_PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
          :PROFILE_DIR_AT_BEND_ZONE  PROFILE_DIR_AT_BEND_ZONE
          :OTHER_PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
          :INNER_CORNER_POINT  INNER_CORNER_POINT
          :OUTER_CORNER_POINT  OUTER_CORNER_POINT
          :INNER_CORNER_PROJECTION_POINT  INNER_CORNER_PROJECTION_POINT
          :OTHER_INNER_CORNER_PROJECTION_POINT
            OTHER_INNER_CORNER_PROJECTION_POINT
          :BLEND_RADIUS 0))
           (result nil)
           (tool-wp    (sha-tool-wp))
           (vec-io     (sd-vec-subtract OUTER_CORNER_POINT INNER_CORNER_POINT))
           (center-pos (sd-vec-add INNER_CORNER_POINT
                         (sd-vec-scale vec-io REL_BLEND_POS)))
           (half-sin (sin (* V_ANGLE 0.5)))
           (half-cos (cos (* V_ANGLE 0.5)))
           (half-tan (tan (* V_ANGLE 0.5)))
           (tang-x-dist (* BLEND_RAD half-cos))
           (tang-y-dist (* BLEND_RAD half-sin))
           (tang1 (sd-vec-add center-pos (make-gpnt2d :x (* -1 tang-x-dist)
                                                      :y (* -1 tang-y-dist))))
           (tang2 (sd-vec-add center-pos (make-gpnt2d :x tang-x-dist
                                                      :y (* -1 tang-y-dist))))
           (corn-y-dist (- V_LENGTH BLEND_RAD))
           (corn-x-dist (+ (* corn-y-dist half-tan) (/ BLEND_RAD half-cos)))
           (corn1 (make-gpnt2d :x (* -1 corn-x-dist)
                             :y corn-y-dist))
           (corn2 (make-gpnt2d :x corn-x-dist
                             :y corn-y-dist))
          )

         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******

           (POLYGON tang1 corn1 corn2 tang2)
           (ARC :CENTER center-pos tang1 tang2)

         ;******** create Adjustpoints ******
         (C_POINT
          0,0
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-corner-wp)
         (list join-profile result)
  )
)

;==============================================================================
;
;   MINIMAL ROUND RELIEF = minimal circle cutting complete intersection of bend
;   zones
;   (angle independent)
;
;                                                                              
;             \    Sheet metal basic face                                       
;              \                                                                
;     other     \    inner-corner-point                                       
;     bend       \  /                                                           
;            _____\_____________________  _                       
;         __/     /\_                      |                       
;        /       /   \                     |                       
;    \  |      g/     |                    |                  
;     \ |     a/__0,0 |                    } bend_flat_length      
;  lip2\|    i/       |                    |       
;       \   d/       /                     |                      
;        \  /      _/                      |                      
;       / \/______/______________________ _|                      
;      /  /     |                                                               
;     /  /  \   |                                                               
;    /  /    outer-corner-point                                              
;   / |_        |                                                               
;      +Y dir   |                                                       
;               |__________________________                             
;       
;                                                                            
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;=============================================================================

(defun sha-minimal-round-profile
  (&key ; following parameter automatically derived from context
        PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
        PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_PROJECTION_POINT OTHER_INNER_CORNER_PROJECTION_POINT
        INNER_CORNER_POINT OUTER_CORNER_POINT
   &allow-other-keys)

   (progn 
      ; if other profile point at bend zone not found, use symmetric point
      (unless other_profile_pnt_at_bend_zone
        (setf other_profile_pnt_at_bend_zone 
                (make-gpnt2d :x (* -1 (gpnt2d_x profile_pnt_at_bend_zone))
                             :y (gpnt2d_y profile_pnt_at_bend_zone))))
        
    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
          )

         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******

         (CIRCLE  :CEN_DIA 0,0 INNER_CORNER_POINT)

         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          INNER_CORNER_POINT
          OUTER_CORNER_POINT
          PROFILE_PNT_AT_BEND_ZONE
          OTHER_PROFILE_PNT_AT_BEND_ZONE
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-corner-wp)
         result
    )
  )
)

;==============================================================================
;
;   ROUND CORNER RELIEF = combined tools : round punch in corner prepared by 
;   lip join punch
;
;                                                                              
;             \    Sheet metal basic face                                       
;              \                                                                
;     other     \    inner-corner-point                                       
;     bend       \  / relief cut (internal representation not on flat drawing)  
;                 \_____________________  _                       
;             ____/\                       |                       
;            /   /\  relief cut (internal representation not on flat drawing)
;    \      /  g   \                       |                  
;     \    |  a/    |                      } bend_flat_length      
;  lip2\   / i/    /                       |       
;       \ / d  _ _/                        |                      
;        /  /   |                          |                      
;       /       |________________________ _|                      
;      /  /\    |                                                               
;     /     \   |                                                               
;    /  /    outer-corner-point                                              
;   /  /        |                                                               
;     v +Y dir  |                                                       
;               |__________________________                             
;       
;                                                                            
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;=============================================================================

(defun sha-rnd-corner-profile
  
  (&key DIA REL_RND_CENT_POS
        ; following parameter are derived automatically from context
        RESOLUTION SIDE CORNER_ANGLE BEND_RADIUS BEND_FLAT_LENGTH BEND_LINE_DIST
        OTHER_BEND_RADIUS OTHER_BEND_FLAT_LENGTH
        THICKNESS PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
        PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT 
   &allow-other-keys)

  (let* ((join-profile (sha-join-lips-profile
          :RESOLUTION RESOLUTION
          :SIDE SIDE 
          :CORNER_ANGLE CORNER_ANGLE 
          :BEND_RADIUS BEND_RADIUS 
          :BEND_FLAT_LENGTH BEND_FLAT_LENGTH 
          :BEND_LINE_DIST BEND_LINE_DIST
          :OTHER_BEND_RADIUS OTHER_BEND_RADIUS
          :OTHER_BEND_FLAT_LENGTH OTHER_BEND_FLAT_LENGTH
          :THICKNESS  THICKNESS 
          :PROFILE_PNT_AT_BEND_ZONE   PROFILE_PNT_AT_BEND_ZONE  
          :OTHER_PROFILE_PNT_AT_BEND_ZONE OTHER_PROFILE_PNT_AT_BEND_ZONE
          :PROFILE_DIR_AT_BEND_ZONE  PROFILE_DIR_AT_BEND_ZONE 
          :OTHER_PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
          :INNER_CORNER_POINT  INNER_CORNER_POINT 
          :OUTER_CORNER_POINT  OUTER_CORNER_POINT 
          :INNER_CORNER_PROJECTION_POINT  INNER_CORNER_PROJECTION_POINT 
          :OTHER_INNER_CORNER_PROJECTION_POINT 
            OTHER_INNER_CORNER_PROJECTION_POINT 
          :BLEND_RADIUS 0))
        (result nil)
        (tool-wp   (sha-tool-wp))
        (vec-io (sd-vec-subtract OUTER_CORNER_POINT INNER_CORNER_POINT))
        (center-pos (sd-vec-add INNER_CORNER_POINT
                     (sd-vec-scale vec-io REL_RND_CENT_POS)))
    
       )

         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******

         (CIRCLE  :CEN_DIA center-pos DIA)

         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          INNER_CORNER_POINT
          OUTER_CORNER_POINT
          PROFILE_PNT_AT_BEND_ZONE
          OTHER_PROFILE_PNT_AT_BEND_ZONE
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-corner-wp)
         (list join-profile result)
  )
)

;=================================================================
;
;   PERP-V-RELIEF perpendicular (to bend-zone) corner relief 
;   with variable angle)
;            ( actual angle passed via parameter :CRN_ANGLE)
;
;                                                                              
;           \           Sheet metal basic face                                  
;            \    _/|                                                          
;   other     \ _/ inner-corner-point                                        
;   bend      _/  / |                                                          
;           _/  \_____________________                 _                       
;         _/  _//|  |                  \                |                       
;       _/  _/ / |<>| relief_offset     } bend_radius   |                       
;  \  P1  _/ g/  |__|_________________ /                |                  
;  /\ | _/  a/   |  |                                   } bend_flat_length      
;lip2\_/   i/    |  |        new bend                   |       
;  \  \   d/     | P3= inner-corner-projection-point    |                      
;   \  \  /a/2   | /|                                   |                      
;    \  \/_______|__|___________________         _______|                      
;     \ /\    |\  s |                                                           
;      /_ \___|_\___|                                   
;     /    \  |  \__profile_pnt_at_bend_zone                                   
;   |_      \_outer corner-point                                          
;             |                                                       
;             |__________________________                             
;     
; WP +Y direction                                                          
;  final drawing in coordinate system with "diag" as y-axis and 0,0 at diag/2  
;============================================================================n

(defun sha-perp-v-shape-profile
  (&key RELIEF_OFFSET ;MIN_WIDTH MIN_LENGTH MIN_ANGLE ; not yet used
        (min_gap 0.01) 
        ; following parameter automatically derived from context
        corner_angle bend_radius bend_flat_length bend_line_dist
        other_bend_radius other_bend_flat_length
        thickness profile_pnt_at_bend_zone other_profile_pnt_at_bend_zone
                  PROFILE_DIR_AT_BEND_ZONE OTHER_PROFILE_DIR_AT_BEND_ZONE
        INNER_CORNER_POINT OUTER_CORNER_POINT INNER_CORNER_PROJECTION_POINT 
        OTHER_INNER_CORNER_PROJECTION_POINT 
        ; parameter DIA is for usage of this function with old style 
        ; rnd_corner_relief, which is a square in 3D with 2D replacement
        DIA
   &allow-other-keys)
   (progn 
      ; set defaults for unspecified usage
      (unless corner_angle (setf corner_angle (sd-deg-to-rad 90)))
      (unless bend_radius (setf bend_radius (MBU-GET-DEFAULT-BEND-RADIUS)))
      (unless other_bend_radius (setf other_bend_radius bend_radius))
      (unless bend_flat_length
        (setf bend_flat_length (* (+ bend_radius (* thickness 0.4) ) PI 0.5)))
      (unless other_bend_flat_length 
        (setf other_bend_flat_length (* (+ other_bend_radius (* thickness 0.4))
                                        PI 0.5)))
      ; DIA in following expression is checked for usage of this function with
      ; old style rnd_corner_relief, which is a square in 3D with 2D replacement
      (unless relief_offset (setf relief_offset (/ dia 2)))

    (let* ((result nil)
           (tool-wp   (sha-tool-wp))
           (vec_IPI (sd-vec-normalize (sd-vec-subtract 
                                         INNER_CORNER_POINT
                                         INNER_CORNER_PROJECTION_POINT)))
           (vec_OPI (sd-vec-normalize (sd-vec-subtract 
                                         INNER_CORNER_POINT
                                         OTHER_INNER_CORNER_PROJECTION_POINT)))
           (vec_OIP (sd-vec-normalize (sd-vec-subtract 
                                         INNER_CORNER_PROJECTION_POINT
                                         OUTER_CORNER_POINT)))
           (vec_IIP (sd-vec-normalize (sd-vec-subtract 
                                         INNER_CORNER_PROJECTION_POINT
                                         INNER_CORNER_POINT)))
           (vec_IPO (sd-vec-normalize (sd-vec-subtract 
                                         OUTER_CORNER_POINT
                                         INNER_CORNER_PROJECTION_POINT)))
           (vec_OPO (sd-vec-normalize (sd-vec-subtract 
                                         OUTER_CORNER_POINT
                                         OTHER_INNER_CORNER_PROJECTION_POINT)))
           (vec_OOP (sd-vec-normalize (sd-vec-subtract 
                                         OTHER_INNER_CORNER_PROJECTION_POINT
                                         OUTER_CORNER_POINT)))
           (vec_IOP (sd-vec-normalize (sd-vec-subtract 
                                         OTHER_INNER_CORNER_PROJECTION_POINT
                                         INNER_CORNER_POINT)))
          )

         ; add dist to relief_offset contur
         (unless (equal relief_offset 0)
           (setf INNER_CORNER_POINT
             (sd-vec-add INNER_CORNER_POINT
                         (sd-vec-add (sd-vec-scale vec_OPI relief_offset)
                                     (sd-vec-scale vec_IPI relief_offset))))
           (setf INNER_CORNER_PROJECTION_POINT
             (sd-vec-add INNER_CORNER_PROJECTION_POINT
                         (sd-vec-add (sd-vec-scale vec_OIP relief_offset)
                                     (sd-vec-scale vec_IIP relief_offset))))

           (setf OTHER_INNER_CORNER_PROJECTION_POINT
             (sd-vec-add OTHER_INNER_CORNER_PROJECTION_POINT
                         (sd-vec-add (sd-vec-scale vec_OOP relief_offset)
                                     (sd-vec-scale vec_IOP relief_offset))))
           (setf OUTER_CORNER_POINT
             (sd-vec-add OUTER_CORNER_POINT
                         (sd-vec-add (sd-vec-scale vec_IPO relief_offset)
                                     (sd-vec-scale vec_OPO relief_offset))))
         )

         (create_workplane :new
                           :name tool-wp
                           :world_origin
         )

         ;********* create Profile *******

         ; check for self intersecting cut profile
         (if (> 0 (* (gpnt2d_x INNER_CORNER_PROJECTION_POINT)
                     (gpnt2d_x OTHER_INNER_CORNER_PROJECTION_POINT)))
           ; regular case -> take given points
           (POLYGON INNER_CORNER_POINT INNER_CORNER_PROJECTION_POINT
                    OUTER_CORNER_POINT OTHER_INNER_CORNER_PROJECTION_POINT
                    INNER_CORNER_POINT)
           ; self intersecting (very asymmetric case) -> create a triangle only
           (let ((RELEVANT-PROJECTION-POINT 
                   (if (< (gpnt2d_y INNER_CORNER_PROJECTION_POINT )
                          (gpnt2d_y OTHER_INNER_CORNER_PROJECTION_POINT))
                     INNER_CORNER_PROJECTION_POINT
                     OTHER_INNER_CORNER_PROJECTION_POINT))
                 (IRELEVANT-PROJECTION-POINT 
                   (if (< (gpnt2d_y INNER_CORNER_PROJECTION_POINT )
                          (gpnt2d_y OTHER_INNER_CORNER_PROJECTION_POINT))
                     OTHER_INNER_CORNER_PROJECTION_POINT
                     INNER_CORNER_PROJECTION_POINT))
                )
             (POLYGON INNER_CORNER_POINT 
                    (sha-line-line-intersection
                      INNER_CORNER_POINT
                      (sd-vec-normalize
                        (sd-vec-subtract IRELEVANT-PROJECTION-POINT
                                         INNER_CORNER_POINT))
                      RELEVANT-PROJECTION-POINT       
                      (sd-vec-normalize
                        (sd-vec-subtract OUTER_CORNER_POINT   
                                         RELEVANT-PROJECTION-POINT)))
                    RELEVANT-PROJECTION-POINT       
                    INNER_CORNER_POINT)
           )
         )

         ;******** create Adjustpoints ******
         (C_POINT
          0,0
          INNER_CORNER_POINT
          OUTER_CORNER_POINT
          INNER_CORNER_PROJECTION_POINT
          OTHER_INNER_CORNER_PROJECTION_POINT
         )
         (setq result (sha-profile-of-wp tool-wp))
         (sha-delete-or-show-corner-wp)
         result
    )
  )
)

; Take round profile if it completely cuts both bend zones
; otherwise take fallback with replacement in 2D
(defun sha-rnd-or-perp-v-shape-profile (&rest plist)
  (let ((ipnt (getf plist :INNER_CORNER_POINT))
        (opnt (getf plist :OUTER_CORNER_POINT)) 
        (dia (getf plist :DIA)) 
       )
    (if (and ipnt opnt dia (< (sd-vec-length (sd-vec-subtract opnt ipnt)) dia))
      (apply 'sha-rnd-corner-profile (nconc plist (list :REL_RND_CENT_POS 0.5)))
      (apply 'sha-perp-v-shape-profile plist)
    )
  )
)

