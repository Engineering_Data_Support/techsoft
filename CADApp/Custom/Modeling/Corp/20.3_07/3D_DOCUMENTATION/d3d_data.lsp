;; Automatically written on 05-Mar-2021 13:04:02
;; Module 3D DOCUMENTATION
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "3D DOCUMENTATION")
(persistent-data
 :key "TOL-TYPE"
 :value :TOL_PLUS_MINUS)
(persistent-data
 :key "DIMENSION"
 :value '( :DIMENSION-VAL-DECIMAL-PLACES 0 :DIMENSION-SIZE 200 :GAP-DL-TO-BOX 2 
           :SPC-DL-TO-BOX 50 :ARROW-FILL T :ARROW-SIZE 200 :LEN-EXTENSION 50 ) )
(persistent-data
 :key "TEMPLATE EXAMPLES SHOWN"
 :value '( :SURFACE T ) )
(persistent-data
 :key "CIRCULAR-DIM-TYPE-RADIUS"
 :value NIL)

;; ----------- end of file -----------
