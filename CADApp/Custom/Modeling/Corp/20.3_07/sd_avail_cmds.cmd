﻿; ------------------------------------------------------------------------------
; SVN: $Id: sd_avail_cmds.cmd 38125 2014-07-03 10:03:33Z pjahn $
; commands for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(:Application "All")

;;----- All: Techosft ----------------------------------------------------------

(:Group "TECHSOFT" :title "TECHSOFT")

("Sheet Metal"
 :title       "Sheet Metal"
 :action      (LISP::PROGN
                (LISP::IF (OLI::SD-MODULE-ACTIVE-P "SHEETADVISOR")
                    (LISP::PROGN
                      (OLI::SD-SWITCH-APPLICATION "SheetAdvisor"))
                    (LISP::PROGN (STARTUP::ACTIVATE-SHEETADVISOR))))
 :description "Start/Switch to Sheet Metal"
 :image       "All/TECHSOFT/SheetMetal"
 :ui-behavior :DEFAULT)
 
("Model Manager"
 :title       "Model Manager"
 :action      (LISP::PROGN
				(LISP:IF (startup::license-free-module-active-p "ModelManager")
					(LISP::PROGN
						(MODELMANAGER::MM-SEND-CMD "SHOW-MANAGER"))
					  (LISP::PROGN (act_deact_module :act "ModelManager" "MODULE-CONTROLLER-Modules-ModelManager-TB" '(STARTUP::ACTIVATE-MODELMANAGER)))))
 :description "Open the Model Manager Workspace"
 :image       "All/TECHSOFT/ModelManager"
 :sd-access   :yes
 :modeling-pe :no
 ) 
 
 
;;----- SolidDesigner: Construction --------------------------------------------
 
(:Application "SolidDesigner")

(:Group "Construction" :title "Construction")

("Querschnitt"
 :title       "Querschnitt als Hilfgeometrie projizieren"
 :action      "geometry_mode :construction cross_section :cross_section_wp"
 :description "Teile-Querschnitt als Hilfsgeometrie auf eine Arbeitsebene projizieren."
 :image       "SolidDesigner/Construction/Querschnitt"
 :ui-behavior :DEFAULT)
 
