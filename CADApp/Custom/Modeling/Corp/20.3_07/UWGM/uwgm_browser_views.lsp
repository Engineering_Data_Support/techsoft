﻿;; LOLAMAX 18
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Description:  Windchill integration default "Browser Views" file
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This file contains definitions of browser search and filters that can be activated
;; via the structure browser "Views" menu button when Windchill integration is activated.

;;Example of a displaying a model property

;;(defun display-windchill-custom-property (node)
;;  (let ((sel_item (oli:sd-pathname-to-obj (oli:browsernode-objpath node))))
;;    (oli:sd-uwgm-inq-model-attribute sel_item "CUSTOM_PROPERTY")
;;  )
;;)
;;
;;(oli:sd-create-column-definition
;;  :uwgm-custom-property
;;  :title "Custom Property"
;;  :display-fnc 'display-windchill-custom-property
;;  :applicable-fnc nil
;;  :edit-fnc nil  ;; not editable
;;)
;;
;; Editing is partially supported through the use of an "edit-fnc" in the column definition
;; Be aware that any additional constraints imposed by windchill on legal attribute values
;; are not honoured and illegal values may cause checkin or upload failures.
;;
;;  :edit-fnc 'set-windchill-custom-property
;;
;;  (defun set-windchill-custom-property (node value)
;;    (let ((sel_item (oli:sd-pathname-to-obj (oli:browsernode-objpath node))))
;;      (oli:sd-uwgm-set-model-attribute sel_item "CUSTOM_PROPERTY" value)
;;    )
;;  )
;;

;; ---------------------------------------------------------------
;;  Utilities

(defun get-sel-item-from-browser-node (node)
  (oli:sd-pathname-to-obj (oli:browsernode-objpath node))
  )
  
(defun display-windchill-system-attribute(node name)
  (let ((sel_item (get-sel-item-from-browser-node node)))
    (oli:sd-uwgm-inq-system-attribute sel_item name)
    )
  )

(defun display-windchill-model-attribute(node name)
  (let* 
      (
       (sel_item (get-sel-item-from-browser-node node))
       (value (oli:sd-uwgm-inq-model-attribute sel_item name))
       )
    ;; need to format the value because it can be a typed object
    (if value (format nil "~A" value) nil)
   )
  )

;; ---------------------------------------------------------------
;; Define Default view

(defun display-windchill-system-version (node)
  (let ((sel_item (get-sel-item-from-browser-node node)))
    (when (oli:sd-uwgm-inq-system-attribute sel_item "PTC_WM_REVISION")
      (format nil "~A.~A" (oli:sd-uwgm-inq-system-attribute sel_item "PTC_WM_REVISION")
              (oli:sd-uwgm-inq-system-attribute sel_item "PTC_WM_ITERATION" )
              )
      )
    )
  )

(defun display-windchill-system-organization-id (node)
  (display-windchill-system-attribute node "PTC_WM_ORGANIZATION_ID")
  )

(defun display-windchill-system-lifecycle (node)
  (display-windchill-system-attribute node "PTC_WM_LIFECYCLE")
  )

(defun display-windchill-system-lifecycle-state (node)
  (display-windchill-system-attribute node "PTC_WM_LIFECYCLE_STATE")
  )

(defun display-windchill-system-created-by (node)
  (display-windchill-system-attribute node "PTC_WM_CREATED_BY")
  )

(defun display-windchill-system-created-on (node)
  (display-windchill-system-attribute node "PTC_WM_CREATED_ON")
  )

(defun display-windchill-system-modified-by (node)
  (display-windchill-system-attribute node "PTC_WM_MODIFIED_BY")
  )

(defun display-windchill-system-modified-on (node)
  (display-windchill-system-attribute node "PTC_WM_MODIFIED_ON")
  )

(defun display-windchill-system-part-number (node)
  (display-windchill-system-attribute node "PTC_WM_PART_NUMBER")
  )

(defun display-windchill-system-part-name (node)
  (display-windchill-system-attribute node "PTC_WM_PART_NAME")
  )

(defun display-windchill-system-last-change-note (node)
  (display-windchill-system-attribute node "PTC_WM_LAST_CHANGE_NOTE")
  )

(defun display-windchill-modified (node)
  (if (oli:sd-uwgm-inq-modified (get-sel-item-from-browser-node node))
      "Geändert"
    ""
    )
  )

(defun display-windchill-status (node)
  (oli:sd-uwgm-inq-status (get-sel-item-from-browser-node node))
  )

(defun display-windchill-cadname (node)
  (oli:sd-uwgm-inq-cadname (get-sel-item-from-browser-node node))
  )

(defun set-windchill-cadname (node value)
  (oli:sd-uwgm-set-cadname (get-sel-item-from-browser-node node) value)
  )

(defun display-windchill-number (node)
  (oli:sd-uwgm-inq-number (get-sel-item-from-browser-node node))
  )

(defun set-windchill-number (node value)
  (oli:sd-uwgm-set-number (get-sel-item-from-browser-node node) value)
  )

(defun display-windchill-commonname (node)
  (oli:sd-uwgm-inq-commonname (get-sel-item-from-browser-node node))
  )

(defun set-windchill-commonname (node value)
  (oli:sd-uwgm-set-commonname (get-sel-item-from-browser-node node) value)
  )

;; ---------------------------------------------------------------
;; Create columns definitions

(oli:sd-create-column-definition
 :uwgm-status
 :title "Status"
 :display-fnc 'display-windchill-status
 :applicable-fnc nil
 :edit-fnc nil ;; not editable
 )

(oli:sd-create-column-definition
  :uwgm-organization-id
  :title "Organisations-ID"
  :display-fnc 'display-windchill-system-organization-id
  :applicable-fnc nil
  :edit-fnc nil ;; not editable
)

(oli:sd-create-column-definition
 :uwgm-lifecycle
 :title "Lebenszyklus"
 :display-fnc 'display-windchill-system-lifecycle
 :applicable-fnc nil
 :edit-fnc nil ;; not editable
 )

(oli:sd-create-column-definition
 :uwgm-lifecycle-state
 :title "Lebenszykluszustand"
 :display-fnc 'display-windchill-system-lifecycle-state
 :applicable-fnc nil
 :edit-fnc nil ;; not editable
 )

(oli:sd-create-column-definition
 :uwgm-version
 :title "Version"
 :display-fnc 'display-windchill-system-version
 :applicable-fnc nil
 :edit-fnc nil ;; not editable
 )

(oli:sd-create-column-definition
 :uwgm-modified
 :title "Geändert"
 :display-fnc 'display-windchill-modified
 :applicable-fnc nil
 :edit-fnc nil ;; not editable
 )

(oli:sd-create-column-definition
 :uwgm-cadname
 :title "Dateiname"
 :display-fnc 'display-windchill-cadname
 :applicable-fnc nil
 :edit-fnc 'set-windchill-cadname
 )

(oli:sd-create-column-definition
 :uwgm-number
 :title "Anzahl"
 :display-fnc 'display-windchill-number
 :applicable-fnc nil
 :edit-fnc 'set-windchill-number
 )

(oli:sd-create-column-definition
 :uwgm-commonname
 :title "Name"
 :display-fnc 'display-windchill-commonname
 :applicable-fnc nil
 :edit-fnc 'set-windchill-commonname
 )

(oli:sd-create-column-definition
 :uwgm-created-by
 :title "Erstellt von"
 :display-fnc 'display-windchill-system-created-by
 )

(oli:sd-create-column-definition
 :uwgm-created-on
 :title "Erstellungsdatum"
 :display-fnc 'display-windchill-system-created-on
 )

(oli:sd-create-column-definition
 :uwgm-modified-by
 :title "Geändert von"
 :display-fnc 'display-windchill-system-modified-by
 )

(oli:sd-create-column-definition
 :uwgm-modified-on
 :title "Änderungsdatum"
 :display-fnc 'display-windchill-system-modified-on
 )

(oli:sd-create-column-definition
 :uwgm-last-change-note
 :title "Hinweis zur letzten Änderung"
 :display-fnc 'display-windchill-system-last-change-note
 )

(oli:sd-create-column-definition
 :uwgm-part-number
 :title "Teilenummer"
 :display-fnc 'display-windchill-system-part-number
 )

(oli:sd-create-column-definition
 :uwgm-part-name
 :title "Teilename"
 :display-fnc 'display-windchill-system-part-name
 )

;; ---------------------------------------------------------------
;;                   create default view
;; ---------------------------------------------------------------

(oli:sd-create-browser-view
 "WINDCHILL"
 :tree-config '(:instance-name " (" :uwgm-cadname ")")
 :detail-config '(:uwgm-cadname :uwgm-number :uwgm-commonname :uwgm-status :uwgm-version :uwgm-lifecycle-state)
 :title "Windchill"
 :enable '(oli::sd-license-free-module-active-p "WINDCHILL_INTEGRATION")
 )

;; ---------------------------------------------------------------
;;         sample code for displaying all system attributes
;; ---------------------------------------------------------------
#|
(oli:sd-create-browser-view
 "WINDCHILL_ALL"
 :tree-config '(:instance-name " (" :uwgm-cadname ")")
 :detail-config '(:uwgm-cadname :uwgm-number :uwgm-commonname :uwgm-status :uwgm-version :uwgm-lifecycle-state
                                :uwgm-part-number :uwgm-part-name :uwgm-last-change-note :uwgm-modified-on
                                :uwgm-modified-by :uwgm-created-on :uwgm-created-by :uwgm-organization-id :uwgm-lifecycle
                                )
 :title "Windchill All"
 :enable '(oli::sd-license-free-module-active-p "WINDCHILL_INTEGRATION")
 )
|#

;; ---------------------------------------------------------------
;;          sample code for displaying typed model attributes
;; ---------------------------------------------------------------
#|
(defun display-windchill-string-attribute (node)
  (display-windchill-model-attribute node "StringAttribute")
  )
(defun display-windchill-boolean-attribute (node)
  (display-windchill-model-attribute node "BooleanAttribute")
  )
(defun display-windchill-integer-attribute (node)
  (display-windchill-model-attribute node "IntegerAttribute")
  )
(defun display-windchill-float-attribute (node)
  (display-windchill-model-attribute node "FloatAttribute")
  )
(defun display-windchill-date-attribute (node)
  (display-windchill-model-attribute node "DateAttribute")
  )

(oli:sd-create-column-definition
 :uwgm-string-attribute
 :title "String Attribute"
 :display-fnc 'display-windchill-string-attribute
 )
(oli:sd-create-column-definition
 :uwgm-boolean-attribute
 :title "Boolean Attribute"
 :display-fnc 'display-windchill-boolean-attribute
 )
(oli:sd-create-column-definition
 :uwgm-integer-attribute
 :title "Integer Attribute"
 :display-fnc 'display-windchill-integer-attribute
 )
(oli:sd-create-column-definition
 :uwgm-float-attribute
 :title "Float Attribute"
 :display-fnc 'display-windchill-float-attribute
 )
(oli:sd-create-column-definition
 :uwgm-date-attribute
 :title "Date Attribute"
 :display-fnc 'display-windchill-date-attribute
 )

|#

;; ---------------------------------------------------------------
;; Set default and activate ours
(oli:sd-set-default-browser-view "WINDCHILL_INTEGRATION" "WINDCHILL")
(oli:sd-set-current-browser-view  "WINDCHILL" :persistent t)
