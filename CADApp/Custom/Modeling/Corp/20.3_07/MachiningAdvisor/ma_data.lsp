;; Automatically written on 05-Mar-2021 13:04:01
;; Module CADCAM_LINK
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "CADCAM_LINK")
(persistent-data
 :key "AUTO-MODE"
 :value '( :AUTO-MODE ( :AXIS ) :SHOW-TOL-AND-QUALITY NIL :SHOW-IMAGE T 
           :CUST-CHECK-LEVEL NIL :FEAT-FEEDBACK-LEVEL :ADJUSTED 
           :LABEL-VISIBILITY :HIDE :COMPUTE-MAX-DRILL_DIA-FOR-TAPPED-HOLE NIL 
           :COMPUTE-MIN-DRILL_DIA-FOR-TAPPED-HOLE NIL :OPERATION-LEVEL ( 
           :ROBUSTNESS 1 :PERFORMANCE 2 ) :COPILOT-MODE-ON-PLANAR-FACE :UV 
           :STEPPED-HOLE-AUTO-MODE ( :USE_DIAMETER_TOLERANCE :USE_PARTIAL_DEPTH 
           :USE_PARTIAL_DEPTH_TOLERANCE :USE_BLIND_HOLE_DEPTH 
           :USE_BLIND_HOLE_DEPTH_TOLERANCE :USE_MAX_DEPTH_AS_THROUGH_HOLE_DEPTH 
           :USE_THROUGH_HOLE_DEPTH_TOLERANCE ) ) )

;; ----------- end of file -----------
