;% Automatically written on 12/20/2018 at 16:27:49
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/General/ArrowStyle"
  :style :|PFEIL_GROß50|
  :title "Pfeil Groß"
  :values
   '("Filled" LISP::T
     "Size" 7.0
     "Type" :ARROW_TYPE
     )
)
