{  @(#)  bom_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of BOM-Module
}

{ >>>if false }

{ LOLAMAX = 162 }

{ General messages }

LET Awm_bom_msg_1
	"Pointez CONFIRM pour supprimer tous les rep�res de la nomenclature active"
LET Awm_bom_msg_2
    "Pas de format charg� pour le rep�re ; s�lectionnez le format puis recommencez"
LET Awm_bom_msg_3
    "Identifiez le rep�re � supprimer"
LET Awm_bom_msg_4
    "Aucun rep�re identifi�"
LET Awm_bom_msg_5
    "Cet �l�ment ne contient pas d'information de rep�re"
LET Awm_bom_msg_6
    "Identifiez la pi�ce pour affectation du rep�re"
LET Awm_bom_msg_7
    "Cette pi�ce ne figure pas dans l'arbre de la nomenclature active"
LET Awm_bom_msg_8
    "Rep�re d�j� affect�"
LET Awm_bom_msg_9
    "Pas de donn�es article-WM affect�es � cet article"
LET Awm_bom_msg_10
    "Saisissez l'origine de la ligne d'attache du rep�re ou pointez CONTINUER pour passer � la pi�ce suivante"
LET Awm_bom_msg_11
    "Saisissez l'origine de la ligne d'attache horizontale du rep�re; pointez FIN si vous avez termin� ou CONTINUER pour passer � la pi�ce suivante"
LET Awm_bom_msg_12
    "Saisissez l'origine de la ligne d'attache verticale du rep�re; pointez FIN si vous avez termin� ou CONTINUER pour passer � la pi�ce suivante"
LET Awm_bom_msg_13
    "Indiquez un point sur la ligne d'attache du rep�re; pointez FIN si vous avez termin� ou CONTINUER pour passer � la pi�ce suivante"
LET Awm_bom_msg_14
    "S�lectionnez un composant de la nomenclature"
LET Awm_bom_msg_15
	"Entr�e incorrecte"
LET Awm_bom_msg_16
	"Cette pi�ce ne figure pas dans la nomenclature"
LET Awm_bom_msg_17
    "Toutes les pi�ces ont un rep�re"
LET Awm_bom_msg_18
    "Identifiez le rep�re dont la ligne d'attache doit �tre retrac�e"
LET Awm_bom_msg_19
	"S�lectionnez le composant de la nomenclature pour tracer le rep�re"
LET Awm_bom_msg_20
    "Fichier de macro incorrect"
LET Awm_bom_msg_21
    "Fichier d'�change introuvable"
LET Awm_bom_msg_22
	"Structure de pi�ces incorrecte : le format de l'annotation ne doit pas contenir de sous-ensembles"
LET Awm_bom_msg_23
    "Extr�mit� 'SUP GAUCHE' non d�finie"
LET Awm_bom_msg_24
    "Extr�mit� 'SUP DROITE' non d�finie"
LET Awm_bom_msg_25
    "Extr�mit� 'INF GAUCHE' non d�finie"
LET Awm_bom_msg_26
    "Extr�mit� 'INF DROITE' non d�finie"
LET Awm_bom_msg_27
	"Identifiez la pi�ce devant servir de titre � la nomenclature"
LET Awm_bom_msg_28
	"Identifiez la pi�ce devant servir de composant � la nomenclature"
LET Awm_bom_msg_29
	"Structure de pi�ce incorrecte : le format de la nomenclature doit avoir un en-t�te et un composant"
LET Awm_bom_msg_30
	"S�lectionnez une option ou identifiez l'extr�mit� de la ligne d'attache du rep�re"
LET Awm_bom_msg_31
	"Pointez GAUCHE ou DROIT ou saisissez la nouvelle extr�mit� de la ligne d'attache du rep�re"
LET Awm_bom_msg_32
	"Saisissez la nouvelle extr�mit� de la ligne d'attache du rep�re"
LET Awm_bom_msg_33
	"Pas de nomenclature affect�e dans WorkManager !"
LET Awm_bom_msg_34
    "Aucun format de nomenclature charg�e"
LET Awm_bom_msg_35
    "Identifiez la position dans la nomenclature"
LET Awm_bom_msg_36
	"Identifiez l'assemblage pour g�n�rer la nomenclature"
LET Awm_bom_msg_37
    "NOMENC-WM non disponible !"
LET Awm_bom_msg_38
	"Aucun assemblage identifi�"
LET Awm_bom_msg_39
	"Saisissez le nouveau rep�re"
LET Awm_bom_msg_40
	"Ce composant ne correspond pas � NOMENC-WM"
LET Awm_bom_msg_41
	"Identifiez l'assemblage pour la nomenclature active"
LET Awm_bom_msg_42
    "Aucune pi�ce ME10 correspondant au titre de la nomenclature n'a �t� trouv�e"
LET Awm_bom_msg_43
"Pointez CONFIRM pour mettre � jour la nomenclature dans WorkManager"
LET Awm_bom_msg_44
"Pointez CONFIRM pour mettre � jour NOMENC avec structure pi�ces ME10 active"
LET Awm_bom_msg_45 ""
LET Awm_bom_msg_46
"Pointez CONFIRM pour supprimer tous les rep�res dans la nomenclature ME10"
LET Awm_bom_msg_47
	"Identifiez assemblage pour ouverture NOMENC dans WorkManager ou pointez ACTIVE"
LET Awm_bom_msg_48
	"Pas de donn�es article-WM attach�es � cet article"
LET Awm_bom_msg_49
	"Saisissez premier rep�re :"
LET Awm_bom_msg_50
	"Saisissez valeur incr�ment des rep�res:"
LET Awm_bom_msg_51
	"Identifiez la pi�ce devant servir de composant � la nomenclature"
LET Awm_bom_msg_52
    "Extr�mit� 'SUP' non d�finie"
LET Awm_bom_msg_53
    "Extr�mit� 'INF' non d�finie"
LET Awm_bom_msg_54
    "Extr�mit� 'DROITE' non d�finie"
LET Awm_bom_msg_55
    "Extr�mit� 'GAUCHE' non d�finie"

LET Awm_bom_msg_56
    "Ce composant appara�t dans plusieurs NOMENC"
LET Awm_bom_msg_57
    "impossible de combiner une NOMENC � n niveaux avec une NOMENC � 1 niveau"
LET Awm_bom_msg_58
    "impossible de mettre � jour une NOMENC � n niveaux"

LET Awm_bom_msg_59
    "Saisissez le nombre de niveaux de la structure :"
LET Awm_bom_msg_21
    "Indiquez la pi�ce pour analyser les donn�es NOMENC ou ACTIVE ou NIVEAU 0"
LET Awm_bom_msg_22
    "Nom de pi�ce incorrect :"

{ Menu texts }

LET Awm_bom_mtext_1 "REPERE"
LET Awm_bom_mtext_2 "NOMENC"
LET Awm_bom_mtext_3 "CF-NOMC"
LET Awm_bom_mtext_4 "CREER REP"
LET Awm_bom_mtext_5 "Liste"
LET Awm_bom_mtext_6 "Identif"
LET Awm_bom_mtext_7 "HORIZONTAL"
LET Awm_bom_mtext_8 "VERTICAL"
LET Awm_bom_mtext_9 "RETRACER"
LET Awm_bom_mtext_10 "Rep�re"
LET Awm_bom_mtext_11 "SUPPRIMER"
LET Awm_bom_mtext_12 "Tout"
LET Awm_bom_mtext_13 "DEBUT REP"
LET Awm_bom_mtext_14 "INCREMENT"
LET Awm_bom_mtext_15 "FORMAT REP"
LET Awm_bom_mtext_16 "SORTIE"
LET Awm_bom_mtext_17 "Dessin"
LET Awm_bom_mtext_18 "FMT NOMENC"
LET Awm_bom_mtext_19 "OUVRIR"
LET Awm_bom_mtext_20 "NIVEAU 0"
LET Awm_bom_mtext_21 "Pi�ce"
LET Awm_bom_mtext_22 "AFFICHER"
LET Awm_bom_mtext_23 "NOMENC"
LET Awm_bom_mtext_24 ""
LET Awm_bom_mtext_25 "M � JOUR"
LET Awm_bom_mtext_26 ""
LET Awm_bom_mtext_27 ""
LET Awm_bom_mtext_28 ""
LET Awm_bom_mtext_29 ""
LET Awm_bom_mtext_30 "ME10 -> WM"
LET Awm_bom_mtext_31 "Rep�re"
LET Awm_bom_mtext_32 "ACTIVE"
LET Awm_bom_mtext_33 "Etat Pi�ce"
LET Awm_bom_mtext_34 "CONTINUER"
LET Awm_bom_mtext_35 "FIN"
LET Awm_bom_mtext_36 "EXTREMITE"
LET Awm_bom_mtext_37 "D�finir"
LET Awm_bom_mtext_38 "SUPERIEURE"
LET Awm_bom_mtext_39 "INFERIEURE"
LET Awm_bom_mtext_40 "GAUCHE"
LET Awm_bom_mtext_41 "DROITE"
LET Awm_bom_mtext_42 "STOCKER"
LET Awm_bom_mtext_43 "Fmt NOMENC"
LET Awm_bom_mtext_44 "SUPPRIMER"
LET Awm_bom_mtext_45 "Table"
LET Awm_bom_mtext_46 "DIN"
LET Awm_bom_mtext_47 "ISO"
LET Awm_bom_mtext_48 "UTIL."
LET Awm_bom_mtext_49 "A"
LET Awm_bom_mtext_50 "B"
LET Awm_bom_mtext_51 "C"

LET Awm_bom_mtext_52 "ANAL. NOMC"
LET Awm_bom_mtext_53                                ""
LET Awm_bom_mtext_54 "AJOUTER"
LET Awm_bom_mtext_55 "EFFACER"
LET Awm_bom_mtext_56 "ACTIVE"
LET Awm_bom_mtext_57 "NIVEAU 0"
LET Awm_bom_mtext_58 "NIVEAUX"

{ Table Titles }

LET Awm_bom_ttitle_1  "Nom-pi�ce"
LET Awm_bom_ttitle_2  "ID-pi�ce"
LET Awm_bom_ttitle_3  "Article-WM"
LET Awm_bom_ttitle_4  "NOMENC"
LET Awm_bom_ttitle_5  "Rep�re"
LET Awm_bom_ttitle_6  "X"
LET Awm_bom_ttitle_7  "ERREUR"
LET Awm_bom_ttitle_8  "DE"
LET Awm_bom_ttitle_9  "NOMENC ME10 ACTIVE"
LET Awm_bom_ttitle_10 "INFO ETAT PIECE ME10"
LET Awm_bom_ttitle_11 "COMPOS. NOMENC ACTIV"
LET Awm_bom_ttitle_12 "Message"

{ >>>endif }
