{  @(#)  awm_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of COBRA
}

{ >>>if false }

{ LOLAMAX = 246 }

{ General messages }

LET Awm_msg_1  "ロードするには SUBPART または CONFIRM と入力して下さい:"
LET Awm_msg_2  "図面の名前を変更するには CONFIRM を入力して下さい:"
LET Awm_msg_3  "パーツ または CURRENT を指定して下さい"
LET Awm_msg_4  "図面は WorkManager からロードされていません"
LET Awm_msg_5  "現在の表題欄のパーツクラスの '名前' を入力して下さい:"
LET Awm_msg_6  "現在の表題欄の文書クラスの '名前' を入力して下さい:"
LET Awm_msg_7  "ストアするパーツ または CURRENT を指定して下さい"
LET Awm_msg_8  "ME10 の図面 ID を消去するには CONFIRM を入力して下さい"
LET Awm_msg_9  "WorkManager と接続中です ..."
LET Awm_msg_10 "ME10INTF の算術演算に対する引き数が不適当です"
LET Awm_msg_11 "WorkManager を起動し、このアクションを再実行して下さい"
LET Awm_msg_12 "WorkManager でエラーが発生しました"
LET Awm_msg_13 "更新する参照がありません"
LET Awm_msg_14 "タブレットが接続されていません"
LET Awm_msg_15 "WorkManager を起動するか, WorkManager で 'DDE_ENABLE ON' を使用後、再実行して下さい"
LET Awm_msg_16 "警告!: 図面 1 のトップ・パーツはまだトップ・レベルの 1 つ下になっています"
LET Awm_msg_17 "警告!: 図面 2 のトップ・パーツはまだトップ・レベルの 1 つ下になっています"
LET Awm_msg_18 "警告!: 更新された図面のトップ・パーツはまだトップ・レベルの 1 つ下になっています"
DEFINE Awm_msg_19 ("パーツ " + Adu_part_y + " を " + Adu_part_x +" に名前を変更します")
END_DEFINE
LET Awm_msg_20 "ヘルプを表示する項目を選択して下さい"
LET Awm_msg_35 "Drawing BOM table. Wait..."
LET Awm_msg_36 "Add PDF limited to unmodified drawings"

{ Menu texts }

DEFINE Awm_mtext_1
		 CENTER "WM"
END_DEFINE
DEFINE Awm_mtext_2
		 CENTER "Desktop"
END_DEFINE
LET Awm_mtext_3  "ロード"
LET Awm_mtext_4  "子パーツ"
LET Awm_mtext_5  "絶対位置"
LET Awm_mtext_6  "ストア"
LET Awm_mtext_7  "全　て"
LET Awm_mtext_8  "パーツ"
LET Awm_mtext_9  "カレント"
LET Awm_mtext_10 "上書きｽﾄｱ"
LET Awm_mtext_11 "専　有"
LET Awm_mtext_12 "ID"
LET Awm_mtext_13 "図  面"
LET Awm_mtext_14 "WM-パーツ"
LET Awm_mtext_15 "情　報"
LET Awm_mtext_16 "トップ"
LET Awm_mtext_17 "ID 消去"

LET Awm_mtext_18 "更　新"
LET Awm_mtext_19 "ﾃｷｽﾄ表示"
LET Awm_mtext_20 "ﾃｷｽﾄ参照名"
LET Awm_mtext_21 "参照内容"
LET Awm_mtext_22 "表題欄呼出"
LET Awm_mtext_23 ""
LET Awm_mtext_24 "定　義"
LET Awm_mtext_25 "部品"
LET Awm_mtext_26 "カレント"
LET Awm_mtext_27 "トップ"
LET Awm_mtext_28 "レベル"
LET Awm_mtext_29 "専有解除"
LET Awm_mtext_30 ""
LET Awm_mtext_31 ""
LET Awm_mtext_32 ""
LET Awm_mtext_33 "表　示"
LET Awm_mtext_34 ""

LET Awm_mtext_35 "設　定"
LET Awm_mtext_36 "クラス"
LET Awm_mtext_37 "参照定義"
LET Awm_mtext_38 "定義削除"
LET Awm_mtext_39 "行　幅"
LET Awm_mtext_40 "大／小文字"
LET Awm_mtext_41 "行　数"
LET Awm_mtext_42 "折り返し"
LET Awm_mtext_43 "精　度"
LET Awm_mtext_44 "オプション"
LET Awm_mtext_45 "大文字"
LET Awm_mtext_46 "オ　ン"
LET Awm_mtext_47 "小文字"
LET Awm_mtext_48 "オ　フ"
LET Awm_mtext_49 "変　更"
LET Awm_mtext_50 "参照表示"
LET Awm_mtext_51 "全　て"
LET Awm_mtext_52 "選　択"
LET Awm_mtext_53 "表  示"
LET Awm_mtext_54 "選　択"
LET Awm_mtext_55 ""
LET Awm_mtext_56 ""
LET Awm_mtext_57 ""
LET Awm_mtext_58 ""
LET Awm_mtext_59 ""

DEFINE Awm_mtext_60
		 CENTER "AIP ﾒｲﾝ･ﾒﾆｭｰ"
END_DEFINE
DEFINE Awm_mtext_61
		 CENTER "AIP メイン・メニュー"
END_DEFINE
LET Awm_mtext_62 "ﾌｧｲﾙ"
LET Awm_mtext_63 "表題欄"
LET Awm_mtext_64 "表題ｾｯﾄ"
LET Awm_mtext_65 ""
LET Awm_mtext_66 ""
LET Awm_mtext_67 ""
LET Awm_mtext_68 ""
LET Awm_mtext_69 ""

LET Awm_mtext_70 "図面ストア"
LET Awm_mtext_71 "パーツ ストア"
LET Awm_mtext_72 "カレント文書上書き"
LET Awm_mtext_73 "新規バージョン作成"
LET Awm_mtext_74 "新規文書作成"
LET Awm_mtext_75 "新規部品および新規文書作成"
LET Awm_mtext_76 "キャンセル"

DEFINE Awm_mtext_77
		 CENTER "データ管理"
END_DEFINE
DEFINE Awm_mtext_78
		 CENTER "データ管理"
END_DEFINE
LET Awm_mtext_79 "子パーツ"
LET Awm_mtext_80 "部品"
DEFINE Awm_mtext_81
		 CENTER "オープン"
END_DEFINE
DEFINE Awm_mtext_82
		 CENTER "エディタ"
END_DEFINE
DEFINE Awm_mtext_83
		 CENTER "PDF を追加"
END_DEFINE

{ Table Titles }

LET Awm_ttitle_1 "ｵﾌ"
LET Awm_ttitle_2 "テキスト参照名"
LET Awm_ttitle_3 "テキスト参照名 & 値"
LET Awm_ttitle_4 "名前"
LET Awm_ttitle_5 "値"
LET Awm_ttitle_6 "テキスト参照情報  [ @s0 項目が見つかりました ]"
LET Awm_ttitle_7 "テーブル"
LET Awm_ttitle_8 "名前"
LET Awm_ttitle_9 "カラム"
LET Awm_ttitle_10 "行 幅"
LET Awm_ttitle_11 "行 数"
LET Awm_ttitle_12 "精　度"
LET Awm_ttitle_13 "折り返し"
LET Awm_ttitle_14 "大/小文字"

{ New General messages }
LET Awm_msg_21
    "パーツ要素の情報を定義するパーツ または CURRENT または TOP を指定して下さい"
LET Awm_msg_22
    "不適当なパーツ名:"
LET Awm_msg_23
    "構造レベルの数を入力して下さい:"

{ Message if BOM not enabled }
LET Awm_msg_24
	"設定エラー: BOM が使用可能となっていません"

LET Awm_msg_25
	"RCC はまだ保留のままです"
LET Awm_msg_26
	"パーツ情報設定:"
LET Awm_msg_27
	"パーツ・ツリー読込み ..."
LET Awm_msg_28
	"パーツ情報検索:"
LET Awm_msg_29 "図面の名前を入力してください:"
LET Awm_msg_30 "図面ファイルがありません"
LET Awm_msg_31 "図面クラスが登録されていません"
LET Awm_msg_32 "図面のバージョンを入力してください:"
LET Awm_msg_33 "説明を入力してください"
LET Awm_msg_34 "注記テキストの変更を入力してください:"

{* Toolbar Item Text Messages for NewUI (PELOOK = 2,3) *}

LET Awm_ttext_1  "A"
LET Awm_ttext_2  "A0"
LET Awm_ttext_3  "A1"
LET Awm_ttext_4  "A2"
LET Awm_ttext_5  "A3"
LET Awm_ttext_6  "A4"
LET Awm_ttext_7  "絶対値"
LET Awm_ttext_8  "全　て"
DEFINE Awm_ttext_9
  ("割当ﾚﾍﾞﾙ : "+ (STR(Awmc_val_assign_level)))
END_DEFINE
LET Awm_ttext_10  "ﾊﾟｰﾂ割当"
LET Awm_ttext_11  "参照割当"
LET Awm_ttext_12  "B"
LET Awm_ttext_13  "BOM ﾚｲｱｳﾄ"
LET Awm_ttext_14  "BOM設定"
LET Awm_ttext_15  "BOM"
LET Awm_ttext_16  "BOM構成"
LET Awm_ttext_17  "C"
DEFINE Awm_ttext_18
  ("大/小文字 : " + Tr_case_convention_value)
END_DEFINE
LET Awm_ttext_19  "大/小文字"
LET Awm_ttext_20  "変更"
LET Awm_ttext_21  "ID消去"
LET Awm_ttext_22  "継続"
LET Awm_ttext_23  "ｲﾝﾃﾞﾝﾄ付き作成"
LET Awm_ttext_24  "ﾘｽﾄから作成"
LET Awm_ttext_25  "ﾃｰﾌﾞﾙから作成"
LET Awm_ttext_26  "ｶﾚﾝﾄ"
LET Awm_ttext_27  "D"
LET Awm_ttext_28  "位置番号削除"
LET Awm_ttext_29  "DesignManager ADVANCED"
LET Awm_ttext_30  "DesignManager DESIGNER"
LET Awm_ttext_31  "DesignManager DESKTOP"
LET Awm_ttext_32  "DesignManager EXPERT"
LET Awm_ttext_33  "DesignManager EXPERT CLASSIC"
LET Awm_ttext_34  "名前描画"
LET Awm_ttext_35  "値描画"
LET Awm_ttext_36  "図面ID"
LET Awm_ttext_37  "図　面"
LET Awm_ttext_38  "E"
LET Awm_ttext_39  "終了"
LET Awm_ttext_40  "ﾌﾗｸﾞﾚｲｱｳﾄ"
LET Awm_ttext_41  "水平"
DEFINE Awm_ttext_42
  ("増分 : " + (STR(Awmc_bom_val_increment)))
END_DEFINE
LET Awm_ttext_43  "情報図面"
LET Awm_ttext_44  "属性情報"
LET Awm_ttext_45  "左"
DEFINE Awm_ttext_46
  ("線 : " + Tr_set_lines_value)
END_DEFINE
LET Awm_ttext_47  "線"
LET Awm_ttext_48  "表題欄ﾛｰﾄﾞ"
LET Awm_ttext_49  "ﾛｰﾄﾞ"
LET Awm_ttext_50  "下段"
LET Awm_ttext_51  "新規図面"
LET Awm_ttext_52  "新規ﾊﾟｰﾂと図面"
LET Awm_ttext_53  "新ﾊﾞｰｼﾞｮﾝ"
LET Awm_ttext_54  "オ　フ"
LET Awm_ttext_55  "ｵﾝ"
LET Awm_ttext_56  "BOM ｵｰﾌﾟﾝ"
LET Awm_ttext_57  "ｵﾌﾟｼｮﾝ"
LET Awm_ttext_58  "上書き"
LET Awm_ttext_59  "位置番号"
LET Awm_ttext_60  "パーツ"
LET Awm_ttext_61  "位置番号"
DEFINE Awm_ttext_62
  ("精度 : " + Tr_set_precision_value)
END_DEFINE
LET Awm_ttext_63  "精度"
LET Awm_ttext_64  "位置番号再描画"
LET Awm_ttext_65  "参照名"
LET Awm_ttext_66  "参照値"
LET Awm_ttext_67  "全位置番号削除"
LET Awm_ttext_68  "位置番号削除"
LET Awm_ttext_69  "図面専有"
LET Awm_ttext_70  "Rev ﾁｪｯｸ"
LET Awm_ttext_71  "右"
LET Awm_ttext_72  "ｽｷｬﾝ(追加)"
LET Awm_ttext_73  "ｽｷｬﾝ(消去)"
DEFINE Awm_ttext_74
  ("ｽｷｬﾝ ﾚﾍﾞﾙ : " + (STR(Awmc_val_scan_level)))
END_DEFINE
LET Awm_ttext_75  "ｸﾗｽ選択"
LET Awm_ttext_76  "選択"
LET Awm_ttext_77  "線終点設定"
LET Awm_ttext_78  "設定"
LET Awm_ttext_79  "BOM表示"
LET Awm_ttext_80  "ﾃﾞｰﾀ表示"
LET Awm_ttext_81  "ﾊﾟｰﾂの状態表示"
LET Awm_ttext_82  "参照表示"
DEFINE Awm_ttext_83
  ("開始位置 : " + (STR(Awmc_bom_val_start_posnr)))
END_DEFINE
LET Awm_ttext_84  "全保管"
LET Awm_ttext_85  "ﾊﾟｰﾂ ｽﾄｱ"
LET Awm_ttext_86  "子ﾊﾟｰﾂ保管"
LET Awm_ttext_87  "ｽﾄｱ"
LET Awm_ttext_88  "子パーツ"
LET Awm_ttext_89  "表題欄 構成"
LET Awm_ttext_90  "表題欄-BOM 構成"
LET Awm_ttext_91  "表題欄構成"
LET Awm_ttext_92  "図面ﾃｰﾌﾞﾙ"
LET Awm_ttext_93  "表題欄"
LET Awm_ttext_94  "ﾄｯﾌﾟﾊﾟｰﾂ"
LET Awm_ttext_95  "ﾄｯﾌﾟ"
LET Awm_ttext_96  "ユーザ"
LET Awm_ttext_97  "割当解除"
LET Awm_ttext_98  "専有解除"
LET Awm_ttext_99  "図面更新"
LET Awm_ttext_100  "表題欄更新"
LET Awm_ttext_101  "ME10 からの更新"
LET Awm_ttext_102  "更新"
LET Awm_ttext_103  "上段"
LET Awm_ttext_104  "鉛直"
LET Awm_ttext_105  "WM-ﾊﾟｰﾂID"
LET Awm_ttext_106  "WMDT"
DEFINE Awm_ttext_107
  ("幅 : " + Tr_set_width_value)
END_DEFINE
LET Awm_ttext_108  "行　幅"
LET Awm_ttext_109  "折り返し"
LET Awm_ttext_110  "DIN"
LET Awm_ttext_111  "ISO"
LET Awm_ttext_112  "ﾜｰｸｽﾍﾟｰｽ"
LET Awm_ttext_113  "検索"
LET Awm_ttext_114  "PDF を追加"
LET Awm_mttext_1   "Drawing Manager"

{* Toolbar Messages New User Interface (PELOOK = 2,3) *}

LET Awm_tmess_1  "Windows ﾕｰｻﾞｲﾝﾀﾌｪｰｽでは ADU はｻﾎﾟｰﾄされていません"

{ >>>endif }
