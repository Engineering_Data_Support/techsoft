﻿{  @(#)  awm_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of COBRA
  Non-English files are found at:   <WorkManager playpen>\desktop\aips\me10\local\<language>\startup\awm_msg.m
}

{ >>>if false }

{ LOLAMAX = 248 }

{ General messages }

LET Awm_msg_1  {lola_msg 1  80 "Prompt message"} "Entrar SUPERIOR o SUBPIEZA o CONFIRMAR para cargar: "
LET Awm_msg_2  {lola_msg 2  80 "Prompt message"} "Entrar CONFIRMAR para renombrar el dibujo a: "
LET Awm_msg_3  {lola_msg 3  80 "Prompt message"} "Identificar pieza para información o ACTUAL"
LET Awm_msg_4  {lola_msg 4  80 "Error message"} "Dibujo no cargado desde WorkManager"
LET Awm_msg_5  {lola_msg 5  80 "Prompt message"} "Entrar 'Nombre' de la clase de piezas del bloque de título actual: "
LET Awm_msg_6  {lola_msg 6  80 "Prompt message"} "Entrar 'Nombre' de la clase de documentos del bloque de título actual: "
LET Awm_msg_7  {lola_msg 7  80 "Prompt message"} "Identificar pieza para almacenar o ACTUAL"
LET Awm_msg_8  {lola_msg 8  80 "Prompt message"} "Entrar CONFIRMAR para borrar ID de dibujo en ME10"
LET Awm_msg_9  {lola_msg 9  80 "Prompt message"} "Ejecutando conexión de WorkManager..."
LET Awm_msg_10 {lola_msg 10 80 "Error message"} "Argumento no válido de función aritmética ME10INTF"
LET Awm_msg_11 {lola_msg 11 80 "Error message"} "Iniciar WorkManager y reintentar la acción"
LET Awm_msg_12 {lola_msg 12 80 "Error message"} "ERROR en WorkManager"
LET Awm_msg_13 {lola_msg 13 80 "Error message"} "No se han hallado referencias para actualizar"
LET Awm_msg_14 {lola_msg 14 80 "Error message"} "No hay tableta conectada"
LET Awm_msg_15 {lola_msg 15 80 "Error message"} "Iniciar WorkManager o utilizar 'DDE_ENABLE ON' con WorkManager y luego reintentar la acción"
LET Awm_msg_16 {lola_msg 16 80 "Warning"} "ADVERTENCIA: la parte superior del dibujo 1 sigue estando un nivel por debajo de SUPERIOR"
LET Awm_msg_17 {lola_msg 17 80 "Warning"} "ADVERTENCIA: la parte superior del dibujo 2 sigue estando un nivel por debajo de SUPERIOR"
LET Awm_msg_18 {lola_msg 18 100 "Warning"} "ADVERTENCIA: la parte superior del dibujo actualizado sigue estando un nivel por debajo de SUPERIOR"
DEFINE Awm_msg_19 ({lola_msg 19 80 "Prompt message" "1 -> partname" "2 -> partname"}
 "Renombrar pieza " + Adu_part_y + " to " + Adu_part_x)
END_DEFINE
LET Awm_msg_20 {lola_msg 20 80 "Prompt message"} "Seleccionar elemento sobre el que obtener ayuda"
LET Awm_msg_35 {lola_msg 243 80 "Prompt message"} "Tabla BOM de dibujo. Espere..."
LET Awm_msg_36   {lola_msg 247 80 "Warning"} "Añadir PDF limitado a dibujos no modificados"

{ Menu texts }

DEFINE Awm_mtext_1
                 CENTER {lola_msg 21 10 "Menu text"} "WM"
END_DEFINE
DEFINE Awm_mtext_2
                 CENTER {lola_msg 22 10 "Menu text"} "Desktop"
END_DEFINE
LET Awm_mtext_3  {lola_msg 23 10 "Menu text"} "CARGAR      "
LET Awm_mtext_4  {lola_msg 24 10 "Menu text"} "SUBPIEZA   "
LET Awm_mtext_5  {lola_msg 25 10 "Menu text"} "ABSOLUTO  "
LET Awm_mtext_6  {lola_msg 26 10 "Menu text"} "ALMACENAR     "
LET Awm_mtext_7  {lola_msg 27 10 "Menu text"} "Todo      "
LET Awm_mtext_8  {lola_msg 29 10 "Menu text"} "Parte     "
LET Awm_mtext_9  {lola_msg 30 10 "Menu text"} "ACTUAL   "
LET Awm_mtext_10 {lola_msg 31 10 "Menu text"} "ALMACENAR Y SOBRESCRIBIR"
LET Awm_mtext_11 {lola_msg 32 10 "Menu text"} "RESERVAR   "
LET Awm_mtext_12 {lola_msg 33 10 "Menu text"} "ID        "
LET Awm_mtext_13 {lola_msg 34 10 "Menu text"} "Dibujo   "
LET Awm_mtext_14 {lola_msg 35 10 "Menu text"} "WM-Pieza   "
LET Awm_mtext_15 {lola_msg 36 10 "Menu text"} "INFORMACIÓN      "
LET Awm_mtext_16 {lola_msg 37 10 "Menu text"} "Superior       "
LET Awm_mtext_17 {lola_msg 38 10 "Menu text"} "BORRAR ID  "

LET Awm_mtext_18 {lola_msg 39 10 "Menu text"} "ACTUALIZAR    "
LET Awm_mtext_19 {lola_msg 40 10 "Menu text"} "DIBUJAR      "
LET Awm_mtext_20 {lola_msg 41 10 "Menu text"} "Nombres TR  "
LET Awm_mtext_21 {lola_msg 42 10 "Menu text"} "Valores TR "
LET Awm_mtext_22 {lola_msg 43 10 "Menu text"} "CARGAR TB   "
LET Awm_mtext_23 ""
LET Awm_mtext_24 {lola_msg 45 10 "Menu text" } "ASIGNAR"
LET Awm_mtext_25 {lola_msg 46 10 "Menu text" } "WM-Piezas"
LET Awm_mtext_26 {lola_msg 47 10 "Menu text" } "ACTUAL"
LET Awm_mtext_27 {lola_msg 48 10 "Menu text" } "SUPERIOR"
LET Awm_mtext_28 {lola_msg 49 10 "Menu text" } "NIVELES"
LET Awm_mtext_29 {lola_msg 50 10 "Menu text"} "DESRESERVAR "
LET Awm_mtext_30 ""
LET Awm_mtext_31 ""
LET Awm_mtext_32 ""
LET Awm_mtext_33 {lola_msg 54 10 "Menu text"} "MOSTRAR DATOS "
LET Awm_mtext_34 ""

LET Awm_mtext_35 {lola_msg 56 10 "Menu text"} "ESTABLECER       "
LET Awm_mtext_36 {lola_msg 57 10 "Menu text"} "Clases   "
LET Awm_mtext_37 {lola_msg 58 10 "Menu text"} "TR-ASIGNAR "
LET Awm_mtext_38 {lola_msg 59 10 "Menu text"} "DESASIGNAR  "
LET Awm_mtext_39 {lola_msg 60 10 "Menu text"} "Anchura     "
LET Awm_mtext_40 {lola_msg 61 10 "Menu text"} "Mayús./minús. "
LET Awm_mtext_41 {lola_msg 62 10 "Menu text"} "Líneas    "
LET Awm_mtext_42 {lola_msg 63 10 "Menu text"} "Ajustar texto "
LET Awm_mtext_43 {lola_msg 64 10 "Menu text"} "Precisión "
LET Awm_mtext_44 {lola_msg 65 10 "Menu text"} "OPCIONES   "
LET Awm_mtext_45 {lola_msg 66 10 "Menu text"} "Superior     "
LET Awm_mtext_46 {lola_msg 67 10 "Menu text"} "Activado        "
LET Awm_mtext_47 {lola_msg 68 10 "Menu text"} "Inferior     "
LET Awm_mtext_48 {lola_msg 69 10 "Menu text"} "Desactivado       "
LET Awm_mtext_49 {lola_msg 70 10 "Menu text"} "CAMBIAR    "
LET Awm_mtext_50 {lola_msg 71 10 "Menu text"} "MOSTRAR REFS. "
LET Awm_mtext_51 {lola_msg 72 10 "Menu text"} "Todo      "
LET Awm_mtext_52 {lola_msg 73 10 "Menu text"} "Seleccionar    "
LET Awm_mtext_53 {lola_msg 74 10 "Menu text"} "MOSTRAR      "
LET Awm_mtext_54 {lola_msg 75 10 "Menu text"} "SELECCIONAR    "
LET Awm_mtext_55 ""
LET Awm_mtext_56 ""
LET Awm_mtext_57 ""
LET Awm_mtext_58 ""
LET Awm_mtext_59 ""

DEFINE Awm_mtext_60
                 CENTER {lola_msg 81 15 "Menu text"} "Menú principal AIP"
END_DEFINE
DEFINE Awm_mtext_61
                 CENTER {lola_msg 82 21 "Menu text"} "Menú principal AIP"
END_DEFINE
LET Awm_mtext_62 {lola_msg 83  6 "Menu text"} "ARCHIVO  "
LET Awm_mtext_63 {lola_msg 84  7 "Menu text"} "BLOQUE-T"
LET Awm_mtext_64 {lola_msg 85  7 "Menu text"} "TB-CONFIG."
LET Awm_mtext_65 ""
LET Awm_mtext_66 ""
LET Awm_mtext_67 ""
LET Awm_mtext_68 ""
LET Awm_mtext_69 ""

LET Awm_mtext_70 {lola_msg 113 40 "Menu text"} "ALMACENAR DIBUJO"
LET Awm_mtext_71 {lola_msg 114 40 "Menu text"} "ALMACENAR PIEZA"
LET Awm_mtext_72 {lola_msg 115 40 "Menu text"} "Sobrescribir documento actual"
LET Awm_mtext_73 {lola_msg 116 40 "Menu text"} "Crear nueva versión"
LET Awm_mtext_74 {lola_msg 117 40 "Menu text"} "Crear nuevo documento"
LET Awm_mtext_75 {lola_msg 118 40 "Menu text"} "Crear nueva pieza y nuevo documento"
LET Awm_mtext_76 {lola_msg 119 40 "Menu text"} "CANCELAR"

DEFINE Awm_mtext_77
                 CENTER {lola_msg 125 15 "Menu text"} "Data Management"
END_DEFINE
DEFINE Awm_mtext_78
                 CENTER {lola_msg 126 21 "Menu text"} "Data Management"
END_DEFINE
LET Awm_mtext_79 {lola_msg 127 10 "Menu text"} "Subpieza"
LET Awm_mtext_80 {lola_msg 128 10 "Menu text"} "Datos maestros"
DEFINE Awm_mtext_81
                 CENTER {lola_msg 130 10 "Menu text"} "Abrir"
END_DEFINE
DEFINE Awm_mtext_82
                 CENTER {lola_msg 131 10 "Menu text"} "Editor"
END_DEFINE

DEFINE Awm_mtext_83
                 CENTER {lola_msg 248 8 "Toolbar text"} "Añadir PDF"
END_DEFINE
{ Table Titles }

LET Awm_ttitle_1 {lola_msg 91  3 "Table Title"} "DESACTTIVADO"
LET Awm_ttitle_2 {lola_msg 92 16 "Table Title"} "NOMBRES REF. TEXTO"
LET Awm_ttitle_3 {lola_msg 93 30 "Table Title"} "NOMBRES Y VALORES DE REFERENCIA DE TEXTO"
LET Awm_ttitle_4 {lola_msg 94 10 "Table Title"} "NOMBRES"
LET Awm_ttitle_5 {lola_msg 95 10 "Table Title"} "VALORES"
LET Awm_ttitle_6 {lola_msg 96 50 "Table Title"} "INFORMACIÓN DE REFERENCIA DE TEXTO [ @s0 elementos encontrados ]"
LET Awm_ttitle_7 {lola_msg 97 10 "Table Title"} "TABLA"
LET Awm_ttitle_8 {lola_msg 98 10 "Table Title"} "NOMBRE"
LET Awm_ttitle_9 {lola_msg 99 6 "Table Title"} "COLUMNA"
LET Awm_ttitle_10 {lola_msg 100 6 "Table Title"} "ANCHURA"
LET Awm_ttitle_11 {lola_msg 101 6 "Table Title"} "LÍNEAS"
LET Awm_ttitle_12 {lola_msg 102 12 "Table Title"} "PRECISIÓN"
LET Awm_ttitle_13 {lola_msg 103 9 "Table Title"} "AJUSTE DE TEXTO"
LET Awm_ttitle_14 {lola_msg 104 9 "Table Title"} "MAYÚS./MINÚS."

{ New General messages }
LET Awm_msg_21 {lola_msg 105  80 "Prompt message"}
    "Identificar pieza para asignar datos de WM-pieza o ACTUAL o SUPERIOR"
LET Awm_msg_22  {lola_msg 106  80 "Error message"}
    "Nombre de pieza no válido: "
LET Awm_msg_23  {lola_msg 107  80 "Prompt message"}
    "Entrar número de niveles de estructura:"

{ Message if BOM not enabled }
LET Awm_msg_24  {lola_msg 108 80 "Error message"}
        "Error de configuración: BOM no activada"

LET Awm_msg_25  {lola_msg 109 80 "Error message"}
        "Aún hay un RCC pendiente"
LET Awm_msg_26  {lola_msg 110 80 "Display message"}
        "Establecer informaciones de pieza: "
LET Awm_msg_27  {lola_msg 111 80 "Display message"}
        "Leer árbol de piezas..."
LET Awm_msg_28  {lola_msg 112 80 "Display message"}
        "Consultar informaciones de pieza: "
LET Awm_msg_29 {lola_msg 120 80 "Prompt message"} "Entrar nombre de dibujo:"
LET Awm_msg_30 {lola_msg 121 80 "Error message"} "No existe ningún archivo de dibujo"
LET Awm_msg_31 {lola_msg 122 80 "Error message"} "No hay clase de dibujo registrada"
LET Awm_msg_32 {lola_msg 123 80 "Prompt message"} "Entrar versión de dibujo:"
LET Awm_msg_33 {lola_msg 124 80 "Prompt message"} "Entrar descripción:"
LET Awm_msg_34 {lola_msg 129 80 "Prompt message"} "Entrar texto de nota de cambio:"

{* Toolbar Item Text Messages for NewUI (PELOOK = 2,3) *}

LET Awm_ttext_1 {lola_msg 131 1 "Toolbar text"} "A"
LET Awm_ttext_2 {lola_msg 132 2 "Toolbar text"} "A0"
LET Awm_ttext_3 {lola_msg 133 2 "Toolbar text"} "A1"
LET Awm_ttext_4 {lola_msg 134 2 "Toolbar text"} "A2"
LET Awm_ttext_5 {lola_msg 135 2 "Toolbar text"} "A3"
LET Awm_ttext_6 {lola_msg 136 2 "Toolbar text"} "A4"
LET Awm_ttext_7 {lola_msg 137 16 "Toolbar text"} "Absoluto"
LET Awm_ttext_8 {lola_msg 138 8 "Toolbar text"} "Todo"
DEFINE Awm_ttext_9
  ({lola_msg 139 30 "Toolbar text" "1 -> assign level"}
  "Asignar niveles: "+ (STR(Awmc_val_assign_level)))
END_DEFINE
LET Awm_ttext_10 {lola_msg 140 20 "Toolbar text"} "Asignar piezas"
LET Awm_ttext_11 {lola_msg 141 20 "Toolbar text"} "Asignar referencias"
LET Awm_ttext_12 {lola_msg 142 1 "Toolbar text"} "B"
LET Awm_ttext_13 {lola_msg 143 20 "Toolbar text"} "Layout de BOM"
LET Awm_ttext_14 {lola_msg 144 20 "Toolbar text"} "Ajustes de BOM"
LET Awm_ttext_15 {lola_msg 145 8 "Toolbar text"} "BOM"
LET Awm_ttext_16 {lola_msg 146 20 "Toolbar text"} "Configuración de BOM"
LET Awm_ttext_17 {lola_msg 147 1 "Toolbar text"} "C"
DEFINE Awm_ttext_18
  ({lola_msg 148 30 "Toolbar text" "1 -> convention value"}
  "Mayúsculas/minúsculas: " + Tr_case_convention_value)
END_DEFINE
LET Awm_ttext_19 {lola_msg 149 20 "Toolbar text"} "Mayúsculas/minúsculas"
LET Awm_ttext_20 {lola_msg 150 20 "Toolbar text"} "Cambiar"
LET Awm_ttext_21 {lola_msg 151 20 "Toolbar text"} "Borrar ID"
LET Awm_ttext_22 {lola_msg 152 20 "Toolbar text"} "Continuar"
LET Awm_ttext_23 {lola_msg 153 20 "Toolbar text"} "Crear por identificador"
LET Awm_ttext_24 {lola_msg 154 20 "Toolbar text"} "Crear de lista"
LET Awm_ttext_25 {lola_msg 155 20 "Toolbar text"} "Crear de tabla"
LET Awm_ttext_26 {lola_msg 156 20 "Toolbar text"} "Actual"
LET Awm_ttext_27 {lola_msg 157  1 "Toolbar text"} "D"
LET Awm_ttext_28 {lola_msg 158 20 "Toolbar text"} "Suprimir núm. pos."
LET Awm_ttext_29 {lola_msg 159 80 "Toolbar text"} "DesignManager ADVANCED"
LET Awm_ttext_30 {lola_msg 160 80 "Toolbar text"} "DesignManager DESIGNER"
LET Awm_ttext_31 {lola_msg 161 80 "Toolbar text"} "DesignManager DESKTOP"
LET Awm_ttext_32 {lola_msg 162 80 "Toolbar text"} "DesignManager EXPERT"
LET Awm_ttext_33 {lola_msg 163 80 "Toolbar text"} "DesignManager EXPERT CLASSIC"
LET Awm_ttext_34 {lola_msg 164 20 "Toolbar text"} "Dibujar nombres"
LET Awm_ttext_35 {lola_msg 165 20 "Toolbar text"} "Dibujar valores"
LET Awm_ttext_36 {lola_msg 166 20 "Toolbar text"} "ID de dibujo"
LET Awm_ttext_37 {lola_msg 167 20 "Toolbar text"} "Dibujo"
LET Awm_ttext_38 {lola_msg 168 1 "Toolbar text"} "E"
LET Awm_ttext_39 {lola_msg 169 8 "Toolbar text"} "Final"
LET Awm_ttext_40 {lola_msg 170 20 "Toolbar text"} "Layout de indicadores"
LET Awm_ttext_41 {lola_msg 171 20 "Toolbar text"} "Horizontal"
DEFINE Awm_ttext_42
  ({lola_msg 172 30 "Toolbar text" "1 -> bom val increment"}
  "Incremento: " + (STR(Awmc_bom_val_increment)))
END_DEFINE
LET Awm_ttext_43 {lola_msg 173 20 "Toolbar text"} "Inform. de dibujo"
LET Awm_ttext_44 {lola_msg 174 10 "Toolbar text"} "Información"
LET Awm_ttext_45 {lola_msg 175 10 "Toolbar text"} "Izquierda"
DEFINE Awm_ttext_46
  ({lola_msg 176 30 "Toolbar text" "1 -> lines value"}
  "Líneas: " + Tr_set_lines_value)
END_DEFINE
LET Awm_ttext_47 {lola_msg 177 20 "Toolbar text"} "Líneas"
LET Awm_ttext_48 {lola_msg 178 20 "Toolbar text"} "Cargar bloque de título"
LET Awm_ttext_49 {lola_msg 179 20 "Toolbar text"} "Cargar"
LET Awm_ttext_50 {lola_msg 180 20 "Toolbar text"} "Minúscula"
LET Awm_ttext_51 {lola_msg 181 20 "Toolbar text"} "Dibujo nuevo"
LET Awm_ttext_52 {lola_msg 182 20 "Toolbar text"} "Pieza y dibujo nuevos."
LET Awm_ttext_53 {lola_msg 183 20 "Toolbar text"} "Versión nueva"
LET Awm_ttext_54 {lola_msg 184 8 "Toolbar text"} "Desactivado"
LET Awm_ttext_55 {lola_msg 185 8 "Toolbar text"} "Activado"
LET Awm_ttext_56 {lola_msg 186 20 "Toolbar text"} "Abrir BOM"
LET Awm_ttext_57 {lola_msg 187 20 "Toolbar text"} "Opciones"
LET Awm_ttext_58 {lola_msg 188 20 "Toolbar text"} "Sobrescribir"
LET Awm_ttext_59 {lola_msg 189 20 "Toolbar text"} "Número pos."
LET Awm_ttext_60 {lola_msg 190 8 "Toolbar text"} "Pieza"
LET Awm_ttext_61 {lola_msg 191 20 "Toolbar text"} "Núm. pos."
DEFINE Awm_ttext_62
  ({lola_msg 192 30 "Toolbar text" "1 -> Precision value"}
  "Precisión: " + Tr_set_precision_value)
END_DEFINE
LET Awm_ttext_63 {lola_msg 193 20 "Toolbar text"} "Precisión"
LET Awm_ttext_64 {lola_msg 194 20 "Toolbar text"} "Redibujar núm. pos."
LET Awm_ttext_65 {lola_msg 195 20 "Toolbar text"} "Nombres de referencia"
LET Awm_ttext_66 {lola_msg 196 20 "Toolbar text"} "Valores de referencia"
LET Awm_ttext_67 {lola_msg 197 20 "Toolbar text"} "Eliminar todos núm. pos."
LET Awm_ttext_68 {lola_msg 198 20 "Toolbar text"} "Eliminar núm. pos."
LET Awm_ttext_69 {lola_msg 199 20 "Toolbar text"} "Reservar dibujo"
LET Awm_ttext_70 {lola_msg 200 20 "Toolbar text"} "Comprobar rev."
LET Awm_ttext_71 {lola_msg 201 8 "Toolbar text"} "Derecha"
LET Awm_ttext_72 {lola_msg 202 20 "Toolbar text"} "Escanear (Agregar)"
LET Awm_ttext_73 {lola_msg 203 20 "Toolbar text"} "Escanear (Borrar)"
DEFINE Awm_ttext_74
  ({lola_msg 204 30 "Toolbar text" "1 -> Scan level"}
  "Niveles de escaneo: " + (STR(Awmc_val_scan_level)))
END_DEFINE
LET Awm_ttext_75 {lola_msg 205 20 "Toolbar text"} "Seleccionar clases"
LET Awm_ttext_76 {lola_msg 206 20 "Toolbar text"} "Seleccionar"
LET Awm_ttext_77 {lola_msg 207 20 "Toolbar text"} "Establecer punto final de línea"
LET Awm_ttext_78 {lola_msg 208 20 "Toolbar text"} "Ajustes"
LET Awm_ttext_79 {lola_msg 209 20 "Toolbar text"} "Mostrar BOM"
LET Awm_ttext_80 {lola_msg 210 20 "Toolbar text"} "Mostrar datos"
LET Awm_ttext_81 {lola_msg 211 20 "Toolbar text"} "Mostrar estado de pieza"
LET Awm_ttext_82 {lola_msg 212 20 "Toolbar text"} "Mostrar referencias"
DEFINE Awm_ttext_83
  ({lola_msg 213 30 "Toolbar text" "1 -> Bom start posnr"}
  "Pos. inicial: " + (STR(Awmc_bom_val_start_posnr)))
END_DEFINE
LET Awm_ttext_84 {lola_msg 214 20 "Toolbar text"} "Almacenar todo"
LET Awm_ttext_85 {lola_msg 215 20 "Toolbar text"} "Almacenar pieza"
LET Awm_ttext_86 {lola_msg 216 20 "Toolbar text"} "Almacenar subpieza"
LET Awm_ttext_87 {lola_msg 217 20 "Toolbar text"} "Almacenar"
LET Awm_ttext_88 {lola_msg 218 20 "Toolbar text"} "Subpieza"
LET Awm_ttext_89 {lola_msg 219 20 "Toolbar text"} "Conf. TB"
LET Awm_ttext_90 {lola_msg 220 20 "Toolbar text"} "Conf. TB-BOM"
LET Awm_ttext_91 {lola_msg 221 20 "Toolbar text"} "Conf. TB"
LET Awm_ttext_92 {lola_msg 222 20 "Toolbar text"} "Tabla a dibujo"
LET Awm_ttext_93 {lola_msg 223 20 "Toolbar text"} "Bloque de título"
LET Awm_ttext_94 {lola_msg 224 20 "Toolbar text"} "Pieza superior"
LET Awm_ttext_95 {lola_msg 225 8 "Toolbar text"} "Superior"
LET Awm_ttext_96 {lola_msg 226 8 "Toolbar text"} "USUARIO"
LET Awm_ttext_97 {lola_msg 227 20 "Toolbar text"} "Desasignar"
LET Awm_ttext_98 {lola_msg 228 20 "Toolbar text"} "Anular reserva"
LET Awm_ttext_99 {lola_msg 229 20 "Toolbar text"} "Actualizar dibujo"
LET Awm_ttext_100 {lola_msg 230 20 "Toolbar text"} "Actualizar TB"
LET Awm_ttext_101 {lola_msg 231 20 "Toolbar text"} "Actualizar de ME10"
LET Awm_ttext_102 {lola_msg 232 8 "Toolbar text"} "Actualizar"
LET Awm_ttext_103 {lola_msg 233 8 "Toolbar text"} "Mayúscula"
LET Awm_ttext_104 {lola_msg 234 8 "Toolbar text"} "Vertical"
LET Awm_ttext_105 {lola_msg 235 20 "Toolbar text"} "WM-ID Pieza"
LET Awm_ttext_106 {lola_msg 236 8 "Toolbar text"} "WMDT"
DEFINE Awm_ttext_107
  ({lola_msg 237 20 "Toolbar text" "1 -> Width"}
   "Anchura: " + Tr_set_width_value)
END_DEFINE
LET Awm_ttext_108 {lola_msg 238 8 "Toolbar text"} "Anchura"
LET Awm_ttext_109 {lola_msg 239 20 "Toolbar text"} "Ajustar texto"
LET Awm_ttext_110 {lola_msg 240 8 "Toolbar text"} "DIN"
LET Awm_ttext_111 {lola_msg 241 8 "Toolbar text"} "ISO"

LET Awm_ttext_112 {lola_msg 242 8 "Toolbar text"} "WorkSpace"
LET Awm_ttext_113 {lola_msg 244 8 "Toolbar text"} "Buscar"
LET Awm_ttext_114  {lola_msg 248 8 "Toolbar text"} "Añadir PDF"
LET Awm_mttext_1  {lola_msg 246 15 "Menu/Toolbar text"} "Drawing Manager"

{* Toolbar Messages New User Interface (PELOOK = 2,3) *}

LET Awm_tmess_1 {lola_msg 245 80 "Toolbar text"} "ADU no se admite en interfaz de usuario Windows"

{ >>>endif }
