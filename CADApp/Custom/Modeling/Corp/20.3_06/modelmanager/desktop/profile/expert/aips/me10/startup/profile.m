{****************************************************************************}
{*                                                                          *}
{*  WorkManager Desktop - ME10-AIP                                          *}
{*                                                                          *}
{*  Profile:                << EXPERT >>                                    *}
{*                                                                          *}
{****************************************************************************}

{############################################################################}
{# Profile - Environment ####################################################}
{############################################################################}
DEFINE Awm_set_profile_dir

    IF (Wmdt_profile = '')  {* Variable set in startup.m *}
	Dms_write_error 1 1 ('Missing environment variable WMDT_PROFILE')
	CANCEL
    ELSE
	LET Wmdt_profile_dir (Wmdt_dir  + '/profile/' + (LWC Wmdt_profile))
    END_IF

END_DEFINE
Awm_set_profile_dir
DELETE_MACRO Awm_set_profile_dir

{* add search path *}
{*******************}
SEARCH ADD (Wmdt_profile_dir + '/aips/me10/custom')
       (Wmdt_profile_dir + '/aips/me10/startup')
       END

{############################################################################}
{# Enable "WorkManager Desktop" - modules                                   #}
{############################################################################}
Enable_wmdt_bom
Enable_wmdt_help

{############################################################################}
{# Enable Profile ###########################################################}
{############################################################################}
DEFINE Enable_profile

    {* load UI-Style II - macros *}
    {*****************************}
    LOAD_MACRO 'uistyle2.mac'

    { --- register load class and attributes --- }
    Awmc_ui_register_load_class Awm_dbr_stds_doc_class
    Awmc_ui_register_load_attr Awm_db_name '' Awm_msg_29
    Awmc_ui_register_load_attr Awm_db_version '' Awm_msg_32

    { --- register store attributes --- }
    Awmc_ui_register_store_attr Awm_db_name 'SCAN_PART' Awm_msg_29
    Awmc_ui_register_store_attr Awm_db_description '' Awm_msg_33

    { --- register store version attributes --- }
    Awmc_ui_register_storev_attr ('CN_' + Awm_db_description) '' Awm_msg_34

    {* Title Block - Setup:    *}
    {***************************}
    {* max. number of change note entries *}
    LET Awmc_val_max_change_notes   8

    {* partname prefix for title block parts *}
    LET Awmc_val_tbname_prefix Awm_db_stds_tbname_prefix

    {* classnames for title block setup *}
    LET Awmc_val_tb_partclass  Awm_dbr_stds_part_class
    LET Awmc_val_tb_docclass   Awm_dbr_stds_doc_class

    {* WorkManager classname for title blocks *}
    LET Awmc_val_tb_class Awm_dbr_stds_tb_class

    {* Store drawing with/without title block *}
    IF (Annotation_active)
	LET Awmc_val_store_with_tb (TRUE)
    ELSE
	LET Awmc_val_store_with_tb (FALSE)
    END_IF

    {* BOM-Setup   *}
    {***************}
    IF (Wmdt_bom_enabled)

	LET Awmc_bom_val_increment     5
	LET Awmc_bom_val_start_posnr   10

	{* LINK_SRC for copy BOM *}
	{*************************}
	LET Awm_bom_val_link_src_attr "LINK_SRC"

	{* BOM-Flag-Leaderline *}
	{***********************}
	DEFINE Awmc_bom_val_leader_line_type        SOLID       END_DEFINE
	DEFINE Awmc_bom_val_leader_line_color       CYAN        END_DEFINE
	DEFINE Awmc_bom_val_leader_arrow            DOT_TYPE    END_DEFINE
	DEFINE Awmc_bom_val_leader_arrow_size       1.5         END_DEFINE

	{* Part-Highlight-Color *}
	{************************}
	DEFINE Awmc_bom_val_part_highlight_color    MAGENTA     END_DEFINE

	{* BOM-Layouts *}
	{***************}
	LET Awmc_bom_val_layout_direction   'UPPER'
	LET Awmc_bom_val_layout_ref_pos     'LOWER_RIGHT'

	{* WM-Document Class for flags and layouts *}
	{*******************************************}
	LET Awmc_bom_val_bfg_class      Awm_dbr_stds_bom_flag_class
	LET Awm_bom_val_flag_layout_a   Awm_db_stds_bom_flag_a
	LET Awm_bom_val_flag_layout_b   Awm_db_stds_bom_flag_b
	LET Awm_bom_val_flag_layout_c   Awm_db_stds_bom_flag_c
	LET Awm_bom_val_bom_layout_a    Awm_db_stds_bom_lay_a
	LET Awm_bom_val_bom_layout_b    Awm_db_stds_bom_lay_b
	LET Awm_bom_val_bom_layout_c    Awm_db_stds_bom_lay_c

    END_IF

    {* Menu-Setup *}
    {**************}
    LET Awmc_file_menu_level            2
    LET Awmc_tbf_menu_level             1
    LET Awmc_tbf_conf_menu_level        1

    IF (Wmdt_bom_enabled)
	LET Awmc_bom_menu_level         1
	LET Awmc_flag_menu_level        1
	LET Awmc_flag_conf_menu_level   1
    ELSE
	LET Awmc_bom_menu_level         0
	LET Awmc_flag_menu_level        0
	LET Awmc_flag_conf_menu_level   0
    END_IF

END_DEFINE
Enable_profile
DELETE_MACRO Enable_profile

{***************************************************************************}
{* load Thumbnail support                                                  *}
{***************************************************************************}
INPUT (Wmdt_dir  + '/startup/thmbnl.inp')
