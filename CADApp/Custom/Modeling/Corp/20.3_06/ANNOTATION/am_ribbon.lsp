; ------------------------------------------------------------------------------
; SVN: $Id$
; TECHSOFT ribbon tab for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(oli::sd-fluentui-create-addon-context "TECHSOFT_AM_CUSTOMIZATIONS"
           :annotationRibbon t
		   :color :black)

(oli::sd-fluentui-show-ribbon-tab-in-application "TECHSOFT" "All")
(oli::sd-fluentui-add-ribbon-tab "TECHSOFT"
			:annotationRibbon t
			:userDefined t
			:show t
			:appendUtilitiesGroup nil
			:position 5
			:application "Annotation"
			:title "TECHSOFT"
			:addonContext "TECHSOFT_AM_CUSTOMIZATIONS"
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Support"
			:parent "TECHSOFT"
			:annotationRibbon t
			:userDefined t
			:show t
			:title "Support"
			:slot 0
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon t
			:userDefined t
			:label "Start Teamviewer"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Start Teamviewer")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon t
			:userDefined t
			:label "Start Netviewer"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Start Netviewer")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon t
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon t
			:userDefined t
			:label "Support by E-Mail"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Support by E-Mail")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon t
			:userDefined t
			:label "Support-Form"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Support-Form")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon t
			:userDefined t
			:label "Filecenter"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Filecenter")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Configuration"
			:parent "TECHSOFT"
			:annotationRibbon t
			:userDefined t
			:show t
			:title "Configuration"
			:slot 1
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon t
			:userDefined t
			:label "Show SDCORPCUSTOMIZEDIR"
			:availCmd '("All" "TECHSOFT" "Show SDCORPCUSTOMIZEDIR")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon t
			:userDefined t
			:label "Show SDSITECUSTOMIZEDIR"
			:availCmd '("All" "TECHSOFT" "Show SDSITECUSTOMIZEDIR")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon t
			:userDefined t
			:label "Show SDUSERCUSTOMIZEDIR"
			:availCmd '("All" "TECHSOFT" "Show SDUSERCUSTOMIZEDIR")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon t
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon t
			:userDefined t
			:label "Software Distribution Server"
			:availCmd '("All" "TECHSOFT" "Software Distribution Server")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon t
			:userDefined t
			:label "List License Servers"
			:availCmd '("All" "TECHSOFT" "List License Servers")
)
(oli::sd-fluentui-add-ribbon-group "PTC_Online_Resources"
			:parent "TECHSOFT"
			:annotationRibbon t
			:userDefined t
			:show t
			:title "PTC Online Resources"
			:slot 2
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "PTC_Online_Resources")
			:annotationRibbon t
			:userDefined t
			:label "Online Documentation"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "PTC Online Documentation")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "PTC_Online_Resources")
			:annotationRibbon t
			:userDefined t
			:label "Online Tutorials"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "PTC Online Tutorials")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Design"
			:parent "TECHSOFT"
			:annotationRibbon t
			:userDefined t
			:show t
			:title "Design"
			:slot 3
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon t
			:userDefined t
			:label "Creo"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Creo Design")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon t
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon t
			:userDefined t
			:label "Silver"
			:availCmd '("All" "TECHSOFT" "Silver Design")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon t
			:userDefined t
			:label "Blue"
			:availCmd '("All" "TECHSOFT" "Blue Design")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon t
			:userDefined t
			:label "Black"
			:availCmd '("All" "TECHSOFT" "Black Design")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Color_Scheme"
			:parent "TECHSOFT"
			:annotationRibbon t
			:userDefined t
			:show t
			:title "Color Scheme"
			:slot 4
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Color_Scheme")
			:annotationRibbon t
			:userDefined t
			:label "Switch"
			:largeImage t			
			:availCmd '("Annotation" "TECHSOFT" "Switch Annotation Color Scheme")
)
(oli::sd-fluentui-set-tab-order-for-app "All" '("SDAPPLICATION" "SDHOME" "SDSTRUCTURE" "SDANALYSIS" "SDVIEW" "TECHSOFT" ))
