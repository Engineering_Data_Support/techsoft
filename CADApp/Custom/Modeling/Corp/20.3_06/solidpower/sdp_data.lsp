;; Automatically written on 01-Sep-2015 14:50:57
;; Module SOLIDPOWER
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "SOLIDPOWER")
(persistent-data
 :key "SOLIDPOWER_SD_BOM"
 :value '( :MASS_ATTRIBUTE "GEWICHT" :MASS_UNITS :KG :MASS_DIGITS 4 
           :MASS_TOLERANCE 0.1 :ROUGH_MACHINED_DIM 2 :ROUGH_MACHINED_ATTRIBUTE1 
           "ROHMASZ" :ROUGH_MACHINED_ATTRIBUTE2 NIL :ROUGH_MACHINED_ATTRIBUTE3 
           NIL :ROUGH_MACHINED_UNITS :MM :ROUGH_MACHINED_DIGITS 0 
           :ROUGH_ADD_VALUE 0 :DENSITY_TOLERANCE 0.1 ) )
(persistent-data
 :key "TS-EINSTICH-DIALOG"
 :value '( :VARIANT-VALUES ( 471 ( :FORM :T1 ) ) ) )           
(persistent-data
 :key "SOLIDPOWER_OTHER_THREAD"
 :value '( :THREAD_TYPE "whitworth" ) )          
 (persistent-data
 :key "TS-ZEBO-332-B-FL-DIALOG"
 :value '( :VALUES ( :D1 6.2999999999999998 ) ) )                
(persistent-data
 :key "LAST_DRILL_UPPER_SINK_STANDARD"
 :value '( :MITTEL_0 "Keine" :GROB_1 "Keine" :FEIN_1 "H1" :MITTEL_1 "Keine" ) )
(persistent-data
 :key "SOLIDPOWER_TOOTH_SHAFTS_5480"
 :value '( :MODULE 2 :TEETH 28 :X_M 0.9 :MANUFACTORING_SHAFT 1 
           :MANUFACTORING_HUB 1 :CUTTER_DIAM 50 :MODULE 2 :CUTTER_LENGTH 30 ) )
(persistent-data
 :key "SOLIDPOWER_TOOTH_SHAFTS_5481"
 :value '( :SHORT_SYMBOL "55x60" :CIRCLE_DIAM 57.5 :OUTER_DIAM_SHAFT 
           59.659999999999997 :INNER_DIAM_HUB 45.479999999999997 :TEETH 42 
           :BLEND_RADIUS_SHAFT 0.5 :BLEND_RADIUS_HUB 0.6 ) )
(persistent-data
 :key "SOLIDPOWER_STANDARD_PARTS"
 :value '( :DEFAULT_MATERIAL "Stahl" :MESSAGE_UNKNOWN_MATERIAL NIL :EXACT_MODELS 
           NIL :STANDARDS "DIN+DIN-EN" ) )
(persistent-data
 :key "SOLIDPOWER_OTHER_THREAD_DESC"
 :value '( :THREAD_DESC "R 1/8" ) )
(persistent-data
 :key "SOLIDPOWER_TOOTH_SHAFTS_14"
 :value '( :SHORT_SYMBOL "8x46x50" :INNER_DIAM 46 :OUTER_DIAM 50 :NUMBER 8 
           :WIDTH 9 ) )
(persistent-data
 :key "TS-STEEL-NEW-BALUSTRADE"
 :value '( :VALUES ( :L2_R 0 :L1_R 0 :L2_L 0 :L1_L 0 :LMAX 800 :HEIGHT 1105 ) ) )
(persistent-data
 :key "SOLIDPOWER_STEEL_STAIR"
 :value '( :HEIGHT 1640 :LENGTH 1954.4758918545044 :ANGLE 0.6981317007977318 
           :UPPER-OFFSET -120 :LOWER-OFFSET -10 :NUMBER-OFSTEPS 9 :PITCH 153 
           :PITCH1 153 :PITCH2 153 :D 13 :OFFSET-STEP 15 :PLACEMENT :SP-W-H ) )
 (persistent-data
 :key "SOLIDPOWER_SCREWS"
 :value '( :SCREW "normteile/schrauben/sechskantschrauben/dinen_iso_4014" 
           :DIAMETER 10 :NUT1 "normteile/muttern/dinen_iso_4032" :NUT1_NAME 
           "Sechskantmutter_ISO_4032_M_10" :NUT2 "" :NUT2_NAME "" :ACC_HEAD1 
           "normteile/scheiben/din125b" :ACC_HEAD1_NAME "Scheibe_B_10_5_DIN_125" 
           :ACC_HEAD2 "" :ACC_HEAD2_NAME "" :ACC_NUT1 
           "normteile/federringe/din_127a" :ACC_NUT1_NAME 
           "Federring_DIN_127-A10_7" :ACC_NUT2 "" :ACC_NUT2_NAME "" :HOLE_D 11 
           :UPPER_SINK_D1 20 :UPPER_SINK_D2 0 :UPPER_SINK_T1 6.2500000000000009 
           :UPPER_SINK_T2 0 :UPPER_SINK_ANGLE 90 :UPPER_SINK_STANDARD "" 
           :LOWER_SINK_D1 0 :LOWER_SINK_D2 0 :LOWER_SINK_T1 9 :LOWER_SINK_T2 
           10.199999999999999 :LOWER_SINK_ANGLE 0 :LOWER_SINK_STANDARD "" 
           :UPPER_SINK_TYPE 0 :LOWER_SINK_TYPE 0 :THREAD_DESC "" :THREAD_TABLE 
           "entfaellt" :MACHINING "Mittel" ) )
(persistent-data
 :key "TS-STEEL-STAIR"
 :value '( :VALUES ( :PLATZIERUNG :SP-W-H ) ) )
(persistent-data
 :key "LAST_DRILL_LOWER_SINK_STANDARD"
 :value '( :MITTEL_5 "Keine" :MITTEL_6 "Keine" :MITTEL_10 "Keine" :GROB_0 
           "Keine" :FEIN_0 "Keine" :MITTEL_0 "Keine" ) )
 (persistent-data
 :key "TS-GEWINDE-TABELLEN-DIALOG"
 :value '( :VALUES ( :THREAD-TYPE "trapez" ) ) )
(persistent-data
 :key "SOLIDPOWER_KEYS"
 :value '( :CURR_KEY "normteile/passfedern/dn6885form_a" :STANDARD "DIN+DIN-EN" ) )
(persistent-data
 :key "TS-STEEL-NEW-BEAM-ASSY-NORMAL-FACES"
 :value '( :VALUES ( :LAENGE 491 ) ) )
(persistent-data
 :key "SOLIDPOWER_LAST_STANDARD_PART"
 :value '( :TYP "PARAMETER" :PARA1 "normteile/profile/rechteck_profile/DIN1017" 
           :PARA2 "0000000000" :PARA3 
           " 'NAME' 'PNR' 'TYPE' 'BEZEICHNUNG' 'B' 'H' 'DESCRIPTION_ENGLISH' 'DELETE' 'FAVORITE'" 
           :PARA4 
           " 'DIN_1017_35x5' '00002038' 'DIN 1017 35x5' 'Stahlbauprofil DIN 1017 35x5' '35' '5' 'DIN 1017 35x5' '' ''" 
           :PARA5 " 'L'" :PARA6 " '230'" ) )
(persistent-data
 :key "TS-GEWFREI-FREIST-BOLZEN-DIALOG"
 :value '( :VALUES ( :BEZ "M3" ) ) )
 (persistent-data
 :key "SOLIDPOWER_STEEL_BALUSTRADE"
 :value '( :DEFAULT_OBJ1 "normteile/profile/rohre_profile/dinen10216-1" 
           :DEFAULT_NAME1 "Rohr_EN_10216-1_42.4x5.6" :DEFAULT_OBJ2 
           "normteile/profile/rohre_profile/dinen10216-1" :DEFAULT_NAME2 
           "Rohr_EN_10216-1_42.4x5.6" :DEFAULT_OBJ3 
           "normteile/profile/rohre_profile/dinen10216-1" :DEFAULT_NAME3 
           "Rohr_EN_10216-1_26.9x2.3" :DEFAULT_NAME4 NIL ) )
(persistent-data
 :key "SOLIDPOWER_DRILL"
 :value '( :SELECT_BY T :DIA 11 :SCREW_DIA "M8" :MACHINING "Mittel" :UPPER_TYPE 
           0 :TECH_STANDARD "Keine" :D1 13 :D2 0 :DEPTH1 4 :DEPTH2 0 :ANGLE 90 
           :LOWER_TYPE 0 :TECH_STANDARD_LOWER "Keine" :D1_LOWER 13 :D2_LOWER 0 
           :DEPTH1_LOWER 24 :DEPTH2_LOWER 28.799999999999997 :ANGLE_LOWER 90 
           :THREAD "" :THREAD_TYPE "Entfällt" :THREAD_HAND "Rechtsgewinde" 
           :TOLERANCE_TYPE "NONE" :TOLERANCE_STR "H12" :TOLERANCE_VAL1 0.1 
           :TOLERANCE_VAL2 0.1 :TOLERANCE_VAL3 -0.1 :CAM_INFO NIL ) )
 (persistent-data
 :key "SOLIDPOWER_AM_BOMAUTO"
 :value '( :CURR_BOM_FLAG_LAYOUT "ISO" :ALIGN_DISTANCE 10 :ALIGN_CHANGE_POS T 
           :RASTER_X 10 :RASTER_Y 10 :RASTER_UPPER_POINTS T :RASTER_LOWER_POINTS 
           NIL :RASTER_RIGHT_POINTS T :RASTER_LEFT_POINTS NIL :KEEP_RASTER T 
           :AUTO_CHANGE_POS T ) )
(persistent-data
 :key "TS-ZEBO-332-D-FL-DIALOG"
 :value '( :VALUES ( :D1 24 ) ) )

;; ----------- end of file -----------
