; ------------------------------------------------------------------------------
; SVN:  $Id: cfg_data.lsp 38125 2014-07-03 10:03:33Z pjahn $
; persistent data customization file for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "CONFIGURATION")

;;avoid/allow saving of persistent data=====================
(persistent-data
 :key "SAVE-PERSISTENT-SETTINGS"
 :value T)
