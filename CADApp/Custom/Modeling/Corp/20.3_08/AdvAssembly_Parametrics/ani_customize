﻿; -*-Lisp-*-
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Customization File for OneSpace Designer Animation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This file can be used to specify specific customizations which are 
;; executed on startup of Animation
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Some general notes on customizing:
;;
;;   Please copy this file to a corp, site or personal user customization
;;   directory to make any adaptations.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; defines the default duration of a new inserted control position
(anim:anim-define-duration-used-for-inserting-new-control-positions 1) ;; time in seconds

;; defines the proposals offered in UI to manipulate 
;; 1. the speed used within the real time mode of PLAY ANIMATION
;; 2. the frame rate used for saving an avi file within PLAY ANIMATION
(anim:anim-define-proposals :speed-factors    (list 25 50 100 125 150 200)
                            :frame-numbers    (list 5 10 15 20 25))

;; customize the AVI codecs available for save animation (codec must also be installed on PC)
;;  First string is the 4 character code (i.e. "MPEG") - second any description
;(anim-add-avi-codec FourCharacterCode Description)
(anim:anim-clear-avi-codecs)
(anim:anim-add-avi-codec "MSVC" "Microsoft Video")
(anim:anim-add-avi-codec "MPG4" "Microsoft Mpeg4")
(anim:anim-add-avi-codec "MP42" "Microsoft Mpeg4")
(anim:anim-add-avi-codec "CVID" "Supermac - Cinepak")
;;(anim:anim-add-avi-codec "IV50" "Indeo 5.0")
;;(anim:anim-add-avi-codec "IV32" "Indeo 3.2")
;;(anim:anim-add-avi-codec "DIVX" "DivX")
(anim:anim-add-avi-codec "VP62" "VP62 Extra Sharp")
(anim:anim-add-avi-codec "TSCC" "TechSmith Camtasia")
(anim:anim-add-avi-codec "DIB " "Uncompressed")
(anim:anim-add-avi-codec "XVID" "XviD")
(anim:anim-add-avi-codec "3IV2" "3ivx D4")

;; customize the range of screen resolutions available when saving an animation
;;  the values for x and y are passed throug and the input is a keyword
;(anim:anim-add-avi-screen-size :width width :height height)
(anim:anim-clear-avi-screen-sizes)
(anim:anim-add-avi-screen-size :width 1280 :height 1024)
(anim:anim-add-avi-screen-size :width 1152 :height  896)
(anim:anim-add-avi-screen-size :width 1024 :height  768)
(anim:anim-add-avi-screen-size :width  800 :height  608)
(anim:anim-add-avi-screen-size :width  640 :height  480)
(anim:anim-add-avi-screen-size :width  320 :height  240)
(anim:anim-add-avi-screen-size :width  160 :height  128)
(anim:anim-add-avi-screen-size :width    0 :height    0)   ;;Current Vport

;; customize the range of accuracy factors used within the analyze mode of PLAY ANIMATION
;; First number defines the logarithm of the steps used
;; Second string assign a name to it
;(anim-add-play-accuracy number string)
(anim:anim-clear-play-accuracy)
(anim:anim-add-play-accuracy  2 "High")
(anim:anim-add-play-accuracy  0 "Normal")
(anim:anim-add-play-accuracy -2 "Low")
