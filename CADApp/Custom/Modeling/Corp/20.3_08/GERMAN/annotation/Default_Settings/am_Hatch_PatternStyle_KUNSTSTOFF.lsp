;% Automatically written on 03/21/2007 at 15:27:08
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :KUNSTSTOFF
  :title "Kunststoff"
  :values '("Description" "L:2, A: 45.0, D:   5.0"
            "PatternDistance" 5.0
            "SubPattern" (:LABEL "Kunststoff" :SUBPATTERN
                                 ((:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET 0 :ANGLE
                                          0.0 :WIDTH 0)
                                  (:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET 0 :ANGLE
                                          1.5707963267948966 :WIDTH 0)))
            "PatternAngle" 0.78539816339744828
            )
)
