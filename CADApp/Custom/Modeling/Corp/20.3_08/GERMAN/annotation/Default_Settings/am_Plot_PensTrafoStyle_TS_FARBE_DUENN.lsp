;% Automatically written on 03/14/2007 at 11:47:21
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot/PensTrafoStyle"
  :style :TS_FARBE_DUENN
  :title "TS_Farbe_duenn"
  :values '("Contents" :DRAWING
            "Description" "Farbdruck dünn"
            "Pens" (:LABEL "comment" :PENLIST
                           ((:OLD_LINETYPE :ALL :COLOR_DEF :ALL :COLOR1
                                0.0,0.0,0.0 :COLOR2
                                1.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.25 :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,0.0,0.0 :COLOR2
                                0.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE :OFF :PEN_NUMBER 0)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                1.0,0.0,0.0 :COLOR2
                                1.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.17000000000000001
                                :PEN_NUMBER 2)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,1.0,0.0 :COLOR2
                                0.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.17000000000000001
                                :PEN_NUMBER 3)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                1.0,1.0,0.0 :COLOR2
                                1.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.14999999999999999
                                :PEN_NUMBER 4)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,0.0,1.0 :COLOR2
                                0.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.10000000000000002
                                :PEN_NUMBER 5)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                1.0,0.0,1.0 :COLOR2
                                1.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.10000000000000002
                                :PEN_NUMBER 6)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,1.0,1.0 :COLOR2
                                0.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.10000000000000002
                                :PEN_NUMBER 7)
                            (:OLD_LINETYPE :PHANTOM :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,1.0,1.0 :COLOR2
                                0.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE :OFF :PEN_NUMBER 0)))
            "TrueColorMode" LISP::NIL
            )
)
