﻿; ------------------------------------------------------------------------------
; SVN: $Id: sha_demoshop.lsp 38153 2014-07-04 12:22:54Z pjahn $
; customization file for creo elements/direct sheet metal 18
; Wolfgang Hofer; Techsoft Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :sheet-advisor)

;==============================================================================;
; ATTENTION:                                                                   ;
;                                                                              ;
; Any tool entries, allowance values and check distances in this file should   ;
; be taken as examples. The exactness of this data is not guaranteed.          ;
; Adaptation of this data to the customers local environment, is strongly      ;
; recommended.                                                                 ;
;                                                                              ;
;==============================================================================;
; NOTE:                                                                        ;
;                                                                              ;
; For better error messages when loading a new defined or changed database use:;
;                                                                              ;
;  (sha-tdb-load "demoshop")   instead of   (load "demoshop")                  ;
;                  ^or your filename                ^or your filename          ;
;                                                                              ;
; This function generates a protocol of the load (each sha-define-shop-table)  ;
; and shows this listing automatically in the display-box if the load will be  ;
; broken by an error.                                                          ;
; With regular (load "shopfile") or if you want to see the protocol anyhow,    ;
; you can visualize it with: (sha-display-load-protocol).                      ;
;                                                                              ;
;==============================================================================;


(sha-define-shop "Neue Herbold"

  ;; :MATERIALS         Name of material table for this shop e.g. sheet_metals
  ;; :BEND-PROCESSES    List of bend processes in this shop  e.g. air_bending
  ;; :BEND-PROCESS-180  List of bend processes specifically for bends > 180 degree
  ;; :CONE-PROCESSES    List of bend processes for conical bends
  ;; :CUT-PROCESSES     List of cut processes in this shop e.g. punch
  ;; :STAMP-PROCESSES   List of stamp processes in this shop   e.g. stamp
  ;; :BEND-RELIEFS      List of bend-relief processes in this shop
  ;; :CORNER-RELIEFS    List of corner relief processes in this shop
  ;; ============== production process settings for multi UNFOLD =============
  ;; :PRODUCTION-PROCESSES  customer definable/changeable/extendable list of
  ;;                        production processes which need special process data
  ;; :PROCESS-RELATIONS allows definition of relations between materials and
  ;;                    processes, if for example certain processes are only 
  ;;                    applicable to a limited range of materials.
  ;;                    The contents of this table is used within the 
  ;;                    :PROCESS-SELECTION-FUNCTION
  ;; :PROCESS-SELECTION-FNC  This function uses the :PROCESS-RELATION table
  ;;                         to decide which processes will be switched on by
  ;;                         by default. The function definition usually exists 
  ;;                         in the sha_demoshop_func.lsp file or the customers 
  ;;                         equivalent of that file
  ;; :DOCU-SETTINGS     default settings for documentation options
  ;; :DRAWING-SETTINGS  default settings for drawing options
  ;; :DATABASE-SETTINGS default settings for database options
  ;; :EXTERNAL-OPTIONS  further list of customer defined options for production
  ;;                    preparation
  ;; :POSTPROCESS-OPTIONS list of data for customer defined postprocess function
  ;; ======= END of production process settings for multi UNFOLD
  ;; :CHECK-DISTANCES   Name of check distance matrix for stamps and punches
  ;; :ALLOWANCE_FORMULA Fall back formula for bend allowance e.g. bend-allowance
  ;; :ALLOWANCE_TABLE   Allowance table for interpolation by fall back formula
  ;; :OFFSET_FORMULA    Fall back formula for offset allowance.
  ;; :HEM_FORMULA       Fall back formula for hem allowance.
  ;; :UP-FLAG           Text in flat drawing for UPward bends.
  ;; :DOWN-FLAG         Text in flat drawing for downward bends.
  ;; :BEND-FLAT-TEXT    Minimal text at bend line in flat drawing
  ;;                    if regular bend tool could not be found in bend tool
  ;;                    tables.
  ;;                    Following properties are allowed:
  ;;                     :RADIUS :ANGLE :ALLOWANCE :UPDOWN :OPEN-ANGLE
  ;;                    Additionally an expression may be given which might use
  ;;                    any of the above special properties.
  ;;                    e.g. (- PI :ANGLE) is equivalent to: :OPEN-ANGLE
  ;;                    Corresponding unit (:mm :deg etc.) (default units are
  ;;                     :mm and  :rad) .Units must stand before the
  ;;                     corresponding property.
  ;;                     corresponding unit (:mm :deg etc.) (default units are
  ;;                     :mm and  :rad) .Units must stand before the
  ;;                     corresponding property.
  ;;                    Numbers stay for: number of decimal places of following
  ;;                    value.
  ;;                    {#} entries in leading string refer to corresponding
  ;;                    property in following list, first property is {1}
  ;;                    Units and numbers are not counted for the {#} entries.
  ;; :OFFSET-FLAT-TEXT  Minimal text at offset bend line in flat drawing.
  ;;                    Syntax like in bend, except accepted properties are:
  ;;                    :OFFSET_HEIGHT :ALLOWANCE :UPDOWN.
  ;; :HEM-FLAT-TEXT     Minimal text at hem bend line in flat drawing.
  ;;                    Syntax like in bend, except accepted properties are:
  ;;                    :HEM_DIST :ALLOWANCE :UPDOWN.
  ;; :LOCALIZED-KEYWORDS  Special keywords used as entries in tool tables, 
  ;;                    which should be displayed as localized strings
  ;;                    e.g.:UP :DOWN (for punch and stamp) :PREF, :NONPREF
  ;; :DISABLE-SORT T    Don't show sort buttons in table header
  ;; :CONTACT           Contact person at shop             e.g. "Mr. Moore"
  ;; :PHONE             Phone number of contact person     e.g. "007126"
  ;;
  ;; implicitly defined:
  ;; :SHOP-NAME  = First parameter of sha-define-shop fnc. e.g "demoshop"

; materials
  :MATERIALS          "sheet_metals"                  ; must

; processes : 'optional'  means:
;            if not existing:
;             for bend/offset/hem/reliefs/corner-reliefs:
;                 unadvised function parameters entered by hand
;             for punch & stamp:
;                 no functionality available

  :BEND-PROCESSES     ("NeueHerbold_Biegen_Tabelle"
						"air_bending"                  ; optional

  ;;===    Addition TECHSOFT      WoHo 03.10.2011
					   
                       "Techsoft_Biegen_Formel"
					   "Techsoft_Biegen_k-Faktor"
  
                       "bend_forming"
                       "bend_folding"
                       "3pnt_bending" 
                       "special_bend")
  :BEND-PROCESS-180   ("roll_bending")                ; Bend process which supports bends with angle > 180 deg.
  :CONE-PROCESSES     ("cone_bending")                ; Bend process for conical faces
  :IMPLICIT-BEND-PROCESSES ("multibend_bend")         ; optional not interactively visible bends
  :OFFSET-PROCESSES   ("offsets"                      ; optional
                       "special_offset")
  :HEM-PROCESSES      ("hems"                         ; optional
                       "special_hem")
  :CUT-PROCESSES      "punches"                       ; optional
  :STAMP-PROCESSES    "stamps"                        ; optional
  :BEND-RELIEFS       ("no_relief"
                       "rect_relief"
                       "rnd_relief"
                       "obl")                         ; optional
  :MITER-BEND-RELIEFS ("miter-chamfer"
                       "no-2d-miter-chamfer"
                       "miter-spline")
  :CORNER-RELIEFS     ("no_3D_corner_relief" 
                       "connected_v_corner_relief"
                       "connected_spline_V_corner_relief"
                       "min_perp_v_corner_relief"
                       "minimal_round_relief"
                       "rnd" 
                       "sqr_corner_relief"
                       "rnd_corner_relief" ; using replacement macros
                       "no_corner_relief"  ; using replacement macros
                       "no_2D_spline_v_corner_relief" ; using replacement macros
                      )
  :PRODUCTION-PROCESSES ("flame_cutting" 
                         "laser_cutting"
                         "nibbling"    
                         "punching"
                         "stamping"
                         "bending" 
                         "welding"
                        )
  :PROCESS-RELATIONS    "process_relations" 
  :PROCESS-SELECTION-FNC sh-needed-shop-processes 
  :DOCU-SETTINGS        "documentation-settings"
  :DRAWING-SETTINGS     "drawing-settings"  
  :DATABASE-SETTINGS    "database-settings"    
  :EXTERNAL-OPTIONS    ("chamfer-option" ; example for customer definable 3D data preparation
                        "change-color"   ; example for customer definable 2D data preparation
                       )
  :POSTPROCESS-SETTINGS "postprocess-settings"

  :MIN-RELIEF-FALLBACK (:KEEP-ATTRIBUTES T
                        :TOOL (:TABNAME "min_perp_v_corner_relief"
                               :ROWKEY (:MIN_WIDTH 1 :MIN_LENGTH 3
                                        :MIN_ANGLE 0.17453292519943295)))
  :CHECK-DISTANCES    "check_distances"               ; optional

; fall back strategy for allowance values
  :ALLOWANCE_FORMULA sh_bend_allowance_din            ; optional
; following two lines may be used as alternative way to calculate the bend
; allowance, it implements a linear interpolation of an allowance table
; :ALLOWANCE_FORMULA linear_interp                    ; optional
; :ALLOWANCE_TABLE    "general_allowances"            ; optional
  :OFFSET_FORMULA    sh_offset_allowance              ; optional (default = equal to :ALLOWANCE_FORMULA)
  :HEM_FORMULA       sh_hem_allowance                 ; optional (default = equal to :ALLOWANCE_FORMULA)
  :CONE_BEND_FORMULA cone_bending_allow_din           ; optional (default = equal to :ALLOWANCE_FORMULA)

  :K-FACTOR-NORM     :DIN  ;; [:DIN | :ANSI]          ; For cone process, k-factor is used with this norm
  
  :UP-FLAG             "AUF"                           ;optional
  :DOWN-FLAG           "AB"                         ;optional
  :BEND-FLAT-TEXT      ("R={1}mm W={2}° Ez={3}mm {4}" :RADIUS :DEG :ANGLE 1 :ALLOWANCE :UPDOWN)
; alternative using open-angle of bend
; :BEND-FLAT-TEXT      ("R={1}mm OA={2}° Al={3}mm {4}" :RADIUS :DEG :OPEN-ANGLE 1 :ALLOWANCE :UPDOWN )
; alternative using open-angle of bend calculated via expression
; :BEND-FLAT-TEXT      ("R={1}mm OA={2}° Al={3}mm {4}" :RADIUS :DEG (- PI :ANGLE) 1 :ALLOWANCE :UPDOWN )
  :OFFSET-FLAT-TEXT    ("Z-Biegung H={1}mm Ez={2}mm" :OFFSET_HEIGHT 1 :ALLOWANCE )
  :HEM-FLAT-TEXT       ("180Grad Abst={1}mm Ez={2}mm {3}" :HEM_DIST 1 :ALLOWANCE :UPDOWN)
  :CONE-BEND-FLAT-TEXT ("Kegelwinkel={1}° Winkel={2}° K-Faktor={3} {4}" :DEG 1 :CONE_ANGLE :DEG 1 :ANGLE :K_FACTOR :UPDOWN)
  :LOCALIZED-KEYWORDS  (:UP "AUF"
                        :DOWN "AB"
                        :PREF "VORZUG" 
                        :NONPREF "NICHTVORZUG") 

  :DISABLE-SORT T
; further customer definable and optional entries
  :CONTACT            "Mr.Moore"                      ; optional
  :PHONE              "007126"                        ; optional
  :BROWSER-ICON    "icon_shop"                        ; optional
)

;============================================================================
; Structure of shop table
;                                                      element type
;                                                     --------------------
;(sha-define-shop-table "table_name"                 ; Any string (identifier,
;                                                    ;  should not be localized
;                                                    ;  for international usage)
;   :TABLE-TITLE "table name"                        ; Any string (localizable)
;                                                    ;  visible in UI
;   :COLUMNS (<:col1> ... <:coln>)                   ; Any keyword (:xxx)
;   :COLUMNS-NAMES (<"col-name1"> ... <"col-namen">) ; Any string (localizable)
;   :UNITS   (<:unit1> ... <:unitn>)                 ; Unit keyword or nil
;   :CONTENTS
;       ((<a11> <a11> .. <a1n>)                      ; type corresponding
;        (<a21> <a21> .. <a2n>)                      ; to column unit
;
;        (<am1> <am1> .. <amn>))
;   :KEY <:coli> or                    ; Optional, default first column.
;        (<:coli>...<:colj>)           ;  In a single shop this could be a
;                                      ;  unique toold_id. If different shops
;                                      ;  may be used, the key should be a
;                                      ;  unique combination of geometric
;                                      ;  parameters which can be found also in
;                                      ;  the other shops
;   :DISPLAY (<:coli>..<:colj>)        ; Optional, default all columns
;   :FLAT-TEXT ("string" <colm>..<coln>);Optional; columns to be put in flat
;                                      ;  drawing as text beside bend-line.
;                                      ;  {#} in string refer to corresponding
;                                      ;  property in following list. Properties
;                                      ;  will be given in same units as entered
;                                      ;  in table if not leaded by an explicit
;                                      ;  unit property and an optional number
;                                      ;  indicating the decimal places of that
;                                      ;  value. Units and numbers are not
;                                      ;  counted for {#} entries.
;                                      ;  Additionally to all columns of the
;                                      ;  table the special properties
;                                      ;  :ANGLE and :OPEN-ANGLE (for bends
;                                      ;                          only)
;                                      ;  :ALLOWANCE (for bends, offsets and
;                                      ;              hems) 
;                                      ;  :UPDOWN (for bends, offsets and hems)
;                                      ;  are accepted.
;                                      ;  Another option would be the usage of
;                                      ;  a lisp expression. within such
;                                      ;  expression all kewords which are
;                                      ;  identical to one of the above
;                                      ;  mentioned columns or special
;                                      ;  properties are replace d by the values
;                                      ;  of those properties.
;                                      ;  e.g.:  (- PI :ANGLE) is equivalent to
;                                      ;         :OPEN-ANGLE
;                                      ;  No text can forced by: :FLAT-TEXT ()
;   :FLAT-INFO ("string" <colm>..<coln>);Optional; Syntax like FLAT-TEXT (see
;                                      ;  above) but result attached to 2D    
;                                      ;  geometry as info text, for follow up
;                                      ;  processes (e.g. NC programs)
;   :MENU-ENTRY                        ;;Optional; Syntax like FLAT-TEXT (see
;                                      ; above), result string is shown in
;                                      ; Menu as name of selected tool
;                                      ; e.g. :MENU-ENTRY ("D{1}" :DIA)
;   :REPLACEMENT-INFO ("string" <colm>..<coln>);Optional; Syntax like FLAT-TEXT
;                                      ; (see above). String contains Drafting macro
;                                      ; name and optional parameters which are
;                                      ; called to replace or delete the
;                                      ; associated feature geometry in the
;                                      ; flat drawing.
;   :USABLE-AS-RELIEF                  ; Indicator that tool can be used as
;                                      ; relief or corner relief
;      (:WIDTH        e.g :VERT        ; width column for relief
;       :LENGTH       e.g.:HORIZ       ; optional: length column for relief
;                                      ; (default:
;                                      ; :LENGTH t = dynamic adapted tool length
;                                      ;           => no size check )
;       :DEPTH        e.g. :MIN_LENGTH ; optional: depth column for relief
;                                      ; (default: 0)
;       :LENGTH_X_ANGLE e.g. (:DEG 90) ; optional: rot angle when bend is in
;                                      ; X_POS (default: 90 deg)
;       :ROTATE-WITH-CORNER t          ; optional: rotate relief with
;                                      ; bend-angle / 2  (default:  nil)
;      )                               ;
;                                      ; :USABLE-AS-RELIEF .. together with
;                                      ; keyword :MENU-ENTRY replace the old
;                                      ; keywords :USABLE-AS-CORNER-RELIEF,
;                                      ; :USABLE-AS-ROUND-RELIEF and
;                                      ; :USABLE-AS-RECTANGULAR-RELIEF
;   :DEFAULT-FUNCTION                  ; This keyword followed by a function
;                                      ; name indicates that the default tool
;                                      ; should be defined by this function 
;                                      ; (e.g. Material dependent) instead of 
;                                      ; taking the global default setting.
;   :HELP "help-page-name"             ; Optional; name of help page (string)
;   :DISABLE-SORT T                    ; Optional; Don't show sort buttons
;                                      ;  in table header
;   :SORT-OFF T                        ; Optional; Default: Sort ON. 
;                                      ;  Switch default to: Sort OFF
;   :SORT-BY <:coli>                   ; Optional; Default first column.
; or                                   ;  Set default sort column to <:coli>.
;   :SORT-BY (<:coli> :REVERSE)        ;  For reverse sort use: list entry
;                                      ;  with column keyword plus :REVERSE.
;   :BROWSER-ICON                      ; the name of a pixmap file (without
;                                      ; extension .pm) to be used in the 
;                                      ; databse BROWSER
;   <:colj> <column contents>          ; Optional; A common column entry valid
;                                      ;  for all tools in the table. 
;                                      ;  Attention! This abbreviated entry of
;                                      ;  identical column entries cannot have
;                                      ;  a unit and can not be displayed .
;                                      ;  It can hold strings, symbols (e.g.lisp
;                                      ;  function names) or unit free numbers
;                                      ;  e.g. :PROF_FNC in punch and stamp
;   <:property1> <any entry>           ; Application specific property
;)
; NOTE: Special column entries are the :MIN_THICK and :MAX_THICK columns
;       which hold the minimal and maximal material thickness values for which
;       the belonging tool could be used. Both columns or one of it may be
;       given. If both are missing, no checks against thickness will be done.
;
; NOTE: Another special column entry is the :ADVICE column. It will be 
;       dynamically filled when a table will be popped up. It holds the results
;       of the advisor legality checks of the tool belonging to it..       
;       The column is optional, if it's missing, no advisor check will 
;       be done on that table.
;
; NOTE: If a table should be extended, at least the columns listed in the 
;       :KEY must have valid entries.
;============================================================================


;;===    Addition TECHSOFT      WoHo 03.10.2011
;;===    Sheets and handlers
 
(sha-define-shop-table "sheet_metals"       

  :TABLE-TITLE "Bleche"

  ;; :MAT_ID       Ident. number/string of the material   e.g. "7144-0503"
  ;; :MATERIAL     material description                   e.g. "UST 1405" 
  ;; :THICK        material thickness                     e.g. 0.88      
  ;; :TENSILE-STRENGTH material tensile strength          e.g. 280
  ;; :MIN_BEND_RAD minimum bending radius                 e.g. 0.4 
  ;; :DENSITY      material density                       e.g  7.8
  ;; :COLOR        part color (representing material)     e.g  10066329
  ;;               Color values can be calculated for example with:
  ;;               (rgb-color 1 0 0)    = red 

 :COLUMNS 
   (:MAT_ID    :MATERIAL  :THICK :K_FACTOR   :MIN_BEND_RAD    :DENSITY :COLOR)
 :COLUMN-NAMES 
   ("Mat-ID"   "Material" "Dicke" "k-Faktor" "MinBiegeradius" "Dichte" "Farbe")
 :UNITS 
   (nil        nil        :MM    nil         :MM              :G/CM^3   nil)
 :CONTENTS
  (

; === If a SolidPower material should be assigned,
; === enter a SolidPower material in the first colum.
; === Also add the handler in the column :HANDLE sha-part-creation-handler-solidpower
; === or handler :HANDLE sha-part-creation-handler-solidpower-ohne-meldung (to avoid messages)
; === example.  
;  ("ST45"     "Stahl"     1.00   0.45       1.00             7.86     10066329 )
;  ("ST45"     "Stahl"     1.25   0.45       1.25             7.86     10066329 )
;  ("ST45"     "Stahl"     1.50   0.45       1.50             7.86     10066329 )

; === The density and color is only applied with :HANDLE sha-part-creation-handler .
; === Column :K_FACTOR is only relevant if you are using process Techsoft_biegen_k-Faktor .

   ;("00-001-020"  "Stahl"     1.00   0.45       1.00             7.86     10066329 )
   ;("00-001-020"  "Stahl"     1.25   0.45       1.25             7.86     10066329 )
   ;("00-001-020"  "Stahl"     1.50   0.45       1.50             7.86     10066329 )
   ;("00-001-020"  "Stahl"     2.50   0.48       2.50             7.86     10066329 )
   ;("00-001-020"  "Stahl"     3.00   0.48       3.00             7.86     10066329 )
   ;("00-001-020"  "Stahl"     5.00   0.50       5.00             7.86     10066329 )
   ("00-001-020"  "Stahl"     2.00   0.45       2.00             7.86     10066329 )
   ("00-001-030"  "Stahl"     3.00   0.45       3.00             7.86     10066329 )
   ("00-001-040"  "Stahl"     4.00   0.50       4.00             7.86     10066329 )
   ("00-001-100"  "Stahl"     6.00   0.50       6.00             7.86     10066329 )
   ("00-001-108"  "Stahl"     8.00   0.50       8.00             7.86     10066329 )
   ("00-001-111"  "Stahl"     10.00   0.50      8.00             7.86     10066329 )
   
   ("02-0010"  "Niro"      1.00   0.42       1.00             7.86     12040119 )
   ("02-0020"  "Niro"      1.50   0.42       1.50             7.86     12040119 )
   ("02-0030"  "Niro"      2.00   0.42       2.00             7.86     12040119 )
   ("02-0040"  "Niro"      3.00   0.42       3.00             7.86     12040119 )
   
   ("03-0010"  "Alu"       1.00   0.40       1.00             2.7      4700151 )
   ("03-0020"  "Alu"       1.50   0.40       1.50             2.7      4700151 )
   ("03-0030"  "Alu"       2.00   0.40       2.00             2.7      4700151 )
   ("03-0040"  "Alu"       3.00   0.40       3.00             2.7      4700151 )
   ("03-0050"  "Alu"       4.00   0.40       4.00             2.7      4700151 )   

  )

 :KEY (:MATERIAL :THICK)         ; The material key MUST not be changed
                                 ; A FLAT-TEXT info is not used
 :DISPLAY (:MAT_ID :MATERIAL :THICK)								 
 :HELP  "sha_sheet_metals"
 :BROWSER-ICON  "icon_shmetals"
 :HANDLE sha-part-creation-handler
 ;:HANDLE sha-part-creation-handler-solidpower
 ;:HANDLE sha-part-creation-handler-solidpower-ohne-meldung
)

;============================================================================
; special entries in bend process tables
; ( | = OR )
;
;(sha-define-shop-table "bend-process-name"
;  :columns (<col1> ... :RADIUS |     ...<col1m>. :ALLOWANCE_TABLE | ..<coln>)
;                       :RADIUS_FORMULA           :ALLOWANCE_FORMULA  
;  :columns-names s.o.
;  :units         s.o.            
;  :contents      s.o.
;
;  :DYNAMIC-COLUMN (:RADIUS :RAD_FORMULA)  ;used for dynamic calculation of
;                                          ;context dependent bend radius
;  :key           s.o.
;  :display       s.o.
;  :flat-text     s.o.
;  :help          s.o.
;
;  NOTE: Every bend process table must have a :RADIUS column.
;        The ALLOWANCE_.. entry is optional. If it is missing the allowance
;        is calculated with the formula given in the :ALLOWANCE_FORMULA entry
;        in the main shop table.
;============================================================================

(sha-define-shop-table "bend_forming"
  :TABLE-TITLE "Gesenkbiegen"
    
  :COLUMNS ( :RADIUS :ANGLE :PREFERENCE :MAX_THICK :ADVICE)
  :COLUMN-NAMES 
           ("Rad"   "Win"  "Vorzug"     "MaxDick" "Fertigungsgerecht")
  :UNITS   ( :MM     :DEG   nil         :MM         nil)
  :CONTENTS(
            (0.80    90     :PREF       10          "-")
            (0.80    30     :PREF       10          "-")
            (0.80    45     :PREF       10          "-")
            (0.80    60     :PREF       10          "-")
            (0.80    90     :PREF       10          "-")
            (1.60    90     :NONPREF    1.9         "-")
            (2.40    90     :NONPREF    2.6         "-")
           )
  :DISPLAY (:RADIUS :ANGLE :PREFERENCE :ADVICE)
  :KEY     (:RADIUS :ANGLE)
  :FLAT-TEXT ("Biegeformel Rad={1} Win={2}° {3}" :RADIUS :ANGLE :UPDOWN)
; alternatives using :OPEN-ANGLE or expression
; :FLAT-TEXT ("Bend form R={1} OAng={2} {3}" :RADIUS :DEG 3 :OPEN-ANGLE :UPDOWN)
; :FLAT-TEXT ("Bend form R={1} OAng={2} {3}" :RADIUS :DEG 3 (- PI :ANGLE) :UPDOWN)
  :ALLOWANCE_TABLE "bend_form_allow"
  :HELP  "sha_bnd_form"
  :BROWSER-ICON  "icon_bend_form"
)

(sha-define-shop-table "air_bending"
  :TABLE-TITLE "Freies Biegen"

  :COLUMNS 
    (:RADIUS :DIE_WIDTH  :PISTON_RAD :PISTON_ANG :MAX_BEND_LENGTH :MAX_BEND_FORCE :MAX_BEND_FORCE_PER_M :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("Rad"  "MatrizBrte" "KolbRad"  "KolbWin" "Max B-Länge" "Max B-Kraft" "Max B-Kraft/m" "Vorzug"  "Fertigungsgerecht")
  :UNITS   
    (:mm     :MM         :MM         :DEG        :MM :KN :KN/M nil          nil)    
  :CONTENTS (
    (0       16          0.8         85          2000 6000 250 :PREF        "-")
    (0       22          0.8         85          2000 6000 200 :PREF        "-")
    (0       35          0.8         85          2000 6000 300 :PREF        "-")
    (0       50          0.8         85          2000 6000 400 :PREF        "-")
    (0      150          0.8         85          3000 8000 800 :NONPREF     "-")
   )
  :ALLOWANCE_FORMULA air_bend_allow
  :DYNAMIC-COLUMN (:RADIUS air_bend_rad)
  :DEFAULT-FUNCTION sha-default-bend-radius
  :DISPLAY (:RADIUS :PREFERENCE :ADVICE)
  :KEY  (:DIE_WIDTH :PISTON_RAD :PISTON_ANG)
  :FLAT-TEXT ("Win={1}° Freies Biegen: MatrizBrte={2} St-Rad={3} St-Win={4}° {5}" 
             :DEG 1 :ANGLE :DIE_WIDTH :PISTON_RAD :PISTON_ANG :UPDOWN)
  :HELP  "sha_air_bend"
  :BROWSER-ICON  "icon_air_bend"
)

(sha-define-shop-table "bend_folding"
  :TABLE-TITLE "Schwenkbiegen"

  :COLUMNS
   (:RADIUS :PISTON_ANG :PREFERENCE :ADVICE)
  :COLUMN-NAMES
   ("Rad"  "KolbWin"  "Vorzug"  "Fertigungsgerecht")
  :UNITS
   (:MM    :DEG         nil         nil)
  :CONTENTS (
    (0.20  30           :PREF       "-")
    (0.40  30           :PREF       "-")
    (0.80  30           :PREF       "-")
    (1.00  30           :PREF       "-")
    (2.00  30           :PREF       "-")
    (1.60  30           :NONPREF    "-")
    (2.40  30           :NONPREF    "-")
   )
  :ALLOWANCE_FORMULA linear_interp
  :ALLOWANCE_TABLE  "Bend_fold_allow"
  :DISPLAY (:RADIUS :PREFERENCE :ADVICE)
  :KEY  (:RADIUS :PISTON_ANG)
  :FLAT-TEXT ("Win={1}° Rad={2} St-Win={3}° {4}" 
             :DEG 1 :ANGLE :RADIUS :PISTON_ANG :UPDOWN)
  :HELP  "sha_bnd_fold"
  :BROWSER-ICON  "icon_bend_fold"
)

;;===    Addition TECHSOFT      WoHo 03.10.2011
;;===    Allowance values are calculated using the DIN formula.

(sha-define-shop-table "Techsoft_Biegen_Formel"
  :TABLE-TITLE "TS Formel"

  :COLUMNS 
    (:RADIUS  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("Radius" "Vorzug"    "Fertigungsgerecht")
  :UNITS   
    (:mm      nil         nil)    
  :CONTENTS (
    (1.00     :NONPREF       "-")
    (1.25     :NONPREF       "-")
    (1.50     :NONPREF       "-")
    (2.00     :PREF       "-")
    (2.50     :NONPREF       "-")
    (3.00     :PREF       "-")
    (4.00     :PREF       "-")
    (5.00     :NONPREF       "-")
    (6.00     :PREF       "-")
    (8.00     :NONPREF       "-")
   )
  :ALLOWANCE_FORMULA air_bend_allow
;  :DYNAMIC-COLUMN (:RADIUS air_bend_rad)
  :DEFAULT-FUNCTION sha-default-bend-radius
  :DISPLAY (:RADIUS :PREFERENCE :ADVICE)
  :KEY  (:RADIUS)
  :FLAT-TEXT ("W={1}° R={2} {3}" 
             :DEG 1 :ANGLE :RADIUS :UPDOWN)
  :HELP  "sha_air_bend"
  :BROWSER-ICON  "icon_air_bend"
)


;;===    Addition TECHSOFT      WoHo 03.10.2011
;;===    Allowance values are calculated using the K-factor.

(sha-define-shop-table "Techsoft_Biegen_k-Faktor"
  :TABLE-TITLE "TS k-Faktor"

  :COLUMNS 
    (:RADIUS  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("Radius" "Vorzug"    "Fertigungsgerecht")
  :UNITS   
    (:mm      nil         nil)    
  :CONTENTS (
    (1.00     :NONPREF       "-")
    (1.25     :NONPREF       "-")
    (1.50     :NONPREF       "-")
    (2.00     :PREF       "-")
    (2.50     :NONPREF       "-")
    (3.00     :PREF       "-")
    (4.00     :PREF       "-")
    (5.00     :NONPREF       "-")
    (6.00     :PREF       "-")
    (8.00     :PREF       "-")
   )
  :ALLOWANCE_FORMULA Sh_bend_allowance_K_fact
;  :DYNAMIC-COLUMN (:RADIUS air_bend_rad)
  :DEFAULT-FUNCTION sha-default-bend-radius
  :DISPLAY (:RADIUS :PREFERENCE :ADVICE)
  :KEY  (:RADIUS)
  :FLAT-TEXT ("W={1}° R={2} {3}" 
             :DEG 1 :ANGLE :RADIUS :UPDOWN)
  :HELP  "sha_air_bend"
  :BROWSER-ICON  "icon_air_bend"
)

;;===    Addition TECHSOFT      WoHo 03.10.2011
;;===    Allowance values are calculated using the table values.

(sha-define-shop-table "NeueHerbold_Biegen_Tabelle"
  :TABLE-TITLE "NH Tabelle"

  :COLUMNS
   (:RADIUS :PREFERENCE :ADVICE)
  :COLUMN-NAMES
   ("Rad"   "Vorzug"    "Fertigungsgerecht")
  :UNITS
   (:MM     nil         nil)
  :CONTENTS (
    (1.00     :NONPREF       "-")
    (1.25     :NONPREF       "-")
    (1.50     :NONPREF       "-")
    (2.00     :PREF       "-")
    (2.50     :NONPREF       "-")
    (3.00     :PREF       "-")
    (4.00     :PREF       "-")
    (5.00     :NONPREF       "-")
    (6.00     :PREF       "-")
    (8.00     :NONPREF       "-")
   )
  :ALLOWANCE_FORMULA linear_interp
  :ALLOWANCE_TABLE  "Techsoft_Biegen_Abzugswerte_Tabelle"
  :DISPLAY (:RADIUS :PREFERENCE :ADVICE)
  :KEY  (:RADIUS)
  :FLAT-TEXT ("W={1}° R={2} {3}" 
             :DEG 1 :ANGLE :RADIUS :UPDOWN)
  :HELP  "sha_air_bend"
  :BROWSER-ICON  "icon_air_bend"
)

(sha-define-shop-table "3pnt_bending"
  :TABLE-TITLE "3Pkt-Biegen"

  :COLUMNS
   (:RADIUS :DIE_WIDTH :PISTON_RAD :PISTON_ANG :PREFERENCE :ADVICE)
  :COLUMN-NAMES
    ("Rad"  "MatrizBrte" "KolbRad" "KolbWin" "Vorzug"  "Fertigungsgerecht")
  :UNITS
    (:mm    :MM         :MM        :DEG        nil          nil)
  :CONTENTS (
   ; (0      30          0.4        45          :PREF        "-")
   ; (0      10          0.8        45          :PREF        "-")
   ; (0      20          1.6        45          :PREF        "-")
   ; (0      30          0.8        60          :PREF        "-")
   ; (0     150          2.4        45          :NONPREF     "-")
	(0       16          0.8         85        :PREF        "-")
    (0       22          0.8         85        :PREF        "-")
    (0       35          0.8         85        :PREF        "-")
    (0       50          0.8         85        :PREF        "-")
    (0      150          0.8         85        :NONPREF     "-")
	
   )
  :ALLOWANCE_FORMULA air_bend_allow
  :DYNAMIC-COLUMN (:RADIUS air_bend_rad)
  :DISPLAY (:RADIUS :PREFERENCE :ADVICE)
  :KEY  (:DIE_WIDTH :PISTON_RAD :PISTON_ANG)
  :FLAT-TEXT ("Win={1}° Freies Biegen: MatrizBrte={2} St-Rad={3} St-Win={4}° {5}" 
	      :DEG 1 :ANGLE :DIE_WIDTH :PISTON_RAD :PISTON_ANG :UPDOWN)
  :HELP  "sha_3pnt_bend"
  :BROWSER-ICON  "icon_3point_bend"
)

(sha-define-shop-table "special_bend"
;
;  Table for special bends keeping allowance of bend. This bend table may contain
;  only one (example) or even no real bend tool entry.
;  It is intended to be filled in by the designer "on the fly" during the design
;  process whenever he needs a special bend from which the bend radius and
;  allowance is known.
;  An extension to the fallback allowance formula (:ALLOWANCE_FORMULA in general
;  sha-define-shop section) must then look for the allowance in the tool key and
;  use it.
;
  :TABLE-TITLE "Spezialbiegung"

  :COLUMNS
   (:RADIUS :ANGLE :MATERIAL :THICK :ALLOWANCE)
  :COLUMN-NAMES
    ( "Radius"  "Winkel" "Material" "Dicke" "Einzugswert")
  :UNITS
    (:mm    :deg     nil          :mm    :mm)
  :CONTENTS (
    (1      90      "Steel"       1.5    -1.64)
   )
  :ALLOWANCE_FORMULA allowance_incl_fallback ; must be identical to fallback
                                           ; formula to allow correct calculation
                                           ; of tools added "on the fly" !
  :DISPLAY (:RADIUS :ANGLE :MATERIAL :THICK :ALLOWANCE)
  :KEY (:RADIUS :ANGLE :MATERIAL :THICK :ALLOWANCE)
  :FLAT-TEXT ("Spezialbiegung: Rad={1} Win={2}° {3} Einzugswert={4}" :MM 1 :RADIUS :DEG 1 :ANGLE :UPDOWN :ALLOWANCE)
  :HELP  "sha_special_bend"
  :BROWSER-ICON  "icon_air_bend"
)

(sha-define-shop-table "roll_bending"
;
;  Table for roll bending.
;  This process allows to use k-factor instead of allowance values and should be used for bends 
;  with a bend angle of more than 180 degrees.
;  The advice filter filters the lines not suitable for current material.
;
  :TABLE-TITLE "Rollen"

  :COLUMNS
   (:RADIUS :MATERIAL  :THICK      :K_FACTOR  :ADVICE)
  :COLUMN-NAMES
   ( "Radius"  "Material" "Dicke" "K-Faktor" "Fertigungsgerecht")
  :UNITS
    ( :mm     nil      :mm    nil    nil)
  :CONTENTS 
   (
    (  1.0 "AA 5052"   1.00   0.85   "-")
    (  1.0 "AA 5052"   1.60   0.85   "-")
    ( 10.0 "AA 5052"   1.60   1.03   "-")
    (100.0 "AA 5052"   1.60   1.24   "-")
    (  1.0 "UST 1203"  1.00   0.85   "-")
    (  1.0 "UST 1203"  1.25   0.76   "-")
    ( 10.0 "UST 1203"  1.25   0.94   "-")
    ( 15.0 "UST 1203"  1.25   0.97   "-")
    ( 20.0 "UST 1203"  1.25   1.01   "-")
    (100.0 "UST 1203"  1.25   1.15   "-")
    (  1.0 "UST 1203"  1.50   0.83   "-")
    (100.0 "UST 1203"  1.50   1.08   "-")
    (  1.0 "UST 1203"  2.00   0.79   "-")
    (200.0 "UST 1203"  2.00   1.05   "-")
    (  1.0 "UST 1203"  3.00   0.85   "-")
    (  1.0 "UST 1203"  5.00   0.85   "-")
    (  1.0 "UST 1203" 10.00   0.85   "-")
   )
  :ALLOWANCE_FORMULA roll_bending_allow_din
  :DISPLAY (:RADIUS :MATERIAL :THICK :K_FACTOR :ADVICE)
  :KEY (:RADIUS :MATERIAL :THICK)
  :FLAT-TEXT ("Rollen: Rad={1} Win={2}° {3} K-Faktor={4}" :MM 1 :RADIUS :DEG 1 :ANGLE :UPDOWN :K_FACTOR)
  :HELP  "sha_roll_bending"
  :BROWSER-ICON  "icon_roll_bending"
)

(sha-define-shop-table "cone_bending"
;
;  Table for cone bending.
;
  :TABLE-TITLE "Kegelbiegen"

  :COLUMNS
   (:CONE_ANGLE  :MATERIAL  :THICK      :K_FACTOR  :ADVICE)
  :COLUMN-NAMES
   ("Kegelwink" "Material" "Dicke" "K-Faktor" "Fert.gerecht")
  :UNITS
    ( :deg    nil      :mm    nil    nil)
  :CONTENTS 
   (
    ( 24.0 "AA 5052"   1.00   0.87   "-")
    (  1.0 "UST 1203"  1.00   0.85   "-")
    (  0.0 "UST 1203"  1.25   0.76   "-")
    ( 24.0 "UST 1203"  1.25   0.83   "-")
    ( 45.0 "UST 1203"  1.25   0.97   "-")
    ( 90.0 "UST 1203"  1.25   1.11   "-")
   )
  :ALLOWANCE_FORMULA cone_bending_allow_din
  :DISPLAY (:CONE_ANGLE :MATERIAL :THICK :K_FACTOR :ADVICE)
  :KEY (:CONE_ANGLE :MATERIAL :THICK)
  :FLAT-TEXT ("Kegelwinkel={1}° Winkel={2}° {3} K-Faktor={4}" :DEG 1 :CONE_ANGLE :DEG 1 :ANGLE :UPDOWN :K_FACTOR)
  :HELP  "sha_cone_bending"
  :BROWSER-ICON  "icon_cone_bending"
)

(sha-define-shop-table "multibend_bend"
;
;  Table for TRUMPF Multibend tool keeping allowance of bend.
;  An extension to the allowance formula (:ALLOWANCE_FORMULA in general
;  sha-define-shop section) must then look for the allowance in the tool key and
;  use it.
;  The TRUMPF Multibend tool is only available for the specified material thicknesses.
;  For the Sheet Metal standard material (thickness = 1.25) this tool does not exist.
;
  :TABLE-TITLE "Mehrfachbiegung"

  :COLUMNS
   (:RADIUS  :ANGLE  :THICK  :ALLOWANCE)
  :COLUMN-NAMES
    ( "Rad"  "Winkel" "Dicke"         "Einzugswert")
  :UNITS
   (:mm    :deg     :mm    :mm)
  :CONTENTS 
   (
    (1      90      1.0    -2.1)
    (1.5    90      1.5    -3.0)
    (2      90      2.0    -3.9)
   )
  :ALLOWANCE_FORMULA Sh_bend_allowance_din   ; uses allowance given in key
  :DISPLAY (:RADIUS :ANGLE :THICK :ALLOWANCE)
  :KEY (:RADIUS :ANGLE :THICK :ALLOWANCE)
  :FLAT-TEXT ("Mehrfachbiegung: Rad={1} Winkel={2}° {3} Einzugswert={4}" :MM 1 :RADIUS :DEG 1 :ANGLE :UPDOWN :ALLOWANCE)
  :HELP  "sha_bend_form"
  :BROWSER-ICON  "icon_bend_form"
)

;============================ Hems tools ======================================

(sha-define-shop-table "hems"       
 :TABLE-TITLE "180Grad-Biegungen"

 :COLUMNS 
   ( :HEM_DIST :PREFERENCE :ALLOWANCE_TABLE :MAX_THICK :MIN_LIP_LENGTH :ADVICE)
 :COLUMN-NAMES
   ("Abst"     "Vorzug"      "EinzugswTab"    "MaxDick" "MinLaschLäng" "Fertigungsgerecht")
 :UNITS 
   ( :mm       nil         nil              :mm    :mm     nil)
 :CONTENTS 
   (
    (0.01      :NONPREF    "hem_allow"      1.50   5.00    "-")
    (1.25      :PREF       "hem_allow"      1.50   7.00    "-")
    (1.50      :PREF       "hem_allow"      1.50   7.00    "-")
    (0.88      :PREF       "hem_allow"      1.6    5       "-")
    (1.00      :PREF       "hem_allow"      2.0    5       "-")
    (1.25      :PREF       "hem_allow"      2.5    6       "-")
    (1.50      :PREF       "hem_allow"      3.0    7       "-")
    (0.01      :NONPREF    "hem_allow"      1.25   5       "-")
    (1.60      :NONPREF    "hem_allow"      3.2    7       "-")
    (2.00      :NONPREF    "hem_allow"      4.0    8       "-")
    (3.00      :NONPREF    "hem_allow"      6.0    12      "-")
   )

 :DISPLAY (:HEM_DIST :PREFERENCE :ADVICE)
 :FLAT-TEXT ("180Grad-Biegung {1}mm {2}" :HEM_DIST :UPDOWN)
 :HELP "sha_hems"
 :BROWSER-ICON  "icon_hems"
)

(sha-define-shop-table "special_hem"
;
;  Table for special hem keeping allowance of hem. This hem table may contain
;  only one (example) or even no real hem tool entry.
;  It is intended to be filled in by the designer "on the fly" during the design
;  process whenever he needs a special hem from which the hem radius and
;  allowance is known.
;  An extension to the fallback allowance formula (:HEM_FORMULA in general
;  sha-define-shop section) must then look for the allowance in the tool key and
;  use it.
;
  :TABLE-TITLE "Spezielle 180-Grad-Biegung"

  :COLUMNS
   (:HEM_DIST :ALLOWANCE)
  :COLUMN-NAMES
   ("Abst" "Einzugswert")
  :UNITS
    (:mm    :mm)
  :CONTENTS (
    (1      -0.5)
   )
  :ALLOWANCE_FORMULA sh_hem_allowance ; must be identical to fallback 
                                      ; formula ( :HEM_FORMULA entry) to allow correct calculation
                                      ; of tools added "on the fly" !
  :DISPLAY (:HEM_DIST :ALLOWANCE)
  :KEY (:HEM_DIST :ALLOWANCE)
  :FLAT-TEXT ("180Grad-Biegung {1}mm {2}mm {3}" :HEM_DIST :ALLOWANCE :UPDOWN)
  :HELP  "sha_hems"
  :BROWSER-ICON  "icon_hems"
)

;============================ Offset tools ====================================

(sha-define-shop-table "offsets"       
 :TABLE-TITLE "Z-Biegungen"

 :COLUMNS 
   ( :OFFSET_HEIGHT :PREFERENCE  :ALLOWANCE_TABLE :MAX_THICK  :ADVICE)
 :COLUMN-NAMES
   ("Höhe"         "Vorzug"      "Einzugswert"      "MaxDick" "Fertigungsgerecht")
 :UNITS
   ( :mm             nil          nil              :mm        nil)
 :CONTENTS
   (
    (1.0             :PREF       "offset_allow"    1.0        "-")
    (2.0             :PREF       "offset_allow"    2.0        "-")
    (3.0             :PREF       "offset_allow"    3.5        "-")
    (4.0             :PREF       "offset_allow"    4.5        "-")	
   )
 :DISPLAY (:OFFSET_HEIGHT :PREFERENCE :ADVICE)
 :FLAT-TEXT ("Z-Biegung {1}mm" :OFFSET_HEIGHT ) 
 :HELP "sha_offsets"
 :BROWSER-ICON  "icon_offs"         
)

(sha-define-shop-table "special_offset"
;
;  Table for special offset keeping allowance of offset. This offset table may contain
;  only one (example) or even no real offset tool entry.
;  It is intended to be filled in by the designer "on the fly" during the design
;  process whenever he needs a special offset from which the offset radius and
;  allowance is known.
;  An extension to the fallback allowance formula (:ALLOWANCE_FORMULA in general
;  sha-define-shop section) must then look for the allowance in the tool key and
;  use it.
;
  :TABLE-TITLE "Spezialabstand"

  :COLUMNS
   (:OFFSET_HEIGHT :ALLOWANCE)
  :COLUMN-NAMES
   ("Höhe"   "Einzugswert")
  :UNITS
    (:mm    :mm)
  :CONTENTS (
    (1      0.2)
   )
  :ALLOWANCE_FORMULA sh_offset_allowance ; must be identical to fallback 
                                         ; formula ( :OFFSET_FORMULA entry) to allow correct calculation
                                         ; of tools added "on the fly" !
  :DISPLAY (:OFFSET_HEIGHT :ALLOWANCE)
  :KEY (:OFFSET_HEIGHT :ALLOWANCE)
  :FLAT-TEXT ("Abstand {1}mm Einzugswert {2}mm" :OFFSET_HEIGHT :ALLOWANCE ) 
  :HELP  "sha_offsets"
  :BROWSER-ICON  "icon_offs"
)
;========================= punch tools =================================
;
; special entries in punch process tables
; ( | = OR )
;
;(sha-define-shop-table "punch-process-name"
;  :table-title    are not necessary, because the are handled by the punch
;                  process types table.
;
;  :columns       s.o.
;  :columns (<col1> ... :PROF_WP | ..<coln>)  ; non parametric tools use
;                       :PROF_FNC             ; PROF_WP
;                                             ; parametric tools use PROF_FNC
;                                             ; the PROF_FNC entries are
;                                             ; usually unique, they may stay
;                                             ; as single entry at the end of
;                                             ; this table definition
;  :columns-names s.o.
;  :columns-names s.o.
;  :units         s.o.            
;  :contents      s.o.
;
;  :key           s.o.
;  :display       s.o.
;  :flat-text     s.o.
;  :help          s.o.
;
; ; unique, not displayable column entries (if not existing as column)
;  :PROF_FNC fnc-symbol       
;)
;============================================================================

(sha-define-shop-table "punches"       
 :TABLE-TITLE "Stanzwerkzeugarten"

 :COLUMNS 
   ( :TOOL_TYPE :DESCRIPTION  )          ; The :TOOL_TYPE column must exist
 :COLUMN-NAMES
   ("Werkzgart" "Beschreibung")
 :UNITS 
   ( nil       		nil   )
 :CONTENTS 
   (
  ;                       v localizable name
    ("rnd"			"Rundloch")
    ("rect_sharp"	"Rechteck scharf")
    ("rect_rnded"	"Rechteck gerundet")
    ("sqr_sharp"	"Quadrat scharf")
    ("sqr_rnded"	"Quadrat gerundet")
    ("obl"			"Langloch")
    ("d_punch"	    "D-Form")
    ("dbl_d_punch"	"Doppel-D-Form")
    ("trapez"		"Trapez")
    ("clstr_rnd"	"Rundlochgruppe")
    ("clstr_rect"	"Rechtecklochgruppe")
    ("kwy_in"		"Sicherung, Nase innen")
    ("kwy_out"		"Sicherung, Nase außen")
    ("kwy_out_rnd"	"Sicherung, Nase außen, rund")
    ("clv_leaf_1"   "Radiusklinker")
    ("clv_leaf_4"   "Radiusklinker 4")
    ("radius"       "Radius (2x)")
    ("con"          "Rasthalterung")
    ("hex"          "Hexagon")
    ("twstsfty"     "Verdrehsicherung")
    ("clstr_obl"    "Langlochgruppe")
   )
 :KEY (:TOOL_TYPE)
 :DISPLAY (:DESCRIPTION)
 :FLAT-TEXT (:TOOL_TYPE)
 :SORT-OFF T
 :HELP "sha_punches"
 :BROWSER-ICON  "icon_punches" 
)


(sha-define-shop-table "rnd"
  :TABLE-TITLE "Rundloch"

  :COLUMNS 
    (:DIA  :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :USED :ADVICE  )
  :COLUMN-NAMES 
     ("Drm" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"  "Verw"   "Fertigungsgerecht")
  :UNITS  
    ( :mm   nil       nil     :mm         :mm         nil     nil    nil)
  :CONTENTS 
    (
     (1.10  "01004"  "RW"      0          100        :PREF     "-"    "-")
     (2.00  "01012"  "CN"      0          100        :NONPREF  "-"    "-")
     (3.00  "01024"  "NE"      0          100        :PREF     "-"    "-")
     (4.00  "01042"  "EM"      0          100        :PREF     "-"    "-")
     (8.00  "01116"  "TAQ"     0          100        :NONPREF  "-"    "-")
    (10.00  "01136"  "TAU"     0          100        :PREF     "-"    "-")
    (12.70  "01158"  "TAV"     0          100        :PREF     "-"    "-")
    (20.00  "01190"  "TBQ"     0          100        :NONPREF  "-"    "-")
   ) 
   :KEY (:DIA )
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:DIA :PREFERENCE :USED :ADVICE)
   :MENU-ENTRY ("D{1}" :DIA)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_rnd"
 ; unique, not displayable column entries
   :PROF_FNC sha-rnd-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
   :BROWSER-ICON  "icon_rnd_co"
   :USABLE-AS-CORNER-RELIEF :DIA
)
  
(sha-define-shop-table "rect_sharp"
  :TABLE-TITLE "Rechteck scharf"

  :COLUMNS 
  ( :HORIZ  :VERT :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
   ("Horiz" "Vert" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS (                                                       
    :mm     :mm     nil       nil     :mm         :mm        nil     nil    nil)
  :CONTENTS 
  (	  	              	                
    (3.20    1.00  "04004"   "EA"     0           100        :NONPREF  "-"    "-")
    (8.50    1.00  "04006"   "0527"   0           100        :NONPREF  "-"    "-")
    (6.00    1.80  "04038"   "5640"   0           100        :PREF     "-"    "-")
   (19.90    1.80  "04032"   "NP"     0           100        :NONPREF  "-"    "-")
   (36.60    3.00  "04108"   "3919"   0           100        :PREF     "-"    "-")
   (66.00    3.00  "04110"   "5036"   0           100        :PREF     "-"    "-")
   (45.00   22.20  "04488"   "7251"   0           100        :NONPREF  "-"    "-")
  )
   :KEY (:HORIZ :VERT)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:HORIZ :VERT :PREFERENCE :USED :ADVICE)
   :MENU-ENTRY ("{1}x{2}" :HORIZ :VERT)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_rect_sharp"
 ; unique, not displayable column entries
   :PROF_FNC sha-rect-sharp-profile
)

(sha-define-shop-table "rect_rnded"
  :TABLE-TITLE "Rechteck gerundet"

  :COLUMNS 
  ( :HORIZ :VERT  :RADIUS :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK :PREFERENCE 
      :USED :ADVICE)
  :COLUMN-NAMES 
  ("Horiz" "Vert" "Rad" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
  ( :mm     :mm    :mm  nil       nil     :mm         :mm         nil
      nil    nil)
  :CONTENTS ( 	     	       	         
   (5.70    4.00   0.8  "02010"   "3471"  0           100        :NONPREF  
      "-"    "-")
   (9.90    6.60   0.8  "02016"   "5166"  0           100        :NONPREF  
      "-"    "-")
  (28.00   20.40   4.7  "02170"   "BAB"   0           100        :PREF     
      "-"    "-")
  (30.40   22.70   6.4  "02114"   "DAJ"   0           100        :PREF     
      "-"    "-")
  (60.30   31.80   3.2  "02136"   "3770"  0           100        :NONPREF  
      "-"    "-")
    )
   :KEY (:HORIZ :VERT :RADIUS)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:HORIZ :VERT :RADIUS :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :MENU-ENTRY ("{1}x{2} R{3}" :HORIZ :VERT :RADIUS)
   :HELP "sha_rect_rnded"
 ; unique, not displayable column entries
   :PROF_FNC sha-rect-rnded-profile
)

(sha-define-shop-table "sqr_sharp"
  :TABLE-TITLE "Quadrat scharf"

  :COLUMNS 
    (:LENGTH :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
    ("Länge" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    ( :mm     nil       nil      :mm        :mm         nil      nil    nil)
  :CONTENTS 
    ( 	     	                       
     (2.50    "03009"   "7253"   0          100         :NONPREF  "-"    "-")
     (4.00    "03010"   "CQ"     0          100         :PREF     "-"    "-")
     (4.10    "03012"   "NT"     0          100         :PREF     "-"    "-")
     (10.00   "03035"   ""       0          100         :NONPREF  "-"    "-")
    )
   :KEY (:LENGTH)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:LENGTH :PREFERENCE :USED :ADVICE)
   :MENU-ENTRY :LENGTH
   :FLAT-TEXT (:TOOL_ID)
   :SORT-OFF T
   :SORT-BY (:PREFERENCE :REVERSE)
   :HELP "sha_sqr_sharp"
   :PROF_FNC sha-sqr-sharp-profile
)
					   
(sha-define-shop-table "sqr_rnded"
  :TABLE-TITLE "Quadrat gerundet"

  :COLUMNS 
    (:LENGTH  :RADIUS :TOOL_ID :DESCR :MIN_THICK :MAX_THICK :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
    ("Länge" "Rad" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm      :mm   nil       nil     :mm         :mm         nil     nil    nil)
  :CONTENTS 
    ( 	    	                     
     (8.20   1.0  "07010"  "4787"     0          100          :PREF     "-"    "-")
     (10.00  1.6  "07030"  "3784"     0          100          :PREF     "-"    "-")
     (13.70  1.6  "07018"  "1048"     0          100          :PREF     "-"    "-")
     (14.60  2.0  "07020"  "3994"     0          100          :NONPREF  "-"    "-")
     (50.80  3.2  "07030"  "3719"     0          100          :NONPREF  "-"    "-")
    )
   :KEY (:LENGTH :RADIUS)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:LENGTH :RADIUS :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :MENU-ENTRY ("{1} R{2}" :LENGTH :RADIUS)
   :SORT-OFF T
   :HELP "sha_sqr_rnded"
 ; unique, not displayable column entries
   :PROF_FNC sha-sqr-rnded-profile
)

(sha-define-shop-table "obl"
  :TABLE-TITLE "Langloch"

  :COLUMNS 
    (:HORIZ  :VERT :TOOL_ID  :DESCR :MIN_THICK :MAX_THICK :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
    ("Horiz" "Vert" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm     :mm    nil       nil     :mm         :mm          nil   nil    nil)
  :CONTENTS 
    (  	    	                            
     ( 3.40   1.60  "08300"   "7245"  0           100         :NONPREF  "-"    "-")
     (12.70   1.60  "08012"   "6662"  0           100         :NONPREF  "-"    "-")
     ( 6.40   2.40  "08022"   "0899"  0           100         :PREF     "-"    "-")
     ( 6.40   3.20  "08042"   "QC"    0           100         :PREF     "-"    "-")
     (12.70   3.20  "08048"   "GB"    0           100         :PREF     "-"    "-")
     (12.70   6.40  "08162"   "GB1"   0           100         :PREF     "-"    "-")
     (25.40  12.70  "08242"   "GM"    0           100         :PREF     "-"    "-")
    )
   :KEY (:HORIZ :VERT)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:HORIZ :VERT :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :SORT-BY (:PREFERENCE :REVERSE)
   :HELP "sha_obl"
   :MENU-ENTRY ("{1}x{2}" :VERT :HORIZ)
   :USABLE-AS-RELIEF (:WIDTH         :VERT     ; WIDTH column for relief
                      :LENGTH        :HORIZ    ; LENGTH column for relief
                     :LENGTH_X_ANGLE (:DEG 90) ; rot angle when bend is in X_POS
                                               ; default 90 deg
                     )
 ; unique, not displayable column entries
   :PROF_FNC sha-obl-profile
   :BROWSER-ICON  "icon_obl"
)

(sha-define-shop-table "d_punch"
  :TABLE-TITLE "D-Form"
  :COLUMNS 
    (:DIA  :WIDTH  :TOOL_ID  :DESCR  :MIN_THICK :MAX_THICK  :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
    ("Drm" "Breite" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm   :mm     nil       nil     :mm         :mm         nil      nil    nil)
  :CONTENTS 
    ( 	          	                      
     ( 9.60   8.80 "09008"   "KC"    0           100         :PREF     "-"    "-")
     (12.10  11.40 "09014"   "3188"  0           100         :NONPREF  "-"    "-")
     (12.80  12.10 "09016"   "KH"    0           100         :PREF     "-"    "-")
    )
   :KEY (:DIA  :WIDTH)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:DIA  :WIDTH :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_d_punch"
 ; unique, not displayable column entries
   :PROF_FNC sha-d-punch-profile
)
						  
(sha-define-shop-table "dbl_d_punch"
  :TABLE-TITLE "Doppel-D-Form"
  :COLUMNS 
    (:DIA   :WIDTH  :TOOL_ID :DESCR :MIN_THICK :MAX_THICK :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
    ("Drm" "Breite" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm    :mm     nil       nil     :mm         :mm         nil     nil    nil)
  :CONTENTS 
    ( 	      		                  
     (30.05 21.50  "09418"   "5175"   0           100        :PREF     "-"    "-")
     (29.40 22.00  "09420"   "5044"   0           100        :NONPREF  "-"    "-")
    )
   :KEY (:DIA :WIDTH)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:DIA :WIDTH :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :SORT-BY :WIDTH 
   :HELP "sha_dbl_d_punch"
 ; unique, not displayable column entries
   :PROF_FNC sha-dbl-d-punch-profile
)
					  
(sha-define-shop-table "trapez"
  :TABLE-TITLE "Trapez"

  :COLUMNS 
    (:LENGTH  :HEIGHT  :RADIUS :ANG  :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK 
        :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
    ("Länge" "Höhe" "Rad" "Win" "WrkzgID" "Beschr" "MinDick" "MaxDick"        "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm      :mm      :mm   :deg  nil       nil     :mm         :mm       
       nil     nil    nil)
  :CONTENTS 
    (	   	              	                  
     (18.50   10.00    3.00  10.00 "09500"   "4114"  0           100        
        :PREF     "-"    "-")
     (27.20   12.70    5.10  15.00 "09506"   "3990"  0           100        
        :NONPREF  "-"    "-")
     (42.00   11.60    3.20  10.00 "09514"   "4642"  0           100        
        :PREF     "-"    "-")
    )
   :KEY (:LENGTH :HEIGHT :RADIUS :ANG)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:LENGTH :HEIGHT :RADIUS :ANG :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_trapez"
 ; unique, not displayable column entries
   :PROF_FNC sha-trapez-profile
)
						   
(sha-define-shop-table "clstr_rnd"
  :TABLE-TITLE "Rundlochgruppe"

  :COLUMNS 
    (:DIA  :ROWS  :HOLES_ROW  :DIST_X :DIST_Y :INDENT :TOOL_ID :DESCR 
       :TOOL_DIST_X :TOOL_DIST_Y   :MIN_THICK   :MAX_THICK :PREFERENCE    :USED :ADVICE)
  :COLUMN-NAMES 
    ("Drm" "Spltn" "Löch/Splt" "Abst x" "Abst y" "Einrück" "WrkzgID" "Beschr" "WrkzgAbst x" "WrkzgAbst y" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm   nil    nil         :mm     :mm     :mm     nil      nil
       :mm          :mm            :mm          :mm        nil     nil    nil)
  :CONTENTS 
    (						      	                 
     (2.4  5      4           3.60    3.12    1.80    "12026"  "6555"  
       14.40        15.60          0.88         1.25       :PREF     "-"    "-")
     (3.1  7      2           9.00    7.90    0.00    "12030" "6977"
       18.00        55.30          0.88         0.88       :NONPREF  "-"    "-")
     (4.0  2      3           5.54    4.8    -2.77    "12020" "6459"
       16.62         9.60          1.50         2.00       :PREF     "-"    "-")
     (4.0  8      1           0.00    4.8     8.30    "12016" "4784"
        5.54        38.40          0.88         2.00       :NONPREF  "-"    "-")
    )
   :KEY (:DIA :ROWS :HOLES_ROW :DIST_X :DIST_Y :INDENT)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:DIA  :ROWS  :HOLES_ROW :DIST_X :DIST_Y :INDENT 
             :TOOL_DIST_X :TOOL_DIST_Y :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_clstr_rnd"
 ; unique, not displayable column entries
   :PROF_FNC sha-clstr-rnd-profile
)
								

(sha-define-shop-table "clstr_rect"
  :TABLE-TITLE "Rechtecklochgruppe"

  :COLUMNS 
    (:HORIZ  :VERT  :ROWS  :HOLES_ROW  :DIST_X  :DIST_Y  :INDENT  :TOOL_ID 
      :DESCR  :TOOL_DIST_X  :TOOL_DIST_Y  :MIN_THICK :MAX_THICK :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
    ("Horiz" "Vert" "Spltn" "Löch/Splt" "Abst x" "Abst y" "Einrück" "WrkzgID" "Beschr" "WrkzgAbst x" "WrkzgAbst y" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm     :mm    nil    nil         :mm      :mm      nil      nil
      nil     :mm           :mm            :mm       :mm          nil    nil    nil)
  :CONTENTS 
    (  
     (5.00   50.00  1      4            10.00   0         0       "JLS"
       "clstr"  40            120            1.00      2.50      :PREF    "-"    "-")
    )
   :KEY (:HORIZ :VERT :ROWS :HOLES_ROW :DIST_X :DIST_Y :INDENT)
   :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
   :DISPLAY (:HORIZ :VERT :ROWS :HOLES_ROW :DIST_X :DIST_Y :INDENT 
             :TOOL_DIST_X  :TOOL_DIST_Y :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_clstr_rect"
 ; unique, not displayable column entries
   :PROF_FNC sha-clstr-rect-profile
)

(sha-define-shop-table "kwy_in"
  :TABLE-TITLE "Sicherung, Nase innen"

  :COLUMNS
   (:DIA  :WIDTH  :DEPTH  :ANG  :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK
      :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES
   ("Drm" "Breite" "Tiefe" "Win" "WrkzgID" "Beschr" "MinDick" "MaxDick"  "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS
   (:MM   :MM     :MM     :DEG  NIL       NIL      :MM         :MM      
      nil         nil    nil)
  :CONTENTS
  (
   (14.2  1.9     0.9     0     "09110"   "3792"   0           100
      :PREF       "-"    "-")
   (20.8  3.1     1.8     0     "09112"   "0624"   0           100
      :PREF       "-"    "-")
   (25.6  2.8     1.3     0     "09114"   "5607"   0           100
      :NONPREF    "-"    "-")
  )
  :KEY (:DIA :WIDTH :DEPTH :ANG )
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:DIA :WIDTH :DEPTH :ANG :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_kwy_in"
 ; unique, not displayable column entries
  :PROF_FNC sha-kwy_in
)

(sha-define-shop-table "kwy_out"
  :TABLE-TITLE "Sicherung, Nase außen"

  :COLUMNS
   (:DIA  :WIDTH  :LENGTH  :ANG  :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK 
      :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES
   ("Drm" "Breite" "Länge" "Win" "WrkzgID" "Beschr" "MinDick" "MaxDick"  "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm   :mm     :mm      :deg  nil       nil     :mm         :mm
      nil          nil    nil)
  :CONTENTS
  (
   (9.0   1.5     10.0     0     "09200"   "3451"  0           100
      :PREF         "-"    "-")
   (18.2  3.2     19.2     0     "09202"   "3307"  0           100
      :NONPREF      "-"    "-")
   (18.8  3.2     20.4     0     "09204"   "3047"  0           100
      :NONPREF      "-"    "-")
  )
  :KEY (:DIA  :WIDTH  :LENGTH  :ANG )
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:DIA  :WIDTH  :LENGTH  :ANG :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_kwy_out"
; unique, not displayable column entries
  :PROF_FNC sha-kwy_out
)

(sha-define-shop-table "kwy_out_rnd"
  :TABLE-TITLE "Sicherung, Nase außen, rund"

  :COLUMNS
   (:DIA  :WIDTH  :LENGTH  :TOOL_ID  :DESCR                    :MIN_THICK :MAX_THICK  :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES
   ("Drm" "Breite" "Länge" "WrkzgID" "Beschr"                    "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm   :mm     :mm      nil       nil                       :mm         :mm
        nil        nil    nil)
  :CONTENTS
  (
   (9.5   2.36    15.85    "FG"      ".375 x .093 x .624 x 0"  0.75        2.36
      :PREF        "-"    "-") ;; .25 .00 .00 .00
  )
  :KEY (:DIA  :WIDTH  :LENGTH)
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:DIA  :WIDTH  :LENGTH :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_kwy_out_rnd"
; unique, not displayable column entries
  :PROF_FNC sha-kwy_out_rnd
)

(sha-define-shop-table "clv_leaf_1"
  :TABLE-TITLE "Radiusklinker"

  :COLUMNS
   (:RADIUS :TOOL_WIDTH :SLOT_WIDTH :ANGLE  :TOOL_ID :DESCR :MIN_THICK 
      :MAX_THICK    :PREFERENCE  :USED :ADVICE  )
  :COLUMN-NAMES
   ("Rad"   "WrkzgBrte" "SchlitzBrte" "Win" "WrkzgID" "Beschr" "MinDick" "MaxDick"   "Vorzug"   "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm     :mm          :mm          :deg  nil       nil     :mm 
      :mm           nil          nil    nil)
  :CONTENTS
   (
    (1.6     4.5          2           10    "16730"   "7189"  0    
      100           :NONPREF      "-"    "-")
    (3.2     6.5          2           10    "16708"   "6695"  0
      100           :PREF         "-"    "-")
   )
  :KEY (:RADIUS :TOOL_WIDTH :SLOT_WIDTH :ANGLE)
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:RADIUS :TOOL_WIDTH :SLOT_WIDTH :ANGLE :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_clv_leaf_1"
; unique, not displayable column entries
  :PROF_FNC sha-clv_leaf_1
)
						  
(sha-define-shop-table "clv_leaf_4"
  :TABLE-TITLE "Radiusklinker 4"

  :COLUMNS
   (:RADIUS :TOOL_WIDTH  :SLOT_WIDTH  :ANGLE :TOOL_ID  :DESCR  :MIN_THICK 
      :MAX_THICK  :PREFERENCE     :USED :ADVICE)
  :COLUMN-NAMES
   ("Rad"   "WrkzgBrte" "SchlitzBrte" "Win" "WrkzgID" "Beschr" "MinDick" "MaxDick"   "Vorzug"   "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm     :mm          :mm          :deg   nil       nil     :mm
      :mm         nil          nil    nil)
  :CONTENTS
   (
    (1.6     8            3            10     "16700"   "4112"   0
      100         :PREF         "-"    "-")
    (3.2     12           3            10     "16704"   "1042"   0
      100         :PREF         "-"    "-")
    (4.8     15           3            10     "16714"   "4637"   0
      100         :NONPREF      "-"    "-")
    (7.5     22           3            10     "16718"   "4977"   0
      100         :NONPREF      "-"    "-")
    (12.7    32           3            10     "16724"   "5511"   0
      100         :NONPREF      "-"    "-")
   )
  :KEY (:RADIUS :TOOL_WIDTH  :SLOT_WIDTH  :ANGLE )
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:RADIUS :TOOL_WIDTH  :SLOT_WIDTH  :ANGLE :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_clv_leaf_4"
; unique, not displayable column entries
  :PROF_FNC sha-clv_leaf_4
)

(sha-define-shop-table "radius"
  :TABLE-TITLE "Radius (2x)"

  :COLUMNS
   (:RADIUS :SLOT_WIDTH :TOOL_ID :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE   
      :USED :ADVICE)
  :COLUMN-NAMES
   ("Rad"  "SchlitzBrte" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"    "Verw" "Fertigungsgerecht")
  :UNITS  
   ( :mm   :mm          nil      nil      :mm         :mm         nil
      nil    nil)
  :CONTENTS
  (
   (3.18   2.16         "PVG"    ""       0           100         :PREF 
      "-"    "-")
   (4.78   2.90         "PVH"    ""       0           100         :PREF 
      "-"    "-")
   (6.35   4.57         "PVF"    ""       0           100         :PREF 
      "-"    "-")
  )
  :KEY (:RADIUS :SLOT_WIDTH )
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:RADIUS :SLOT_WIDTH :PREFERENCE :USED :ADVICE) 
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_radius"
; unique, not displayable column entries
  :PROF_FNC sha-radius
)

(sha-define-shop-table "con"
  :TABLE-TITLE "Rasthalterung"

  :COLUMNS
   (:OUT_LENGTH  :OUT_WIDTH  :IN_LENGTH  :IN_WIDTH :RADIUS   :N_RAD  :TOOL_ID
      :DESCR :MIN_THICK :MAX_THICK :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES
   ("Auß_Läng" "Auß_Brte" "In_Läng" "In_Brte" "Auß_Rad" "Nas_Rad" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"      "Verw" "Fertigungsgerecht")
  :UNITS   
   (:mm          :mm         :mm         :mm        :mm      :mm     nil
      nil     :mm         :mm         nil          nil    nil)
  :CONTENTS
   (
    (35.9        13.5        27.1        8.6        0         0       "09604"
      "5894"  0           100         :PREF         "-"    "-")
    (38.9        9.5         32.6        4.0        0.4       2       "09600"
      "HQ"    0           100         :PREF         "-"    "-")
   )
  :KEY (:OUT_LENGTH  :OUT_WIDTH  :IN_LENGTH  :IN_WIDTH :RADIUS :N_RAD )
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:OUT_LENGTH :OUT_WIDTH :IN_LENGTH :IN_WIDTH :RADIUS :N_RAD 
            :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_con"
; unique, not displayable column entries
  :PROF_FNC sha-con
)

(sha-define-shop-table "hex"
  :TABLE-TITLE "Hexagon"

  :COLUMNS 
   (:DIST  :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
   ("Abst" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"    "Verw"  "Fertigungsgerecht")
  :UNITS
   (:mm    nil       nil     :mm          :mm         nil         nil    nil)
  :CONTENTS
  (
   (5      "0105"    " "      0            100         :PREF        "-"    "-")
   (10     "0110"    " "      0            100         :PREF        "-"    "-")
   (15     "0115"    " "      0            100         :PREF        "-"    "-")
   (20     "0120"    " "      0            100         :PREF        "-"    "-")
  )
  :KEY (:DIST)
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:DIST :PREFERENCE :USED :ADVICE )
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_hex"
; unique, not displayable column entries
  :PROF_FNC sha-hexagon
)

(sha-define-shop-table "twstsfty"
  :TABLE-TITLE "Verdrehsicherung"

  :COLUMNS
   (:IN_DIA  :OUT_DIA  :SLOT_WIDTH  :ANGLE  :NR_SLOTS  :TOOL_ID  :DESCR 
      :MIN_THICK :MAX_THICK :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES
   ("In_Drm" "Auß_Drm" "SchlitzBrte" "Win" "AnzSchliz" "WrkzgID" "Beschr"      "MinDick" "MaxDick" "Vorzug"      "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm      :mm       :mm          :deg    nil         nil      nil
      :mm         :mm         nil          nil    nil)
  :CONTENTS
  (
   (4.9      5.2       0.3          60      18          "6947"   ""
      0           100         :PREF        "-"    "-")
  )
  :KEY (:IN_DIA :OUT_DIA :SLOT_WIDTH :ANGLE :NR_SLOTS)
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:IN_DIA :OUT_DIA :SLOT_WIDTH :ANGLE :NR_SLOTS :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_twstsvty"
 ; unique, not displayable column entries
  :PROF_FNC sha-twstsfty
)

(sha-define-shop-table "clstr_obl"
  :TABLE-TITLE "Langlochgruppe"

  :COLUMNS
    (:HORIZ  :VERT  :ROWS  :HOLES_ROW  :DIST_X  :DIST_Y  :INDENT  :TOOL_ID
       :DESCR :TOOL_DIST_X :TOOL_DIST_Y   :MIN_THICK   :MAX_THICK :PREFERENCE 
         :USED :ADVICE)
  :COLUMN-NAMES
    ("Horiz" "Vert" "Spltn" "Löch/Splt" "Abst x" "Abst y" "Einrück" "WrkzgID" "Beschr" "WrkzgAbst x" "WrkzgAbst y" "MinDick" "MaxDick" "Vorzug" "Verw" "Fertigungsgerecht")
  :UNITS
    (:mm     :mm    nil    nil         :mm      :mm      :mm      nil
       nil    :mm          :mm            :mm          :mm        nil
         nil    nil)
  :CONTENTS
  (
   (4.0      15.9   2      2           15.86    9.52     7.93     "12012"
       "04180" 31.75       19.05          1.5          2.0        :NONPREF
         "-"    "-")
  )
  :KEY (:HORIZ :VERT :ROWS :HOLES_ROW :DIST_X :DIST_Y :INDENT )
  :DYNAMIC-COLUMN (:USED sha-punch-tool-already-used)
  :DISPLAY (:HORIZ :VERT :ROWS :HOLES_ROW :DIST_X :DIST_Y :INDENT :PREFERENCE 
            :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_clstr_obl"
; unique, not displayable column entries
  :PROF_FNC sha-clstr_obl
)
;========================= stamp tools =================================
;
; special entries in punch process tables
; ( | = OR )
;
;(sha-define-shop-table "stamp-process-name"
;  :table-title    are not necessary, because the are handled by the stamp
;                  process types table.
;
;  :columns       s.o.
;  :columns (<col1> ..:PROF_WP | ..:TOOL_PART :DESIGN_THICK | <coln>)  
;                     :PROF_FNC    :TOOL_FNC 
;                  ; Non parametric tools use PROF_WP and :TOOL_PART (and optional :DESIGN_THICK)
;                  ; Parametric tools use PROF_FNC and :TOOL_FNC
;
;                  ; The PROF_FNC amd :TOOL_FNC entries are
;                  ; usually unique, they may stay as single entry
;                  ; at the end of this table definition
;
;                  ; In case of a nonparametric tool (column :TOOL_PART which refers the name of a file containing
;                  ; the tool part (.sdpc format !)), the extra column :DESIGN_THICK might be used. It allows to define 
;                  ; the original sheet-thickness for which the tool part has been designed. If this column is not given
;                  ; a default value of 1 mm sheet-thickness is assumed as design sheet thickness.  
;  :columns-names s.o.
;  :columns-names s.o.
;  :units         s.o.            
;  :contents      s.o.
;
;  :key           s.o.
;  :display       s.o.
;  :flat-text     s.o.
;  :help          s.o.
;
; ; unique, not displayable column entries (if not existing as column)
;  :PROF_FNC fnc-symbol       
;  :TOOL_FNC fnc-symbol       
;
;========================= stamp tools =================================

(sha-define-shop-table "stamps"       
 :TABLE-TITLE "Prägewerkzeuge"

 :COLUMNS 
   ( :TOOL_TYPE :DESCRIPTION  )  ; The :TOOL_TYPE column must exist
 :COLUMN-NAMES
   ("Werkzgart" "Beschreibung")
 :UNITS 
   ( nil       nil   )
 :CONTENTS 
   (                     ; v localizable names
    ("csnk_rnd"		"Senkung, rund")
    ("spp" 		"Warze, durchbohrt")
    ("spr" 		"Rechteck, durchbohrt")
    ("dmp"		"Napf")
    ("dmph"		"Napf, mit Loch")
    ("emb_rect_rnd"	"Sicke, rechteckig gerundet")
    ("lance"		"Schlitzbrücke")
    ("dbl_lance"	"Schlitzbrückenpaar")
    ("cdg"              "Kartenaufnehmer")
    ("lvr"              "Kieme")
    ("lvr_smpl"         "Kieme, einfach")
    ("ext_flange"       "Gewindeeinzug")
    ("multibend"       "Mehrfachbiegung")
	("ts_examples"       "TECHSOFT Beispiele")
   )
 :KEY (:TOOL_TYPE)
 :DISPLAY (:DESCRIPTION)
 :FLAT-TEXT (:TOOL_TYPE)
 :HELP "sha_stamps"
 :BROWSER-ICON  "icon_stamps"
)

(sha-define-shop-table "ts_examples"
  :TABLE-TITLE "TECHSOFT Beispiele"

  :COLUMNS
   (:LENGTH  :WIDTH  :STAMP_DIR  :DESCR   :TOOL_ID  :MIN_THICK  :MAX_THICK
      :PROF_WP        :TOOL_PART    :ADVICE)
  :COLUMN-NAMES
   ("Length" "Width" "Stamp Dir" "Descr"  "Tool Id" "Min Thick" "Max Thick"
   "Prof WP"       "Tool Part"   "Advice")
  :UNITS
   (:mm      :mm      nil         nil      nil       :mm         :mm
      nil              nil           nil)
  :CONTENTS
  (
   (10      07       :UP          "Haken"   "10000"    0.50       1.50
       "haken.sdw" "haken.sdp" "-")
   (60      15       :UP          "Kieme"   "20000"    0.8       2.0
       "kieme.sdw" "kieme.sdp" "-")
  )
)

(sha-define-shop-table "csnk_rnd"   
  :TABLE-TITLE "Senkung, rund"

;  Second entry in TOOL_ID and DESCR is diameter of pre_punch            
  :COLUMNS 
    (:HOLE_DIA  :OUT_DIA  :ANG  :STAMP_DIR  :MIN_THICK  :MAX_THICK  
       :TOOL_ID    :DESCR     :PREFERENCE   :USED :ADVICE)
  :COLUMN-NAMES 
    ("LochDrm" "AußDrm" "Win" "PrägRicht" "MinDick" "MaxDick"        "WrkzgID"   "Beschr"    "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm        :mm        :deg  nil         :mm         :mm          
       nil         nil    nil     nil    nil)
  :CONTENTS 
    (
     (4.75      6.75       90    :UP          1.25        1.30
       "14100 5.1" "6581 5.1" :PREF     "-"    "-")
     (4.75      7.20       90    :UP          1.50        1.60       
       "14102 5.3" "6633 5.3" :NONPREF  "-"    "-")
     (3.70      5.30       90    :DOWN        1.00        1.00 
       "14018"     "7237"     :NONPREF  "-"    "-")
     (4.70      6.10       90    :DOWN        1.50        1.60
       "14004 5.3" "6659 5.3" :PREF     "-"    "-")
     (4.75      6.75       90    :DOWN        1.25        1.30
       "14006 5.1" "6655 5.1" :PREF     "-"    "-")
     (6.00      8.20       90    :DOWN        1.50        1.60
       "14008 7.0" "6624 7.0" :NONPREF  "-"    "-")
     (3.40      5.70      100    :DOWN        1.25        1.30 
      "14020 4.1"  "7372 4.1" :NONPREF  "-"    "-")
    )
   :KEY (:HOLE_DIA :OUT_DIA :ANG :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:HOLE_DIA :OUT_DIA :ANG :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_csnk_rnd"
 ; unique, not displayable column entries
   :PROF_FNC sha-csnk-rnd-profile
   :TOOL_FNC sha-csnk-rnd-tool
)

(sha-define-shop-table "spp"    
  :TABLE-TITLE "Warze, durchbohrt"

  :COLUMNS 
    (:SEMIP_DIA  :PUNCH_DIA  :HOLE_DIA  :HEIGHT  :DEPTH  :STAMP_DIR   
       :MIN_THICK   :MAX_THICK  :TOOL_ID   :DESCR   :PREFERENCE   :USED :ADVICE)
  :COLUMN-NAMES
    ("WarzDrm" "StanzDrm" "LochDrm" "Höhe" "Tiefe" "PrägRicht"       "MinDick"  "MaxDick" "WrkzgID"  "Beschr"  "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm         :mm         :mm        :mm      :mm     nil
       :mm          :mm         nil          nil     nil     nil    nil)
  :CONTENTS 
    (        
     (3.20       3.30        0.0        1.20      1.50   :UP
       1.80         2.00       "13400"     "6344"     :PREF     "-"    "-")
     (10.70      10.80       0.0        0.60      0.80   :UP
       1.50         1.50       "13406"     "6646"     :PREF     "-"    "-")
     (3.10       3.20       0.0        0.70       0.90   :UP
       1.30         1.60       "13404"     "6669"     :NONPREF  "-"    "-")
     (7.90       7.90       3.40       0.90       0.90   :UP
       1.00         1.00       "13506 3.4" "7067 3.4" :NONPREF  "-"    "-")
     (10.00      10.00       4.20       0.60      0.60   :UP
       1.25         1.25       "13504 4.1" "7066 4.1" :NONPREF  "-"    "-")
    )
   :KEY (:SEMIP_DIA :PUNCH_DIA :HOLE_DIA :HEIGHT :DEPTH 
         :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:SEMIP_DIA :PUNCH_DIA :HOLE_DIA :HEIGHT :DEPTH 
             :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_spp"
 ; unique, not displayable column entries
   :PROF_FNC sha-spp-profile
   :TOOL_FNC sha-spp-tool
)

(sha-define-shop-table "spr"
  :TABLE-TITLE "Rechteck, durchbohrt"

  :COLUMNS
    (:HORIZ  :VERT  :HORIZ_PUNCH  :VERT_PUNCH  :HEIGHT  :DEPTH  :STAMP_DIR  :MIN_THICK   :MAX_THICK  :TOOL_ID  :DESCR  :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES
        ("Horiz" "Vert" "Horiz_Stanz" "Vert_Stanz" "Höhe" "Tiefe" "PrägRicht" "MinDick"  "MaxDick" "WrkzgID" "Beschr" "Vorzug"      "Verw" "Fertigungsgerecht")
  :UNITS
    (:mm     :mm    :mm           :mm          :mm      :mm      nil        :mm          :mm         nil        nil     nil        nil    nil)
  :CONTENTS
    (
     (5      3      5.5           3.5          1.3      0.8      :UP        1            5            "spr3-5"  "spr"   :PREF      "-"    "-")
     (5      3      5.5           3.5          1.3      0.8      :DOWN      1            5            "spr3-5"  "spr"   :PREF      "-"    "-")
    )
   :KEY (:HORIZ  :VERT  :HORIZ_PUNCH  :VERT_PUNCH :HEIGHT :DEPTH
         :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:HORIZ  :VERT  :HORIZ_PUNCH  :VERT_PUNCH :HEIGHT :DEPTH
             :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_spr"
 ; unique, not displayable column entries
   :PROF_FNC sha-spr-profile
   :TOOL_FNC sha-spr-tool
)


(sha-define-shop-table "dmp"  
  :TABLE-TITLE "Napf"

  :COLUMNS 
    (:DIMPLE_DIA  :DEPTH  :ANG :STAMP_DIR  :MIN_THICK  :MAX_THICK :TOOL_ID 
       :DESCR  :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
    ("Drm"       "Tiefe" "Win" "PrägRicht"  "MinDick"  "MaxDick" "WrkzgID"       "Beschr" "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm          :mm     :deg  nil          :mm         :mm         nil
       nil    nil     nil    nil)
  :CONTENTS 
    (        
     (7.90       0.90    90     :DOWN         1.00        1.00        "13130"  
       "7199" :NONPREF  "-"    "-")
     (9.20       1.90   110     :DOWN         1.25        1.25        "13106"  
       "6690" :PREF     "-"    "-")
     (11.10      2.10    90     :DOWN         1.25        1.25        "13128"  
       "7180" :PREF     "-"    "-")
     (20.00      0.50    90     :DOWN         1.50        1.60        "13124"  
       "7184" :NONPREF  "-"    "-")
     (8.20       0.90    90     :UP           1.25        1.25        "13306"  
       "7175" :PREF     "-"    "-")
     (11.10      1.20    90     :UP           1.25        1.25        "13308"
       "7176" :NONPREF  "-"    "-")
    )
   :KEY (:DIMPLE_DIA :DEPTH :ANG :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:DIMPLE_DIA :DEPTH :ANG :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_dmp"
 ; unique, not displayable column entries
   :PROF_FNC sha-dmp-profile
   :TOOL_FNC sha-dmp-tool
)

(sha-define-shop-table "dmph"  
  :TABLE-TITLE "Napf, mit Loch"

  :COLUMNS 
    (:DIA   :HOLE_DIA  :DEPTH  :ANG  :STAMP_DIR :MIN_THICK  :MAX_THICK
       :TOOL_ID    :DESCR        :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
    ("Drm"  "LochDrm" "Tiefe" "Win" "PrägRicht"  "MinDick"  "MaxDick"        "WrkzgID"   "Beschr"     "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm    :mm        :mm     :deg  nil         :mm         :mm       
       nil          nil         nil     nil    nil)
  :CONTENTS 
    (
     (9.00  4.40       2.20    90    :UP          2.00        2.00 
       "13300"     "6367"        :PREF     "-"    "-")
     (10.00 4.50       5.50    120   :UP          0.80        0.80
       "13302"     "6496"        :NONPREF  "-"    "-")
     (12.70 3.47       4.10    90    :UP          1.50        1.60
       "13304"     "6698"        :NONPREF  "-"    "-")
     (7.90  3.40       0.90    90    :DOWN        1.00        1.00
      "13130  3.2" "7199  3.2"   :PREF     "-"    "-")
     (9.20  5.50       1.90    110   :DOWN        1.25        1.25
      "13106  4.7" "6690  4.7"   :PREF     "-"    "-")
     (11.10 3.40       1.00    90    :DOWN        1.25        1.25
      "13126  3.2" "7177  3.2"   :NONPREF  "-"    "-")
    )
   :KEY (:DIA :HOLE_DIA :DEPTH :ANG :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:DIA :HOLE_DIA :DEPTH :ANG :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_dmph"
 ; unique, not displayable column entries
   :PROF_FNC sha-dmph-profile
   :TOOL_FNC sha-dmph-tool
)

(sha-define-shop-table "emb_rect_rnd"  
  :TABLE-TITLE "Sicke, rechteckig gerundet"

  :COLUMNS 
    (:HORIZ  :VERT :RADIUS  :DEPTH :ANG  :STAMP_DIR  :MIN_THICK  :MAX_THICK
        :TOOL_ID  :DESCR  :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
    ("Horiz" "Vert" "Rad" "Tiefe" "Win" "PrägRicht" "MinDick" "MaxDick"        "WrkzgID" "Beschr" "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm     :mm    :mm   :mm     :deg  nil         :mm         :mm
        nil       nil    nil     nil    nil)
  :CONTENTS 
    (
     (60.00 15.00   5.00  2.80    45    :UP          1.00        2.00
        "13002"  "6362"  :PREF     "-"    "-")
    )
   :KEY (:HORIZ :VERT :RADIUS :DEPTH :ANG :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:HORIZ :VERT :RADIUS :DEPTH :ANG :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_emb_rect_rnd"
 ; unique, not displayable column entries
   :PROF_FNC sha-emb-rect-rnd-profile
   :TOOL_FNC sha-emb-rect-rnd-tool
)


(sha-define-shop-table "lance" 
  :TABLE-TITLE "Schlitzbrücke"

  :COLUMNS 
    (:OUT_LENGTH  :IN_LENGTH :RADIUS  :DEPTH :WIDTH   :STAMP_DIR  :MIN_THICK
       :MAX_THICK  :TOOL_ID   :DESCR  :PREFERENCE  :USED :ADVICE)
  :COLUMN-NAMES 
    ("AußLänge"  "InLänge"  "Rad"   "Tiefe" "Breite" "PrägRicht" "MinDick"       "MaxDick"   "WrkzgID"   "Beschr" "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm          :mm         :mm    :mm     :mm     nil         :mm
       :mm          nil         nil    nil     nil    nil)
  :CONTENTS 
    (
     (11.4        4.4         0      3.5     2.50    :UP          1.0
       1.6          "13024"     "6281"  :PREF     "-"    "-")
     (12.8        7.6         0      2.35    3.90    :UP          1.50
       1.50         "13042"     "6993"  :NONPREF  "-"    "-")
    )
   :KEY (:OUT_LENGTH :IN_LENGTH :RADIUS :DEPTH :WIDTH :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:OUT_LENGTH :IN_LENGTH :RADIUS :DEPTH :WIDTH :STAMP_DIR 
             :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_lance"
 ; unique, not displayable column entries
   :PROF_FNC sha-lance-profile
   :TOOL_FNC sha-lance-tool
)

(sha-define-shop-table "dbl_lance"
  :TABLE-TITLE "Schlitzbrückenpaar"

  :COLUMNS 
    (:OUT_LENGTH  :IN_LENGTH :RADIUS :DEPTH :WIDTH :SLOT_WIDTH   :STAMP_DIR
       :MIN_THICK :MAX_THICK  :TOOL_ID   :DESCR  :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES 
    ("AußLänge"  "InLänge"  "Rad"   "Tiefe" "Breite" "SchlitzBrte" "PrägRicht"      "MinDick"  "MaxDick"  "WrkzgID"  "Beschr"  "Vorzug"  "Verw" "Fertigungsgerecht")
  :UNITS 
    (:mm          :mm         :mm   :mm     :mm     :mm          nil
       :mm          :mm          nil           nil    nil     nil    nil)
  :CONTENTS 
    (
     (12.7        7.9         0     2.4     3.87    1.78         :UP
       1.5          1.6          "13022"   "HT"    :PREF     "-"    "-")
    )
   :KEY (:OUT_LENGTH :IN_LENGTH :RADIUS :DEPTH :WIDTH :SLOT_WIDTH :STAMP_DIR)
   :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
   :DISPLAY (:OUT_LENGTH :IN_LENGTH :RADIUS :DEPTH :WIDTH :SLOT_WIDTH 
             :STAMP_DIR :PREFERENCE :USED :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_dbl_lance"
 ; unique, not displayable column entries
   :PROF_FNC sha-dbl-lance-profile
   :TOOL_FNC sha-dbl-lance-tool
)

(sha-define-shop-table "cdg"
  :TABLE-TITLE "Kartenaufnehmer"

  :COLUMNS
   (:TOPWIDTH :BOTTOMWIDTH :LIPLENGTH  :LIPRAD  :LIPHEIGHT  :SLOTLENGTH
      :SLOTRAD  :STAMP_DIR   :X_CLUST    :Y_CLUST  :MIN_THICK :MAX_THICK  
        :TOOL_ID :DESCR :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES
   ("OberBrte" "UnterBrte" "LaschLäng" "LaschRad" "LaschHöhe"  "SchlitzLänge" "SchlitzRad" "PrägRicht" "X-Grp"    "Y-Grp"   "MinDick"  "MaxDick" "WrkzgID"   "Beschr" "Vorzug"   "Verw" "Fertigungsgerecht")
  :UNITS   
   (:mm       :mm          :mm         :mm      :mm         :mm 
      :mm       nil          nil         nil       :mm         :mm
        nil       nil      nil          nil    nil)
  :CONTENTS
  (
   (3.0       2.0          29.00       2.50     3.00        42.1  
      1.6      :UP           30.0        70        1.50        1.50
       "13044"  "6996"  :NONPREF  "-"    "-")
   (2.2       1.9          40.36       2.50     2.50        55.0
      2.4      :UP           25.4        90        1.60        1.60
       "13028"  "6392"  :PREF    "-"    "-")
   (3.7       2.3          46.00       2.50     3.10        58.0
      1.5      :UP           30.0       105        1.00        1.00
       "13032"  "6697"  :NONPREF  "-"    "-")
   (3.0       1.9          35.36       2.50     2.50        50.0
      2.4      :UP           24.0        80        1.25        1.25
       "13038"  "6906"  :NONPREF  "-"    "-")
  )
  :KEY (:TOPWIDTH :BOTTOMWIDTH :LIPLENGTH :LIPRAD :LIPHEIGHT :SLOTLENGTH
        :SLOTRAD :STAMP_DIR :X_CLUST :Y_CLUST )
  :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
  :DISPLAY (:TOPWIDTH :BOTTOMWIDTH :LIPLENGTH :LIPRAD :LIPHEIGHT :SLOTLENGTH
            :SLOTRAD :STAMP_DIR :X_CLUST :Y_CLUST :PREFERENCE :USED :ADVICE )
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_cdg"
 ; unique, not displayable column entries
  :PROF_FNC sha-cdg-profile
  :TOOL_FNC sha-cdg-tool
)


(sha-define-shop-table "lvr"
  :TABLE-TITLE "Kieme"

  :COLUMNS
   (:LENGTH  :WIDTH :RADIUS :ANGLE :DEPTH  :STAMP_DIR  :MIN_THICK  :MAX_THICK 
      :TOOL_ID :DESCR :USED :ADVICE)
  :COLUMN-NAMES
   ("Länge" "Breite" "Rad"  "Win" "Tiefe" "PrägRicht" "MinDick" "MaxDick"      "WrkzgID" "Beschr" "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm      :mm    :mm    :deg    :mm     nil         :mm         :mm
      nil       nil    nil    nil)
  :CONTENTS
  (
   (8        6      2      30      2.7    :UP          1.00        2.00
      "13008"  "6606" "-"    "-")
  )
  :KEY (:LENGTH  :WIDTH :RADIUS :ANGLE :DEPTH  :STAMP_DIR)
  :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
  :DISPLAY (:LENGTH  :WIDTH :RADIUS :ANGLE :DEPTH  :STAMP_DIR :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_lvr"
 ; unique, not displayable column entries
  :PROF_FNC sha-lvr-profile
  :TOOL_FNC sha-lvr-tool
)
   

(sha-define-shop-table "lvr_smpl"
  :TABLE-TITLE "Kieme, einfach"

  :COLUMNS
   (:LENGTH  :HEIGHT  :STAMP_DIR  :DESCR   :TOOL_ID  :MIN_THICK  :MAX_THICK
    :USED :ADVICE) 
  :COLUMN-NAMES
   ("Länge" "Höhe" "PrägRicht" "Beschr"  "WrkzgID" "MinDick" "MaxDick"       "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm      :mm      nil         nil      nil       :mm         :mm
      nil    nil)
  :CONTENTS
  (
   (50       6       :UP          "6666"   "1188"    1.00        2.00
      "-"    "-")
  )
  :KEY (:LENGTH  :HEIGHT  :STAMP_DIR)
  :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
  :DISPLAY (:LENGTH  :HEIGHT  :STAMP_DIR :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_lvr_smpl"
 ; unique, not displayable column entries
  :PROF_FNC sha-lvr_smpl-profile
  :TOOL_FNC sha-lvr_smpl-tool
)

(sha-define-shop-table "ext_flange"
  :TABLE-TITLE "Gewindeeinzug"

  :COLUMNS
   (:HOLE_DIA :HEIGHT   :STAMP_DIR  :TOOL_ID        :DESCR  :MIN_THICK
      :MAX_THICK :PREFERENCE :USED :ADVICE)
  :COLUMN-NAMES
   ("LochDrm" "Höhe"    "PrägRicht" "WrkzgID"       "Beschr" "MinDick"       "MaxDick"         "Vorzug"    "Verw" "Fertigungsgerecht")
  :UNITS
   (:mm        :mm      nil         nil              nil    :mm
      :mm        nil         nil    nil)
  :CONTENTS
  (
   (3.10       0.90    :UP          "13065 Prep.1.6" "M4"   1.00
      2.50   :PREF      "-"    "-")
   (2.65       0.90    :UP          "13080 Prep.2.0" "   "  1.00
      2.00   :PREF      "-"    "-")
   (3.10       0.90    :DOWN        "13065 Prep.1.6" "M4"   1.00
      2.50   :PREF      "-"    "-")
   (2.65       0.90    :DOWN        "13080 Prep.2.0" "   "  1.00
      2.00   :PREF      "-"    "-")
  )
  :KEY (:HOLE_DIA :HEIGHT :STAMP_DIR )
  :DYNAMIC-COLUMN (:USED sha-stamp-tool-already-used)
  :DISPLAY (:HOLE_DIA :HEIGHT :STAMP_DIR :PREFERENCE :USED :ADVICE)
  :FLAT-TEXT (:TOOL_ID)
  :HELP "sha_ext_flange"
 ; unique, not displayable column entries
  :PROF_FNC sha-ext_flange-profile
  :TOOL_FNC sha-ext_flange-tool
)
;                                              ^
;                    Pre-punched hole diameter |
;                    German: Vorloch  

(sha-define-shop-table "multibend"                                                
  ; TRUMPF Multibend tool. ATTENTION: DELTA_X values in this table must be identical to allowance values in multibend_bend table multiplied by -1.0.
  :TABLE-TITLE "Mehrfachbiegung"
  :COLUMNS
    ( :TOOL_ID    :DESCR   :THICK    :B       :H     :F       :U     :Z   :DELTA_X :B-MAX       :H-MIN      :H-MAX     :STAMP_DIR :MIN_THICK :MAX_THICK :PREFERENCE  :ADVICE)
  :COLUMN-NAMES
    ("WrkzgID"  "Beschr" "Dicke" "Breite" "Höhe" "Schnitt"  "Tiefe" "Z"  "Delta_x" "MaxBreite" "MinHöhe" "MaxHöhe"  "StanzRich" "MinDick" "MaxDick"  "Vorzug"  "FertGerecht")
  :UNITS
    ( nil         nil      :mm       :mm      :mm    :mm      :mm    :mm  :mm        :mm          :mm        :mm         nil        :mm       :mm         nil         nil)
  :CONTENTS
 (
  ( "1017-12" "Multibend 1.0" 1.0    55       23     2        4      0     2.1      55            10         25         :UP        1.0       1.0        :PREF       "-")
  ( "1017-13" "Multibend 1.5" 1.5    55       23     2        4      0.2   3.0      55            10         25         :UP        1.5       1.5        :PREF       "-")
  ( "1017-14" "Multibend 2.0" 2.0    55       25     2        4      0.2   3.9      55            10         25         :UP        2.0       2.0        :PREF       "-")
 )
   :KEY       (:THICK :STAMP_DIR)
   :DISPLAY   (:THICK :B :H :STAMP_DIR :DESCR  :PREFERENCE  :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :SORT-BY   (:THICK)
 ; unique, not displayable column entries
   :PROF_FNC sha-multibend-profile
   :TOOL_FNC sha-multibend-tool
   :HELP "sha_multibend"
 ; unique, not displayable column entries
)

;========================== bend relief tool tables ========================

(sha-define-shop-table "rnd_relief"
  :TABLE-TITLE "Runde Freisparung"

  :COLUMNS 
    (:HORIZ  :VERT :TOOL_ID  :DESCR :MIN_THICK :MAX_THICK :PREFERENCE  :ADVICE)
  :COLUMN-NAMES 
    ("Länge" "Breite" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Fertigungsgerecht")
  :UNITS 
    (:mm     :mm    nil       nil     :mm         :mm          nil   nil)
  :CONTENTS 
    (  	    	                            
;;===    Addition TECHSOFT begin WoHo 03.10.2011
	 (30.00   1.60  "TS001"   "TS001" 0           100         :PREF     "-")
;;===    Addition TECHSOFT end WoHo 03.10.2011	 
     (12.70   1.60  "08012"   "6662"  0           100         :NONPREF  "-")
     (17.50   2.00  "08314"   "7278"  0           100         :PREF     "-")
     ( 6.40   2.40  "08022"   "0899"  0           100         :PREF     "-")
     (12.70   3.20  "08048"   "GB"    0           100         :PREF     "-")
    )
   :KEY (:VERT)
   :DISPLAY (:VERT :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :FLAT-INFO (":TOOL_ID {1}" :TOOL_ID)
  ;:REPLACEMENT-INFO ("sha_remove_relief {1}" :VERT) ; not needed in rev 11.5 and higher
                                                     ; use new no_relief tool
   :HELP "sha_rnd_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :VERT
   :BROWSER-ICON  "icon_round_rel"
   :USABLE-AS-RELIEF (:WIDTH         :VERT     ; width column for relief
                      :DEPTH         :HORIZ    ; depth column for relief
                      :LENGTH_X_ANGLE (:DEG 90) ; rot angle when bend is in X_POS
                     )
   :PROF_FNC sha-obl-profile
)

(sha-define-shop-table "rect_relief"
  :TABLE-TITLE "Eckige Freisparung"

  :COLUMNS 
  ( :HORIZ  :VERT :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
   ("Länge" "Breite" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug" "Fertigungsgerecht")
  :UNITS (                                                       
    :mm     :mm     nil       nil     :mm         :mm        nil     nil)
  :CONTENTS 
  (	  	              	                
   (10.00    0.01  "00000"   "00"     0           100        :NONPREF  "-")
    (8.20    1.00  "07010"   "4787"   0           100        :PREF     "-")
   (13.70    1.60  "07018"   "1048"   0           100        :PREF     "-")
    (9.50    2.00  "07012"   "3703"   0           100        :NONPREF  "-")
   (25.40    3.20  "07024"   "PZ"     0           100        :PREF     "-")
   (32.00   10.00  "07044"   "JB"     0           100        :NONPREF  "-")
   (45.00   22.20  "04488"   "7251"   0           100        :NONPREF  "-")
  )
   :KEY (:VERT)
   :DISPLAY (:VERT :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_rect_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :VERT
   :USABLE-AS-RELIEF (:WIDTH         :VERT     ; width column for relief
                      :DEPTH         :HORIZ    ; depth column for relief
                     :LENGTH_X_ANGLE (:DEG 90) ; rot angle when bend is in X_POS
                     )
   :DEFAULT-FUNCTION sha-default-relief-tool
   :BROWSER-ICON  "icon_rect_rel"
   :PROF_FNC sha-rect-sharp-profile
)

(sha-define-shop-table "no_relief"
  :TABLE-TITLE "Keine Freisparung"

  :COLUMNS 
  ( :TYPE :ADVICE)
  :COLUMN-NAMES 
    ("Art" "Fertigungsgerecht")
  :UNITS (nil     nil)
  :CONTENTS 
  (	  	              	                
     ("None via cut" "-")
  )
   :KEY (:TYPE)
   :DISPLAY (:TYPE :ADVICE)
   :HELP "sha_no_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY "Angepasst"
   :USABLE-AS-RELIEF
     (
      :LENGTH_X_ANGLE  0       ; rot angle when bend is in X_POS
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :IMPRINT_FNC sha-relief-cut-1  ; internal function ; NOT for public change
   :BROWSER-ICON  "icon_no_rel"
   :PROF_FNC nil
)

;========================== miter relief tool tables ========================

(sha-define-shop-table "miter-chamfer"
  :TABLE-TITLE "Gehrungsfase Freisparung"

  :COLUMNS 
  ( :PERCT_DEPTH :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
   ( "ProzTiefe" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"  "Fertigungsgerecht")
  :UNITS (                                                       
    nil   nil       nil     :mm         :mm        nil     nil)
  :CONTENTS 
  (	  	              	                
   (100  "mr100"   "100%"     0           100        :NONPREF  "-")
   (80   "mr80"    "80%"      0           100        :NONPREF  "-")
  )
   :KEY (:PERCT_DEPTH)
   :DISPLAY (:PERCT_DEPTH :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_miter_chamfer_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :PERCT_DEPTH
   :BROWSER-ICON  "icon_rect_rel"
   :PROF_FNC sha-miter-chamfer-profile
)

(sha-define-shop-table "no-2d-miter-chamfer"
  :TABLE-TITLE "Keine 2D-Gehrungsfasenfreisparung"

  :COLUMNS 
  ( :PERCT_DEPTH :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
   ( "ProzTiefe" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"  "Fertigungsgerecht")
  :UNITS (                                                       
    nil          nil       nil     :mm         :mm        nil     nil)
  :CONTENTS 
  (	  	              	                
   (100   "mr100"   "100%"     0           100        :NONPREF  "-")
   (80    "mr80"    "80%"      0           100        :NONPREF  "-")
  )
   :KEY (:PERCT_DEPTH)
   :DISPLAY (:PERCT_DEPTH :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_rect_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :PERCT_DEPTH
   :REPLACEMENT-INFO "sha_remove_relief 0" 
   :BROWSER-ICON  "icon_rect_rel"
   :PROF_FNC sha-miter-chamfer-profile
)

(sha-define-shop-table "miter-spline"
  :TABLE-TITLE "Gehrungs-Spline Freisparung"

  :COLUMNS 
  ( :MID-POINTS :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
   ("MittPunkte" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"  "Fertigungsgerecht")
  :UNITS (                                                       
    nil         nil       nil     :mm         :mm        nil     nil)
  :CONTENTS 
  (	  	              	                
   (1  "mr100"   "MSPL 1 Mid pnt"     0           100        :NONPREF  "-")
   (2  "mr100"   "MSPL 1 Mid pnt"     0           100        :NONPREF  "-")
   (3  "mr100"   "MSPL 1 Mid pnt"     0           100        :NONPREF  "-")
  )
   :KEY (:MID-POINTS)
   :DISPLAY (:MID-POINTS :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_miter_spline_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :MID-POINTS
   :BROWSER-ICON  "icon_rect_rel"
   :PROF_FNC sha-miter-spline-profile
)

;========================== corner relief tool tables ========================

; rnd_corner_relief is a rectangular 3D representation with a 2D replacement by 
; a round corner relief, which might be smaller than the bend zone size
; This type of relief has been replaced by a real 3D round hole representation
; in rnd_3Dcorner_relief

(sha-define-shop-table "rnd_corner_relief"
   :TABLE-TITLE "Runde 2D-Eckenfreisparung"
  :COLUMNS 
    (:DIA :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE  )
  :COLUMN-NAMES 
     ("Drm" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"  "Fertigungsgerecht")
  :UNITS
    ( :mm   nil       nil     :mm         :mm         nil     nil)
  :CONTENTS 
    (
     (1.00  "01003"  "RW"      0          100        :PREF     "-")
     (2.00  "01012"  "CN"      0          100        :NONPREF  "-")
     (3.00  "01024"  "NE"      0          100        :PREF     "-")
     (4.00  "01042"  "EM"      0          100        :PREF     "-")
    (10.00  "01136"  "TAU"     0          100        :PREF     "-")
   ) 
   :KEY (:DIA )
   :DISPLAY (:DIA :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :REPLACEMENT-INFO ("sha_rnd_crn_relief_repl {1}" :DIA)
   :MENU-ENTRY :DIA
   :HELP "sha_rnd_relief"
 ; unique, not displayable column entries
   :BROWSER-ICON  "icon_rnd_co"
   :USABLE-AS-RELIEF
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :PROF_FNC sha-rnd-or-perp-v-shape-profile  
)


; sqr_corner-relief is pre SD2000+ syntax. Listed here for compatibility reasons only

(sha-define-shop-table "sqr_corner_relief"
  :TABLE-TITLE "Quadratische Eckenfreisparung"

  :COLUMNS 
    (:LENGTH :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("Breite" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( :mm     nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     (0.50    "03005"   "7253"   0          100         :NONPREF  "-")
     (2.50    "03009"   "7253"   0          100         :NONPREF  "-")
     (4.00    "03010"   "CQ"     0          100         :PREF     "-")
     (5.50    "03016"   "FP"     0          100         :PREF     "-")
     (6.40    "03020"   "DA"     0          100         :PREF     "-")
     (10.00   "03035"   ""       0          100         :NONPREF  "-")
    )
   :KEY (:LENGTH)
   :DISPLAY (:LENGTH :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_rect_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY  :LENGTH
   :USABLE-AS-RELIEF
     (
      :LENGTH_X_ANGLE  (:DEG 45) ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
     )
   :BROWSER-ICON  "icon_sqr_co"
   :PROF_FNC sha-sqr-corner-sharp-profile
)

;;; old obsolete definiton of corner relief with replacement in 2D
;;; replaced by no_3D_corner_relief (without 2D replacement)

(sha-define-shop-table "no_corner_relief"
  :TABLE-TITLE "Keine quadratische 2D-Eckenfreisparung"

  :COLUMNS 
    (:LENGTH :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("Breite" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( :mm     nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     (2.50    "03009"   "7253"   0          100         :NONPREF  "-")
     (4.00    "03010"   "CQ"     0          100         :PREF     "-")
     (5.50    "03016"   "FP"     0          100         :PREF     "-")
     (6.40    "03020"   "DA"     0          100         :PREF     "-")
     (10.00   "03035"   ""       0          100         :NONPREF  "-")
    )
   :KEY (:LENGTH)
   :DISPLAY (:LENGTH :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :REPLACEMENT-INFO ("sha_remove_corner_relief")
   :HELP "sha_sqr_sharp"
 ; unique, not displayable column entries
   :USABLE-AS-CORNER-RELIEF      :LENGTH
   :USABLE-AS-RELIEF
     (:WIDTH :LENGTH    ; width column for relief
      :DEPTH :HORIZ     ; depth column for relief
      :LENGTH_X_ANGLE (:DEG 90) ; rot angle when bend is in X_POS
     )
   :HANDLE sha-change-feature-color
   :BROWSER-ICON  "icon_no_3D_co"
   :PROF_FNC sha-min-perp-v-shape-profile
)

(sha-define-shop-table "no_2D_spline_v_corner_relief"
  ; 3D spline corner relief with 2D replacement by "no relief"
  :TABLE-TITLE "Keine 2D-Spline-V-Eckenfreisparungen"
  :COLUMNS
    (:REL_EXTREM_POS :TOOL_ID  :DESCR :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES
    ("Rel Extrem Pos" "WrkzgID" "Besch" "MindDicke" "MaxiDicke" "Vorz"   "FerGer") 
  :UNITS
    ( nil   nil       nil    :mm       :mm        nil      nil)
  :CONTENTS
    (
     ( 0    "07030"  "complete cut"   0  40        :NONPREF  "-")
    )
   :KEY       (:REL_EXTREM_POS)
   :DISPLAY   (:REL_EXTREM_POS :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_conn_spline_V_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :DESCR
   :USABLE-AS-RELIEF
     (
      :LENGTH_X_ANGLE  0       ; rotation angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
    )
   :HANDLE sha-change-feature-color  ; changes feature color to optically indicate a different 2D representation
                                                   
   :BROWSER-ICON  "icon_no_3D_co"
   :PROF_FNC sha-connected-spline-V-profile
   :REPLACEMENT-INFO ("sha_remove_corner_relief")
 )

(sha-define-shop-table "no_3D_corner_relief"
  :TABLE-TITLE "Keine Freisparung"

  :COLUMNS 
    (:TYPE :BLEND_RADIUS :ADVICE)
  :COLUMN-NAMES 
    ("Art" "BiegeRad" "Fertigungsgerecht")
  :UNITS 
    (nil   :mm    nil)
  :CONTENTS 
    ( 	     	                       
     ("Sharp Join"   0 "-")
    )
   :KEY (:BLEND_RADIUS)
   :DISPLAY (:BLEND_RADIUS :TYPE :ADVICE)
   :SORT-BY :BLEND_RADIUS
   :MENU-ENTRY ("R={1}" :BLEND_RADIUS)
   :HELP "sha_no_3D_c_relief"
 ; unique, not displayable column entries
   :USABLE-AS-RELIEF     
     (
      :LENGTH_X_ANGLE  0       ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_no_3D_co"
   :PROF_FNC sha-join-lips-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change 
)

(sha-define-shop-table "connected_v_corner_relief"

  :TABLE-TITLE "Verbund V-Form-Eckenfreisparung"
  :COLUMNS 
    (:MIN_CRN_ANGLE :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("MinEckenWin" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( :deg            nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     ( 5 "07025"   "7253"   0          100         :NONPREF  "-")
    )
   :KEY (:MIN_CRN_ANGLE)
   :DISPLAY (:MIN_CRN_ANGLE :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_conn_v_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY "Angepasst"
   :USABLE-AS-RELIEF     
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_connected_v_co"
   :PROF_FNC sha-connected-v-shape-profile
)

(sha-define-shop-table "connected_fix_radius_blended_V_corner_relief"

  :TABLE-TITLE "Verbund FestRadVerrund V"
  :COLUMNS 
    (:BLEND_RADIUS :MIN_CRN_ANGLE :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("BiegeRad" "MinEckenWin" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( :mm :deg            nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     ( 0   5 "07025"   "7253"   0          0.5       :NONPREF  "-")
     ( 0.5 5 "07026"   "7253"   0          1         :NONPREF  "-")
     ( 1   5 "07027"   "7253"   0          2         :NONPREF  "-")
     ( 1.5 5 "07027"   "7253"   0          3         :NONPREF  "-")
     ( 2   5 "07028"   "7253"   0          4         :NONPREF  "-")
     ( 4   5 "07028"   "7253"   0          8         :NONPREF  "-")
     ( 5   5 "07028"   "7253"   0          10         :NONPREF  "-")
     ( 6   5 "07028"   "7253"   0          12         :NONPREF  "-")
    )
   :KEY (:BLEND_RADIUS :MIN_CRN_ANGLE)
   :DISPLAY (:BLEND_RADIUS :MIN_CRN_ANGLE :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_conn_fix_rad_bld_V_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY ("V R{1}" :BLEND_RADIUS)
   :USABLE-AS-RELIEF     
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_connect_blended_V_co"
   :PROF_FNC sha-connected-v-shape-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
)

(sha-define-shop-table "connected_variable_radius_blended_V_corner_relief"

  :TABLE-TITLE "Verbund VariabRadVerrund V"
  :COLUMNS 
    (:REL_BLEND_POS :MIN_CRN_ANGLE :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("RelBiegePos" "MinEckenWin" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( nil :deg            nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     ( 0    5 "07030"   "sharp"     0          0.5       :NONPREF  "-")
     ( 0.25 5 "07031"   "R->25%"    0          2         :NONPREF  "-")
     ( 0.5  5 "07032"   "R->50%"    0          3         :NONPREF  "-")
     ( 0.75 5 "07033"   "R->75%"    0          4         :NONPREF  "-")
     ( 1    5 "07034"   "R->100%"   0          8         :NONPREF  "-")
    )
   :KEY       (:REL_BLEND_POS :MIN_CRN_ANGLE)
   :DISPLAY   (:REL_BLEND_POS :MIN_CRN_ANGLE :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_conn_var_radius_bld_V_c_relief"
   :MENU-ENTRY :DESCR 
 ; unique, not displayable column entries
   :USABLE-AS-RELIEF     
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_connect_blended_V_co"
   :PROF_FNC sha-connected-v-shape-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
)

(sha-define-shop-table "connected_spline_V_corner_relief"

  :TABLE-TITLE "Spline V"
  :COLUMNS
    (:REL_EXTREM_POS :TOOL_ID  :DESCR :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES
    ("RelExtremPos" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht") 
  :UNITS
    ( nil   nil       nil    :mm       :mm        nil      nil)
  :CONTENTS
    (
     ( 0    "07030"  "complete cut"   0  40        :NONPREF  "-")
     ( 0.25 "07031"  "squeeze 25%"    0   4        :NONPREF  "-")
     ( 0.5  "07032"  "squeeze 50%"    0   1.5      :NONPREF  "-")
     ( 0.75 "07033"  "squeeze 75%"    0   1        :NONPREF  "-")
    )
   :KEY       (:REL_EXTREM_POS)
   :DISPLAY   (:REL_EXTREM_POS :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_conn_spline_V_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :DESCR
   :USABLE-AS-RELIEF
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
    )
   :BROWSER-ICON  "icon_connect_spline_V_co"
   :PROF_FNC sha-connected-spline-V-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
   :IGNORE_IMPRINT_FNC (:REL_EXTREM_POS 0)  ; no imprint function for regular (non squeezed) spline
)

; example for splines with manual shape definition

(sha-define-shop-table "connected_spline_V_corner_relief_manual"

  ; entry form-factor means: FORM_FACTOR = 1 is unchanged regular interp spline
  ;                                          with two tangend conditions
  ;                          FORM_FACTOR >1 slimmer shape
  ;                          FORM_FACTOR <1 less slim shape

  :TABLE-TITLE "Spline V-Form man"
  :COLUMNS
    (:REL_EXTREM_POS :FORM_FACTOR :START_OFFSET :OTHER_START_OFFSET :TOOL_ID  :DESCR :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES
    ("RelExtremPos" "FormFaktor" "StartVersatz" "AnderStartVersatz" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht") 
  :UNITS
    ( nil   nil   :mm :mm  nil       nil    :mm       :mm        nil      nil)
  :CONTENTS
    (
     ( 0    1     0   0    "07030"  "complete cut"   0  40        :NONPREF  "-")
     ( 0    1.2   0   0    "07030"  "complete cut"   0  40        :NONPREF  "-")
     ( 0.25 1     0   0    "07031"  "squeeze 25%"    0   4        :NONPREF  "-")
     ( 0.25 1.2   0   0    "07031"  "squeeze 25%"    0   4        :NONPREF  "-")
     ( 0.5  1     0   0    "07032"  "squeeze 50%"    0   1.5      :NONPREF  "-")
     ( 0.75 0.9   0   0    "07033"  "squeeze 75%"    0   1        :NONPREF  "-")
    )
   :KEY       (:REL_EXTREM_POS :FORM_FACTOR :START_OFFSET :OTHER_START_OFFSET)
   :DISPLAY   (:REL_EXTREM_POS :FORM_FACTOR :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_conn_spline_V_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY :DESCR
   :USABLE-AS-RELIEF
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
    )
   :BROWSER-ICON  "icon_connect_spline_V_co"
   :PROF_FNC sha-connected-spline-V-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
)

(sha-define-shop-table "minimal_round_relief"

  :TABLE-TITLE "Min runde Eckenfreisparung"
  :COLUMNS 
    (:TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     ( "Laser Min Rnd"   "Min Round Laser cut"   0   100  :PREF  "-")
    )
   :KEY (:TOOL_ID)
   :DISPLAY (:TOOL_ID :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :MENU-ENTRY "Angepasst"
   :HELP "sha_min_rnd_c_relief"
 ; unique, not displayable column entries
   :USABLE-AS-RELIEF     
     (
      :LENGTH_X_ANGLE  0 ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_rnd_co"
   :PROF_FNC sha-minimal-round-profile
)

(sha-define-shop-table "rnd_3Dcorner_relief"
  :TABLE-TITLE "Rundloch"

  :COLUMNS 
    (:DIA :REL_RND_CENT_POS :TOOL_ID  :DESCR  :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE  )
  :COLUMN-NAMES 
     ("Drm" "RelHilfsPos" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"  "Fertigungsgerecht")
  :UNITS  
    ( :mm nil  nil       nil     :mm         :mm         nil     nil)
  :CONTENTS 
    (
     (1.10 0.5 "01004"  "RW"      0          100        :PREF     "-")
     (2.00 0.5 "01012"  "CN"      0          100        :NONPREF  "-")
     (3.00 0.5 "01024"  "NE"      0          100        :PREF     "-")
     (4.00 0.5 "01042"  "EM"      0          100        :PREF     "-")
     (8.00 0.5 "01116"  "TAQ"     0          100        :NONPREF  "-")
    (10.00 0.5 "01136"  "TAU"     0          100        :PREF     "-")
    (12.70 0.5 "01158"  "TAV"     0          100        :PREF     "-")
    (20.00 0.5 "01190"  "TBQ"     0          100        :NONPREF  "-")
   ) 
   :KEY (:DIA :REL_RND_CENT_POS)
   :DISPLAY (:DIA :REL_RND_CENT_POS :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_rnd_3D_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY ("D={1}  at {2}%" :DIA (* 100 :REL_RND_CENT_POS))
   :USABLE-AS-RELIEF 
     (:LENGTH_X_ANGLE  0       ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_rnd_co"
   :PROF_FNC sha-rnd-corner-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
)

(sha-define-shop-table "min_perp_v_corner_relief"
  :TABLE-TITLE "Min rechtw V-Form-Eckenfreisparung"
  :COLUMNS 
    (:MIN_WIDTH :MIN_LENGTH :MIN_ANGLE :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("MinBreite" "MinLänge" "MinWin" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( :mm        :mm          :deg       nil       nil      :mm        :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     (1         3           10         "07025"   "7253"   0          2      :NONPREF  "-")
     (2         5           10         "07025"   "7253"   0          4      :NONPREF  "-")
    )
   :KEY (:MIN_WIDTH :MIN_LENGTH :MIN_ANGLE)
   :DISPLAY (:MIN_WIDTH :MIN_LENGTH :MIN_ANGLE :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :HELP "sha_min_perp_v_c_relief"
 ; unique, not displayable column entries
   :MENU-ENTRY "Angepasst"
   :USABLE-AS-RELIEF     
     (:WIDTH :MIN_WIDTH        ; width column for relief
      :DEPTH :MIN_LENGTH       ; depth column for relief
      :LENGTH_X_ANGLE 0        ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length => no size check
     )
   :BROWSER-ICON  "icon_perp_v_co"
   :PROF_FNC sha-min-perp-v-shape-profile
)

(sha-define-shop-table "blended_V_corner_relief"
  :TABLE-TITLE "V-Form-Eckenfreisparung"
  :COLUMNS 
    (:V_ANGLE :BLEND_RAD :V_LENGTH :REL_BLEND_POS :TOOL_ID  :DESCR   :MIN_THICK  :MAX_THICK  :PREFERENCE :ADVICE)
  :COLUMN-NAMES 
    ("VWin" "VerrndRad" "VLänge" "RelVerrndPos" "WrkzgID" "Beschr" "MinDick" "MaxDick" "Vorzug"   "Fertigungsgerecht")
  :UNITS 
    ( :deg    :mm   :mm     nil       nil      nil        :mm       :mm         nil      nil)
  :CONTENTS 
    ( 	     	                       
     (20      0.4   10      0.3       "07025"  "7253"     0          2      :NONPREF  "-")
     (30      0.4   10      0.3       "07026"  "7254"     0          2      :NONPREF  "-")
     (40      0.4   10      0.3       "07027"  "7255"     0          2      :NONPREF  "-")
     (45      0.4   10      0.3       "07028"  "7256"     0          2      :NONPREF  "-")
     (50      0.4   10      0.3       "07029"  "7257"     0          2      :NONPREF  "-")
     (60      0.4   10      0.3       "07030"  "7258"     0          2      :NONPREF  "-")
    )
   :KEY (:V_ANGLE :BLEND_RAD :V_LENGTH)
   :DISPLAY (:V_ANGLE :BLEND_RAD :V_LENGTH :REL_BLEND_POS :TOOL_ID  :DESCR :PREFERENCE :ADVICE)
   :FLAT-TEXT (:TOOL_ID)
   :MENU-ENTRY ("{1}° R={2}" :V_ANGLE :BLEND_RAD)
   :HELP "sha_bld_V_c_relief"
 ; unique, not displayable column entries
   :USABLE-AS-RELIEF     
     (
      :LENGTH_X_ANGLE 0        ; rot angle when bend is in X_POS
      :ROTATE-WITH-CORNER t    ; rotate relief with bend-bend-angle / 2
      :LENGTH t                ; dynamic adapted tool length with use of relief cut => no size check
     )
   :BROWSER-ICON  "icon_blended_V_co"
   :PROF_FNC sha-blended-V-profile
   :IMPRINT_FNC sha-corner-cut-1  ; internal function ; NOT for public change
)

;========================== Production process definitions ==================

; Definitions of option setting for SHA_MULTI_UNFOLD command. 
; Each production process contains the options which should be set or unset 
; to define the appropriate combination of geometry and information 
; needed for certain production data. 

; The "global" switches :FILE :DOCU :OUTPUT_WP :DB should be set in the global
; drawing and documentation settings list 

(sha-define-shop-table "flame_cutting"
  :TABLE-TITLE         "Brennschneiden"
  :TITLE               "Brennschneiden-Dokumentation"
  :PROCESS-TYPE        :CUT-PROCESSES
  :STAMP_GEO           nil
  :PUNCH_GEO           t
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :BEND_LINE           nil
  :TEXT_LABELS         nil
  :REF_POINTS          nil
  :MERGE               t
  :MI                  nil
  :DXF                 nil
  :DWG                 nil
  :IGES                nil
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "laser_cutting"
  :TABLE-TITLE         "Laserschneiden"
  :TITLE               "Laserschneiden-Dokumentation"
  :PROCESS-TYPE        :CUT-PROCESSES
  :STAMP_GEO           nil
  :PUNCH_GEO           t
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :BEND_LINE           nil
  :TEXT_LABELS           nil
  :REF_POINTS          nil
  :MERGE             t
  :MI                  nil
  :DXF                 nil
  :DWG                 nil
  :IGES                nil
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "nibbling"
  :TABLE-TITLE         "Nibbeln"
  :TITLE               "Nibbeln-Dokumentation"
  :PROCESS-TYPE        :CUT-PROCESSES
  :STAMP_GEO           nil
  :PUNCH_GEO           t
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :BEND_LINE           nil
  :TEXT_LABELS           nil
  :REF_POINTS          t
  :MERGE             t
  :MI                  nil
  :DXF                 nil
  :DWG                 nil
  :IGES                nil
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "punching"
  :TABLE-TITLE         "Stanzen"
  :TITLE               "Lochen-Dokumentation"
  :PROCESS-TYPE        :CUT-PROCESSES
  :STAMP_GEO           nil
  :PUNCH_GEO           t
  :REF_POINTS          t
  :BEND_LINE           nil
  :TEXT_LABELS           nil
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :MERGE             t
  :MI                  nil
  :DXF                 nil
  :DWG                 nil
  :IGES                t
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "stamping"
  :TABLE-TITLE         "Prägen"
  :TITLE               "Prägen-Dokumentation"
  :PROCESS-TYPE        :STAMP-PROCESSES
  :STAMP_GEO           t  
  :PUNCH_GEO           nil
  :REF_POINTS          t
  :BEND_LINE           nil
  :TEXT_LABELS           nil
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :MERGE               t
  :MI                  nil
  :DXF                 nil
  :DWG                 nil
  :IGES                nil
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "bending"
  :TABLE-TITLE         "Biegen"
  :TITLE               "Biegen-Dokumentation"
  :PROCESS-TYPE        :BEND-PROCESSES
  :STAMP_GEO           t
  :PUNCH_GEO           t
  :REF_POINTS          nil
  :BEND_LINE           t
  :TEXT_LABELS         t
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :MERGE               t
  :MI                  t
  :DXF                 t
  :DWG                 nil
  :IGES                nil
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "welding"
  :TABLE-TITLE         "Schweißen"
  :TITLE               "Schweißen-Dokumentation"
  :PROCESS-TYPE        :WELD-PROCESSES
  :BEND_LINE           nil
  :TEXT_LABELS           nil
  :STAMP_GEO           nil
  :PUNCH_GEO           t
  :REF_POINTS          nil
  :BEND_ALLOWANCE_SHOP t
  :SHARP_BEND_RADIUS   1
  :MERGE               t
  :MI                  nil
  :DXF                 nil
  :DWG                 nil
  :IGES                nil
  :TXT                 nil
  :HTML                t
)

(sha-define-shop-table "process_relations"
  :COLUMNS
   (:MATERIAL :MIN_THICK :MAX_THICK :PROCESSES)
  :UNITS 
   (nil       :mm        :mm        nil)
  :CONTENTS
   (
     ("UST 1203" 1.00      9    ("laser_cutting" "punching" "stamping" "bending" "welding"))
     ("UST 1203" 9         50   ("flame_cutting" "welding"))
   )
  :KEY (:MATERIAL :MIN_THICK :MAX_THICK)
)
 
(sha-define-shop-table  "drawing-settings"
  :TABLE-TITLE         "2D-Zeichnen"
  :TITLE               "2D-Zeichnen-Dokumentation"
  :BOTH_SIDES          nil     ; t= show both side (upside and downside)
  :BEND_LINE           t       ; t= show text lines
  :TEXT_LABELS         t       ; t= show tool text labels
  :SINGLE_BEND_TEXT    t       ; t= use only one bend text for bends which can be produced in one bend action
  :STAMP_GEO           t       ; t= show stamp geometry 
  :PUNCH_GEO           t       ; t= show punch geometry
  :REF_POINTS          nil     ; t= show tool reference points
  :MERGE               t       ; t= merge sheet border lines (e.g. bend zone border geomettry)
  :TOPPART_ONLY        nil     ; t= use one top part only (no 2D parts structure)
  :CONVERT_SPLINES     nil     ; t= run spline/biarc conversion
  :SPLINE-MIN-RAD      0.1     ; minimum arc radius in CONVERT_SPLINE in [mm]
  :SPLINE-MAX-RAD      1000    ; maximum arc radius in CONVERT_SPLINE in [mm]
  :SPLINE-MIN-LENGTH   0.1     ; minimum length of elements in CONVERT_SPLINE in [mm]
  :REPLACEMENTS        t       ; t= run replacement macros
  :BEND_ALLOWANCE_SHOP t       ; t= use bend allowance calculation according to shop specification
  :BEND_ALLOWANCE_FIXED 0.4    ; use fixed bend allowance k-factor (ANSI notation: values 0-1)
  :CONE_ALLOWANCE      0.5     ; use fixed bend allowance k-factor (ANSI notation: values 0-1)
  :SHARP_BEND_RADIUS   1       ; setf sharp bend radius [mm]
  :POSTPROCESS         nil     ; t= start automatic customer postprocess
  :MI                  nil     ; t= produce MI file with flat drawing
  :DXF                 nil     ; t= produce DXF file with flat drawing
  :DWG                 nil     ; t= produce DWG file with flat drawing
  :IGES                nil     ; t= produce IGES file with flat drawing
  :TXT                 nil     ; t= produce TXT file with flat drawing
  :FILE                nil     ; t= produce file output with flat drawing
  :OUTPUT_WP           t       ; t= output flat result to workplane
  :OUTPUT_OSDD         nil     ; t= send flat result to running Drafting
)

(sha-define-shop-table  "documentation-settings"
  :COMPANY "Parametric Technology GmbH"
  :LOGO "personality/SheetAdvisor/flat_docu_icons/images/PTC_RGB_LOGOTYPE_GRAY.png"
  :LOGOX 63
  :LOGOY 186
  :LINE1 "Customer company name"             ; to be customized by customer
  :LINE2 "Customer Street 1"                 ; to be customized by customer
  :LINE3 "Telefon      1234567890"           ; to be customized by customer
  :LINE4 "Telefax      1234567890"           ; to be customized by customer
  :LINE5 "12345 Customer town"               ; to be customized by customer
  :LINE6 "http://www.customer-company.com/"  ; to be customized by customer
  :LINE7 "www.customer-company.com"          ; to be customized by customer
  :NSTARTP "start.html"
  :NJSCRIPT "ftiens4.js"
  :NTREEN "tree.js"
  :NSTYLEN "style.css"
  :NNAVIP "navi.html"
  :NBANNERP "banner.html"
  :NTITLE "Inhalt"
  :CBODY "white"
  :NSTYLE  "BODY {background-color: rgb(153,204,204)}"
  :NIMAGES "personality/SheetAdvisor/flat_docu_icons/images"
  :PTITLE (fix_mesg "Automatischer {1}-Bericht" (extended-program-name :product-only))
  :BTITLE "Bericht"
  :TITLE  (fix_mesg"Dokumentation für {1}-Teil" (sheetmetal-name))
  :STITLE "Werte für Teil"
  :GTITLE "Grafische Darstellung:"
  :PIXBCOL 1,1,1  ;;white
  :ISOZ -45      ; Isometric view Angle Z
  :ISOY 35.05    ; Isometric view Angle Y
  :DIEZ 20.66    ; Dimetric view Angle Z
  :DIEY 19.43    ; Dimetric view Angle Y
  :3DX 700.0     ; Width of preview viewport for 3D view, 
  :3DY 400.0     ; Height of preview viewport for 3D view
  :2DX 700.0     ; Width of preveiw viewport for 2D view
  :2DY 700.0     ; Height "
  :3DFX 800      ; Width of preview viewport for 3D Flat view
  :3DFY 800      ; Height of preview viewport for 3D Flat view
  :DOCU  nil
  :HTML  t 
  :VRML  t
  :VRMLF 2       ; VRML file type/rev format (1/2)
  :TIFF  t
  :ISO   t
  :DIM   nil
  :FREE  nil
  :ADD_3D_DOC nil  ; Add additional 3D flat 
  :ADD_3D_DOC_PTITLE "3D Flat Part" ;Title for the HTML Page for the 3D Sheet Part as flat
  :ADD_3D_DOC_PNAME  "3D Flat"      ;Prosess name for the HTML documentation & navigation bar
  :PART-INFO t
  :GENERAL-TOOL-INFO t
  :PUNCH-STAMP-TOOL-INFO t
  :RELIEF-TOOL-INFO t
  :BEND-TOOL-INFO t
  :ADD-TAB1 nil ; Add additional info to table1.
                ; Form: (("str1" "str2") ("str3" "str4") ...)
                ; Either directly listed here or as function-symbol of a funtion which
                ; produces such a list of 2 string lists.
                ; Each double string sublist produces one new line in the first table of the html document.
  :ADD-TAB2 nil ; Add additional info to table2.
                ; Form: (("str1" "str2" "str3") ("str4" "str5" "str6") ...)
                ; Either directly listed here or as function-symbol of a funtion which
                ; produces such a list of 3 string lists.
                ; Each triple string sublist produces one new line in the second table of the html document.
   :2D-FLAT-MIN-TEXT-SIZE-PIX 12
)

(sha-define-shop-table "database-settings"
  :USEDB nil
  :DDM-ACTION ""
  :MM-ACTION ""
 ;;example of valid parameters for DB-PARAMS
 ;;("TMPDIR" "ELID" "PELID" "L_TAB" "STATION" "USER" "DR_NR" "THICKNESS" "MATERIAL")
  :DB-PARAMS ("TMPDIR" "L_TAB" "ELID" "PELID")
  :REF_GRP "DB-PREF"
  :KEY_WORD "NAME"
)

; example for partner specific option settings

; example for customer definable "Drawing option" with 3D modification
(sha-define-shop-table "chamfer-option"
  :TITLE "Fase"
  :INIT-STATE nil                     ; initial state of option
  :EVAL-WHEN-T nil                    ; execute this function when option flag is T
  :FUNCTION sha-flat-remove-chamfer   ; function definition in sha_demoshop_func.lsp
  :OPTION-KEY :CHAMFER-PROCESS        ; key for reference in :DEPENDENCIES list
  :DEPENDENCIES nil                   ; list of other options and their dependent on/off setting
)

; example for customer definable "Drawing option" with 2D modification
(sha-define-shop-table "change-color"
  :TITLE "Farbe ändern"
  :INIT-STATE nil                     ; initial state of option
  :EVAL-WHEN-T t                      ; execute this function when option flag is T
  :OPTION-KEY :CHANGE-COLOR           ; key for reference in :DEPENDENCIES list
  :DEPENDENCIES (:TOPPART_ONLY t)     ; list of other options and their dependent on/off setting
                                      ; Keys of standard options: see option keys of SHA_MULTI_UNFOLD syntax
                                      ; e.g. (:TOPPART_ONLY t) 

 ; The following Drafting macro sequences may refer to:
 ; <string> OR <list of strings> OR
 ; <function returning a string or list of strings>
 ;
 ; In this example the function given with :MACRO-EARLY does a global color
 ; change (example function can be found in sha_demoshop_func.lsp)
 ; while the string given with :MACRO-LATE does a local color change.
 ; Depending on the "TOP part only" option setting in the MULTI_UNFOLD menu
 ; the first or the latter of this MACRO effects becomes visible.
 ; The :MACRO-EARLY sequence will be executed before all other macro options
 ; (except potentially existing replacement macros),
 ; while the :MACRO-LATE sequence will be executed after all other macro
 ; options have been executed.

  :MACRO-EARLY sha-change-color-red
  :MACRO-LATE "CHANGE_COLOR WHITE SELECT ALL CONFIRM END"
)

(sha-define-shop-table "postprocess-settings"
  :TITLE "Auswertung"
  :FUNCTION sha-shop-postprocess-function
)

;========================== allowance tables and formulas ==================

;;===    Addition TECHSOFT      WoHo 03.10.2011

(sha-define-shop-table "hem_allow"
  :TABLE-TITLE "180-Grad-Einzugswerte"

  :COLUMNS
    ;    material key     tool key
    (:MATERIAL :THICK   :HEM_DIST  :ALLOWANCE)
  :UNITS
    (nil       :mm      :mm       :mm)
  :CONTENTS
    (
 
       ("Stahl"   1.25  0.01   -0.70)
       ("Stahl"   1.25  1.25   -0.20)
	   ("Stahl"   1.50  0.01   -0.85)
	   ("Stahl"   1.50  1.50   -0.25)
	   ("Alu"     1.50  1.50   -0.23)

    )
  :KEY
    (:MATERIAL :THICK :HEM_DIST)
)

;;===    Addition TECHSOFT      WoHo 03.10.2011

(sha-define-shop-table "offset_allow"
  :TABLE-TITLE "Z-Biegung-Einzugswerte"

  :COLUMNS
    ;    material key     tool key
    (:MATERIAL :THICK    :OFFSET_HEIGHT  :ALLOWANCE)
  :UNITS
    (nil       :mm       :mm             :mm)
  :CONTENTS
    (

       ("Stahl"    1.25  1.0             0.20)
       ("Stahl"    1.25  2.0             0.80)
       ("Stahl"    1.25  3.0             1.60)
       ("Stahl"    1.25  4.0             2.70)

       ("Stahl"    1.50  1.0             0.10)
       ("Stahl"    1.50  2.0             0.50)
       ("Stahl"    1.50  3.0             1.20)
       ("Stahl"    1.50  4.0             2.30)

       ("Alu"      1.00  1.0             0.20)
       ("Alu"      1.00  2.0             0.90)
       ("Alu"      1.00  3.0             1.98)
       ("Alu"      1.00  4.0             2.90)

       ("Alu"      1.60  1.0             0.10)
       ("Alu"      1.60  2.0             0.65)
       ("Alu"      1.60  3.0             1.30)
       ("Alu"      1.60  4.0             2.30)

	   )
  :KEY
    (:MATERIAL :THICK :OFFSET_HEIGHT)
)

(sha-define-shop-table "bend_form_allow"
  :TABLE-TITLE "Gesenkbiege-Einzugswerte"

  :COLUMNS
    ;    material key     tool key
    (:MATERIAL :THICK   :ANGLE :RADIUS    :ALLOWANCE)
  :UNITS
    (nil       :mm    :deg   :mm        :mm)
  :CONTENTS
   (    
    ;; Zincor 1.00 

    ("UST 1203" 1.00   90.0   0.4    -1.65)
    ("UST 1203" 1.00   90.0   1.6    -2.10)
    ("UST 1203" 1.00   90.0   2.4    -2.40)

    ;; Zincor 1.25 

    ("UST 1203" 1.25   90.0   0.4    -1.95)
    ("UST 1203" 1.25   90.0   1.6    -2.35)
    ("UST 1203" 1.25   90.0   2.4    -2.70)

    ;; Zincor 1.50 

    ("UST 1203" 1.50    90.0   0.4    -2.35)
    ("UST 1203" 1.50    90.0   1.6    -2.70)
    ("UST 1203" 1.50    90.0   2.4    -3.21)

    ;; Aluminum 1.00 

    ("AA 5052" 1.00  90.0   0.4  -1.75)
    ("AA 5052" 1.00  90.0   1.6  -2.10)
    ("AA 5052" 1.00  90.0   2.4  -2.45)

    ;; Aluminum 1.60 

    ("AA 5052" 1.60  45.0   0.4  -1.35)
    ("AA 5052" 1.60  90.0   0.4  -2.55)
    ("AA 5052" 1.60  90.0   0.6  -2.61)
    ("AA 5052" 1.60  90.0   0.8  -2.67)
    ("AA 5052" 1.60  90.0   1.6  -2.90)
    ("AA 5052" 1.60  90.0   2.4  -3.25)
   )
  :KEY
    (:MATERIAL :THICK :ANGLE :RADIUS)
)

(sha-define-shop-table "bend_fold_allow"
  :TABLE-TITLE "Schwenkbiege-Einzugswerte"
   ; if used with the unchanged "linear_interp" formula (file:demoshop_func.lsp)
   ; this table needs an extra column: :ANGLE for the interpolation
   ; and should usually start with an ANGLE 0.0 entry because the interpolation
   ; formula does not do any extrapolations outside the range of the given
   ; minimum and maximum given :ANGLE entry.


  :COLUMNS
    ;    material key     tool key       !!!extra column :ANGLE
    ;                                       needed for interpolation
    (:MATERIAL :THICK :RADIUS :PISTON_ANG :ANGLE :ALLOWANCE)
  :UNITS
    (nil       :mm    :mm     :deg        :deg   :mm)
  :CONTENTS
   (    
    ;; Zincor 1.00 

    ("UST 1203" 1.00   0.2    30           0.0    0.00)
    ("UST 1203" 1.00   0.2    30          90.0   -1.59)
    ("UST 1203" 1.00   0.2    30         120.0   -2.49)
    ("UST 1203" 1.00   0.4    30           0.0    0.00)
    ("UST 1203" 1.00   0.4    30          90.0   -1.75)
    ("UST 1203" 1.00   0.4    30         120.0   -2.63)
    ("UST 1203" 1.00   0.8    30           0.0    0.00)
    ("UST 1203" 1.00   0.8    30          90.0   -1.87)
    ("UST 1203" 1.00   0.8    30         120.0   -2.81)
    ("UST 1203" 1.00   1.6    30           0.0    0.00)
    ("UST 1203" 1.00   1.6    30          90.0   -2.10)
    ("UST 1203" 1.00   1.6    30         120.0   -3.15)
    ("UST 1203" 1.00   2.4    30           0.0    0.00)
    ("UST 1203" 1.00   2.4    30          45.0   -1.20)
    ("UST 1203" 1.00   2.4    30          90.0   -2.40)
    ("UST 1203" 1.00   2.4    30         120.0   -3.60)

    ;; Zincor 1.25 

    ("UST 1203" 1.25   0.2    30           0.0    0.00)
    ("UST 1203" 1.25   0.2    30          90.0   -1.93)
    ("UST 1203" 1.25   0.2    30         120.0   -2.84)
    ("UST 1203" 1.25   0.4    30           0.0    0.00)
    ("UST 1203" 1.25   0.4    30          90.0   -1.98)
    ("UST 1203" 1.25   0.4    30         120.0   -2.97)
    ("UST 1203" 1.25   0.8    30           0.0    0.00)
    ("UST 1203" 1.25   0.8    30          90.0   -2.11)
    ("UST 1203" 1.25   0.8    30         120.0   -3.17)
    ("UST 1203" 1.25   1.6    30           0.0    0.00)
    ("UST 1203" 1.25   1.6    30          90.0   -2.37)
    ("UST 1203" 1.25   1.6    30         120.0   -3.56)
    ("UST 1203" 1.25   2.4    30           0.0    0.00)
    ("UST 1203" 1.25   2.4    30          45.0   -1.35)
    ("UST 1203" 1.25   2.4    30          90.0   -2.70)
    ("UST 1203" 1.25   2.4    30         120.0   -4.11)

    ;; Zincor 1.50 

    ("UST 1203" 1.50   0.2    30           0.0    0.00)
    ("UST 1203" 1.50   0.2    30          90.0   -2.28)
    ("UST 1203" 1.50   0.2    30         120.0   -3.43)
    ("UST 1203" 1.50   0.4    30           0.0    0.00)
    ("UST 1203" 1.50   0.4    30          90.0   -2.36)
    ("UST 1203" 1.50   0.4    30         120.0   -3.54)
    ("UST 1203" 1.50   0.8    30           0.0    0.00)
    ("UST 1203" 1.50   0.8    30          90.0   -2.51)
    ("UST 1203" 1.50   0.8    30         120.0   -3.77)
    ("UST 1203" 1.50   1.6    30           0.0    0.00)
    ("UST 1203" 1.50   1.6    30          90.0   -2.82)
    ("UST 1203" 1.50   1.6    30         120.0   -4.23)
    ("UST 1203" 1.50   2.4    30           0.0    0.00)
    ("UST 1203" 1.50   2.4    30          45.0   -1.60)
    ("UST 1203" 1.50   2.4    30          90.0   -3.21)
    ("UST 1203" 1.50   2.4    30         120.0   -4.80)

    ;; Aluminum 1.00 

    ("AA 5052" 1.00   0.4    30            0.0    0.00)
    ("AA 5052" 1.00   0.4    30           90.0   -1.75)
    ("AA 5052" 1.00   0.8    30            0.0    0.00)
    ("AA 5052" 1.00   0.8    30           90.0   -1.87)
    ("AA 5052" 1.00   1.6    30            0.0    0.00)
    ("AA 5052" 1.00   1.6    30           90.0   -2.10)
    ("AA 5052" 1.00   2.4    30            0.0    0.00)
    ("AA 5052" 1.00   2.4    30           45.0   -1.20)
    ("AA 5052" 1.00   2.4    30           90.0   -2.45)
    ("AA 5052" 1.00   2.4    30          120.0   -3.60)

    ;; Aluminum 1.60 

    ("AA 5052" 1.60   0.4    30            0.0    0.00)
    ("AA 5052" 1.60   0.4    30           45.0   -1.35)
    ("AA 5052" 1.60   0.4    30           90.0   -2.55)
    ("AA 5052" 1.60   0.6    30            0.0    0.00)
    ("AA 5052" 1.60   0.6    30           90.0   -2.61)
    ("AA 5052" 1.60   0.8    30            0.0    0.00)
    ("AA 5052" 1.60   0.8    30           90.0   -2.67)
    ("AA 5052" 1.60   1.6    30            0.0    0.00)
    ("AA 5052" 1.60   1.6    30           90.0   -2.90)
    ("AA 5052" 1.60   2.4    30            0.0    0.00)
    ("AA 5052" 1.60   2.4    30           45.0   -1.60)
    ("AA 5052" 1.60   2.4    30           90.0   -3.25)
    ("AA 5052" 1.60   2.4    30           90.0   -4.80)
   )
  :KEY
    (:MATERIAL :THICK :RADIUS :PISTON_ANG :ANGLE)
)


;;===    Addition TECHSOFT      WoHo 03.10.2011
;; hier anpassen dworschak

(sha-define-shop-table "Techsoft_Biegen_Abzugswerte_Tabelle"
  :TABLE-TITLE "TS Abzugswerte"
  :COLUMNS
    (:MATERIAL :THICK :RADIUS :ANGLE :ALLOWANCE)
  :UNITS
    (nil       :mm    :mm     :deg   :mm)
  :CONTENTS
   (    
    ;; Stahl 

   ; ("Stahl"    1.00   1.00     0     0.00)
   ; ("Stahl"    1.00   1.00    30    -0.36)
   ; ("Stahl"    1.00   1.00    45    -0.54)
   ; ("Stahl"    1.00   1.00    60    -0.86)
   ; ("Stahl"    1.00   1.00    90    -1.85)
   ; ("Stahl"    1.00   1.00   120    -0.95)
	;
   ; ("Stahl"    1.25   1.25     0     0.00)
   ; ("Stahl"    1.25   1.25    30    -0.47)
   ; ("Stahl"    1.25   1.25    45    -0.67)
   ; ("Stahl"    1.25   1.25    60    -1.06)
   ; ("Stahl"    1.25   1.25    90    -2.32)
   ; ("Stahl"    1.25   1.25   120    -1.21)
	;
	;
   ; ("Stahl"    1.50   1.50     0     0.00)
   ; ("Stahl"    1.50   1.50    30    -0.54)
   ; ("Stahl"    1.50   1.50    45    -0.83)
   ; ("Stahl"    1.50   1.50    60    -1.27)
   ; ("Stahl"    1.50   1.50    90    -2.61)
   ; ("Stahl"    1.50   1.50   120    -1.47)
	
    ("Stahl"    2.00   2.00     0     0.00)
	("Stahl"    2.00   2.00    30    -0.97)   ;original  ("Stahl"    2.00   2.00    30    -0.73)    
	("Stahl"    2.00   2.00    45    -1.44)   ;original  ("Stahl"    2.00   2.00    45    -1.08)    
	("Stahl"    2.00   2.00    60    -2.26)   ;original  ("Stahl"    2.00   2.00    60    -1.70)    
    ("Stahl"    2.00   2.00    90    -4.45)   ;original  ("Stahl"    2.00   2.00    90    -3.60)
	("Stahl"    2.00   2.00   120    -1.86)
 
	
    ("Stahl"    2.50   2.50     0     0.00)
    ("Stahl"    2.50   2.50    30    -0.91)
    ("Stahl"    2.50   2.50    45    -1.35)
    ("Stahl"    2.50   2.50    60    -2.10)
    ("Stahl"    2.50   2.50    90    -4.39)
    ("Stahl"    2.50   2.50   120    -2.34)
	
    ("Stahl"    3.00   3.00     0     0.00) ;original ("Stahl"    3.00   3.00     0     0.00)
    ("Stahl"    3.00   3.00    30    -1.52) ;original ("Stahl"    3.00   3.00    30    -1.15)
    ("Stahl"    3.00   3.00    45    -2.13) ;original ("Stahl"    3.00   3.00    45    -1.61)
    ("Stahl"    3.00   3.00    60    -3.32) ;original ("Stahl"    3.00   3.00    60    -2.50)
    ("Stahl"    3.00   3.00    90    -5.46) ;original ("Stahl"    3.00   3.00    90    -5.46)
    ("Stahl"    3.00   3.00   120    -2.76) ;original ("Stahl"    3.00   3.00   120    -2.76)
	
    ("Stahl"    4.00   4.00     0     0.00) ;original ("Stahl"    4.00   4.00     0     0.00)
    ("Stahl"    4.00   4.00    30    -1.93) ;original ("Stahl"    4.00   4.00    30    -1.48)
    ("Stahl"    4.00   4.00    45    -2.73) ;original ("Stahl"    4.00   4.00    45    -2.10)
    ("Stahl"    4.00   4.00    60    -4.27) ;original ("Stahl"    4.00   4.00    60    -3.28)
    ("Stahl"    4.00   4.00    90    -9.30) ;original ("Stahl"    4.00   4.00    90    -7.13)
    ("Stahl"    4.00   4.00   120    -3.71) ;original ("Stahl"    4.00   4.00   120    -3.71)
	
    ("Stahl"    5.00   5.00     0     0.00)
    ("Stahl"    5.00   5.00    30    -1.91)
    ("Stahl"    5.00   5.00    45    -2.62)
    ("Stahl"    5.00   5.00    60    -4.06)
    ("Stahl"    5.00   5.00    90    -8.92)
    ("Stahl"    5.00   5.00   120    -4.63)
	
    ("Stahl"    6.00   6.00     0     0.00) ;original ("Stahl"    6.00   6.00     0     0.00)
    ("Stahl"    6.00   6.00    30    -3) 	;original ("Stahl"    6.00   6.00    30    -2.26)
    ("Stahl"    6.00   6.00    45    -4.07) ;original ("Stahl"    6.00   6.00    45    -3.07)
    ("Stahl"    6.00   6.00    60    -6.29) ;original ("Stahl"    6.00   6.00    60    -4.74)
    ("Stahl"    6.00   6.00    90   -13.7) 	;original ("Stahl"    6.00   6.00    90   -10.32)
    ("Stahl"    6.00   6.00   120    -5.63) ;original ("Stahl"    6.00   6.00   120    -5.63)
	
    ("Stahl"    8.00   8.00     0     0.00)
    ("Stahl"    8.00   8.00    30    -2.73)
    ("Stahl"    8.00   8.00    45    -4.62)
    ("Stahl"    8.00   8.00    60    -7.07)
    ("Stahl"    8.00   8.00    90   -15.36)
    ("Stahl"    8.00   8.00   120    -8.30)

	("Stahl"    10.00   10.00     0     0.00)
    ("Stahl"    10.00   10.00    30    -3.00)
    ("Stahl"    10.00   10.00    45    -5.00)
    ("Stahl"    10.00   10.00    60    -7.5)
    ("Stahl"    10.00   10.00    90   -16.0)
    ("Stahl"    10.00   10.00   120    -9.00)
	
    ;; Niro 	
	
    ("Niro"     1.00   1.00     0     0.00)
    ("Niro"     1.00   1.00    30    -0.36)
    ("Niro"     1.00   1.00    45    -0.56)
    ("Niro"     1.00   1.00    60    -0.90)
    ("Niro"     1.00   1.00    90    -1.92)
    ("Niro"     1.00   1.00   120    -0.94)
	
    ("Niro"     1.50   1.50     0     0.00)
    ("Niro"     1.50   1.50    30    -0.55)
    ("Niro"     1.50   1.50    45    -0.82)
    ("Niro"     1.50   1.50    60    -1.28)
    ("Niro"     1.50   1.50    90    -2.72)
    ("Niro"     1.50   1.50   120    -1.47)	

    ("Niro"     2.00   2.00     0     0.00)
    ("Niro"     2.00   2.00    30    -0.72)
    ("Niro"     2.00   2.00    45    -1.11)
    ("Niro"     2.00   2.00    60    -1.75)
    ("Niro"     2.00   2.00    90    -3.69)
    ("Niro"     2.00   2.00   120    -1.83)
	
    ("Niro"     3.00   3.00     0     0.00)
    ("Niro"     3.00   3.00    30    -1.14)
    ("Niro"     3.00   3.00    45    -1.65)	
    ("Niro"     3.00   3.00    60    -2.57)
    ("Niro"     3.00   3.00    90    -5.56)
    ("Niro"     3.00   3.00   120    -2.73)
	
    ;; Alu
 
    ("Alu"      1.00   1.00     0     0.00)
    ("Alu"      1.00   1.00    30    -0.37)
    ("Alu"      1.00   1.00    45    -0.54)
    ("Alu"      1.00   1.00    60    -0.87)
    ("Alu"      1.00   1.00    90    -1.91)
    ("Alu"      1.00   1.00   120    -0.96)
	
    ("Alu"      1.50   1.50     0     0.00)
    ("Alu"      1.50   1.50    30    -0.55)
    ("Alu"      1.50   1.50    45    -0.84)
    ("Alu"      1.50   1.50    60    -1.30)
    ("Alu"      1.50   1.50    90    -2.67)
    ("Alu"      1.50   1.50   120    -1.46)
	
    ("Alu"      2.00   2.00     0     0.00)
    ("Alu"      2.00   2.00    30    -0.75)
    ("Alu"      2.00   2.00    45    -1.07)
    ("Alu"      2.00   2.00    60    -1.65)
    ("Alu"      2.00   2.00    90    -3.44)
    ("Alu"      2.00   2.00   120    -1.94)
	
    ("Alu"      3.00   3.00     0     0.00)
    ("Alu"      3.00   3.00    30    -1.11)
    ("Alu"      3.00   3.00    45    -1.63)	
    ("Alu"      3.00   3.00    60    -2.50)
    ("Alu"      3.00   3.00    90    -5.20)
    ("Alu"      3.00   3.00   120    -2.89)
	
    ("Alu"      4.00   4.00     0     0.00)
    ("Alu"      4.00   4.00    30    -1.58)
    ("Alu"      4.00   4.00    45    -2.19)		
    ("Alu"      4.00   4.00    60    -3.37)
    ("Alu"      4.00   4.00    90    -7.23)
    ("Alu"      4.00   4.00   120    -3.81)
	
    )
  :KEY
    (:MATERIAL :THICK :RADIUS :ANGLE)
)


(sha-define-shop-table "general_allowances"

  :TABLE-TITLE "Allgemeine Einzugswerte"
   ; general allowance values to be used together with an interpolation
   ; formula e.g. "linear_interp" formula (file:demoshop_func.lsp).
   ; The table can be used for any bend process which has the :RADIUS 
   ; (and optional :ANGLE) as :KEY entries or it can be used as a fallback
   ; allowance calculation together with the interpolation formula
   ; "linear_interp" (or similar), which takes the :RADIUS with it's value as
   ; default tool key if no tool key is given (which is the case when the 
   ; formula is called as fall-back formula).
   ;
   ; Even if the regular tool key does not contain an :ANGLE column this table
   ; needs the extra column: :ANGLE for the interpolation
   ; and should usually start with an ANGLE 0.0 entry because the  example
   ; interpolation formula (linear_interp) does not do any extrapolations 
   ; outside the range of the given minimum and maximum given :ANGLE entry.

  :COLUMNS
    ;    material key  tool key
    ;                
    (:MATERIAL :THICK :RADIUS :ANGLE :ALLOWANCE)
  :UNITS
    (nil       :mm    :mm     :deg   :mm)
  :CONTENTS
   (    
    ;; Zincor 1.00 

    ("UST 1203" 1.00   0.4     0.0    0.00)
    ("UST 1203" 1.00   0.4    90.0   -1.75)
    ("UST 1203" 1.00   0.4   120.0   -2.63)
    ("UST 1203" 1.00   0.8     0.0    0.00)
    ("UST 1203" 1.00   0.8    90.0   -1.87)
    ("UST 1203" 1.00   0.8   120.0   -2.81)
    ("UST 1203" 1.00   1.6     0.0    0.00)
    ("UST 1203" 1.00   1.6    90.0   -2.10)
    ("UST 1203" 1.00   1.6   120.0   -3.15)
    ("UST 1203" 1.00   2.4     0.0    0.00)
    ("UST 1203" 1.00   2.4    45.0   -1.20)
    ("UST 1203" 1.00   2.4    90.0   -2.40)
    ("UST 1203" 1.00   2.4   120.0   -3.60)

    ;; Zincor 1.25 

    ("UST 1203" 1.25   0.4     0.0    0.00)
    ("UST 1203" 1.25   0.4    90.0   -1.98)
    ("UST 1203" 1.25   0.4   120.0   -2.97)
    ("UST 1203" 1.25   0.8     0.0    0.00)
    ("UST 1203" 1.25   0.8    90.0   -2.11)
    ("UST 1203" 1.25   0.8   120.0   -3.17)
    ("UST 1203" 1.25   1.6     0.0    0.00)
    ("UST 1203" 1.25   1.6    90.0   -2.37)
    ("UST 1203" 1.25   1.6   120.0   -3.56)
    ("UST 1203" 1.25   2.4     0.0    0.00)
    ("UST 1203" 1.25   2.4    45.0   -1.35)
    ("UST 1203" 1.25   2.4    90.0   -2.70)
    ("UST 1203" 1.25   2.4   120.0   -3.05)

    ;; Zincor 1.50 

    ("UST 1203" 1.50   0.4     0.0    0.00)
    ("UST 1203" 1.50   0.4    90.0   -2.36)
    ("UST 1203" 1.50   0.4   120.0   -3.54)
    ("UST 1203" 1.50   0.8     0.0    0.00)
    ("UST 1203" 1.50   0.8    90.0   -2.51)
    ("UST 1203" 1.50   0.8   120.0   -3.77)
    ("UST 1203" 1.50   1.6     0.0    0.00)
    ("UST 1203" 1.50   1.6    90.0   -2.82)
    ("UST 1203" 1.50   1.6   120.0   -4.23)
    ("UST 1203" 1.50   2.4     0.0    0.00)
    ("UST 1203" 1.50   2.4    45.0   -1.60)
    ("UST 1203" 1.50   2.4    90.0   -3.21)
    ("UST 1203" 1.50   2.4   120.0   -4.80)

    ;; Aluminum 1.00 

    ("AA 5052" 1.00   0.4      0.0    0.00)
    ("AA 5052" 1.00   0.4     90.0   -1.75)
    ("AA 5052" 1.00   0.8      0.0    0.00)
    ("AA 5052" 1.00   0.8     90.0   -1.87)
    ("AA 5052" 1.00   1.6      0.0    0.00)
    ("AA 5052" 1.00   1.6     90.0   -2.10)
    ("AA 5052" 1.00   2.4      0.0    0.00)
    ("AA 5052" 1.00   2.4     45.0   -1.20)
    ("AA 5052" 1.00   2.4     90.0   -2.45)
    ("AA 5052" 1.00   2.4    120.0   -3.60)

    ;; Aluminum 1.60 

    ("AA 5052" 1.60   0.4      0.0    0.00)
    ("AA 5052" 1.60   0.4     45.0   -1.35)
    ("AA 5052" 1.60   0.4     90.0   -2.55)
    ("AA 5052" 1.60   0.6      0.0    0.00)
    ("AA 5052" 1.60   0.6     90.0   -2.61)
    ("AA 5052" 1.60   0.8      0.0    0.00)
    ("AA 5052" 1.60   0.8     90.0   -2.67)
    ("AA 5052" 1.60   1.6      0.0    0.00)
    ("AA 5052" 1.60   1.6     90.0   -2.90)
    ("AA 5052" 1.60   2.4      0.0    0.00)
    ("AA 5052" 1.60   2.4     45.0   -1.60)
    ("AA 5052" 1.60   2.4     90.0   -3.25)
    ("AA 5052" 1.60   2.4     90.0   -4.80)
   )
  :KEY
    (:MATERIAL :THICK :RADIUS :ANGLE)
)

(sha-define-shop-table "check_distances"

  ; this table contains the minimal distances which should be checked during 
  ; the design process. For each combination of two processes it lists 
  ; a factor and a minimal distance. The formula listed at the
  ; :DISTANCE-FORMULA entry takes these entries to calculate the real distance
  ; which is valid for the actual material
  ;
  ; The example formula 'check-dist' here calculates the final
  ; distance with: 
  ;                  Check-distance = Factor * Material-thickness
  ;                  IF (Check-distance < Minimal-distance)
  ;                    Check-distance = Minimal-distance
  ;                  END-IF
  ;
  ; The "anonymous" process refers to all edges on the checked face which
  ; can not be classified to any of the Sheet Metal processes, e.g. the edges
  ; of the sheet border.
  ;
  ; NOTE:          
  ; Currently all bend processes, the offset process and the hem process will
  ; not be differentiated in the check. The first bend process entry (here
  ; "air_bending") will be taken as the relevant entry for these group of
  ; processes. Therefore other bend processes are currently not listed in
  ; this table.

  :TABLE-TITLE "Abstände prüfen"

:COLUMNS
    (:PROCESS     :FACT1 :FACT2  :FACT3  :FACT4   :MIND1  :MIND2  :MIND3  :MIND4 )
  :COLUMN-NAMES
    ("Prozess"   "Anonym"  "Stanz" "Präg" "FreiBieg" "Anon"  "Stanz" "Präg" "FreiBieg") 
  :UNITS
   ( nil          nil    nil     nil     nil      :mm     :mm     :mm     :mm   )
  :CONTENTS
  (
   ("anonymous"   0      2.5     5.0     0        0       3.2     6.4     0     )
   ("punches"     2.5    2.5     5.0     5.0      3.2     3.2     6.4     6.4   )
   ("stamps"      5.0    5.0     5.0     5.0      6.4     6.4     6.4     6.4   )
   ("air_bending" 0      5.0     5.0     0        0       6.4     6.4     0     )
  )
  :DIST-FORMULA check-dist
  :BROWSER-ICON  "icon_checkdist"
)

; Default settings for this shop

(sha-define-default-settings "Neue Herbold"
  (UNITS 1.0 :mm) ; Set units for following default data.
                  ; Save and reset of current units will be done
                  ; automatically at begin respectively end of
                  ; sha-define-default-settings.
                  ; values in tool keys are ALWAYS in internal units [mm/rad/g]

;;===    Addition TECHSOFT      WoHo 03.10.2011
  (sha_set_default_material           "Stahl")
  (sha_set_default_material_thickness 2.00)
  (sha_set_default_bend_process       "NeueHerbold_Biegen_Tabelle")


  ; Following three alternatives for default bend radius definition may be used.
 ; The system seeks for a default radius definition in following sequence:
 ; 1. seek for a process specific definition, (see example :DEFAULT-FUNCTION
 ;    entry at air_bending)
 ; if not found 
 ; 2. seek for a shopwide default function definition (see below)
 ; if not found
 ; 3 take the fixed value given by sha_set_default_bend_radius (see below)
  (sha_set_default_bend_radius        1.50)
  (sha-set-default-bend-radius-function 'sha-default-bend-radius)

  ;; Special for "New By Connect" function. 
  (sha-set-default-connect-bend-process "bend_folding")
  (sha-set-default-connect-bend-radius  0.2)
  (sha_set_default_hem_process        "hems")
  (sha_set_default_hem_dist           1.25)
  (sha_set_default_relief_type        "rnd_relief")
  (sha_set_default_relief_tool        '(:VERT 1.6))
  (sha_set_default_relief_depth       1.0)
  (sha_set_default_miter_relief_type  "miter-chamfer")
  (sha_set_default_miter_relief_tool  '(:PERCT_DEPTH 100))
 ;(sha_set_default_corner_relief_type "sqr_corner_relief")
 ;(sha_set_default_corner_relief_tool '(:LENGTH 2.5))
  (sha_set_default_corner_relief_type "min_perp_v_corner_relief")
  (sha_set_default_corner_relief_tool (list :MIN_WIDTH 1 :MIN_LENGTH 3 
                                        :MIN_ANGLE (sd-deg-to-rad 10)))
  (sha-set-default-corner-gap-distance 0.1) ; gap distance between two lips 
  (sha-set-default-corner-long-lip-offset 0) ; offset of longer lip to neighbour
                                             ; lip outer face
  (sha-set-default-corner-constellation :SYMMETRIC) ; corner constellation default
                                                    ; alternatives are: 
                                                    ; :SYMMETRIC :THIS_LIP_LONG :OTHER_LIP_LONG
                                                    ; :THIS_LIP_HALF_LONG :OTHER_LIP_HALF_LONG
                        
  (sha_set_default_bodycheck_flag     :ON)

  ;; The following settings are used for the flat generation. It defines
  ;; part of the environment for the flat generation.
  (sha_set_sharp_bend_radius          0.4)

  ;; set colors depending on current color scheme.
  ;; Please note, these colors do not change if color scheme is changed.
  ;; Colors should be set here according to customers needs and preferred color scheme.

  (if (equal (sha-get-color-scheme-for-flat) :COCREATE17_0)
    (progn
      (sha_set_flat_default_color                 (rgb-color 1.0 1.0 1.0))
      (sha_set_flat_fold_up_color                 (rgb-color 0.0 1.0 1.0))
      (sha_set_flat_fold_down_color               (rgb-color 0.0 1.0 1.0))
      (sha_set_flat_error_color                   (rgb-color 1.0 0.0 0.0))
      ;; The general setting for tool colors is obsolete with version 2002+ (11.50)
      ;; (sha_set_flat_tool_color                    (rgb-color 0.0 1.0 0.0))
      ;; Instead indivual color settings can be used:
      (sha_set_flat_punch_up_color                (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_punch_down_color              (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_stamp_up_color                (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_stamp_down_color              (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_bend_relief_color             (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_corner_relief_color           (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_no_bend_relief_color          (rgb-color 1.0 1.0 0.0))
      (sha_set_flat_no_corner_relief_color        (rgb-color 1.0 1.0 0.0))
      (sha_set_flat_punch_up_label_color          (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_punch_down_label_color        (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_stamp_up_label_color          (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_stamp_down_label_color        (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_bend_relief_label_color       (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_corner_relief_label_color     (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_punch_up_ref_point_color      (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_punch_down_ref_point_color    (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_stamp_up_ref_point_color      (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_stamp_down_ref_point_color    (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_bend_relief_ref_point_color   (rgb-color 0.0 1.0 0.0))
      (sha_set_flat_corner_relief_ref_point_color (rgb-color 0.0 1.0 0.0))
    )
    ;; else Creo or user defined color scheme
    (progn
      (sha_set_flat_default_color                 (rgb-color 0.0 0.0 0.0))
      (sha_set_flat_fold_up_color                 (rgb-color 0.0 0.0 1.0))
      (sha_set_flat_fold_down_color               (rgb-color 0.0 0.0 1.0))
      (sha_set_flat_error_color                   (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_punch_up_color                (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_punch_down_color              (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_stamp_up_color                (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_stamp_down_color              (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_bend_relief_color             (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_corner_relief_color           (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_no_bend_relief_color          (rgb-color 1.0 0.5 0.0))
      (sha_set_flat_no_corner_relief_color        (rgb-color 1.0 0.5 0.0))
      (sha_set_flat_punch_up_label_color          (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_punch_down_label_color        (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_stamp_up_label_color          (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_stamp_down_label_color        (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_bend_relief_label_color       (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_corner_relief_label_color     (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_punch_up_ref_point_color      (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_punch_down_ref_point_color    (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_stamp_up_ref_point_color      (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_stamp_down_ref_point_color    (rgb-color 1.0 0.0 0.0))
      (sha_set_flat_bend_relief_ref_point_color   (rgb-color 0.0 0.5 0.0))
      (sha_set_flat_corner_relief_ref_point_color (rgb-color 0.0 0.5 0.0))
    )
  )    

  (sha_set_flat_fold_up_linetype      0)                       ; set to solid
  (sha_set_flat_fold_down_linetype    1)                       ; set to dashed
  (sha_set_flat_text_size             3.5)                     ; std text size
  (sha_set_flat_default_linewidth     0.0)                     ; no line width
  (sha_set_flat_tool_linewidth        0.0)                     ; no line width
  (sha_set_flat_error_linewidth       0.0)                     ; no line width
  (sha_set_flat_fold_up_linewidth     0.0)                     ; no line width
  (sha_set_flat_fold_down_linewidth   0.0)                     ; no line width
  (sha_set_flat_show_labels           t)                       ; all labels
  ;;(sha_set_flat_show_reference_point  t)                     ; feature ref.pt.
  (sha_set_flat_show_punch_ref_point          t)               ; show ref pts
  (sha_set_flat_show_stamp_ref_point          t)               ; for individual 
  (sha_set_flat_show_bend_relief_ref_point    t)               ; feature 
  (sha_set_flat_show_corner_relief_ref_point  t)               ; types
  (sha_set_flat_workplane_offset      0)                       ; flat WP pos.
  (sha_set_flat_part_name             "sha-flat")              ; Drafting part name
  (sha_set_flat_trim_bend_lines       t)                       ; cut bend lines
                                                               ; at geometry
  (sha_set_flat_single_bend_text      nil)                     ; one text per
                                                               ; bend feature if
                                                               ; bend is split
  (sha_set_flat_merge_bend_geometry   nil)                     ; Merge Vertices
                                                               ; of bend area
  (sha_set_flat_show_no_relief_info   nil)                     ; do not show no 
                                                               ; relief geometry
  (sha_set_show_mergeable_lines       nil)                     ; bend bordrs off

  (sha_set_flat_cont_at_tang_faces      t)                     ; continue at tangential faces
                                                               ; switch off for side face handling
                                                               ; with blended sides or special tools
                                                               ; with blends to base face.

  (sha_set_flat_sheet_border_handling               t) ; special handling of side faces at border
  (sha_set_flat_sheet_border_show_inner_contour     t)
  (sha_set_flat_sheet_border_show_detail_contour    t)
  (sha_set_flat_sheet_border_inner_contour_color    (rgb-color 0.5 0.5 1.0)) 
  (sha_set_flat_sheet_border_detail_contour_color   (rgb-color 0.9 0.9 0.9))
  
  (sha_set_flat_tool_handling                       t) ; special handling of side faces of tools
  (sha_set_flat_planar_tools_calculate_hidden_lines t) 
  (sha_set_flat_planar_tools_calculate_silhouette   t) 
  (sha_set_flat_planar_tools_hidden_linetype        1) ; 1 - dashed
  (sha_set_flat_planar_tools_silhouette_linetype    7) ; 7 - dotted
  (sha_set_flat_feature_detail                      t) 
  (sha_set_flat_feature_standard_colors             t) 
  (sha_set_flat_tool_show_boundary                  t) 
  (sha_set_flat_tool_show_detail                    t) 
  (sha_set_flat_tool_boundary_color                 (rgb-color 0.5 0.5 1.0))
  (sha_set_flat_tool_detail_color                   (rgb-color 0.9 0.9 0.9))
  (sha_set_flat_hide_inner_bottom_edges 		  nil)
  (sha-enable-replacements)                                    ; Enable replace-
  (sha-enable-relief-replacement)                              ; ments of relief
  (sha-enable-corner-relief-replacement)                       ; feat. in flat
  (sha-set-costmodel-function         'sha-cost-model)         ; set costmodel
  (sha-set-fold-bspline-mark-color :on)	                       ; :on=color freeform bend side faces
  (sha-set-default-by-solid-gap-distance 0.1)                  ; default gap distance in new-sheet-by-solid
)

;;; For above linetype setting:
;;; Valid linetypes are:
;;;
;;; 0 = SOLID         1 = DASH                2 = DOT
;;; 3 = DASH_DOT      4 = DASH_DOT_DOT        5 = LONG_DASH
;;; 6 = CENTER_DASH   7 = CENTER_DASH_DASH    8 = PHANTOM
;;;
;;; For further infos refer to
;;; Creo Elements/Direct Drafting Interfacing documentation

; Out-comment the following line if you don't want this shop becoming the
; default shop when loading this file
 (sha_set_current_shop "Neue Herbold")

(sha-default-settings "Neue Herbold")
