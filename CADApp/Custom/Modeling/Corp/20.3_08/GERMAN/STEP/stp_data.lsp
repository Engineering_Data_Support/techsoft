; ------------------------------------------------------------------------------
; SVN: $Id: sd_data.lsp 24257 2011-05-27 07:20:42Z pjahn $
; customization file for creo elements/direct modeling 18 - STEP data adapter
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "18.0")
(persistent-data-module "STEP")
;;switch to classic kernel (instead of granite)
(persistent-data
 :key "STEP_IMPORT"
 :value '( :MUTUAL-EXCLUSION-VARIABLES ( :PROCESSOR-DIRECT ) ) )

;; ----------- end of file -----------
