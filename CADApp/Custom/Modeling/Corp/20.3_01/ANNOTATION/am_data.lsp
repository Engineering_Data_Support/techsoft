;; Automatically written on 05-Mar-2021 13:04:02
;; Module ANNOTATION
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "ANNOTATION")

persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '(  ( "Annotation" :TECHSOFT ) ) )

(persistent-data
 :key "AM_CREATE_SYMBOL_DATUMT"
 :value '( :PROPOSALS ( :DATUM_BOTTOM ( "A" "" ) :DATUM_TOP ( "0.01" "" ) ) ) )
(persistent-data
 :key "AM_CREATE_TEXT"
 :value '( :PROPOSALS ( :ABS_ANGLE ( 0 1.5707963267948966 4.8405300038407999 
           4.8030488675854555 5.497787143782138 4.7123889803846897 
           2.3561944901923448 3.9269908169872414 0.7853981633974483 
           0.5235987755982988 3.1415926535897931 6.2831853071795862 ) :LINESP ( 
           2.2000000000000002 1.8 1.1000000000000001 1 ) :SLANT ( 0 ) :RATIO ( 1 0.8 0.9 
           0.66 1.25 ) :SIZE ( 3.5 2.5 3.3999999999999999 50 40 2 2.2000000000000002 
           7 14 10 5 1.5 3 20 30 8 45 ) ) ) )
(persistent-data
 :key "AM_TEXT_MODIFY"
 :value '( :PROPOSALS ( :SLANT ( 0 0.5235987755982988 ) :RATIO ( 1 10 ) :LINESP ( 5 50 ) 
           :ABS_ANGLE ( 0 0.0174532925199433 1.5707963267948966 -0.174532925199433 
           3.3161255787892259 1.7453292519943295 0.174532925199433 
           0.7853981633974483 2.3561944901923448 4.7123889803846897 
           0.2617993877991494 0.5235987755982988 3.1415926535897931 
           3.9269908169872414 5.497787143782138 6.2831853071795862 ) :SIZE ( 3 
           2.2000000000000002 2 2.5 0.1 1 3.2000000000000002 1.5 
           2.7999999999999998 3.2999999999999998 3.3999999999999999 0.001 3.5 
           2.6000000000000001 0.01 2.7000000000000002 15 2.2999999999999998 1.8 
           1.8999999999999999 ) ) ) )
(persistent-data
 :key "AM_CREATE_SYMBOL_TOLERANCE"
 :value '( :PROPOSALS ( :DATUM1 ( "A" "" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" 
           "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z" ) :DATUM3 ( "" 
           "A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" 
           "R" "S" "T" "U" "V" "W" "X" "Y" "Z" ) :DATUM2 ( "" "B" "A" "C" "D" "E" 
           "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P" "Q" "R" "S" "T" "U" "V" 
           "W" "X" "Y" "Z" ) :TOL-VALUE ( "0,5" "0.5" "0,05" "0,02" "1" "0.02" 
           "0.05" "0.1" "0.15" "0.2" "0,2" "0,15" "2.0" "4" "0.3" "0.04" "0.015" 
           "0.01" "" ) ) ) )
(persistent-data
 :key "TEMPLATE EXAMPLES SHOWN"
 :value '( :SURFACE T ) )
(persistent-data
 :key "AM_CREATE_SYMBOL_WELDING"
 :value '( :PROPOSALS ( :T2 ( "3" "" "5" "4" "10" "8" "2" "Nahtlänge 30-40mm" ) :T4 ( "" 
           "10x100 (63)" "dicht" "23x50" "50 (100)" "50 (100" "23x50 (50)" 
           "Nahtlänge 30-40mm" "9x100 (150)" "(50)" ) :T3 ( "" "10x100 (63)" 
           "32x50 (50)" "dicht" "50 (100)" "4x50" "50 (50)" "30x50 (50)" 
           "23x50 (50)" "23x50" "10x160 (165)" "9x100 (150)" 
           "3x um 120° versetzt" "4x40" "4x40 (50)" "8x40 (210)" "8x30 (~55)" 
           "3x100" "4x30 (50)" "9x30 (~70)" ) :T1 ( "3" "2" "" "5" "10" "4" "8" "6" 
           "1" "ausschweißen" "max.3" "1.5" ) ) ) )
(persistent-data
 :key "FRAME TYPE DEFAULT"
 :value "WM-3")
(persistent-data
 :key "DIMENSION FIXTEXTS: EXPANDED"
 :value T)
(persistent-data
 :key "AM_CREATE_BREAKOUT"
 :value '( :VALUES ( :OPTIONS T :REAL-PREVIEW NIL ) ) )
(persistent-data
 :key "LAST-STYLE-AM_CREATE_DIM_ARC"
 :value :NH)
(persistent-data
 :key "PLOT TRANSF"
 :value '( :PATTERN-STYLE :STANDARD :TRANSF-STYLE :BLACK_WHITE ) )
(persistent-data
 :key "VIEW DEFAULTS"
 :value '( :VIEW-VIEW-GAP ( :ABS 20 :REL 1 ) :CREATE-DRAWING-DEFAULT-VIEWS ( :LEFT 
           :TOP :FRONT ) :UPDATE-TYPE :UPDATE_WHOLE_DRAWING ) )


(persistent-data
 :key "AM_CREATE_SYMBOL_DATUM_RTL"
 :value '( :PROPOSALS ( :DATUM ( "A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" 
           "N" "O" "P" "Q" "R" "S" "T" "U" "V" "W" "X" "Y" "Z" ) ) ) )
(persistent-data
 :key "AM_TEXT_DEFINE"
 :value '( :VALUES ( :TEXT_CATEGORY "Neue Herbold - Werkstoffe" ) ) )
(persistent-data
 :key "LAST-STYLE-AM_CREATE_DIM_DTLONG"
 :value :NO_STYLE)
(persistent-data
 :key "STARTUPTIME"
 :value '( 20 43.025369619487599 20 ) )
(persistent-data
 :key "LAST-STYLE-AM_CREATE_DIM_SINGLE"
 :value :NO_STYLE)
(persistent-data
 :key "TEXT CREATE"
 :value '( :APPEARANCE T ) )
(persistent-data
 :key "DIMSLANTANGLE"
 :value 0.174532925199433)
(persistent-data
 :key "DIMENSION TOLERANCETEXTS: EXPANDED"
 :value T)
(persistent-data
 :key "VP-CONFIG"
 :value '( :AUX-3D-VP ( 37.0,17.0 1077.0,348.0 ) :ANNO-VP-WITH-3D ( -23.0,389.0 
           1077.0,348.0 ) :AUX-3D-VP-VISIBLE NIL :ANNO-VP ( -10.0,-33.0 1250.0,875.0 
           :MAXIMIZE ) ) )
(persistent-data
 :key "LAST-STYLE-AM_CREATE_DIM_CHAIN"
 :value :NO_STYLE)
(persistent-data
 :key "LAST-STYLE-AM_CREATE_DIM_RADIUS"
 :value :NH)
(persistent-data
 :key "DIM TOLERANCE PROPOSALS"
 :value '( :TOL_PM ( 0.25 0.1 0.1 0.05 0.2 2 0.3 0.5 0.1 ) :TOL_UL_LOWER ( 0.1 -0.1 
           0.1 5 0.1 0.7 25 -25 -50 "" 0.3 3 0.1 -0.1 0.1 0.1 0.1 0.1 -1 -0.1 
           0.05 -0.2 -0.42 0.1 0.2 0.1 0.5 0.1 -2 -0.5 0.1 0 "-.1" ) :TOL_UL_UPPER ( 
           0.1 -0.1 -0.1 0 -0.2 2 "" 0.1 0.03 65 235 "+0.3" 0.1 0.1 -1 0.5 1 0.1 
           -0.1 0.21 0.1 0.25 0.7 0.1 0.1 0.3 ".2" 0.2 ) ) )
(persistent-data
 :key "ATTACH DRAWING"
 :value '( :VARIANT T :TRUE-COPY T :MIRROR T ) )
(persistent-data
 :key "AM_MODIFY_BROKEN_VIEW"
 :value '( :VALUES ( :APPEARANCE T ) ) )
(persistent-data
 :key "HATCH PATTERN EXAMPLES SHOWN"
 :value T)
(persistent-data
 :key "DIM FIX TEXT PROPOSALS"
 :value '( :BASIC_POSTFIX ( "40" ) :SUBFIX ( "vorgebohrt <Diameter>7,8" 
           "(bei Montage geb.)" "Schw.nahtvorb." "Schweißnahtvorb." 
           "Schweißnahtvorbereitung" "D" "" ) :SUPERFIX ( "Schw.nahtvorb." "C" "" ) 
           :POSTFIX ( "vers.gez." "x5" "x4" "x3" "x2" "x1,5" "x1,25" "x1" "]" ")" 
           "=360°" "x45°" "" "E9" " blank drehen" "S" "m7" "m6" "m5" "k8" "k7" 
           "k6" "h11" "h10" "h9" "h8" "h7" "h6" "h5" "h4" "g8" "g7" "g6" "g5" 
           "f8" "f7" "f6" "e8" "e7" "e6" "K8" "K7" "K6" "H13" "H12" "H11" "H10" 
           "H9" "H8" "H7" ) :PREFIX ( "21x250=" "<Gewindeprofil>" "7x200=" "6x140=" 
           "6x160=" "3x220=" "3x225=" "NW" "5x300=" "16x <Gewindeprofil>" "(" 
           "A" "M" "<Diameter>" "" "2x Kegelstift <Diameter>" "2x" "3x" "6x" 
           "SW" "5x" "4x" "TR" "Ƣ" "~" "[" "Stern 1=<Diameter>" 
           "Stern 5=<Diameter>" "Stern 5 <Diameter>" "Messpunkte 1+2=" 
           "Messpunkt 1+2=" "MP1" "Rotormitte=" "Rotormitte =" "Rotormitte " 
           "Rotormitte" "Messpunkte= " "Messpunkte=" "DIN 5480-N" "DIN 5480 - N" 
           "R" "11x <Gewindeprofil>" "9x250=" "10x <Diameter>" "32x45=" "ca." 
           "18,25 Wendel a 200=" "18." "26 Wendel a 200=" "9x155=" ) ) )
(persistent-data
 :key "AM_POS_CREATE"
 :value '( :VALUES ( :CREATE_FROM :GEO ) ) )
(persistent-data
 :key "LAST-STYLE-DIM_COMMON"
 :value :NO_STYLE)
(persistent-data
 :key "CREATE DRAWING"
 :value '( :UP_DIR 0.0,1.0,0.0 :FR_DIR 0.0,0.0,-1.0 ) )
(persistent-data
 :key "AM_CREATE_SYMBOL_SURFACE"
 :value '( :PROPOSALS ( :E ( "" "0.3" ) :B ( "ISO 9013-231" "ISO 9013-342" "" "geätzt" 
           "geschliffen" "Herstellerzeichen" "ISO 9013-341" "gelasert" "Rohling" 
           "gefräst" "abgedreht" "gehont" "präzisionsgeschliffen" "verchromt" ) :F ( 
           "" "50" "25" "12.5" "3.2" "1.6" "0.8" "0.4" "0.2" "0.1" "0.05" 
           "0.025" "0.0125" ) :A2 ( "" "50" "25" "12.5" "3.2" "1.6" "0.8" "0.4" 
           "0.2" "0.1" "0.05" "0.025" "0.0125" ) :A1 ( "" "Rz 25" "Rz 6.3" "Rz16" 
           "Rz 0.8" "25" "6.3" "50" "12.5" "3.2" "1.6" "0.8" "0.4" "0.2" "0.1" 
           "0.05" "0.025" "0.0125" ) ) :VALUES ( :ROUGHNESS_UNIT :VARIOUS ) ) )
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "Annotation/Dimension/Specific/Thread/FormatStyle" :M ) ( 
           "Annotation/Plot/PensTrafoStyle" :TRUE_COLOR ) ( 
           "Annotation/General/ArrowStyle" :STANDARD ) ( 
           "Annotation/General/LineStyle" :SECTION_GEO ) ( 
           "Annotation/View/CaptionStyle" :STANDARD ) ( 
           "Annotation/Hatch/PatternStyle" :SCHWEIßNAHT ) ( 
           "Annotation/Plot/PrinterStyle" :A4Q ) ( "Annotation/View/FormatStyle" 
           :NUMBER_ONLY ) ( "Annotation/General/TextStyle" :STANDARD ) ( 
           "Annotation/Dimension" :NH ) ( "Annotation/Plot" :WIN_DEFAULT ) ( 
           "Annotation/General/SizeStyle" :STANDARD ) ( "Annotation/Plot/PageStyle" 
           :FIT ) ( "Annotation/Dimension/Text/FormatStyle" :MM ) ( "Annotation/Refline" 
           :NH ) ( "Annotation" :NH ) ) )
(persistent-data
 :key "DIMENSION CREATE"
 :value '( :APPEARANCE T ) )
(persistent-data
 :key "AM_CREATE_DEPENDENT_GENERAL"
 :value '( :VALUES ( :REAL-PREVIEW T ) ) )
(persistent-data
 :key "LAST-STYLE-AM_CREATE_DIM_SYMSIN"
 :value :NH)

;; ----------- end of file -----------
