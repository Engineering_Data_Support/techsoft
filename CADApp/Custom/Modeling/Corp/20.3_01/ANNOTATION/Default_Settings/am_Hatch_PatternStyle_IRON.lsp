;% Automatically written on 10/30/2014 at 08:32:34
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :IRON
  :title "Eisen"
  :values
   '("Description" "L:1, A: 45.0, D:   5.0"
     "PatternAngle" 0.78539816339744828
     "PatternDistance" 5.0
     "SubPattern" (:LABEL "Eisen" :SUBPATTERN
                     ((:COLOR 0.62352941176470589,0.62352941176470589,0.62352941176470589
                              :LINETYPE :SOLID :DISTANCE 1.0 :OFFSET
                              0.0 :ANGLE 0.0 :WIDTH 0)))
     )
)
