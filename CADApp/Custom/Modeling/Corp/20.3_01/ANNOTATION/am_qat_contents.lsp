;; Automatically written on 26-Feb-2021 08:51:44
(oli::sd-fluentui-set-quick-access-toolbar-contents "Annotation"
  '(
   (:AVAILCMD ("Annotation" "Filing" "New Session"))
   (:AVAILCMD ("Annotation" "Filing" "Load ..."))
   (:AVAILCMD ("Annotation" "Filing" "Save ..."))
   (:AVAILCMD ("Annotation" "Plot" "Plot"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "TECHSOFT" "Sheet Metal"))
   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))
   ))