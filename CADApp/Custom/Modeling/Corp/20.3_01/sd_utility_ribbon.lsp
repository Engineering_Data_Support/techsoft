;; Automatically written on Montag, März 19, 2018 at 11:32:29

(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Abstand"
			:splitButton t
			:hideLabel t
			:contents
			`(
				(:label "Abstand"
				 :availCmd ("SolidDesigner" "Measure" "Dist"))
				(:label "Abstand 2er Punkte"
				 :availCmd ("SolidDesigner" "Measure" "Dist by 2 Points"))
				(:label "Abstand zwischen 2 Elementen"
				 :availCmd ("SolidDesigner" "Measure" "Generic Distance"))
				(:label "Abstand in Richtung"
				 :availCmd ("SolidDesigner" "Measure" "Distance in Direction"))
				(:label "Kantenlänge"
				 :availCmd ("SolidDesigner" "Measure" "Edge Length"))
				(:label "Konturlänge"
				 :availCmd ("SolidDesigner" "Measure" "Contour Length"))
				(:label "Umfang"
				 :availCmd ("SolidDesigner" "Measure" "Perimeter of Face"))
				(:label "Abstand Punkt zu Kante"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Point to Edge"))
				(:label "Abstand Punkt zu Fläche"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Point to Face"))
				(:label "Abstand Punkt zu Kurve"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Point to Curve"))
				(:label "Abstand Punkt zu Oberfläche"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Point to Surface"))
				(:label "Abstand Fläche zu Fläche"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Face to Face"))
				(:label "Abstand Oberfläch zu Oberfläch"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Surf to Surf"))
				(:label "Abstand Kante zu Fläche"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Edge to Face"))
				(:label "Abstand Kante zu Oberfläche"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Edge to Surf"))
				(:label "Abstand Kante zu Kante"
				 :availCmd ("SolidDesigner" "Measure" "Dist of Edge to Edge"))
				(:separator t)
				(:label "Maximalabstand"
				 :availCmd ("SolidDesigner" "Measure" "Maximal Distance"))
				(:label "Radius"
				 :availCmd ("SolidDesigner" "Measure" "Radius of Edge"))
				(:label "Durchmesser"
				 :availCmd ("SolidDesigner" "Measure" "Diameter of Edge"))
			)
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "3D-Richtung"
			:splitButton t
			:hideLabel t
			:contents
			`(
				(:label "3D-Richtung"
				 :availCmd ("SolidDesigner" "Measure" "Direction 3D"))
				(:label "3D-Achse"
				 :availCmd ("SolidDesigner" "Measure" "Axis 3D"))
				(:label "3D-Vektor"
				 :availCmd ("SolidDesigner" "Measure" "Vector 3D"))
			)
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Winkel"
			:splitButton t
			:hideLabel t
			:contents
			`(
				(:label "Winkel"
				 :availCmd ("SolidDesigner" "Measure" "Angle"))
				(:label "Winkel zwischen Kanten"
				 :availCmd ("SolidDesigner" "Measure" "Angle of Edge"))
				(:label "Winkel zwischen Flächen"
				 :availCmd ("SolidDesigner" "Measure" "Angle of Face"))
				(:label "Winkel zwischen AEbenen"
				 :availCmd ("SolidDesigner" "Measure" "Angle of Workplane"))
				(:label "Winkel zwischen Richtungen"
				 :availCmd ("SolidDesigner" "Measure" "Angle of Direction"))
			)
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "&Wählen"
			:hideLabel t
			:availCmd '("All" "Select" "Select")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Sondenauswahl"
			:hideLabel t
			:availCmd '("SolidDesigner" "Select" "Flyby Probe")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Anno erstellen"
			:hideLabel t
			:availCmd '("SolidDesigner" "3D Documentation" "Create Anno")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "&2D löschen"
			:hideLabel t
			:availCmd '("SolidDesigner" "Delete" "Delete 2D")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "&3D löschen"
			:hideLabel t
			:availCmd '("SolidDesigner" "Delete" "Delete 3D")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Ansicht aus aktuell. AE"
			:splitButton t
			:hideLabel t
			:contents
			`(
				(:label "Ansicht aus aktuell. AE"
				 :availCmd ("All" "View" "View by Curr WP"))
				(:label "Trimetrische Ansicht"
				 :availCmd ("All" "View" "Trimetric View"))
				(:label "Nach Schnitt anzeigen"
				 :availCmd ("All" "View" "View by Clipping New"))
			)
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Ansicht auf Fläche"
			:hideLabel t
			:availCmd '("All" "View" "View by Face 2")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("SDSTRUCTURE" "UTILITIES")
			:annotationRibbon nil
			:userDefined t
			:label "Rechner"
			:hideLabel t
			:availCmd '("SolidDesigner" "Measure" "Calculator")
)
