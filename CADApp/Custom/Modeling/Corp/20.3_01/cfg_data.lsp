;; Automatically written on 05-Mar-2021 13:04:02
;; Module CONFIGURATION
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "CONFIGURATION")


;;avoid/allow saving of persistent data=====================
(persistent-data
 :key "SAVE-PERSISTENT-SETTINGS"
 :value T)
;; ----------- end of file -----------
