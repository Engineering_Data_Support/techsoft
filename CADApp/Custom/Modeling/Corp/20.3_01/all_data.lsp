;; Automatically written on 05-Mar-2021 13:04:02
;; Module ALL
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "ALL")
;;lock toolbars=============================================
(persistent-data
 :key "LOCK TOOLBARS"
 :value 0)



(persistent-data
 :key "TS-AM-STUECKLISTE-BROWSER-DS"
 :value '( :SIZE ( :WIDTH 876 :HEIGHT 434 ) ) )
(persistent-data
 :key "STARTUP MODULES"
 :value '( "IGESDEK" "STEP" "GRANITE" "ACIS_SAT" "STL" "CADCAM_LINK" 
           "BASICSHEETS" "ANNOTATION" "3DDOCU" "SDPOWER-FREE" "PART_LIBRARY" ) )
(persistent-data
 :key "TS-AM-PASS-SCAN-DIALOG"
 :value '( :MUTUAL-EXCLUSION-VARIABLES ( :AUF ) ) )
(persistent-data
 :key "CHECKANDRESOLVE-DS"
 :value '( :SIZE ( :WIDTH 618 :HEIGHT 234 ) ) )
(persistent-data
 :key "TS-CATALOG-MAIN-DS"
 :value '( :SIZE ( :WIDTH 893 :HEIGHT 725 ) ) )
(persistent-data
 :key "BUTTON-FACE-COLOR"
 :value 15790320)
(persistent-data
 :key "AUTO-SAVE"
 :value '( :TIME-INTERVAL 30 :FILENAME 
           "C:\\Users\\jhaa\\AppData\\Local\\Temp/modeling_autosave_jhaa" :ON T 
           :OUT-OF-MEM NIL :BACKUP-COUNT 10 :CONFIRM NIL ) )
(persistent-data
 :key "AM-FNTSYMS-DS"
 :value '( :SIZE ( :WIDTH 300 :HEIGHT 239 ) ) )
(persistent-data
 :key "CHECKANDRESOLVE"
 :value '( :VISIBLE T :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "TDB_BROWSER"
 :value '( :VISIBLE NIL :TABLE-SEC-ICONS NIL :TREE-SEC-ICONS NIL ) )
(persistent-data
 :key "OUTPUT-BOX-DS"
 :value '( :SIZE ( :WIDTH 698 :HEIGHT 335 ) ) )
(persistent-data
 :key "AVAILABLE-SHORTCUTS-BROWSER-DS"
 :value '( :SIZE ( :WIDTH 300 :HEIGHT 319 ) ) )
(persistent-data
 :key "GEO-RESOLUTION"
 :value '( :GEO-RESOLUTION 0.000001 :FINE-RESOLUTION-VALUES NIL ) )
(persistent-data
 :key "AM-BROWSER"
 :value '( :VISIBLE NIL :TABLE-SEC-ICONS NIL ) )
(persistent-data
 :key "IGNORE-CMD-ICONS"
 :value NIL)
(persistent-data
 :key "CLASH-ANALYSIS-CONFIGURATIONS-BROWSER"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "FILE-DIALOG"
 :value '( :ITEMS ( "Elementtyp 78 2" "Name 110 0" "Größe 50 3" "Datum 11c 1" 
           "Änderungsdatum 90 1" "Typ 78 2" "Markierungen 50 4" ) :VIEW-MODE 1 ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-REDRAW"
 :value '( :DISPLAY-LISTS NIL :OCCLUSION-CULLING NIL :PHONG-SHADING NIL 
           :SMOOTH-UPDATE T :REDRAW-TIMEOUT 2000 :SIMPLIFY-MODEL-FACTOR 40 
           :SIMPLIFY-MODEL :BOXED :SMOOTH-UPDATE-TIME 500 ) )
(persistent-data
 :key "TS-BOHRUNGEN-ERKENNEN"
 :value '( :VALUES ( :MUSTER-ERZEUGEN T ) ) )
(persistent-data
 :key "AM_ROT_CENTERLINE_HOLECIRCLE"
 :value '( :VALUES ( :MODE :BY_3_CLINES ) ) )
(persistent-data
 :key "COLOR-SELECTOR-DS"
 :value '( :POS ( :X 135 :Y 76 ) ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-DYNAMIC-VIEWING"
 :value '( :CTRL-LESS-THRESHOLD 4 :SIMPLIFY-MODEL :BOXED :CAE :CAE-DEFAULT 
           :WORKPLANE :WP-DEFAULT :CLIPPING :CLIP-NO-LINES :SCENERY :ALL 
           :MOUSE-INTERACTION-MODE :ON :REDRAW-TIMEOUT 0 :SIMPLIFY-MODEL-FACTOR 
           70 :LABEL :L3D-DEFAULT :3D-OBJECT :DEFAULT ) )
(persistent-data
 :key "WALL-THICKNESS-BROWSER"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "SHOWN-BROWSER-MODES"
 :value '( :FACESET-FEATURES T :536F6C6964506F776572 T :464541542D47726F757073 T ) )
(persistent-data
 :key "STARTUPDIR"
 :value "S:\\mi1\\Auftraege - Projekte\\Auftrag 060577 - Polykemi\\Einzelteile")
(persistent-data
 :key "VP-DEFAULT-SETTINGS-CLIPPING"
 :value '( :CLASH-COLOR 16711680 ) )
(persistent-data
 :key "TS-AM-STUECKLISTE-BROWSER"
 :value '( :TABLE-SEC-ICONS NIL :VISIBLE NIL ) )
(persistent-data
 :key "PATTERN_TYPE"
 :value :LINEAR)
(persistent-data
 :key "VP-DEFAULT-SETTINGS-FLY-BY"
 :value '( :TOOLTIP-DELAY 500 :HIGHLIGHT-DELAY 20 ) )
(persistent-data
 :key "AM-FNTSYMS"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-VIEW-BY"
 :value '( :WP-PAN-ZOOM-MODE :CENTER-CENTER :FACE-CENTER-FACE T 
           :FACE-UP-DIR-MODE :BEST ) )
(persistent-data
 :key "2D-DRAWING-STYLE"
 :value :NH)
(persistent-data
 :key "CUST-FEATURE-BROWSER-DS"
 :value '( :SIZE ( :WIDTH 478 :HEIGHT 345 ) ) )
(persistent-data
 :key "CHECK_PART"
 :value '( :VALUES ( :VERBOSE T :LABELS T ) :MUTUAL-EXCLUSION-VARIABLES ( 
           :MAXIMAL_CHECK ) ) )
(persistent-data
 :key "GLOBAL-SKIN"
 :value :CREO)
(persistent-data
 :key "AM_TEMPLATE_DEFINE"
 :value '( :VARIANT-VALUES ( T ( :SYM_CATEGORY "Zeichnungen" ) ) ) )
(persistent-data
 :key "FILING-REVISION"
 :value :DEFAULT)
(persistent-data
 :key "DB-ATTRIBUTE-BROWSER"
 :value '( :TREE-SEC-ICONS NIL :VISIBLE NIL :TABLE-SEC-ICONS NIL :DISP-ROOT-NODE 
           NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-SHADOW-MIRROR"
 :value '( :FLOOR-RELATIVE-DISTANCE -0.20549 :VP-SCENERY-ENABLED NIL 
           :ENVIRONMENT-MAP 
           "C:/PTC/Creo Elements/Direct Modeling 19.0/personality/rendering/environments/studio1.jpg" ) )
(persistent-data
 :key "UIL-HISTORY"
 :value '( :ENTRIES ( "2" "585" "284.6166382329" "595.9384172542" "287.7140970236" 
           "284.6025864522" "645" "50+30" "50" "80" "150" "150+30" "20" 
           "433.7019337941" "419.371500209" "40" "53" "60" "102" "287" "720/2" 
           "480+40" "480" "520" "490" "490+40" "510" "510+40" "565" "565+40" 
           "250-30" "220" "250" "675" "675+40" "540" "500" "500+40" "515" 
           "515+40" "25" "35" "2200" "2200+720" "2552.2" "2552.2+720" "a" "15" 
           "15+2" "5" ) :MAXNUM 50 :STORE T ) )
(persistent-data
 :key "AM_MODIFY_BORDER"
 :value '( :VALUES ( :OPTIONS T ) ) )
(persistent-data
 :key "PART_PROPERTY_PROPOSALS"
 :value '( :DENSITY NIL :BASE_DENSITY NIL ) )
(persistent-data
 :key "AVAILABLE-SHORTCUTS-BROWSER"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "DIRECTION-DECODER"
 :value '( :REFERENCE-ELEMENT-TYPE :EDGE-FACE :INITIAL-DIRECTION-NEGATIVE NIL ) )
(persistent-data
 :key "AM_MODIFY_CUTAWAY_DEPTH"
 :value '( :VALUES ( :REAL-PREVIEW NIL ) ) )
(persistent-data
 :key "MLE-FONT"
 :value '( :CHARSET 0 :ITALIC NIL :WEIGHT 400 :FONTSIZE -12 :FACENAME 
           "Courier New" ) )
(persistent-data
 :key "SHA_BEND_ANIMATION"
 :value '( :VALUES ( :SAVE_MODE T ) ) )
(persistent-data
 :key "PARCEL-GBROWSER-DS"
 :value '( :POS ( :X 178 :Y 166 ) :SIZE ( :WIDTH 679 :HEIGHT 529 ) ) )
(persistent-data
 :key "CONFIGURATION-SETTINGS"
 :value '( :PART-COLOR 255 :ASSY-COLOR 65535 :POSITION-OUTSIDE-OWNER NIL 
           :SMOOTH-UPDATE-APPLY T :WARN-ON-FORMATIONS T :SMOOTH-UPDATE-TIME 2 ) )
(persistent-data
 :key "DB-ATTRIBUTE-BROWSER-DS"
 :value '( :SIZE ( :WIDTH 300 :HEIGHT 239 ) :POS ( :X 0 :Y 0 ) ) )
(persistent-data
 :key "PARCEL-GBROWSER"
 :value '( :VISIBLE NIL :EXPAND-OTHER-PSEUDO-FOLDERS NIL 
           :EXPAND-SHARE-PSEUDO-FOLDERS T :EXPAND-DOCUPLANESETS T 
           :EXPAND-VIEWSETS NIL :EXPAND-STUDIES T :EXPAND-ANIMATIONS T 
           :EXPAND-RELATIONS T :EXPAND-RELATIONSETS T :EXPAND-STOCK-CONTAINERS T 
           :EXPAND-CONTAINERS T :EXPAND-PARTS NIL :PSEUDO-FOLDERS-OTHERS T 
           :PSEUDO-FOLDERS-SHARED T :BORDER-MODE NIL :RESTORE-EXPAND-STATE NIL 
           :FILTER-SELECT-IN-TREE NIL :FILTER-VP-HIGHLIGHT NIL 
           :SEARCH-VP-HIGHLIGHT NIL ) )
(persistent-data
 :key "CUST-FEATURE-BROWSER"
 :value '( :VISIBLE NIL ) )
(persistent-data
 :key "NEWSLETTERREGISTRATION"
 :value -1)
(persistent-data
 :key "SHOWTUTORIALATSTARTUP"
 :value NIL)
(persistent-data
 :key "CROSS_SECTION"
 :value '( :VALUES ( :RESOLVER_MODE :SAME ) ) )
(persistent-data
 :key "SD-STL-STLEDITOR-DS"
 :value '( :SIZE ( :WIDTH 616 :HEIGHT 700 ) ) )
(persistent-data
 :key "PLASTIC-FEATURE-BROWSER"
 :value '( :DISP-ROOT-NODE NIL :TABLE-SEC-ICONS NIL ) )
(persistent-data
 :key "TEMPLATES"
 :value '( :VISIBLE NIL :DISP-ROOT-NODE NIL :TABLE-SEC-ICONS NIL ) )
(persistent-data
 :key "UNITS"
 :value '( :MASS ( :G 1 ) :ANGLE ( :DEG 1 ) :LENGTH ( :MM 1 ) ) )
(persistent-data
 :key "CLASH-ANALYSIS-BROWSER-DS"
 :value '( :SIZE ( :WIDTH 400 :HEIGHT 239 ) ) )
(persistent-data
 :key "PATTERN_DELETE"
 :value '( :VALUES ( :OPTION :REMOVE-REFOBJS ) ) )
(persistent-data
 :key "SHA_ADD_LIP_HEM_SUBDIALOG"
 :value '( :VALUES ( :AUTOMATIC-MITER T ) ) )
(persistent-data
 :key "PRESELECTION"
 :value T)
(persistent-data
 :key "TEMP-AUX-VP-CONFIG"
 :value '( ( -10.0,-33.0 1250.0,841.0 :MAXIMIZE ) NIL ) )
(persistent-data
 :key "CURRENTQUICKSTARTTUTORIAL"
 :value 3)
(persistent-data
 :key "DEFAULT-SETTINGS-STYLE-REFERENCE_COLUMN-TREE-DIALOG"
 :value '( :HEIGHT 286 :WIDTH 509 :COLUMN-WIDTHS ( 114 84 63 67 86 59 ) ) )
(persistent-data
 :key "AM_SYM_DEFINE"
 :value '( :VARIANT-VALUES ( T ( :SYM_CATEGORY "Neue Herbold - Allgemein" ) ) ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-SHOW"
 :value '( :WORKPLANES_2DGEO_LABEL T :WORKPLANES_HIDDEN T 
           :WORKPLANES_2DGEO_VERTICES NIL :WORKPLANES_2DCONSTRUCTION T 
           :WORKPLANES_2DGEO T :WORKPLANES_LABEL T :WORKPLANES_LOCAL_AXIS NIL 
           :WORKPLANE_SETS_MATCH_LINES T :WORKPLANES_BORDER T :WORKPLANES T 
           :CLIP_LINES T :CLIP_PLANES T :COORD_SYSTEM T :DOCU_PLANES T 
           :3DGEO_LABEL T :3DGEO_VERTICES NIL :3DGEO_HIDDEN_DIMMED NIL 
           :3DGEO_MIXED NIL :3DGEO_WIRE NIL :3DGEO_HIDDEN T :3DGEO_EDGED T 
           :3DGEO_SHADED T :3DGEO T :3DGEO_PART_TRANSPARENCY T 
           :WORKPLANES_DIM_INACTIVE T :CLIP_HATCHES NIL :FEATURE_PTS NIL ) )
(persistent-data
 :key "TDB_BROWSER-DS"
 :value '( :SIZE ( :WIDTH 674 :HEIGHT 798 ) ) )
(persistent-data
 :key "CLASH-ANALYSIS-BROWSER"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "AM_CREATE_DETAIL"
 :value '( :VALUES ( :OPTIONS T ) ) )
(persistent-data
 :key "AM-BROWSER-DS"
 :value '( :POS ( :X 759 :Y 272 ) :SIZE ( :WIDTH 528 :HEIGHT 403 ) ) )
(persistent-data
 :key "ALERT-HISTORY"
 :value '( :MAX_SIZE 50 ) )
(persistent-data
 :key "RELATION-BROWSER"
 :value '( :VISIBLE NIL :TREE-SEC-ICONS NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-2D"
 :value '( :CGEO-WIDTH 1 ) )
(persistent-data
 :key "TEMPLATES-DS"
 :value '( :POS ( :X 637 :Y 152 ) :SIZE ( :WIDTH 300 :HEIGHT 239 ) ) )
(persistent-data
 :key "DEFAULT-SETTINGS_COLUMN-TREE-DIALOG"
 :value '( :COLUMN-WIDTHS ( 412 335 143 42 748 ) :HEIGHT 844 :WIDTH 1606 ) )
(persistent-data
 :key "USE-OK-CANCEL-BUTTONS"
 :value :NONE)
(persistent-data
 :key "MEASUREMENT-BROWSER"
 :value '( :TABLE-SEC-ICONS NIL :TREE-SEC-ICONS NIL :GRAPH-MODE NIL :DUAL-MODE T ) )

;; ----------- end of file -----------
