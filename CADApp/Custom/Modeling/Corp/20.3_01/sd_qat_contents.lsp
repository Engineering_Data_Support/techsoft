;; Automatically written on 26-Feb-2021 08:51:44
(oli::sd-fluentui-set-quick-access-toolbar-contents "SolidDesigner"
  '(
   (:AVAILCMD ("SolidDesigner" "Filing" "New Session"))
   (:AVAILCMD ("SolidDesigner" "Filing" "Load ..."))
   (:AVAILCMD ("SolidDesigner" "Filing" "Save ..."))
   (:AVAILCMD ("SolidDesigner" "Print" "Print Preview"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "Miscellaneous" "Annotation"))
   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))
   (:AVAILCMD ("SolidDesigner" "SolidPower" "ModifyStandardPartPara"))
   (:AVAILCMD ("SolidDesigner" "SolidPower" "LoadStandardPart"))
   (:AVAILCMD ("All" "Miscellaneous" "SheetAdv"))
   (:AVAILCMD ("All" "View" "Store View"))
   (:AVAILCMD ("All" "View" "Recall View"))
   ))