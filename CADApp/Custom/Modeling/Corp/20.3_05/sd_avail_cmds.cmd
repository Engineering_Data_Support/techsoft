﻿; ------------------------------------------------------------------------------
; SVN: $Id: sd_avail_cmds.cmd 38125 2014-07-03 10:03:33Z pjahn $
; commands for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(:Application "All")

;;----- All: Techosft ----------------------------------------------------------

(:Group "TECHSOFT" :title "TECHSOFT")

("Sheet Metal"
 :title       "Sheet Metal"
 :action      (LISP::PROGN
                (LISP::IF (OLI::SD-MODULE-ACTIVE-P "SHEETADVISOR")
                    (LISP::PROGN
                      (OLI::SD-SWITCH-APPLICATION "SheetAdvisor"))
                    (LISP::PROGN (STARTUP::ACTIVATE-SHEETADVISOR))))
 :description "Start/Switch to Sheet Metal"
 :image       "All/TECHSOFT/SheetMetal"
 :ui-behavior :DEFAULT)
 
("Model Manager"
 :title       "Model Manager"
 :action      (LISP::PROGN
				(LISP:IF (startup::license-free-module-active-p "ModelManager")
					(LISP::PROGN
						(MODELMANAGER::MM-SEND-CMD "SHOW-MANAGER"))
					  (LISP::PROGN (act_deact_module :act "ModelManager" "MODULE-CONTROLLER-Modules-ModelManager-TB" '(STARTUP::ACTIVATE-MODELMANAGER)))))
 :description "Open the Model Manager Workspace"
 :image       "All/TECHSOFT/ModelManager"
 :sd-access   :yes
 :modeling-pe :no
 ) 
 
#| ("TECHSOFT Filecenter"
 :title       "TECHSOFT Filecenter"
 :action      (oli::sd-display-url "https://support.techsoft.at")
 :description "TECHSOFT Filecenter - web based file exchange with TECHSOFT"
 :image       "All/TECHSOFT/filecenter"
 :sd-access   :yes)
 
("TECHSOFT Support-Form"
 :title       "TECHSOFT Support Form"
 :action      (oli::sd-display-url "http://www.techsoft.at/support/service/support-formular/")
 :description "TECHSOFT Support Form - Open a support case using our web support form"
 :image       "All/TECHSOFT/supportform"
 :sd-access   :yes)

("Start Netviewer"
 :title       "Start Netviewer"
 :action      (oli::sd-sys-background-job (format nil "\"~a\\\addons\\netviewer\\netviewer.exe\"" (oli::sd-sys-getenv "BASEDIR")))
 :description "Start the netviewer client to collaborate with a TECHSOFT support engineer"
 :image       "All/TECHSOFT/netviewer"
 :sd-access   :yes)

("Start Teamviewer"
 :title       "Start Teamviewer"
 :action      (oli::sd-sys-background-job (format nil "\"~a\\\addons\\teamviewer\\teamviewer.exe\"" (oli::sd-sys-getenv "BASEDIR")))
 :description "Start the netviewer client to collaborate with a TECHSOFT support engineer"
 :image       "All/TECHSOFT/teamviewer"
 :sd-access   :yes)

("TECHSOFT Support by E-Mail"
 :title       "TECHSOFT Support via E-Mail"
 :action      (oli::sd-sys-background-job (format nil "explorer.exe \"mailto:support@techsoft.at?subject=Supportanfrage zu ~a\"" (oli::sd-sys-getenv "LINKNAME") (oli::sd-sys-getenv "TEMP")))
 :description "Contact the TECHSOFT support team by E-Mail"
 :image       "All/TECHSOFT/email"
 :sd-access   :yes)

("Show SDCORPCUSTOMIZEDIR"
 :title       "Show SDCORPCUSTOMIZEDIR"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDCORPCUSTOMIZEDIR")))
 :description "Show the directory for corporate wide settings"
 :image       "All/TECHSOFT/corp"
 :sd-access   :yes) 

("Show SDSITECUSTOMIZEDIR"
 :title       "Show SDSITECUSTOMIZEDIR"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDSITECUSTOMIZEDIR")))
 :description "Show the directory for site wide settings"
 :image       "All/TECHSOFT/site"
 :sd-access   :yes)
 
("Show SDUSERCUSTOMIZEDIR"
 :title       "Show SDUSERCUSTOMIZEDIR"
 :action      (oli::sd-sys-background-job (format nil "%windir%\\explorer.exe \"~a\"" (oli::sd-sys-getenv "SDUSERCUSTOMIZEDIR")))
 :description "Show the directory for user defined settings"
 :image       "All/TECHSOFT/user"
 :sd-access   :yes)
 
("Software Distribution Server"
 :title       "Software Distribution Server"
 :action      (oli::sd-display-url (format nil "http://~a:~a" (oli::sd-sys-getenv "SWDSERVERHOSTNAME") (oli::sd-sys-getenv "SWDSERVERPORT")))
 :description "Open the website of the Software Distribution Server for Creo Elements/Direct Manager Server"
 :image       "All/TECHSOFT/swdserver"
 :sd-access   :yes)
 
("List License Servers"
 :title       "List License Servers"
 :action      (frame2-ui::display (oli::sd-sys-getenv "LICENSESERVER"))
 :description "Display a list of configured License Servers"
 :image       "All/TECHSOFT/licenseserver"
 :sd-access   :yes)
 
("PTC Online Documentation"
 :title       "Online Documentation"
 :action      (oli::sd-display-url "http://www.ptc.com/cs/help/creo_elements_direct_hc/modeling_190_hc/")
 :description "PTC Online Documentation - Opens the online documentation for Creo Elements/Direct Modeling from the PTC Website (Internet connection required)"
 :image       "All/TECHSOFT/ptc_online_documentation"
 :sd-access   :yes)
 
("PTC Online Tutorials"
 :title       "Online Tutorials"
 :action      (oli::sd-display-url "http://learningexchange.ptc.com/tutorials/listing/product_version_id:81")
 :description "PTC Online Tutorials - Opens a list of tutorials for Creo Elements/Direct Modeling from the PTC Website (Internet connection required)"
 :image       "All/TECHSOFT/ptcu"
 :sd-access   :yes)

("Black Design"
 :title       "Schwarz"
 :action      (uib::win-set-skin-style :black)
 :description "Switch user interface to black design"
 :image       "All/TECHSOFT/black_design"
 :sd-access   :yes)
 
("Silver Design"
 :title       "Silber"
 :action      (uib::win-set-skin-style :silver)
 :description "Switch user interface to silver design"
 :image       "All/TECHSOFT/silver_design"
 :sd-access   :yes)

("Blue Design"
 :title       "Blau"
 :action      (uib::win-set-skin-style :blue)
 :description "Switch user interface to blue design"
 :image       "All/TECHSOFT/blue_design"
 :sd-access   :yes)

("Creo Design"
 :title       "Creo"
 :action      (uib::win-set-skin-style :creo)
 :description "Switch user interface to Creo design"
 :image       "All/TECHSOFT/creo_design"
 :sd-access   :yes) |#
 
;;----- SolidDesigner: Construction --------------------------------------------
 
(:Application "SolidDesigner")

(:Group "Construction" :title "Construction")

("Querschnitt"
 :title       "Querschnitt als Hilfgeometrie projizieren"
 :action      "geometry_mode :construction cross_section :cross_section_wp"
 :description "Teile-Querschnitt als Hilfsgeometrie auf eine Arbeitsebene projizieren."
 :image       "SolidDesigner/Construction/Querschnitt"
 :ui-behavior :DEFAULT)
 
;;----- Annotation: TECHSOFT --------------------------------------------
#| 
(:Application "Annotation")

(:Group "TECHSOFT" :title "TECHSOFT")

("Switch Annotation Color Scheme"
 :title       "Switch Annotation Color Scheme"
 :action      "(ts-switch-background-color)"
 :description "Switch between the default color scheme and the b/w color scheme in Annotation."
 :image       "Annotation/TECHSOFT/Switch_Annotation_Color_Scheme"
 :ui-behavior :DEFAULT)
 |#
