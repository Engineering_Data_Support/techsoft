{
  All localizable text strings for ME10 related to WorkManager database
}

{ DB: system attributs }
LET Awm_db_elid   "要素ID"
LET Awm_db_uid    "ID文字列"
LET Awm_db_linkref {lola_msg 35 32 "DBattr" } "リンク参照"

{ DB: system predefined classes }
LET Awm_dbr_default_part "default_part"
LET Awm_dbr_default_doc  "default_doc"
LET Awm_dbr_tb_class     "me10i_title_block"

{ DB: document class attributs for storing drawings without frames }
LET Awm_db_format "FORMAT"
LET Awm_db_offset "OFFSET"
LET Awm_db_scale "尺度"

{ AIP-DemoDB: class names }
LET Awm_dbr_demodb_part_class "me_part"
LET Awm_dbr_demodb_doc_class  "me_drawing"
LET Awm_dbr_demodb_tb_class   "me_tb_frame"

{ AIP-DemoDB: partname prefix of all title block drawings }
LET Awm_db_demodb_tbname_prefix ".tb_frame_"

{ AIP-BOM-DemoDB: class name for flags and layouts }
LET Awm_dbr_demodb_bom_flag_class "me_bom_flag"

{ AIP-BOM-DemoDB: names for flags and layouts }
LET Awm_db_demodb_bom_flag_a "DIN"
LET Awm_db_demodb_bom_flag_b "ISO"
LET Awm_db_demodb_bom_flag_c "ユーザ"
LET Awm_db_demodb_bom_lay_a "A"
LET Awm_db_demodb_bom_lay_b "B"
LET Awm_db_demodb_bom_lay_c "C"

{ WMDT-Standard-Schema: class names }
LET Awm_dbr_stds_part_class "部品"
LET Awm_dbr_stds_doc_class  "図面"
LET Awm_dbr_stds_tb_class   "図枠"

{ WMDT-Standard-Schema: partname prefix of all title block drawings }
LET Awm_db_stds_tbname_prefix ".tb_frame_"

{ WMDT-Standard-Schema: class name for flags and layouts }
LET Awm_dbr_stds_bom_flag_class "BOMフラグ"

{ WMDT-Standard-Schema: names for flags and layouts }
LET Awm_db_stds_bom_flag_a "DIN"
LET Awm_db_stds_bom_flag_b "ISO"
LET Awm_db_stds_bom_flag_c "ユーザ"
LET Awm_db_stds_bom_lay_a  "A"
LET Awm_db_stds_bom_lay_b  "B"
LET Awm_db_stds_bom_lay_c  "C"

{ WMDT-Standard-Schema: system attributs }
LET Awm_db_name         "名前"
LET Awm_db_version      "バージョン"
LET Awm_db_description  "説明"
LET Awm_db_no_of_sheets "シート枚数"
LET AWM_db_pos          "位置"
