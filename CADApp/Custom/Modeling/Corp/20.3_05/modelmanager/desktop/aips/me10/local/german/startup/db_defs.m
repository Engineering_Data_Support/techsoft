{
  All localizable text strings for ME10 related to WorkManager database
}

{ DB: system attributs }
LET Awm_db_elid   "ELID"
LET Awm_db_uid    "EINDEUT_ID"
LET Awm_db_linkref {lola_msg 35 32 "DBattr" } "VERB_REF"

{ DB: system predefined classes }
LET Awm_dbr_default_part "standardteil"
LET Awm_dbr_default_doc  "standarddok"
LET Awm_dbr_tb_class     "me10i_schriftfeld"

{ DB: document class attributs for storing drawings without frames }
LET Awm_db_format "FORMAT"
LET Awm_db_offset "VERSATZ"
LET Awm_db_scale "MASSSTAB"

{ AIP-DemoDB: class names }
LET Awm_dbr_demodb_part_class "me_teil"
LET Awm_dbr_demodb_doc_class  "me_zeichnung"
LET Awm_dbr_demodb_tb_class   "me_sfeld_rahmen"

{ AIP-DemoDB: partname prefix of all title block drawings }
LET Awm_db_demodb_tbname_prefix ".sfeld_rahmen_"

{ AIP-BOM-DemoDB: class name for flags and layouts }
LET Awm_dbr_demodb_bom_flag_class "me_stl_markierung"

{ AIP-BOM-DemoDB: names for flags and layouts }
LET Awm_db_demodb_bom_flag_a "DIN"
LET Awm_db_demodb_bom_flag_b "ISO"
LET Awm_db_demodb_bom_flag_c "BENUTZER"
LET Awm_db_demodb_bom_lay_a "A"
LET Awm_db_demodb_bom_lay_b "B"
LET Awm_db_demodb_bom_lay_c "C"

{ WMDT-Standard-Schema: class names }
LET Awm_dbr_stds_part_class "STAMMDATEN"
LET Awm_dbr_stds_doc_class  "ZEICHNUNG_2D"
LET Awm_dbr_stds_tb_class   "RAHMEN_2D"

{ WMDT-Standard-Schema: partname prefix of all title block drawings }
LET Awm_db_stds_tbname_prefix ".sfeld_rahmen_"

{ WMDT-Standard-Schema: class name for flags and layouts }
LET Awm_dbr_stds_bom_flag_class "STL_MARKIERUNG"

{ WMDT-Standard-Schema: names for flags and layouts }
LET Awm_db_stds_bom_flag_a "DIN"
LET Awm_db_stds_bom_flag_b "ISO"
LET Awm_db_stds_bom_flag_c "BENUTZER"
LET Awm_db_stds_bom_lay_a  "A"
LET Awm_db_stds_bom_lay_b  "B"
LET Awm_db_stds_bom_lay_c  "C"

{ WMDT-Standard-Schema: system attributs }
LET Awm_db_name         "NAME"
LET Awm_db_version      "VERSION"
LET Awm_db_description  "BESCHREIBUNG"
LET Awm_db_no_of_sheets "BLATT_ANZAHL"
LET Awm_db_pos          "POS"
