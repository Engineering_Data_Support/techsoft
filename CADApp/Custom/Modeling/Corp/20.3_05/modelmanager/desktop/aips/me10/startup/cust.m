{  @(#)  cust.m $ Revision 3.1 $ 02/20/97
 ****************************************************************************}
{*                                                                          *}
{*  WorkManager Desktop                                                     *}
{*  Version 3.2: ME10 Customize file                                        *}
{*                                                                          *}
{****************************************************************************}

{############################################################################}
{# Enable "WorkManager Desktop" - modules                                   #}
{############################################################################}
{Enable_wmdt_bom}
{Enable_wmdt_help}

{***************************************************************************}
{* Title Block - Setup:                                                    *}
{*                                                                         *}
{* To overwrite default- or demodb-settings please uncomment the           *}
{* following lines                                                         *}
{***************************************************************************}

{* max. number of change note entries *}
{! LET Awmc_val_max_change_notes   5 !}

{* partname prefix for title block parts *}
{! LET Awmc_val_tbname_prefix  '.tb_frame_' !}

{* alternaive partname prefixs for title block parts to be reload to the   *}
{* current title block from the database                                   *}
{! LET Awmc_val_tbname_alt_prefix_1 '.sfeld_rahmen_' !}
{! LET Awmc_val_tbname_alt_prefix_2 '.cadre_ct_'     !}
{! LET Awmc_val_tbname_alt_prefix_3 '.tb_frame_'     !}
{! LET Awmc_val_tbname_alt_prefix_4 ''               !}
{! LET Awmc_val_tbname_alt_prefix_5 ''               !}

{* classnames for title block setup *}
{! LET Awmc_val_tb_partclass  Awm_dbr_default_part !}
{! LET Awmc_val_tb_docclass   Awm_dbr_default_doc !}

{* WorkManager classname for title blocks *}
{! LET Awmc_val_tb_class Awm_dbr_tb_class !}

{* Store drawing with/without title block *}
{! LET Awmc_val_store_with_tb (FALSE) !}

{* Auto reload of Title Blocks when drawing is loaded *}
{! LET Awmc_val_auto_relod_of_2d_frames (TRUE) !}
{! LET Awmc_val_never_reload_2d_frames  (FALSE) !}


{############################################################################}
{# BOM customizing ##########################################################}
{############################################################################}
{****************************************************************************}
{* To overwrite default settings please uncomment the following lines       *}
{****************************************************************************}

{* BOM-Flag *}
{************}
{! LET Awmc_bom_val_increment     5  !}
{! LET Awmc_bom_val_start_posnr   10 !}

{* LINK_SRC for copy BOM *}
{*************************}
{! LET Awm_bom_val_link_src_attr "LINK_SRC" !}

{* BOM-Flag-Leaderline *}
{***********************}
{! DEFINE Awmc_bom_val_leader_line_type        SOLID       END_DEFINE !}
{! DEFINE Awmc_bom_val_leader_line_color       CYAN        END_DEFINE !}
{! DEFINE Awmc_bom_val_leader_arrow            DOT_TYPE    END_DEFINE !}
{! DEFINE Awmc_bom_val_leader_arrow_size       1.5         END_DEFINE !}

{* Part-Highlight-Color *}
{************************}
{! DEFINE Awmc_bom_val_part_highlight_color    MAGENTA     END_DEFINE !}

{* BOM-Layouts *}
{***************}
{! LET Awmc_bom_val_layout_direction   'UPPER'       !}
{! LET Awmc_bom_val_layout_ref_pos     'LOWER_RIGHT' !}

{* WM-Document Class for flags and layouts *}
{*******************************************}
{! LET Awmc_bom_val_bfg_class  '<class_name>' !}
