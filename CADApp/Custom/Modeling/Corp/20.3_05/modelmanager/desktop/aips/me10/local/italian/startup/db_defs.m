{
  All localizable text strings for ME10 related to WorkManager database
}

{ DB: system attributs }
LET Awm_db_elid   "IDEL"
LET Awm_db_uid    "STRINGA_ID_UNIVOCA"
LET Awm_db_linkref {lola_msg 35 32 "DBattr" } "RIF_RELAZ"

{ DB: system predefined classes }
LET Awm_dbr_default_part "parte_predef"
LET Awm_dbr_default_doc  "doc_predef"
LET Awm_dbr_tb_class     "cartiglio_me10i"

{ DB: document class attributs for storing drawings without frames }
LET Awm_db_format "FORMATO"
LET Awm_db_offset "POSIZIONE"
LET Awm_db_scale "SCALA"

{ AIP-DemoDB: class names }
LET Awm_dbr_demodb_part_class "parte_me"
LET Awm_dbr_demodb_doc_class  "disegno_me"
LET Awm_dbr_demodb_tb_class   "cornice_ct_me"

{ AIP-DemoDB: partname prefix of all title block drawings }
LET Awm_db_demodb_tbname_prefix ".tb_frame_"

{ AIP-BOM-DemoDB: class name for flags and layouts }
LET Awm_dbr_demodb_bom_flag_class "flag_distbase_me"

{ AIP-BOM-DemoDB: names for flags and layouts }
LET Awm_db_demodb_bom_flag_a "DIN"
LET Awm_db_demodb_bom_flag_b "ISO"
LET Awm_db_demodb_bom_flag_c "UTENTE"
LET Awm_db_demodb_bom_lay_a "A"
LET Awm_db_demodb_bom_lay_b "B"
LET Awm_db_demodb_bom_lay_c "C"

{ WMDT-Standard-Schema: class names }
LET Awm_dbr_stds_part_class "DATI ORIGINALI"
LET Awm_dbr_stds_doc_class  "DISEGNO_2D"
LET Awm_dbr_stds_tb_class   "CORNICE_2D"

{ WMDT-Standard-Schema: partname prefix of all title block drawings }
LET Awm_db_stds_tbname_prefix ".tb_frame_"

{ WMDT-Standard-Schema: class name for flags and layouts }
LET Awm_dbr_stds_bom_flag_class "FLAG_DISTBASE"

{ WMDT-Standard-Schema: names for flags and layouts }
LET Awm_db_stds_bom_flag_a "DIN"
LET Awm_db_stds_bom_flag_b "ISO"
LET Awm_db_stds_bom_flag_c "UTENTE"
LET Awm_db_stds_bom_lay_a  "A"
LET Awm_db_stds_bom_lay_b  "B"
LET Awm_db_stds_bom_lay_c  "C"

{ WMDT-Standard-Schema: system attributs }
LET Awm_db_name         "NOME"
LET Awm_db_version      "VERSIONE"
LET Awm_db_description  "DESCRIZIONE"
LET Awm_db_no_of_sheets "NUMERO_FOGLI"
LET Awm_db_pos          "POS"
