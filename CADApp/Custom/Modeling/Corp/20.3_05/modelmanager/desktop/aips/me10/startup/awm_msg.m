{  @(#)  awm_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of COBRA
  Non-English files are found at:   <WorkManager playpen>\desktop\aips\me10\local\<language>\startup\awm_msg.m
}

{ >>>if false }

{ LOLAMAX = 248 }

{ General messages }

LET Awm_msg_1  {lola_msg 1  80 "Prompt message"} "Enter TOP or SUBPART or CONFIRM to load: "
LET Awm_msg_2  {lola_msg 2  80 "Prompt message"} "Enter CONFIRM to rename drawing to: "
LET Awm_msg_3  {lola_msg 3  80 "Prompt message"} "Identify part for info or CURRENT"
LET Awm_msg_4  {lola_msg 4  80 "Error message"} "Drawing not loaded from Workmanager"
LET Awm_msg_5  {lola_msg 5  80 "Prompt message"} "Enter 'Name' of the part class for the current title block: "
LET Awm_msg_6  {lola_msg 6  80 "Prompt message"} "Enter 'Name' of the document class for the current title block: "
LET Awm_msg_7  {lola_msg 7  80 "Prompt message"} "Identify part to store or CURRENT"
LET Awm_msg_8  {lola_msg 8  80 "Prompt message"} "Enter CONFIRM to clear drawing ID in ME10"
LET Awm_msg_9  {lola_msg 9  80 "Prompt message"} "Running WorkManager connection ..."
LET Awm_msg_10 {lola_msg 10 80 "Error message"} "Invalid argument to ME10INTF arithmetic function"
LET Awm_msg_11 {lola_msg 11 80 "Error message"} "Start WorkManager then retry the action"
LET Awm_msg_12 {lola_msg 12 80 "Error message"} "ERROR in WorkManager"
LET Awm_msg_13 {lola_msg 13 80 "Error message"} "No references found for update"
LET Awm_msg_14 {lola_msg 14 80 "Error message"} "No tablet connected"
LET Awm_msg_15 {lola_msg 15 80 "Error message"} "Start WorkManager or use 'DDE_ENABLE ON' with WorkManager then retry the action"
LET Awm_msg_16 {lola_msg 16 80 "Warning"} "WARNING!: Top part of drawing 1 is still 1 level under TOP"
LET Awm_msg_17 {lola_msg 17 80 "Warning"} "WARNING!: Top part of drawing 2 is still 1 level under TOP"
LET Awm_msg_18 {lola_msg 18 100 "Warning"} "WARNING!: Top part of the updated drawing is still 1 level under TOP"
DEFINE Awm_msg_19 ({lola_msg 19 80 "Prompt message" "1 -> partname" "2 -> partname"}
 "Rename part " + Adu_part_y + " to " + Adu_part_x)
END_DEFINE
LET Awm_msg_20 {lola_msg 20 80 "Prompt message"} "Select item to get help about"
LET Awm_msg_35 {lola_msg 243 80 "Prompt message"} "Drawing BOM table. Wait..."
LET Awm_msg_36   {lola_msg 247 80 "Warning"} "Add PDF limited to unmodified drawings"

{ Menu texts }

DEFINE Awm_mtext_1
		 CENTER {lola_msg 21 10 "Menu text"} "WM"
END_DEFINE
DEFINE Awm_mtext_2
		 CENTER {lola_msg 22 10 "Menu text"} "Desktop"
END_DEFINE
LET Awm_mtext_3  {lola_msg 23 10 "Menu text"} "LOAD      "
LET Awm_mtext_4  {lola_msg 24 10 "Menu text"} "SUBPART   "
LET Awm_mtext_5  {lola_msg 25 10 "Menu text"} "ABSOLUTE  "
LET Awm_mtext_6  {lola_msg 26 10 "Menu text"} "STORE     "
LET Awm_mtext_7  {lola_msg 27 10 "Menu text"} "All       "
LET Awm_mtext_8  {lola_msg 29 10 "Menu text"} "Part      "
LET Awm_mtext_9  {lola_msg 30 10 "Menu text"} "CURRENT   "
LET Awm_mtext_10 {lola_msg 31 10 "Menu text"} "STORE OVWR"
LET Awm_mtext_11 {lola_msg 32 10 "Menu text"} "RESERVE   "
LET Awm_mtext_12 {lola_msg 33 10 "Menu text"} "ID        "
LET Awm_mtext_13 {lola_msg 34 10 "Menu text"} "Drawing   "
LET Awm_mtext_14 {lola_msg 35 10 "Menu text"} "WM-Part   "
LET Awm_mtext_15 {lola_msg 36 10 "Menu text"} "INFO      "
LET Awm_mtext_16 {lola_msg 37 10 "Menu text"} "Top       "
LET Awm_mtext_17 {lola_msg 38 10 "Menu text"} "CLEAR ID  "

LET Awm_mtext_18 {lola_msg 39 10 "Menu text"} "UPDATE    "
LET Awm_mtext_19 {lola_msg 40 10 "Menu text"} "DRAW      "
LET Awm_mtext_20 {lola_msg 41 10 "Menu text"} "TR Names  "
LET Awm_mtext_21 {lola_msg 42 10 "Menu text"} "TR Values "
LET Awm_mtext_22 {lola_msg 43 10 "Menu text"} "LOAD TB   "
LET Awm_mtext_23 ""
LET Awm_mtext_24 {lola_msg 45 10 "Menu text" } "ASSIGN"
LET Awm_mtext_25 {lola_msg 46 10 "Menu text" } "WM-Parts"
LET Awm_mtext_26 {lola_msg 47 10 "Menu text" } "CURRENT"
LET Awm_mtext_27 {lola_msg 48 10 "Menu text" } "TOP"
LET Awm_mtext_28 {lola_msg 49 10 "Menu text" } "LEVELS"
LET Awm_mtext_29 {lola_msg 50 10 "Menu text"} "UNRESERVE "
LET Awm_mtext_30 ""
LET Awm_mtext_31 ""
LET Awm_mtext_32 ""
LET Awm_mtext_33 {lola_msg 54 10 "Menu text"} "SHOW DATA "
LET Awm_mtext_34 ""

LET Awm_mtext_35 {lola_msg 56 10 "Menu text"} "SET       "
LET Awm_mtext_36 {lola_msg 57 10 "Menu text"} "Classes   "
LET Awm_mtext_37 {lola_msg 58 10 "Menu text"} "TR-ASSIGN "
LET Awm_mtext_38 {lola_msg 59 10 "Menu text"} "UNASSIGN  "
LET Awm_mtext_39 {lola_msg 60 10 "Menu text"} "Width     "
LET Awm_mtext_40 {lola_msg 61 10 "Menu text"} "Case Conv "
LET Awm_mtext_41 {lola_msg 62 10 "Menu text"} "Lines     "
LET Awm_mtext_42 {lola_msg 63 10 "Menu text"} "Word Wrap "
LET Awm_mtext_43 {lola_msg 64 10 "Menu text"} "Precision "
LET Awm_mtext_44 {lola_msg 65 10 "Menu text"} "OPTIONS   "
LET Awm_mtext_45 {lola_msg 66 10 "Menu text"} "Upper     "
LET Awm_mtext_46 {lola_msg 67 10 "Menu text"} "On        "
LET Awm_mtext_47 {lola_msg 68 10 "Menu text"} "Lower     "
LET Awm_mtext_48 {lola_msg 69 10 "Menu text"} "Off       "
LET Awm_mtext_49 {lola_msg 70 10 "Menu text"} "CHANGE    "
LET Awm_mtext_50 {lola_msg 71 10 "Menu text"} "SHOW REFS "
LET Awm_mtext_51 {lola_msg 72 10 "Menu text"} "All       "
LET Awm_mtext_52 {lola_msg 73 10 "Menu text"} "Select    "
LET Awm_mtext_53 {lola_msg 74 10 "Menu text"} "SHOW      "
LET Awm_mtext_54 {lola_msg 75 10 "Menu text"} "SELECT    "
LET Awm_mtext_55 ""
LET Awm_mtext_56 ""
LET Awm_mtext_57 ""
LET Awm_mtext_58 ""
LET Awm_mtext_59 ""

DEFINE Awm_mtext_60
		 CENTER {lola_msg 81 15 "Menu text"} "AIP Main Menu"
END_DEFINE
DEFINE Awm_mtext_61
		 CENTER {lola_msg 82 21 "Menu text"} "AIP Main Menu"
END_DEFINE
LET Awm_mtext_62 {lola_msg 83  6 "Menu text"} "FILE  "
LET Awm_mtext_63 {lola_msg 84  7 "Menu text"} "T-BLOCK"
LET Awm_mtext_64 {lola_msg 85  7 "Menu text"} "TB-CONF"
LET Awm_mtext_65 ""
LET Awm_mtext_66 ""
LET Awm_mtext_67 ""
LET Awm_mtext_68 ""
LET Awm_mtext_69 ""

LET Awm_mtext_70 {lola_msg 113 40 "Menu text"} "STORE DRAWING"
LET Awm_mtext_71 {lola_msg 114 40 "Menu text"} "STORE PART"
LET Awm_mtext_72 {lola_msg 115 40 "Menu text"} "Overwrite current document"
LET Awm_mtext_73 {lola_msg 116 40 "Menu text"} "Create new version"
LET Awm_mtext_74 {lola_msg 117 40 "Menu text"} "Create new document"
LET Awm_mtext_75 {lola_msg 118 40 "Menu text"} "Create new part and new document"
LET Awm_mtext_76 {lola_msg 119 40 "Menu text"} "CANCEL"

DEFINE Awm_mtext_77
		 CENTER {lola_msg 125 15 "Menu text"} "Data Management"
END_DEFINE
DEFINE Awm_mtext_78
		 CENTER {lola_msg 126 21 "Menu text"} "Data Management"
END_DEFINE
LET Awm_mtext_79 {lola_msg 127 10 "Menu text"} "Subpart"
LET Awm_mtext_80 {lola_msg 128 10 "Menu text"} "Masterdata"
DEFINE Awm_mtext_81
		 CENTER {lola_msg 130 10 "Menu text"} "Open"
END_DEFINE
DEFINE Awm_mtext_82
		 CENTER {lola_msg 131 10 "Menu text"} "Editor"
END_DEFINE

DEFINE Awm_mtext_83
		 CENTER {lola_msg 248 8 "Toolbar text"} "ADD PDF"
END_DEFINE
{ Table Titles }

LET Awm_ttitle_1 {lola_msg 91  3 "Table Title"} "OFF"
LET Awm_ttitle_2 {lola_msg 92 16 "Table Title"} "TEXT REF NAMES"
LET Awm_ttitle_3 {lola_msg 93 30 "Table Title"} "TEXT REFERENCE NAMES & VALUES"
LET Awm_ttitle_4 {lola_msg 94 10 "Table Title"} "NAMES"
LET Awm_ttitle_5 {lola_msg 95 10 "Table Title"} "VALUES"
LET Awm_ttitle_6 {lola_msg 96 50 "Table Title"} "TEXT REFERENCE INFORMATION  [ @s0 items found ]"
LET Awm_ttitle_7 {lola_msg 97 10 "Table Title"} "TABLE"
LET Awm_ttitle_8 {lola_msg 98 10 "Table Title"} "NAME"
LET Awm_ttitle_9 {lola_msg 99 6 "Table Title"} "COLUMN"
LET Awm_ttitle_10 {lola_msg 100 6 "Table Title"} "WIDTH"
LET Awm_ttitle_11 {lola_msg 101 6 "Table Title"} "LINES"
LET Awm_ttitle_12 {lola_msg 102 12 "Table Title"} "PRECISION"
LET Awm_ttitle_13 {lola_msg 103 9 "Table Title"} "WORD WRAP"
LET Awm_ttitle_14 {lola_msg 104 9 "Table Title"} "CASE CONV"

{ New General messages }
LET Awm_msg_21 {lola_msg 105  80 "Prompt message"}
    "Identify part to assign WM-part data or CURRENT or TOP"
LET Awm_msg_22  {lola_msg 106  80 "Error message"}
    "Invalid part name: "
LET Awm_msg_23  {lola_msg 107  80 "Prompt message"}
    "Enter number of structure levels:"

{ Message if BOM not enabled }
LET Awm_msg_24  {lola_msg 108 80 "Error message"}
	"Configuration Error: BOM not enabled"

LET Awm_msg_25  {lola_msg 109 80 "Error message"}
	"There is still a RCC pending"
LET Awm_msg_26  {lola_msg 110 80 "Display message"}
	"Set part infos: "
LET Awm_msg_27  {lola_msg 111 80 "Display message"}
	"Read parts tree ..."
LET Awm_msg_28  {lola_msg 112 80 "Display message"}
	"Inquire part infos: "
LET Awm_msg_29 {lola_msg 120 80 "Prompt message"} "Enter drawing name:"
LET Awm_msg_30 {lola_msg 121 80 "Error message"} "No drawing file exists"
LET Awm_msg_31 {lola_msg 122 80 "Error message"} "No drawing class registered"
LET Awm_msg_32 {lola_msg 123 80 "Prompt message"} "Enter drawing version:"
LET Awm_msg_33 {lola_msg 124 80 "Prompt message"} "Enter description:"
LET Awm_msg_34 {lola_msg 129 80 "Prompt message"} "Enter change note text:"

{* Toolbar Item Text Messages for NewUI (PELOOK = 2,3) *}

LET Awm_ttext_1 {lola_msg 131 1 "Toolbar text"} "A"
LET Awm_ttext_2 {lola_msg 132 2 "Toolbar text"} "A0"
LET Awm_ttext_3 {lola_msg 133 2 "Toolbar text"} "A1"
LET Awm_ttext_4 {lola_msg 134 2 "Toolbar text"} "A2"
LET Awm_ttext_5 {lola_msg 135 2 "Toolbar text"} "A3"
LET Awm_ttext_6 {lola_msg 136 2 "Toolbar text"} "A4"
LET Awm_ttext_7 {lola_msg 137 16 "Toolbar text"} "Absolute"
LET Awm_ttext_8 {lola_msg 138 8 "Toolbar text"} "All"
DEFINE Awm_ttext_9
  ({lola_msg 139 30 "Toolbar text" "1 -> assign level"}
  "Assign Levels : "+ (STR(Awmc_val_assign_level)))
END_DEFINE
LET Awm_ttext_10 {lola_msg 140 20 "Toolbar text"} "Assign Parts"
LET Awm_ttext_11 {lola_msg 141 20 "Toolbar text"} "Assign References"
LET Awm_ttext_12 {lola_msg 142 1 "Toolbar text"} "B"
LET Awm_ttext_13 {lola_msg 143 20 "Toolbar text"} "BOM Layout"
LET Awm_ttext_14 {lola_msg 144 20 "Toolbar text"} "BOM Settings"
LET Awm_ttext_15 {lola_msg 145 8 "Toolbar text"} "BOM"
LET Awm_ttext_16 {lola_msg 146 20 "Toolbar text"} "BOM-Config"
LET Awm_ttext_17 {lola_msg 147 1 "Toolbar text"} "C"
DEFINE Awm_ttext_18
  ({lola_msg 148 30 "Toolbar text" "1 -> convention value"}
  "Case Convention : " + Tr_case_convention_value)
END_DEFINE
LET Awm_ttext_19 {lola_msg 149 20 "Toolbar text"} "Case Convention"
LET Awm_ttext_20 {lola_msg 150 20 "Toolbar text"} "Change"
LET Awm_ttext_21 {lola_msg 151 20 "Toolbar text"} "Clear ID"
LET Awm_ttext_22 {lola_msg 152 20 "Toolbar text"} "Continue"
LET Awm_ttext_23 {lola_msg 153 20 "Toolbar text"} "Create by Ident"
LET Awm_ttext_24 {lola_msg 154 20 "Toolbar text"} "Create from List"
LET Awm_ttext_25 {lola_msg 155 20 "Toolbar text"} "Create from Table"
LET Awm_ttext_26 {lola_msg 156 20 "Toolbar text"} "Current"
LET Awm_ttext_27 {lola_msg 157  1 "Toolbar text"} "D"
LET Awm_ttext_28 {lola_msg 158 20 "Toolbar text"} "Delete PosNo"
LET Awm_ttext_29 {lola_msg 159 80 "Toolbar text"} "DesignManager ADVANCED"
LET Awm_ttext_30 {lola_msg 160 80 "Toolbar text"} "DesignManager DESIGNER"
LET Awm_ttext_31 {lola_msg 161 80 "Toolbar text"} "DesignManager DESKTOP"
LET Awm_ttext_32 {lola_msg 162 80 "Toolbar text"} "DesignManager EXPERT"
LET Awm_ttext_33 {lola_msg 163 80 "Toolbar text"} "DesignManager EXPERT CLASSIC"
LET Awm_ttext_34 {lola_msg 164 20 "Toolbar text"} "Draw Names"
LET Awm_ttext_35 {lola_msg 165 20 "Toolbar text"} "Draw Values"
LET Awm_ttext_36 {lola_msg 166 20 "Toolbar text"} "Drawing ID"
LET Awm_ttext_37 {lola_msg 167 20 "Toolbar text"} "Drawing"
LET Awm_ttext_38 {lola_msg 168 1 "Toolbar text"} "E"
LET Awm_ttext_39 {lola_msg 169 8 "Toolbar text"} "End"
LET Awm_ttext_40 {lola_msg 170 20 "Toolbar text"} "Flag Layout"
LET Awm_ttext_41 {lola_msg 171 20 "Toolbar text"} "Horizontal"
DEFINE Awm_ttext_42
  ({lola_msg 172 30 "Toolbar text" "1 -> bom val increment"}
  "Increment : " + (STR(Awmc_bom_val_increment)))
END_DEFINE
LET Awm_ttext_43 {lola_msg 173 20 "Toolbar text"} "Info Drawing"
LET Awm_ttext_44 {lola_msg 174 10 "Toolbar text"} "Info"
LET Awm_ttext_45 {lola_msg 175 10 "Toolbar text"} "Left"
DEFINE Awm_ttext_46
  ({lola_msg 176 30 "Toolbar text" "1 -> lines value"}
  "Lines : " + Tr_set_lines_value)
END_DEFINE
LET Awm_ttext_47 {lola_msg 177 20 "Toolbar text"} "Lines"
LET Awm_ttext_48 {lola_msg 178 20 "Toolbar text"} "Load Title-Block"
LET Awm_ttext_49 {lola_msg 179 20 "Toolbar text"} "Load"
LET Awm_ttext_50 {lola_msg 180 20 "Toolbar text"} "Lower"
LET Awm_ttext_51 {lola_msg 181 20 "Toolbar text"} "New Drawing"
LET Awm_ttext_52 {lola_msg 182 20 "Toolbar text"} "New Part&Draw."
LET Awm_ttext_53 {lola_msg 183 20 "Toolbar text"} "New Version"
LET Awm_ttext_54 {lola_msg 184 8 "Toolbar text"} "Off"
LET Awm_ttext_55 {lola_msg 185 8 "Toolbar text"} "On"
LET Awm_ttext_56 {lola_msg 186 20 "Toolbar text"} "Open BOM"
LET Awm_ttext_57 {lola_msg 187 20 "Toolbar text"} "Options"
LET Awm_ttext_58 {lola_msg 188 20 "Toolbar text"} "Overwrite"
LET Awm_ttext_59 {lola_msg 189 20 "Toolbar text"} "POS-Number"
LET Awm_ttext_60 {lola_msg 190 8 "Toolbar text"} "Part"
LET Awm_ttext_61 {lola_msg 191 20 "Toolbar text"} "Pos-No"
DEFINE Awm_ttext_62
  ({lola_msg 192 30 "Toolbar text" "1 -> Precision value"}
  "Precision : " + Tr_set_precision_value)
END_DEFINE
LET Awm_ttext_63 {lola_msg 193 20 "Toolbar text"} "Precision"
LET Awm_ttext_64 {lola_msg 194 20 "Toolbar text"} "Redraw PosNo"
LET Awm_ttext_65 {lola_msg 195 20 "Toolbar text"} "Reference Names"
LET Awm_ttext_66 {lola_msg 196 20 "Toolbar text"} "Reference Values"
LET Awm_ttext_67 {lola_msg 197 20 "Toolbar text"} "Remove PosNo All"
LET Awm_ttext_68 {lola_msg 198 20 "Toolbar text"} "Remove PosNo"
LET Awm_ttext_69 {lola_msg 199 20 "Toolbar text"} "Reserve Drawing"
LET Awm_ttext_70 {lola_msg 200 20 "Toolbar text"} "Rev Check"
LET Awm_ttext_71 {lola_msg 201 8 "Toolbar text"} "Right"
LET Awm_ttext_72 {lola_msg 202 20 "Toolbar text"} "Scan (Add)"
LET Awm_ttext_73 {lola_msg 203 20 "Toolbar text"} "Scan (Clear)"
DEFINE Awm_ttext_74
  ({lola_msg 204 30 "Toolbar text" "1 -> Scan level"}
  "Scan Levels : " + (STR(Awmc_val_scan_level)))
END_DEFINE
LET Awm_ttext_75 {lola_msg 205 20 "Toolbar text"} "Select Classes"
LET Awm_ttext_76 {lola_msg 206 20 "Toolbar text"} "Select"
LET Awm_ttext_77 {lola_msg 207 20 "Toolbar text"} "Set Line End Point"
LET Awm_ttext_78 {lola_msg 208 20 "Toolbar text"} "Settings"
LET Awm_ttext_79 {lola_msg 209 20 "Toolbar text"} "Show BOM"
LET Awm_ttext_80 {lola_msg 210 20 "Toolbar text"} "Show Data"
LET Awm_ttext_81 {lola_msg 211 20 "Toolbar text"} "Show Part Status"
LET Awm_ttext_82 {lola_msg 212 20 "Toolbar text"} "Show References"
DEFINE Awm_ttext_83
  ({lola_msg 213 30 "Toolbar text" "1 -> Bom start posnr"}
  "Start POS : " + (STR(Awmc_bom_val_start_posnr)))
END_DEFINE
LET Awm_ttext_84 {lola_msg 214 20 "Toolbar text"} "Store All"
LET Awm_ttext_85 {lola_msg 215 20 "Toolbar text"} "Store Part"
LET Awm_ttext_86 {lola_msg 216 20 "Toolbar text"} "Store Subpart"
LET Awm_ttext_87 {lola_msg 217 20 "Toolbar text"} "Store"
LET Awm_ttext_88 {lola_msg 218 20 "Toolbar text"} "Subpart"
LET Awm_ttext_89 {lola_msg 219 20 "Toolbar text"} "TB Conf"
LET Awm_ttext_90 {lola_msg 220 20 "Toolbar text"} "TB-BOM Conf"
LET Awm_ttext_91 {lola_msg 221 20 "Toolbar text"} "TB-Config"
LET Awm_ttext_92 {lola_msg 222 20 "Toolbar text"} "Table to Drawing"
LET Awm_ttext_93 {lola_msg 223 20 "Toolbar text"} "Title-Block"
LET Awm_ttext_94 {lola_msg 224 20 "Toolbar text"} "Top Part"
LET Awm_ttext_95 {lola_msg 225 8 "Toolbar text"} "Top"
LET Awm_ttext_96 {lola_msg 226 8 "Toolbar text"} "USER"
LET Awm_ttext_97 {lola_msg 227 20 "Toolbar text"} "Unassign"
LET Awm_ttext_98 {lola_msg 228 20 "Toolbar text"} "Unreserve"
LET Awm_ttext_99 {lola_msg 229 20 "Toolbar text"} "Update Drawing"
LET Awm_ttext_100 {lola_msg 230 20 "Toolbar text"} "Update TB"
LET Awm_ttext_101 {lola_msg 231 20 "Toolbar text"} "Update from ME10"
LET Awm_ttext_102 {lola_msg 232 8 "Toolbar text"} "Update"
LET Awm_ttext_103 {lola_msg 233 8 "Toolbar text"} "Upper"
LET Awm_ttext_104 {lola_msg 234 8 "Toolbar text"} "Vertical"
LET Awm_ttext_105 {lola_msg 235 20 "Toolbar text"} "WM-Part ID"
LET Awm_ttext_106 {lola_msg 236 8 "Toolbar text"} "WMDT"
DEFINE Awm_ttext_107
  ({lola_msg 237 20 "Toolbar text" "1 -> Width"}
   "Width : " + Tr_set_width_value)
END_DEFINE
LET Awm_ttext_108 {lola_msg 238 8 "Toolbar text"} "Width"
LET Awm_ttext_109 {lola_msg 239 20 "Toolbar text"} "Word Wrap"
LET Awm_ttext_110 {lola_msg 240 8 "Toolbar text"} "DIN"
LET Awm_ttext_111 {lola_msg 241 8 "Toolbar text"} "ISO"

LET Awm_ttext_112 {lola_msg 242 8 "Toolbar text"} "WorkSpace"
LET Awm_ttext_113 {lola_msg 244 8 "Toolbar text"} "Find"
LET Awm_ttext_114  {lola_msg 248 8 "Toolbar text"} "ADD PDF"
LET Awm_mttext_1  {lola_msg 246 15 "Menu/Toolbar text"} "Drawing Manager"

{* Toolbar Messages New User Interface (PELOOK = 2,3) *}

LET Awm_tmess_1 {lola_msg 245 80 "Toolbar text"} "ADU is not supported in Windows User Interface"

{ >>>endif }
