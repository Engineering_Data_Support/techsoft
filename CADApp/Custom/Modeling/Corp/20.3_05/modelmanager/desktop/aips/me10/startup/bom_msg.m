{  @(#)  bom_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of BOM-Module
}

{ >>>if false }

{ LOLAMAX = 162 }

{ General messages }

LET Awm_bom_msg_1 {lola_msg 1  80 "Prompt message" }
	"Enter CONFIRM to remove all position numbers of the current BOM"
LET Awm_bom_msg_2 {lola_msg 2  80 "Prompt message" }
    "No position number layout loaded; select layout, then retry action"
LET Awm_bom_msg_3 {lola_msg 3  80 "Prompt message" }
    "Identify position number to be removed"
LET Awm_bom_msg_4 {lola_msg 4  80 "Prompt message" }
    "No position number identified"
LET Awm_bom_msg_5 {lola_msg 5  80 "Prompt message" }
    "Element contains no position info"
LET Awm_bom_msg_6 {lola_msg 6  80 "Prompt message" }
    "Identify part to assign position number"
LET Awm_bom_msg_7 {lola_msg 7  80 "Prompt message" }
    "Part is not in the current BOM tree"
LET Awm_bom_msg_8 {lola_msg 8  80 "Prompt message" }
    "Position number already assigned"
LET Awm_bom_msg_9 {lola_msg 9  80 "Prompt message" }
    "No WM-part data assigned to this part"
LET Awm_bom_msg_10 {lola_msg 10 80 "Prompt message" }
    "Enter begin point of position leader line or press CONTINUE to proceed with next part"
LET Awm_bom_msg_11 {lola_msg 11 80 "Prompt message" }
    "Enter begin point of horizontal position leader line; press END to complete or press CONTINUE to proceed with next part"
LET Awm_bom_msg_12 {lola_msg 12 80 "Prompt message" }
    "Enter begin point of vertical position leader line; press END to complete or press CONTINUE to proceed with next part"
LET Awm_bom_msg_13 {lola_msg 13 80 "Prompt message" }
    "Indicate point on position leader line; press END to finish this part and start next part or press CONTINUE to skip this part and continue with next part"
LET Awm_bom_msg_14 {lola_msg 14 80 "Prompt message" }
    "Select BOM component"
LET Awm_bom_msg_15 {lola_msg 15 80 "Prompt message" }
	"Invalid input"
LET Awm_bom_msg_16 {lola_msg 16 80 "Prompt message" }
	"BOM does not contain this part"
LET Awm_bom_msg_17 {lola_msg 17 80 "Prompt message" }
    "No parts found without a position number"
LET Awm_bom_msg_18 {lola_msg 18 80 "Prompt message" }
    "Identify position number to redraw position leader line"
LET Awm_bom_msg_19 {lola_msg 19 80 "Prompt message" }
	"Select BOM component to draw position flag"
LET Awm_bom_msg_20 {lola_msg 20 80 "Prompt message" }
    "Invalid macro file"
LET Awm_bom_msg_21 {lola_msg 21 80 "Prompt message" }
    "Exchange file not found"
LET Awm_bom_msg_22 {lola_msg 22 80 "Prompt message" }
	"Invalid parts structure: Position flag layout must not contain subparts"
LET Awm_bom_msg_23 {lola_msg 23 80 "Prompt message" }
    "Endpoint 'UPPER LEFT' is not defined"
LET Awm_bom_msg_24 {lola_msg 24 80 "Prompt message" }
    "Endpoint 'UPPER RIGHT' is not defined"
LET Awm_bom_msg_25 {lola_msg 25 80 "Prompt message" }
    "Endpoint 'LOWER LEFT' is not defined"
LET Awm_bom_msg_26 {lola_msg 26 80 "Prompt message" }
    "Endpoint 'LOWER RIGHT' is not defined"
LET Awm_bom_msg_27 {lola_msg 27 80 "Prompt message" }
	"Identify part for BOM header"
LET Awm_bom_msg_28 {lola_msg 28 80 "Prompt message" }
	"Identify part for BOM component"
LET Awm_bom_msg_29 {lola_msg 29 80 "Prompt message" }
	"Invalid parts structure: BOM layout has to consist of BOM header and BOM component part"
LET Awm_bom_msg_30 {lola_msg 30 80 "Prompt message" }
	"Select option or identify existing end point of position leader line"
LET Awm_bom_msg_31 {lola_msg 31 80 "Prompt message" }
	"Enter LEFT or RIGHT or new end point for position leader line"
LET Awm_bom_msg_32 {lola_msg 32 80 "Prompt message" }
	"Enter new end point for position leader line"
LET Awm_bom_msg_33 {lola_msg 33 80 "Prompt message" }
	"No BOM assigned from WorkManager!"
LET Awm_bom_msg_34 {lola_msg 34 80 "Prompt message" }
    "No BOM layout loaded"
LET Awm_bom_msg_35 {lola_msg 35 80 "Prompt message" }
    "Identify position in BOM"
LET Awm_bom_msg_36 {lola_msg 36 80 "Prompt message" }
	"Identify assembly to generate BOM"
LET Awm_bom_msg_37 {lola_msg 37 80 "Prompt message" }
    "WM-BOM not available!"
LET Awm_bom_msg_38 {lola_msg 38 80 "Prompt message" }
	"No assembly identified"
LET Awm_bom_msg_39 {lola_msg 39 80 "Prompt message" }
	"Enter new position number"
LET Awm_bom_msg_40 {lola_msg 40 80 "Prompt message" }
	"Component does not correspond to WM-BOM"
LET Awm_bom_msg_41 {lola_msg 41 80 "Prompt message" }
	"Identify assembly for current BOM"
LET Awm_bom_msg_42 {lola_msg 42 80 "Prompt message" }
    "No ME10 part found which corresponds to the BOM header"
LET Awm_bom_msg_43 {lola_msg 43 80 "Prompt message" }
"Enter CONFIRM to update BOM in WorkManager"
LET Awm_bom_msg_44 {lola_msg 44 80 "Prompt message" }
"Enter CONFIRM to update BOM in ME10 with the current ME10 parts structure"
LET Awm_bom_msg_45 ""
LET Awm_bom_msg_46 {lola_msg 46 80 "Prompt message" }
"Enter CONFIRM to delete all position numbers in the ME10 BOM"
LET Awm_bom_msg_47 {lola_msg 47 80 "Prompt message" }
	"Identify assembly to open the BOM in WorkManager or CURRENT"
LET Awm_bom_msg_48 {lola_msg 48 80 "Prompt message" }
	"Part has no WM-part data attached"
LET Awm_bom_msg_49 {lola_msg 49 80 "Prompt message" }
	"Enter start position number:"
LET Awm_bom_msg_50 {lola_msg 50 80 "Prompt message" }
	"Enter position number increment value:"
LET Awm_bom_msg_51 {lola_msg 51 80 "Prompt message" }
	"Identify part for BOM component"
LET Awm_bom_msg_52 {lola_msg 52 80 "Prompt message" }
    "Endpoint 'UPPER' is not defined"
LET Awm_bom_msg_53 {lola_msg 53 80 "Prompt message" }
    "Endpoint 'LOWER' is not defined"
LET Awm_bom_msg_54 {lola_msg 54 80 "Prompt message" }
    "Endpoint 'RIGHT' is not defined"
LET Awm_bom_msg_55 {lola_msg 55 80 "Prompt message" }
    "Endpoint 'LEFT' is not defined"

LET Awm_bom_msg_56 {lola_msg 56 80 "Prompt message" }
    "Component appears in more than one BOM"
LET Awm_bom_msg_57 {lola_msg 57 80 "Prompt message" }
    "cannot combine n-level-BOM and 1-level-BOM"
LET Awm_bom_msg_58 {lola_msg 58 80 "Prompt message" }
    "update not possible in n-level-BOM"

LET Awm_bom_msg_59  {lola_msg 59  80 "Prompt message"}
    "Enter number of structure levels:"
LET Awm_bom_msg_21 {lola_msg 60  80 "Prompt message"}
    "Identify part to scan BOM data or CURRENT or TOP"
LET Awm_bom_msg_22  {lola_msg 61  80 "Error message"}
    "Invalid part name: "

{ Menu texts }

LET Awm_bom_mtext_1 {lola_msg 71  10 "Menu text" } "POS-NO"
LET Awm_bom_mtext_2 {lola_msg 72  10 "Menu text" } "BOM"
LET Awm_bom_mtext_3 {lola_msg 73  10 "Menu text" } "BOM-CNF"
LET Awm_bom_mtext_4 {lola_msg 74  10 "Menu text" } "POS CREATE"
LET Awm_bom_mtext_5 {lola_msg 75  10 "Menu text" } "List"
LET Awm_bom_mtext_6 {lola_msg 76  10 "Menu text" } "Ident"
LET Awm_bom_mtext_7 {lola_msg 77  10 "Menu text" } "HORIZONTAL"
LET Awm_bom_mtext_8 {lola_msg 78  10 "Menu text" } "VERTICAL"
LET Awm_bom_mtext_9 {lola_msg 79  10 "Menu text" } "REDRAW"
LET Awm_bom_mtext_10 {lola_msg 80  10 "Menu text" } "Pos-No"
LET Awm_bom_mtext_11 {lola_msg 81  10 "Menu text" } "DELETE"
LET Awm_bom_mtext_12 {lola_msg 82  10 "Menu text" } "All"
LET Awm_bom_mtext_13 {lola_msg 83  10 "Menu text" } "START-POS"
LET Awm_bom_mtext_14 {lola_msg 84  10 "Menu text" } "INCREMENT"
LET Awm_bom_mtext_15 {lola_msg 85  10 "Menu text" } "POS-LAYOUT"
LET Awm_bom_mtext_16 {lola_msg 86  10 "Menu text" } "OUTPUT"
LET Awm_bom_mtext_17 {lola_msg 87  10 "Menu text" } "Drawing"
LET Awm_bom_mtext_18 {lola_msg 88  10 "Menu text" } "BOM LAYOUT"
LET Awm_bom_mtext_19 {lola_msg 89  10 "Menu text" } "OPEN BOM"
LET Awm_bom_mtext_20 {lola_msg 90  10 "Menu text" } "TOP"
LET Awm_bom_mtext_21 {lola_msg 91  10 "Menu text" } "Part"
LET Awm_bom_mtext_22 {lola_msg 92  10 "Menu text" } "SHOW"
LET Awm_bom_mtext_23 {lola_msg 93  10 "Menu text" } "BOM"
LET Awm_bom_mtext_24 ""
LET Awm_bom_mtext_25 {lola_msg 95  10 "Menu text" } "UPDATE"
LET Awm_bom_mtext_26 ""
LET Awm_bom_mtext_27 ""
LET Awm_bom_mtext_28 ""
LET Awm_bom_mtext_29 ""
LET Awm_bom_mtext_30 {lola_msg 100  10 "Menu text" } "ME10 -> WM"
LET Awm_bom_mtext_31 {lola_msg 101  10 "Menu text" } "Pos-No"
LET Awm_bom_mtext_32 {lola_msg 102  10 "Menu text" } "CURRENT"
LET Awm_bom_mtext_33 {lola_msg 103  10 "Menu text" } "Parts Stat"
LET Awm_bom_mtext_34 {lola_msg 104  10 "Menu text" } "CONTINUE"
LET Awm_bom_mtext_35 {lola_msg 105  10 "Menu text" } "END"
LET Awm_bom_mtext_36 {lola_msg 106  10 "Menu text" } "LINE END P"
LET Awm_bom_mtext_37 {lola_msg 107  10 "Menu text" } "Set"
LET Awm_bom_mtext_38 {lola_msg 108  10 "Menu text" } "UPPER"
LET Awm_bom_mtext_39 {lola_msg 109  10 "Menu text" } "LOWER"
LET Awm_bom_mtext_40 {lola_msg 110  10 "Menu text" } "LEFT"
LET Awm_bom_mtext_41 {lola_msg 111  10 "Menu text" } "RIGHT"
LET Awm_bom_mtext_42 {lola_msg 112  10 "Menu text" } "STORE"
LET Awm_bom_mtext_43 {lola_msg 113  10 "Menu text" } "BOM Layout"
LET Awm_bom_mtext_44 {lola_msg 114  10 "Menu text" } "REMOVE"
LET Awm_bom_mtext_45 {lola_msg 115  10 "Menu text" } "BOM Table"
LET Awm_bom_mtext_46 {lola_msg 116  10 "Menu text" } "DIN"
LET Awm_bom_mtext_47 {lola_msg 117  10 "Menu text" } "ISO"
LET Awm_bom_mtext_48 {lola_msg 118  10 "Menu text" } "USER"
LET Awm_bom_mtext_49 {lola_msg 119  10 "Menu text" } "A"
LET Awm_bom_mtext_50 {lola_msg 120  10 "Menu text" } "B"
LET Awm_bom_mtext_51 {lola_msg 121  10 "Menu text" } "C"

LET Awm_bom_mtext_52 {lola_msg 122 10 "Menu text" } "SCAN BOM"
LET Awm_bom_mtext_53                                ""
LET Awm_bom_mtext_54 {lola_msg 124 10 "Menu text" } "ADD"
LET Awm_bom_mtext_55 {lola_msg 125 10 "Menu text" } "CLEAR"
LET Awm_bom_mtext_56 {lola_msg 126 10 "Menu text" } "CURRENT"
LET Awm_bom_mtext_57 {lola_msg 127 10 "Menu text" } "TOP"
LET Awm_bom_mtext_58 {lola_msg 128 10 "Menu text" } "LEVELS"

{ Table Titles }

LET Awm_bom_ttitle_1  {lola_msg 151 20 "Table title" } "Part-Name"
LET Awm_bom_ttitle_2  {lola_msg 152 20 "Table title" } "Part-ID"
LET Awm_bom_ttitle_3  {lola_msg 153 20 "Table title" } "WM-Part"
LET Awm_bom_ttitle_4  {lola_msg 154 20 "Table title" } "BOM"
LET Awm_bom_ttitle_5  {lola_msg 155 20 "Table title" } "Pos-No"
LET Awm_bom_ttitle_6  {lola_msg 156 20 "Table title" } "X"
LET Awm_bom_ttitle_7  {lola_msg 157 20 "Table title" } "ERROR"
LET Awm_bom_ttitle_8  {lola_msg 158 20 "Table title" } "FROM"
LET Awm_bom_ttitle_9  {lola_msg 159 20 "Table title" } "CURRENT ME10-BOM"
LET Awm_bom_ttitle_10 {lola_msg 160 20 "Table title" } "ME10-PART STATUS INFO"
LET Awm_bom_ttitle_11 {lola_msg 161 20 "Table title" } "CURRENT BOM COMPONENTS"
LET Awm_bom_ttitle_12 {lola_msg 162 20 "Table title" } "Message"

{ >>>endif }
