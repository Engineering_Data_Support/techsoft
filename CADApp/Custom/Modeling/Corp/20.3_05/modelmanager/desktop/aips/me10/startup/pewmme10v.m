{  @(#)  pewmme10v.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{   Macro re-definitions for ME10v                                      }
{ **********************************************************************}

DEFINE Tm_window_fit
    Window_fit
END_DEFINE

DEFINE Call_last_menu
LOCAL L_menu
    LET L_menu (VAL Last_menu)
    L_menu
END_DEFINE

DEFINE Menu_control_icons
    WAIT 0
END_DEFINE

DEFINE Table_control_icons
PARAMETER Table_name
    WAIT 0
END_DEFINE

