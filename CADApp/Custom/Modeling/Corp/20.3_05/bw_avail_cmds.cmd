﻿; Folgende Sonderzeichen entsprechen folgenden Umlauten:
; Ï=ü
; Ì=ä
; Î=ö
; Ø=Ä
; Ú=Ö
; Û=Ü
; Þ=ß


(:Application "All")

 
;;----- SolidDesigner:  --------------------------------------------
 
(:Application "SolidDesigner")

  (:Group "Borgware" :title "Borgware")

  
 ("Teamviewer starten"
 :title      (ui::multi-lang "Start Teamviewer" :german "Teamviewer starten")
 :action      "(oli::sd-sys-background-job (format nil \"\\\"~a\\\\\\bw_tools\\\\addons\\\\teamviewer\\\\TeamViewerQS_de-idc2gqabsh.exe\\\"\" (oli::sd-sys-getenv \"BASEDIR\")))"
 :description  (ui::multi-lang "Start the teamviewer client to collaborate with a BORGWARE support engineer" :german "Startet eine Teamviewer Sitzung")
 :image       "icons/teamviewer"
:ui-behavior :DEFAULT) 
 
("BORGWARE Support per E-Mail"
 :title      "BORGWARE Support via E-Mail"
:action      "(oli::sd-sys-background-job (format nil \"explorer.exe \\\"mailto:support@borgwareplm.de?subject=Supportanfrage zu ~a\\\"\" (oli::sd-sys-getenv \"LINKNAME\") (oli::sd-sys-getenv \"TEMP\")))"
 :description (ui::multi-lang "Contact a BORGWARE support engineer by email" :german "BORGWARE Support via E-Mail kontaktieren")
 :image       "icons/email"
:ui-behavior :DEFAULT) 
 
 ("BORGWAREWWW"
 :title "BORGWAREPLM Webseite"
 :image       "icons/www"
 :action      "(oli::sd-display-url \"http://www.borgwareplm.de\")"
 :description (ui::multi-lang "Show Borgwareplm Website" :german "Webseite der Borgwareplm GmbH")
 :ui-behavior :DEFAULT) 
 
 ("SDCORPCUSTOMIZEDIR anzeigen"
 :title      "Show SDCORPCUSTOMIZEDIR"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDCORPCUSTOMIZEDIR\")))"
 :description "Show the directory for corporate wide settings"
 :image       "icons/corp"
:ui-behavior :DEFAULT) 
 
 ("SDSITECUSTOMIZEDIR anzeigen"
 :title      "Show SDSITECUSTOMIZEDIR"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDSITECUSTOMIZEDIR\")))"
 :description "Show the directory for site wide settings"
 :image       "icons/site"
:ui-behavior :DEFAULT) 
 
 ("SDUSERCUSTOMIZEDIR anzeigen"
 :title      "SDUSERCUSTOMIZEDIR anzeigen"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDUSERCUSTOMIZEDIR\")))"
 :description "Show the directory for user defined settings"
 :image       "icons/user"
:ui-behavior :DEFAULT) 
 
 ("License Server auflisten"
 :title      "License Server auflisten"
 :action      "(frame2-ui::display (oli::sd-sys-getenv \"LICENSESERVER\"))"
 :description "Konfigurierte Lizenzserver anzeigen"
 :image       "icons/licenseserver"
:ui-behavior :DEFAULT) 
 
("PTC Online Documentation"
 :title      "PTC Online Documentation"
 :action      "(oli::sd-display-url \"http://www.ptc.com/cs/help/creo_elements_direct_hc/modeling_190_hc/\")"
 :description "PTC Online Dokumentation - Öffnet die Online Dokumentation zu Creo Elements/Direct Modeling von der PTC Website (Internet-Verbindung erforderlich)"
 :image       "icons/ptc"
:ui-behavior :DEFAULT) 
 
("PTC Online Tutorials"
 :title "Online Tutorials"
 :action      "(oli::sd-display-url \"http://learningexchange.ptc.com/tutorials/listing/product_version_id:81\")"
 :description "PTC Online Tutorials - Übersicht über Creo Elements/Direct Modeling Tutorials von der PTC Website (Internet connection required)"
 :image       "icons/ptcu"
:ui-behavior :DEFAULT) 

  ("rnd_part_color"
   :title (ui::multi-lang "Color Part" :german "Teile einfärben")
   :image "icons/rnd_col"
   :action "rnd_part_color"
   :description (ui::multi-lang "Color Part with random color" :german "Färbt Teile mit zufälliger Farbe ein.")
 :ui-behavior :DEFAULT) 
 
 ("show_undo"  
   :title "show_undo"
   :image "icons/einblenden"
   :action "show_undo"
   :description "show_undo"
 :ui-behavior :DEFAULT) 
 
 ("bw_wendel"
   :title "Teil wendeln"
   :image "icons/dupl"
   :action "bw_wendel"
   :description "Teil kopieren und wendeln."
 :ui-behavior :DEFAULT) 
 
("loesche_leere_bgr"
   :title "Leere löschen"
   :image "icons/delete_assy"
   :action "loesche_leere_bgr"
   :description "Löscht leere Teile und leere Baugruppen."
 :ui-behavior :DEFAULT) 
 
 ("bw_ass_ren"
   :title "Teilename->Modellname"
   :image "icons/mirror"
   :action "bw_ass_ren"
   :description "Ändert Modellnamen auf den Teilenamen."
 :ui-behavior :DEFAULT) 
 
 ("bw_part_props"
   :title "Teileeigenschaft"
   :image "icons/teileigenschaft"
   :action "bw_part_props"
   :description "Ändert Facettengenauigkeit von Teilen"
 :ui-behavior :DEFAULT) 
 
 ("bw_stp_exp"
   :title "STEP Export"
   :image "icons/save_step"
   :action "bw_stp_exp"
   :description "Speichert alle Teile unterhalb einer Baugruppe im STEP Format ab."
:ui-behavior :DEFAULT) 
  
 ("bw_ass_copy"
   :title "Baugruppe kopieren"
   :image "icons/ltab"
   :action "bw_ass_copy"
   :description "Kopiert Baugruppe und ordnet Zeichnung zur Kopie zu."
 :ui-behavior :DEFAULT) 
  
 ("bw_ex_rename"
   :title "Exemplare Umbenennen"
   :image "icons/ex_ren"
   :action "bw_ex_rename"
   :description "Umbenennen aller Exemplare unterhalb einer Baugruppe."
 :ui-behavior :DEFAULT) 
 
 ("bw_db"
   :title "bw_db"
   :image "icons/tabelle"
   :action "bw_db"
   :description "Materialattribute für ModelManager setzen."
 :ui-behavior :DEFAULT) 
  
 ("Module"
   :title "Modulaktivierung"
   :image "icons/bw"
   :action "(UI::UIC-SHOW-MODULE-CONTROLLER)"
   :description "Modulaktivierung"
 :ui-behavior :DEFAULT) 
 
 ("DC4_Druckfeder"
      :title "Druckfeder"
   :image "icons/bw"
   :action "dc4-druckfeder-dialog"
   :description "Erstellt eine Druckfeder"
 :ui-behavior :DEFAULT) 

 ("DC4_Zugfeder"
   :title "Zugfeder"
   :image "icons/bw"
   :action "dc4-zugfeder-dialog"
   :description "Erstellt eine Zugfeder"
 :ui-behavior :DEFAULT) 
 
 
("Black Design"
 :title       "Schwarz"
 :action      "(uib::win-set-skin-style :black)"
 :description "Switch user interface to black design"
 :image        "icons/black_design"
:ui-behavior :DEFAULT) 
  
("Silver Design"
 :title      "Silber"
 :action      "(uib::win-set-skin-style :silver)"
 :description "Switch user interface to silver design"
 :image       "icons/silver_design" 
 :ui-behavior :DEFAULT) 
 
("Blue Design"
 :title       "Blau"
 :action      "(uib::win-set-skin-style :blue)"
 :description "Switch user interface to blue design"
 :image        "icons/blue_design"
 :ui-behavior :DEFAULT) 

("Creo Design"
 :title      "Creo"
 :action      "(uib::win-set-skin-style :creo)"
 :description "Switch user interface to Creo design"
 :image        "icons/creo_design" 
 :ui-behavior :DEFAULT) 
 
;;----- Annotation:  --------------------------------------------

#| (:Application "Annotation")

(:Group "TECHSOFT" :title "TECHSOFT")

("Switch Annotation Color Scheme"
 :title       "Switch Annotation Color Scheme"
 :action      "(ts-switch-background-color)"
 :description "Switch between the default color scheme and the b/w color scheme in Annotation."
 :image       "Annotation/TECHSOFT/Switch_Annotation_Color_Scheme"
 :ui-behavior :DEFAULT) |#
