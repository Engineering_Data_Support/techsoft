;% Automatically written on 10/30/2014 at 09:07:39
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/General/TextStyle"
  :style :EXTRAGROSS
  :title "extraGross"
  :values
   '("AbsAngle" 0
     "Adjust" 1
     "Color" 255
     "Filled" LISP::T
     "Font" "hp_i3098_v"
     "Frame" :OFF
     "LineSpace" 2.2000000000000002
     "Ratio" 1
     "Size" 7.0
     "Slant" 0
     )
)
