;% Automatically written on 10/30/2014 at 08:32:34
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :NO_HATCH
  :title "KeinSchraff"
  :values
   '("Description" "L:1, A:  0.0, D: 1000000."
     "PatternAngle" 0.0
     "PatternDistance" 1000000.0
     "SubPattern" (:LABEL "KeinSchraff" :SUBPATTERN
                     ((:COLOR 0.0,0.0,0.0 :LINETYPE :SOLID :DISTANCE
                              1.0 :OFFSET 0.0 :ANGLE 0.0 :WIDTH 0)))
     )
)
