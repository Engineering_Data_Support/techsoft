;% Automatically written on 03/21/2007 at 15:27:08
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :HOLZ
  :title "Holz"
  :values '("Description" "L:1, A: 90.0, D:   2.0"
            "PatternDistance" 2.0
            "SubPattern" (:LABEL "Holz" :SUBPATTERN
                                 ((:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET 0 :ANGLE
                                          0.0 :WIDTH 0)))
            "PatternAngle" 1.5707963267948966
            )
)
