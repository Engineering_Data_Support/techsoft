;% Automatically written on 03/21/2007 at 15:27:08
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :LEGIERTER_STAHL
  :title "legierter_Stahl"
  :values '("Description" "L:3, A: 45.0, D:   4.0"
            "PatternDistance" 4.0
            "SubPattern" (:LABEL "legierter_Stahl" :SUBPATTERN
                                 ((:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET 0 :ANGLE
                                          0.0 :WIDTH 0)
                                  (:COLOR 1.0,1.0,0.0 :LINETYPE
                                          :LONG_DASHED :DISTANCE 1
                                          :OFFSET 0.25 :ANGLE 0.0
                                          :WIDTH 0)
                                  (:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1 :OFFSET 0.5
                                          :ANGLE 0.0 :WIDTH 0)))
            "PatternAngle" 0.78539816339744828
            )
)
