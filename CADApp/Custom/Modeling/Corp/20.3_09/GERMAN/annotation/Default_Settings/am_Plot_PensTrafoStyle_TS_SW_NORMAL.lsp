;% Automatically written on 03/14/2007 at 11:47:21
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot/PensTrafoStyle"
  :style :TS_SW_NORMAL
  :title "TS_SW_normal"
  :values '("Contents" :DRAWING
            "Description" "S/W-Druck normal"
            "Pens" (:LABEL "comment" :PENLIST
                           ((:OLD_LINETYPE :ALL :COLOR_DEF :ALL :COLOR1
                                0.0,0.0,0.0 :COLOR2
                                1.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.5 :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,0.0,0.0 :COLOR2
                                0.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE :OFF :PEN_NUMBER 0)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                1.0,0.0,0.0 :COLOR2
                                1.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.34999999999999998
                                :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,1.0,0.0 :COLOR2
                                0.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.34999999999999998
                                :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                1.0,1.0,0.0 :COLOR2
                                1.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.25 :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,0.0,1.0 :COLOR2
                                0.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.10000000000000002
                                :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                1.0,0.0,1.0 :COLOR2
                                1.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.10000000000000002
                                :PEN_NUMBER 1)
                            (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,1.0,1.0 :COLOR2
                                0.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE 0.10000000000000002
                                :PEN_NUMBER 1)
                            (:OLD_LINETYPE :PHANTOM :COLOR_DEF :SINGLE
                                :COLOR1
                                0.0,1.0,1.0 :COLOR2
                                0.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1
                                0 :PENSIZE2 1000000.0 :NEW_LINETYPE
                                :SAME :NEW_PENSIZE :OFF :PEN_NUMBER 0)))
            "TrueColorMode" LISP::NIL
            )
)
