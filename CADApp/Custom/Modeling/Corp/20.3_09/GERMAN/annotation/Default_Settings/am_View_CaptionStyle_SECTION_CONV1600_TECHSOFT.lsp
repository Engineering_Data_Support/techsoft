;% Automatically written on 09/29/2008 at 08:50:55
;% CoCreate Modeling Revision: 2008 (16.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/View/CaptionStyle"
  :style :SECTION_CONV1600_TECHSOFT
  :title "Section"
  :values '("Position" :BELOW
            "Case" :ASIS
            "Numbering" :LETTERANDNUMBERS
            "Offset" 5.0
            "Characters" "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
            )
)
