;% Automatically written on 03/07/2007 at 13:08:58
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/General/TextStyle"
  :style :|SEHR_GROß|
  :title "Sehr_Groß"
  :visible-if :TECHSOFT
  :values '("Color" 65280
            "Size" 7.0
            )
)
