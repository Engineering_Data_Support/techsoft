; ------------------------------------------------------------------------------
; SVN:  $Id: am_data.lsp 38125 2014-07-03 10:03:33Z pjahn $
; persistent data customization file for annotation (19.0)
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "18.0")
(persistent-data-module "ANNOTATION")

;;use techsoft default settings=============================

(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '(  ( "Annotation" :TECHSOFT ) ) )
 
;;views selected when creating a new drawing================
	   
(persistent-data
 :key "VIEW DEFAULTS"
 :value '( :CREATE-DRAWING-DEFAULT-VIEWS ( :LEFT :TOP :FRONT ) :UPDATE-TYPE 
           :UPDATE_WHOLE_DRAWING ) )  


 ;; ----------- end of file -----------