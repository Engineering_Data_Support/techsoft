; ---------------------------------------------------------------------------------
; SVN: $Id: am_qat_contents.lsp 38118 2014-07-03 07:45:53Z pjahn $
; customization file the quick access toolbar in creo elements/direct annotation 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ---------------------------------------------------------------------------------

(oli::sd-fluentui-set-quick-access-toolbar-contents "Annotation"
  '(
   (:AVAILCMD ("Annotation" "Filing" "New Session"))
   (:AVAILCMD ("Annotation" "Filing" "Load ..."))
   (:AVAILCMD ("Annotation" "Filing" "Save ..."))
   (:AVAILCMD ("Annotation" "Plot" "Plot"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "TECHSOFT" "Sheet Metal"))
   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))   
   ))