; ------------------------------------------------------------------------------
; SVN:  $Id: cfg_data.lsp 166 2010-05-12 04:49:53Z pjahn $
; persistent data customization file for creo elemtens/direct modeling 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "18.0")
(persistent-data-module "SOLIDDESIGNER")

;; set active color scheme
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "SolidDesigner/ColorSchemes" :COCREATE17_0 ) ) ) ;; :CREO

;; ----------- end of file -----------
