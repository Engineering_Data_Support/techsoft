﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Description:  Creo Elements/Direct Modeling default keyboard shortcut file
;
; (C) Copyright 2011 Parametric Technology GmbH, all rights reserved.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This file contains definitions of keyboard accelerators and command
;; abbreviations which are valid in "All" applications including Annotation.


(keyboard-filing-revision "18.00")

;; ----- Definition of Keyboard Accelerators ----------------------------------
;; A keyboard accelerator for an available command can be defined like this:
;;     A combination of:
;;        - Shift, Ctrl, Alt
;;        - a character or number
;;        - F1 ... F12
;;        - NumPad0 ... NumPad9
;;     separated by a space " "
;;     Examples: "Shift Ctrl A"  or  "Ctrl NumPad5"
;;     Ctrl Alt should never be used as Windows treats this as Alt Gr on German keyboards

(start-keyboard-accelerators)

("F1"  :command "On Context"           :group "Help"     :application "All")
("F2"  :command "Select"               :group "Select")
("F3"  :command "Global Axes On/Off"   :group "View")
("F11" :command "Full Screen"          :group "View")
("F12" :command "Browser Bar")

("CTRL C" :command "Copy"              :group "Edit")
("CTRL V" :command "Paste")
("CTRL Q" :command "Toggle Open Menus" :group "Miscellaneous")
("CTRL Y" :command "Redo One"          :group "Miscellaneous")
("CTRL Z" :command "Undo One"          :group "Miscellaneous")

("SPACE"  :command "Option Mini Toolbar" :group "Miscellaneous")
("SHIFT SPACE" :command "Toggle Command UI" :group "Miscellaneous")
("CTRL BACKSPACE" :command "Activate Last Command" :group "View")

;;----- Definition of Command Abbreviations -----------------------------------
;; A command abbreviation is a sequence of one to N characters to be typed 
;; in to invoke an available command. 
;; NOTE: The [ENTER] key is necessary after input of the character
;;       sequence to invoke the command.

(start-command-abbreviations)

("f" :command "Fit"                    :group "View"     :application "All")

;; All command abbreviations for application "All":

("netviewer" :command "Netviewer starten" :group "Techsoft")
("teamviewer" :command "Teamviewer starten")
("support" :command "Techsoft Support per E-Mail")


