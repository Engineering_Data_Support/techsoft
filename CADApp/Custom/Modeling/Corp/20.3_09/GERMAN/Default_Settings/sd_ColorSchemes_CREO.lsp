;% Automatically written on 05/26/2011 at 08:29:58
;% Creo Elements/Direct Modeling Revision: 18.0 (18.0)

(oli:sd-set-setting-values
  :application "SolidDesigner"
  :style-path "SolidDesigner/ColorSchemes"
  :style :CREO
  :title "Creo"
  :values
   '("3DObjects/2dConstructionColor" 16711935
     "3DObjects/2dGeometryColor" 16777215
     "3DObjects/CurrentPartColor" 65280
     "3DObjects/PartBaseColor" 10066329
     "3DObjects/PartFaceColor" 4243417
     "3DObjects/WPBackBorderColor" 10066367
     "3DObjects/WPBackCurrentColor" 48896
     "3DObjects/WPFrontBorderColor" 13421823
     "3DObjects/WPFrontCurrentColor" 327560
     "Viewport/ClipLineColor" 16777215
     "Viewport/SelectionColor" 15373056
     )
)
