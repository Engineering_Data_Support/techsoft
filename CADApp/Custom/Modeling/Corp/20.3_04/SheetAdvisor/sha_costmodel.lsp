﻿
(in-package  :sheet-advisor)
(use-package :sheet-advisor)

(export 'sha-cost-model)
;; The function which symbol is set with:
;;
;; Sha-set-cost-model-function 'your-cost-model
;;
;; will be called if the Cost Estimation button is pressed within Sheet Metal
;; By default this is sheet-advisor:sha-cost-model.


(defun sha-cost-model (sta filename)

  ;; Initialize local variables by extracting the data from the sta structure

  (let

  (
    (partname     (flat-statistics-sd-partname sta))
    (material     (flat-statistics-material-id sta))
    (thickness    (flat-statistics-thickness sta))
    (bends        (flat-statistics-number-of-bends sta))
    (diff-bends   (flat-statistics-number-of-different-bends sta))
    (hems         (flat-statistics-number-of-hems sta))
    (diff-hems    (flat-statistics-number-of-different-hems sta))
    (flat-width   (flat-statistics-flat-enclosing-box-width sta))
    (flat-height  (flat-statistics-flat-enclosing-box-height sta))
    ;(body-width  (flat-statistics-body-enclosing-box-width sta))
    ;(body-height (flat-statistics-body-enclosing-box-height sta))
    ;(body-depth  (flat-statistics-body-enclosing-box-depth sta))
    (punches      (flat-statistics-number-of-punch-features sta))
    (diff-punches (flat-statistics-number-of-different-punch-features sta))
    (stamps       (flat-statistics-number-of-stamp-features sta))
    (diff-stamps  (flat-statistics-number-of-different-stamp-features sta))
    (nibble-len   (flat-statistics-total-length-of-non-feature-elements sta))
    (contour-punches 
      (flat-statistics-number-of-non-straight-non-feature-elements sta))

    ;; Note that body-width, -height, depth are commented out because
    ;; they are not used in this sample cost model.

    ;; Other local variables required later...

    (x) (y)               ; Size of the raw material table
    (cost)                ; Cost of one raw material table
    (punch-qty-x)         ; Number of flats that fit horizontally on table
    (punch-qty-y)         ; Number of flats that fit vertically on table
    (punch-qty)           ; Number of flats that fit on table
    (punch-qty-rotated)   ; Number of 90deg rotated flats that fit on table
    (punch-runtime)       ; Punching machine time per part
    (punch-setuptime)     ; Punching machine setup time
    (punch-rate)          ; Cost of punching machine per time unit
    (fold-runtime)        ; Folding machine time per part
    (fold-setuptime)      ; Folding machine setup time
    (fold-rate)           ; Cost of folding machine per time unit
    (stream)              ; LISP file handle
    (mat-costs)           ; Material costs depending on lotsize
    (punch-costs)         ; Punch costs depending on lotsize
    (fold-costs)          ; Fold costs depending on lotsize
  )

  
  ;; Set some additional constants since they are currently not in the TDB

  (cond 
   ((sd-string= material "AA 5052" ) (setq x 1000) (setq y 1250) (setq cost 12.5))
   ((sd-string= material "UST 1203") (setq x  750) (setq y 1000) (setq cost 17.5))
   ((setq x  750) (setq y 1000) (setq cost 17.5)))  ;; default cost/size


  ;; Calculate number of flat parts per table (punch-qty)

  (setq punch-qty-x (floor (/ x flat-width)))
  (setq punch-qty-y (floor (/ y flat-height)))
  (setq punch-qty (* punch-qty-x punch-qty-y))

  (setq punch-qty-x (floor (/ x flat-height)))
  (setq punch-qty-y (floor (/ y flat-width)))
  (setq punch-qty-rotated (* punch-qty-x punch-qty-y))

  (setq punch-qty (max punch-qty punch-qty-rotated))


  ;; Calculate punch process times and rates

  (setq punch-runtime 
    (+ (* punches         3.6)           ;; 3.6 seconds per punch
       (* stamps          3.8)           ;; 3.8 seconds per punch
       (* contour-punches 3.6)           ;; non feature, non straight elems
       (* (/ nibble-len   100.0) 4.0)    ;; assume 4s per 100mm nibbling
       18))                              ;; add 18s to take the part
					 ;; out of the machine

  (setq punch-setuptime                  ;; 30min before any
    (+ (* 30 60)                         ;; punching can start
       (* diff-punches 25)               ;; 25s per punch tool setup
       (* diff-stamps 25)))              ;; 25s per stamp tool setup
  
  (setq punch-rate (/ 125 (* 60 60)))    ;; 125 Dollars per punch machine hour
					 ;; divided by 60*60 to get the rate
					 ;; per second


  ;; Calculate fold process times and rates
  ;; (Note: Sheet Metal internally handles offsets as two bends.
  ;; For this reason there is currently no offset count. Each offset
  ;; just counts as two bends)

  (setq fold-runtime
    (+ (* bends 20)          ;; 20 seconds per bend
       (* hems  40)          ;; 40 seconds per hem
       20))                  ;; 20s to take a part out of the machine

  (setq fold-setuptime
    (+ (* diff-bends 180)    ;; 180s per new tool setup
       (* diff-hems  200)))  ;; 200s per new tool setup

  (setq fold-rate (/ 150 (* 60 60))) ;; 150 Dollars per punch machine hour


  ;; Prepare the output to a file, give some header information

  (setf stream (open filename :direction :output :external-format :utf-8-bom))

  (format stream "~%")
  (format stream "~A~%"   "ERGEBNISSE DER KOSTENERMITTLUNG")
  (format stream "~A~%~%" "-----------------------")

  (format stream "~25A~A~%"   "Teil:" partname)
  (format stream "~25A~A~%"   "Material:" material)
  (format stream "~25A~Fmm~%" "Dicke:" thickness)
  (format stream "~25A~D~%"   "Anzahl pro Blech:" punch-qty)
  (format stream "~25A~F~%"   "Kosten pro Blech:" cost)

  (format stream "~%")
  (format stream "~22A~4D   (~D ~A)~%" 
    "Biegungen:" bends diff-bends   
    "unterschiedlich")
  (format stream "~22A~4D   (~D ~A)~%" 
    "180GrdBiegungen:" hems diff-hems    
    "unterschiedlich")
  (format stream "~22A~4D   (~D ~A)~%" 
    "Stanz-/Freisparungen:" punches diff-punches 
    "unterschiedlich")
  (format stream "~22A~4D   (~D ~A)~%" 
    "Prägungen:" stamps diff-stamps  
    "unterschiedlich")
  (format stream "~%")


  (if (< punch-qty 1)

    (format stream "~%~A~%" "Fehler: Abwicklung ist größer als Rohmaterial oder Standard-Paneelgröße.")

    ;; else

    (progn
 
      ;; Calculate costs for different lotsizes and print them to the file

      (format stream "~25<~A~;~> | ~25<~A~;~>~%" 
	"Anzahl der Teile" 
	"Kosten pro Teil")
      (format stream "---------------------------------------------------~%")

      (dolist (lotsize '(1 5 10 50 100 500 1000))

        (setq mat-costs
          (* cost (/ (ceiling (/ lotsize punch-qty )) lotsize)))

        (setq punch-costs
          (* (+ punch-runtime
    	    (/ punch-setuptime lotsize)) punch-rate))

        (setq fold-costs
          (* (+ fold-runtime
	        (/ fold-setuptime lotsize)) fold-rate))

        (format stream " ~24<~6D~;~> | ~,2F~%" 
	  lotsize (+ mat-costs punch-costs fold-costs))
      )
    )
  )

  (format stream "~%~%~A~%" "(Hinweis: Alle Werte basieren auf dem letzten Abwicklungsprozess.)")

  (format stream "~%~%~%")

  (close stream)

  )
)
