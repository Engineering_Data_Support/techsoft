;; Automatically written on Montag, März 19, 2018 at 11:32:29

(oli::sd-fluentui-show-ribbon-tab-in-application "SDHOME" "All")
(oli::sd-fluentui-update-ribbon-tab "SDHOME"
			:annotationRibbon nil
			:show t
			:position 0
			:appendUtilitiesGroup t
			:application "All"
			:title "&Modeling"
)
(oli::sd-fluentui-show-ribbon-tab-in-application "SDSTRUCTURE" "All")
(oli::sd-fluentui-update-ribbon-tab "SDSTRUCTURE"
			:annotationRibbon nil
			:show t
			:position 1
			:appendUtilitiesGroup t
			:application "All"
			:title "&Struktur"
)
(oli::sd-fluentui-set-tab-order-for-app "All" '("SDHOME" "SDSTRUCTURE" "SDANALYSIS" "SDVIEW" "SDAPPLICATION" "SDMODELMANAGER" ))
