; ------------------------------------------------------------------------------
; SVN: $Id$
; TECHSOFT ribbon tab for creo elements/direct modeling 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(oli::sd-fluentui-create-addon-context "TECHSOFT_CUSTOMIZATIONS"
           :color :black)

(oli::sd-fluentui-show-ribbon-tab-in-application "TECHSOFT" "All")
(oli::sd-fluentui-add-ribbon-tab "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:appendUtilitiesGroup nil
			:position 5
			:application "All"
			:title "TECHSOFT"
			:addonContext "TECHSOFT_CUSTOMIZATIONS"
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Support"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "TECHSOFT Support"
			:slot 0
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Teamviewer starten"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Teamviewer starten")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Netviewer starten"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Netviewer starten")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Support per E-Mail"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Support per E-Mail")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Support-Formular"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Support-Formular")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Filecenter"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Filecenter")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Configuration"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "Konfiguration"
			:slot 1
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "SDCORPCUSTOMIZEDIR anzeigen"
			:availCmd '("All" "TECHSOFT" "SDCORPCUSTOMIZEDIR anzeigen")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "SDSITECUSTOMIZEDIR anzeigen"
			:availCmd '("All" "TECHSOFT" "SDSITECUSTOMIZEDIR anzeigen")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "SDUSERCUSTOMIZEDIR anzeigen"
			:availCmd '("All" "TECHSOFT" "SDUSERCUSTOMIZEDIR anzeigen")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "Software Distribution Server"
			:availCmd '("All" "TECHSOFT" "Software Distribution Server")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "License Server auflisten"
			:availCmd '("All" "TECHSOFT" "License Server auflisten")
)
(oli::sd-fluentui-add-ribbon-group "PTC_Online_Resources"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "PTC Online Ressourcen"
			:slot 2
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "PTC_Online_Resources")
			:annotationRibbon nil
			:userDefined t
			:label "Online Dokumentation"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "PTC Online Dokumentation")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "PTC_Online_Resources")
			:annotationRibbon nil
			:userDefined t
			:label "Online Tutorials"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "PTC Online Tutorials")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Design"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "Design"
			:slot 3
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Creo"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Creo Design")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Silber"
			:availCmd '("All" "TECHSOFT" "Silver Design")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Blau"
			:availCmd '("All" "TECHSOFT" "Blue Design")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Schwarz"
			:availCmd '("All" "TECHSOFT" "Black Design")
)
(oli::sd-fluentui-set-tab-order-for-app "All" '("SDAPPLICATION" "SDHOME" "SDSTRUCTURE" "SDANALYSIS" "SDVIEW" "TECHSOFT" ))
