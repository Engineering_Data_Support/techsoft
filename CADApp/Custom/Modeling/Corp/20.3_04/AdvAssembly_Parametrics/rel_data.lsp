;; Automatically written on 08-Aug-2019 15:57:28
;; Module ADVASMB_PARAMETRICS
(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "ADVASMB_PARAMETRICS")
(persistent-data
 :key "SETTINGS-SOLVING-DYN-SETS"
 :value 1)
(persistent-data
 :key "SETTINGS-SOLVING-DYN-TRANSFORM"
 :value 1)
(persistent-data
 :key "SETTINGS-SOLVING-DYN-MOVEMENT"
 :value 1)
(persistent-data
 :key "SETTINGS-SOLVING-DYN-NSTEPS"
 :value 20)

;; ----------- end of file -----------
