;% Automatically written on 03/18/2021 at 15:35:36
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)


(oli:sd-create-browser-filter
  "parcel-gbrowser" :NAME "SolidPower-Material"
  :TITLE "Keinen SolidPower Werkstoff"
  :CASE-SENSITIVE NIL
  :MATCH :ALL
  :CRITERIA '((:COLUMN :IS-PART :OPERATION :EQUALS :VALUE "Yes")
              (:COLUMN :SOLIDPOWER-MATERIAL :OPERATION :EQUALS :VALUE
                       "")))
(oli:sd-create-browser-filter
  "parcel-gbrowser" :NAME "SolidPower-Normteil"
  :TITLE "Kein SolidPower Normteil"
  :CASE-SENSITIVE NIL
  :MATCH :ALL
  :CRITERIA '((:COLUMN :SOLIDPOWER-PART :OPERATION :EQUALS :VALUE
                       "n/a")))