;; Automatically written on 18-Mar-2021 15:35:35
;; Module ANNOTATION
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "ANNOTATION")
(persistent-data
 :key "TEMPLATE EXAMPLES SHOWN"
 :value '( :SURFACE T ) )
(persistent-data
 :key "FRAME TYPE DEFAULT"
 :value "WM-4")
(persistent-data
 :key "DIMENSION FIXTEXTS: EXPANDED"
 :value T)
(persistent-data
 :key "VIEW DEFAULTS"
 :value '( :CREATE-DRAWING-DEFAULT-VIEWS ( :LEFT :TOP :FRONT ) :UPDATE-TYPE 
           :UPDATE_SELECTED_VIEWS ) )
(persistent-data
 :key "STARTUPTIME"
 :value '( 6 24.6274888118108102 20 ) )
(persistent-data
 :key "DIMENSION TOLERANCETEXTS: EXPANDED"
 :value T)
(persistent-data
 :key "VP-CONFIG"
 :value '( :AUX-3D-VP ( 100.0,100.0 500.0,400.0 ) :ANNO-VP-WITH-3D ( 0.0,0.0 
           1240.0,643.0 ) :AUX-3D-VP-VISIBLE NIL :ANNO-VP ( -10.0,-33.0 1286.0,875.0 
           :MAXIMIZE ) ) )
(persistent-data
 :key "HATCH PATTERN EXAMPLES SHOWN"
 :value T)
(persistent-data
 :key "DIM FIX TEXT PROPOSALS"
 :value '( :SUBFIX ( "D" "" ) :SUPERFIX ( "C" "" ) :POSTFIX ( "S" "m7" "m6" "m5" "k8" "k7" 
           "k6" "h11" "h10" "h9" "h8" "h7" "h6" "h5" "h4" "g8" "g7" "g6" "g5" 
           "f8" "f7" "f6" "e8" "e7" "e6" "K8" "K7" "K6" "H13" "H12" "H11" "H10" 
           "H9" "H8" "H7" "H6" "H5" "G7" "G6" "F8" "F7" "F6" "A11" "B" 
           "x45<Degree>" "" ) :PREFIX ( "A" "M" "<Diameter>" "" ) ) )
(persistent-data
 :key "CREATE DRAWING"
 :value '( :UP_DIR 0.0,0.0,1.0 :FR_DIR 0.0,1.0,0.0 ) )
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "Annotation/Dimension/Specific/Thread/FormatStyle" :UNF ) ( 
           "Annotation/Plot/PensTrafoStyle" :TRUE_COLOR ) ( 
           "Annotation/General/ArrowStyle" :STANDARD ) ( 
           "Annotation/General/LineStyle" :SECTION_GEO ) ( 
           "Annotation/View/CaptionStyle" :DETAIL_CONV1600_TECHSOFT ) ( 
           "Annotation/Plot/PrinterStyle" :A0 ) ( "Annotation/View/FormatStyle" 
           :TEILANSICHT ) ( "Annotation/General/TextStyle" :STANDARD ) ( 
           "Annotation/Dimension" :NH ) ( "Annotation/Plot" :A0 ) ( 
           "Annotation/General/SizeStyle" :STANDARD ) ( "Annotation/Plot/PageStyle" 
           :FACTOR1 ) ( "Annotation/Dimension/Text/FormatStyle" :MM ) ( 
           "Annotation/Refline" :NH ) ( "Annotation" :NH ) ) )

;; ----------- end of file -----------
