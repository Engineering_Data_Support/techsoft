;% Automatically written on 03/17/2021 at 17:55:24
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/View/FormatStyle"
  :style :TEILANSICHT
  :title "Teilansicht"
  :values
   '("FormatBrowser" "<NameNummer>_von_<ÜbergeordnetNameNummer>"
     "FormatLabel" "<Name> von <ÜbergeordnetNameNummer>"
     "InternalFormatBrowser" "<:PREFIX:><:NUMBER:>_von_<:PARENT-PREFIX-NUMBER:>"
     "InternalFormatLabel" "<:PREFIX:> von <:PARENT-PREFIX-NUMBER:>"
     )
)
