;% Automatically written on 03/18/2021 at 15:35:36
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/View/CaptionStyle"
  :style :DETAIL_CONV1600_TECHSOFT
  :title "Detail"
  :values
   '("Characters" "Z Y X W V U T S R Q P O N M L K J I H G F E D C B A"
     "Numbering" :LETTERSONLY
     )
)
