;% Automatically written on 03/18/2021 at 15:35:36
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation"
  :style :NH
  :title "nh"
  :values
   '("AuxGeo/Centerline" :AUXILLIARY
     "AuxGeo/CenterlineAbsOffset" 0.0
     "AuxGeo/CenterlineRelOffset" 0.10000000000000002
     "AuxGeo/SymmetryAbsOffset" 0.0
     "AuxGeo/Symmetryline" :AUXILLIARY
     "Bom/BomMenuEnable" :ON
     "Bom/BomScan3DEnable" :ON
     "Bom/Highlight" :ON
     "Bom/HighlightColors/Current" 65535
     "Bom/HighlightColors/Enabled" LISP::T
     "Bom/HighlightColors/Multi" 16711680
     "Bom/HighlightColors/Todo" 65280
     "Bom/ScanContainer" :OFF
     "Bom/ScanInseparable" :ON
     "Copilot/Appearance/Constraint/Color" 16776960
     "Copilot/Appearance/Constraint/Visible" :ON
     "Copilot/Appearance/FlyBy/DragColor" 14935034
     "Copilot/Appearance/FlyBy/HandleColor" 12073871
     "Copilot/Appearance/FlyBy/SelectColor" 16753920
     "Copilot/Appearance/Handle/FillColor" 255
     "Copilot/Appearance/Handle/PenColor" 65535
     "Copilot/Appearance/Handle/Size" 3
     "Copilot/Appearance/InfoBox/BGColor" 16777215
     "Copilot/Appearance/InfoBox/Border" :ON
     "Copilot/Appearance/InfoBox/Color" 0
     "Copilot/Appearance/InfoBox/Prefix" :ON
     "Copilot/Appearance/SnapLine/Color" 65280
     "Copilot/Appearance/SnapLine/Ltype" :CENTER_DASH_DASH
     "Copilot/Appearance/SnapPoint/Color" 65280
     "Copilot/Appearance/SnapPoint/Count" 5
     "Copilot/Appearance/SnapPoint/Delay" 0.29999999999999999
     "Copilot/Grid/AngleList" (:DEG 0.10000000000000002 0.20000000000000001
                               0.5 1 2 5 10 15 30 45 60 90)
     "Copilot/Grid/DefaultAngle" 15
     "Copilot/Grid/DefaultDistance" 1.0
     "Copilot/Grid/DistanceList" (:MM 1.0E-4 2.0000000000000001E-4
                                 5.0000000000000001E-4 0.001 0.002
                                 0.0050000000000000001 0.01 0.02
                                 0.050000000000000003
                                 0.10000000000000002
                                 0.20000000000000001 0.5 1.0 2.0 5.0
                                 10.0 20.0 50.0 100.0 200.0 500.0
                                 1000.0 2000.0 5000.0 10000.0 20000.0
                                 50000.0)
     "Dimension/Arrows/FirstArrowSize" :ABS
     "Dimension/Arrows/FirstArrowStyle" :STANDARD
     "Dimension/Arrows/Placement" :ARROW_AUTO
     "Dimension/Arrows/SecondArrowSize" :ABS
     "Dimension/Arrows/SecondArrowStyle" :STANDARD
     "Dimension/AutoPlace/DatumStep" 8.0
     "Dimension/AutoPlace/MinimalSpace" 0.0
     "Dimension/AutoPlace/Mode" :AUTO
     "Dimension/AutoPlace/Position" :BELOW
     "Dimension/AutoPlace/Strategy" :GEO
     "Dimension/AutoPlace/SwitchDirection" :ALL
     "Dimension/CatchCheck/BoxSelect" :ON
     "Dimension/CatchCheck/CatchLines" LISP::T
     "Dimension/CatchCheck/CatchRange" 5.0
     "Dimension/CatchCheck/CursorPosition" :LINE_POS
     "Dimension/CatchCheck/InsertCatchCenter" LISP::NIL
     "Dimension/CatchCheck/InsertCheck" LISP::NIL
     "Dimension/CatchCheck/OldAlignment" LISP::NIL
     "Dimension/CatchCheck/RemoveCheck" LISP::NIL
     "Dimension/Lines/Broken" LISP::NIL
     "Dimension/Lines/ExtensionLineVisibility" LISP::T
     "Dimension/Lines/LineStyle" :REFERENCE
     "Dimension/Lines/OffsetLine" 2.0
     "Dimension/Lines/OffsetPoint" 0.0
     "Dimension/Lines/RestoreBreaks" LISP::T
     "Dimension/Lines/RestoreStaggers" LISP::T
     "Dimension/Specific/Chamfer/Postfix" "x45<Degree>"
     "Dimension/Specific/Chamfer/Prefix" ""
     "Dimension/Specific/Coordinate/FitArrow" LISP::NIL
     "Dimension/Specific/Coordinate/LeadingZero" LISP::T
     "Dimension/Specific/Coordinate/Standard" :DIN
     "Dimension/Specific/Coordinate/UseOrientation" LISP::NIL
     "Dimension/Specific/Diameter/BothDiaArrows" LISP::NIL
     "Dimension/Specific/Diameter/CenterLineVisibility" LISP::T
     "Dimension/Specific/Diameter/Postfix" ""
     "Dimension/Specific/Diameter/Prefix" " <Diameter>"
     "Dimension/Specific/Diameter/Standard" :DIN
     "Dimension/Specific/Radius/CenterLineVisibility" LISP::NIL
     "Dimension/Specific/Radius/Postfix" ""
     "Dimension/Specific/Radius/Prefix" " <BASIC>R"
     "Dimension/Specific/Thread/Format" :M
     "Dimension/Specific/Thread/FormatStyle/Format" "<Gewindeprofil><Nominal-Durchmesser> x <Ganghöhe> X <Länge>"
     "Dimension/Specific/Thread/LabelLeftHand" "links"
     "Dimension/Specific/Thread/LabelRightHand" ""
     "Dimension/Specific/Thread/Postfix" ""
     "Dimension/Specific/Thread/Prefix" " <BASIC>M"
     "Dimension/Text/Common/AddAutoFixes" LISP::T
     "Dimension/Text/Common/Bracket" :OFF
     "Dimension/Text/Common/FrameColor" :TEXT_COLOR
     "Dimension/Text/Common/FrameType" :TEXT_STYLE
     "Dimension/Text/Common/Gap" 2.0
     "Dimension/Text/Common/KeepLastFixes" LISP::NIL
     "Dimension/Text/Common/KeepLastTolerance" LISP::NIL
     "Dimension/Text/Common/Location" :ABOVE
     "Dimension/Text/Common/Orientation" :PARALLEL
     "Dimension/Text/Common/Space" 2.0
     "Dimension/Text/Common/ToleranceAlignment" LISP::NIL
     "Dimension/Text/Common/UnderlineEdited" LISP::T
     "Dimension/Text/FormatStyle/AddSign" LISP::NIL
     "Dimension/Text/FormatStyle/DecPlaces" 2
     "Dimension/Text/FormatStyle/DecimalPunctuation" :POINT
     "Dimension/Text/FormatStyle/InchFract" :|64|
     "Dimension/Text/FormatStyle/LeftZeroSupression" LISP::NIL
     "Dimension/Text/FormatStyle/RightZeroSupression" LISP::T
     "Dimension/Text/FormatStyle/Unit" :MM
     "Dimension/Text/MainTolerance/SizeMode" :REL06
     "Dimension/Text/MainTolerance/TextStyle" :SMALL
     "Dimension/Text/MainValue/AngleUnitFormat" :DEGREE
     "Dimension/Text/MainValue/LengthUnitFormat" :MM
     "Dimension/Text/MainValue/TextStyle" :STANDARD
     "Dimension/Text/Postfix/SizeMode" :REL10
     "Dimension/Text/Postfix/TextStyle" :STANDARD
     "Dimension/Text/Prefix/SizeMode" :REL10
     "Dimension/Text/Prefix/TextStyle" :STANDARD
     "Dimension/Text/SecondTolerance/SizeMode" :REL06
     "Dimension/Text/SecondTolerance/TextStyle" :SMALL
     "Dimension/Text/SecondValue/AngleUnitFormat" :NONE
     "Dimension/Text/SecondValue/LengthUnitFormat" :NONE
     "Dimension/Text/SecondValue/SizeMode" :REL10
     "Dimension/Text/SecondValue/TextStyle" :STANDARD
     "Dimension/Text/Subfix/SizeMode" :REL10
     "Dimension/Text/Subfix/TextStyle" :STANDARD
     "Dimension/Text/Superfix/SizeMode" :REL10
     "Dimension/Text/Superfix/TextStyle" :STANDARD
     "Drawing/Number" (:SELECTED_OWNER :CURR_PART :LAST_OWNER :CURR_ASSEMBLY
                     :TOP_PART_ASSEMBLY :SELECTED_OWNER_MODEL_NAME)
     "Drawing/OwnerStrategy" (:LAST_OWNER :CURR_ASSEMBLY :TOP_PART_ASSEMBLY
                            :CURR_PART)
     "Drawing/ProjectionMode" :FIRST_ANGLE
     "Drawing/Proposals/Angle" (:DEG 0 45 90 135 180 225 270 315 360)
     "Drawing/Proposals/DotsPerInch" (5 10 25 50 75 150 300 600 1200)
     "Drawing/Proposals/Factor" (0.25 0.33333333333000004
                            0.40000000000000002 0.5 0.66666666666000007
                            0.80000000000000004 1 1.25 1.5 2 2.5 3 4)
     "Drawing/Proposals/LineWidth" (:MM 0.0 0.25 0.34999999999999998 0.5
                                   0.69999999999999996 1.0
                                   1.3999999999999999)
     "Drawing/Proposals/Scale" (0.001 0.002 0.0050000000000000001 0.01 0.02
                           0.050000000000000003 0.10000000000000002
                           0.20000000000000001 0.5 1 2 5 10 20 50)
     "Drawing/Scale" "1"
     "Drawing/UseExistingViewSet" LISP::T
     "Drawing/ViewSet" (:DRAWING-NUMBER-DOT-SHEET :DRAWING-NUMBER
                      :OWNER-NAME :DEFAULT)
     "Filing/SaveDrawingCompressed" LISP::T
     "Filing/WarningSave" :WARNING
     "Filing/WarningSave2D3D" :OFF
     "General/ArrowStyle/Filled" LISP::T
     "General/ArrowStyle/Size" 3.5
     "General/ArrowStyle/Type" :ARROW_TYPE
     "General/LineStyle/Color" 16777215
     "General/LineStyle/Ltype" :SOLID
     "General/LineStyle/PenScaled" LISP::NIL
     "General/LineStyle/Pensize" 0.0
     "General/SizeStyle/RelFactor" 1.0
     "General/SizeStyle/Relative" LISP::NIL
     "General/TextStyle/AbsAngle" 0
     "General/TextStyle/Adjust" 1
     "General/TextStyle/Color" 16711680
     "General/TextStyle/Filled" LISP::T
     "General/TextStyle/Font" "osd_default"
     "General/TextStyle/Frame" :OFF
     "General/TextStyle/LineSpace" 2.2000000000000002
     "General/TextStyle/Ratio" 1
     "General/TextStyle/Size" 3.5
     "General/TextStyle/Slant" 0
     "Geometry/Construction" :CONSTRUCTION
     "Geometry/Standard" :STANDARD
     "Hatch/Appearance" :TECHSOFT
     "Hatch/Other/AngleOffset" :BY_VIEW
     "Hatch/Other/AngleVariants" 9
     "Hatch/Other/DistanceFactor" 1.3
     "Hatch/Other/DistanceScale" :BY_VIEW
     "Hatch/Other/NewHatches" :COPY_VARIATE
     "Hatch/Other/Synchronization" :SHARED
     "Hatch/PatternStyle/Description" "Schraffurmuster"
     "Hatch/PatternStyle/PatternAngle" 0.78539816339744828
     "Hatch/PatternStyle/PatternDistance" 5.0
     "Hatch/PatternStyle/SubPattern" (:LABEL "Standard" :SUBPATTERN
                                        ((:COLOR
                                          0.62352941176470589,0.62352941176470589,0.62352941176470589
                                          :LINETYPE :SOLID :DISTANCE 1
                                          :OFFSET 0 :ANGLE 0 :WIDTH 0)))
     "Hatch/Proposals/PatternDistance" (:MM 1 2 5 10 15)
     "Hatch/Proposals/SubPatternDistance" (0.25 0.33333333333000004 0.5 1 2
                                      3 4)
     "Hatch/Proposals/SubPatternOffset" (0 0.25 0.33333333333000004 0.5
                                    0.66666666666000007 0.75)
     "Misc/DefaultStyleBehaviour/Dimension" :BROWSER
     "Misc/DefaultStyleBehaviour/Plot" :BROWSER
     "Misc/Warning/ObsoleteFunctionCall14" :CONSOLE
     "Plot/PagePosition" :FIT
     "Plot/PageStyle/Fit" LISP::T
     "Plot/PageStyle/OffsetX" 0.0
     "Plot/PageStyle/OffsetY" 0.0
     "Plot/PageStyle/Position" :CENTER
     "Plot/PageStyle/Rotation" 0.0
     "Plot/PageStyle/Scale" 1.0
     "Plot/PatternLength" :STANDARD
     "Plot/PatternLengthStyle/CenterDashDash" 5.0
     "Plot/PatternLengthStyle/DashCenter" 5.0
     "Plot/PatternLengthStyle/Dashed" 5.0
     "Plot/PatternLengthStyle/DotCenter" 5.0
     "Plot/PatternLengthStyle/Dotted" 5.0
     "Plot/PatternLengthStyle/LongDashed" 5.0
     "Plot/PatternLengthStyle/Phantom" 5.0
     "Plot/PenTrafo" :TS_SW_DUENN
     "Plot/PensTrafoStyle/Contents" :DRAWING
     "Plot/PensTrafoStyle/Description" "Stift-Transformation"
     "Plot/PensTrafoStyle/InclLineSize" LISP::T
     "Plot/PensTrafoStyle/Pens" (:PENLIST
                               ((:OLD_LINETYPE :ALL :COLOR_DEF :ALL
                                    :COLOR1
                                    0.0,0.0,0.0 :COLOR2
                                    1.0,1.0,1.0 :PENSIZE_DEF :ALL
                                    :PENSIZE1 0 :PENSIZE2 1000000.0
                                    :NEW_LINETYPE :SAME :NEW_PENSIZE
                                    :OFF :PEN_NUMBER 1 :NEW_COLOR
                                    :SAME)))
     "Plot/PensTrafoStyle/TrueColorMode" LISP::NIL
     "Plot/Printer" :PDF
     "Plot/PrinterStyle/Destination" "C:/Users/OSCHM/AppData/Local/Temp/plot.pdf"
     "Plot/PrinterStyle/Name" :PDF_GENERIC
     "Plot/PrinterStyle/NumberCopies" 1
     "Plot/PrinterStyle/Orientation" :LANDSCAPE
     "Plot/PrinterStyle/PaperSize" :A4_______297_MM_X_210_MM___
     "Plot/PrinterStyle/Properties" (:LABEL "720" :TYPE :PDF :VALUE 720)
     "Plot/PrinterStyle/ToFile" LISP::T
     "Plot/PrinterStyle/Type" :PDF
     "Plot/Source" :DRAWING
     "Refline/ArrowStyle" :STANDARD
     "Refline/DestinationGap" 0.0
     "Refline/LineStyle" :REFERENCE
     "Refline/SourceGap" 0.0
     "Symbol/Appearance/AbsAngle" LISP::NIL
     "Symbol/Appearance/Adjust" LISP::NIL
     "Symbol/Appearance/AutoAngle" LISP::NIL
     "Symbol/Appearance/ColorAll" LISP::NIL
     "Symbol/Appearance/ColorGeometry" LISP::NIL
     "Symbol/Appearance/ColorHatch" LISP::NIL
     "Symbol/Appearance/ColorText" LISP::NIL
     "Symbol/Appearance/SizeAbsolute" LISP::NIL
     "Symbol/Appearance/SizeRelative" LISP::NIL
     "Symbol/Appearance/TextRatio" LISP::NIL
     "Symbol/Appearance/TextReadable" LISP::NIL
     "Symbol/Appearance/TextSlant" LISP::NIL
     "Symbol/Proposals/Angle" (:DEG 0 45 90 135 180 225 270 315 360)
     "Symbol/Proposals/Size" (:MM 2.5 3.5 5 7 10 14 20)
     "Symbol/Proposals/SizeRelative" (0.25 0.34999999999999998 0.5
                                 0.69999999999999996 1
                                 1.4299999999999999 2.0
                                 2.8599999999999999 4.0)
     "Symbol/Specific/Surface/Standard" :ALL
     "Symbol/Specific/Tolerance/DatumCharacters" "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
     "Symbol/Specific/Tolerance/ProjTolCharacters" "0 1 2 3 4 5 6 7 8 9 . ,"
     "Symbol/Specific/Tolerance/ValueCharacters" "0 1 2 3 4 5 6 7 8 9 . , > / + - X x C Z"
     "Symbol/Specific/Welding/Sizes/AllaroundCircle" 1.25
     "Symbol/Specific/Welding/Sizes/IndicationHeight" 6
     "Symbol/Specific/Welding/Sizes/IndicationWidth" 5
     "Symbol/Specific/Welding/Sizes/TextGeoRatio" 0.69999999999999996
     "Symbol/SymbolStyle" :STANDARD
     "Text/Appearance" :STANDARD
     "Text/Editor" :BUILD_IN
     "Text/Feedback" "TF_TEXT"
     "Text/Proposals/Angle" (:DEG 0 45 90 135 180 225 270 315 360)
     "Text/Proposals/Size" (:MM 2.5 3.5 5 7 10 14 20)
     "Text/Proposals/Text" LISP::NIL
     "Transfer3DAnnos/Behaviour/CreateRefGeo" :OFF
     "Transfer3DAnnos/Behaviour/DefaultTransferType" :VIEW
     "Transfer3DAnnos/Behaviour/TransferAppearance" :OFF
     "Transfer3DAnnos/Behaviour/TransferContent" :OFF
     "Transfer3DAnnos/Behaviour/TransferDuringUpdate" :ON
     "Transfer3DAnnos/Behaviour/TransferPosition" :OFF
     "UnHide/Colors/Background" 8421504
     "UnHide/Colors/Calculated" 16760576
     "UnHide/Colors/Manual" 49151
     "View/Appearance/HiddenLines/LineStyle" :HIDDEN
     "View/Appearance/HiddenLines/Mode" :HIDDEN_OFF
     "View/Appearance/NormalLines/LineStyle" :VISIBLE
     "View/Appearance/NormalLines/PartColor" LISP::NIL
     "View/Appearance/TangentLines/LineStyle" :TANGENT
     "View/Appearance/TangentLines/Mode" LISP::T
     "View/Appearance/ThreadLines/ArcBegin" 3.2288591161895095
     "View/Appearance/ThreadLines/ArcEnd" 1.6580627893946129
     "View/Appearance/ThreadLines/LinesHidden" :HIDDEN
     "View/Appearance/ThreadLines/LinesNormal" :VISIBLE-THREAD
     "View/Broken/Appearance/BorderEdge" :BROKEN
     "View/Broken/Appearance/Pattern/Gap" 15.0
     "View/Broken/Appearance/Pattern/Height" 5.0
     "View/Broken/Appearance/Pattern/Symmetry" :YES
     "View/Broken/Appearance/Pattern/Type" :ZIGZAG
     "View/Broken/Appearance/Pattern/Width" 5.0
     "View/Broken/Other/CleanUpAnnotation" :YES
     "View/CaptionStyle/Case" :ASIS
     "View/CaptionStyle/Characters" "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
     "View/CaptionStyle/Numbering" :NUMBERSONLY
     "View/CaptionStyle/Offset" 5.0
     "View/CaptionStyle/Position" :BELOW
     "View/Cutaway/Appearance/BorderEdge/BorderType" :SPLINE
     "View/Cutaway/Appearance/BorderEdge/LineStyle" :BROKEN
     "View/Cutaway/Appearance/BorderEdge/Visible" :NO
     "View/Cutaway/Appearance/ViewLabel/CaptionStyle" :CUTAWAY_CONV1600_TECHSOFT
     "View/Cutaway/Appearance/ViewLabel/Case" :ASIS
     "View/Cutaway/Appearance/ViewLabel/Characters" "A B C D E F G H I"
     "View/Cutaway/Appearance/ViewLabel/Format" :NO_LABEL
     "View/Cutaway/Appearance/ViewLabel/LabelStyle" :STANDARD
     "View/Cutaway/Appearance/ViewLabel/Numbering" :NUMBERSONLY
     "View/Cutaway/Appearance/ViewLabel/Prefix" "Ausbruch"
     "View/Cutaway/Appearance/ViewLabel/TextType" "Ausbruch"
     "View/Cutaway/Other/ModeDependantViews" :NO
     "View/Cutaway/Other/ShowCatchToolbar" :NO
     "View/Detail/Appearance/Border" :BROKEN
     "View/Detail/Appearance/BorderEdge" :BROKEN
     "View/Detail/Appearance/BorderType" :CIRCLE
     "View/Detail/Appearance/DetailLabel/Format" :DET_REF
     "View/Detail/Appearance/DetailLabel/LabelStyle" :STANDARD
     "View/Detail/Appearance/DetailLabel/Offset" 5.0
     "View/Detail/Appearance/DetailLabel/Prefix" "siehe"
     "View/Detail/Appearance/ViewLabel/CaptionStyle" :DETAIL_CONV1600_TECHSOFT
     "View/Detail/Appearance/ViewLabel/Case" :UPPER
     "View/Detail/Appearance/ViewLabel/Characters" "Z Y X W V U T S"
     "View/Detail/Appearance/ViewLabel/Format" :NUMBER_ONLY
     "View/Detail/Appearance/ViewLabel/InclScale" :YES
     "View/Detail/Appearance/ViewLabel/LabelStyle" :MEDIUM
     "View/Detail/Appearance/ViewLabel/Numbering" :NUMBERSONLY
     "View/Detail/Appearance/ViewLabel/Offset" 5.0
     "View/Detail/Appearance/ViewLabel/Position" :BELOW
     "View/Detail/Appearance/ViewLabel/TextDetail" "Detail"
     "View/Detail/Appearance/ViewLabel/TextType" "Detail"
     "View/Detail/Other/AddSymCenLines" :ON
     "View/Detail/Other/ElementsKeepDuringCreation" LISP::NIL
     "View/Detail/Other/PreserveViewCenter" :ON
     "View/Flat/Appearance/ViewLabel/CaptionStyle" :STANDARD
     "View/Flat/Appearance/ViewLabel/Case" :ASIS
     "View/Flat/Appearance/ViewLabel/Characters" "J K L M N O P Q R"
     "View/Flat/Appearance/ViewLabel/Format" :NUR_NAME
     "View/Flat/Appearance/ViewLabel/Numbering" :NUMBERSONLY
     "View/Flat/Appearance/ViewLabel/TextFlat" "Abwicklung"
     "View/Flat/Appearance/ViewLabel/TextType" "Abwicklung"
     "View/Flat/Other/PlacementCenter" LISP::T
     "View/FormatStyle/FormatBrowser" "default Format browser"
     "View/FormatStyle/FormatLabel" "default Format label"
     "View/FormatStyle/InternalFormatBrowser" "default Format browser"
     "View/FormatStyle/InternalFormatLabel" "default Format label"
     "View/Isometric/Appearance/IsoMode" :ISO_T
     "View/Isometric/Appearance/IsoModeStyle/RotationX" LISP::NIL
     "View/Isometric/Appearance/IsoModeStyle/RotationY" LISP::NIL
     "View/Isometric/Other/AddSymCenLines" :ON
     "View/Lock" :OFF
     "View/Other/CopiedPartsComponentBehaviour" :ALWAYS
     "View/Other/FilterPartsComponentBehaviour" :FILTER-TEMPORARY
     "View/Other/NewPartsComponentBehaviour" :ALWAYS
     "View/Other/PartsInDependantViewsBehaviour" :OFF
     "View/Other/WarningViewNeedUpdate" :ON
     "View/Partial/Appearance/BorderEdge" :BROKEN
     "View/Partial/Appearance/BorderType" :CIRCLE
     "View/Partial/Appearance/ViewLabel/CaptionStyle" :PARTIAL_CONV1600_TECHSOFT
     "View/Partial/Appearance/ViewLabel/Case" :ASIS
     "View/Partial/Appearance/ViewLabel/Characters" "Z Y X W V U T S"
     "View/Partial/Appearance/ViewLabel/Format" :TEILANSICHT
     "View/Partial/Appearance/ViewLabel/InclScale" :AUTOMATIC
     "View/Partial/Appearance/ViewLabel/LabelStyle" :STANDARD
     "View/Partial/Appearance/ViewLabel/Numbering" :NUMBERSONLY
     "View/Partial/Appearance/ViewLabel/TextPartial" "Teilansicht"
     "View/Partial/Appearance/ViewLabel/TextType" "Teilansicht"
     "View/Partial/Other/ElementsKeepDuringCreation" LISP::NIL
     "View/Partial/Other/ParNameOfDocuplane" :NO
     "View/Section/Appearance/InnerEdges" :BROKEN
     "View/Section/Appearance/Line/Geo" :SECTION_GEO
     "View/Section/Appearance/Line/Leader/ArrowStyle" :|PFEIL_GROß|
     "View/Section/Appearance/Line/Leader/Length" 15.0
     "View/Section/Appearance/Line/Leader/LineStyle" :SECTION_SEGM
     "View/Section/Appearance/Line/Leader/Mode" :LEADER_TO
     "View/Section/Appearance/Line/Segment/Length" 3.0
     "View/Section/Appearance/Line/Segment/LineStyle" :SECTION_SEGM
     "View/Section/Appearance/Line/Segment/Mode" :SEGM_OVERLAP
     "View/Section/Appearance/SectionLabel/Characters" "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
     "View/Section/Appearance/SectionLabel/Format" :SEC_OLD
     "View/Section/Appearance/SectionLabel/LabelStyle" :LARGE
     "View/Section/Appearance/SectionLabel/Numbering" :LETTERANDNUMBERS
     "View/Section/Appearance/SectionLabel/Offset" 7.0
     "View/Section/Appearance/SectionLabel/Position" :TOP_SIDE
     "View/Section/Appearance/ViewLabel/CaptionStyle" :SECTION_CONV1600_TECHSOFT
     "View/Section/Appearance/ViewLabel/Case" :ASIS
     "View/Section/Appearance/ViewLabel/Format" :SEC_OLD
     "View/Section/Appearance/ViewLabel/InclScale" :AUTOMATIC
     "View/Section/Appearance/ViewLabel/LabelStyle" :STANDARD
     "View/Section/Appearance/ViewLabel/Offset" 5.0
     "View/Section/Appearance/ViewLabel/Position" :BELOW
     "View/Section/Appearance/ViewLabel/Prefix" "Schnitt"
     "View/Section/Appearance/ViewLabel/TextType" "Schnitt"
     "View/Section/Other/AddSymCenLines" :ON
     "View/Section/Other/ModeDependantViews" :YES
     "View/Section/Other/PreserveViewCenter" :OFF
     "View/Section/TempAuxLines/Color" 16711680
     "View/Section/TempAuxLines/Mode" :ON
     "View/Section/TempAuxLines/ModeArc" :CIRC
     "View/Section/TempAuxLines/ViewAngle" :OFF
     "View/Standard/Appearance/ViewLabel/CaptionStyle" :STANDARD
     "View/Standard/Appearance/ViewLabel/Case" :ASIS
     "View/Standard/Appearance/ViewLabel/Format" :NO_LABEL
     "View/Standard/Appearance/ViewLabel/FormatDependent" :DEP
     "View/Standard/Appearance/ViewLabel/LabelStyle" :STANDARD
     "View/Standard/Appearance/ViewLabel/Numbering" :NUMBERSONLY
     "View/Standard/Appearance/ViewLabel/Offset" 5.0
     "View/Standard/Appearance/ViewLabel/Position" :BELOW
     "View/Standard/Appearance/ViewName/InclName" :YES
     "View/Standard/Appearance/ViewName/TextBack" "Rückansicht"
     "View/Standard/Appearance/ViewName/TextBottom" "Untsicht"
     "View/Standard/Appearance/ViewName/TextFront" "Vordans"
     "View/Standard/Appearance/ViewName/TextGeneral" "Allg"
     "View/Standard/Appearance/ViewName/TextIsometric" "ISO"
     "View/Standard/Appearance/ViewName/TextLeft" "SA Links"
     "View/Standard/Appearance/ViewName/TextRight" "SA Rechts"
     "View/Standard/Appearance/ViewName/TextTop" "Drfsicht"
     "View/Standard/Appearance/ViewName/TextTypeGen" "Allg"
     "View/Standard/Appearance/ViewName/TextTypeIso" "ISO"
     "View/Standard/Appearance/ViewName/TextTypeStd" "Std"
     "View/Standard/Appearance/ViewName/Unique" :IN_DRAWING
     "View/Standard/Appearance/ViewScale/Case" :ASIS
     "View/Standard/Appearance/ViewScale/InclScale" :AUTOMATIC
     "View/Standard/Appearance/ViewScale/ScaleBrackets" :NONE
     "View/Standard/Appearance/ViewScale/ScalePrefix" "M"
     "View/Standard/Appearance/ViewScale/ScaleSyntax" :COLON_SYNTAX
     "View/Standard/Other/AddSymCenLines" :ON
     "View/Update/AutoUpdate/ApplicationSwitch" LISP::NIL
     "View/Update/AutoUpdate/FastUpdate" :UPDATE
     "View/Update/AutoUpdate/SlowUpdate" :ALERT
     "View/Update/AutoUpdate/Threshold" 5.0
     "View/Update/CalculationShaded/FileFormat" :PNG
     "View/Update/CalculationShaded/Lights" :AM_DEFAULT
     "View/Update/CalculationShaded/MaxMBytes" 12
     "View/Update/CalculationShaded/RenderMode" LISP::NIL
     "View/Update/CalculationShaded/RenderToneMapping" :NONE
     "View/Update/CalculationShaded/Resolution" 300
     "View/Update/CalculationShaded/UseRenderToFile" LISP::T
     "View/Update/Check" :ON
     "View/Update/ClashHandling/AutoRecognition" :OFF
     "View/Update/ClashHandling/Pressfit" :ON
     "View/Update/Color/Equal_3d" 16711935
     "View/Update/Color/New_3d" 65535
     "View/Update/Color/Recre_2d" 16711680
     "View/Update/Color/Transf_2d" 65280
     "View/Update/Color/Upd_2d" 255
     "View/Update/Color/Upd_3d" 16776960
     "View/Update/CreateCenterlines" LISP::T
     "View/Update/CreateSymmetryLines" LISP::T
     "View/Update/ElementsRecreated" :ON
     "View/Update/ErrorViewUpdate" :DISPLAY-ALERT
     "View/Update/FarBasePointDimension" :ON
     "View/Update/SecuredPartsBehaviour" :ON_REMOVE
     "View/Update/UpdateMode/CalculationGraphicResolution" :MEDIUM
     "View/Update/UpdateMode/CalculationMode" :CLASSIC
     "View/Update/UpdateMode/CoordSystemsInView" :CALCULATE
     "View/Update/UpdateMode/FacePartsInView" :CALCULATE
     "View/Update/UpdateMode/SolidPartsInView" :CALCULATE
     "View/Update/UpdateMode/WirePartsInView" :IGNORE
     "View/Update/UpdateMode/WorkplanesInView" :IGNORE
     "Viewport/Appearance/BackgroundColor" 1644895
     "Viewport/Appearance/HighlightColor" 16776960
     "Viewport/Appearance/HighlightLtype" :PHANTOM
     "Viewport/Appearance/Inverse" :INVERSE
     "Viewport/Appearance/ShowPensize" LISP::NIL
     "Viewport/Grid/Color" 5066061
     "Viewport/Grid/Distance" 10
     "Viewport/Grid/Ltype" :DOTTED
     "Viewport/Grid/Usage" :OFF
     "Viewport/Redraw/Dynamic/Arcs" :OFF
     "Viewport/Redraw/Dynamic/Circles" :ON
     "Viewport/Redraw/Dynamic/Dimensions" :OFF
     "Viewport/Redraw/Dynamic/Hatches" :OFF
     "Viewport/Redraw/Dynamic/Lines" :ON
     "Viewport/Redraw/Dynamic/Pictures" :OFF
     "Viewport/Redraw/Dynamic/Splines" :ON
     "Viewport/Redraw/Dynamic/Texts" :OFF
     "Viewport/Redraw/PartsAsBox/Color" 8421504
     "Viewport/Redraw/PartsAsBox/Size" 16
     )
)
