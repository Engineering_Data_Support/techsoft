;; Automatically written on 18-Mar-2021 15:35:35
;; Module ALL
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "ALL")
(persistent-data
 :key "STARTUP MODULES"
 :value '( "IGESDEK" "STEP" "GRANITE" "ProductView" "ACIS_SAT" "STL" 
           "CADCAM_LINK" "BASICSHEETS" "PART_LIBRARY" "SDPOWER-STEEL" ) )
(persistent-data
 :key "TS-CATALOG-MAIN-DS"
 :value '( :SIZE ( :WIDTH 730 :HEIGHT 650 ) ) )
(persistent-data
 :key "BUTTON-FACE-COLOR"
 :value 15790320)
(persistent-data
 :key "AUTO-SAVE"
 :value '( :FILENAME 
           "C:\\Users\\jhaa\\AppData\\Local\\Temp/modeling_autosave_jhaa" :ON T 
           :TIME-INTERVAL 30 :OUT-OF-MEM NIL :BACKUP-COUNT 10 :CONFIRM NIL ) )
(persistent-data
 :key "TDB_BROWSER"
 :value '( :TABLE-SEC-ICONS NIL :TREE-SEC-ICONS NIL ) )
(persistent-data
 :key "OUTPUT-BOX-DS"
 :value '( :SIZE ( :WIDTH 946 :HEIGHT 304 ) ) )
(persistent-data
 :key "GEO-RESOLUTION"
 :value '( :GEO-RESOLUTION 0.000001 :FINE-RESOLUTION-VALUES NIL ) )
(persistent-data
 :key "AM-BROWSER"
 :value '( :VISIBLE NIL :TABLE-SEC-ICONS NIL ) )
(persistent-data
 :key "IGNORE-CMD-ICONS"
 :value NIL)
(persistent-data
 :key "CLASH-ANALYSIS-CONFIGURATIONS-BROWSER"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "FILE-DIALOG"
 :value '( :VIEW-MODE 1 ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-REDRAW"
 :value '( :DISPLAY-LISTS NIL :OCCLUSION-CULLING NIL :PHONG-SHADING NIL 
           :SMOOTH-UPDATE T :REDRAW-TIMEOUT 2000 :SIMPLIFY-MODEL-FACTOR 40 
           :SIMPLIFY-MODEL :BOXED :SMOOTH-UPDATE-TIME 500 ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-DYNAMIC-VIEWING"
 :value '( :CTRL-LESS-THRESHOLD 4 :SIMPLIFY-MODEL :BOXED :CAE :CAE-DEFAULT 
           :WORKPLANE :WP-DEFAULT :CLIPPING :CLIP-NO-LINES :SCENERY :ALL 
           :MOUSE-INTERACTION-MODE :ON :REDRAW-TIMEOUT 0 :SIMPLIFY-MODEL-FACTOR 
           70 :LABEL :L3D-DEFAULT :3D-OBJECT :DEFAULT ) )
(persistent-data
 :key "SHOWN-BROWSER-MODES"
 :value '( :FACESET-FEATURES T :536F6C6964506F776572 T :464541542D47726F757073 T ) )
(persistent-data
 :key "STARTUPDIR"
 :value "C:\\Users\\jhaa\\AppData\\Roaming\\PTC")
(persistent-data
 :key "VP-DEFAULT-SETTINGS-CLIPPING"
 :value '( :CLASH-COLOR 16711680 ) )
(persistent-data
 :key "TS-AM-STUECKLISTE-BROWSER"
 :value '( :VISIBLE NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-FLY-BY"
 :value '( :TOOLTIP-DELAY 500 :HIGHLIGHT-DELAY 20 ) )
(persistent-data
 :key "AM-FNTSYMS"
 :value '( :VISIBLE NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-VIEW-BY"
 :value '( :WP-PAN-ZOOM-MODE :CENTER-CENTER :FACE-CENTER-FACE T 
           :FACE-UP-DIR-MODE :BEST ) )
(persistent-data
 :key "2D-DRAWING-STYLE"
 :value :NH)
(persistent-data
 :key "GLOBAL-SKIN"
 :value :CREO)
(persistent-data
 :key "FILING-REVISION"
 :value :DEFAULT)
(persistent-data
 :key "TS-COMPP-BROWSER"
 :value '( :VISIBLE NIL :BORDER-MODE NIL ) )
(persistent-data
 :key "DB-ATTRIBUTE-BROWSER"
 :value '( :VISIBLE NIL :TABLE-SEC-ICONS NIL :TREE-SEC-ICONS NIL :DISP-ROOT-NODE 
           NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-SHADOW-MIRROR"
 :value '( :PART-FACE-MIRROR-ENABLED NIL :FLOOR-ENABLED NIL :SHADOW-ENABLED NIL 
           :VP-SCENERY-ENABLED NIL :ENVIRONMENT-MAP 
           "C:/CADApp/PTC/Creo Elements/Direct Modeling 20.3.2.0/personality/rendering/environments/studio1.jpg" ) )
(persistent-data
 :key "UIL-HISTORY"
 :value '( :ENTRIES ( "10" ) :MAXNUM 50 :STORE T ) )
(persistent-data
 :key "PART_PROPERTY_PROPOSALS"
 :value '( :DENSITY ( "Keine" "1" ) :BASE_DENSITY ( "Standard" "1" ) ) )
(persistent-data
 :key "AVAILABLE-SHORTCUTS-BROWSER"
 :value '( :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "DIRECTION-DECODER"
 :value '( :REFERENCE-ELEMENT-TYPE :EDGE-FACE :INITIAL-DIRECTION-NEGATIVE NIL ) )
(persistent-data
 :key "CONFIGURATION-SETTINGS"
 :value '( :PART-COLOR 255 :ASSY-COLOR 65535 :POSITION-OUTSIDE-OWNER NIL 
           :SMOOTH-UPDATE-APPLY T :WARN-ON-FORMATIONS T :SMOOTH-UPDATE-TIME 2 ) )
(persistent-data
 :key "PARCEL-GBROWSER"
 :value '( :BORDER-MODE NIL :RESTORE-EXPAND-STATE NIL :FILTER-SELECT-IN-TREE NIL 
           :FILTER-VP-HIGHLIGHT NIL :SEARCH-VP-HIGHLIGHT NIL ) )
(persistent-data
 :key "NEWSLETTERREGISTRATION"
 :value -1)
(persistent-data
 :key "SHOWTUTORIALATSTARTUP"
 :value NIL)
(persistent-data
 :key "TEMPLATES"
 :value '( :VISIBLE NIL :DISP-ROOT-NODE NIL :TABLE-SEC-ICONS NIL ) )
(persistent-data
 :key "UNITS"
 :value '( :MASS ( :G 1 ) :ANGLE ( :DEG 1 ) :LENGTH ( :MM 1 ) ) )
(persistent-data
 :key "PRESELECTION"
 :value T)
(persistent-data
 :key "CURRENTQUICKSTARTTUTORIAL"
 :value 1)
(persistent-data
 :key "DEFAULT-SETTINGS-STYLE-REFERENCE_COLUMN-TREE-DIALOG"
 :value '( :HEIGHT 106 :WIDTH 730 :COLUMN-WIDTHS ( 58 68 71 75 75 141 78 69 59 ) ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-SHOW"
 :value '( :WORKPLANES_2DGEO_LABEL T :WORKPLANES_HIDDEN T 
           :WORKPLANES_2DGEO_VERTICES NIL :WORKPLANES_2DCONSTRUCTION T 
           :WORKPLANES_2DGEO T :WORKPLANES_LABEL T :WORKPLANES_LOCAL_AXIS NIL 
           :WORKPLANE_SETS_MATCH_LINES T :WORKPLANES_BORDER T :WORKPLANES T 
           :CLIP_LINES T :CLIP_PLANES T :COORD_SYSTEM T :DOCU_PLANES T 
           :3DGEO_LABEL T :3DGEO_VERTICES NIL :3DGEO_HIDDEN_DIMMED NIL 
           :3DGEO_MIXED NIL :3DGEO_WIRE NIL :3DGEO_HIDDEN T :3DGEO_EDGED T 
           :3DGEO_SHADED T :3DGEO T :3DGEO_PART_TRANSPARENCY T 
           :WORKPLANES_DIM_INACTIVE T :CLIP_HATCHES NIL :FEATURE_PTS NIL ) )
(persistent-data
 :key "CLASH-ANALYSIS-BROWSER"
 :value '( :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "ALERT-HISTORY"
 :value '( :MAX_SIZE 50 ) )
(persistent-data
 :key "RELATION-BROWSER"
 :value '( :VISIBLE NIL :TREE-SEC-ICONS NIL :TABLE-MODE T :GRAPH-MODE NIL ) )
(persistent-data
 :key "VP-DEFAULT-SETTINGS-2D"
 :value '( :CGEO-WIDTH 1 ) )
(persistent-data
 :key "DEFAULT-SETTINGS_COLUMN-TREE-DIALOG"
 :value '( :COLUMN-WIDTHS ( 335 351 80 42 484 ) :HEIGHT 1031 :WIDTH 1401 ) )
(persistent-data
 :key "USE-OK-CANCEL-BUTTONS"
 :value :NONE)
(persistent-data
 :key "LAST-USED-COMMANDS"
 :value '( :ON NIL ) )

;; ----------- end of file -----------
