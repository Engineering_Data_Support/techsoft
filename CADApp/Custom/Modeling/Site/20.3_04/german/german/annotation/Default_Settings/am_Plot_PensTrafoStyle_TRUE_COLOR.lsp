;% Automatically written on 03/18/2021 at 15:35:36
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot/PensTrafoStyle"
  :style :TRUE_COLOR
  :title "True Color"
  :values
   '("Pens" (:LABEL "Echtfarbe" :PENLIST
               ((:OLD_LINETYPE :ALL :COLOR_DEF :ALL :COLOR1
                    0.0,0.0,0.0 :COLOR2
                    1.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE :OFF
                    :PEN_NUMBER 1 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,1.0,1.0 :COLOR2
                    1.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE :OFF
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,1.0,1.0 :COLOR2
                    1.0,1.0,1.0 :PENSIZE_DEF :SINGLE :PENSIZE1 0
                    :PENSIZE2 0 :NEW_LINETYPE :SAME :NEW_PENSIZE 0.25
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,0.0,0.0 :COLOR2
                    1.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.17999999999999999 :PEN_NUMBER 2 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,0.0,1.0 :COLOR2
                    0.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE 0.25
                    :PEN_NUMBER 5 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,1.0,0.0 :COLOR2
                    0.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.34999999999999998 :PEN_NUMBER 3 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,1.0,0.0 :COLOR2
                    1.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.34999999999999998 :PEN_NUMBER 4 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,1.0,1.0 :COLOR2
                    0.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.69999999999999996 :PEN_NUMBER 7 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,0.0,1.0 :COLOR2
                    1.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE 0.12
                    :PEN_NUMBER 6 :NEW_COLOR :SAME)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,0.0,0.0 :COLOR2
                    0.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE :OFF
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)))
     )
)
