;% Automatically written on 03/18/2021 at 15:35:36
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Dimension"
  :style :NH
  :title "nh"
  :values
   '("AutoPlace/DatumStep" 7.0
     "AutoPlace/MinimalSpace" 10.0
     "Lines/Broken" LISP::T
     "Lines/OffsetPoint" 2.0
     "Specific/Diameter/BothDiaArrows" LISP::T
     "Specific/Thread/Format" :M
     "Specific/Thread/LabelLeftHand" "links"
     "Specific/Thread/LabelRightHand" ""
     "Text/Common/Space" 1.0
     "Text/MainTolerance/SizeMode" :REL06
     "Text/MainTolerance/TextStyle" :TOLERANZ
     "Text/Postfix/TextStyle" :TOLERANZ
     "Text/Prefix/TextStyle" :TOLERANZ
     "Text/SecondTolerance/TextStyle" :TOLERANZ
     "Text/Superfix/TextStyle" :STANDARD
     )
)
