;% Automatically written on 03/18/2021 at 11:45:20
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot/PrinterStyle"
  :style :A3
  :title "A3"
  :values
   '("Destination" ""
     "Name" :|\\\\HERBOLD-TA.NEUE-HERBOLD.DE\\FOLLOWME|
     "NumberCopies" 1
     "Orientation" :LANDSCAPE
     "PaperSize" :A3
     "Properties" (:LABEL " " :TYPE :MSWINDOW_GDI_PRINTER :VALUE LISP::NIL)
     "ToFile" LISP::NIL
     "Type" :MSWINDOW_GDI_PRINTER
     )
)
