;% Automatically written on 03/17/2021 at 16:52:01
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :IRON
  :title "Eisen"
  :values
   '("Description" "L:1, A: 45.0, D:   5.0"
     "PatternAngle" 0.78539816339744828
     "PatternDistance" 5.0
     "SubPattern" (:LABEL "Eisen" :SUBPATTERN
                     ((:COLOR 0.62352941176470589,0.62352941176470589,0.62352941176470589
                              :LINETYPE :SOLID :DISTANCE 1.0 :OFFSET
                              0.0 :ANGLE 0.0 :WIDTH 0)))
     )
)
