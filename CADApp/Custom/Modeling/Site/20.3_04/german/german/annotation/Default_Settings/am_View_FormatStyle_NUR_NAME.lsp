;% Automatically written on 03/17/2021 at 17:19:42
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/View/FormatStyle"
  :style :NUR_NAME
  :title "Nur Name"
  :values
   '("FormatBrowser" "<NameNummer>"
     "FormatLabel" "<Name>"
     "InternalFormatBrowser" "<:PREFIX:><:NUMBER:>"
     "InternalFormatLabel" "<:PREFIX:>"
     )
)
