;; Automatically written on 18-Mar-2021 15:35:35
;; Module 3D DOCUMENTATION
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "3D DOCUMENTATION")
(persistent-data
 :key "DIMENSION"
 :value '( :DIMENSION-SIZE 200 :SPC-DL-TO-BOX 50 :ARROW-FILL T :ARROW-SIZE 200 
           :LEN-EXTENSION 50 ) )

;; ----------- end of file -----------
