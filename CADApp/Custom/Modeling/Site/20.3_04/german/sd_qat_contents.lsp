;; Automatically written on 18-Mar-2021 12:06:56
(oli::sd-fluentui-set-quick-access-toolbar-contents "SolidDesigner"
  '(
   (:AVAILCMD ("SolidDesigner" "Filing" "New Session"))
   (:AVAILCMD ("SolidDesigner" "Filing" "Load ..."))
   (:AVAILCMD ("SolidDesigner" "Filing" "Save ..."))
   (:AVAILCMD ("SolidDesigner" "Print" "Print Preview"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "Miscellaneous" "Annotation"))
   (:AVAILCMD ("All" "Miscellaneous" "SheetAdv"))
   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))
   ))