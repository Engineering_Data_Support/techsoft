﻿
;;Customization function to define VIEW PROFILES used within the
;;view creation commands and update
;; (docu-clear-view-profiles)
;; resets all defined view profiles
;;
;; (docu-register-view-profile 
;; defines standard profiles for all types of views
;; :name                           "Unique profile name" 
;; :label                          "This name is displayed in the UI"
;; :minimum-number-of-parts        100  ;; if these two keywords are not specified then the profile will
;; :maximum-number-of-parts        1000 ;; never be selected by default, but can be selected 
                                        ;; within the user interface.
                                        ;; if there are more than 1 profile with the same number-of-parts
                                        ;; settings the LAST one is selected automatically (see profile
                                        ;; 'Complex Simple' and 'Simple Single' below).
;; :update-mode                    :graphics ;; or :classic  or :shaded_only
;; :shaded-mode                    :off ;; or :on
;; :render-mode                    :off ;; or :on
;; :shaded-resolution           300 ;; or any other positive number or :undefined --> automatic
;; :facet-accuracy                 :medium   ;; or :low, :high, :current
;; :econofast                      :on ;; or :off or :default 
;; :associativity-2d               :full ;; or :limited
;; :pressfit-handling              :on   ;; or :off or :default
;; :clash-recognition                 :off  ;; or :on  or :default
;; :remove-small-parts             0 ;; or any percentage number, ie 1.0 
;; :remove-library-parts           :on ;; or off 
;; :remove-parts-mode              :immediately  ;; or :temporary
;; :remove-full-circles            0 ;; or any threshold value, ie. 0.5 
;; :remove-duplicate-hidden-lines  :on ;; or :off 
;; :thread-creation                  :on ;; or :off, :default or :parent (for detail & section)
;; :centerline-creation            :on ;; or :off, :default or :parent (for detail & section)
;; :symmetryline-creation      :on ;; or :off, :default or :parent (for detail & section)
;;  :use-part-color                   :off ;; or  :on
;; :hidden-line-visible            :on ;; or :off, :default or :parent (for detail & section)
;; :tangent-line-visible           :on ;; or :off, :default or :parent (for detail & section)
;; )
;;
;; (docu-register-exception-profile
;; defines global settings for specific view types
;; :name                           "Unique profile name"
;; :label                          "This name is displayed in the UI"
;; :minimum-number-of-parts        1
;; :maximum-number-of-parts        999999
;; :view-type                      :standard ;; or :all, :section, :detail, :general
;;                                              or a list of view-types like (list :standard :general)
;; :update-mode                    :graphics ;; or :classic
;; :facet-accuracy                 :medium   ;; or :low, :high, :current
;; :econofast                      :on ;; or :off or :default
;; :associativity-2d               :full ;; or :limited
;; :pressfit-handling              :on   ;; or :off or :default
;; :clash-recognition              :off  ;; or :on  or :default
;; :remove-small-parts             0 ;; or any percentage number, ie 1.0
;; :remove-library-parts           :on ;; or off
;; :remove-parts-mode              :immediately  ;; or :temporary
;; :remove-full-circles            0 ;; or any threshold value, ie. 0.5
;; :remove-duplicate-hidden-lines  :on ;; or :off
;; :thread-creation                :on ;; or :off, :default or :parent (for detail & section)
;; :centerline-creation            :on ;; or :off, :default or :parent (for detail & section)
;; :symmetryline-creation          :on ;; or :off, :default or :parent (for detail & section)
;; :hidden-line-visible            :on ;; or :off, :default or :parent (for detail & section)
;; :tangent-line-visible           :on ;; or :off, :default or :parent (for detail & section)
;; )

(docu-clear-view-profiles)

(docu-register-view-profile
  :name                           "Simple Single" ;; same as 'Complex Single' but
                                                  ;; :update-view-immediately :on
  :label                          "Einzelnes Teil"
  :minimum-number-of-parts        1
  :maximum-number-of-parts        1
  :update-mode                    :classic
  :shaded-mode                    :off    
  :econofast                      :off
  :associativity-2d               :full
  :pressfit-handling              :off
  :clash-recognition              :off
  :update-view-immediately        '(:by_faces 300)
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off
  :use-part-color                 :off   
  :thread-creation                :on
  :centerline-creation            :on
  :symmetryline-creation          :on
  :hidden-line-visible            :on
  :tangent-line-visible           :on
)


(docu-register-view-profile
  :name                           "Small"
  :label                          "Baugr klein"
  :minimum-number-of-parts        2
  :maximum-number-of-parts        99
  :update-mode                    :classic
  :shaded-mode                    :off    
  :econofast                      :off
  :associativity-2d               :full
  :pressfit-handling              :default
  :clash-recognition              :default
  :update-view-immediately        '(:by_faces 300)
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off
  :use-part-color                 :off  
  :thread-creation                :default
  :centerline-creation            :default
  :symmetryline-creation          :default
  :hidden-line-visible            :default
  :tangent-line-visible           :default
)

(docu-register-view-profile
  :name                           "Medium"
  :label                          "Baugr mittel"
  :minimum-number-of-parts        100
  :maximum-number-of-parts        500
  :update-mode                    :graphics
  :shaded-mode                    :off    
  :facet-accuracy                 :medium
  :associativity-2d               :limited
  :pressfit-handling              :default
  :clash-recognition              :off
  :update-view-immediately        :off
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately   
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off
  :use-part-color                 :off    
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off
  :hidden-line-visible            :default
  :tangent-line-visible           :default
)

(docu-register-view-profile
  :name                           "Large"
  :label                          "Baugr groß"
  :minimum-number-of-parts        501
  :maximum-number-of-parts        999999
  :update-mode                    :graphics
  :shaded-mode                    :off    
  :facet-accuracy                 :low
  :associativity-2d               :limited
  :pressfit-handling              :off
  :clash-recognition              :off
  :update-view-immediately        :off
  :remove-small-parts             1.0
  :remove-library-parts           :on
  :remove-parts-mode              :immediately   
  :remove-full-circles            0.5
  :remove-duplicate-hidden-lines  :on 
  :use-part-color                 :off    
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off
  :hidden-line-visible            :off
  :tangent-line-visible           :off
)

(docu-register-view-profile 
  :name                           "PhotoRealistic" 
  :label                          "Fotorealistisch"
  :update-mode                    :shaded_only
  :shaded-mode                    :on
  :render-mode                    :on
  :use-part-color                 :on  
  :hidden-line-visible            :off
  :tangent-line-visible           :off
  :remove-duplicate-hidden-lines  :on   
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off  
)

(docu-register-view-profile 
  :name                           "ShadedOnly" 
  :label                          "Nur schattiert"
  :update-mode                    :shaded_only
  :shaded-mode                    :on
  :render-mode                    :off  
  :use-part-color                 :on
  :hidden-line-visible            :off
  :tangent-line-visible           :off
  :remove-duplicate-hidden-lines  :on   
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off  
)

(docu-register-view-profile 
  :name                           "ShadedGeometry" 
  :label                          "Schattiert + Geometrie"
  :update-mode                    :graphics
  :shaded-mode                    :on
  :render-mode                    :off  
  :use-part-color                 :on
  :hidden-line-visible            :off
  :tangent-line-visible           :off
  :remove-duplicate-hidden-lines  :on   
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off  
)

(docu-register-view-profile 
  :name                           "NC" 
  :label                          "NC"
  :update-mode                    :classic 
  :shaded-mode                    :off  
  :econofast                      :off 
  :associativity-2d               :full
  :pressfit-handling              :on
  :clash-recognition              :default
  :update-view-immediately        :on
  :remove-small-parts             0
  :remove-library-parts           :off
  :remove-parts-mode              :immediately  
  :remove-full-circles            0
  :remove-duplicate-hidden-lines  :off 
  :use-part-color                 :off  
  :thread-creation                :off
  :centerline-creation            :off
  :symmetryline-creation          :off
  :hidden-line-visible            :off
  :tangent-line-visible           :off
)

(docu-register-view-exception-profile 
  :name                           "Section" 
  :label                          "Schnitt"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :section
  :centerline-creation            :off   
  :symmetryline-creation          :off
  :hidden-line-visible            :off
  :tangent-line-visible           :parent
  :visible-cutaway-mode           :no
)

(docu-register-view-exception-profile 
  :name                           "Detail" 
  :label                          "Detail"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :detail
  :centerline-creation            :parent
  :symmetryline-creation          :parent
  :thread-creation                :parent
  :hidden-line-visible            :parent
  :tangent-line-visible           :parent
  :visible-cutaway-mode           :yes
)

(docu-register-view-exception-profile 
  :name                           "Broken"
  :label                          "Unterbrochen"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :broken
  :centerline-creation            :off
  :symmetryline-creation          :off
  :thread-creation                :parent
  :hidden-line-visible            :parent
  :tangent-line-visible           :parent
)

(docu-register-view-exception-profile
  :name                           "Isometric Small" 
  :label                          "Isometrisch klein"
  :minimum-number-of-parts        0
  :maximum-number-of-parts        999999
  :view-type                      :general
  :centerline-creation            :off
  :symmetryline-creation          :off
  :thread-creation                :off
  :visible-cutaway-mode           :yes
  :visible-section-mode           :yes
)

(docu-register-view-exception-profile 
  :name                           "Isometric Large" 
  :label                          "Isometrisch groß"
  :minimum-number-of-parts        10
  :maximum-number-of-parts        999999
  :view-type                      :general
  :update-mode                    :graphics
  :facet-accuracy                 :low
)

