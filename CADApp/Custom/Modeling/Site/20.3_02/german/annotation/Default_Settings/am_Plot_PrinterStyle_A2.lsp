;% Automatically written on 07/15/2016 at 08:49:55
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot/PrinterStyle"
  :style :A2
  :title "A2"
  :values
   '("Destination" ""
     "Name" :|\\\\BRAUER\\OCE_94XX|
     "NumberCopies" 1
     "Orientation" :LANDSCAPE
     "PaperSize" :OCE_A2_420X594_MM
     "Properties" (:LABEL " " :TYPE :MSWINDOW_GDI_PRINTER :VALUE LISP::NIL)
     "ToFile" LISP::NIL
     "Type" :MSWINDOW_GDI_PRINTER
     )
)
