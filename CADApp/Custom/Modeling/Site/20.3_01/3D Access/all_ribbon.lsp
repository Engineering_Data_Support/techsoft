; ------------------------------------------------------------------------------
; SVN: $Id$
; TECHSOFT ribbon tab for creo elements/direct access 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(oli::sd-fluentui-create-addon-context "TECHSOFT_CUSTOMIZATIONS"
           :color :black)

(oli::sd-fluentui-show-ribbon-tab-in-application "TECHSOFT" "All")
(oli::sd-fluentui-add-ribbon-tab "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:appendUtilitiesGroup nil
			:position 5
			:application "All"
			:title "TECHSOFT"
			:addonContext "TECHSOFT_CUSTOMIZATIONS"
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Support"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "TECHSOFT Support"
			:slot 0
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Start Teamviewer"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Start Teamviewer")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Start Netviewer"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Start Netviewer")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Support by E-Mail"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Support by E-Mail")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Support-Form"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Support-Form")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Support")
			:annotationRibbon nil
			:userDefined t
			:label "Filecenter"
			:availCmd '("All" "TECHSOFT" "TECHSOFT Filecenter")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Configuration"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "Configuration"
			:slot 1
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "Show SDCORPCUSTOMIZEDIR"
			:availCmd '("All" "TECHSOFT" "Show SDCORPCUSTOMIZEDIR")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "Show SDSITECUSTOMIZEDIR"
			:availCmd '("All" "TECHSOFT" "Show SDSITECUSTOMIZEDIR")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "Show SDUSERCUSTOMIZEDIR"
			:availCmd '("All" "TECHSOFT" "Show SDUSERCUSTOMIZEDIR")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "Software Distribution Server"
			:availCmd '("All" "TECHSOFT" "Software Distribution Server")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Configuration")
			:annotationRibbon nil
			:userDefined t
			:label "List License Servers"
			:availCmd '("All" "TECHSOFT" "List License Servers")
)
(oli::sd-fluentui-add-ribbon-group "PTC_Online_Resources"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "PTC Online Resources"
			:slot 2
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "PTC_Online_Resources")
			:annotationRibbon nil
			:userDefined t
			:label "Online Documentation"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "PTC Online Documentation")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "PTC_Online_Resources")
			:annotationRibbon nil
			:userDefined t
			:label "Online Tutorials"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "PTC Online Tutorials")
)
(oli::sd-fluentui-add-ribbon-group "TECHSOFT_Design"
			:parent "TECHSOFT"
			:annotationRibbon nil
			:userDefined t
			:show t
			:title "Design"
			:slot 3
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Creo"
			:largeImage t
			:availCmd '("All" "TECHSOFT" "Creo Design")
)
(oli::sd-fluentui-add-ribbon-separator
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Silver"
			:availCmd '("All" "TECHSOFT" "Silver Design")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Blue"
			:availCmd '("All" "TECHSOFT" "Blue Design")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("TECHSOFT" "TECHSOFT_Design")
			:annotationRibbon nil
			:userDefined t
			:label "Black"
			:availCmd '("All" "TECHSOFT" "Black Design")
)
(oli::sd-fluentui-set-tab-order-for-app "All" '("SDAPPLICATION" "SDHOME" "SDSTRUCTURE" "SDANALYSIS" "SDVIEW" "TECHSOFT" ))
