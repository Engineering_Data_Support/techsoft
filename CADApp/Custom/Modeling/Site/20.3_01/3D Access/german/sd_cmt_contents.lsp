;; Creo Elements/Direct Modeling Revision 18.0
;;
;; NOTE: This file defines the contents of the Command Mini Toolbar which
;; appears after a selection during PRESELECT.
;; The commands referenced by sd-define-application-cmt-contents are
;; available commands identified by "Application" - "Group" - "Command" 
;;
;; In the Modeling application the following scenarios (identified by
;; specific keywords) are supported:
;;
;;  :2d-edge             One or more modifiable 2D geometries
;;  :2d-circular-edge    One modifiable arc or circle
;;  :2d-fillet-edge      One modifiable fillet edge.
;;  :faces               More than one modifiable face, radial or offset face
;;  :single-face         One modifiable face
;;  :offset-faces        More than one modifiable cone or spun-bspline faces
;;  :single-offset-face  One modifiable cone or spun-bspline face
;;  :radial-face         One modifiable radial face
;;  :blend-faces         More than one modifiable blend faces
;;  :single-blend-face   One modifiable blend face
;;  :chamfer-faces       More than one modifiable chamfer faces
;;  :single-chamfer-face One modifiable chamfer face
;;  :geometric-feature-faces 
;;                       One modifiable geometric feature
;;  :edge                One or more modifiable edges
;;  :wp                  One or more workplanes where at least the position is
;;                       allowed to be modified
;;  :wp-with-profile     One workplane with some real geometry (overlapping
;;                       loops and not closed profiles are allowed).
;;                       NOTE: If this scenario is detected, the separation of
;;                       the profile into closed loop sections is performed
;;                       automatically to be used in commands like PULL
;;  :part-assembly       One or more parts and/or assemblies where at least the
;;                       position is allowed to be modified
;;  :annotation-3d       One 3D annotation where at least the
;;                       position is allowed to be modified


(oli:sd-define-application-cmt-default-contents "SolidDesigner"
 :contents
 '( :3d-wire-edges    (("SolidDesigner" "3D Curve" "Position Curves")
                       ("SolidDesigner" "3D Curve" "Adjust Curves")
                       ("SolidDesigner" "3D Curve" "Fillet Curve")
                       ("SolidDesigner" "3D Curve" "Change Radius")
                       ("SolidDesigner" "3D Curve" "Modify Spline 3D")
                       ("SolidDesigner" "3D Curve" "Untrim Curve")
                       ("SolidDesigner" "Surfacing" "Split Edge")
                       ("SolidDesigner" "3D Curve" "Wire Editor")
                       ("SolidDesigner" "3D Curve" "Delete Curve")
                       ("SolidDesigner" "Freeform" "Pipe Add")
                       ("SolidDesigner" "Machine" "Linear Pull Edges")
                       ("SolidDesigner" "Settings" "Edge Properties"))
   :2d-edge          (("SolidDesigner" "Modify 2D" "Stretch 2D CP")
                      ("SolidDesigner" "Modify 2D" "Move 2D CP")
                      ("SolidDesigner" "Modify 2D" "Copy 2D")
                      ("SolidDesigner" "Modify 2D" "Rotate 2D")
                      ("SolidDesigner" "Modify 2D" "Split 2D")
                      ("SolidDesigner" "Create 2D" "Equidistance")
                      ("SolidDesigner" "Modify 2D" "Convert Geo")
                      ("SolidDesigner" "Machine" "Linear Pull Edges")
                      ("SolidDesigner" "3D Documentation" "Create Anno")
                      ("SolidDesigner" "Settings" "2D Edge Properties"))
   :2d-circular-edge (("SolidDesigner" "Modify 2D" "Change Radius 2D")
                      ("SolidDesigner" "Modify 2D" "Stretch 2D CP")
                      ("SolidDesigner" "Modify 2D" "Move 2D CP")
                      ("SolidDesigner" "Modify 2D" "Copy 2D")
                      ("SolidDesigner" "Modify 2D" "Split 2D")
                      ("SolidDesigner" "Create 2D" "Equidistance")
                      ("SolidDesigner" "Modify 2D" "Convert Geo")
                      ("SolidDesigner" "Machine" "Linear Pull Edges")
                      ("SolidDesigner" "3D Documentation" "Create Anno")
                      ("SolidDesigner" "Settings" "2D Edge Properties"))
   :2d-fillet-edge   (("SolidDesigner" "Modify 2D" "Stretch 2D CP")
                      ("SolidDesigner" "Modify 2D" "2D Fillet Modify New")
                      ("SolidDesigner" "Modify 2D" "2D Fillet Remove New")
                      ("SolidDesigner" "Settings" "2D Edge Properties"))
   :faces            (("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Stretch")
                      ("SolidDesigner" "Modify 3D" "Linear Pull Faces")
                      ("SolidDesigner" "Modify 3D" "Angular Pull Faces")
                      ("SolidDesigner" "Modify 3D" "Offset")
                      ("SolidDesigner" "Modify 3D" "Taper")
                      ("SolidDesigner" "Modify 3D" "Align")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :cone-spun-faces  (("SolidDesigner" "Modify 3D" "Offset")
                      ("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Stretch")
                      ("SolidDesigner" "Modify 3D" "Taper")
                      ("SolidDesigner" "Modify 3D" "Align")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :radial-face      (("SolidDesigner" "Modify 3D" "Change Radius")
		      ;;("SolidDesigner" "Modify 3D" "Change Diameter")
                      ("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Align")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :blend-faces      (("SolidDesigner" "Modify 3D" "Blend Modify")
                      ("SolidDesigner" "Modify 3D" "Blend Remove")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :chamfer-faces    (("SolidDesigner" "Modify 3D" "Chamfer Modify")
                      ("SolidDesigner" "Modify 3D" "Chamfer Remove")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :geometric-feature-faces
                     (("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Cut Faces")
                      ("SolidDesigner" "Modify 3D" "Copy Faces")
		      ("SolidDesigner" "Process Info" "Create FSF Boss")
		      ("SolidDesigner" "Process Info" "Create FSF Boss Breakthrough")
		      ("SolidDesigner" "Process Info" "Create FSF Pocket")
		      ("SolidDesigner" "Process Info" "Create FSF Breakthrough")
                      ("SolidDesigner" "Modify 3D" "Stretch")
                      ("SolidDesigner" "Modify 3D" "Linear Pull Faces")
                      ("SolidDesigner" "Modify 3D" "Offset")
                      ("SolidDesigner" "Modify 3D" "Taper")
                      ("SolidDesigner" "Modify 3D" "Align")
                      ("SolidDesigner" "Settings" "Face Properties"))
  :cylinder-feature  (("SolidDesigner" "Modify 3D" "Change Radius")
		      ;;("SolidDesigner" "Modify 3D" "Change Diameter")
                      ("SolidDesigner" "Modify 3D" "Cut Faces")
		      ("SolidDesigner" "Process Info" "Create FSF Breakthrough")
		      ("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Stretch")
                      ("SolidDesigner" "Modify 3D" "Taper")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :cylinder-face    (("SolidDesigner" "Modify 3D" "Change Radius")
		      ;;("SolidDesigner" "Modify 3D" "Change Diameter")
                      ("SolidDesigner" "Modify 3D" "Cut Faces")
		      ("SolidDesigner" "Process Info" "Create FSF Breakthrough")
                      ("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Stretch")
                      ("SolidDesigner" "Modify 3D" "Taper")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :planar-face      (("SolidDesigner" "Modify 3D" "Move 3D")
                      ("SolidDesigner" "Modify 3D" "Stretch")
                      ("SolidDesigner" "Modify 3D" "Linear Pull Faces")
                      ("SolidDesigner" "Modify 3D" "Angular Pull Faces")
                      ("SolidDesigner" "Modify 3D" "Taper")
                      ("SolidDesigner" "Modify 3D" "Align")
                      ("SolidDesigner" "2D CoPilot" "Line/Arc")
                      ("SolidDesigner" "2D CoPilot" "Rectangle")
                      ("SolidDesigner" "2D CoPilot" "Circle")
                      ("SolidDesigner" "Settings" "Face Properties"))
   :edge             (("SolidDesigner" "Modify 3D" "Create Const Radius Blend")
                      ("SolidDesigner" "Modify 3D" "Create Var2 Radius Blend")
                      ("SolidDesigner" "Modify 3D" "Create Varn Radius Blend")
                      ("SolidDesigner" "Modify 3D" "Create Const Dist Chamfer")
                      ("SolidDesigner" "Modify 3D" "Create Dist Dist Chamfer")
                      ("SolidDesigner" "Modify 3D" "Create Dist Angle Chamfer")
		      ("SolidDesigner" "Modify 3D" "Change Edge Radius")
		      ;;("SolidDesigner" "Modify 3D" "Change Edge Diameter")
		      ("SolidDesigner" "Modify 3D" "Stretch Edge")
                      ("SolidDesigner" "Machine" "Linear Pull Edges")
                      ("SolidDesigner" "3D Documentation" "Create Anno")
                      ("SolidDesigner" "Settings" "Edge Properties"))
   :single-wp        (("SolidDesigner" "2D CoPilot" "Line/Arc")
                      ("SolidDesigner" "2D CoPilot" "Rectangle")
                      ("SolidDesigner" "2D CoPilot" "Circle")
                      ("SolidDesigner" "Workplane" "Position WP")
                      ("SolidDesigner" "Workplane" "Set Active WP")
                      ("SolidDesigner" "Settings" "Workplane Properties"))
   :single-wp-with-profile  
                     (("SolidDesigner" "Machine" "Linear Pull Profile")
                      ("SolidDesigner" "Machine" "Angular Pull Profile")
                      ("SolidDesigner" "Machine" "New Punch")
                      ("SolidDesigner" "Machine" "New Stamp")
                      ("SolidDesigner" "Workplane" "Set Active WP")
                      ("SolidDesigner" "Workplane" "Position WP")
                      ("SolidDesigner" "2D CoPilot" "Line/Arc")
                      ("SolidDesigner" "2D CoPilot" "Rectangle")
                      ("SolidDesigner" "2D CoPilot" "Circle")
                      ("SolidDesigner" "Settings" "Workplane Properties"))
   :wps              (("SolidDesigner" "Workplane" "Position WP")
                      ("SolidDesigner" "Settings" "Workplane Properties"))
   :part-assembly    (("SolidDesigner" "Part and Assy" "Position Dynamic")
                      ("SolidDesigner" "Part and Assy" "Copy Part/Assy")
                      ("SolidDesigner" "Part and Assy" "Share Part/Assy")
                      ("SolidDesigner" "Part and Assy" "Set Active Part")
                      ("SolidDesigner" "Settings" "Part Properties")
                      ("SolidDesigner" "Settings" "Assembly Properties"))
   :face-part        (("SolidDesigner" "Part and Assy" "Position Dynamic")
                      ("SolidDesigner" "Modify 3D" "Paste")
                      ("SolidDesigner" "Surfacing" "Grow Surface")
                      ("SolidDesigner" "Surfacing" "Insert Face")
                      ("SolidDesigner" "Surfacing" "Thicken a Face Part")
                      ("SolidDesigner" "Surfacing" "Show Gaps")
                      ("SolidDesigner" "Part and Assy" "Copy Part/Assy")
                      ("SolidDesigner" "Part and Assy" "Share Part/Assy")
                      ("SolidDesigner" "Part and Assy" "Set Active Part")
                      ("SolidDesigner" "Settings" "Part Properties"))
   :single-part      (("SolidDesigner" "Part and Assy" "Position Dynamic")
                      ("SolidDesigner" "Machine" "Shell")
                      ("SolidDesigner" "Machine" "Unite")
                      ("SolidDesigner" "Machine" "Subtract")
                      ("SolidDesigner" "Part and Assy" "Copy Part/Assy")
                      ("SolidDesigner" "Part and Assy" "Share Part/Assy")
                      ("SolidDesigner" "Part and Assy" "Set Active Part")
                      ("SolidDesigner" "Settings" "Part Properties"))
   :annotation-3d    (;; for driving dimensions the system automatically
                      ;; adds the corresponding Modeling command (e.g. Move 3D)
                      ("SolidDesigner" "3D Documentation" "Move Anno")
                      ("SolidDesigner" "3D Documentation" "Delete Anno")
                      ("SolidDesigner" "3D Documentation" "3D Anno Properties"))
   :geometric-custom-feature (("SolidDesigner" "Process Info" "Modify Custom Feature")
                              ("SolidDesigner" "Process Info" "Create Linear Pattern")
                              ("SolidDesigner" "Process Info" "Create Linear Grid Pattern")
                              ("SolidDesigner" "Process Info" "Modify Pattern from Feature")
                              ("SolidDesigner" "Process Info" "Delete Custom Feature")
                              ("SolidDesigner" "Process Info" "Create Radial Pattern")
                              ("SolidDesigner" "Process Info" "Create Radial Grid Pattern")
                              ("SolidDesigner" "Process Info" "Delete Pattern from Feature"))
   :empty            (("SolidDesigner" "Part and Assy" "New Part")
                      ("SolidDesigner" "Part and Assy" "New Assembly")
                      ("SolidDesigner" "Workplane" "New WP")
                      ("SolidDesigner" "Workplane" "New WP + Constr. Geo")
                      ("SolidDesigner" "Workplane" "New WP on Face")
                      ("SolidDesigner" "Workplane" "New WP on Axis")
                      ("SolidDesigner" "Workplane" "New WP by Point and Dir"))
   )
 )

