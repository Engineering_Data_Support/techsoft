﻿(in-package :BW-package)
(use-package :OLI)

(oli::sd-fluentui-create-addon-context "BORGWARE_CUSTOMIZATIONS"
           :color :blue)

			
(oli::sd-fluentui-show-ribbon-tab-in-application "BWSDTAB" "All")
(oli::sd-fluentui-add-ribbon-tab "BWSDTAB"
  	:annotationRibbon nil
	:userDefined t
	:show t
	:appendUtilitiesGroup nil
	:position 5
	:application "All"
	:title "BORGWARE"
  :addonContext "BORGWARE_CUSTOMIZATIONS"
)

(oli::sd-fluentui-add-ribbon-group "BWTB"
  :parent "BWSDTAB"
  :title "BORGWARE Tools"
)

(oli::sd-fluentui-add-ribbon-group "PTC"
			:parent "BWSDTAB"
			:title "PTC Online Ressourcen"
)

(oli::sd-fluentui-add-ribbon-group "Design"
			:parent "BWSDTAB"
			:title "Design"
)


(oli::sd-fluentui-add-ribbon-group "Support"
			:parent "BWSDTAB"
			:title "Support"
)

(oli::sd-fluentui-add-ribbon-group "Config"
			:parent "BWSDTAB"
			:title "Anpassungen"
)
			
;Menue aufbauen Modeling
(sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "BWTB")
	:availCmd '("All" "Borgware" "Module")
	:label "Module" 
	:largeImage t
	;;:image (concatenate 'string *module_path* "/icons/bw.bmp")
)

;; separator
(sd-fluentui-add-ribbon-separator
	:parent '("BWSDTAB" "BWTB")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Support")
			:label "Teamviewer starten"
			:availCmd '("All" "Borgware" "Teamviewer starten")
			:largeImage t
)
;(oli::sd-fluentui-add-ribbon-button 
;			:parent '("BWSDTAB" "Support")
;			:label "Netviewer starten"
;			:availCmd '("All" "Borgware" "Netviewer starten")
;			:largeImage t
;)

;; separator
(sd-fluentui-add-ribbon-separator
	:parent '("BWSDTAB" "Support")
)

;(oli::sd-fluentui-add-ribbon-button 
;			:parent '("BWSDTAB" "Support")
;			:label  "BORGWARE Filecenter"
;			:availCmd '("All" "Borgware" "Filecenter")
;)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Support")
			:label "E-Mail an BORGWARE"
			:availCmd '("All" "Borgware" "BORGWARE Support per E-Mail")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Support")
			:label "BORGWARE Support"
			:availCmd '("All" "Borgware" "BORGWAREWWW")
)



(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "PTC")
			:label "Online Dokumentation"
			:largeImage t
			:availCmd '("All" "Borgware" "PTC Online Documentation")
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "PTC")
	:label "Online Tutorials"
	:largeImage t
	:availCmd '("All" "Borgware" "PTC Online Tutorials")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Config")
			:label "Lizenzserver auflisten"
			:availCmd '("All" "Borgware" "License Server auflisten")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Config")
			:label "Zentrale Anpassungen anzeigen"
			:availCmd '("All" "Borgware" "SDSITECUSTOMIZEDIR anzeigen")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Config")
			:label "Benutzeranpassungen anzeigen"
			:availCmd '("All" "Borgware" "SDUSERCUSTOMIZEDIR anzeigen")
)


(oli::sd-fluentui-add-ribbon-separator
	:parent '("BWSDTAB" "Design")
	:annotationRibbon nil
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "Design")
	:label "Silver"
	:availCmd '("All" "Borgware" "Silver Design")
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "Design")
	:label "Blue"
	:availCmd '("All" "Borgware" "Blue Design")
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "Design")
	:label "Black"
	:availCmd '("All" "Borgware" "Black Design")
)


