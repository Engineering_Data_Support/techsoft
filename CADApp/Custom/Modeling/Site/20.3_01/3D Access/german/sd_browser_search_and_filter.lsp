﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Description:  Creo Elements/Direct Modeling's default 
;               "Browser Search and Filter" file
; Language:     Lisp
;
; (C) Copyright 2011 Parametric Technology GmbH, all rights reserved.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This file contains definitions of browser filters and searches that can be 
;; activated via the structure browser "Filters" or "Searches" menu button.
;;
;; A browser filter or search can be specified as follows:
;;
;;  (oli:sd-create-browser-search or oli:sd-create-browser-filter
;;     {STRING}
;;        Unique browser search/filter name
;;     :name {STRING}
;;        Browser search/filter name of the search/filter.
;;        If the name conicides with an existing search/filter name, then that
;;        search/filter will be recreated.
;;        If not given, the browser search/filter name "NONAME-SEARCH"/"NONAME-FILTER"
;;        will be used.
;;     :title {STRING}
;;        Browser search/filter title that will be displayed in the 
;;        browser search/filter menu.
;;        If not given, the browser search/filter name "unnamed" will be displayed. 
;;     :criteria {LIST containing PROPERTY-LISTs}
;;        Each property list specifies a search/filter criterion by means of the following
;;        properties
;;            (
;;             :column {KEYWORD}
;;               Each column can be referenced via a registered keyword.
;;               The following column keywords are available by default
;;               for the structure browser "parcel-gbrowser":
;;                  :instance-name, :contents-name, :shared, :clip-flag,
;;                  :modified, :modifiable, :path-name
;;             :value {STRING | BOOLEAN | NUMBER | LIST of STRINGs}
;;               Value to be compared with the column value.
;;             :operation {:EQUALS|:NOT-EQUALS|:GREATER-THAN|:LESS-THAN|
;;                         :GREATER-THAN-OR-EQUALS|:LESS-THAN-OR-EQUALS)
;;               Filter operation that is used to compare the specified filter value
;;               with the column value. 
;;               If not specified, then the operation EQUALS will be used.
;;            )
;;        If a browser node satisfies the search/filter condition, then that browser node 
;;        will be displayed in the browser view.
;;        If the value of a filter is a list of strings, then the filter condition
;;        is satisfied if any of the strings match with the column value. A list of
;;        strings behaves like an OR condition.
;;        If no filter is specified, then the no filtering will take place and
;;        all browser nodes will be displayed.
;;     :match {KEYWORD [:all]}
;;        Specifies the combined effect if more than one criterion is supplied.
;;         - :all - A query match for a browser item occurs if all criteria match.
;;                  The criteria are combined like an AND condition.
;;         - :any - a query match for a browser item occurs if at least one of the
;;                  criteria matches.
;;                  The criterial are combined like an OR condition.
;;     :case-sensitive {BOOLEAN [t]}
;;        Specifies whether the value comparisons will be executed in a case sensitive
;;        manner or not. 
;;     :enable {LISP expression}
;;        The browser view will be included in the browser view menu
;;        if this LISP expression evaluates to a NON-NIL value.
;;        If not given, the browser view will be accessible in the browser
;;        view menu without restrictions.
;;


(oli:sd-create-browser-filter
  "parcel-gbrowser"
  :name "MODIFIABLES"
  :title "Modifizierbar"
  :criteria
  '((:column :modifiable :value t))
)

(oli:sd-create-browser-filter
  "parcel-gbrowser"
  :name "MODIFIED"
  :title "Geändert"
  :criteria
  '((:column :modified :value t))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                           ;;
;; Filter for Feature Types                                                  ;;
;;                                                                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(oli:sd-create-browser-search
  "parcel-gbrowser" 
  :name "Modify3d Error Search"
  :title "Übersprungen & markiert"
  :criteria 
  '((:column :feature-type :operation :equals :value 0))
)

(oli:sd-create-browser-filter
  "parcel-gbrowser"
  :name "Modify3d Error Filter"
  :title "Übersprungen & markiert"
  :criteria 
  '((:column :feature-type :operation :equals :value 0))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                           ;;
;; Filter for Clash Analysis Issue Browser                                   ;;
;;                                                                           ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(oli:sd-create-browser-filter
  "CLASH-ANALYSIS-BROWSER"
  :name "Open and Modified"
  :title "Offen & geändert"
  :case-sensitive lisp::nil
  :match :any
  :criteria '((:column :status :operation :equals :value 0)
              (:column :status :operation :equals :value 5)
              (:column :status :operation :equals :value 6)
	      )
  )

(oli:sd-create-browser-filter
  "CLASH-ANALYSIS-BROWSER"
  :name "Accepted and Ignored"
  :title "Akzeptiert & ignoriert"
  :case-sensitive lisp::nil
  :match :any
  :criteria '((:column :status :operation :equals :value 2)
	      (:column :status :operation :equals :value 3)
	      (:column :status :operation :equals :value 7)
	      )
  )

(oli:sd-create-browser-filter
  "CLASH-ANALYSIS-BROWSER"
  :name "Clashing"
  :title "Kollision"
  :case-sensitive lisp::nil
  :match :any
  :criteria '((:column :resulttype :operation :equals :value 0)
	      (:column :resulttype :operation :equals :value 2)
	      )
  )

(oli:sd-create-browser-filter
  "CLASH-ANALYSIS-BROWSER"
  :name "Touching"
  :title "Berührung"
  :case-sensitive lisp::nil
  :match :any
  :criteria '((:column :resulttype :operation :equals :value 1)
	      (:column :resulttype :operation :equals :value 2)
	      )
  )

(oli:sd-create-browser-filter
  "CLASH-ANALYSIS-BROWSER"
  :name "Clearance"
  :title "Zwischenraum"
  :case-sensitive lisp::nil
  :match :any
  :criteria '((:column :resulttype :operation :equals :value 3))
  )

(oli:sd-create-browser-filter
  "CLASH-ANALYSIS-BROWSER"
  :name "Configuration Specific"
  :title "Konfigurationsspezifisch"
  :case-sensitive lisp::nil
  :match :any
  :criteria '((:column :inherited :operation :not-equals :value 0))
  )
