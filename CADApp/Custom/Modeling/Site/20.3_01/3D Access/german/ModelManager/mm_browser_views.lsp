﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Description:  Model Manager's default "Browser Views" file
;
; (C) Copyright 2011 Parametric Technology GmbH, all rights reserved.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This file contains definitions of browser search and filters that can be activated
;; via the structure browser "Views" menu button when Model Manager is
;; activated.

;; Add MM-STATUS
(oli::sd-create-uda-enum-column-definition 
 :mm-state
 :title "MM-Status"
 :attribute "DB-GREF"
 :value-list-key :MM_STATUS_TEXT
 :attachment :contents
 :enumerators (mapcar #'(lambda (state) (second state))
                modelmanager::*mm-status-messages*)
 )

;; ADD VERSION
(oli::sd-create-uda-string-column-definition 
 :mm-version
 :title "Version"
 :attribute "DB-DREF"
 :value-list-key :VERSION
 :attachment :contents
 )

;; Add IN_PACKET_DMS_NAME
(oli::sd-create-uda-string-column-definition 
 :in-packet-dms-name-column
 :title "Mappenbesitzer"
 :attribute "DB-GREF"
 :value-list-key :IN_PACKET_DMS_NAME
 :attachment :contents
 )

;; Define DOC UID and PART UID for Details view
(oli::sd-create-uda-string-column-definition 
 :doc-uid-column
 :title "Dok-Info"
 :attribute "DB-DREF"
 :value-list-key :unique_id_string
 :attachment :contents
 )

(oli::sd-create-uda-string-column-definition 
 :part-uid-column
 :title "Stammdaten"
 :attribute "DB-PREF"
 :value-list-key :unique_id_string
 :attachment :contents
 )

;; Define Default view
(oli::sd-create-browser-view
  "MM_DEFAULT"
  :title "Standard"
  :tree-config '(:instance-name " [" :mm-version "] " :mm-state)
  :detail-config '( :contents-name :doc-uid-column :mm-version :mm-state :db-state :part-uid-column
                                   :in-packet-dms-name-column :shared :selective-instance-context
                                   :path-name :modifiable :modified)
  :activate-detail-configuration nil
  :enable '(oli::sd-license-free-module-active-p "ModelManager")
  )
  
;; Define view with instance name of shared 3d objects in pseudo folders name
(oli::sd-create-browser-view
  "MM_DEFAULT_SHARED_INSTANCE_NAMES"
  :title "Instanznamen in Pseudoordnern"
  :derive-virtual-title-from-child t
  :tree-config '(:instance-name " [" :mm-version "] " :mm-state)
  :detail-config '( :contents-name :doc-uid-column :mm-version :mm-state :db-state :part-uid-column
                                   :in-packet-dms-name-column :shared :selective-instance-context
                                   :path-name :modifiable :modified)
  :activate-detail-configuration nil
  :enable '(oli::sd-license-free-module-active-p "ModelManager")
  )

(oli::sd-set-default-browser-view "ModelManager" "MM_DEFAULT")
