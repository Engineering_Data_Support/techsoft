; ------------------------------------------------------------------------------
; SVN:  $Id: sd_data.lsp 38125 2014-07-03 10:03:33Z pjahn $
; persistent data customization file for creo elemtens/direct 3d access 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "18.0")
(persistent-data-module "SOLIDDESIGNER")

;; set active color scheme
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "SolidDesigner/ColorSchemes" :COCREATE17_0 ) ) ) ;; :CREO

;; ----------- end of file -----------