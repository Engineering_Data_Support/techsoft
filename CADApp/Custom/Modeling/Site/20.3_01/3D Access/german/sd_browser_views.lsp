﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
; Description:  Creo Elements/Direct Modeling's default "Browser Views" file
; Language:     Lisp
;
; (C) Copyright 2011 Parametric Technology GmbH, all rights reserved.
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This file contains definitions of browser views that can be activated
;; via the structure browser "Views" menu button.
;;
;; A browser view can be specified as follows:
;;
;;  (oli:sd-create-browser-view
;;     {STRING}
;;        Unique browser view name
;;     :title {STRING}
;;        Browser view title that will be displayed in the 
;;        browser views menu.
;;        If not given, the browser view name will be displayed. 
;;     :tree-config {LIST|STRING}
;;        List containing column keywords and padding strings.
;;        The value of the specified columns and the padding strings are
;;        concatenated together to form the text that will be displayed
;;        at each browser node in the browser tree. If a string is passed
;;        then this should identify an existing configuration. See
;;        sd-create-browser-configuration for details.
;;     :detail-config {LIST|STRING}
;;        List containing column keywords. Specified columns appear
;;        when detail mode of browser is switched on. If a string is passed
;;        then this should identify an existing configuration. See
;;        sd-create-browser-configuration for details.
;;     :activate-detail-configuration [t|nil]
;;        If set to t this parameter ensures that the detail part of the
;;        browser is displayed otherwise the view shows tree only when
;;        activated. Default is nil.
;;     :enable {LISP expression}
;;        The browser view will be included in the browser view menu
;;        if this LISP expression evaluates to a NON-NIL value.
;;        If not given, the browser view will be accessible in the browser
;;        view menu without restrictions.
;;     :derive-virtual-title-from-child [t|nil]
;;        This flag determines how virtual folders display their titles within
;;        this view. If set to true then the Tree Configuration title of the
;;        folder's first child is used along with the total number of children
;;        belonging to the folder. If this flag is set to nil then the model
;;        name of the first child is used along with the number of children
;;        belonging to the folder. In this case if no model name is available
;;        then the browser view will again use the Tree Configuration title
;;        of the folder's first child, as if the flag had been set to true.
;;
;; This file contains browser configurations which can be called from multiple
;; browser views.
;;
;; sd-create-browser-configuration 
;;     {STRING}
;;        Unique browser configuration name
;;     :config {LIST}
;;        This list may contain a mixture of column keywords and strings.
;;        If a keyword is specified it should refer to either one of the
;;        pre-defined column definitions or to a column previously created
;;        with one of the column creation functions.
;;        Any strings specified are used as separators for display purposes.
;;        If the configuration is referenced in a :tree-config parameter of
;;        sd-create-browser-view then the contents of the specified columns
;;        and any strings specified will be concatenated together to form a
;;        single string to be displayed at each browser node in the
;;        "Structure Tree" column unless :tree-item-func is specified.
;;        If :tree-item-func is specified then the string returned by this
;;        user defined function is used for display and any strings in the
;;        :config list are ignored.
;;        If the configuration is referenced in the detail-config parameter
;;        then a separate column will be displayed for each column specified
;;        in the configuration and any strings will again be ignored.
;;     :tree-item-func {SYMBOL}
;;        Function used to display item text
;;        This function receives a property list as a parameter corresponding
;;        to all columns specified in :config. It returns a string which will
;;        be displayed next to the tree item. If the configuration is
;;        referenced in a :detail-config parameter of sd-create-browser-view
;;        then the function will not be called and therefore has no effect.
;;        It is only useful when used in conjunction with the :tree-config
;;        parameter of sd-create-browser-view.
        

(oli:sd-create-browser-view
  "DEFAULT"
  :tree-config '(:instance-name)
  :detail-config '(:contents-name :db-state :shared :selective-instance-context)
  :title "Standard"
  :enable '(and (not (oli:sd-license-free-module-active-p "ModelManager"))
                (not (oli:sd-license-free-module-active-p "PEWMSD")))
)

(oli:sd-set-default-browser-view "SolidDesigner" "DEFAULT")

(oli:sd-browser-exec-cmd "parcel-gbrowser"
  :set-browser-mode-order 
  '( :container-mode
     :configuration-mode
     :clipping-mode
     :clash_analysis-mode
     :rel_set-mode 
     :animation-mode
     :simplification
     :molddesignadv
     :pattern-mode
     :taper-feature
     :sheet-metal
     :3d-library
     :study-mode
     :manufacturing
     :gdt-dims
     :gdt-symbols
     :gdt
     :3d-annotation
     :group-mode
     :grouping-features
     :open-reference
     :element-names
     :generic-texts
     :hidden-features
     :user-defined-feature
     :feature-mode
     :coord_sys-mode
     :docu_plane-mode
     :layout-mode
     :others-mode)
)
