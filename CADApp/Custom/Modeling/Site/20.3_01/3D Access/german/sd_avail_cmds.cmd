﻿; Folgende Sonderzeichen entsprechen folgenden Umlauten:
; Ï=ü
; Ì=ä
; Î=ö
; Ø=Ä
; Ú=Ö
; Û=Ü
; Þ=ß


(:Application "All")

 
;;----- SolidDesigner:  --------------------------------------------
 


(:Group "Borgware" :title "Borgware")
 ("Module"
   :title "Modulaktivierung"
   :image "icons/bw"
   :action "(UI::UIC-SHOW-MODULE-CONTROLLER)"
   :description "Modulaktivierung"
   :sd-access   :yes) 

("Model Manager"
 :title       "Model Manager"
 :action      (LISP::PROGN
				(LISP:IF (startup::license-free-module-active-p "ModelManager")
					(LISP::PROGN
						(MODELMANAGER::MM-SEND-CMD "SHOW-MANAGER"))
					  (LISP::PROGN (act_deact_module :act "ModelManager" "MODULE-CONTROLLER-Modules-ModelManager-TB" '(STARTUP::ACTIVATE-MODELMANAGER)))))
 :description "Öffnet das Arbeitsbereichsfenster von Model Manager"
 :image       "All/TECHSOFT/ModelManager"
 :sd-access   :yes
 :modeling-pe :no
 ) 
 
 ("Teamviewer starten"
 :title      (ui::multi-lang "Start Teamviewer" :german "Teamviewer starten")
 :action      "(oli::sd-sys-background-job (format nil \"\\\"~a\\\\\\bw_tools\\\\addons\\\\teamviewer\\\\TeamViewerQS_de-idc2gqabsh.exe\\\"\" (oli::sd-sys-getenv \"BASEDIR\")))"
 :description  (ui::multi-lang "Start the teamviewer client to collaborate with a BORGWARE support engineer" :german "Startet eine Teamviewer Sitzung")
 :image       "icons/teamviewer"
 :sd-access   :yes) 
 
("BORGWARE Support per E-Mail"
 :title      "BORGWARE Support via E-Mail"
:action      "(oli::sd-sys-background-job (format nil \"explorer.exe \\\"mailto:support@borgwareplm.de?subject=Supportanfrage zu ~a\\\"\" (oli::sd-sys-getenv \"LINKNAME\") (oli::sd-sys-getenv \"TEMP\")))"
 :description (ui::multi-lang "Contact a BORGWARE support engineer by email" :german "BORGWARE Support via E-Mail kontaktieren")
 :image       "icons/email"
 :sd-access   :yes) 
 
 ("BORGWAREWWW"
 :title "BORGWAREPLM Webseite"
 :image       "icons/www"
 :action      "(oli::sd-display-url \"http://www.borgwareplm.de\")"
 :description (ui::multi-lang "Show Borgwareplm Website" :german "Webseite der Borgwareplm GmbH")
   :sd-access   :yes) 
 
 ("SDCORPCUSTOMIZEDIR anzeigen"
 :title      "Show SDCORPCUSTOMIZEDIR"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDCORPCUSTOMIZEDIR\")))"
 :description "Show the directory for corporate wide settings"
 :image       "icons/corp"
 :sd-access   :yes) 
 
 ("SDSITECUSTOMIZEDIR anzeigen"
 :title      "Show SDSITECUSTOMIZEDIR"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDSITECUSTOMIZEDIR\")))"
 :description "Show the directory for site wide settings"
 :image       "icons/site"
 :sd-access   :yes) 
 
 ("SDUSERCUSTOMIZEDIR anzeigen"
 :title      "SDUSERCUSTOMIZEDIR anzeigen"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDUSERCUSTOMIZEDIR\")))"
 :description "Show the directory for user defined settings"
 :image       "icons/user"
 :sd-access   :yes) 
 
 ("License Server auflisten"
 :title      "License Server auflisten"
 :action      "(frame2-ui::display (oli::sd-sys-getenv \"LICENSESERVER\"))"
 :description "Konfigurierte Lizenzserver anzeigen"
 :image       "icons/licenseserver"
 :sd-access   :yes) 
 
("PTC Online Documentation"
 :title      "PTC Online Documentation"
 :action      "(oli::sd-display-url \"http://www.ptc.com/cs/help/creo_elements_direct_hc/modeling_190_hc/\")"
 :description "PTC Online Dokumentation - Öffnet die Online Dokumentation zu Creo Elements/Direct Modeling von der PTC Website (Internet-Verbindung erforderlich)"
 :image       "icons/ptc"
 :sd-access   :yes
) 
 
("PTC Online Tutorials"
 :title "Online Tutorials"
 :action      "(oli::sd-display-url \"http://learningexchange.ptc.com/tutorials/listing/product_version_id:81\")"
 :description "PTC Online Tutorials - Übersicht über Creo Elements/Direct Modeling Tutorials von der PTC Website (Internet connection required)"
 :image       "icons/ptcu"
 :sd-access   :yes
) 

  
 
 
("Black Design"
 :title       "Schwarz"
 :action      "(uib::win-set-skin-style :black)"
 :description "Switch user interface to black design"
 :image        "icons/black_design"
 :sd-access   :yes
) 
  
("Silver Design"
 :title      "Silber"
 :action      "(uib::win-set-skin-style :silver)"
 :description "Switch user interface to silver design"
 :image       "icons/silver_design" 
 :sd-access   :yes
 ) 
 
("Blue Design"
 :title       "Blau"
 :action      "(uib::win-set-skin-style :blue)"
 :description "Switch user interface to blue design"
 :image        "icons/blue_design"
 :sd-access   :yes
 ) 

("Creo Design"
 :title      "Creo"
 :action      "(uib::win-set-skin-style :creo)"
 :description "Switch user interface to Creo design"
 :image        "icons/creo_design" 
 :sd-access   :yes
 ) 
 

 
;;----- Annotation:  --------------------------------------------

#| (:Application "Annotation")

(:Group "TECHSOFT" :title "TECHSOFT")

("Switch Annotation Color Scheme"
 :title       "Switch Annotation Color Scheme"
 :action      "(ts-switch-background-color)"
 :description "Switch between the default color scheme and the b/w color scheme in Annotation."
 :image       "Annotation/TECHSOFT/Switch_Annotation_Color_Scheme"
 ) |#
