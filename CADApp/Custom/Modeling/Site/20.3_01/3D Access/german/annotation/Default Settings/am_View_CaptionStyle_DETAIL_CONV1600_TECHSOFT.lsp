;% Automatically written on 09/29/2008 at 08:50:55
;% CoCreate Modeling Revision: 2008 (16.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/View/CaptionStyle"
  :style :DETAIL_CONV1600_TECHSOFT
  :title "Detail"
  :values '("Position" :BELOW
            "Case" :UPPER
            "Numbering" :NUMBERSONLY
            "Offset" 5.0
            "Characters" "Z Y X W V U T S"
            )
)
