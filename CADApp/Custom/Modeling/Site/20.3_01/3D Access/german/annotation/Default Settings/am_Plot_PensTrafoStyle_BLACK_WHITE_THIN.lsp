;% Automatically written on 10/30/2014 at 09:07:38
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot/PensTrafoStyle"
  :style :BLACK_WHITE_THIN
  :title "Schwarz & Weiß (dünn)"
  :values
   '("Pens" (:LABEL "8 Stift-Transformation(en) definiert" :PENLIST
               ((:OLD_LINETYPE :ALL :COLOR_DEF :ALL :COLOR1
                    0.0,0.0,0.0 :COLOR2
                    1.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE :OFF
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,0.0,0.0 :COLOR2
                    1.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.17999999999999999 :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,0.0,1.0 :COLOR2
                    0.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE 0.5
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,1.0,0.0 :COLOR2
                    0.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE 0.25
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,1.0,0.0 :COLOR2
                    1.0,1.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.17999999999999999 :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,1.0,1.0 :COLOR2
                    0.0,1.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE 0.13
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    1.0,0.0,1.0 :COLOR2
                    1.0,0.0,1.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE
                    0.080000000000000002 :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)
                (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE :COLOR1
                    0.0,0.0,0.0 :COLOR2
                    0.0,0.0,0.0 :PENSIZE_DEF :ALL :PENSIZE1 0 :PENSIZE2
                    1000000.0 :NEW_LINETYPE :SAME :NEW_PENSIZE :OFF
                    :PEN_NUMBER 1 :NEW_COLOR
                    0.0,0.0,0.0)))
     )
)
