﻿(in-package :BW-package)
(use-package :OLI)

(oli::sd-fluentui-create-addon-context "BORGWARE_CUSTOMIZATIONS"
           :color :blue)

			
(oli::sd-fluentui-show-ribbon-tab-in-application "BWSDTAB" "All")
(oli::sd-fluentui-add-ribbon-tab "BWSDTAB"
  	:annotationRibbon t
	:userDefined t
	:show t
	:appendUtilitiesGroup nil
	:position 5
	:application "All"
	:title "BORGWARE"
  :addonContext "BORGWARE_CUSTOMIZATIONS"
)

(oli::sd-fluentui-add-ribbon-group "BWTB"
  :parent "BWSDTAB"
  :annotationRibbon t
  :title "BORGWARE Tools"
)

(oli::sd-fluentui-add-ribbon-group "PTC"
			:parent "BWSDTAB"
			:annotationRibbon t
			:title "PTC Online Ressourcen"
)

(oli::sd-fluentui-add-ribbon-group "Design"
			:parent "BWSDTAB"
			:annotationRibbon t
			:title "Design"
)


(oli::sd-fluentui-add-ribbon-group "Support"
			:parent "BWSDTAB"
			:annotationRibbon t
			:title "Support"
)

(oli::sd-fluentui-add-ribbon-group "Config"
			:parent "BWSDTAB"
			:annotationRibbon t
			:title "Anpassungen"
)
			
;Menue aufbauen Modeling
(sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "BWTB")
	:annotationRibbon t
	:availCmd '("All" "Borgware" "Module")
	:label "Module" 
	:largeImage t
	;;:image (concatenate 'string *module_path* "/icons/bw.bmp")
)

;; separator
(sd-fluentui-add-ribbon-separator
	:parent '("BWSDTAB" "BWTB")
	:annotationRibbon t
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Support")
			:annotationRibbon t
			:label "Teamviewer starten"
			:availCmd '("All" "Borgware" "Teamviewer starten")
			:largeImage t
)
;(oli::sd-fluentui-add-ribbon-button 
;			:parent '("BWSDTAB" "Support")
;			:label "Netviewer starten"
;			:availCmd '("All" "Borgware" "Netviewer starten")
;			:largeImage t
;)

;; separator
(sd-fluentui-add-ribbon-separator
	:parent '("BWSDTAB" "Support")
	:annotationRibbon t
)

;(oli::sd-fluentui-add-ribbon-button 
;			:parent '("BWSDTAB" "Support")
;			:label  "BORGWARE Filecenter"
;			:availCmd '("All" "Borgware" "Filecenter")
;)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Support")
			:annotationRibbon t
			:label "E-Mail an BORGWARE"
			:availCmd '("All" "Borgware" "BORGWARE Support per E-Mail")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Support")
			:annotationRibbon t
			:label "BORGWARE Support"
			:availCmd '("All" "Borgware" "BORGWAREWWW")
)



(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "PTC")
			:annotationRibbon t
			:label "Online Dokumentation"
			:largeImage t
			:availCmd '("All" "Borgware" "PTC Online Documentation")
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "PTC")
	:annotationRibbon t
	:label "Online Tutorials"
	:largeImage t
	:availCmd '("All" "Borgware" "PTC Online Tutorials")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Config")
			:annotationRibbon t
			:label "Lizenzserver auflisten"
			:availCmd '("All" "Borgware" "License Server auflisten")
)

(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Config")
			:annotationRibbon t
			:label "Zentrale Anpassungen anzeigen"
			:availCmd '("All" "Borgware" "SDSITECUSTOMIZEDIR anzeigen")
)
(oli::sd-fluentui-add-ribbon-button 
			:parent '("BWSDTAB" "Config")
			:annotationRibbon t
			:label "Benutzeranpassungen anzeigen"
			:availCmd '("All" "Borgware" "SDUSERCUSTOMIZEDIR anzeigen")
)


(oli::sd-fluentui-add-ribbon-separator
	:parent '("BWSDTAB" "Design")
	:annotationRibbon t
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "Design")
	:annotationRibbon t
	:label "Silver"
	:availCmd '("All" "Borgware" "Silver Design")
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "Design")
	:annotationRibbon t
	:label "Blue"
	:availCmd '("All" "Borgware" "Blue Design")
)

(oli::sd-fluentui-add-ribbon-button 
	:parent '("BWSDTAB" "Design")
	:annotationRibbon t
	:label "Black"
	:availCmd '("All" "Borgware" "Black Design")
)


