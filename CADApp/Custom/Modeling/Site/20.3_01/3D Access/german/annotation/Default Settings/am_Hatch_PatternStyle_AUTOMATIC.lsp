;% Automatically written on 03/21/2007 at 15:27:08
;% CoCreate OneSpace Modeling Revision: 2007 (15.00)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :AUTOMATIC
  :title "Automatisch"
  :values '("Description" "L:1, A:45.0, D: 5.0"
            "PatternDistance" 5.0
            "SubPattern" (:LABEL "Automatisch" :SUBPATTERN
                                 ((:COLOR 1.0,1.0,0.0 :LINETYPE :SOLID
                                          :DISTANCE 1.0 :OFFSET 0.0
                                          :ANGLE 0.0 :WIDTH 0)))
            "PatternAngle" 0.78539816339744828
            )
)
