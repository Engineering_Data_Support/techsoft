; ------------------------------------------------------------------------------
; SVN:  $Id: sd_data.lsp 37961 2014-06-23 06:34:58Z pjahn $
; persistent data customization file for creo elemtens/direct 3d access 19.0
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "18.0")
(persistent-data-module "SOLIDDESIGNER")

;; set active color scheme
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "SolidDesigner/ColorSchemes" :COCREATE17_0 ) ) ) ;; :CREO

;; ----------- end of file -----------