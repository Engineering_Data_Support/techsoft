; -------------------------------------------------------------------------------
; SVN: $Id: sd_qat_contents.lsp 38098 2014-07-01 15:33:12Z pjahn $
; customization file the quick access toolbar in creo elements/direct 3d access 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; -------------------------------------------------------------------------------

(oli::sd-fluentui-set-quick-access-toolbar-contents "SolidDesigner"
  '(
   (:AVAILCMD ("SolidDesigner" "Filing" "New Session"))
   (:AVAILCMD ("SolidDesigner" "Filing" "Load ..."))
   (:AVAILCMD ("SolidDesigner" "Filing" "Save ..."))
   (:AVAILCMD ("SolidDesigner" "Print" "Print Preview"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "Miscellaneous" "Annotation"))

   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))
   ))