; ---------------------------------------------------------------------------------
; SVN: $Id: am_qat_contents.lsp 37961 2014-06-23 06:34:58Z pjahn $
; customization file the quick access toolbar in creo elements/direct annotation 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ---------------------------------------------------------------------------------

(oli::sd-fluentui-set-quick-access-toolbar-contents "Annotation"
  '(
   (:AVAILCMD ("Annotation" "Filing" "New Session"))
   (:AVAILCMD ("Annotation" "Filing" "Load ..."))
   (:AVAILCMD ("Annotation" "Filing" "Save ..."))
   (:AVAILCMD ("Annotation" "Plot" "Plot"))
   (:AVAILCMD ("All" "Miscellaneous" "Undo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Redo One"))
   (:AVAILCMD ("All" "Miscellaneous" "Toolbox Buttons Icon"))
   (:AVAILCMD ("All" "Miscellaneous" "SolidDesigner"))
   (:AVAILCMD ("All" "TECHSOFT" "Model Manager"))   
   ))