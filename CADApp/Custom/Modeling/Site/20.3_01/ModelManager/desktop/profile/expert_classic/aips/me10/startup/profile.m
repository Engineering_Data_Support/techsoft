{****************************************************************************}
{*                                                                          *}
{*  WorkManager Desktop - ME10-AIP                                          *}
{*                                                                          *}
{*  Profile:                << EXPERT_CLASSIC >>                            *}
{*                                                                          *}
{****************************************************************************}

{############################################################################}
{# Profile - Environment ####################################################}
{############################################################################}
DEFINE Awm_set_profile_dir

    IF (Wmdt_profile = '')  {* Variable set in startup.m *}
	Dms_write_error 1 1 ('Missing environment variable WMDT_PROFILE')
	CANCEL
    ELSE
	LET Wmdt_profile_dir (Wmdt_dir  + '/profile/' + (LWC Wmdt_profile))
    END_IF

END_DEFINE
Awm_set_profile_dir
DELETE_MACRO Awm_set_profile_dir

{* add search path *}
{*******************}
SEARCH ADD (Wmdt_profile_dir + '/aips/me10/custom')
       (Wmdt_profile_dir + '/aips/me10/startup')
       END

{############################################################################}
{# Enable "WorkManager Desktop" - modules                                   #}
{############################################################################}
Enable_wmdt_bom
Enable_wmdt_help

{############################################################################}
{# Enable Profile ###########################################################}
{############################################################################}
DEFINE Enable_profile

    {* Title Block - Setup:    *}
    {***************************}
    {* max. number of change note entries *}
    LET Awmc_val_max_change_notes   8

    {* partname prefix for title block parts *}
    LET Awmc_val_tbname_prefix Awm_db_stds_tbname_prefix

    {* classnames for title block setup *}
    LET Awmc_val_tb_partclass  Awm_dbr_stds_part_class
    LET Awmc_val_tb_docclass   Awm_dbr_stds_doc_class

    {* WorkManager classname for title blocks *}
    LET Awmc_val_tb_class Awm_dbr_stds_tb_class

    {* Store drawing with/without title block *}
    IF (Annotation_active)
	LET Awmc_val_store_with_tb (TRUE)
    ELSE
	LET Awmc_val_store_with_tb (FALSE)
    END_IF

    {* BOM-Setup   *}
    {***************}
    IF (Wmdt_bom_enabled)

	LET Awmc_bom_val_increment     5
	LET Awmc_bom_val_start_posnr   10

	{* LINK_SRC for copy BOM *}
	{*************************}
	LET Awm_bom_val_link_src_attr "LINK_SRC"

	{* BOM-Flag-Leaderline *}
	{***********************}
	DEFINE Awmc_bom_val_leader_line_type        SOLID       END_DEFINE
	DEFINE Awmc_bom_val_leader_line_color       CYAN        END_DEFINE
	DEFINE Awmc_bom_val_leader_arrow            DOT_TYPE    END_DEFINE
	DEFINE Awmc_bom_val_leader_arrow_size       1.5         END_DEFINE

	{* Part-Highlight-Color *}
	{************************}
	DEFINE Awmc_bom_val_part_highlight_color    MAGENTA     END_DEFINE

	{* BOM-Layouts *}
	{***************}
	LET Awmc_bom_val_layout_direction   'UPPER'
	LET Awmc_bom_val_layout_ref_pos     'LOWER_RIGHT'

	{* WM-Document Class for flags and layouts *}
	{*******************************************}
	LET Awmc_bom_val_bfg_class      Awm_dbr_stds_bom_flag_class
	LET Awm_bom_val_flag_layout_a   Awm_db_stds_bom_flag_a
	LET Awm_bom_val_flag_layout_b   Awm_db_stds_bom_flag_b
	LET Awm_bom_val_flag_layout_c   Awm_db_stds_bom_flag_c
	LET Awm_bom_val_bom_layout_a    Awm_db_stds_bom_lay_a
	LET Awm_bom_val_bom_layout_b    Awm_db_stds_bom_lay_b
	LET Awm_bom_val_bom_layout_c    Awm_db_stds_bom_lay_c

    END_IF

END_DEFINE
Enable_profile
DELETE_MACRO Enable_profile

{***************************************************************************}
{* load Thumbnail support                                                  *}
{***************************************************************************}
INPUT (Wmdt_dir  + '/startup/thmbnl.inp')
