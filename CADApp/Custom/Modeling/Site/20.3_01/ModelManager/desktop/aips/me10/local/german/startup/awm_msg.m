{  @(#)  awm_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of COBRA
}

{ >>>if false }

{ LOLAMAX = 246 }

{ General messages }

LET Awm_msg_1  "SUBPART oder CONFIRM eingeben zum Laden von:"
LET Awm_msg_2  "CONFIRM eingeben, um Zeichnung umzubenennen in:"
LET Awm_msg_3  "Teil wegen Info anw�hlen oder CURRENT"
LET Awm_msg_4  "Zeichnung vom WorkManager nicht geladen."
LET Awm_msg_5  "'Name' der Teileklasse f�r das aktuelle Schriftfeld eingeben:"
LET Awm_msg_6  "'Name' der Dokumentklasse f�r das aktuelle Schriftfeld eingeben:"
LET Awm_msg_7  "Teil wegen Speicherung anw�hlen oder CURRENT"
LET Awm_msg_8  "CONFIRM eingeben, um Zeichnungs-ID in ME10 zu entfernen."
LET Awm_msg_9  "Beim Erstellen der Verbindung zum WorkManager ..."
LET Awm_msg_10 "Ung�ltiges Argument f�r die arithmetische Funktion ME10INTF."
LET Awm_msg_11 "WorkManager starten, dann Aktion neu versuchen."
LET Awm_msg_12 "FEHLER in WorkManager."
LET Awm_msg_13 "Keine Referenzen zur Aktualisierung gefunden."
LET Awm_msg_14 "Kein Tablett angeschlossen."
LET Awm_msg_15 "WorkMgr starten, oder 'DDE_ENABLE ON' mit WorkMgr verwenden, dann Aktion neu versuchen."
LET Awm_msg_16 "WARNUNG!: Oberstes Bauteil der Zeichnung 1 ist immer noch 1 Stufe unter TOP."
LET Awm_msg_17 "WARNUNG!: Oberstes Bauteil der Zeichnung 2 ist immer noch 1 Stufe unter TOP."
LET Awm_msg_18 "WARNUNG!: Oberstes Bauteil der akt. Zeichnung ist immer noch 1 Stufe unter TOP."
DEFINE Awm_msg_19 ("Teil von " + Adu_part_y + " in " + Adu_part_x +" umbenennen.")
END_DEFINE
LET Awm_msg_20 "Men�punkt f�r Hilfe w�hlen."
LET Awm_msg_35 "Drawing BOM table. Wait..."
LET Awm_msg_36 "Add PDF limited to unmodified drawings"

{ Menu texts }

DEFINE Awm_mtext_1
		 CENTER "WorkMgr"
END_DEFINE
DEFINE Awm_mtext_2
		 CENTER "Desktop"
END_DEFINE
LET Awm_mtext_3  "LADEN"
LET Awm_mtext_4  "EINZELTEIL"
LET Awm_mtext_5  "ABSOLUT"
LET Awm_mtext_6  "SPEICHERN"
LET Awm_mtext_7  "Alles"
LET Awm_mtext_8  "Teil"
LET Awm_mtext_9  "AKTUELL"
LET Awm_mtext_10 "SPEICH �BR"
LET Awm_mtext_11 "RESERVIER"
LET Awm_mtext_12 "ID"
LET Awm_mtext_13 "Zeichnung"
LET Awm_mtext_14 "WM-Teil"
LET Awm_mtext_15 "INFO"
LET Awm_mtext_16 "Top"
LET Awm_mtext_17 "ID ENTFERN"

LET Awm_mtext_18 "AKTUALISIR"
LET Awm_mtext_19 "ZEICHNEN"
LET Awm_mtext_20 "TR-Namen"
LET Awm_mtext_21 "TR-Werte"
LET Awm_mtext_22 "SFELD LAD"
LET Awm_mtext_23 ""
LET Awm_mtext_24 "ZUWEISEN"
LET Awm_mtext_25 "WM-Teile"
LET Awm_mtext_26 "AKTUELL"
LET Awm_mtext_27 "TOP"
LET Awm_mtext_28 "EBENEN"
LET Awm_mtext_29 "RES AUFHEB"
LET Awm_mtext_30 ""
LET Awm_mtext_31 ""
LET Awm_mtext_32 ""
LET Awm_mtext_33 "DATEN ZEIG"
LET Awm_mtext_34 ""

LET Awm_mtext_35 "SETZEN"
LET Awm_mtext_36 "Klassen"
LET Awm_mtext_37 "TR-ZUWEIS"
LET Awm_mtext_38 "ZUW AUFHEB"
LET Awm_mtext_39 "Breite"
LET Awm_mtext_40 "Gro�/Klein"
LET Awm_mtext_41 "Zeilen"
LET Awm_mtext_42 "Zeilenumbr"
LET Awm_mtext_43 "Genauigkt"
LET Awm_mtext_44 "OPTIONEN"
LET Awm_mtext_45 "GrBuchstab"
LET Awm_mtext_46 "Ein"
LET Awm_mtext_47 "KlBuchstab"
LET Awm_mtext_48 "Aus"
LET Awm_mtext_49 "�NDERN"
LET Awm_mtext_50 "REF ZEIGEN"
LET Awm_mtext_51 "Alles"
LET Awm_mtext_52 "Ausw�hlen"
LET Awm_mtext_53 "ZEIGEN"
LET Awm_mtext_54 "W�HLEN"
LET Awm_mtext_55 ""
LET Awm_mtext_56 ""
LET Awm_mtext_57 ""
LET Awm_mtext_58 ""
LET Awm_mtext_59 ""

DEFINE Awm_mtext_60
		 CENTER "AIP-Hauptmen�"
END_DEFINE
DEFINE Awm_mtext_61
		 CENTER "AIP-Hauptmen�"
END_DEFINE
LET Awm_mtext_62 "DATEI"
LET Awm_mtext_63 "S-FELD"
LET Awm_mtext_64 "SF-KONF"
LET Awm_mtext_65 ""
LET Awm_mtext_66 ""
LET Awm_mtext_67 ""
LET Awm_mtext_68 ""
LET Awm_mtext_69 ""

LET Awm_mtext_70 "ZEICHNUNG SPEICHERN"
LET Awm_mtext_71 "TEIL SPEICHERN"
LET Awm_mtext_72 "Aktuelles Dokument �berschreiben"
LET Awm_mtext_73 "Neue Version erstellen"
LET Awm_mtext_74 "Neues Dokument erstellen"
LET Awm_mtext_75 "Neues Teil und neues Dokument erstellen"
LET Awm_mtext_76 "ABBRECHEN"

DEFINE Awm_mtext_77
		 CENTER "Datenverwaltung"
END_DEFINE
DEFINE Awm_mtext_78
		 CENTER "Datenverwaltung"
END_DEFINE
LET Awm_mtext_79 "Einzelteil"
LET Awm_mtext_80 "Stammdaten"
DEFINE Awm_mtext_81
		 CENTER "�ffnen"
END_DEFINE
DEFINE Awm_mtext_82
		 CENTER "Editor"
END_DEFINE

DEFINE Awm_mtext_83
		 CENTER "PDF hinzu"
END_DEFINE

{ Table Titles }

LET Awm_ttitle_1 "AUS"
LET Awm_ttitle_2 "TEXT-REF NAMEN"
LET Awm_ttitle_3 "TEXTREFERENZ-NAMEN & WERTE"
LET Awm_ttitle_4 "NAMEN"
LET Awm_ttitle_5 "WERTE"
LET Awm_ttitle_6 "TEXTREFERENZINFORMATION  [ @s0 Eintr�ge gefunden]"
LET Awm_ttitle_7 "TABELLE"
LET Awm_ttitle_8 "NAME"
LET Awm_ttitle_9 "SPALTE"
LET Awm_ttitle_10 "BREITE"
LET Awm_ttitle_11 "ZEILEN"
LET Awm_ttitle_12 "PR�ZISION"
LET Awm_ttitle_13 "ZEILENUMB"
LET Awm_ttitle_14 "GRO�/KLEI"

{ New General messages }
LET Awm_msg_21
    "Teil angeben f�r die Zuweisung von WM-Teiledaten oder AKTUELL (CURRENT) oder TOP."
LET Awm_msg_22
    "Ung�ltiger Teilename:"
LET Awm_msg_23
    "Anzahl der Strukturebenen eingeben:"

{ Message if BOM not enabled }
LET Awm_msg_24
	"Konfigurationsfehler: STL nicht aktiviert."

LET Awm_msg_25
	"Vorheriger Pr�fproze� nicht abgeschlossen."
LET Awm_msg_26
	"Teile-Infos setzen:"
LET Awm_msg_27
	"Teilebaum lesen ..."
LET Awm_msg_28
	"Teile-Infos abfragen:"
LET Awm_msg_29 "Zeichnungsname eingeben:"
LET Awm_msg_30 "Es existiert keine Zeichnungsdatei."
LET Awm_msg_31 "Keine Zeichnungsklasse registriert."
LET Awm_msg_32 "Zeichnungsversion eingeben:"
LET Awm_msg_33 "Beschreibung eingeben:"
LET Awm_msg_34 "�nderungsnotiztext eingeben:"

{* Toolbar Item Text Messages for NewUI (PELOOK = 2,3) *}

LET Awm_ttext_1  "A"
LET Awm_ttext_2  "A0"
LET Awm_ttext_3  "A1"
LET Awm_ttext_4  "A2"
LET Awm_ttext_5  "A3"
LET Awm_ttext_6  "A4"
LET Awm_ttext_7  "Absolut"
LET Awm_ttext_8  "Alles"
DEFINE Awm_ttext_9  
  ("Zuweisungsebenen : "+ (STR(Awmc_val_assign_level)))
END_DEFINE
LET Awm_ttext_10  "Teile zuweisen"
LET Awm_ttext_11  "Bez�ge zuweisen"
LET Awm_ttext_12  "B"
LET Awm_ttext_13  "STL-Layout"
LET Awm_ttext_14  "STL-Vorgaben"
LET Awm_ttext_15  "STL"
LET Awm_ttext_16  "STL-Konfig"
LET Awm_ttext_17  "C "
DEFINE Awm_ttext_18
  ("Gro�/Kleinschreibung : " + Tr_case_convention_value)
END_DEFINE

LET Awm_ttext_19  "Gro�/Kleinschreibung"
LET Awm_ttext_20  "�ndern"
LET Awm_ttext_21  "L�sch ID"
LET Awm_ttext_22  "Weiter"
LET Awm_ttext_23  "Durch Angeben erstellen"
LET Awm_ttext_24  "Aus Liste erstellen"
LET Awm_ttext_25  "Aus Tabelle erstellen"
LET Awm_ttext_26  "Aktuell"
LET Awm_ttext_27  "D "
LET Awm_ttext_28  "PosNr l�schen"
LET Awm_ttext_29  "DesignManager ADVANCED"
LET Awm_ttext_30  "DesignManager DESIGNER"
LET Awm_ttext_31  "DesignManager DESKTOP"
LET Awm_ttext_32  "DesignManager EXPERT"
LET Awm_ttext_33  "DesignManager EXPERT CLASSIC"
LET Awm_ttext_34  "ZeichNamen"
LET Awm_ttext_35  "ZeichWerte"
LET Awm_ttext_36  "Zeichnungs-ID"
LET Awm_ttext_37  "Zeichnung"
LET Awm_ttext_38  "E"
LET Awm_ttext_39  "Ende"
LET Awm_ttext_40  "Markierungslayout"
LET Awm_ttext_41  "Waagerecht"
DEFINE Awm_ttext_42
  ("Zuwachs : " + (STR(Awmc_bom_val_increment)))
END_DEFINE  
LET Awm_ttext_43  "Info Zeichnung"
LET Awm_ttext_44  "Info"
LET Awm_ttext_45  "Links"
DEFINE Awm_ttext_46
  ("Linien : " + Tr_set_lines_value)
END_DEFINE  
LET Awm_ttext_47  "Linien"
LET Awm_ttext_48  "Schriftfeld laden"
LET Awm_ttext_49  "Laden"
LET Awm_ttext_50  "Unten"
LET Awm_ttext_51  "Neue Zeichnung"
LET Awm_ttext_52  "Teil&Zeich neu"
LET Awm_ttext_53  "Neue Version"
LET Awm_ttext_54  "Aus"
LET Awm_ttext_55  "Ein"
LET Awm_ttext_56  "STL �ffnen"
LET Awm_ttext_57  "Optionen"
LET Awm_ttext_58  "�berschreiben"
LET Awm_ttext_59  "POS-Nummer"
LET Awm_ttext_60  "Teil"
LET Awm_ttext_61  "Pos-Nr"
DEFINE Awm_ttext_62
  ("Genauigkeit : " + Tr_set_precision_value)
END_DEFINE  
LET Awm_ttext_63  "Genauigkt"
LET Awm_ttext_64  "PosNr neu zeichnen"
LET Awm_ttext_65  "Bezugsnamen"
LET Awm_ttext_66  "Bezugswerte"
LET Awm_ttext_67  "Alle PosNr entfernen"
LET Awm_ttext_68  "PosNr entfernen"
LET Awm_ttext_69  "Zeichnung reservieren"
LET Awm_ttext_70  "�ndStand"
LET Awm_ttext_71  "Rechts"
LET Awm_ttext_72  "Durchsuchen (Hinzuf�gen)"
LET Awm_ttext_73  "Durchsuchen (L�schen)"
DEFINE Awm_ttext_74
  ("Durchsuchungsebenen : " + (STR(Awmc_val_scan_level)))
END_DEFINE
LET Awm_ttext_75  "Klassen ausw�hlen"
LET Awm_ttext_76  "Ausw�hlen"
LET Awm_ttext_77  "Linienendpunkt festlegen"
LET Awm_ttext_78  "Vorgaben"
LET Awm_ttext_79  "STL zeigen"
LET Awm_ttext_80  "Daten zeigen"
LET Awm_ttext_81  "Teilestatus zeigen"
LET Awm_ttext_82  "Bez�ge zeigen"
DEFINE Awm_ttext_83
  ("Anfangsposition : " + (STR(Awmc_bom_val_start_posnr)))
END_DEFINE
LET Awm_ttext_84  "Alles speichern"
LET Awm_ttext_85  "Teil speichern"
LET Awm_ttext_86  "Einzelteil speichern"
LET Awm_ttext_87  "Speichern"
LET Awm_ttext_88  "Subpart"
LET Awm_ttext_89  "SF-Konf"
LET Awm_ttext_90  "SF-STL-Konf"
LET Awm_ttext_91  "SF-Konfig"
LET Awm_ttext_92  "Tabelle nach Zeichnung"
LET Awm_ttext_93  "Schriftfeld"
LET Awm_ttext_94  "TOP-Teil"
LET Awm_ttext_95  "Top"
LET Awm_ttext_96  "BENUTZER"
LET Awm_ttext_97  "Zuweisung aufheben"
LET Awm_ttext_98  "Reservierung aufheben"
LET Awm_ttext_99  "Zeichnung aktualisieren"
LET Awm_ttext_100  "SF aktualisieren"
LET Awm_ttext_101  "Von ME10 aktualisieren"
LET Awm_ttext_102  "Aktualisieren"
LET Awm_ttext_103  "Oben"
LET Awm_ttext_104  "Senkrecht"
LET Awm_ttext_105  "WM-Teile-ID"
LET Awm_ttext_106  "WMDT"
DEFINE Awm_ttext_107 
  ("Breite : " + Tr_set_width_value)
END_DEFINE   
LET Awm_ttext_108  "Breite"
LET Awm_ttext_109  "Zeilenumbruch"
LET Awm_ttext_110  "DIN"
LET Awm_ttext_111  "ISO"

LET Awm_ttext_112  "Arbeitsbereich"
LET Awm_ttext_113  "Suchen"
LET Awm_ttext_114  "PDF hinzu"
LET Awm_mttext_1   "Drawing Manager"

{* Toolbar Messages New User Interface (PELOOK = 2,3) *}

LET Awm_tmess_1  "ADU wird unter Windows nicht unterst�tzt."

{ >>>endif }
