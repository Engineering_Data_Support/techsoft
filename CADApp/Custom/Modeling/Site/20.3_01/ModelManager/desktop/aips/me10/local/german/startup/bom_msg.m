{  @(#)  bom_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of BOM-Module
}

{ >>>if false }

{ LOLAMAX = 162 }

{ General messages }

LET Awm_bom_msg_1
	"CONFIRM eingeben, um alle Positionsnummern der aktuellen STL (BOM) zu l�schen."
LET Awm_bom_msg_2
    "Kein Positionsnummerlayout geladen; Layout w�hlen und neu versuchen."
LET Awm_bom_msg_3
    "Positionsnummer zum L�schen angeben."
LET Awm_bom_msg_4
    "Keine Positionsnummer angegeben."
LET Awm_bom_msg_5
    "Element enth�lt keine Positionsinformation."
LET Awm_bom_msg_6
    "Teil f�r die Zuweisung der Positionsnummer angeben."
LET Awm_bom_msg_7
    "Teil befindet sich nicht im aktuellen STL-Baum (BOM)."
LET Awm_bom_msg_8
    "Positionsnummer ist bereits zugewiesen."
LET Awm_bom_msg_9
    "Teil enth�lt keine zugewiesenen WM-Teiledaten."
LET Awm_bom_msg_10
    "Anfangspunkt der Positionshinweislinie eingeben, oder WEITER, um fortzufahren."
LET Awm_bom_msg_11
    "Anfangspunkt der waagerechten Positionshinweislinie eingeben; ENDE oder WEITER."
LET Awm_bom_msg_12
    "Anfangspunkt der senkrechten Positionshinweislinie eingeben; ENDE oder WEITER."
LET Awm_bom_msg_13
    "Punkt auf Positionshinweislinie angeben; ENDE oder WEITER f�r n�chstes Teil."
LET Awm_bom_msg_14
    "STL-Komponente (BOM) w�hlen."
LET Awm_bom_msg_15
	"Ung�ltige Eingabe."
LET Awm_bom_msg_16
	"STL (BOM) enth�lt dieses Teil nicht."
LET Awm_bom_msg_17
    "Keine Teile ohne Positionsnummer gefunden."
LET Awm_bom_msg_18
    "Positionsnummer angeben, um die Positionshinweislinie neu zu zeichnen."
LET Awm_bom_msg_19
	"St�cklistenkomponente w�hlen, um Positions-Markierung zu zeichnen."
LET Awm_bom_msg_20
    "Ung�ltige Makrodatei."
LET Awm_bom_msg_21
    "Austauschdatei nicht gefunden."
LET Awm_bom_msg_22
	"Ung�ltige Teilestruktur: Positionsmarkierungs-Layout darf keine Einzelteile enthalten."
LET Awm_bom_msg_23
    "Endpunkt 'OBEN LINKS' ist nicht definiert."
LET Awm_bom_msg_24
    "Endpunkt 'OBEN RECHTS' ist nicht definiert."
LET Awm_bom_msg_25
    "Endpunkt 'UNTEN LINKS' ist nicht definiert."
LET Awm_bom_msg_26
    "Endpunkt 'UNTEN RECHTS' ist nicht definiert."
LET Awm_bom_msg_27
	"Teil f�r STL-Kopfzeile (BOM) angeben."
LET Awm_bom_msg_28
	"Teil f�r STL-Komponente (BOM) angeben."
LET Awm_bom_msg_29
	"Ung�ltige Teilestruktur: STL-Layout mu� aus STL-Kopf und STL-Komponente bestehen"
LET Awm_bom_msg_30
	"Option w�hlen oder existierenden Endpunkt der Positionshinweislinie angeben."
LET Awm_bom_msg_31
	"LINKS oder RECHTS oder neuen Endpunkt f�r Positionshinweislinie eingeben."
LET Awm_bom_msg_32
	"Neuen Endpunkt f�r Positionshinweislinie eingeben."
LET Awm_bom_msg_33
	"Keine St�ckliste (BOM) von WorkManager zugewiesen !"
LET Awm_bom_msg_34
    "Kein St�cklisten-Layout geladen."
LET Awm_bom_msg_35
    "Position in STL (BOM) angeben."
LET Awm_bom_msg_36
	"Baugruppe zur Erstellung der St�ckliste angeben."
LET Awm_bom_msg_37
    "WM-St�ckliste nicht verf�gbar !"
LET Awm_bom_msg_38
	"Keine Baugruppe angegeben."
LET Awm_bom_msg_39
	"Neue Positionsnummer eingeben."
LET Awm_bom_msg_40
	"Komponente korrespondiert nicht mit WM-St�ckliste."
LET Awm_bom_msg_41
	"Baugruppe f�r aktuelle St�ckliste angeben."
LET Awm_bom_msg_42
    "Kein ME10-Teil gefunden, das mit der STL-Kopfzeile korrespondiert."
LET Awm_bom_msg_43
"CONFIRM zum Aktualisieren der St�ckliste in WorkManager eingeben."
LET Awm_bom_msg_44
"CONFIRM eingeben, um die STL in ME10 mit der akt. ME10-Teilestruktur zu aktualis"
LET Awm_bom_msg_45 ""
LET Awm_bom_msg_46
"CONFIRM eingeben, um alle Positionsnummern in der ME10-STL zu l�schen."
LET Awm_bom_msg_47
	"Baugruppe angeben, um die St�ckliste in WorkManager zu �ffnen oder CURRENT."
LET Awm_bom_msg_48
	"Teil hat keine WM-Teiledaten zugewiesen."
LET Awm_bom_msg_49
	"Anfangspositionsnummer eingeben:"
LET Awm_bom_msg_50
	"Inkrementwert der Positionsnummer eingeben:"
LET Awm_bom_msg_51
	"Teil f�r STL-Komponente (BOM) angeben."
LET Awm_bom_msg_52
    "Endpunkt 'UPPER' ist nicht definiert."
LET Awm_bom_msg_53
    "Endpunkt 'LOWER' ist nicht definiert."
LET Awm_bom_msg_54
    "Endpunkt 'RECHTS' ist nicht definiert."
LET Awm_bom_msg_55
    "Endpunkt 'LINKS' ist nicht definiert."

LET Awm_bom_msg_56
    "Komponente erscheint in mehr als einer St�ckliste (STL)."
LET Awm_bom_msg_57
    "St�cklisten der n-Ebene und der 1-Ebene k�nnen nicht kombiniert werden."
LET Awm_bom_msg_58
    "Aktualisierung in der n-Ebenen-St�ckliste nicht m�glich."

LET Awm_bom_msg_59
    "Anzahl der Strukturebenen eingeben:"
LET Awm_bom_msg_21
    "Teil zur STL-Ableitung angeben oder AKTUELL (CURRENT) oder TOP."
LET Awm_bom_msg_22
    "Ung�ltiger Teilename:"

{ Menu texts }

LET Awm_bom_mtext_1 "POS-NR"
LET Awm_bom_mtext_2 "STL"
LET Awm_bom_mtext_3 "STL-KONFIG"
LET Awm_bom_mtext_4 "POS ERSTEL"
LET Awm_bom_mtext_5 "Listen"
LET Awm_bom_mtext_6 "Angeben"
LET Awm_bom_mtext_7 "WAAGERECHT"
LET Awm_bom_mtext_8 "SENKRECHT"
LET Awm_bom_mtext_9 "NEUZEICH"
LET Awm_bom_mtext_10 "Pos-Nr"
LET Awm_bom_mtext_11 "L�SCHEN"
LET Awm_bom_mtext_12 "Alles"
LET Awm_bom_mtext_13 "START-POS"
LET Awm_bom_mtext_14 "INKREMENT"
LET Awm_bom_mtext_15 "POS-LAYOUT"
LET Awm_bom_mtext_16 "AUSGABE"
LET Awm_bom_mtext_17 "Zeichnung"
LET Awm_bom_mtext_18 "STL-LAYOUT"
LET Awm_bom_mtext_19 "STL �FFNEN"
LET Awm_bom_mtext_20 "TOP"
LET Awm_bom_mtext_21 "Teil"
LET Awm_bom_mtext_22 "ZEIGEN"
LET Awm_bom_mtext_23 "STL"
LET Awm_bom_mtext_24 ""
LET Awm_bom_mtext_25 "AKTUALIS"
LET Awm_bom_mtext_26 ""
LET Awm_bom_mtext_27 ""
LET Awm_bom_mtext_28 ""
LET Awm_bom_mtext_29 ""
LET Awm_bom_mtext_30 "ME10 -> WM"
LET Awm_bom_mtext_31 "Pos-Nr"
LET Awm_bom_mtext_32 "AKTUELL"
LET Awm_bom_mtext_33 "TeilStatus"
LET Awm_bom_mtext_34 "WEITER"
LET Awm_bom_mtext_35 "ENDE"
LET Awm_bom_mtext_36 "LIN ENDPKT"
LET Awm_bom_mtext_37 "Setzen"
LET Awm_bom_mtext_38 "OBEN"
LET Awm_bom_mtext_39 "UNTEN"
LET Awm_bom_mtext_40 "LINKS"
LET Awm_bom_mtext_41 "RECHTS"
LET Awm_bom_mtext_42 "SPEICHERN"
LET Awm_bom_mtext_43 "STL-Layout"
LET Awm_bom_mtext_44 "ENTFERNEN"
LET Awm_bom_mtext_45 "STL-Tabell"
LET Awm_bom_mtext_46 "DIN"
LET Awm_bom_mtext_47 "ISO"
LET Awm_bom_mtext_48 "BENUTZER"
LET Awm_bom_mtext_49 "A"
LET Awm_bom_mtext_50 "B"
LET Awm_bom_mtext_51 "C"

LET Awm_bom_mtext_52 "STL ABLEIT"
LET Awm_bom_mtext_53                                ""
LET Awm_bom_mtext_54 "HINZU"
LET Awm_bom_mtext_55 "L�SCHEN"
LET Awm_bom_mtext_56 "AKTUELL"
LET Awm_bom_mtext_57 "TOP"
LET Awm_bom_mtext_58 "EBENEN"

{ Table Titles }

LET Awm_bom_ttitle_1  "Teilename"
LET Awm_bom_ttitle_2  "Teile-ID"
LET Awm_bom_ttitle_3  "WM-Teil"
LET Awm_bom_ttitle_4  "STL"
LET Awm_bom_ttitle_5  "Pos-Nr"
LET Awm_bom_ttitle_6  "X"
LET Awm_bom_ttitle_7  "FEHLER"
LET Awm_bom_ttitle_8  "VON"
LET Awm_bom_ttitle_9  "AKTUELLE ME10-STL"
LET Awm_bom_ttitle_10 "ME10-TEILESTATUSINFO"
LET Awm_bom_ttitle_11 "AKTU STL-KOMPONENTEN"
LET Awm_bom_ttitle_12 "Meldung"

{ >>>endif }
