{  @(#)  awm_msg.m $ Revision 3.1 $ 02/20/97
* **********************************************************************}
{
  All localizable text strings for ME10 side of COBRA
}

{ >>>if false }

{ LOLAMAX = 246 }

{ General messages }

LET Awm_msg_1  "Selezionare SUBPART o CONFIRM per caricare:"
LET Awm_msg_2  "Inserire CONFIRM per cambiare il nome del disegno in:"
LET Awm_msg_3  "Identificare la parte o inserire CURRENT"
LET Awm_msg_4  "Disegno non caricato da WorkManager"
LET Awm_msg_5  "Inserire il 'nome' della classe di parti per il cartiglio attuale:"
LET Awm_msg_6  "Inserire il 'nome' della classe di documenti per il cartiglio attuale:"
LET Awm_msg_7  "Identificare la parte da salvare o inserire CURRENT"
LET Awm_msg_8  "Inserire CONFIRM per azzerare l'ID del disegno in ME10"
LET Awm_msg_9  "Esecuzione del collegamento a WorkManager ..."
LET Awm_msg_10 "Argomento non valido per la funzione aritmetica ME10INTF"
LET Awm_msg_11 "Avviare WorkManagere e ripetere l'operazione"
LET Awm_msg_12 "ERRORE in WorkManager"
LET Awm_msg_13 "Nessun riferimento presente per l'aggiornamento"
LET Awm_msg_14 "Nessuna tavoletta collegata"
LET Awm_msg_15 "Avviare WorkManager o usare 'DDE_ENABLE ON' con WorkManager e poi ripetere l'operazione"
LET Awm_msg_16 "ATTENZIONE!: La parte radice del disegno 1 si trova ancora 1 livello al di sotto di Radice-Top"
LET Awm_msg_17 "ATTENZIONE!: La parte radice del disegno 2 � ancora 1 livello al di sotto di Radice-Top"
LET Awm_msg_18 "ATTENZIONE!: La parte radice del disegno aggiornato � ancora 1 livello al di sotto di Radice-Top"
DEFINE Awm_msg_19 ("Modifica il nome della parte " + Adu_part_y + " in " + Adu_part_x)
END_DEFINE
LET Awm_msg_20 "Selezionare la voce su cui avere la spiegazione"
LET Awm_msg_35 "Drawing BOM table. Wait..."
LET Awm_msg_36 "Add PDF limited to unmodified drawings"

{ Menu texts }

DEFINE Awm_mtext_1
		 CENTER "WorkMgr"
END_DEFINE
DEFINE Awm_mtext_2
		 CENTER "Desktop"
END_DEFINE
LET Awm_mtext_3  "CARICA"
LET Awm_mtext_4  "SOTTOPARTE"
LET Awm_mtext_5  "ORIGINE"
LET Awm_mtext_6  "SALVA"
LET Awm_mtext_7  "Tutto"
LET Awm_mtext_8  "Parte"
LET Awm_mtext_9  "CORRENTE"
LET Awm_mtext_10 "SALVA SOVR"
LET Awm_mtext_11 "RISERVATO"
LET Awm_mtext_12 "ID"
LET Awm_mtext_13 "Disegno"
LET Awm_mtext_14 "Parte WM"
LET Awm_mtext_15 "INFORMAZ."
LET Awm_mtext_16 "Radice-Top"
LET Awm_mtext_17 "AZZERA ID"

LET Awm_mtext_18 "AGGIORNA"
LET Awm_mtext_19 "COMPILA"
LET Awm_mtext_20 "Nomi Rif."
LET Awm_mtext_21 "Valori Rif"
LET Awm_mtext_22 "CARICA"
LET Awm_mtext_23 ""
LET Awm_mtext_24 "ASSEGNA"
LET Awm_mtext_25 "Parti-WM"
LET Awm_mtext_26 "CORRENTE"
LET Awm_mtext_27 "RADICE-TOP"
LET Awm_mtext_28 "LIVELLI"
LET Awm_mtext_29 "ANN.RISERV"
LET Awm_mtext_30 ""
LET Awm_mtext_31 ""
LET Awm_mtext_32 ""
LET Awm_mtext_33 "EVID. DATI"
LET Awm_mtext_34 ""

LET Awm_mtext_35 "DEFINISCI"
LET Awm_mtext_36 "Classi"
LET Awm_mtext_37 "ASSEGN-RIF"
LET Awm_mtext_38 "TOGLI RIF."
LET Awm_mtext_39 "Larghezza"
LET Awm_mtext_40 "Maius/min"
LET Awm_mtext_41 "Linee"
LET Awm_mtext_42 "A Capo"
LET Awm_mtext_43 "Fmt.Numeri"
LET Awm_mtext_44 "OPZIONI"
LET Awm_mtext_45 "Maiuscolo"
LET Awm_mtext_46 "S�"
LET Awm_mtext_47 "Minuscolo"
LET Awm_mtext_48 "No"
LET Awm_mtext_49 "CAMBIA"
LET Awm_mtext_50 "EVID. RIF."
LET Awm_mtext_51 "Tutti"
LET Awm_mtext_52 "Scegli"
LET Awm_mtext_53 "MOSTRA"
LET Awm_mtext_54 "SCEGLI"
LET Awm_mtext_55 ""
LET Awm_mtext_56 ""
LET Awm_mtext_57 ""
LET Awm_mtext_58 ""
LET Awm_mtext_59 ""

DEFINE Awm_mtext_60
		 CENTER "Menu princ. AIP"
END_DEFINE
DEFINE Awm_mtext_61
		 CENTER "Menu principale AIP"
END_DEFINE
LET Awm_mtext_62 "FILE"
LET Awm_mtext_63 "CARTIGL"
LET Awm_mtext_64 "CONF CT"
LET Awm_mtext_65 ""
LET Awm_mtext_66 ""
LET Awm_mtext_67 ""
LET Awm_mtext_68 ""
LET Awm_mtext_69 ""

LET Awm_mtext_70 "MEMORIZZA DISEGNO"
LET Awm_mtext_71 "MEMORIZZA PARTE"
LET Awm_mtext_72 "Sovrascrivere documento attuale"
LET Awm_mtext_73 "Crea nuova versione"
LET Awm_mtext_74 "Crea nuovo documento"
LET Awm_mtext_75 "Crea nuova parte e nuovo documento"
LET Awm_mtext_76 "ANNULLA"

DEFINE Awm_mtext_77
		 CENTER "Gestione Dati"
END_DEFINE
DEFINE Awm_mtext_78
		 CENTER "Gestione Dati"
END_DEFINE
LET Awm_mtext_79 "Sottoparte"
LET Awm_mtext_80 "DatiOrigin"
DEFINE Awm_mtext_81
		 CENTER "Apri"
END_DEFINE
DEFINE Awm_mtext_82
		 CENTER "Editor"
END_DEFINE
DEFINE Awm_mtext_83
		 CENTER "Aggiun. PDF"
END_DEFINE

{ Table Titles }

LET Awm_ttitle_1 "OFF"
LET Awm_ttitle_2 "NOMI RIFERIM."
LET Awm_ttitle_3 "VALORI E NOMI DEI RIFERIMENTI"
LET Awm_ttitle_4 "NOMI"
LET Awm_ttitle_5 "VALORI"
LET Awm_ttitle_6 "INFO SUI RIFERIMENTI [elementi trovati: @s0 ]"
LET Awm_ttitle_7 "TABELLA"
LET Awm_ttitle_8 "NOME"
LET Awm_ttitle_9 "COLON."
LET Awm_ttitle_10 "LARGH."
LET Awm_ttitle_11 "LINEE"
LET Awm_ttitle_12 "FMT.NUMERI"
LET Awm_ttitle_13 "A CAPO"
LET Awm_ttitle_14 "MAIUS/MIN"

{ New General messages }
LET Awm_msg_21
    "Identificare la parte a cui assegnare i dati parte-WM o CORRENTE o RADICE-TOP"
LET Awm_msg_22
    "Nome di parte non valido:"
LET Awm_msg_23
    "Inserire il numero di livelli della struttura:"

{ Message if BOM not enabled }
LET Awm_msg_24
	"Errore di configurazione: modulo DISTINTA BASE non abilitato"

LET Awm_msg_25
	"E' in corso un CONTROLLO REVISIONE"
LET Awm_msg_26
	"Imposta info della Parte:"
LET Awm_msg_27
	"Lettura dell'albero delle parti ..."
LET Awm_msg_28
	"Richiedi info della Parte:"
LET Awm_msg_29 "Inserire il nome del disegno:"
LET Awm_msg_30 "Nessun file di disegno esistente"
LET Awm_msg_31 "Nessuna classe di disegni registrata"
LET Awm_msg_32 "Inserire la versione del disegno:"
LET Awm_msg_33 "Inserire descrizione:"
LET Awm_msg_34 "Inserire testo di nota di modifica:"

{* Toolbar Item Text Messages for NewUI (PELOOK = 2,3) *}

LET Awm_ttext_1  "A"
LET Awm_ttext_2  "A0"
LET Awm_ttext_3  "A1"
LET Awm_ttext_4  "A2"
LET Awm_ttext_5  "A3"
LET Awm_ttext_6  "A4"
LET Awm_ttext_7  "Assoluto"
LET Awm_ttext_8  "Tutto"
DEFINE Awm_ttext_9  
  ("Assegna Livelli : "+ (STR(Awmc_val_assign_level)))
END_DEFINE
LET Awm_ttext_10  "Assegna Parti"
LET Awm_ttext_11  "Assegna Riferimenti"
LET Awm_ttext_12  "B"
LET Awm_ttext_13  "Layout DB"
LET Awm_ttext_14  "Impostazioni DB"
LET Awm_ttext_15  "DIST. BASE"
LET Awm_ttext_16  "Config DB"
LET Awm_ttext_17  "C "
DEFINE Awm_ttext_18
  ("Convenzione Maiusc : " + Tr_case_convention_value)
END_DEFINE
LET Awm_ttext_19  "Convenzione Maiusc"
LET Awm_ttext_20  "Modifica"
LET Awm_ttext_21  "Cancella ID"
LET Awm_ttext_22  "Continua"
LET Awm_ttext_23  "Crea da Ident"
LET Awm_ttext_24  "Crea da Lista"
LET Awm_ttext_25  "Crea da Tabella"
LET Awm_ttext_26  "Attuale"
LET Awm_ttext_27  "D "
LET Awm_ttext_28  "Elimina Num Pos"
LET Awm_ttext_29  "DesignManager ADVANCED"
LET Awm_ttext_30  "DesignManager DESIGNER"
LET Awm_ttext_31  "DesignManager DESKTOP"
LET Awm_ttext_32  "DesignManager EXPERT"
LET Awm_ttext_33  "DesignManager EXPERT CLASSIC"
LET Awm_ttext_34  "Mostra Nomi****"
LET Awm_ttext_35  "Mostra Valori****"
LET Awm_ttext_36  "ID Disegno"
LET Awm_ttext_37  "Disegno"
LET Awm_ttext_38  "E"
LET Awm_ttext_39  "Fine"
LET Awm_ttext_40  "Indic Layout"
LET Awm_ttext_41  "Orizzontale"
DEFINE Awm_ttext_42
  ("Incremento : " + (STR(Awmc_bom_val_increment)))
END_DEFINE  
LET Awm_ttext_43  "Info Disegno"
LET Awm_ttext_44  "Info"
LET Awm_ttext_45  "Sinistro"
DEFINE Awm_ttext_46
  ("Linee : " + Tr_set_lines_value)
END_DEFINE  
LET Awm_ttext_47  "Linee"
LET Awm_ttext_48  "Carica Cartiglio"
LET Awm_ttext_49  "Carica"
LET Awm_ttext_50  "Inferiore"
LET Awm_ttext_51  "Nuovo Disegno"
LET Awm_ttext_52  "Nuova Parte&Disegno"
LET Awm_ttext_53  "Nuova Versione"
LET Awm_ttext_54  "Off"
LET Awm_ttext_55  "On"
LET Awm_ttext_56  "Apri DB"
LET Awm_ttext_57  "Opzioni"
LET Awm_ttext_58  "Sovrascrivi"
LET Awm_ttext_59  "Numero Pos"
LET Awm_ttext_60  "Parte"
LET Awm_ttext_61  "Num-Posiz"
DEFINE Awm_ttext_62
  ("Precisione : " + Tr_set_precision_value)
END_DEFINE  
LET Awm_ttext_63  "Precisione"
LET Awm_ttext_64  "Ridis Num Pos"
LET Awm_ttext_65  "Nomi Riferimento"
LET Awm_ttext_66  "Valori Riferimento"
LET Awm_ttext_67  "Elimina Tutti Num Pos"
LET Awm_ttext_68  "Elimina Num Pos"
LET Awm_ttext_69  "Riserva Disegno"
LET Awm_ttext_70  "Controllo Rev"
LET Awm_ttext_71  "Destro"
LET Awm_ttext_72  "Naviga (Aggiungi)"
LET Awm_ttext_73  "Naviga (Pulisci)"
DEFINE Awm_ttext_74
  ("Esamina Livelli : " + (STR(Awmc_val_scan_level)))
END_DEFINE
LET Awm_ttext_75  "Selezione Classi"
LET Awm_ttext_76  "Selezione"
LET Awm_ttext_77  "Imposta Punto Finale Linea"
LET Awm_ttext_78  "Impostazioni"
LET Awm_ttext_79  "Mostra Bom"
LET Awm_ttext_80  "Mostra Dati"
LET Awm_ttext_81  "Mostra Stato Parte"
LET Awm_ttext_82  "Mostra Riferimenti"
DEFINE Awm_ttext_83
  ("Pos Partenza : " + (STR(Awmc_bom_val_start_posnr)))
END_DEFINE
LET Awm_ttext_84  "Salva tutto"
LET Awm_ttext_85  "Salva Parte"
LET Awm_ttext_86  "Salva Sottoparte"
LET Awm_ttext_87  "Salva"
LET Awm_ttext_88  "Sottoparte"
LET Awm_ttext_89  "Config Cart"
LET Awm_ttext_90  "Config Cart DB"
LET Awm_ttext_91  "Config CT"
LET Awm_ttext_92  "Tabella a Disegno"
LET Awm_ttext_93  "Cartiglio"
LET Awm_ttext_94  "Parte Top"
LET Awm_ttext_95  "Top"
LET Awm_ttext_96  "UTENTE"
LET Awm_ttext_97  "Annulla Assegnamento"
LET Awm_ttext_98  "Annulla Riserva"
LET Awm_ttext_99  "Aggiorna Disegno"
LET Awm_ttext_100  "Aggiorna Cartiglio"
LET Awm_ttext_101  "Aggiorna da ME10"
LET Awm_ttext_102  "Aggiorna"
LET Awm_ttext_103  "Maiuscolo"
LET Awm_ttext_104  "Verticale"
LET Awm_ttext_105  "ID Parte WM"
LET Awm_ttext_106  "WMDT"
DEFINE Awm_ttext_107 
  ("Ampiezza : " + Tr_set_width_value)
END_DEFINE   
LET Awm_ttext_108  "Ampiezza"
LET Awm_ttext_109  "Parole a capo"
LET Awm_ttext_110  "DIN"
LET Awm_ttext_111  "ISO"

LET Awm_ttext_112  "Spazio di Lavoro"
LET Awm_ttext_113  "Ricerca"
LET Awm_ttext_114  "Aggiun. PDF"
LET Awm_mttext_1   "Drawing Manager"

{* Toolbar Messages New User Interface (PELOOK = 2,3) *}

LET Awm_tmess_1  "ADU non supportato nell'Interfaccia Windows"

{ >>>endif }
