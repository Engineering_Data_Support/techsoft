{  @(#)  enable.m $ Revision 2.1 $ 03/20/96
 ****************************************************************************}
{*                                                                          *}
{*  WorkManager Desktop:                                                    *}
{*  Version 3.2: Enable_xxx-functions                                       *}
{*                                                                          *}
{****************************************************************************}

{*****************************}
{* Macro to enable BOM-Modul *}
{*****************************}
DEFINE Enable_wmdt_bom
LOCAL Awm_os
LOCAL Pelook

  LET Awm_val_doc_module 0
  IF (NOT Annotation_active)
    { *** Load ME10_ANNO module. Exists from ME10 version > 11.00b *** }
    TRAP_ERROR
    LOAD_MODULE "ME10_ANNO"

    { *** Awm_val_doc_module = 1 : ME10_ANNO module is available *** }
    IF (NOT CHECK_ERROR)
      LET Awm_val_doc_module 1
    END_IF
  END_IF
    {* load macros *}
    {***************}
  LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdtbom.mac")

  {* init BOM-module *}
  {*******************}
  Awmc_initialize_bom

  {* pop up AIP menu  *}
  {********************}
  INQ_ENV 10
  LET Awm_os (INQ 4)
  IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
  { DOS, Windows, Windows NT, Windows '95 }
    INQ_ENV 10
    LET Pelook (INQ 6)
    IF ((Pelook > 1) AND (Pelook < 4) AND (MACRO_EXISTS "Create_wmdt_bom_toolbars"))
      Create_wmdt_bom_toolbars
    ELSE_IF (NOT Annotation_active)
      Awmc_file_menu
    END_IF
  ELSE  { HP-UX }
    IF (NOT Annotation_active)
      Awmc_file_menu
    END_IF
  END_IF

  LET Wmdt_bom_enabled (TRUE)

END_DEFINE

{***************************************************************************}
{* activate Online Help                                                    *}
{***************************************************************************}
DEFINE Enable_wmdt_help

LOCAL Awm_os
LOCAL Lang_dir

  Wmdt_get_local_lang_dir
  LET Lang_dir Dms_return_val
  IF (Lang_dir <> "")
    LET Lang_dir ("/" + Lang_dir)
  END_IF

  LOAD_MACRO (Wmdt_dir + "/aips/me10/startup/wmdthlp.mac")

  LET Awm_val_help_path (Wmdt_dir + Lang_dir + '/help')
  LET Awm_val_help_csl       (Wmdt_dir + '/aips/me10/startup/maptable.csl')
  LET Awm_val_cust_help_csl  (Wmdt_dir + '/aips/me10/custom/maptable.csl')

  INQ_ENV 10
  LET Awm_os (INQ 4)
  IF ((Awm_os = 3) OR (Awm_os = 6) OR (Awm_os = 7) OR (Awm_os = 8))
  { DOS, Windows, Windows NT, Windows '95 }
    LET Awmc_val_netscape_exe (Wmdt_dir + Lang_dir + '/bin/netscape/netscape.exe')
  ELSE
  { HP-UX }
    LET Awm_call_viewer_script  (Wmdt_dir + Lang_dir + '/bin/call_help.sh')
  END_IF

  INPUT 'callhelp.m'

  Awmc_init_me_online_help

  LET Wmdt_help_enabled (TRUE)

END_DEFINE

{***************************}
{* Macro to enable Demo-DB *}
{***************************}
DEFINE Enable_default_schema

  LET Awmc_val_max_change_notes   8
  LET Awmc_val_tbname_prefix  Awm_db_demodb_tbname_prefix

  LET Awmc_val_tb_partclass   Awm_dbr_demodb_part_class
  LET Awmc_val_tb_docclass    Awm_dbr_demodb_doc_class

  LET Awmc_val_tb_class       Awm_dbr_demodb_tb_class

  IF (Wmdt_bom_enabled)
    LET Awmc_bom_val_bfg_class  Awm_dbr_demodb_bom_flag_class
    LET Awm_bom_val_flag_layout_a Awm_db_demodb_bom_flag_a
    LET Awm_bom_val_flag_layout_b Awm_db_demodb_bom_flag_b
    LET Awm_bom_val_flag_layout_c Awm_db_demodb_bom_flag_c
    LET Awm_bom_val_bom_layout_a Awm_db_demodb_bom_lay_a
    LET Awm_bom_val_bom_layout_b Awm_db_demodb_bom_lay_b
    LET Awm_bom_val_bom_layout_c Awm_db_demodb_bom_lay_c

    LET Awm_bom_val_link_src_attr "LINK_SRC"
  END_IF

  IF (Annotation_active)
    {* Speichern eines TB in einer SD-Drawing erlauben *}
    {***************************************************}
    LET Awmc_val_store_with_tb TRUE
  END_IF

END_DEFINE
