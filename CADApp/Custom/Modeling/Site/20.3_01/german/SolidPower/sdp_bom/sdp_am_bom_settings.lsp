(in-package :TS-AM-STL)

(use-package :custom)
(use-package :oli)

(defvar *ts-am-stl-posnr-inkrement*)
(setf *ts-am-stl-posnr-startwert* "10")

(defvar *ts-am-stl-posnr-inkrement*)
(setf *ts-am-stl-posnr-inkrement* 10)

(defvar *ts-am-stl-zeich-pos*)
(setf *ts-am-stl-zeich-pos* "LOWER_RIGHT")

(defvar *ts-am-stl-zeich-akt-lay*)
(setf *ts-am-stl-zeich-akt-lay* "neue_herbold")

(defvar *ts-am-stl-flag-akt-lay*)
(setf *ts-am-stl-flag-akt-lay* "Layout1")

(defvar *ts-am-stl-zeich-dir*)
(setf *ts-am-stl-zeich-dir* :GROW-UPWARD)

(defvar *ts-am-stl-refline-color*)
(setf *ts-am-stl-refline-color* (make-gpnt3d :x 0.0 :y 1.0 :z 0.0))

(defvar *ts-am-stl-refline-size*)
(setf *ts-am-stl-refline-size* 3.5)

(defvar *ts-am-stl-refline-type*)
(setf *ts-am-stl-refline-type* "DOT_TYPE")

(export '(
          *ts-am-stl-posnr-startwert*
          *ts-am-stl-posnr-inkrement*
          *ts-am-stl-zeich-pos*
          *ts-am-stl-zeich-dir*
          *ts-am-stl-zeich-akt-lay*
          *ts-am-stl-refline-color*
          *ts-am-stl-refline-size*
          *ts-am-stl-refline-type*
         )
)
