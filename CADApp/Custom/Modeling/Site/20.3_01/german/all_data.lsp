; ----------------------------------------------------------------------------------------
; SVN:  $Id: all_data.lsp 166 2010-05-12 04:49:53Z pjahn $
; persistent data customization file for all modules of creo elemtens/direct modeling 19
; Peter Jahn; TECHSOFT Datenverarbeitung GmbH; http://www.techsoft.at
; ----------------------------------------------------------------------------------------

(in-package :mei)
(persistent-data-revision "19.0")
(persistent-data-module "ALL")

;;lock toolbars=============================================
(persistent-data
 :key "LOCK TOOLBARS"
 :value 0)

;;define modules for automatic startup====================== 
(persistent-data
 :key "STARTUP MODULES"
 :value '(  "IGESDEK" "STEP" "GRANITE" "ProductView" "ACIS_SAT" "STL" "CADCAM_LINK" 
           "BASICSHEETS" "PART_LIBRARY" "SDPOWER-STEEL") )

		
;;enable settings for search and filter
(persistent-data
 :key "PARCEL-GBROWSER"
 :value '( :BORDER-MODE NIL :RESTORE-EXPAND-STATE T :FILTER-SELECT-IN-TREE T 
           :FILTER-VP-HIGHLIGHT T :SEARCH-VP-HIGHLIGHT T ) )

;; ----------- end of file -----------
