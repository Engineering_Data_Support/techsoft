;% Automatically written on 10/30/2014 at 09:07:39
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Dimension"
  :style :NH
  :title "nh"
  :values
   '("Lines/OffsetPoint" 2.0
     "Specific/Diameter/BothDiaArrows" LISP::T
     "Text/MainTolerance/SizeMode" :REL06
     "Text/MainTolerance/TextStyle" :TOLERANZ
     "Text/Postfix/TextStyle" :TOLERANZ
     "Text/Prefix/TextStyle" :TOLERANZ
     "Text/SecondTolerance/TextStyle" :TOLERANZ
     "Text/Superfix/TextStyle" :STANDARD
	 "Specific/Thread/LabelLeftHand" "links"
     "Specific/Thread/LabelRightHand" ""
     )
)
