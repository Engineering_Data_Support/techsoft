;% Automatically written on 07/15/2016 at 08:49:55
;% PTC Creo Elements/Direct Modeling Revision: 19.0 (19.0)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Plot"
  :style :A3Q_EINP
  :title "A3q_einp"
  :values
   '("PagePosition" :FIT
     "PageStyle/Fit" LISP::T
     "PageStyle/OffsetX" 0.0
     "PageStyle/OffsetY" 0.0
     "PageStyle/Position" :CENTER
     "PageStyle/Rotation" 0.0
     "PageStyle/Scale" 1.0
     "PatternLength" :STANDARD
     "PatternLengthStyle/CenterDashDash" 5.0
     "PatternLengthStyle/DashCenter" 5.0
     "PatternLengthStyle/Dashed" 5.0
     "PatternLengthStyle/DotCenter" 5.0
     "PatternLengthStyle/Dotted" 5.0
     "PatternLengthStyle/LongDashed" 5.0
     "PatternLengthStyle/Phantom" 5.0
     "PenTrafo" :BLACK_WHITE
     "PensTrafoStyle/Contents" :DRAWING
     "PensTrafoStyle/Description" "Volles Farbspektrum verwenden"
     "PensTrafoStyle/InclLineSize" LISP::T
     "PensTrafoStyle/Pens" (:LABEL "Echtfarbe" :PENLIST
                              ((:OLD_LINETYPE :ALL :COLOR_DEF :ALL
                                   :COLOR1
                                   0.0,0.0,0.0 :COLOR2
                                   1.0,1.0,1.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   :OFF :PEN_NUMBER 1 :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   1.0,1.0,1.0 :COLOR2
                                   1.0,1.0,1.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   :OFF :PEN_NUMBER 1 :NEW_COLOR
                                   0.0,0.0,0.0)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   1.0,1.0,1.0 :COLOR2
                                   1.0,1.0,1.0 :PENSIZE_DEF :SINGLE
                                   :PENSIZE1 0 :PENSIZE2 0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.25 :PEN_NUMBER 1 :NEW_COLOR
                                   0.0,0.0,0.0)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   1.0,0.0,0.0 :COLOR2
                                   1.0,0.0,0.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.17999999999999999 :PEN_NUMBER 2
                                   :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   0.0,0.0,1.0 :COLOR2
                                   0.0,0.0,1.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.25 :PEN_NUMBER 5 :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   0.0,1.0,0.0 :COLOR2
                                   0.0,1.0,0.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.34999999999999998 :PEN_NUMBER 3
                                   :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   1.0,1.0,0.0 :COLOR2
                                   1.0,1.0,0.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.34999999999999998 :PEN_NUMBER 4
                                   :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   0.0,1.0,1.0 :COLOR2
                                   0.0,1.0,1.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.69999999999999996 :PEN_NUMBER 7
                                   :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   1.0,0.0,1.0 :COLOR2
                                   1.0,0.0,1.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   0.12 :PEN_NUMBER 6 :NEW_COLOR :SAME)
                               (:OLD_LINETYPE :ALL :COLOR_DEF :SINGLE
                                   :COLOR1
                                   0.0,0.0,0.0 :COLOR2
                                   0.0,0.0,0.0 :PENSIZE_DEF :ALL
                                   :PENSIZE1 0 :PENSIZE2 1000000.0
                                   :NEW_LINETYPE :SAME :NEW_PENSIZE
                                   :OFF :PEN_NUMBER 1 :NEW_COLOR
                                   0.0,0.0,0.0)))
     "PensTrafoStyle/TrueColorMode" LISP::T
     "Printer" :A3
     "PrinterStyle/Destination" ""
     "PrinterStyle/Name" :|\\\\BRAUER\\KYOCERA_FS-9130DN_KX|
     "PrinterStyle/NumberCopies" 1
     "PrinterStyle/Orientation" :LANDSCAPE
     "PrinterStyle/PaperSize" :A4
     "PrinterStyle/Properties" (:LABEL " " :TYPE :MSWINDOW_GDI_PRINTER
                                  :VALUE LISP::NIL)
     "PrinterStyle/ToFile" LISP::NIL
     "PrinterStyle/Type" :MSWINDOW_GDI_PRINTER
     "Source" :SHEET
     )
)
