;; Automatically written on 17-Mar-2021 18:06:11
;; Module SHEETADVISOR
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "SHEETADVISOR")
(persistent-data
 :key "SHEET-METAL-DEFAULTS"
 :value '( :IMPLICITLY-SET-DEFAULT-RELIEF NIL :IMPLICITLY-SET-DEFAULT-BODYCHECK 
           NIL :IMPLICITLY-SET-DEFAULT-GAP-DIST T :DEFAULT-RELIEF-TYPE 
           "rect_relief" :DEFAULT-RELIEF-TOOL ( :WIDTH 1 ) 
           :DEFAULT-MITER-RELIEF-TYPE "miter_chamfer" :DEFAULT-MITER-RELIEF-TOOL ( 
           :PERCT_DEPTH 100 ) :DEFAULT-RELIEF-DEPTH 0 :DEFAULT-CORNER-RELIEF-TYPE 
           "min_perp_v_corner_relief" :DEFAULT-CORNER-RELIEF-TOOL ( :MIN_WIDTH 1 ) 
           :DEFAULT-CORNER-GAP 0.1 :DEFAULT-CHECK-PART T ) )

;; ----------- end of file -----------
