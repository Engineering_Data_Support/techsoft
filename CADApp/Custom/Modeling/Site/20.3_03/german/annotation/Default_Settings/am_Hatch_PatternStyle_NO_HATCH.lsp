;% Automatically written on 03/17/2021 at 16:52:01
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "Annotation"
  :style-path "Annotation/Hatch/PatternStyle"
  :style :NO_HATCH
  :title "KeinSchraff"
  :values
   '("Description" "L:1, A:  0.0, D: 1000000."
     "PatternAngle" 0.0
     "PatternDistance" 1000000.0
     "SubPattern" (:LABEL "KeinSchraff" :SUBPATTERN
                     ((:COLOR 0.0,0.0,0.0 :LINETYPE :SOLID :DISTANCE
                              1.0 :OFFSET 0.0 :ANGLE 0.0 :WIDTH 0)))
     )
)
