;; Automatically written on 17-Mar-2021 18:06:11
;; Module ANNOTATION
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "ANNOTATION")
(persistent-data
 :key "TEMPLATE EXAMPLES SHOWN"
 :value '( :SURFACE T ) )
(persistent-data
 :key "VIEW DEFAULTS"
 :value '( :CREATE-DRAWING-DEFAULT-VIEWS ( :LEFT :TOP :FRONT ) :UPDATE-TYPE 
           :UPDATE_SELECTED_VIEWS ) )
(persistent-data
 :key "STARTUPTIME"
 :value '( 4 28.1680036783218384 20 ) )
(persistent-data
 :key "VP-CONFIG"
 :value '( :AUX-3D-VP ( 100.0,100.0 500.0,400.0 ) :ANNO-VP-WITH-3D ( 0.0,0.0 
           1368.0,710.0 ) :AUX-3D-VP-VISIBLE NIL :ANNO-VP ( -10.0,-33.0 1384.0,749.0 
           :MAXIMIZE ) ) )
(persistent-data
 :key "HATCH PATTERN EXAMPLES SHOWN"
 :value T)
(persistent-data
 :key "CREATE DRAWING"
 :value '( :UP_DIR 0.0,1.0,0.0 :FR_DIR 0.0,0.0,-1.0 ) )
(persistent-data
 :key "ACTIVE DEFAULT SETTING STYLES"
 :value '( ( "Annotation/General/ArrowStyle" :STANDARD ) ( 
           "Annotation/General/LineStyle" :STANDARD ) ( 
           "Annotation/View/CaptionStyle" :DETAIL_CONV1600_TECHSOFT ) ( 
           "Annotation/Plot/PrinterStyle" :A0 ) ( "Annotation/View/FormatStyle" 
           :DET_REF ) ( "Annotation/General/TextStyle" :STANDARD ) ( 
           "Annotation/Dimension" :NH ) ( "Annotation/Plot" :A0 ) ( 
           "Annotation/General/SizeStyle" :STANDARD ) ( "Annotation/Plot/PageStyle" 
           :FACTOR1 ) ( "Annotation/Dimension/Text/FormatStyle" :MM ) ( 
           "Annotation/Refline" :NH ) ( "Annotation" :NH ) ) )

;; ----------- end of file -----------
