;; Automatically written on 17-Mar-2021 18:06:11
;; Module SOLIDPOWER
(in-package :mei)
(persistent-data-revision "20.3")
(persistent-data-module "SOLIDPOWER")
(persistent-data
 :key "SOLIDPOWER_SD_BOM"
 :value '( :MASS_ATTRIBUTE "GEWICHT" :MASS_UNITS :KG :MASS_DIGITS 4 
           :MASS_TOLERANCE 0.1 :ROUGH_MACHINED_DIM 2 :ROUGH_MACHINED_ATTRIBUTE1 
           "ROHMASZ" :ROUGH_MACHINED_ATTRIBUTE2 NIL :ROUGH_MACHINED_ATTRIBUTE3 
           NIL :ROUGH_MACHINED_UNITS :MM :ROUGH_MACHINED_DIGITS 0 
           :ROUGH_ADD_VALUE 0 :DENSITY_TOLERANCE 0.1 ) )
(persistent-data
 :key "LAST_DRILL_UPPER_SINK_STANDARD"
 :value '( :MITTEL_0 "Keine" :GROB_1 "Keine" :FEIN_1 "H1" :MITTEL_1 "Keine" ) )
(persistent-data
 :key "SOLIDPOWER_AM_BOMAUTO"
 :value '( :CURR_BOM_FLAG_LAYOUT "ISO" :ALIGN_DISTANCE 10 :ALIGN_CHANGE_POS T 
           :RASTER_X 10 :RASTER_Y 10 :RASTER_UPPER_POINTS T :RASTER_LOWER_POINTS 
           NIL :RASTER_RIGHT_POINTS T :RASTER_LEFT_POINTS NIL :KEEP_RASTER T 
           :AUTO_CHANGE_POS T ) )
(persistent-data
 :key "SOLIDPOWER_SCREWS"
 :value '( :SCREW "normteile/schrauben/sechskantschrauben/din_933" :DIAMETER 12 
           :NUT1 "normteile/muttern/din6924" :NUT1_NAME 
           "Sechskantmutter_M12_DIN_6924" :NUT2 "" :NUT2_NAME "" :ACC_HEAD1 "" 
           :ACC_HEAD1_NAME "" :ACC_HEAD2 "" :ACC_HEAD2_NAME "" :ACC_NUT1 "" 
           :ACC_NUT1_NAME "" :ACC_NUT2 "" :ACC_NUT2_NAME "" :HOLE_D 13.5 
           :UPPER_SINK_D1 20 :UPPER_SINK_D2 0 :UPPER_SINK_T1 12.8000000000000007 
           :UPPER_SINK_T2 0 :UPPER_SINK_ANGLE 0 :UPPER_SINK_STANDARD "" 
           :LOWER_SINK_D1 0 :LOWER_SINK_D2 0 :LOWER_SINK_T1 0 :LOWER_SINK_T2 0 
           :LOWER_SINK_ANGLE 0 :LOWER_SINK_STANDARD "" :UPPER_SINK_TYPE 0 
           :LOWER_SINK_TYPE 0 :THREAD_DESC "" :THREAD_TABLE "entfaellt" 
           :MACHINING "Mittel" ) )
(persistent-data
 :key "LAST_DRILL_LOWER_SINK_STANDARD"
 :value '( :MITTEL_5 "Keine" :MITTEL_6 "Keine" :MITTEL_10 "Keine" :GROB_0 
           "Keine" :FEIN_0 "Keine" :MITTEL_0 "Keine" ) )
(persistent-data
 :key "SOLIDPOWER_LAST_STANDARD_PART"
 :value '( :TYP "PARAMETER" :PARA1 "normteile/profile/rechteck_profile/DIN1017" 
           :PARA2 "0000000000" :PARA3 
           " 'NAME' 'PNR' 'TYPE' 'BEZEICHNUNG' 'B' 'H' 'DESCRIPTION_ENGLISH' 'DELETE' 'FAVORITE'" 
           :PARA4 
           " 'DIN_1017_35x5' '00002038' 'DIN 1017 35x5' 'Stahlbauprofil DIN 1017 35x5' '35' '5' 'DIN 1017 35x5' '' ''" 
           :PARA5 " 'L'" :PARA6 " '230'" ) )
(persistent-data
 :key "TS-GEWFREI-FREIST-BOLZEN-DIALOG"
 :value '( :VALUES ( :BEZ "M3" ) ) )
(persistent-data
 :key "SOLIDPOWER_DRILL"
 :value '( :SELECT_BY T :DIA 8 :SCREW_DIA "M20" :MACHINING "Mittel" :UPPER_TYPE 
           0 :TECH_STANDARD "Keine" :D1 36 :D2 12 :DEPTH1 16.5 :DEPTH2 2 :ANGLE 
           90 :LOWER_TYPE 0 :TECH_STANDARD_LOWER "Keine" :D1_LOWER 14 :D2_LOWER 
           12 :DEPTH1_LOWER 10 :DEPTH2_LOWER 2 :ANGLE_LOWER 90 :THREAD "" 
           :THREAD_TYPE "Entfällt" :THREAD_HAND "Rechtsgewinde" :TOLERANCE_TYPE 
           "NONE" :TOLERANCE_STR "H5" :TOLERANCE_VAL1 0.1 :TOLERANCE_VAL2 0.1 
           :TOLERANCE_VAL3 -0.1 :CAM_INFO NIL ) )

;; ----------- end of file -----------
