;% Automatically written on 03/17/2021 at 18:06:11
;% Creo Elements/Direct Modeling Revision: 20.3 (20.3)

(oli:sd-set-setting-values
  :application "SolidDesigner"
  :style-path "SolidDesigner/ColorSchemes"
  :style :COCREATE17_0
  :title "CoCreate"
  :values
   '("3DObjects/2dConstructionColor" 16711935
     "3DObjects/2dGeometryColor" 16777215
     "3DObjects/CurrentPartColor" 65280
     "3DObjects/PartBaseColor" 10066329
     "3DObjects/PartFaceColor" 4243417
     "3DObjects/WPBackBorderColor" 10066367
     "3DObjects/WPBackCurrentColor" 48896
     "3DObjects/WPFrontBorderColor" 13421823
     "3DObjects/WPFrontCurrentColor" 327560
     "Viewport/ClipHatchColor" 0
     "Viewport/ClipLineColor" 0
     "Viewport/SelectionColor" 15373056
     )
)
