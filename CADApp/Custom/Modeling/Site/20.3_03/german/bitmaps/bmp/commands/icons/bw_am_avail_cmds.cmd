﻿; Folgende Sonderzeichen entsprechen folgenden Umlauten:
; Ï=ü
; Ì=ä
; Î=ö
; Ø=Ä
; Ú=Ö
; Û=Ü
; Þ=ß
(:Application "All")
 
;;----- SolidDesigner:  --------------------------------------------
;;----- Annotation:  --------------------------------------------
(:Application "Annotation")

(:Group "Borgware" :title "Borgware")
(  "a4"
   :title "a4"
   :image "/icons/bw"
   :action "(BW-package::a4_plot)"
   :description "a4"
)

(  "a4_eg"
   :title "a4"
   :image "/icons/bw"
   :action "(BW-package::a4_plot_eg)"
   :action "(am_plot_ex :PLOT_STYLE :LASER_A4_EG_QUER)"
   :description "a4"
)

(  "a4q"
   :title "a4q"
   :image "/icons/bw"
   :action "(BW-package::a4q_plot)"
   :description "a4q"
)

(  "a3"
   :title "a3"
   :image "/icons/bw"
   :action "(BW-package::a3_plot)"
   :description "a3"
)

(  "a2"
   :title "a2"
   :image "/icons/bw"
   :action "(BW-package::a2_plot)"
   :description "a2"
)

(  "a1"
   :title "a1"
   :image "/icons/bw"
   :action "(BW-package::a1_plot)"
   :description "a1"
)

(  "a0"
   :title "a0"
   :image "/icons/bw"
   :action "(BW-package::a0_plot)"
   :description "a0"
)

(  "a00"
   :title "a00"
   :image "/icons/bw"
   :action "(BW-package::a00_plot)"
   :description "a00"
)

(  "plot_bs"
   :title "plot_bs"
   :image "/icons/plot"
   :action "plot_bs"
   :description "plot_bs"
)

(  "bw_plot_sheets"
   :title "PDF+DXF"
   :image "/icons/dxf"
   :action "bw_plot_sheets"
   :description "DXF und PDF Ausgabe aller BlÌtter."
)

(  "bw_change"
   :title "bw_change"
   :image "/icons/bw"
   :action "bw_change"
   :description "bw_change"
)

(  "bw_dims"
   :title "Bemassungen nummerieren"
   :image "/icons/tabelle"
   :action "bw_dims"
   :description "Nummeriert Bemassungen am aktuellen Blatt."
)

(  "bw_cn"
   :title "Änderungsnotizen"
   :image "/icons/referenzen"
   :action "bw_cn"
   :description "Ausfuellen der Aenderungsnotizen."
)

(  "bw_hinweis"
   :title "Text+Hinweis+Knick"
   :image "/icons/text_refline"
   :action "bw_hinweis"
   :description "Erstellt Text und Hinweislinie mit Knick."
)
(  "bw_hinweis_ohne_knick"
   :title "Text+Hinweis"
   :image "/icons/text_refline"
   :action "bw_hinweis_ohne_knick"
   :description "Erstellt Text und Hinweislinie ohne Knick."
)

(  "bw_plot"
   :title "PDF Erstellen"
   :image "/icons/pdf"
   :action "bw_plot"
   :description "Erstellt PDF Datei."
)

(  "bw_infostamp"
   :title "Infostempel"
   :image "/icons/plotstamp"
   :action "bw_infostamp"
   :description "Stempelt Blatt."
)

("bw_posnum_dialog"
   :title "Positionsnummer"
   :image "/icons/pos"
   :action "bw_posnum_dialog"
   :description "Erstellt eine Positionsnummer."
)  
  
("bw_reference"
   :title "Schriftkopf"
   :image "/icons/referenzen"
   :action "bw_reference"
   :description "Schriftkopf ausfuellen."
)

("bw-teilkreis"
   :title "Teilkreis"
   :image "/icons/teilkreis"
   :action "bw-teilkreis"
   :description "Teilkreis erstellen."
)

("Module"
   :title "Module"
   :image "/icons/bw"
   :action "(UI::UIC-SHOW-MODULE-CONTROLLER)"
   :description "Modul"
) 

 ("Teamviewer starten"
 :title      (ui::multi-lang "Start Teamviewer" :german "Teamviewer starten")
 :action      "(oli::sd-sys-background-job (format nil \"\\\"~a\\\\\\bw_tools\\\\addons\\\\teamviewer\\\\TeamViewerQS_de-idc2gqabsh.exe\\\"\" (oli::sd-sys-getenv \"BASEDIR\")))"
 :description  (ui::multi-lang "Start the teamviewer client to collaborate with a BORGWARE support engineer" :german "Startet eine Teamviewer Sitzung")
 :image       "icons/teamviewer"
:ui-behavior :DEFAULT) 
 
("BORGWARE Support per E-Mail"
 :title      "BORGWARE Support via E-Mail"
:action      "(oli::sd-sys-background-job (format nil \"explorer.exe \\\"mailto:support@borgwareplm.de?subject=Supportanfrage zu ~a\\\"\" (oli::sd-sys-getenv \"LINKNAME\") (oli::sd-sys-getenv \"TEMP\")))"
 :description (ui::multi-lang "Contact a BORGWARE support engineer by email" :german "BORGWARE Support via E-Mail kontaktieren")
 :image       "icons/email"
:ui-behavior :DEFAULT) 
 
 ("BORGWAREWWW"
 :title 	  "BORGWAREPLM Webseite"
 :image       "icons/www"
 :action      "(oli::sd-display-url \"http://www.borgwareplm.de\")"
 :description (ui::multi-lang "Show Borgwareplm Website" :german "Webseite der Borgwareplm GmbH")
 :ui-behavior :DEFAULT) 
 
 ("SDCORPCUSTOMIZEDIR anzeigen"
 :title      "Show SDCORPCUSTOMIZEDIR"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDCORPCUSTOMIZEDIR\")))"
 :description "Show the directory for corporate wide settings"
 :image       "icons/corp"
:ui-behavior :DEFAULT) 
 
 ("SDSITECUSTOMIZEDIR anzeigen"
 :title      "Show SDSITECUSTOMIZEDIR"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDSITECUSTOMIZEDIR\")))"
 :description "Show the directory for site wide settings"
 :image       "icons/site"
:ui-behavior :DEFAULT) 
 
 ("SDUSERCUSTOMIZEDIR anzeigen"
 :title      "SDUSERCUSTOMIZEDIR anzeigen"
 :action      "(oli::sd-sys-background-job (format nil \"%windir%\\\\explorer.exe \\\"~a\\\"\" (oli::sd-sys-getenv \"SDUSERCUSTOMIZEDIR\")))"
 :description "Show the directory for user defined settings"
 :image       "icons/user"
:ui-behavior :DEFAULT) 
 
("License Server auflisten"
 :title      "License Server auflisten"
 :action      "(frame2-ui::display (oli::sd-sys-getenv \"LICENSESERVER\"))"
 :description "Konfigurierte Lizenzserver anzeigen"
 :image       "icons/licenseserver"
:ui-behavior :DEFAULT) 
 
( "PTC Online Documentation"
 :title      "PTC Online Documentation"
 :action      "(oli::sd-display-url \"http://www.ptc.com/cs/help/creo_elements_direct_hc/modeling_190_hc/\")"
 :description "PTC Online Dokumentation - Öffnet die Online Dokumentation zu Creo Elements/Direct Modeling von der PTC Website (Internet-Verbindung erforderlich)"
 :image "icons/ptc"
)
 
("PTC Online Tutorials"
 :title "Online Tutorials"
 :action      "(oli::sd-display-url \"http://learningexchange.ptc.com/tutorials/listing/product_version_id:81\")"
 :description "PTC Online Tutorials - Übersicht über Creo Elements/Direct Modeling Tutorials von der PTC Website (Internet connection required)"
 :image "icons/ptcu"
)

("Black Design"
 :title       "Schwarz"
 :action      "(uib::win-set-skin-style :black)"
 :description "Switch user interface to black design"
 :image        "icons/black_design"
)
 
("Silver Design"
 :title      "Silber"
 :action      "(uib::win-set-skin-style :silver)"
 :description "Switch user interface to silver design"
 :image        "icons/silver_design"
)

("Blue Design"
 :title       "Blau"
 :action      "(uib::win-set-skin-style :blue)"
 :description "Switch user interface to blue design"
 :image        "icons/blue_design"
)
 
("Creo Design"
 :title      "Creo"
 :action      "(uib::win-set-skin-style :creo)"
 :description "Switch user interface to Creo design"
 :image        "icons/creo_design"
)

(  "bw_plot_all_sheets"
   :title "Alles Plotten"
   :image "/icons/plot"
   :action "bw_plot_all_sheets"
   :description "Plott Ausgabe aller BlÌtter."
)

("Switch Annotation Color Scheme"
  :title       "Switch Annotation Color Scheme"
 :action      "(ts-switch-background-color)"
 :description "Wechsel zwischen Creo und CoCreate Farbscheme in Annotation."
 :image "/icons/switch"
)
