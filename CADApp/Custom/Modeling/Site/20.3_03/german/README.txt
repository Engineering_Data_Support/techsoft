﻿README-Datei für Anpassung

Diese Datei enthält eine Tabelle, die den Anwendungsnamen im betreffenden
Unterverzeichnis der Anpassungen und im Präfix der Anpassungsdatei abbildet.

Produktname                          | Unterverzeichnis        | Präfix | (Interner Name)
-----------------------------------------------------------------------------
Creo Elements/Direct Modeling        |                         | sd     | (SolidDesigner)
Annotation                           | ANNOTATION              | am     | (Annotation)
Sheet Metal                          | SheetAdvisor            | sha    | (SheetAdvisor)
3D Documentation                     | 3D_DOCUMENTATION        | d3d    | (3D Documentation)
Rendering                            | Rendering               | rdr    | (Rendering)
3D Library                           | SOLIDLIB                | sl     | (SolidLibrary)
Mold Design                          | MoldDesignAdvisor       | md     | (Mold Design Advisor)
Mold Base                            | MoldBaseAdvisor         | mb     | (Mold Base Advisor)
Design Data Management               | PEWMSD                  | wm     | (PEWMSD)
Windchill WGM                        | UWGM                    | uwgm   | (WINDCHILL_INTEGRATION)
Model Manager                        | ModelManager            | mm     | (ModelManager)
AdvAssembly Parametrics              | AdvAssembly_Parametrics | rel    | (AdvAsmb_Parametrics)
Animation                            | AdvAssembly_Parametrics | ani    | (Animation)
Finite Element Analysis              | DesignAdvisor           | da     | (DesignAdvisor)
Machining                            | MachiningAdvisor        | ma     | (CADCAM_LINK)
Printed Circuit Board                | sdpcb                   | pcb    | (PCB I/F)
Design Simplification                | DesignSimp              | ds     | (DESIGN_SIMPLIFICATION)
Inspection                           | INSPECTION              | ins    | (INSPECTION)
Step                                 | STEP                    | stp    | (STEP)
HR Dump                              | hrdump                  | hrd    | (hrdump)
Dispatcher                           | Dispatcher              | dis    | (Dispatcher)
Custom Features                      | DESIGNINFO              | dif    | (DESIGNINFO)
Direct Links                         | DirectLinks             | dl     | (Direct_Links)
Granite                              | Granite                 | gnt    | (Granite)
Product View                         | ProductView             | pv     | (ProductView)
