﻿DEFINE_ENCODING 'UTF-8'
{#####################################################
 Borgware GmbH
 Hauptstraße 8
 D-72401 Haigerloch-Owingen
 Tel +49 (7474) 698-0
 www.borgware.de
 $Date: 2014-04-28 14:14:01 +0200 (Mo, 28 Apr 2014) $
 $Revision: 47 $
 $Author: uli.aichholz $
 $HeadURL: https://bwsvn.borgware.local/svn/Standards/18.1/trunk/ccm18_10/drafting18.1/config18.m $
#####################################################}
{WHITE|YELLOW|GREEN|CYAN|MAGENTA|BLACK|RED|BLUE}
{SOLID|DOTTED|DOT_CENTER|DASH_CENTER|DASHED|LONG_DASHED|PHANTOM}


DEFINE Init_drafting_config
	INQ_ENV 0					{ Prueft ob OSD Drafting / ME30	}
	LET Progname  (SUBSTR (INQ 301) 4 4)

	CONFIGURE_EDITOR						'$' 1 79	{ OSD Drafting-Editor konfigur.	}
	RECALL_BUFFER							ON			{ Buffer einschalten		}
	{ PROMPT_LIST CLEAR PROMPT_LIST	ON}					{ Promptliste CLEAR/ON		}
	PRE_VIEW								ON			{ Vorschau ON/OFF		}
	MAX_FEEDBACK							1000		{ Anzahl Elem. f. aendern	}
	UNITS 1									MM			{ Laengeneinheit		}
	UNITS 1									DEG			{ Winkeleinheit			}
	CS_AXIS									1,0 0,1		{ Koordinatenachsen		}
	CS_REF_PT								0,0			{ Nullpt. Koordinatensys.	}
	CURSOR_COORDINATES						ON			{ Anzeige der Koordinaten	}
	CURSOR									SMALL		{ Cursor klein/gross		}
	ARROW_CURSOR							ON			{ Cursor auf Menue		}
	FOLLOW									OFF			{ Ursprung folge ein/aus	}
	GRID_FACTOR								10			{ Gitterabstand			}
	ORIGIN									ALL OFF		{ Ursprung aus			}
	RULER									ALL	OFF		{ Gitter/Lineal aus		}
	LINE									WHITE SOLID END	{ Geometrielinien + -typ SOLID|DOTTED|DOT_CENTER|DASH_CENTER|DASHED|LONG_DASHED|PHANTOM	}
 PENSIZE									0			{ Geometriestiftdicke		}
	LINEWIDTH								0			{ Geometrieliniendicke		}
	PLOT_STOP_ON_ERROR ON
	LEADER_ARROW							1.5 ARROW_TYPE	{ Hinw.- +Refl.Begrenzg.	ARROW_TYPE|DOT_TYPE|TRIANGLE_TYPE|SLASH_TYPE|JIS_TYPE|NONE }
	CURRENT_VERTEX_COLOR					GREEN		{ Linienendpunkte		}
	CURRENT_VERTEX_COLOR USED_MULTIPLE		YELLOW		{ Scheitelpunkte		}
	CURRENT_VERTEX_COLOR CENTER				CYAN		{ Einzelner Mittelpunkt		}
	CURRENT_VERTEX_COLOR USED_MULTIPLE		CENTER MAGENTA	{ Mehrfache Mittelpunkte}
	C_LINE									RED DOTTED END	{ Hilfslinien + -typ		}
	IF (NOT Meview)
		TEXT 								GREEN END         { Textfarbe WHITE|YELLOW|GREEN|CYAN|MAGENTA|BLACK|RED|BLUE}
		TEXT_FRAME 							OFF             { Rahmen OFF | BALLOON | BOX | FLAG }
		TEXT_ANGLE 							0               { Winkel}
		TEXT_ADJUST 						1               { Textausrichtung (1-9) }
		TEXT_LINESPACE 						2.2               { Zeilenabstand (Faktor x Schriftgröße) }
		TEXT_FILL 							OFF             { Text ausgefüllt (Blockschrift) }
		TEXT_SIZE 							3.5             { Text Größe}
		TEXT_RATIO 							0.8             { Text Verhältnis Breite/Höhe}
		TEXT_SLANT 							0				{ Neigung (kursiv)		}
		RTL_SRC_GAP							2				{ Reflinie Start-Abst.		}
		RTL_DST_GAP							0				{ Reflinie End-Abst.		}
		RTL_OFFSET							2				{ Reflinie Hori.Teil(Txt)	}
		RTL_LINETYPE						SOLID			{ Reflinie Linientyp		}
		RTL_COLOR							YELLOW			{ Reflinie Farbe		}
  RTL_PENSIZE							0				{ Reflinie Stiftdicke		}
		RTL_LINEWIDTH						0				{ Reflinie Liniendicke		}
		ARROW_FILL							ON				{ Hinw.- +Refl.Pfeil fuell	}
		SL_OFFSET							2				{ Symmlinie Überstand		}
		SL_LINETYPE							DOT_CENTER		{ Symmlinie Linientyp		}
		SL_COLOR							YELLOW			{ Symmlinie Farbe		}
  SL_PENSIZE							0.00			{ Symmlinie Stiftdicke		}
		SL_LINEWIDTH						0.00			{ Symmlinie Liniendicke		}
		CL_ABS_OFFSET						0				{ Mittellinie abs.Überst.	}
		CL_REL_OFFSET						0.1				{ Mittellinie rel.Überst.	}
		CL_LINETYPE							DOT_CENTER		{ Mittellinie Linientyp		}
		CL_COLOR							YELLOW			{ Mittellinie Farbe		}
  CL_PENSIZE							0.00			{ Mittellinie Stiftdicke	}
		CL_LINEWIDTH						0.00			{ Mittellinie Liniendicke	}
		SPLINE_CONVERSION					ON				{ <4.x Splines -> B-Splin	}
		BSPL_POLYGON_FEEDBACK				OFF				{ Splines dynamisch		}
		DA_FILTER_DEL_COLOR					ALL CONFIRM
		DA_FILTER_CLEAR_GEOTYPES
		DA_FILTER_CLEAR_LINETYPES
		DA_FILTER_DEL_ORIENT				ALL CONFIRM
		DA_FILTER_DEL_PENSIZE				ALL CONFIRM
		DA_FILTER_DEL_LINESIZE				ALL CONFIRM
		DA_FILTER_SET_NAME					''
		DA_DIM_HOLE_INSERTION				ON
		DA_DIM_AUTO_STRATEGY				3
		DA_DIM_AUTO_LOC						BELOW
		SPLITTING							ON				{ Trennfunktion ein/aus		}
		TRIMMING							ON				{ Trimmfunktion ein/aus		}
		KEEP_CORNER							OFF				{ Ecke behalten ein/aus		}
	END_IF
	DRAWING_SCALE							1			{ Zeichnungsmassstab		}
	CATCH RANGE								8			{ Fangkreis interaktiv		}
	CATCH NO_VIEWPORT_RANGE					0			{ Fangkreis in Makros		}
	VIEWPORT_CATCH							ON			{ Fenster rastet ein		}
	SPOTLIGHT								ON			{ Hervorhebung ein/aus		}
	CURRENT_SPOTLIGHT_ATTR					DOTTED		{ Hervorhebungslinienart	}
 CURRENT_SPOTLIGHT_ATTR	MAGENTA {RGB_COLOR 0.8 0.4 0.2	    { Hervorhebungsfarbe	}
	VIEW									TOP			{ Ansicht von Top		}
	CHECK_WINDOW							ON			{ Max.Zoom pruefen ein/aus	}
	AUTO_NEW_SCREEN							ON			{ Autom.Bildaufbau n.Icon	}
	ON_ERROR								''			{ Errorflag zuruecksetzen	}
	ENABLE_BREAK										{ Breaktaste einschalten	}
	DISPLAY_LIST							ALL OFF		{ Anzeigeliste			}
	LET Tablet_window_auto_dyn				OFF			{ Zoom-Dynamik			}
	CHANGE_VIEWPORT_COLOR	1 BACKGROUND_COLOR BLACK END	{ Hintergrund Fenster 1	}
	CHANGE_VIEWPORT_COLOR	1 BORDER_COLOR     WHITE END	{ Rahmen      Fenster 1	}
	CHANGE_VIEWPORT_COLOR	1 TEXT_COLOR       WHITE END	{ Text        Fenster 1	}
	CHANGE_VIEWPORT_COLOR	1 CURSOR_COLOR     WHITE END	{ Cursor      Fenster 1	}
	IF (NOT Meview)
		UA_DISTANCE_GRID					1				{ Copilot Fanggitter		}
		UA_ANGLE_GRID						5				{ Copilot Fang-Winkel		}
		UA_TANGENT_CATCH_RANGE				0.25			{ Copilot Tangent-Fanggr.	}
		UA_PERPENDICULAR_CATCH_RANGE		0.1				{ Copilot Rechtw.-Fanggr.	}
		UA_CENTER_CATCH_RANGE				0.05			{ Copilot Mitten.-Fanggr.	}
		UA_DIMENSION_FEEDBACK				ON				{ Copilot Bemass. Anzeig.	}
		UA_COPILOT							ON				{ Copilot ein/aus		}
		UA_SET_CATCH_DELAY					0				{ Copilot Fangzeit Verzoeg.	}
		PART_DRW_SCALE_REF					CENTER
		FLYBY								OFF 			{ON|OFF}
		UA_SNAP_POINTS_COUNT				15				{ Anzahl Historienpunkte}
		UR_SET_MAX_STEPS					100				{ keine Begrenzung der Zurück-Schritte}
		UA_SET_SNAP_POINTS_DELAY			0
		MH_SEL_GEO							ALL SUBTRACT PARTS END	{ Teile nicht dynamisch positionieren}
		MH_MODE								ON {ON|OFF}
		MH_PRESELECT 						ON
		INQ_ENV 10
		IF ((INQ 6)=4)
			SHOW_MINI_TOOLBAR 				ON
		END_IF
		UA_TEXT_COLOR						WHITE 
		disabletextborder					 
		disablelongprefixes		
		UA_TEXT_BOX_MODE					GRAPHICAL 
		{ REDRAW_MODE PARTS_AS_BOX 16 {16 ist default 0 schaltet die Funktion ab}
		{ FLYBY_SELECT_COLOR CYAN {WHITE|YELLOW|GREEN...}
	END_IF
 


{----------------------------------------------------------------------------
- Bemassungs-Einstellungen                                                  |
----------------------------------------------------------------------------}
  DEFINE Placement				{ Plaziert Bemassung		}
    AUTO					{ automatisch			}
  END_DEFINE

  DEFINE Sm_dam_menu_company_string
    (GETENV 'ad_site')
  END_DEFINE

  DEFINE Dim_coord_settings
    dim_coord iso
    SELECT_DIM_ARROW BOTH_ARROW ABSOLUTE 3.5 ARROW_TYPE
    SELECT_DIM_ARROW FIRST_ARROW
    FILL_ON RELATIVE 0.4 DOT_TYPE 
	DIM_COORD_LEADING_ZERO ON
  END_DEFINE

  DEFINE Dim_standard_company
	   LET Da_unshow_fixtable 1		{Alle DIM Tabellen nach Auswahl schliessen}
    DA_STYLE_DEFER_UPDATE
    LET Dam_std_flag			5
    CURRENT_DIM_TEXTS MAIN_NUM ABSOLUTE	3.5				{ Hauptbem.Textgroesse	}
    CURRENT_DIM_TEXTS MAIN_TOL ABSOLUTE	3.5				{ Haupttol.Textgroesse	}
    CURRENT_DIM_TEXTS MAIN_NUM			GREEN				{ Hauptbem.Textfarbe	}
    CURRENT_DIM_TEXTS MAIN_TOL			YELLOW			{ Haupttol.Textfarbe	}
    CURRENT_DIM_TEXTS SEC_NUM			GREEN			{ Sekbem.Textfarbe	}
    CURRENT_DIM_TEXTS SEC_TOL			YELLOW			{ Sektol.Textfarbe	}
    CURRENT_DIM_TEXTS PREFIX			YELLOW				{ Praefix Textfarbe	}
    CURRENT_DIM_TEXTS POSTFIX			YELLOW				{ Postfix Textfarbe	}
    CURRENT_DIM_TEXTS SUPERFIX			YELLOW				{ Superfix Textfarbe	}
    CURRENT_DIM_TEXTS SUBFIX			YELLOW				{ Subfix Textfarbe	}
    DIM_LINES_COLOR						YELLOW			{ Masslinienfarbe	}
    DIM_TEXT_LOCATION					ABOVE			{ ABOVE | ON | BELOW Bemtextpos.bzgl.Linie	}
    DIM_MIN_SPACE						0				{ Abst.Geom.+Linien	}
    DIM_TEXT_SPACE						2				{ Abst.zwi.Mass+Linie	}
	
    CURRENT_DIM_UNITS MAIN_DIM			MM				{ Hauptbem.Laengeneinheit}
    CURRENT_DIM_UNITS MAIN_DIM			DEGREE			{ DEGREE | DEG_MIN_SEC | RADIAN | GRADIAN Hauptbem.Winkeleinheit}
    CURRENT_DIM_UNITS SEC_DIM			NONE			{ Sekbem.aus		}
    CURRENT_DIM_TEXTS MAIN_NUM FONT		"hp_i3098_v"	{ Hauptbem.Schriftart	}
    CURRENT_DIM_TEXTS MAIN_NUM RATIO	1				{ Hauptbem.Lg/Breiteverh}
    CURRENT_DIM_TEXTS MAIN_NUM SLANT	0				{ Hauptbem.Textneigung	}
    CURRENT_DIM_TEXTS MAIN_TOL FONT		"hp_i3098_v"	{ Haupttol.Schriftart	}
    CURRENT_DIM_TEXTS MAIN_TOL RELATIVE	0.6				{ Haupttol.Textgroesse	}
    CURRENT_DIM_TEXTS MAIN_TOL RATIO	1				{ Haupttol.Lg/Breiteverh}
    CURRENT_DIM_TEXTS MAIN_TOL SLANT	0				{ Haupttol.Textneigung	}
    CURRENT_DIM_TEXTS SEC_NUM  FONT		"hp_i3098_v"	{ Sekbem.Schriftart	}
    CURRENT_DIM_TEXTS SEC_NUM  RELATIVE	1				{ Sekbem.Textgroesse	}
    CURRENT_DIM_TEXTS SEC_NUM  RATIO	1				{ Sekbem.Lg/Br.verh.	}
    CURRENT_DIM_TEXTS SEC_NUM  SLANT	0				{ Sekbem.Textneigung	}
    CURRENT_DIM_TEXTS SEC_TOL  FONT		"hp_i3098_v"	{ Sektol.Schriftart	}
    CURRENT_DIM_TEXTS SEC_TOL  RELATIVE	0.6				{ Sektol.Textgroesse	}
    CURRENT_DIM_TEXTS SEC_TOL  RATIO	1				{ Sektol.Lg/Br.verh.	}
    CURRENT_DIM_TEXTS SEC_TOL  SLANT	0				{ Sektol.Textneigung	}
    CURRENT_DIM_TEXTS PREFIX   FONT		"hp_i3098_v"	{ Praefix Schriftart	}
    CURRENT_DIM_TEXTS PREFIX   RELATIVE	1				{ Praefix Textgroesse	}
    CURRENT_DIM_TEXTS PREFIX   RATIO	1				{ Praefix Lg/Breitenverh.}
    CURRENT_DIM_TEXTS PREFIX   SLANT	0				{ Praefix Textneigung	}
    CURRENT_DIM_TEXTS POSTFIX  FONT		"hp_i3098_v"	{ Postfix Schriftart	}
    CURRENT_DIM_TEXTS POSTFIX  RELATIVE	1				{ Postfix Textgroesse	}
    CURRENT_DIM_TEXTS POSTFIX  RATIO	1				{ Postfix Lg/Br.verh.	}
    CURRENT_DIM_TEXTS POSTFIX  SLANT	0				{ Postfix Textneigung	}
    CURRENT_DIM_TEXTS SUPERFIX FONT		"hp_i3098_v"	{ Superfix Schriftart	}
    CURRENT_DIM_TEXTS SUPERFIX RELATIVE	1				{ Superfix Textgroesse	}
    CURRENT_DIM_TEXTS SUPERFIX RATIO	1				{ Superfix Lg/Br.verh.	}
    CURRENT_DIM_TEXTS SUPERFIX SLANT	0				{ Superfix Textneigung	}
    CURRENT_DIM_TEXTS SUBFIX   FONT		"hp_i3098_v"	{ Subfix Schriftart	}
    CURRENT_DIM_TEXTS SUBFIX   RELATIVE	1				{ Subfix Textgroesse	}
    CURRENT_DIM_TEXTS SUBFIX   RATIO	1				{ Subfix Lg/Br.verh.	}
    CURRENT_DIM_TEXTS SUBFIX   SLANT	0				{ Subfix Textneigung	}
    CURRENT_DIM_TEXTS MAIN_ALL BRACKETS	OFF
    CURRENT_DIM_TEXTS SEC_ALL BRACKETS	OFF
    DIM_FORMAT MM '0,1'								{ Kommma st.f. mm z.B. 0.11 oder 0,00 oder 0,111 }
    DIM_FORMAT CM '0,11'								{ Kommma st.f. cm z.B. 0.11 oder 0,00 oder 0,111 }
    DIM_FORMAT MT '0,11'								{ Kommma st.f. m z.B. 0.11 oder 0,00 oder 0,111 }
    DIM_FORMAT KM '0,11'								{ Kommma st.f. km z.B. 0.11 oder 0,00 oder 0,111 }
    DIM_FORMAT INCHES					"1.000"			{ Kom.st.f. Zoll	}
    DIM_FORMAT FRACT_INCH 64			"1.000"			{ Kom.st.f. Zoll/64	}
    DIM_FORMAT FT_FR_INCH 64			"1.000"			{ Kom.st.f. Fuss,Zoll,1/64}
    DIM_FORMAT FT_FR_IN_SIGN 64			"1.000"			{ Kom.st.f. Fuss,Zoll,1/64}
    DIM_FORMAT FT_FR_IN_TEXT 64			"1.000"			{ Kom.st.f. Fuss,Zoll,1/64}
    DIM_FORMAT DEGREE					"0,11"			{ Kom.st.f. Grad	}
    DIM_FORMAT RADIAN					"0,11"			{ Kom.st.f. Radiant	}
    DIM_FORMAT GRADIAN					"0,11"			{ Kom.st.f. Gradiant	}
    DIM_FORMAT DEG_MIN_SEC				"0,11"			{ Kom.st.f. Grad,min,sec}
    DIM_TEXT_ORIENTATION				PARALLEL		{ Bemtextausrichtg.	}
    DIM_TEXT_GAP						2		{ Abst.neben Masszahl	}
    DIM_FRAME							OFF		{ Rahmen um Masszahl	}
    DIM_TEXT_FRAME_COLOR_MODE			TEXT_COLOR	{ Rahmenfarbe		}
    DIM_BROKEN							OFF		{ Zu schmale Linie ein/aus}
    DIM_DATUM_STEP						8		{ Bezugsbem.Abstufung	}
    DIM_OFFSET_POINT					2		{ Abst.Geom.+Linien	}
    DIM_CATCH_RANGE						5		{ Fangkreis Massmitte	}
    DIM_OFFSET_LINE						2		{ Abst.Masslin+Masshil.lin}
    DIM_EXTENSION_LENGTH				1		{ Ueberragendes Ende	}
    DIM_CURSOR_POSITION					LINE_POS	{ Pos.Text an Cursor	}
    CHECK_DIM_DETAIL					ON		{ Check Detail zu Top Bem.}
    DIM_PENSIZE							0
    DIM_CHAMFER_PREFIX					''
    DIM_CHAMFER_POSTFIX					'x45<degree>'
    CLEAR_PREFIX
    CLEAR_POSTFIX
    CLEAR_TOLERANCE
    CLEAR_SUPERFIX
    CLEAR_SUBFIX
    DA_DIM_COORD NO_STANDARD
    END
    DA_DIM_DIAMETER ISO
    END
    DA_STYLE_ENABLE_UPDATE
    DA_STYLE_UPDATE
	DIM_BREAK_RESTORE 					ON
    DIM_CATCH_LINES						ON
	DIM_SELECT_BY_TEXTBOX 				ON
	DIM_STAGGER_RESTORE 				ON
	DIM_UNDERLINE_EDITED 				ON
	
    Dim_coord_settings
  END_DEFINE
  IF (NOT Meview)
	Dim_standard_company
  END_IF

{----------------------------------------------------------------------------
- Hidden-Line Einstellungen                                                 |
----------------------------------------------------------------------------}
  DEFINE Sw_config_hidden_line
    HL_DEFAULT_FACE_COLOR		RGB_COLOR 0.184313725 0.184313725 0.392156863
    SHOW GLOBAL INFOS 'HIDDEN: 1'	ON	{ Info-Attribut HIDDEN: 1	}
    HL_SET_COLOR			GREEN	{ Farbe der Strichlierten	}
    HL_SET_KEEP_COLOR			BLACK	{ Wird nicht strichliert	}
    HL_SET_LINETYPE			DASHED	{ Linienart d.Strichliert	}
  END_DEFINE

  IF (Hidden_line_enabled=1)			{ Wenn Verd Linien ein		}
    Sw_config_hidden_line
  END_IF

{----------------------------------------------------------------------------
- Parametric-Design Einstellungen (Beinhaltet das File pd_defaults)         |
----------------------------------------------------------------------------}
  DEFINE Sw_config_parametric_design
    PD_AUTO_SYMMETRY_LINE		YES		{ Autom.Erken.Symmlin	}
    PD_AUTO_SYMMETRY_COLOR		YELLOW		{ " mit der Farbe	}
    PD_AUTO_SYMMETRY_LINETYPE		DOT_CENTER	{ " und dem Linientyp	}
    PD_AUTO_ZERO_DISTANCE_TOLERANCE	1E-6		{ Nullabstandtol.	}
    PD_AUTO_SAME_DISTANCE_TOLERANCE	1E-6		{ Gleichdistanztol.	}
    PD_AUTO_ANGLE_TOLERANCE		1E-6		{ Einwinkelungstol.	}
    PD_AUTO_TANGENT_TOLERANCE		1E-6		{ Tangentialtol.	}
    PD_RESOLVE_MERGE_TOLERANCE		1E-6		{ Verschmelzungstol.	}
    PD_SHOW_COLOR			CYAN		{ Labelfarbe		}
    PD_SHOW_LABEL_SIZE			3.5		{ Labelgroesse		}
    PD_SHOW_USE_POSTFIX			NO
    ADD_CURRENT_INFO			"PD_ZONE" END	{ Info Attribut		}
    PD_NEW_POINT_COLOR			GREEN		{ Punkt-Farbe ori.gruen	}
    PD_NEW_POINT_LINETYPE		DOT_CENTER	{ 'Punkt auf' - Linientyp}
    PD_NEW_POINT_VISIBILITY		INVISIBLE	{ 'Punkt auf' sichtbar	}
    PD_NEW_C_LINE_COLOR			GREEN		{ PD-Hilfslinie Farbe	}
    PD_NEW_C_LINE_LINETYPE		DOT_CENTER	{ PD-Hilfslinie Linienart}
    PD_NEW_C_LINE_VISIBILITY		INVISIBLE	{ PD-Hilfslinie sichtbar }
    PD_PREVIEW_COLOR			MAGENTA		{ PD-Ûbersicht Farbe	}
    PD_DEFAULT_DIM_COLOR		GREEN		{ PD-Bemassung Farbe	}
    PD_DEFAULT_DIM_TEXT_SIZE		3.5		{ PD-Bemassung Textgroesse}
    Design_intent_on					{ PD-Abhaengigk.autom. ein}
  END_DEFINE

	IF (NOT Meview)
		IF (Parametric_design_enabled = 1)			{ Wenn Parametrik ein	}
			Sw_config_parametric_design
		END_IF
	END_IF



  INQ_ENV 0
  IF ((INQ 2)<10.5)
    LOAD_FONT				'hp_blk_v.fnt'	{ Zus. Fonts laden	}
    LOAD_FONT				'hp_blk_c.fnt'	{ Zus. Fonts laden	}
    LOAD_FONT				'hp_d17_v.fnt'	{ Zus. Fonts laden	}
    LOAD_FONT				'hp_d17_c.fnt'	{ Zus. Fonts laden	}
    LOAD_FONT				'i3098_v.fnt'	{ Zus. Fonts laden	}
    LOAD_FONT				'i3098_c.fnt'	{ Zus. Fonts laden	}
  END_IF
  CURRENT_FONT				'hp_i3098_v'	{ Aktueller Textfont	}


{----------------------------------------------------------------------------
- Schraffur-Definitionen (Definiert Standardwerte aus dem File defaults um) |
----------------------------------------------------------------------------}
	IF (NOT Meview)
		HATCH					CYAN SOLID END	{ Farbe			}
		TEXT_HOLE_INSERTION 	OFF				{ Rahmen um eingefügten Text }
		HATCH_DIST				5				{ Abstand		}
		HATCH_ANGLE				45				{ Winkel		}
		HATCH_REF_PT			0,0				{ Schraffur-Referenzpunkt}
		CURRENT_HATCH_PATTERN	NONE			{ Aktuelles Muster	}
		USE_MULTILINE_HATCH		ON				{ Ein = besser bei Abst.0}
	END_IF
	
  DEFINE I_hatch_iron					{ Muster Eisen		}
    HATCH_ANGLE				45		{ Schraffurwinkel	}
    HATCH_DIST				15		{ Schraffurabstand	}
    LET Versatz_1			0
    LET Abstand_1			1
    LET Relwink_1			0
    LET Versatz_2			(1/3)
    LET Abstand_2			1
    LET Relwink_2			0
    CURRENT_HATCH_PATTERN
    Versatz_1 Abstand_1 Relwink_1 GREEN SOLID
    Versatz_2 Abstand_2 Relwink_2 GREEN SOLID
    CONFIRM HATCH
  END_DEFINE

  DEFINE I_hatch_steel					{ Muster Stahl		}
    HATCH_ANGLE				45		{ Schraffurwinkel	}
    HATCH_DIST				5		{ Schraffurabstand	}
    LET Versatz_1			0
    LET Abstand_1			1
    LET Relwink_1			0
    CURRENT_HATCH_PATTERN
    Versatz_1 Abstand_1 Relwink_1 GREEN SOLID
    CONFIRM HATCH
  END_DEFINE

  DEFINE I_hatch_copper					{ Muster Kupfer		}
    HATCH_ANGLE				45		{ Schraffurwinkel	}
    HATCH_DIST				10		{ Schraffurabstand	}
    LET Versatz_1			0
    LET Abstand_1			1
    LET Relwink_1			0
    LET Versatz_2			0.5
    LET Abstand_2			1
    LET Relwink_2			0
    CURRENT_HATCH_PATTERN
    Versatz_1 Abstand_1 Relwink_1 GREEN SOLID
    Versatz_2 Abstand_2 Relwink_2 GREEN DASHED
    CONFIRM HATCH
  END_DEFINE

  DEFINE I_hatch_kunstst				{ Kunststoff		}
    HATCH_ANGLE				0		{ Schraffurwinkel	}
    HATCH_DIST				1		{ Schraffurabstand	}
    LET Versatz_1			0
    LET Abstand_1			1
    LET Relwink_1			0
    LET Versatz_2			(1/3)
    LET Abstand_2			3
    LET Relwink_2			45
    CURRENT_HATCH_PATTERN
    Versatz_1 Abstand_1 Relwink_1 GREEN SOLID
    Versatz_2 Abstand_2 Relwink_2 GREEN SOLID
    CONFIRM HATCH
  END_DEFINE

  DEFINE I_hatch_sch60					{ Schleifscheibe schraeg}
    HATCH_ANGLE				60		{ Schraffurwinkel	}
    HATCH_DIST				1		{ Schraffurabstand	}
    LET Versatz_1			0
    LET Abstand_1			1
    LET Relwink_1			0
    CURRENT_HATCH_PATTERN
    Versatz_1 Abstand_1 Relwink_1 RED DOTTED
    CONFIRM HATCH
  END_DEFINE

  DEFINE I_hatch_sch90					{ Schleifscheibe gerade	}
    HATCH_ANGLE				90		{ Schraffurwinkel	}
    HATCH_DIST				1		{ Schraffurabstand	}
    LET Versatz_1			0
    LET Abstand_1			1
    LET Relwink_1			0
    CURRENT_HATCH_PATTERN
    Versatz_1 Abstand_1 Relwink_1 GREEN DOTTED
    CONFIRM HATCH
  END_DEFINE


{----------------------------------------------------------------------------
- Tablett-Menue-Feld-Belegung: LIN-ART mit FARBEN, FARBEN mit LIN-DICKEN    |
----------------------------------------------------------------------------}
  DEFINE Tm_linetype_solid				SOLID			Tm_color_white	END_DEFINE
  DEFINE Tm_linetype_dashed				DASHED			Tm_color_red	END_DEFINE
  DEFINE Tm_linetype_dot_center			DOT_CENTER		Tm_color_yellow	END_DEFINE
  DEFINE Tm_linetype_phantom			PHANTOM			Tm_color_cyan	END_DEFINE
  DEFINE Tm_linetype_dotted				DOTTED			Tm_color_red	END_DEFINE
  DEFINE Tm_linetype_long_dashed		LONG_DASHED		Tm_color_green	END_DEFINE
  DEFINE Tm_linetype_dash_center		DASH_CENTER		Tm_color_yellow	END_DEFINE
  DEFINE Tm_linetype_center_dash_dash	CENTER_DASH_DASH	Tm_color_cyan	END_DEFINE

  DEFINE Tm_color_black					BLACK	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_white					WHITE	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_red					RED	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_yellow				YELLOW	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_green					GREEN	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_cyan					CYAN	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_magenta				MAGENTA	LINEWIDTH 0.00	END_DEFINE
  DEFINE Tm_color_blau					BLUE	LINEWIDTH 0.00	END_DEFINE


{----------------------------------------------------------------------------
-                         OSD Drafting 8.0 erweiterungen                    |
----------------------------------------------------------------------------}
	CMD_BG_COLOR		rgb_color 0.571 0.571 0.667	{ Commandline BG Color	}
	CMD_TXT_COLOR		WHITE				{ Commandline Text Color}
	IF (NOT Meview)
		AUTO_STORE_TIME	60				{ Automatische Sicherung}
	END_IF
	INQ_ENV 10
  IF (NOT((INQ 3)=7)) { Maus } 
	IF ((INQ 6)=1) { Classic }
		{ Nur fuer Mouse version}
		{ DEFINE_MOUSE_KEY 2 SHIFT ('EDIT_PART PARENT'#M)}
		{ DEFINE_MOUSE_KEY 3 SHIFT ('PRT_EDITOR'#M)}
		{ DEFINE_MOUSE_KEY 2 PLAIN ('EDIT_PART'#M)}
		{ DEFINE_MOUSE_KEY 3 PLAIN ('EDIT_PART PARENT'#M) }
		DEFINE_MOUSE_KEY 1 SHIFT ('Tm_window_new' + CHR 13)
		DEFINE_MOUSE_KEY 2 SHIFT ('window fit' + CHR 13)
		DEFINE_MOUSE_KEY 3 SHIFT ('WINDOW ZOOM 0.7' + CHR 13)
	END_IF
  END_IF

{ MOUSE_INTERACTION DRAFTING_MODE 						{ DRAFTING_MODE | MODELING_MODE | CREO_MODE }
{ SET_DYN_MOUSE_OLD_ZOOM_MODE OFF 						{ zoom wie in solid }
{----------------------------------------------------------------------------
-                         OSD Drafting 10.5 erweiterungen                   |
----------------------------------------------------------------------------}
    USE_NEW_FILE_FEATURES	ON		{ Erhaltung der Gr-/Kl-schreibung} 
{----------------------------------------------------------------------------
-                         OSD Drafting 11.0 erweiterungen                   |
----------------------------------------------------------------------------}
IF (NOT Meview)
  PRP_ATTR PRP_CL		YELLOW		{ Proj Bez.Punkt Schnittlinie	}
  PRP_ATTR PRP_CL		SOLID		{ Proj Bez.Punkt Schnittlinie	}
  PRP_ATTR PRP_CL		0		{ Proj Bez.Punkt Schnittlinie	}
  PRP_ATTR PRP_CL		LENGTH 10	{ Proj Bez.Punkt Schnittlinie	}
  PRP_ATTR PRP_CL		VISIBLE		{ Proj Bez.Punkt Schnittlinie	}
  PRP_ATTR PRP_CIRCLE		YELLOW		{ Proj Bez.Punkt Kreis		}
  PRP_ATTR PRP_CIRCLE		SOLID		{ Proj Bez.Punkt Kreis		}
  PRP_ATTR PRP_CIRCLE		0		{ Proj Bez.Punkt Kreis		}
  PRP_ATTR PRP_CIRCLE		RADIUS 1	{ Proj Bez.Punkt Kreis		}
  PRP_ATTR PRP_CIRCLE		VISIBLE		{ Proj Bez.Punkt Kreis		}
  PRP_ATTR PRP_PL		YELLOW		{ Proj Bez.Punkt Linienverlaeng	}
  PRP_ATTR PRP_PL		SOLID		{ Proj Bez.Punkt Linienverlaeng	}
  PRP_ATTR PRP_PL		0		{ Proj Bez.Punkt Linienverlaeng	}
  PRP_ATTR PRP_PL		INVISIBLE	{ Proj Bez.Punkt Linienverlaeng	}
END_IF
  BEEPER		ON			{ Beep ON/OFF			}

  INQ_ENV 10
  IF ((INQ 19)=1)
    WUI_CLASSIC		OFF			{ WUI 2D (ON) 3D (OFF)		}
    BUTTON_TRIGGER	ON_PUSH			{ Ausloesemodus Druecken/Loslassen Maustaste ON_PUSH|ON_RELEASE}
  END_IF


{----------------------------------------------------------------------------
- Systemfenster und dyn. Zoom Definition (Definiert Standardwerte aus dem   |
  File defaults um)                                                         |
----------------------------------------------------------------------------}
  AUTO_REDRAW		ON			{ Auto Akt Systemfensters	}
  REDRAW_USE_DOUBLE_BUFFER	ON		{ Vermeidung Flimmereffekte	}
  REDRAW_MODE		PARTS_AS_BOX 16		{ Zoom Teil als Box		}
  REDRAW_MODE		PARTS_AS_BOX MAGENTA	{ Zoom Teil als Box		}
{ REDRAW_MODE		DYNAMIC ALL LINES ON}	{ Zoom dyn. Anzeige f. Linien	}
{ REDRAW_MODE		DYNAMIC ALL ARCS ON}	{ Zoom dyn. Anzeige f.		}
{ REDRAW_MODE		DYNAMIC ALL CIRCLES ON}	{ Zoom dyn. Anzeige f. Kreise	}
{ REDRAW_MODE		DYNAMIC ALL TEXTS ON}	{ Zoom dyn. Anzeige f. Texte	}
{ REDRAW_MODE		DYNAMIC ALL SPLINES ON}	{ Zoom dyn. Anzeige f. Splines	}
{ REDRAW_MODE		DYNAMIC ALL BSPLINES ON}{ Zoom dyn. Anzeige f. BSplines	}
{ REDRAW_MODE		DYNAMIC PARTS ON}	{ Zoom dyn. Anzeige f. Teile	}
  REDRAW_MODE		DYNAMIC HATCHING OFF	{ Zoom dyn. Anzeige f. Schraf.	}
{ REDRAW_MODE		DYNAMIC ALL DIMENSIONS ON}{ Zoom dyn. Anzeige f. Bem.	}
{ REDRAW_MODE		DYNAMIC ALL PIXMAPS OFF}{ Zoom dyn. Anzeige f. Pixel	}
{ REDRAW_MODE		DYNAMIC ALL PRPS ON}	{ Zoom dyn. Anzeige f.		}




{----------------------------------------------------------------------------
- Info text namespaces Definitionen (Definiert Standardwerte aus dem File   |
  defaults um)                                                              |
----------------------------------------------------------------------------}
  REGISTER_INFO_NAMESPACE 2.78 'LAYER:*'         'SYS:LAYER_SCOPE' END
  REGISTER_INFO_NAMESPACE 2.78 'URL:*'           'SYS:URL_SCOPE' END
  REGISTER_INFO_NAMESPACE 2.78 'HIDDEN:*'        'SYS:HL_SCOPE' END
  REGISTER_INFO_NAMESPACE 2.78 'Z_LEVEL:*'       'SYS:HL_SCOPE' END
  REGISTER_INFO_NAMESPACE 2.78 'PART_Z_LEVEL:*'  'SYS:HL_SCOPE' END
  REGISTER_INFO_NAMESPACE 2.78 'FACE_COLOR:*'    'SYS:HL_SCOPE' END


{----------------------------------------------------------------------------
-                         OSD Drafting 12.0 erweiterungen                   |
----------------------------------------------------------------------------}

{----------------------------------------------------------------------------
- Plotvorschau Einstellungen (Ab Version 12.00)                             |
----------------------------------------------------------------------------}
  PP_WIN_TYPE		PP_BW		{ Type PP_ORIGINAL/PP_GRAYSCALE/PP_BW	}
  {PP_WIN_SHOW		OFF}		{ Plotvorschau ON/OFF	}
  PP_WIN_RULER		OFF		{ Lineal ON/OFF		}
  PP_WIN_SIZE		297,210		{ Vorschaugroesse	}
  PP_WIN_BG_COLOR	WHITE		{ Hintergrundfarbe	}


{----------------------------------------------------------------------------
-                         OSD Drafting 15.0 erweiterungen                   |
----------------------------------------------------------------------------}

{----------------------------------------------------------------------------
- Bemassungs Einstellungen (Ab Version 15.00)                               |
----------------------------------------------------------------------------}
IF (NOT Meview)
	DIM_OLD_ALIGNMENT	ON	{ Ausrichtung Bemassung alt }
END_IF

{----------------------------------------------------------------------------
-                         OSD Drafting 15.5 erweiterungen                   |
----------------------------------------------------------------------------}
{Linienart der Konstruktionslinien}
{(elan::set-c-geo-line-type 9)}		{Alter Linientyp vor 15.50	}
{(elan::set-c-geo-line-type 2)}		{Neuer Linientyp ab 15.50	}

  DISPLAY_NO_WAIT 'Alle Standardwerte wurden neu gesetzt! ' WAIT 1
END_DEFINE

Init_drafting_config


DEFINE Manager_tablo
INQ_ENV 10
  IF (INQ 3>1)
 (*****    Tablett Digicad A3          ********)
 TMENU Tm_cc  343.5,165    363,176      "STORE MI ALL DEL_OLD 'c:/temp/workfile'"
 TMENU Tm_cc  382,165      402,176      "LOAD 'c:/temp/workfile'"
 {TMENU Tm_cc  363,165      382,176      'M5'}
 {TMENU Tm_cc  343.5,154    363,165      'M7'}
 {TMENU Tm_cc  363,154      382,165      'M8'}
 {TMENU Tm_cc  382,154      402,165      'M9'}
 {TMENU Tm_cc  343.5,132.5  363,143.5    'M13'}
 {TMENU Tm_cc  363,132.5    382,143.5    'M14'}
 {TMENU Tm_cc  382,132.5    402,143.5    'M15'}
 {TMENU Tm_cc  343.5,122    363,132.5    'M16'}
 {TMENU Tm_cc  363,122      382,132.5    'M17'}
 {TMENU Tm_cc  382,122      402,132.5    'M18'}
 {TMENU Tm_cc  363,111      382,122      'M20'}
 {TMENU Tm_cc  382,111      402,122      'M21'}
 {TMENU Tm_cc  343.5,100    363,111      'M22'}
 {TMENU Tm_cc  363,100      382,111      'M23'}
 {TMENU Tm_cc  382,100      402,111      'M24'}
 {TMENU Tm_cc  343.5,89     363,100      'M25'}
 {TMENU Tm_cc  363,89       382,100      'M26'}
 {TMENU Tm_cc  382,89       402,100      'M27'}
 {TMENU Tm_cc  343.5,78     363,89       'M28'}
 {TMENU Tm_cc  363,78       382,89       'M29'}
 {TMENU Tm_cc  382,78       402,89       'M30'}
 {TMENU Tm_cc  343.5,67     363,78       'M31'}
 {TMENU Tm_cc  363,67       382,78       'M32'}
 {TMENU Tm_cc  382,67       402,78       'M33'}
 {TMENU Tm_cc  343.5,56     363,67       'M34'}
 {TMENU Tm_cc  363,56       382,67       'M35'}
 {TMENU Tm_cc  382,56       402,67       'M36'}
 {TMENU Tm_cc  343.5,45     363,56        'M37'}
 {TMENU Tm_cc  363,45       382,56       'M38'}
 {TMENU Tm_cc  382,45       402,56       'M39'}
 {TMENU Tm_cc  343.5,34     363,45        'M40'}
 {TMENU Tm_cc  363,34       382,45       'M41'}
 {TMENU Tm_cc  382,34       402,45       'M42'}
 {TMENU Tm_cc  382,22       402,34       'M45'}

 TMENU Tm_cc  111,208 127,217   "linesize"
 TMENU Tm_cc  129,208 146,217   "pensize"
 TMENU Tm_cc  129.6,198 146,207.6   "change_linesize"
 TMENU Tm_cc  112.4,198 127,207.6   "change_pensize"
 END_IF
END_DEFINE

Manager_tablo


