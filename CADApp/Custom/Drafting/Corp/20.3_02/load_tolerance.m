﻿DEFINE_ENCODING 'UTF-8'
{#####################################################
 Borgware GmbH
 Hauptstraße 8
 D-72401 Haigerloch-Owingen
 Tel +49 (7474) 698-0
 www.borgware.de
 $Date: 2014-01-23 15:57:43 +0100 (Do, 23 Jan 2014) $
 $Revision: 34 $
 $Author: uli.aichholz $
 $HeadURL: https://bwsvn.borgware.local/svn/Standards/18.1/trunk/ccm18_10/drafting18.1/load_tolerance.m $
#####################################################}
Dim_tolerance_table_load ((GETENV 'MECORPCUSTOMIZEDIR') + '\toleranz_utf.dtx')

