﻿DEFINE_ENCODING 'UTF-8'
{#####################################################
 Borgware GmbH
 Hauptstraße 8
 D-72401 Haigerloch-Owingen
 Tel +49 (7474) 698-0
 www.borgware.de
 $Date: 2014-01-23 15:57:43 +0100 (Do, 23 Jan 2014) $
 $Revision: 34 $
 $Author: uli.aichholz $
 $HeadURL: https://bwsvn.borgware.local/svn/Standards/18.1/trunk/ccm18_10/drafting18.1/standards.m $
#####################################################}

{### Aufruf fuer PPlus ###}
DEFINE Init_global_vars 
LET Meview 0
INQ_ENV 0
IF (POS (INQ 301) 'ME10v')
	LET Meview 1
END_IF
END_DEFINE Init_global_vars

DEFINE Me10_verlassen
  LOCAL A
  LOCAL J
  LOCAL N
  LET J 'j'
  LET N 'n'
  READ 'Wollen Sie ME10 verlassen [j/n] ?' DEFAULT (VAL 'j') A
  IF (A='j')
    EXIT CONFIRM
  END_IF
END_DEFINE

DEFINE Ui_cust_wui
  INQ_ENV 10
  IF (((INQ 6)=2)OR((INQ 6)=3)) {WUI oder mixed mode}
    BUTTON_TRIGGER ON_RELEASE {mgl. Werte ON_PUSH|ON_RELEASE}
    WUI_CLASSIC ON {mgl. Werte ON|OFF}
    {Wui_set} {holt sich das Environment aus %userprofile%}
  END_IF
END_DEFINE Ui_cust_wui


DEFINE Bw_bemfix_table_load
  PARAMETER Load_table
  PARAMETER Filename
  LOCAL Local_row
  LOCAL Item
  TRAP_ERROR
  OPEN_INFILE 1 Filename
  IF (CHECK_ERROR)
    BEEP
    DISPLAY ('Datei ' + Filename + ' kann nicht geladen werden')
  END_IF
 
  LET Local_row ((LTAB_ROWS Load_table)+1)
  LET Item ''
  LOOP
    READ_FILE 1 Item
  EXIT_IF (Item='END-OF-FILE')
    SELECT_FROM_LTAB Load_table 1=Item
    END
    IF ((READ_LTAB 'sys_select' 1 1)='')
      WRITE_LTAB Load_table Local_row 1 Item
      READ_FILE 1 Item
      WRITE_LTAB Load_table Local_row 2 Item
      READ_FILE 1 Item
      WRITE_LTAB Load_table Local_row 3 Item
      IF (Load_table='Tolerance_ltab')
        READ_FILE 1 Item
        WRITE_LTAB Load_table Local_row 4 Item
      END_IF
      LET Local_row (Local_row+1)
    ELSE
      READ_FILE 1 Item
      READ_FILE 1 Item
      IF (Load_table='Tolerance_ltab')
        READ_FILE 1 Item
      END_IF
    END_IF
  END_LOOP
  CLOSE_FILE 1
  SORT_LTAB Load_table 1 CONFIRM
END_DEFINE


{----------------------------------------------------------------------------
- Funktionstasten u. Menuezeilen-Programmierung  F1 - F8 (+CHR 13 = Return )|
- --> Sw_config_menufields geht nicht mit ME-Maus-Version und VDAPS/DIN !!  |
----------------------------------------------------------------------------}
 DEFINE Sw_config_keys
	CLEAR_KEYS
	INQ_ENV 10
	IF (NOT ((INQ 6)=4)) 
		{DEFINE_KEY 1 ## ruft die ME10 Hilfe auf (aus KeysU.cfg) ##}
		DEFINE_KEY 'F02' EXECUTE ('RUN''' + "cmd /k set|more" +CHR 13)	
		DEFINE_KEY 'F11' EXECUTE 'Me10_verlassen'
		DEFINE_KEY 'F12' EXECUTE 'Sm_data_management' {WM/DM Menu}
	END_IF
	{ DEFINE_KEY 1 ## ruft die ME10 Hilfe auf (aus KeysU.cfg) ##}
{    DEFINE_KEY 'F03' EXECUTE 'Res'										{ Reset alle Werte		}
    DEFINE_KEY 'F04' EXECUTE 'NEW_SCREEN'								{ Bildneuaufbau			}
    DEFINE_KEY 'F05' EXECUTE 'DIM_PREFIX "(" END DIM_POSTFIX ")" END'	{ vor/hin Mass			}
    DEFINE_KEY 'F06' EXECUTE 'DIM_PREFIX "<diameter>" END'				{ ø vor Mass			}
    DEFINE_KEY 'F07' EXECUTE 'DIM_POSTFIX "x45<degree>" END'			{ Mass + x45°			}
    DEFINE_KEY 'F08' EXECUTE 'DIM_PREFIX "M" END'						{ M vor Mass			}
	{ DEFINE_KEY 'F09'} 
	{ DEFINE_KEY 'F10'}

	{ Beispiel für neue Tastenbelegung mit ALT-Taste}
	DEFINE_KEY 'S' ALT EXECUTE 'VERTICAL' {ALT+S =Senkrecht}
	DEFINE_KEY 'W' ALT EXECUTE 'HORIZONTAL' {ALT+W =Waagerecht}
	DEFINE_KEY 'D' ALT '<diameter>' {ALT+D =Druchmesser}
	{ Beispiele für neue Tastenbelegungen}
	{ DEFINE_KEY 'R' CONTROL EXECUTE 'strgmitr' ***strg + R***}
	{ DEFINE_KEY 'R' ALT EXECUTE 'altmitr' ***alt + R***}
	{ DEFINE_KEY 'R' ALT_CONTROL EXECUTE 'strgalt' ***strg + alt + R***}
  END_DEFINE
  Sw_config_keys				{ Zum Starten aktivieren	}

  
  
  DEFINE Sw_config_menufields
	IF ((NOT Meview)AND (((GETENV'LOOK')='1')OR((GETENV'LOOK')='3')))
		Eight_menu_slots 'Clear' 'CLEAR_PREFIX  CLEAR_POSTFIX CLEAR_TOLERANCE CLEAR_SUPERFIX CLEAR_SUBFIX'	26 1
	{   Eight_menu_slots ''  ''						   26 2}
	{   Eight_menu_slots 'Reset' 'Res'					   26 3}
		Eight_menu_slots 'Reset' 'Res'                                   	26 4
		Eight_menu_slots '( )'  'DIM_PREFIX "(" END DIM_POSTFIX ")"END '	27 1
		Eight_menu_slots ' ø  '  'DIM_PREFIX "<diameter>" END'				27 2
		Eight_menu_slots ' x45°' 'DIM_POSTFIX "x45<degree>"END'			    27 3
		Eight_menu_slots ' M  '  'DIM_PREFIX "M" END'				  		27 4
	END_IF
  END_DEFINE Sw_config_menufields
  
  
{ OSD Drafting Bem. Tabelle Toleranzen fuellen }
DEFINE Toleranzen_laden
	IF (NOT (Meview))
		INQ_ENV 0
		{ OSD Drafting Bem. Tabellen Postfix/Praefix fuellen }
		Bw_bemfix_table_load 'Prefix_ltab'  ((GETENV 'MECORPCUSTOMIZEDIR') + '\prefix.dtx')
		Bw_bemfix_table_load 'Postfix_ltab' ((GETENV 'MECORPCUSTOMIZEDIR') + '\postfix.dtx')
		{ muss per input geladen werden Dim_tolerance_table_load  ((GETENV 'DRAFTING_STD_DIR') +'\toleranz_utf.dtx') }
		INPUT ((GETENV 'MECORPCUSTOMIZEDIR') + '\load_tolerance.m')
	END_IF
END_DEFINE
Toleranzen_laden
  
