DEFINE sauber_machen
  LOCAL zname
  LOCAL pfad
  LET pfad (GETENV("APPDATA"))
  LET zname 'backup.mi'
  Csn_check_inv_geo
  eigenstaendig
  Punkte_0_1
  Doppelte_Linien_0_1
  STORE SELECT GLOBAL ALL CONFIRM (pfad+'\'+zname) END
  EDIT_PART TOP
  DAC
  LOAD (pfad+'\'+zname)
  PURGE_FILE (pfad+'\'+zname) CONFIRM
  END_DEFINE

DEFINE Csn_check_inv_geo
   LOCAL Versie
   LOCAL frage
   LOCAL ergebniss
   INQ_ENV 0
   LET Versie (VAL (SUBSTR (INQ 301) 22 6))
   IF (Versie>11.6)
     SHOW_INV_GEO GLOBAL CONFIRM
     INQ_SELECTED_ELEM GLOBAL INFOS 'INVISIBLE' END
     IF ((INQ 14)>0)
       DISPLAY (STR (INQ 14) +' INVISIBLE Geometrie gefunden!')
       READ 'L�schen? (j/n)' DEFAULT 'j' frage
       IF (frage='j')
         Csn_verwijder_inv_geo
       END_IF
     END_IF
   END_IF
END_DEFINE

DEFINE Csn_verwijder_inv_geo
   SHOW_INV_GEO GLOBAL CONFIRM
   DELETE GLOBAL INFOS 'INVISIBLE' END
END_DEFINE

DEFINE eigenstaendig
  LOCAL count
  LOCAL partname
  EDIT_PART TOP
  LET Count 1
  PB_LTAB_UPDATE
  loop
    LET partname (READ_LTAB 'PBT_LTAB' (count + 1) 2)
    UNSHARE_PART (str partname)
    PB_LTAB_UPDATE
    LET count (count+1)
  exit_if (count=(LTAB_ROWS 'PBT_LTAB'))
  end_loop
end_define

DEFINE Punkte_0_1
  INQ_ENV 8
  LET I (INQ 5)
  WHILE (I>0)
    TRAP_ERROR
    EDIT_PART ('~'+(STR I))
    IF (NOT CHECK_ERROR)
      CLEAN_DRAWING CLEAN_CLOSE_POINTS 0.1 CONFIRM
      END_IF
    LET I (I - 1)
  END_WHILE
  END
END_DEFINE

DEFINE Doppelte_Linien_0_1
  INQ_ENV 8
  LET I (INQ 5)
  WHILE (I>0)
    TRAP_ERROR
    EDIT_PART ('~'+(STR I))
    IF (NOT CHECK_ERROR)
      CLEAN_DRAWING CLEAN_DUPLICATE_GEOMETRY 0.1 CONFIRM
      END_IF
    LET I (I - 1)
  END_WHILE
  END
END_DEFINE