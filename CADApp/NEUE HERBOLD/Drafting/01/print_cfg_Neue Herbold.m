DEFINE_ENCODING 'UTF-8'
PLOT_DESTINATION '' END
WIN_PRT_MGR COPIES 1 END
WIN_PRT_MGR ORIENTATION LANDSCAPE END
WIN_PRT_MGR PAPER 'A4' END
PLOT_STOP_ON_ERROR ON END
PLOT_SCALE 1 END
PLOT_CENTER ON END
LET Sys_plot_source_ll 0,0 END
LET Sys_plot_source_ur 100,100 END
LET Sys_plot_ox 0 END
LET Sys_plot_oy 0 END
LET Sys_plot_rot 0 END
LET Sys_plot_source ALL END
LET Sys_plot_as_displayed 0 END
LET Plot_pix_color_option 0 END
LET Plot_pix_reverse_video_option 0 END
LET Plot_pix_res_option 1 END
LET Sys_plot_sheets TRUE END
TRUE_COLOR_PLOTTING OFF END
PLOT_TRANSFORMATION ALL ALL ALL SAME 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.000 0.502 0.000 0.502 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.000 0.502 0.000 0.502 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.000 0.502 0.000 0.502 0 0 SAME PENWIDTH
 0.25 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.502 1.000 0.000 0.502 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.502 1.000 0.000 0.502 0 0 SAME PENWIDTH
 0.35 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.502 1.000 0.000 0.502 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.502 1.000 0.000 0.502 0 0 SAME PENWIDTH
 0.25 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.000 0.502 0.502 1.000 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.000 0.502 0.502 1.000 0 0 SAME PENWIDTH
 0.7 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.000 0.502 0.502 1.000 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.000 0.502 0.502 1.000 0 0 SAME PENWIDTH
 0.1 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.502 1.000 0.502 1.000 ALL SAME 1
PLOT_TRANSFORMATION ALL 0.000 0.502 0.502 1.000 0.502 1.000 0 0 SAME PENWIDTH
 0.18 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.502 1.000 0.502 1.000 ALL SAME 0
PLOT_TRANSFORMATION ALL BLACK ALL SAME 0
PLOT_TRANSFORMATION ALL WHITE ALL SAME 1
PLOT_TRANSFORMATION ALL 0.502 1.000 0.502 1.000 0.502 1.000 0 0 SAME PENWIDTH
 0.5 1
PLOT_TRANSFORMATION PHANTOM CYAN ALL SAME 0
TRUE_COLOR_PLOTTING OFF
PLOT_LINETYPE_LENGTH 3 5 5 5 5 5 2
PLOT_LINETYPE_LENGTH MIN_DIST_FACTOR ALL 0.25
