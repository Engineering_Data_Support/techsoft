{ ==================================== }
{ chr_list.m		 os / 17.08.04 }
{ Darstellen der Character von 0 - 256 }

DEFINE OS_chr_list
  LOCAL p0
  LOCAL p
  LOCAL z
  LET p0 (0,-5)
  LET p (0,0)
  LET z 0
  INIT_PART '.chr_list'
  INQ_ENV 8
  TEXT ('TEXT (CHR xx) Font = '+(STR (INQ 301))) p0 END
  LOOP
    TEXT ("(CHR " + (STR z) + ") = " + (CHR z)) p END
    EXIT_IF (z = 256)
    LET z (z +1)
    IF ((z = 51) OR (z = 101) OR (z = 151) OR (z = 201) OR (z = 251))
       LET p (PNT_XY ((X_OF p) + 50) 0)    
    ELSE
       LET p (PNT_XY (X_OF p) ((Y_OF p) + 5))
    END_IF
  END_LOOP
  END_PART
  WINDOW_FIT
END_DEFINE
OS_chr_list
