{*** Dateien laden und neu speichern ***}

CATALOG_LAYOUT '1234567890123456789012345678901234567890123456789012345678901234546789
DEFINE_CATALOG '' '' 1 1 END
DEFINE_CATALOG 'Dateiname  (maximal 70 Zeichen)' 'FILE_NAME' 1 1   END

DEFINE store_mi
  LOCAL filename
  LOCAL Q
  CATALOG '' DEL_OLD 'c:/makro/tmp_mi'
  OPEN_INFILE 1 'c:/makro/tmp_mi
  EDIT_FILE 'c:/makro/tmp_mi'  {nur zum evtl. editieren der Liste}
  REPEAT
    READ_FILE 1 filename
    LET filename (TRIM filename)
    IF ((filename = '') OR
        (filename = 'VERZEICHNIS: .') OR
        (filename = 'Dateiname  (maximal 70 Zeichen)') OR
        (filename = '======================================================================') OR
        (filename = 'END-OF-FILE'))
    ELSE
      EDIT_PART TOP
      DELETE ALL CONFIRM
      TRAP_ERROR
      LOAD filename
      IF (CHECK_ERROR)
      ELSE
        STORE MI ALL DEL_OLD Filename
		END_IF
    END_IF
  UNTIL (Filename = 'END-OF-FILE')
  CLOSE_FILE 1
END_DEFINE