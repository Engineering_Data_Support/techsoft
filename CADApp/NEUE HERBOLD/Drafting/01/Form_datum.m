{*******************************************************************}
{ Dieses Makro wird �ber ein Menue aufgerufen.}
{ Es kann in das Menu PLOT eingebaut werden, damit der Nutzer wei�, wann er die Zeichnung geplottet }
{ - Es sind 2 Makros, die den Zeichensatz in den Makros steuern.}
{  31.01.1993 D.G.}
DEFINE Form_datum
  LOCAL A
  LOCAL Bz
  INQ_ENV 0
      let Form_datum_zahl 1
    Form_schrift_speicher_vor
	  CURRENT_FONT 'hp_d17_v'
      TEXT_FRAME OFF
      TEXT_ANGLE 0
      TEXT_ADJUST 4
      TEXT_LINESPACE 1.5
      TEXT_FILL OFF
      TEXT_SIZE 2.5
      TEXT_RATIO 0.9
      TEXT_SLANT 0
      LET A ((SUBSTR DATE 1 2)+'. '+(SUBSTR DATE 4 3)+'. '+(SUBSTR DATE 10 8 )+' Uhr')
      {READ PNT}
	  unten_rechts
	  INIT_PART 'PLOT_DATUM'
	  TEXT ('plotd.: '+A) Pkt_ur   end
	  CHANGE_COLOR
      WHITE SELECT ALL CONFIRM
	  EDIT_PART TOP
      Form_schrift_speicher_zur
END_DEFINE

DEFINE unten_rechts
  LOCAL Pkt_ul
  LOCAL Pkt_or
  LOCAL Zeichnungsscale
  EDIT_PART TOP
  INQ_ENV 7
  LET Pkt_ul (INQ 101)
  LET Pkt_or (INQ 102)
  INQ_ENV 6
  LET Zeichnungsscale ( INQ 4 )
  LET Pkt_ur (PNT_XY (( X_OF Pkt_or ) - ( 53 / Zeichnungsscale )) ((Y_OF Pkt_ul) + ( 2.6 / Zeichnungsscale )))
END_DEFINE