DEFINE M_entf_teile_leer
  LOCAL Teil
  LOCAL Dummy
  LOCAL Teilezahl
  PB_LTAB_UPDATE
  LET Teilezahl (LTAB_ROWS 'PBT_LTAB')
  LET Dummy 0
  PARTS_LIST TREE DEL_OLD 'c:\tmp\parts.tmp'
  OPEN_INFILE 2 'c:\tmp\parts.tmp'
  LOOP
    READ_FILE 2 Teil
  EXIT_IF ((Teil)='END-OF-FILE')
    LET Teil (TRIM Teil)
    LET A (POS Teil '~')
    LET Teil (SUBSTR Teil A ((LEN Teil) - A))
    LET A (POS Teil ']')
    LET Teil (SUBSTR Teil 1 (A -1))
    TRAP_ERROR
    EDIT_PART Teil
    INQ_ENV 7
    IF ((CHECK_ERROR)=0)
      IF ((X_OF (INQ 101)=0) AND (Y_OF (INQ 101)=0) AND (X_OF (INQ 102)=0)
        AND (Y_OF (INQ 102)=0))
        DELETE ALL CONFIRM
        LET Dummy ((Dummy)+1)
      END_IF
    END_IF
  END_LOOP
  EDIT_PART TOP
  CLOSE_FILE 2
  IF ((Dummy)=0)
    DISPLAY 'Keine leeren Teile vorhanden'
  ELSE
    PB_LTAB_UPDATE
    DISPLAY (STR (Teilezahl - (LTAB_ROWS 'PBT_LTAB'))+
    ' leere Teile wurden entfernt')
  END_IF
END_DEFINE

