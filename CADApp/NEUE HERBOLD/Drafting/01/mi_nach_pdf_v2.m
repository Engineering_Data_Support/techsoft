{===================================================================================}
{ os / 04.08.2005                                                                   }
{ ge�ndert: os / 11.08.2005  Erkennung von Annotation-Drawings hinzugef�gt.         }
{===================================================================================}

DEFINE os_pdf_batch_plot
  LOCAL Pfad_input
  LOCAL Pfad_output
  LOCAL Datei_name
  LOCAL Datei_name_anno
  LOCAL Anno_drawing
  LOCAL Anno_blatt
  LOCAL Anno_blattzahl
  
  LET Temp "C:\Temp\"
  LET Pfad_input "C:\Temp\"
  LET Pfad_output "C:\Temp\"  

  {== Verzeichnis lesen und in eine Datei schreiben ==}
  CATALOG Pfad_input SELECT "FILE_NAME" "*.mi" DEL_OLD (Temp + "catalog-datei.tmp")
  OPEN_INFILE 1 (Temp + "catalog-datei.tmp")
  LOOP
    READ_FILE 1 Datei_name
    EXIT_IF (Datei_name="END-OF-FILE")
    IF (POS Datei_name ".mi")
       LET Anno_drawing 0
       {== Eingelesene Zeile wird auf Dateinamen gekuerzt ==}
       LET Datei_name (SUBSTR Datei_name 1 ((POS Datei_name ".mi") +2))
       DELETE ALL CONFIRM
       LOAD (Pfad_input + Datei_name)
  
		  INQ_ENV 0
		  IF (POS (INQ 305) 'Annotation')
    		LET Anno_drawing 1
 		 	END_IF

			IF (Anno_drawing=0)
			 		LET Datei_name (SUBSTR Datei_name 1 ((POS Datei_name ".mi") -1))
       		{* Aktionen aufrufen *}
			 		GD_plot_pdf Pfad_output Datei_name
			ELSE
				  TRAP_ERROR
 					DELETE SELECT GLOBAL INFOS 'DOCU_MARKED_AS_INVISIBLE' CONFIRM
 					END
 					IF (CHECK_ERROR)
 					END_IF
					
					Set_sys_plot_sheets TRUE Make_sheet_table
    			LET Anno_blattzahl (LTAB_ROWS 'List_of_sheets')
			 		LET Datei_name (SUBSTR Datei_name 1 ((POS Datei_name ".mi") -1))
    			LOOP
    				LET Anno_blatt (READ_LTAB 'List_of_sheets' Anno_blattzahl 2)
   				  VIEW Anno_blatt
   				  EDIT_PART Anno_blatt
   				  
   				  {LET Datei_name_anno (Datei_name +  '_Blatt_' + (STR Anno_blattzahl))}
						LET Datei_name_anno (Datei_name +  '_Blatt_' + (STR (READ_LTAB 'List_of_sheets' Anno_blattzahl 1)))

   				  {* Aktionen aufrufen *}
   				  GD_plot_pdf Pfad_output Datei_name_anno

						LET Anno_blattzahl (Anno_blattzahl - 1)
						EXIT_IF (Anno_blattzahl = 0)
    			END_LOOP
			END_IF

			{alles loeschen} 	
      DACA
    END_IF
  END_LOOP
  CLOSE_FILE 1
END_DEFINE

{M4021 GD_plot_pdf}
DEFINE GD_plot_pdf
  PARAMETER Pfad_output
  PARAMETER Datei_name

  PLOT_TRANSFORMATION RESET
  {Plottransformation}
  Plot_black_and_white
 
  Set_sys_plot_sheets FALSE
  Set_sys_plot_plotscale 1
  Set_sys_plot_rot 0
  Set_sys_plot_type 'PDF_GENERIC'
  Set_sys_plot_filename_delold (TRUE)
  {Format bestimmen}
  GD_format_berechnen
  LET Sys_plot_filename_generate 0
  LET Sys_plot_filename_prefix (Pfad_output + Datei_name + '.pdf')
  Plot_plot
END_DEFINE

{ ***** Formatgroesse bestimmen ********************* }
{M4030 GD_format_berechnen}
DEFINE GD_format_berechnen
  LOCAL Pkt_ul
  LOCAL Pkt_or
  LOCAL Zeichnungsscale
  LOCAL Breite
  LOCAL Hoehe
  LOCAL Length
  {EDIT_PART TOP}
  INQ_ENV 7
  LET Pkt_ul (INQ 101)
  LET Pkt_or (INQ 102)
  INQ_ENV 6
  LET Zeichnungsscale ( INQ 4 )
  LET Breite ( ( ( X_OF Pkt_or ) - ( X_OF Pkt_ul ) ) * Zeichnungsscale )
  LET Hoehe  ( ( ( Y_OF Pkt_or ) - ( Y_OF Pkt_ul ) ) * Zeichnungsscale )
  INQ_ENV 6
  LET Length (INQ 2)
  Set_sys_plot_format_width (Breite*Length)
  Set_sys_plot_format_height (Hoehe*Length)
  Set_sys_plot_format 'USER'
  Plot_define_format_viewport
END_DEFINE

DEFINE daca
  RESET_SYSTEM CONFIRM
  EDIT_PART TOP
  DELETE ALL CONFIRM
  RENAME_PART 'Top'
  SYMBOL_PART OFF 'Top' END
  DRAWING_SCALE 1
  SHOW ALL ON
  SHOW VERTEX OFF
  I_RULER_GRID_RESET
END_DEFINE
