{===================================================================================}
{ os / 04.08.2005                                                                   }
{===================================================================================}

DEFINE os_pdf_batch_plot
  LOCAL Pfad_input
  LOCAL Pfad_output
  LOCAL Datei_name
  
  LET Temp "C:\Temp\"
  LET Pfad_input "C:\Temp\"
  LET Pfad_output "C:\Temp\"  

  {== Verzeichnis lesen und in eine Datei schreiben ==}
  CATALOG Pfad_input SELECT "FILE_NAME" "*.mi" DEL_OLD (Temp + "catalog-datei.tmp")
  OPEN_INFILE 1 (Temp + "catalog-datei.tmp")
  LOOP
    READ_FILE 1 Datei_name
    EXIT_IF (Datei_name="END-OF-FILE")
    IF (POS Datei_name ".mi")
       {== Eingelesene Zeile wird auf Dateinamen gekuerzt ==}
       LET Datei_name (SUBSTR Datei_name 1 ((POS Datei_name ".mi") +2))
       DELETE ALL CONFIRM
       LOAD (Pfad_input + Datei_name)
			 LET Datei_name (SUBSTR Datei_name 1 ((POS Datei_name ".mi") -1))
       {* Aktionen aufrufen *}
			 GD_plot_pdf Pfad_output Datei_name
       DACA
    END_IF
  END_LOOP
  CLOSE_FILE 1
END_DEFINE

{M4021 GD_plot_pdf}
DEFINE GD_plot_pdf
  PARAMETER Pfad_output
  PARAMETER Datei_name

  PLOT_TRANSFORMATION RESET
  {Plottransformation}
  Plot_black_and_white
 
  Set_sys_plot_sheets FALSE
  Set_sys_plot_plotscale 1
  Set_sys_plot_rot 0
  Set_sys_plot_type 'PDF_GENERIC'
  Set_sys_plot_filename_delold (TRUE)
  {Format bestimmen}
  GD_format_berechnen
  LET Sys_plot_filename_generate 0
  LET Sys_plot_filename_prefix (Pfad_output + Datei_name + '.pdf')
  Plot_plot
END_DEFINE

{ ***** Formatgroesse bestimmen ********************* }
{M4030 GD_format_berechnen}
DEFINE GD_format_berechnen
  LOCAL Pkt_ul
  LOCAL Pkt_or
  LOCAL Zeichnungsscale
  LOCAL Breite
  LOCAL Hoehe
  LOCAL Length
  EDIT_PART TOP
  INQ_ENV 7
  LET Pkt_ul (INQ 101)
  LET Pkt_or (INQ 102)
  INQ_ENV 6
  LET Zeichnungsscale ( INQ 4 )
  LET Breite ( ( ( X_OF Pkt_or ) - ( X_OF Pkt_ul ) ) * Zeichnungsscale )
  LET Hoehe  ( ( ( Y_OF Pkt_or ) - ( Y_OF Pkt_ul ) ) * Zeichnungsscale )
  INQ_ENV 6
  LET Length (INQ 2)
  Set_sys_plot_format_width (Breite*Length)
  Set_sys_plot_format_height (Hoehe*Length)
  Set_sys_plot_format 'USER'
  Plot_define_format_viewport
END_DEFINE

DEFINE daca
  RESET_SYSTEM CONFIRM
  EDIT_PART TOP
  DELETE ALL CONFIRM
  RENAME_PART 'Top'
  SYMBOL_PART OFF 'Top' END
  DRAWING_SCALE 1
  SHOW ALL ON
  SHOW VERTEX OFF
  I_RULER_GRID_RESET
END_DEFINE
