DEFINE eigenstaendig
  LOCAL count
  LOCAL partname
  EDIT_PART TOP
  LET Count 1
  PB_LTAB_UPDATE
  loop
    LET partname (READ_LTAB 'PBT_LTAB' (count + 1) 2)
    UNSHARE_PART (str partname)
    PB_LTAB_UPDATE
    LET count (count+1)
  exit_if (count=(LTAB_ROWS 'PBT_LTAB'))
  end_loop
end_define