DEFINE Change_dwg

  LOCAL Zaehler
  LOCAL Z_max
  LOCAL Teil

  EDIT_PART TOP

  { GEO }
  DISPLAY_NO_WAIT "Geometrie-Farbe wird ge�ndert. Bitte warten!"

  {wei�e Linien --> solid }
  CHANGE_LINETYPE SOLID SELECT GLOBAL WHITE CONFIRM END

  { rote Linien --> verdeckt }
  CHANGE_LINETYPE DASHED SELECT GLOBAL RED CONFIRM END
 
 { gelbe Linien --> Strich-Punkt }
  CHANGE_LINETYPE DOT_CENTER SELECT GLOBAL YELLOW CONFIRM END
 
   { Stift- & Linienst�rke --> 0 }
  CHANGE_LINESIZE 0 SELECT GLOBAL ALL CONFIRM END
  CHANGE_PENSIZE 0 SELECT GLOBAL ALL CONFIRM END

  { HATCH }
  DISPLAY_NO_WAIT "Schraffur-Farbe wird ge�ndert. Bitte warten!"
  CHANGE_HATCH_COLOR CYAN SELECT GLOBAL ALL CONFIRM END

  { DIMENSIONS }
  DISPLAY_NO_WAIT "Bemassungs-Farbe wird ge�ndert. Bitte warten!"
  EDIT_PART TOP
  CREATE_LTAB "Teileltab"
  PARTS_LIST TREE LTAB "Teileltab"
  LET Zaehler 0
  LET Z_max (LTAB_ROWS "Teileltab")
  LOOP
    LET Zaehler (Zaehler+1)
    EXIT_IF (Zaehler>Z_max)
    LET Teil (READ_LTAB "Teileltab" Zaehler 1)
    TRAP_ERROR
    EDIT_PART Teil
    { Ma�linien --> gr�n }
    CHANGE_DIM_COLOR GREEN SELECT DIMENSIONS ALL CONFIRM END
    { Bema�ungstexte --> gelb }
    CHANGE_DIM_TEXTS DIM_ALL YELLOW SELECT DIMENSIONS ALL CONFIRM END
    IF (CHECK_ERROR) END_IF
  END_LOOP

  { Texte --> Schriftart 'hp_d17_v' }
  Gdm_text_font_aendern

  EDIT_PART TOP
  WINDOW FIT

END_DEFINE

DEFINE Gdm_text_font_aendern
  LOCAL Zaehler
  LOCAL Z_max
  LOCAL Teil
  EDIT_PART TOP
  CREATE_LTAB 'Teileltab'
  PARTS_LIST TREE LTAB 'Teileltab'
  LET Zaehler 0
  LET Z_max (LTAB_ROWS 'Teileltab')
  LOOP
    LET Zaehler (Zaehler+1)
  EXIT_IF (Zaehler>Z_max)
    LET Teil (READ_LTAB 'Teileltab' Zaehler 1)
    EDIT_PART Teil
    TRAP_ERROR
    CHANGE_DIM_TEXTS DIM_ALL FONT 'hp_d17_v' SELECT DIMENSIONS ALL CONFIRM END
    IF (CHECK_ERROR)
      DISPLAY_NO_WAIT 'keine Bemassung gefunden'
    END_IF
    TRAP_ERROR
    INQ_SELECTED_ELEM GLOBAL TEXTS ALL
    END
    WHILE (INQ 14)
        CHANGE_TEXT_FONTNAME 'hp_d17_v' (INQ 101) END
      INQ_NEXT_ELEM
    END_WHILE
    IF (CHECK_ERROR)
      DISPLAY_NO_WAIT 'keine Texte gefunden'
    END_IF
  END_LOOP
  EDIT_PART TOP
  WINDOW FIT
END_DEFINE