@echo on
if exist c:\makro\liste.txt del c:\makro\liste.txt
if exist s:\dxf_transfer\*.log del s:\dxf_transfer\*.log
:start
if "%1" == "" goto end
echo %~d1%~p1%~n1 >> c:\makro\liste.txt
shift
goto start
:end
s:
cd\
cd dxf_transfer
set command=C:\Programme\CoCreate\CoCreate_Drafting_2008\dxfdwg\dxfdwg.exe -o -b c:\makro\liste.txt
start %command%
exit
