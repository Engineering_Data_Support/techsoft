DEFINE Form_schrift_speicher_zur
  IF (Form_datum_zahl=1)
    TEXT_FRAME      (Text_inq_601)
    TEXT_FILL      (Text_inq_602)
    CURRENT_FONT    (Text_inq_302)
    TEXT_ADJUST    (Text_inq_3)
    TEXT_LINESPACE  (Text_inq_4)
    TEXT_RATIO      (Text_inq_5)
    TEXT_SIZE      (Text_inq_6)
    TEXT_SLANT      (Text_inq_7)
    TEXT_ANGLE      (Text_inq_8)
  END_IF
END_DEFINE
{*******************************************************************}
DEFINE Form_schrift_speicher_vor
  INQ_ENV 12
  IF (Form_datum_zahl=1)
    LET Text_inq_602        (INQ 602)
    LET Text_inq_601        (INQ 601)
    LET Text_inq_302        (INQ 302)
    LET Text_inq_8          (INQ 8)
    LET Text_inq_7          (INQ 7)
    LET Text_inq_6          (INQ 6)
    LET Text_inq_5          (INQ 5)
    LET Text_inq_4          (INQ 4)
    LET Text_inq_3          (INQ 3)
    LET Text_inq_2          (INQ 2)
  END_IF
END_DEFINE