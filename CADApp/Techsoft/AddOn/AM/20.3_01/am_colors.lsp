; ------------------------------------------------------------------------------
; SVN:  $Id: am_colors.lsp 185 2010-07-08 09:08:04Z pjahn $
; macro to switch between color settings in annotation
; Wolfgang Hofer; Techsoft Datenverarbeitung GmbH; http://www.techsoft.at 
; ------------------------------------------------------------------------------

(in-package :custom)
(use-package '(:docu :OLI))

(export '(ts-set-current-bg-colors
          ts-set-current-bg-color-init
))

(defconstant +ts-addon-dir+ (oli::sd-convert-filename-from-platform (directory-namestring *load-truename*)))
(setf cmd (format nil "input '~Aam_colors.m'" +ts-addon-dir+))
(oli::sd-execute-annotator-command :cmd cmd)

(sd-defdialog 'ts-switch-background-color
:toolbox-button t
:dialog-title "Switch Background Color"
:dialog-type :interrupt
:dialog-control :sequential
:ok-action
  '(progn
    (oli::sd-execute-annotator-command :cmd "Ts_am_switch_bg")
    (setf *ts-switch-background-color* 
       (oli::sd-execute-annotator-function :fnc "Ts_am_get_current_bg_switch"))
    (oli::sd-set-persistent-data "ANNOTATION" "TS_BG_COLOR_SWITCH" 
          `(:TS_BG_COLOR_SWITCH ,*ts-switch-background-color*))
    (ts-set-current-bg-colors)	
   )
)

(sd-defdialog 'am_black_white
  :dialog-title "B/W Color Settings"
  :dialog-control :sequential 
  :ok-action '(progn
	  (MODIFY_DEFAULT_SETTING :path "Annotation/Bom/HighlightColors/Multi" :A_COLOR 16318464)
	  (MODIFY_DEFAULT_SETTING :path "Annotation/Bom/HighlightColors/Todo" :A_COLOR 63744)
;	  (MODIFY_DEFAULT_SETTING :path "Annotation/Viewport/Appearance/BackgroundColor" :A_COLOR 14803425)  ;225,225,225
; 	  (MODIFY_DEFAULT_SETTING :path "Annotation/Viewport/Appearance/BackgroundColor" :A_COLOR 15461355)  ;235,235,235
;	  (MODIFY_DEFAULT_SETTING :path "Annotation/Viewport/Appearance/BackgroundColor" :A_COLOR 15790320)  ;240,240,240
 	  (MODIFY_DEFAULT_SETTING :path "Annotation/Viewport/Appearance/BackgroundColor" :A_COLOR 16119285)  ;245,245,245
;	  (MODIFY_DEFAULT_SETTING :path "Annotation/Viewport/Appearance/BackgroundColor" :A_COLOR 16448250)  ;250,250,250
;	  (MODIFY_DEFAULT_SETTING :path "Annotation/Viewport/Appearance/BackgroundColor" :A_COLOR 16777215)  ;255,255,255
	  
	;;(oli::sd-execute-annotator-command :cmd "Docu_set_current_highlight_attr solid 1,0,0")
  )
)

(sd-defdialog 'am_color
  :dialog-title "Default Color Settings"
  :dialog-control :sequential
  :ok-action '(progn
  (MODIFY_DEFAULT_SETTING 
	:path "Annotation/Viewport/Appearance/BackgroundColor"
	:A_COLOR 1644895)
	;;(oli::sd-execute-annotator-command :cmd "Docu_set_current_highlight_attr solid 1,1,1")
  )
)

(defun ts-set-current-bg-colors ()
 ;;(display "ts-set-current-bg-colors")
 (setf *ts-switch-background-color* 
       (oli::sd-execute-annotator-function :fnc "Ts_am_get_current_bg_switch"))
 (if *ts-switch-background-color*
	 (am_color)
	 (am_black_white)   
 )
 (oli::sd-execute-annotator-command :cmd " Ts_am_set_current_bg")
)

(defun ts-set-current-bg-color-init() 
 (setf *ts-switch-background-color* 
  (getf (oli::sd-get-persistent-data "ANNOTATION" "TS_BG_COLOR_SWITCH" :default '(:TS_BG_COLOR_SWITCH t))
        :TS_BG_COLOR_SWITCH))
 (if *ts-switch-background-color* 
  (oli::sd-execute-annotator-command :cmd "LET Ts_am_current_bg_switch TRUE")
  (oli::sd-execute-annotator-command :cmd "LET Ts_am_current_bg_switch FALSE")
 )
 (ts-set-current-bg-colors)
)

(defvar *sco-am_plot_ex*
     #'DOCU::am_plot_ex)
(defun DOCU::am_plot_ex (&rest args)
 (let (result)
  ;;(display "MY DOCU::am_plot_ex")
  ;;(display (format nil "args=~a" args))
  (setf result (apply *sco-am_plot_ex* args))
  ;;(display (format nil "result=~a" result))
  (custom::ts-set-current-bg-colors)
  result
 )
)

(ts-set-current-bg-color-init)