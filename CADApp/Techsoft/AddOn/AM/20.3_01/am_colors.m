{ ------------------------------------------------------------------------------ }
{ SVN:  $Id: am_colors.m 221 2011-01-10 10:32:01Z pjahn $																	 }
{ macro to switch between color settings in annotation                           }
{ Wolfgang Hofer; Techsoft Datenverarbeitung GmbH; http://www.techsoft.at        }
{ ------------------------------------------------------------------------------ }

LET Ts_am_current_bg_switch TRUE

DEFINE Ts_am_get_current_bg_switch
LOCAL Dmy
LET Dmy (DOCU_OPEN_CONNECTION_TO_SD)
    IF (Ts_am_current_bg_switch=TRUE)
        LET Dmy (DOCU_ADD_LINE_TO_SD 't')
    ELSE
        LET Dmy (DOCU_ADD_LINE_TO_SD 'nil')
    END_IF
LET Dmy (DOCU_CLOSE_CONNECTION_TO_SD) 
END_DEFINE

DEFINE Ts_am_set_white_bg
  screen_transformation all all all same RGB_COLOR 0.3 0.3 0.3
  screen_transformation all WHITE all same BLACK
  screen_transformation all YELLOW all same RGB_COLOR 0.6 0.6 0.6
  screen_transformation all RED all same RGB_COLOR 0.3 0.3 0.3
  screen_transformation all GREEN all same RGB_COLOR 0.3 0.3 0.3
  screen_transformation all CYAN all same RGB_COLOR 0.3 0.3 0.3
  screen_transformation all MAGENTA all same RGB_COLOR 0.3 0.3 0.3
  screen_transformation all BLUE all same RGB_COLOR 0.3 0.3 0.3
  screen_transformation all .9 .99 .0 .0 .0 .0 all same RGB_COLOR .8 0 0 {part has PosNr}
  screen_transformation all .0 .0 .9 .99 .0 .0 all same RGB_COLOR 0 .5 0 {parts without PosNr}
END_DEFINE

DEFINE Ts_am_set_orig_bg
	screen_transformation RESET
END_DEFINE  

DEFINE Ts_am_set_current_bg
 IF (Ts_am_current_bg_switch=TRUE)
  Ts_am_set_orig_bg
 ELSE
  Ts_am_set_white_bg
 END_IF
END_DEFINE

DEFINE Ts_am_switch_bg
 LET Ts_am_current_bg_switch (NOT Ts_am_current_bg_switch)
 Ts_am_set_current_bg
END_DEFINE
 
DEFINE Docu_set_current_highlight_attr
  PARAMETER New_highlight_ltype
  PARAMETER New_highlight_color
  IF (New_highlight_ltype<>'')
    CURRENT_HIGHLIGHT_ATTR (VAL New_highlight_ltype)
    END
  END_IF
  IF (New_highlight_color<>'')
  	IF (New_highlight_color = 'GREEN')
      CURRENT_HIGHLIGHT_ATTR RGB_COLOR 0.9,0.6,0
      END
    ELSE
      CURRENT_HIGHLIGHT_ATTR (VAL New_highlight_color)
      END
    END_IF
  END_IF
END_DEFINE