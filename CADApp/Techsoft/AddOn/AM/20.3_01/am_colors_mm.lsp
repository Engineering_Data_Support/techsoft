(in-package :custom)
(use-package :oli)


;; does not work
#|
(defvar *sco-ndm_am_store*
     #'MODELMANAGER::ndm_am_store)
(defun MODELMANAGER::ndm_am_store (&rest args)
 (let (result)
  (display "MY MODELMANAGER::ndm_am_store")
  (display (format nil "args=~a" args))
  (setf result (apply *sco-ndm_am_store* args))
  (display (format nil "result=~a" result))
  (custom::ts-set-current-bg-colors)
  result
 )
)
|#

;; does not work
#|
(oli::sd-unsubscribe-event 'EVENT::*DB-DWG-STORE-COMPLETE-EVENT* 'custom::ts-set-current-bg-colors)
(oli::sd-subscribe-event   'EVENT::*DB-DWG-STORE-COMPLETE-EVENT* 'custom::ts-set-current-bg-colors)

|#

(defvar *sco-ndm_am_addthumbnail*
     #'MODELMANAGER::ndm_am_addthumbnail)
(defun MODELMANAGER::ndm_am_addthumbnail (&rest args)
 (let (result)
  ;;(display "MY MODELMANAGER::ndm_am_addthumbnail")
  ;;(display (format nil "args=~a" args))
  (setf result (apply *sco-ndm_am_addthumbnail* args))
  ;;(display (format nil "result=~a" result))
  (custom::ts-set-current-bg-colors)
  result
 )
)

(oli::sd-execute-annotator-command :cmd "DEFINE Awmc_create_mi_file")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Part_name")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Tmp_dir")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Dwg_fil")
(oli::sd-execute-annotator-command :cmd "  Awm_create_mi_file Part_name Tmp_dir Dwg_fil")
(oli::sd-execute-annotator-command :cmd "  Ts_am_set_current_bg")
(oli::sd-execute-annotator-command :cmd "END_DEFINE")

(oli::sd-execute-annotator-command :cmd "DEFINE Annotation_store_thumbnail")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Imgwidth")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Imgheight")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Filename")
(oli::sd-execute-annotator-command :cmd "  PARAMETER Hatchcolor")
(oli::sd-execute-annotator-command :cmd "  Ts_am_set_current_bg")
(oli::sd-execute-annotator-command :cmd "  Annotation_store_thumbnail_old Imgwidth Imgheight Filename")
(oli::sd-execute-annotator-command :cmd "END_DEFINE")

